@ECHO OFF
setlocal enabledelayedexpansion

IF "%PASSWORD%"=="" GOTO MissingPasswd

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

:DeleteLogFiles
call %BatchDir%\common\deletefiles.bat %_Log%

:Setup
echo %_Date% !time! Sample data loading started .......
echo %_Date% !time! Sample data loading started .......>BatchLog.txt
for /F "tokens=*" %%A in ('type "%BatchDir%\LoadData.txt"') do (  
echo %_Date% !time! %%A is in progress... >>BatchLog.txt
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %SQLDir%\LoadData\%%A.sql -v TargetSchema=%TargetSchema% -o log\%%A.txt -I -e -b >>BatchLog.txt 2>&1
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.bat >>BatchLog.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A >>BatchLog.txt)
IF !ERRORLEVEL! NEQ 0 ( echo %_Date% !time! Please check the log file log\%%A.txt >>BatchLog.txt && GOTO Error )
echo.
)
GOTO End

:Error
echo You encountered Error! Please check the BatchLog file for further details... 
setlocal disabledelayedexpansion

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto Error

:End
ECHO %_Date% !time! SUCCESSFULLY executed sample data loading
setlocal disabledelayedexpansion
cmd /k