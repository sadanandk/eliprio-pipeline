@ECHO OFF

IF "%PASSWORD%"=="" GOTO MissingPasswd

REM Read config files for required values  
IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

REM set enviornment variables
set PATH=%PATH%;C:\Program Files\7-Zip\;
set PATH=%PATH%;C:\Program Files\R\R-3.5.0\bin;
SET rootDir=%~dp0
echo %_Date% %time% Start Process Execution >Batchlog.txt
echo %_Date% %time% Start Process Execution

SET executionName=%1
if "%executionName%" NEQ "" (SET executionName=%executionName%_)

REM set sampleDataPath to network path if DB resides on other machine
if "%nwrootPath%" NEQ "" (
SET SampleDataPath=%nwrootPath%\code\sql\SampleDataRun\CSV_for_Tables\
echo setting up n/w path to %nwrootPath% >>Batchlog.txt
) ELSE (
SET SampleDataPath=%~dp0\code\sql\SampleDataRun\CSV_for_Tables\
echo setting up SampleDataPath to %SampleDataPath% >>Batchlog.txt
)

SET Src_Schema=%SrcSchema%
REM Create folder with date & timestamp under log folder & check if the folder already exists
IF not exist %_Log% ( mkdir %_Log% )

REM Delete any log files created in the previous execution 
:DeleteLogFiles
call %BatchDir%\common\deletefiles.bat %_Log%

REM deleteing old input files from the input folder
call %BatchDir%\common\deletefiles.bat %Report%

REM delete old output files
call %BatchDir%\common\deletefiles.bat %outputDir%

copy Config.txt %OUTPUT%\Config.txt >>Batchlog.txt

:ProcessSelection
IF "%PreProcess%"=="Y" ( GOTO PrePRocess ) ELSE ( IF "%RunDRECohort%"=="Y" ( GOTO ProcessDREData ) ELSE ( GOTO ENDDREProcess ) )

:PreProcess
echo.
REM Call Pre-processing batch file 
echo %_Date% %time% Pre-processing  data 
echo %_Date% %time% Pre-processing  data  >>Batchlog.txt

IF not exist %BatchDir%\PreProcess.bat ( echo %_Date% %time% Error Message: PreProcess.bat file does not exist! >>Batchlog.txt && GOTO Error )

Call %BatchDir%\PreProcess.bat %PreProcessList% %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPATH% %SampleDataPath% %OUTPUT% %Report% %sampledatarun% %Src_Schema% >>Batchlog.txt

IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in PreProcess.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed PreProcess.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)
echo.
IF "%RunDRECohort%"=="Y" ( GOTO ProcessDREData ) ELSE ( GOTO ENDDREProcess )

:ProcessDREData
REM Call DRE Processing batch file 
echo %_Date% %time% Process data for DRE
echo %_Date% %time%  Process data for DRE >>Batchlog.txt
echo.
IF not exist %BatchDir%\DRE.bat ( echo %_Date% %time% Error Message: DRE.bat file does not exist! >>Batchlog.txt && GOTO Error )

Call %BatchDir%\DRE.bat %DRElist% %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPATH% %SampleDataPath% %OUTPUT% %Report% %sampledatarun% %utilityDir% %Src_Schema% >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO Error ) 


:ModelScoringPipeline_DRE
REM Call Model Scoring Pipeline
SET featureFilePre=DRE_feature
SET Threshold=%DRE_Threshold%
echo %_Date% %time% Generate Scored file using Model DRE
echo %_Date% %time% Prepare Scored file using Model DRE>>BatchLog.txt
IF not exist %ModelDir%\ModelScoringPipeline.bat ( echo %_Date% %time% Error Message: ModelScoringPipeline.bat file does not exist! >>BatchLog.txt && GOTO Error )
IF not exist %ModelDir%\libs\ ( echo %_Date% %time% Error Message: libs folder does not exist! >>BatchLog.txt && GOTO Error )
Call %ModelDir%\ModelScoringPipeline.bat %DRE% %DREmodel% %Report% %DREModel% %_DT% %outputDir% %_Log% %featureFilePre% %Threshold% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE ModelScoringPipeline.bat) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_ModelScoringPipeline.bat ) >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)


:ENDDREProcess
REM Added model_reporting at GOTO End -- Updated Date: 2019-05-03 
IF "%RunTDSCohort%"=="Y" ( GOTO ProcessTDSData ) ELSE ( IF "%RunDRECohort%"=="Y" ( GOTO model_reporting ) ELSE ( GOTO END) ) 
REM ELSE ( GOTO model_reporting ) 

:ProcessTDSData
REM Call TDS Processing batch file 
echo.
echo %_Date% %time% Process data for TDS
echo %_Date% %time%  Process data for TDS >>Batchlog.txt
IF not exist %BatchDir%\TDS.bat ( echo %_Date% %time% Error Message: TDS.bat file does not exist! >>Batchlog.txt && GOTO Error )

Call %BatchDir%\TDS.bat %TDSList% %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPATH% %SampleDataPath% %OUTPUT% %Report% %sampledatarun% %utilityDir% %Src_Schema% >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO Error ) 
echo.

:TDS_Feature_Encode
REM call to encode  input feature file
echo %_Date% %time% Encode feature file for TDS
IF not exist %Report%\TDS_feature.csv ( echo %_Date% %time% WARN Message: feature file for TDS does not exist! >>BatchLog.txt && GOTO Error )
Call %BatchDir%\modelreports\R_Encoding.bat %rootDir% %outputDir% %TDSmodel% %TDS% %_DT% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model: ERRORLEVEL %ERRORLEVEL% in TDS R_Encoding.bat) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS R_Encoding.bat >>BatchLog.txt)
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)
echo. >>BatchLog.txt
echo.

:ModelScoringPipeline_TDS
REM Call Model Scoring Pipeline
SET featureFilePre=TDS_feature_en
echo %_Date% %time% Generate Scored file using Model TDS
echo %_Date% %time%  Prepare Scored file using Model TDS>>BatchLog.txt
IF not exist %ModelDir%\ModelScoringPipeline.bat ( echo %_Date% %time% Error Message: ModelScoringPipeline.bat file does not exist! >>BatchLog.txt && GOTO Error )
IF not exist %ModelDir%\libs\ ( echo %_Date% %time% Error Message: libs folder does not exist! >>BatchLog.txt && GOTO Error )
Call %ModelDir%\ModelScoringPipeline.bat %TDS% %TDSmodel% %Report% %TDSModel% %_DT% %outputDir% %_Log% %featureFilePre% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS ModelScoringPipeline.bat) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_ModelScoringPipeline.bat ) >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)
echo.

:model_reporting
echo %_Date% %time% Generate KPI, Summary and Diagnostic reports 
echo %_Date% %time% Generate KPI, Summary and Diagnostic reports >>BatchLog.txt
call %BatchDir%\modelreports\ModelReporting.bat >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model: ERRORLEVEL %ERRORLEVEL% in ModelReporting.bat) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed ModelReporting.bat ) >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)
echo.

:Reporting
IF "%RunDRECohort%"=="N" ( GOTO TDS_Reporting )

:DRE_Reporting
REM Call Reporting batch file 
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for DRE
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for DRE >>Batchlog.txt
IF not exist %BatchDir%\Reporting\DRE_Reporting.bat ( echo %_Date% %time% Error Message: DRE_Reporting.bat file does not exist! >>Batchlog.txt && GOTO Error )
Call %BatchDir%\reporting\DRE_Reporting.bat %~dp0 %_DT% >>BatchLog.txt
REM IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Reporting.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Reporting.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: Some of the reports has not been generated for DRE. Please check the logs) 
echo.

:TDS_Reporting
IF "%RunTDSCohort%"=="N" ( GOTO END )
REM Call Reporting batch file 
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for TDS
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for TDS >>Batchlog.txt
IF not exist %BatchDir%\reporting\TDS_Reporting.bat ( echo %_Date% %time% Error Message: TDS_Reporting.bat file does not exist! >>Batchlog.txt && GOTO Error )
Call %BatchDir%\reporting\TDS_Reporting.bat %~dp0 %_DT% >>BatchLog.txt
REM IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS_Reporting.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_Reporting.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: Some of the reports has not been generated for TDS. Please check the logs)  
echo.

Goto End

REM If any error is encountered 
:ERROR
copy BatchLog.txt %OUTPUT%\BatchLog.txt
echo %_Date% %time%  Pipeline: You encountered Error! Check log file for further detail.
SET executionName=%executionName%Error_
GOTO ARCHIVE

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed Pipeline
copy BatchLog.txt %OUTPUT%\BatchLog.txt>>Batchlog.txt
IF "%executionName%"=="" ( SET executionName=%TargetSchema%_)
GOTO ARCHIVE

:ARCHIVE
echo.
call %BatchDir%\archive.bat %_DT% %executionName% %OUTPUT% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 (
ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in archive.bat 
) ELSE (
ECHO %_Date% %time% Created archive file with name %executionName%%_DT%.zip 
)

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto Exit

:Exit
cmd /k