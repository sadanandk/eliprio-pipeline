
@ECHO OFF


IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%Login%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort


Rem "C:\Program Files\Microsoft SQL Server\110\Tools\Binn"  
Rem "C:\Program Files (x86)\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn"
Rem "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn"
REM SET SQLCMD="C:\Program Files (x86)\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE"
REM SET PATH=F:\VikasS\E\IMS_DRE_End_to_End_Pipeline
rem Set Findstr="C:\Windows\system32\findstr.exe"
SET SERVER=%SERVER%
SET DB=%DB%
SET Login=%Login%
SET PASSWORD=%PASSWORD%
SET TargetSchema=%TargetSchema%
SET Step=%Step%
SET SQLCMD=%SQLCMD%
SET INPUT=%ScriptPATH%
SET OUTPUT=%OUTPUT%
SET Report=%Report%
SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET B=N
SET mypath=%~dp0
setlocal enabledelayedexpansion

REM IF "%SQLCMD%"=="" GOTO Abort
IF "%INPUT%"=="" GOTO Abort
IF "%OUTPUT%"=="" GOTO Abort
IF "%Report%"=="" GOTO Abort
if not exist %utilityDir%\CSVReader.class SET B=Y
Rem echo %B%
if not exist %utilityDir%\CSVReader.java SET B=Y
Rem echo %B%
Rem echo "%B%"=="Y"
Rem echo Java Utility %B%
IF "%B%"=="Y" ( echo Java utility missing && GOTO Error)
Rem echo 'Java Utility Missing...'  

Rem echo %INPUT%
Rem %SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\SQLTest.sql -v TargetSchema=%TargetSchema% -o %OUTPUT%\SQLTest.txt -I 
rem echo %Step%

:DeleteLogFiles
Rem if exist %OUTPUT%\*.txt ( del %OUTPUT%\*.txt )

if exist %Report%\HeaderTemp.csv ( del %Report%\HeaderTemp.csv )

if exist %Report%\Header.csv ( del %Report%\Header.csv )

if exist %Report%\Detail.csv ( del %Report%\Detail.csv )


:TDSCohort
echo.
echo %_Date% %time%  TDS Cohort execution started
echo.
for /F "tokens=*" %%A in ('type "%mypath%\%TDSList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDS\%%A.sql -v TargetSchema=%TargetSchema% Src_Schema=%Src_Schema% -o %OUTPUT%\%%A.txt -I -e -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\%%A.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)
echo.
echo %_Date% %time%  TDS Cohort execution completed


:Step4GenerateCSV
echo.
echo.
echo %_Date% %time%  Generate CSV File...
REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDS\CSV_Header.sql -b > %Report%\HeaderTemp.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %Report%\HeaderTemp.csv && GOTO Error )
Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%Report%\HeaderTemp.csv -DoutputF=%Report%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )
rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDS\Combine_Query_AED_TDS_Features.sql -b > %Report%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %Report%\Detail.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %Report%\TDS_feature.csv ( del %Report%\TDS_feature.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %Report%\Header.csv %Report%\TDS_feature.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%Report%\TDS_feature.csv
rem echo append detail data to report file: %time%
type %Report%\Detail.csv >> %Report%\TDS_feature.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

del %Report%\HeaderTemp.csv
del %Report%\Header.csv
del %Report%\Detail.csv

echo %_Date% %time% CSV File generated.
echo.

echo %_Date% %time%  Generate CSV File For Payer Type Patient List...

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDS\TDS_Payer_Type_PatientList.sql -b > %Report%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %Report%\Detail.csv && GOTO Error )
REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDS\TDS_Payer_Type_PatientListHeader.sql -b > %Report%\HeaderTemp.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %Report%\HeaderTemp.csv && GOTO Error )
Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%Report%\HeaderTemp.csv -DoutputF=%Report%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )


rem echo delete existing report file: %time%
if exist %Report%\TDS_Payer_Type_PatientList.csv ( del %Report%\TDS_Payer_Type_PatientList.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %Report%\Header.csv %Report%\TDS_Payer_Type_PatientList.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%Report%\TDS_Payer_Type_PatientList.csv
rem echo append detail data to report file: %time%
type %Report%\Detail.csv >> %Report%\TDS_Payer_Type_PatientList.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

del %Report%\HeaderTemp.csv
del %Report%\Header.csv
del %Report%\Detail.csv

echo %_Date% %time% CSV File generated For Payer Type Patient List.
echo.

GOTO End

:Abort
echo Enter login and password 
echo Exiting...
Goto End
Rem Exit

:Error
echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End
exit /b 0
Rem echo %_Date% %time% Execution of SQL Batch file completed




