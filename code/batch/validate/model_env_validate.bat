@ECHO OFF
REM Java path verification
echo.
echo ################## validation starts ####################
setlocal enabledelayedexpansion

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

REM check java version and path
ECHO.
ECHO %_Date% !time! check java version and path
for /f tokens^=2-5^ delims^=.-_^" %%j in ('java -fullversion 2^>^&1') do @set "java_ver=%%j%%k"
echo %_Date% !time! java_ver %java_ver%
if %errorlevel% NEQ 1 (
(echo %java_ver% | findstr /i %CNST_JAVA_ver% >nul) && (echo %_Date% !time! Test Passed::java version is 1.8) || (echo %_Date% !time! Java version is not "1.8" && GOTO ERROR)
)
if %errorlevel% NEQ 1 (
IF !JAVA_HOME! NEQ '' ( ECHO %_Date% !time! JAVA_HOME is set to %JAVA_HOME% ) ELSE ( ECHO %_Date% !time! JAVA_HOME is not set && GOTO Error)
)


REM check 7z version and path
ECHO.
ECHO %_Date% !time! check 7z version and path
for /f "tokens=2" %%i in ('7z ^| findstr /C:"7-Zip"') do set sevenz_ver=%%i
echo %_Date% !time! 7z_ver %sevenz_ver%
if "%sevenz_ver%"=="" (
ECHO %_Date% !time! 7z is not installed && GOTO Error
) ELSE (
ECHO %_Date% !time! 7z is installed
)
  
REM check RScript version 
ECHO.
ECHO %_Date% !time! check RScript version
for /f "tokens=5" %%g in ('RScript --version 2^>^&1') do set R_ver=%%g
echo %_Date% !time! RScript_ver %R_ver%
if "%R_ver%" NEQ "" ( 
(echo %R_ver% | findstr /i %CNST_RScript_ver% >nul) && (echo %_Date% !time! Test Passed::RScript version is 3.5.1) || (echo %_Date% !time! RScript version is not "3.5.1" && GOTO ERROR)
)
  
  
REM check R libraries installed
ECHO.
ECHO %_Date% !time! check R libraries installed
SET libloc="C:\Program Files\R\R-3.5.1\library"
Call RScript  --vanilla --no-save code\R\validateRLib.R %libloc%
if %errorlevel% NEQ 1 (echo %_Date% !time! Test Passed::RScript libraries are available) ELSE (echo %_Date% !time! RScript libraries were not installed && GOTO ERROR)
)

:SUCCESS
echo.
echo %_Date% !time! Model env. setup validation successful!!
echo ################## validation ends ####################
echo.
ENDLOCAL
GOTO END

:ERROR
echo ERROR!! Model Validation of enviornment failed please check log file for further details. 
echo ################## validation ends ####################
echo.
ENDLOCAL
exit /b 1

:END
exit /b 0

