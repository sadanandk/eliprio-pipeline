@ECHO OFF
REM Java path verification
echo.
echo ################## validation starts ####################
setlocal enabledelayedexpansion

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

REM check java version and path
ECHO.
ECHO %_Date% !time! check java version and path
for /f tokens^=2-5^ delims^=.-_^" %%j in ('java -fullversion 2^>^&1') do @set "java_ver=%%j%%k"
echo %_Date% !time! java_ver %java_ver%
if %errorlevel% NEQ 1 (
(echo %java_ver% | findstr /i %CNST_JAVA_ver% >nul) && (echo %_Date% !time! Test Passed::java version is 1.8) || (echo %_Date% !time! Java version is not "1.8" && GOTO ERROR)
)
if %errorlevel% NEQ 1 (
IF !JAVA_HOME! NEQ '' ( ECHO %_Date% !time! JAVA_HOME is set to %JAVA_HOME% ) ELSE ( ECHO %_Date% !time! JAVA_HOME is not set && GOTO Error)
)


REM check 7z version and path
ECHO.
ECHO %_Date% !time! check 7z version and path
for /f "tokens=2" %%i in ('7z ^| findstr /C:"7-Zip"') do set sevenz_ver=%%i
echo %_Date% !time! 7z_ver %sevenz_ver%
if "%sevenz_ver%"=="" (
ECHO %_Date% !time! 7z is not installed && GOTO Error
) ELSE (
ECHO %_Date% !time! 7z is installed
)


:SUCCESS
echo.
echo %_Date% !time! Feature env. setup validation successful!!
echo ################## validation ends ####################
echo.
ENDLOCAL
GOTO END

:ERROR
echo ERROR!! Feature Validation of enviornment failed please check log file for further details. 
echo ################## validation ends ####################
echo.
ENDLOCAL
exit /b 1

:END
exit /b 0

