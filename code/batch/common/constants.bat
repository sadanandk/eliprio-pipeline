
REM Set Date_=%date:~10,4%%date:~4,2%%date:~7,2%

REM Set Date_time_stamp for the log folder
SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET _Time=%time:~0,2%%time:~3,2%

REM Set the log path
SET SQLCMD=SQLCMD
SET ScriptPath=code\sql
SET _Log=log
SET _DT=%_Date%%_Time: =0%
SET Output=%_Log%
SET INPUT=%ScriptPath%
SET Report=input
SET PreProcessList=PreProcess.txt
SET SampleDataList=SampleData.txt
SET DREList=DRE.txt
SET DREListUCB=DREUCB.txt
SET TDSList=TDS.txt
SET BatchDir=code\batch
SET ModelDir=code\model
SET SQLDir=code\sql
SET outputDir=output
SET utilityDir=code\utility 
REM echo %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %Step% %SQLCMD% %ScriptPATH% %OUTPUT%
SET SmokeTestList=SmokeTest.txt
SET DREmodel=DRE_SHA
SET TDSmodel=TDS_AED_SHA
SET TDS=TDS
SET DRE=DRE
SET BulkInsertScript=DML_Script
Rem linux path
SET Rlib=C:/Program Files/R/R-3.5.1/library
Rem windows path
SET RScript=C:\Program Files\R\R-3.5.1\bin
SET 7Zip=C:\Program Files\7-Zip\
SET delimiter=","
Rem SET delimiter1="|"
Rem Cohort Characterization for DRE and TDS
SET CC_Rpt=d6.4_
SET TDSCList=TDS_CohortCharacterization.txt
SET TDSCCList=TDS_CohortCharacterizationReport.txt
SET DRECCList=DRE_CohortCharacterization.txt
SET DRECCCList=DRE_CohortCharacterizationReport.txt
REM DRE 2.5 Evaluation Period
SET DRE2_5List=DRE_2_5yr_evl_period.txt
REM DRE EndPoint 2.1 for DRE
SET DEP21List=DRE_Endpoint2_1.txt
SET DEP211List=DRE_Endpoint2_1Report.txt
SET DEP211_1List=DRE_Endpoint2_1_1Report.txt
SET DEP21PHCList=DRE_Endpoint2_1PHC.txt
SET DEP_Rpt=d6.5_
Rem Pipeline Metric for DRE and TDS
SET PM_Rpt=d6.3_
SET PMMList=PipelineMetric.txt
Rem DRE End Point Report
SET DERList=DRE_Endpoint2_2.txt
SET DRECList=DRE_Endpoint2_2Report.txt
SET DREENDRList=DRE_Endpoint2_3Report.txt
SET DREENDList=DRE_Endpoint2_3.txt
REM DRE model threshold .. do not change this without notifying to concern 
SET DRE_Threshold=0.0905
REM SET DRE_highcut=0.2975
REM delimiter to save files
SET delimiter=","
SET Score_rpt=D5_
SET DREScore_rpt=d6_5_d5_
SET KPI_rpt=D6.1_
SET DIAG_rpt=D6.2_
SET Summ_rpt=D6.2_
SET RiskSt_rpt=D6.5_
SET Conf_rpt=D6.1_

REM enviornment setup 1.8 to 18  CNST for constant
SET CNST_JAVA_ver=18
SET CNST_RScript_ver=3.5.1

REM Existing batch after read 
exit /b 0