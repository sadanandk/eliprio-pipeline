SET Folder=%1
IF exist %Folder%\* (
del /q %Folder%\*
for /d %%x in (%Folder%\*) do @rd /s /q "%%x" 
set _TMP=
for /f "delims=" %%a in ('dir /b %Folder%') do set _TMP=%%a
IF {%_TMP%} NEQ {} ( 
echo WARN::Some of the files from %Folder% folder are in use.
echo.
)
)

exit /b
