@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

REM generate SHA reports
if "%nwrootPath%" NEQ "" (
SET CsvSHAPath=%nwrootPath%\code\sql\Load_Source_Data\CSV\
REM echo setting up n/w path to %nwrootPath% >>Batchlog.txt
) ELSE (
SET CsvSHAPath=%~dp0\code\sql\Load_Source_Data\CSV\
REM echo setting up SampleDataPath to %CsvSHAPath% >>Batchlog.txt
)

echo CsvSHAPath=%CsvSHAPath%
echo log=%log%

:StepSampleData
echo %_Date% %time% DRE Cohort Characterization Report execution started
for /F "tokens=*" %%A in ('type "%mypath%\%DRECCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\%%A.sql -v TargetSchema=%TargetSchema% CsvSHAPath=%CsvSHAPath% -b -I
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.)


:StepGenerateCSV
echo %_Date% %time%  Generate CSV File For  DRE Cohort Characterization Report...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_CohortCharacterizationHeadercolumn.sql -b > %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_CohortCharacterizationHeadercolumn2.sql -b > %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn2.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn2.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_CohortCharacterizationHeader.sql -b > %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn2.csv -DoutputF=%outputDir%\Header1.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%-

REM For One Year
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter%  -W -h-1 -i %INPUT%\DREReport\DRE_CohortCharacterizationData_with_inditerminent.sql -b > %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv ( del %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

copy %outputDir%\Header0.csv %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv

type %outputDir%\Header.csv >> %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv >> %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv
echo *--^> Order followed is of PHS w/ Indeterminates >>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv
echo All variable calculations (except for Outcome Variable which is for evaluation period) are taken from the year leading up to and including the index date (i.e. observation period) >>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_1yr.csv

del %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv

REM ------------------2_5 year--------------------------------------

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter%  -W -h-1 -i %INPUT%\DREReport\DRE_CohortCharacterizationData_with_inditerminent2_5.sql -b > %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv ( del %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

copy %outputDir%\Header1.csv %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >> %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv >> %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv
echo *--^> Order followed is of PHS w/ Indeterminates >>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv
echo All variable calculations (except for Outcome Variable which is for evaluation period) are taken from the year leading up to and including the index date (i.e. observation period) >>%outputDir%\%CC_Rpt%rpt_dre_cohortcharacterization_2_5yr.csv

del %outputDir%\%CC_Rpt%rpt_dre_cohortcharacterizationData.csv
del %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeader.csv
del %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn.csv
del %outputDir%\%CC_Rpt%Rpt_DRE_CohortCharacterizationHeadercolumn2.csv
del %outputDir%\Header.csv
del %outputDir%\Header1.csv

REM generate SHA reports

REM xcopy /y %CsvSHAPath%Cohort_Char_SHA_1_Yr.csv %outputDir%\
REM xcopy /y %CsvSHAPath%Cohort_Char_SHA_2_5_Yr.csv %outputDir%\
REM xcopy /y %CsvSHAPath%Cohort_Char_SHA_Train_1_Yr.csv %outputDir%\
REM xcopy /y %CsvSHAPath%Cohort_Char_SHA_Train_2_5_Yr.csv %outputDir%\

echo %_Date% %time% CSV File generated.
echo.

GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End