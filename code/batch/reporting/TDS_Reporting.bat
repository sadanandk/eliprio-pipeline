@ECHO OFF

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)

SET rootDir=%1

:CohortCharacterization
REM Call TDS CohortCharacterization batch file 
echo %_Date% %time% TDS Cohort Characterization Report >>%Output%\TDSReporting.txt
IF not exist %BatchDir%\reporting\TDS_CohortCharacterization.bat ( echo %_Date% %time% Error Message: TDS_CohortCharacterization.bat file does not exist! >>%Output%\TDSReporting.txt && GOTO Error )
Call %BatchDir%\reporting\TDS_CohortCharacterization.bat %TDSCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir%>>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS_CohortCharacterization.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_CohortCharacterization.bat ) >>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The baseline characteristics report has not been generated for TDS. Please check the logs in TDSReporting.txt )
echo.>>%Output%\TDSReporting.txt

REM IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR ) ELSE ( GOTO End )
:Pipeline Metric
REM Call Pipeline Metric batch file 
echo %_Date% %time% Pipeline Metric Report For TDS >>%Output%\TDSReporting.txt
IF not exist %BatchDir%\reporting\TDS_PipelineMetric.bat ( echo %_Date% %time% Error Message: TDS_PipelineMetric.bat file does not exist! >>%Output%\TDSReporting.txt && GOTO Error )

Call %BatchDir%\reporting\TDS_PipelineMetric.bat %PMMList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir%>>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS_PipelineMetric.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_PipelineMetric.bat ) >>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The TDS_pipelinemetric report has not been generated for TDS. Please check the logs in TDSReporting.txt)
echo. >>%Output%\TDSReporting.txt

:Match_Flag_condition
REM Call Pipeline Metric batch file 
echo %_Date% %time% Pipeline Metric Report For TDS >>%Output%\TDSReporting.txt
IF not exist %BatchDir%\reporting\TDS_PipelineMetric.bat ( echo %_Date% %time% Error Message: TDS_PipelineMetric.bat file does not exist! >>%Output%\TDSReporting.txt && GOTO Error )

Call %BatchDir%\reporting\TDS_PipelineMetric.bat %PMMList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir%>>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS_PipelineMetric.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_PipelineMetric.bat ) >>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The TDS_pipelinemetric report has not been generated for TDS. Please check the logs in TDSReporting.txt)
echo. >>%Output%\TDSReporting.txt

:TDS_Patient_Matching
echo %_Date% %time% TDS Patient Matching Report  REM >>%Output%\TDSReporting.txt
IF not exist %BatchDir%\reporting\TDS_Matching_Patient.bat ( echo %_Date% %time% Error Message: TDS_Matching_Patient.bat file does not exist! >>%Output%\TDSReporting.txt && GOTO Error )

Call %BatchDir%\reporting\TDS_Matching_Patient.bat %DEP21PHCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% %outDataPath% >>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS_Matching_Patient.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_Matching_Patient.bat ) >>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The TDS_Matching_Patient report has not been generated for DRE. Please check the logs in TDSReporting.txt)
echo. REM >>%Output%\TDSReporting.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR ) ELSE ( GOTO End )

:ERROR
echo You encountered Error while gnerating reports! Please check the TDSReporting.txt log file for further details... 
exit /b 1

:END
exit /b


:ERROR
echo You encountered Error while gnerating reports! Please check the TDSReporting.txt log file for further details... 
exit /b 1

:END
exit /b