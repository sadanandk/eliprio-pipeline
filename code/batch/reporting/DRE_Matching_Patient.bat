@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

:StepDREMatchingPatient
echo %_Date% %time% DRE Matching Patient Report execution started
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_Patient_Matching.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% %time% DRE Matching Patient Report execution %%A completed
echo.


:StepGenerateCSV
echo %_Date% %time%  Generate CSV File For  DRE 2.5 Evaluation Report...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_Patient_MatchingHeader.sql -b > %outputDir%\DRE_Patient_MatchingHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\DRE_Patient_MatchingHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\DRE_Patient_MatchingHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter%  -W -h-1 -i %INPUT%\DREReport\DRE_Patient_MatchingData.sql -b > %outputDir%\DRE_Patient_MatchingData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\DRE_Patient_MatchingData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\Patient_Matching_Flag.csv ( del %outputDir%\Patient_Matching_Flag.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header.csv %outputDir%\Patient_Matching_Flag.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\Patient_Matching_Flag.csv

rem echo append detail data to report file: %time%
type %outputDir%\DRE_Patient_MatchingData.csv >> %outputDir%\Patient_Matching_Flag.csv
rem type %outputDir%\DRE_Patient_MatchingData.csv >> abc.txt
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

del %outputDir%\DRE_Patient_MatchingHeader.csv
del %outputDir%\Header.csv
del %outputDir%\DRE_Patient_MatchingData.csv

echo %_Date% %time% CSV File generated.
echo.

GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End