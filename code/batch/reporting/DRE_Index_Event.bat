@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

if "%nwrootPath%" NEQ "" (
SET SampleDataPath=%nwrootPath%\code\sql\Setup\CSV_for_Tables\
REM echo setting up n/w path to %nwrootPath% >Batchlog.txt
) ELSE (
SET SampleDataPath=%~dp0\code\sql\Setup\CSV_for_Tables\
REM echo setting up SampleDataPath to %SampleDataPath% >Batchlog.txt
)

echo %_Date% %time% DRE Index Event Report execution started
for /F "tokens=*" %%A in ('type "%mypath%\%DRECCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_Index_Event.sql -v TargetSchema=%TargetSchema% -b >log\DRE_Index_Event.txt -I
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in DRE_Index_Event.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DRE_Index_Event.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.)

:StepGenerateCSV
echo %_Date% %time%  Generate CSV File For  DRE Index Event Report...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_Index_Event_Header.sql -b > %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Header.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Header.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Header.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

REM Data 

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter%  -W -h-1 -i %INPUT%\DREReport\DRE_Index_Event_Data1.sql -b > %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData1.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData1.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv ( del %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header.csv %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData1.csv >> %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

REM Data 

REM %SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter%  -W -h-1 -i %INPUT%\DREReport\DRE_Index_Event_Data2.sql -b > %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData2.csv
REM IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData2.csv && GOTO Error )
REM 
REM echo.>>%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
REM echo.>>%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
REM 
REM echo Index Event 2.5 Year>>%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
REM echo.>>%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
REM 
REM type %outputDir%\Header.csv>>%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
REM IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )
REM 
REM echo.>>%outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
REM 
REM type %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData2.csv >> %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Report.csv
REM IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

del %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData1.csv
REM del %outputDir%\%CC_Rpt%Rpt_DRE_Index_EventData2.csv
del %outputDir%\%CC_Rpt%Rpt_DRE_Index_Event_Header.csv
del %outputDir%\Header.csv

REM Copy Score file of Train and Test 
REM echo copying score files of Train and Test

REM echo f | xcopy /y /f %SampleDataPath%src_sample\d5_dre_sha_score_1_0yr_Test.csv OUTPUT\d5_dre_sha_score_1_0yr_Test.csv
REM echo f | xcopy /y /f %SampleDataPath%src_sample\d5_dre_sha_score_1_0yr_Train.csv OUTPUT\d5_dre_sha_score_1_0yr_Train.csv
REM echo f | xcopy /y /f %SampleDataPath%src_sample\d6_5_d5_dre_sha_score_2_5yr_Test.csv OUTPUT\d6_5_d5_dre_sha_score_2_5yr_Test.csv
REM echo f | xcopy /y /f %SampleDataPath%src_sample\d6_5_d5_dre_sha_score_2_5yr_Train.csv OUTPUT\d6_5_d5_dre_sha_score_2_5yr_Train.csv
REM echo f | xcopy /y /f %SampleDataPath%src_sample\KPI.csv OUTPUT\KPI.csv

GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End
