@ECHO OFF

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
REM call code\batch\common\constants.bat

SET rootDir=%1


:DREIndexEvent
REM Call BaselineCharacteristic batch file 
echo %_Date% %time% DRE Index Evenet Report >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_Index_Event.bat ( echo %_Date% %time% Error Message: DRE_CohortCharacterization.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_Index_Event.bat %DRECCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Index_Event.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Index_Event.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The DRE_Index_Event report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt


:CohortCharacterization
REM Call BaselineCharacteristic batch file 
echo %_Date% %time% DRE Cohort Characterization Report >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_CohortCharacterization.bat ( echo %_Date% %time% Error Message: DRE_CohortCharacterization.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_CohortCharacterization.bat %DRECCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_CohortCharacterization.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_CohortCharacterization.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The baseline characteristics report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt

:Pipeline Metric
REM Call Pipeline Metric batch file 
echo %_Date% %time% Pipeline Metric Report   >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\PipelineMetric.bat ( echo %_Date% %time% Error Message: PipelineMetric.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\PipelineMetric.bat %PMMList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir%>>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in PipelineMetric.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed PipelineMetric.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The pipelinemetric report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt

:DRE_EndPoint
IF "%Environment%"=="PHC" ( GOTO DRE_EndPointPHC ) ELSE ( GOTO DRE_EndPointUCB )

:DRE_EndPointPHC
REM Call DRE Endpoint batch file for PHC
echo %_Date% %time% DRE EndPoint2_1 Report for  PHC   >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_Endpoint2_1PHC.bat ( echo %_Date% %time% Error Message: DRE_Endpoint2_1PHC.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_Endpoint2_1PHC.bat %DEP21PHCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% %outDataPath% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Endpoint2_1PHC.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Endpoint2_1PHC.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The DRE_Endpoint2_1 report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR ) ELSE ( GOTO End )

:DRE_EndPointUCB
REM Call DRE Endpoint batch file for UCB
echo %_Date% %time% DRE EndPoint2_1 Report for UCB  >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_Endpoint2_1.bat ( echo %_Date% %time% Error Message: DRE_Endpoint2_1.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_Endpoint2_1.bat %DEP21List% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% %outDataPath% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Endpoint2_1.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Endpoint2_1.bat ) >>%Output%\DREReporting.txt
echo. >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The DRE_Endpoint2_1 report has not been generated for DRE. Please check the logs in DREReporting.txt)

echo %_Date% %time% DRE Economic EndPoint Report for UCB  >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_Economic_Endpoint.bat ( echo %_Date% %time% Error Message: DRE_Economic_Endpoint.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_Economic_Endpoint.bat %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% %outDataPath% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Economic_Endpoint.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Economic_Endpoint.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The DRE_Economic_Endpoint report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt
REM IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR ) ELSE ( GOTO End )


:DRE_Patient_Matching
echo %_Date% %time% DRE Patient Matching Report REM  >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_Matching_Patient.bat ( echo %_Date% %time% Error Message: DRE_Matching_Patient.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_Matching_Patient.bat %DEP21PHCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% %outDataPath% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Endpoint2_1PHC.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Matching_Patient.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: The DRE_Matching_Patient report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. REM  >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR ) ELSE ( GOTO End )

:ERROR
echo You encountered Error while gnerating reports! Please check the DREReporting.txt log file for further details... 
IF exist %outputDir%\d5_dre_sha_score_1_0yr_Test.csv del %outputDir%\d5_dre_sha_score_1_0yr_Test.csv
IF exist %outputDir%\d5_dre_sha_score_1_0yr_Train.csv     del %outputDir%\d5_dre_sha_score_1_0yr_Train.csv
IF exist %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Test.csv     del %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Test.csv
IF exist %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Train.csv     del %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Train.csv
IF exist %outputDir%\KPI.csv     del %outputDir%\KPI.csv
IF exist %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv     del %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv
IF exist %outputDir%\d6.5_rpt_dre_subpop_cohorts_2_5Y.csv     del %outputDir%\d6.5_rpt_dre_subpop_cohorts_2_5Y.csv
exit /b 1



:END
IF exist %outputDir%\d5_dre_sha_score_1_0yr_Test.csv del %outputDir%\d5_dre_sha_score_1_0yr_Test.csv
IF exist %outputDir%\d5_dre_sha_score_1_0yr_Train.csv     del %outputDir%\d5_dre_sha_score_1_0yr_Train.csv
IF exist %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Test.csv     del %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Test.csv
IF exist %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Train.csv     del %outputDir%\d6_5_d5_dre_sha_score_2_5yr_Train.csv
IF exist %outputDir%\KPI.csv     del %outputDir%\KPI.csv
IF exist %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv     del %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv
IF exist %outputDir%\d6.5_rpt_dre_subpop_cohorts_2_5Y.csv     del %outputDir%\d6.5_rpt_dre_subpop_cohorts_2_5Y.csv
exit /b