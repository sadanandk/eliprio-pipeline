@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

:StepGenerateTxt

echo %_Date% %time% Economic EndPoint Report execution started
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_High_Medium_Stratification_Economic_EndPoints_Data.sql -v TargetSchema=%TargetSchema% -o %outputDir%\%DEP_Rpt%rpt_dre_high_medium_economic_endpoint.txt -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in DREReporting.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_High_Medium_Stratification_Economic_EndPoints_Data )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% %time% rpt_dre_high_medium_economic_endpoint completed

echo %_Date% %time% Economic EndPoint Report execution started
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_High_Stratification_Economic_EndPoints_Data.sql -v TargetSchema=%TargetSchema% -o %outputDir%\%DEP_Rpt%rpt_dre_high_economic_endpoint.txt -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in DREReporting.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_High_Stratification_Economic_EndPoints_Data )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% %time% rpt_dre_high_economic_endpoint completed

echo %_Date% %time% Economic EndPoint Report execution started
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_Low_Stratification_Economic_EndPoints_Data.sql -v TargetSchema=%TargetSchema% -o %outputDir%\%DEP_Rpt%rpt_dre_low_economic_endpoint.txt -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in DREReporting.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Low_Stratification_Economic_EndPoints_Data )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% %time% rpt_dre_low_economic_endpoint completed

echo %_Date% %time% Economic EndPoint Report execution started
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_NotHigh_Stratification_Economic_EndPoints_Data.sql -v TargetSchema=%TargetSchema% -o %outputDir%\%DEP_Rpt%rpt_dre_nothigh_economic_endpoint.txt -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in DREReporting.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_NotHigh_Stratification_Economic_EndPoints_Data )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% %time% rpt_dre_nothigh_economic_endpoint completed


:StepGenerateCSV

echo %_Date% %time%  Generate CSV File For  DRE Economic EndPoint for High Medium Stratification Report...

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -i %INPUT%\DREReport\DRE_High_Medium_Stratification_Economic_EndPoints_Data.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )


rem echo delete existing report file: %time%

if exist %outputDir%\%DEP_Rpt%rpt_dre_high_medium_economic_endpoints.csv ( del %outputDir%\%DEP_Rpt%rpt_dre_high_medium_economic_endpoints.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo append detail data to report file: %time%
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_high_medium_economic_endpoints.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_high_medium_economic_endpoints.csv

del %outputDir%\Detail.csv

echo %_Date% %time% CSV File generated.


echo %_Date% %time%  Generate CSV File For  DRE Economic EndPoint for High Stratification Report...

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -i %INPUT%\DREReport\DRE_High_Stratification_Economic_EndPoints_Data.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )


rem echo delete existing report file: %time%

if exist %outputDir%\%DEP_Rpt%rpt_dre_high_economic_endpoints.csv ( del %outputDir%\%DEP_Rpt%rpt_dre_high_economic_endpoints.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo append detail data to report file: %time%
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_high_economic_endpoints.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_high_economic_endpoints.csv

del %outputDir%\Detail.csv

echo %_Date% %time% CSV File generated.


echo %_Date% %time%  Generate CSV File For  DRE Economic EndPoint for Low Stratification Report...

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -i %INPUT%\DREReport\DRE_Low_Stratification_Economic_EndPoints_Data.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )


rem echo delete existing report file: %time%

if exist %outputDir%\%DEP_Rpt%rpt_dre_low_economic_endpoints.csv ( del %outputDir%\%DEP_Rpt%rpt_dre_low_economic_endpoints.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo append detail data to report file: %time%
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_low_economic_endpoints.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_low_economic_endpoints.csv

del %outputDir%\Detail.csv

echo %_Date% %time% CSV File generated.

echo %_Date% %time%  Generate CSV File For  DRE Economic EndPoint for Not High Stratification Report...

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -i %INPUT%\DREReport\DRE_NotHigh_Stratification_Economic_EndPoints_Data.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )


rem echo delete existing report file: %time%

if exist %outputDir%\%DEP_Rpt%rpt_dre_nothigh_economic_endpoints.csv ( del %outputDir%\%DEP_Rpt%rpt_dre_nothigh_economic_endpoints.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo append detail data to report file: %time%
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_nothigh_economic_endpoints.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_nothigh_economic_endpoints.csv

del %outputDir%\Detail.csv

echo %_Date% %time% CSV File generated.

echo %_Date% %time%  Generate CSV File For  DRE Cost Consolidated Report ...

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header0.sql -b > %outputDir%\HeaderTemp0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header1.sql -b > %outputDir%\HeaderTemp.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv ( del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv ( del %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >> %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv

rem echo append detail data to report file: %time%
type %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv

del %outputDir%\HeaderTemp0.csv
del %outputDir%\HeaderTemp.csv
del %outputDir%\Header0.csv
del %outputDir%\Header.csv
del %outputDir%\Detail.csv
del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data.csv

echo %_Date% %time% CSV File generated.
echo.



echo %_Date% %time%  Generate CSV File For  DRE Cost Consolidated Report ...

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header0_without_Indeterminate.sql -b > %outputDir%\HeaderTemp0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header1.sql -b > %outputDir%\HeaderTemp.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_without_Indeterminate.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv ( del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >> %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv

rem echo append detail data to report file: %time%
type %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv

del %outputDir%\HeaderTemp0.csv
del %outputDir%\HeaderTemp.csv
del %outputDir%\Header0.csv
del %outputDir%\Header.csv
del %outputDir%\Detail.csv
del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate.csv

echo %_Date% %time% CSV File generated.
echo.

echo %_Date% %time%  Generate CSV File For  DRE Cost Consolidated Report ...

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header0_2_5Y.sql -b > %outputDir%\HeaderTemp0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header1_2_5Y.sql -b > %outputDir%\HeaderTemp.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_2_5Y.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv ( del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >> %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv

rem echo append detail data to report file: %time%
type %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv

del %outputDir%\HeaderTemp0.csv
del %outputDir%\HeaderTemp.csv
del %outputDir%\Header0.csv
del %outputDir%\Header.csv
del %outputDir%\Detail.csv
del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_2_5Y.csv

echo %_Date% %time% CSV File generated.
echo.



echo %_Date% %time%  Generate CSV File For  DRE Cost Consolidated Report ...

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header0_without_Indeterminate_2_5Y.sql -b > %outputDir%\HeaderTemp0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_Header1_2_5Y.sql -b > %outputDir%\HeaderTemp.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\HeaderTemp.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\HeaderTemp.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\DREReport\DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.sql -b > %outputDir%\Detail.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\Detail.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv ( del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )


rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >> %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv

rem echo append detail data to report file: %time%
type %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
type %outputDir%\Detail.csv >> %outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )
echo.>>%outputDir%\%DEP_Rpt%rpt_dre_Outcomes_2_4_Dashboard.csv

del %outputDir%\HeaderTemp0.csv
del %outputDir%\HeaderTemp.csv
del %outputDir%\Header0.csv
del %outputDir%\Header.csv
del %outputDir%\Detail.csv
del %outputDir%\%DEP_Rpt%DRE_Cost_Consolidated_Data_without_Indeterminate_2_5Y.csv

echo %_Date% %time% CSV File generated.
echo.

GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End