@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

:StepSampleData
echo %_Date% %time% Pipeline Metric Report for TDS
for /F "tokens=*" %%A in ('type "%mypath%\%PMMList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_PipelineMetric.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in TDSReporting.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS Match Flag Condition
for /F "tokens=*" %%A in ('type "%mypath%\%PMMList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_Matching_Flag_condition.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in TDSReporting.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

:StepGenerateCSV
echo %_Date% %time%  Generate CSV File for Pipeline Metric Report for TDS...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_PipelineMetricHeader0.sql -b > %outputDir%\%PM_Rpt%TDS_PipelineMetricHeader0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%PM_Rpt%TDS_PipelineMetricHeader0.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_PipelineMetricHeader.sql -b > %outputDir%\%PM_Rpt%TDS_PipelineMetricHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%PM_Rpt%TDS_PipelineMetricHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%PM_Rpt%TDS_PipelineMetricHeader0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%PM_Rpt%TDS_PipelineMetricHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDSReport\TDS_PipelineMetricData.sql -b > %outputDir%\%PM_Rpt%TDS_pipelinemetricdata.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%PM_Rpt%TDS_pipelinemetricdata.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv ( del %outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >>%outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv

rem echo append detail data to report file: %time%
type %outputDir%\%PM_Rpt%TDS_pipelinemetricdata.csv >> %outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

echo.>> %outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv
echo *--^> Columns B and C displays pipeline numbers and their percentages as cascade from previous step. Columns D and E too display pipeline numbers. However for columns D and E the percentages are calculated in reference to the patients that matched with SHA (i.e. row. no. 3) >>%outputDir%\%PM_Rpt%rpt_tds_pipelinemetric.csv

del %outputDir%\%PM_Rpt%TDS_PipelineMetricHeader0.csv
del %outputDir%\Header0.csv
del %outputDir%\%PM_Rpt%TDS_PipelineMetricHeader.csv
del %outputDir%\Header.csv
del %outputDir%\%PM_Rpt%TDS_pipelinemetricdata.csv

echo %_Date% %time% CSV File generated.
echo.

echo %_Date% %time%  Generate CSV File for Matching Flag Condition Report for TDS...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_Matching_Flag_conditionHeader0.sql -b > %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader0.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_Matching_Flag_conditionHeader.sql -b > %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDSReport\TDS_Matching_Flag_conditionData.sql -b > %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%PM_Rpt%rpt_tds_matching_flag_condition.csv ( del %outputDir%\%PM_Rpt%rpt_tds_matching_flag_condition.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%PM_Rpt%rpt_tds_matching_flag_condition.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%PM_Rpt%rpt_tds_matching_flag_condition.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >>%outputDir%\%PM_Rpt%rpt_tds_matching_flag_condition.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%PM_Rpt%rpt_tds_matching_flag_condition.csv

rem echo append detail data to report file: %time%
type %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditiondata.csv >> %outputDir%\%PM_Rpt%rpt_tds_matching_flag_condition.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )


del %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader0.csv
del %outputDir%\Header0.csv
del %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditionHeader.csv
del %outputDir%\Header.csv
del %outputDir%\%PM_Rpt%TDS_Matching_Flag_conditiondata.csv

echo %_Date% %time% CSV File generated.
echo.
GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End