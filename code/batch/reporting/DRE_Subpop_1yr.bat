@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

:StepSampleData
REM echo %_Date% %time% DRE 2.5 Evaluation Report execution started
REM for /F "tokens=*" %%A in ('type "%mypath%\%DRE2_5List%"') do (  
REM echo %_Date% !time! %%A is in progress... 
REM %SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_2_5yr_evl_period.sql -v TargetSchema=%TargetSchema% -I -b
REM IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
REM IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
REM ECHO %_Date% !time! %%A completed
REM echo.)

echo %_Date% %time% DRE SubPop 1Year Report execution started
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_SubPop_Cohorts.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% %time% rpt_dre_high_economic_endpoint%%A completed
echo.


:StepGenerateCSV
echo %_Date% %time%  Generate CSV File For  DRE 2.5 Evaluation Report...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_SubPop_CohortsHeader.sql -b > %outputDir%\DRE_SubPop_CohortsHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\DRE_SubPop_CohortsHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\DRE_SubPop_CohortsHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter%  -W -h-1 -i %INPUT%\DREReport\DRE_SubPop_CohortsData.sql -b > %outputDir%\DRE_SubPop_CohortsData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\DRE_SubPop_CohortsData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv ( del %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header.csv %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\d6.5_rpt_dre_subpop_cohorts.csv

rem echo append detail data to report file: %time%
type %outputDir%\DRE_SubPop_CohortsData.csv >> %outputDir%\d6.5_rpt_dre_subpop_cohorts.csv
rem type %outputDir%\DRE_2_5yr_evl_periodData.csv >> abc.txt
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

del %outputDir%\DRE_SubPop_CohortsHeader.csv
del %outputDir%\Header.csv
del %outputDir%\DRE_SubPop_CohortsData.csv

echo %_Date% %time% CSV File generated.
echo.

GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End