@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

:StepSampleData

echo %_Date% %time% Std_Rpt_TDS_EliprioMatchedGroupPatientID SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\Std_Rpt_TDS_EliprioMatchedGroupPatientID.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS Cohort Characterization SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS Cohort Characterization Overlap With Train SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_withTrain.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS Cohort Characterization Non Overlap With Train SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_NonOverlap_withTrain.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS Cohort Characterization Overlap With Test SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_withTest.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS Cohort Characterization Non Overlap With Test SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_NonOverlap_withTest.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS_CohortCharacterization_Eliprio_p05 SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_Eliprio_p05.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS_CohortCharacterization_MatchedCohort_p05 SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_MatchedCohort_p05.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS_CohortCharacterization_Eliprio_MaxF1 SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_Eliprio_MaxF1.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS_CohortCharacterization_MatchedCohort_MaxF1 SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_CohortCharacterization_MatchedCohort_MaxF1.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)

echo %_Date% %time% TDS_Patient_timeline_statistic SQL Query execution in progress
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\TDS_Patient_timeline_statistic.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time!SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)



:StepGenerateCSV
echo %_Date% %time%  Generate CSV File For  TDS Cohort Characterization Report...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterizationHeader0.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader0.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterizationHeader.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDSReport\TDS_CohortCharacterizationData.sql -b > %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterizationdata.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterizationdata.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv ( del %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterizationdata.csv >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

echo.>> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv

echo *--^> Order followed is of PHS >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv
echo All variable calculations are taken from the year leading up to and including the index date (i.e. observation period) (except for Outcome Variable which is for evaluation period)  >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.csv

del %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader0.csv
del %outputDir%\Header0.csv
del %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterizationHeader.csv
del %outputDir%\Header.csv
del %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterizationdata.csv

echo %_Date% %time% CSV File generated.
echo.

echo %_Date% %time%  Generate CSV File For  TDS Cohort Characterization Report for Overlap and non_overlap with Train...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader0.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader0.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_NonOverlap_TrainData.sql -b > %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv ( del %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_Traindata.csv >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

echo.>> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv
echo *--^> Order followed is of PHS >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv
echo All variable calculations are taken from the year leading up to and including the index date (i.e. observation period) (except for Outcome Variable which is for evaluation period)  >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_train.csv

del %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader0.csv
del %outputDir%\Header0.csv
del %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TrainHeader.csv
del %outputDir%\Header.csv
del %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_Traindata.csv

echo %_Date% %time% Overlap Non overlap CSV File generated.
echo.

echo %_Date% %time%  Generate CSV File For  TDS Cohort Characterization Report for Overlap and non_overlap with Test...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader0.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader0.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDSReport\TDS_CohortCharacterization_Overlap_NonOverlap_TestData.sql -b > %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv ( del %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_Testdata.csv >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

echo.>> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv
echo *--^> Order followed is of PHS >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv
echo All variable calculations are taken from the year leading up to and including the index date (i.e. observation period) (except for Outcome Variable which is for evaluation period)  >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_overlap_nonoverlap_test.csv

del %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader0.csv
del %outputDir%\Header0.csv
del %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_TestHeader.csv
del %outputDir%\Header.csv
del %outputDir%\%CC_Rpt%rpt_TDS_CohortCharacterization_Overlap_NonOverlap_Testdata.csv

echo %_Date% %time% Overlap Non overlap Test CSV File generated.
echo.

echo %_Date% %time%  Generate CSV File For TDS_CohortCharacterization_Eliprio_MatchedCohort Report...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterization_Eliprio_MatchedCohortHeader0.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader0.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_CohortCharacterization_Eliprio_MatchedCohortHeader.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDSReport\TDS_CohortCharacterization_Eliprio_MatchedCohortData.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv ( del %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortData.csv >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

echo.>> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv
echo *--^> Order followed is of PHS >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv
echo All variable calculations are taken from the year leading up to and including the index date (i.e. observation period) (except for Outcome Variable which is for evaluation period)  >> %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization_eliprio_matchedcohort.csv


del %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader0.csv
del %outputDir%\Header0.csv
del %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortHeader.csv
del %outputDir%\Header.csv
del %outputDir%\%CC_Rpt%Rpt_TDS_CohortCharacterization_Eliprio_MatchedCohortData.csv
del %outputDir%\Std_Rpt_TDS_EliprioMatchedGroupPatientID.csv

echo %_Date% %time% TDS_CohortCharacterization_Eliprio_MatchedCohort CSV File generated.
echo.

echo %_Date% %time%  Generate CSV File For TDS Timeline Statistics Report...

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_Patient_Timeline_StatisticHeader0.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader0.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader0.csv && GOTO Error )

REM echo create header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter% -W -i %INPUT%\TDSReport\TDS_Patient_Timeline_StatisticHeader.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader.csv -I
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader.csv && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader0.csv -DoutputF=%outputDir%\Header0.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader.csv -DoutputF=%outputDir%\Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )


rem echo create detail file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter% -W -h-1 -i %INPUT%\TDSReport\TDS_patient_timeline_statisticData.sql -b > %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticData.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticData.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%CC_Rpt%rpt_tds_patient_timeline_statistic.csv  ( del %outputDir%\%CC_Rpt%rpt_tds_patient_timeline_statistic.csv  )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo Write clean header data to report file: %time%
copy %outputDir%\Header0.csv %outputDir%\%CC_Rpt%rpt_tds_patient_timeline_statistic.csv 
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_patient_timeline_statistic.csv 

rem echo Write clean header data to report file: %time%
type %outputDir%\Header.csv >>%outputDir%\%CC_Rpt%rpt_tds_patient_timeline_statistic.csv 
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to report.csv file && GOTO Error )

echo.>>%outputDir%\%CC_Rpt%rpt_tds_patient_timeline_statistic.csv 

rem echo append detail data to report file: %time%
type %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticData.csv >> %outputDir%\%CC_Rpt%rpt_tds_patient_timeline_statistic.csv 
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to report.csv file && GOTO Error )

del %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader0.csv
del %outputDir%\Header0.csv
del %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticHeader.csv
del %outputDir%\Header.csv
del %outputDir%\%CC_Rpt%Rpt_TDS_Patient_Timeline_StatisticData.csv

echo %_Date% %time% CSV File generated.
echo.


:DataAnalysisPlan
echo %_Date% %time% Generate TXT file For  TDS Cohort Characterization Report...
for /F "tokens=*" %%A in ('type "%mypath%\%TDSCCList%"') do (
echo %_Date% %time% %%A is in progress...
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\TDSReport\%%A.sql -v TargetSchema=%TargetSchema% -o %outputDir%\%CC_Rpt%rpt_tds_cohortcharacterization.txt -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\TDSReporting.txt && GOTO Error ) 
ECHO %_Date% %time% %%A completed
)

GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End