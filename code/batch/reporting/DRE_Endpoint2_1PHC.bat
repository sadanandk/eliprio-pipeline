@ECHO OFF

IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%LOGIN%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort
setlocal enabledelayedexpansion

SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET mypath=%~dp0

:StepSampleDataEndPoint2_1
echo %_Date% %time% DRE EndPoint 2.1 Table popultion started
for /F "tokens=*" %%A in ('type "%mypath%\%DEP21PHCList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\DRE_Endpoint2_1.sql -v TargetSchema=%TargetSchema% -I -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DRE_Endpoint2_1.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.)

:StepGenerateCSV
echo %_Date% %time%  Generate CSV File For  DRE EndPoint 2.1 Report...

REM echo create first header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_Endpoint2_1Header0.sql -b > %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header0.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%DEP_Rpt%rpt_dre_endpointheader.csv && GOTO Error )

REM echo create first header file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_Endpoint2_1Header.sql -b > %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%DEP_Rpt%rpt_dre_endpointheader.csv && GOTO Error )

REM echo create second header file: %time% 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_Endpoint2_1Header1.sql -b > %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header1.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%DEP_Rpt%rpt_dre_endpointheader1.csv && GOTO Error )

REM echo create third header file: %time% 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD%  -s%delimiter%  -W -i %INPUT%\DREReport\DRE_Endpoint2_1Header2.sql -b > %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header2.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing Header record, please check the log file and %outputDir%\%DEP_Rpt%rpt_dre_endpointheader2.csv && GOTO Error )

Rem echo clean header1 file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header0.csv -DoutputF=%outputDir%\%DEP_Rpt%Header.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header1 file: %time%
java -classpath %utilityDir% -DheaderF=%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header.csv -DoutputF=%outputDir%\%DEP_Rpt%Header1.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header2 file: %time% 
java -classpath %utilityDir% -DheaderF=%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header1.csv -DoutputF=%outputDir%\%DEP_Rpt%Header2.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

Rem echo clean header3 file: %time% 
java -classpath %utilityDir% -DheaderF=%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header2.csv -DoutputF=%outputDir%\%DEP_Rpt%Header3.csv CSVReader
IF %ERRORLEVEL% NEQ 0 ( echo Error cleaning header record using java utility && GOTO Error )

rem echo create detail data file: %time%
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -I -s%delimiter%  -W -h-1 -i %INPUT%\DREReport\DRE_Endpoint2_1Data.sql -b > %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1data.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error writing detail record, please check the log file and %outputDir%\%DEP_Rpt%rpt_dre_endpointdata.csv && GOTO Error )

rem echo delete existing report file: %time%
if exist %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv ( del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv )
IF %ERRORLEVEL% NEQ 0 ( echo File does not exists or its in use && GOTO Error )

rem echo write clean header data to report file: %time%
copy %outputDir%\%DEP_Rpt%Header.csv %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to rpt_dre_endpoint2_1.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv

rem echo write clean header1 data to report file: %time% NEW CODE
type %outputDir%\%DEP_Rpt%Header1.csv >> %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to rpt_dre_endpoint.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv

rem echo write clean header2 data to report file: %time% 
type %outputDir%\%DEP_Rpt%Header2.csv >> %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to rpt_dre_endpoint.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv

rem echo write clean header3 data to report file: %time% 
type %outputDir%\%DEP_Rpt%Header3.csv >> %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying header row to rpt_dre_endpoint.csv file && GOTO Error )

echo.>>%outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv

rem echo append detail data to report file: %time%
type %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1data.csv >> %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv
IF %ERRORLEVEL% NEQ 0 ( echo Error copying detail row to rpt_dre_endpoint2_1.csv file && GOTO Error )

rem echo delete all the three headers and one data file
del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header0.csv
del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header.csv
del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header1.csv
del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1header2.csv
del %outputDir%\%DEP_Rpt%Header.csv
del %outputDir%\%DEP_Rpt%Header1.csv
del %outputDir%\%DEP_Rpt%Header2.csv
del %outputDir%\%DEP_Rpt%Header3.csv
del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1data.csv

COPY %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv %outputDir%\%DEP_Rpt%rpt_dre_endpoint.csv
del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.csv
echo %_Date% %time% DRE EndPoint CSV File generated.
echo.

:DREEndPoint2_1
echo %_Date% %time%  Generate TXT File For  DRE Endpoint2_1 Report...
for /F "tokens=*" %%A in ('type "%mypath%\%DEP211List%"') do (
echo %_Date% %time% %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\DREReport\%%A.sql -v TargetSchema=%TargetSchema% -o %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.txt -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\DREReporting.txt && GOTO Error ) 
ECHO %_Date% %time% %%A completed
)

COPY %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.txt %outputDir%\%DEP_Rpt%rpt_dre_endpoint.txt
del %outputDir%\%DEP_Rpt%rpt_dre_endpoint2_1.txt
GOTO End

:Abort
echo Parameter values not specified. Please check the config file. 
echo Exiting...
Goto End
Rem Exit

:Error

echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End