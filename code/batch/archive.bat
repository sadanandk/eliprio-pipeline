@echo off

mkdir %temp%\e2e_pipeline <NUL
mkdir %temp%\e2e_pipeline\logs <NUL
mkdir %temp%\e2e_pipeline\input <NUL
mkdir %temp%\e2e_pipeline\output <NUL
echo %_DT% >>%OUTPUT%\archive.txt
REM copy results to temp directory
copy %OUTPUT% %temp%\e2e_pipeline\logs >>%OUTPUT%\archive.txt
copy input\* %temp%\e2e_pipeline\input >>%OUTPUT%\archive.txt
REM copy output\* %temp%\e2e_pipeline\output >>%OUTPUT%\archive.txt

( dir /b /a "output" | findstr . ) > nul && (  copy output\* %temp%\e2e_pipeline\output >>%OUTPUT%\archive.txt ) || (  echo output folder empty >>%OUTPUT%\archive.txt)

REM create zip file
7z a -tzip "archive\%executionName%%_DT%.zip" "%temp%/e2e_pipeline/*" >>%OUTPUT%\archive.txt
if %ERRORLEVEL% NEQ 0 ( GOTO ERROR)

REM delete temp data
if exist "%temp%\e2e_pipeline" ( @RD /S /Q "%temp%\e2e_pipeline" ) >>%OUTPUT%\archive.txt

echo Created archive of results into archive folder with name %executionName%%_DT%.zip

:END
exit /b 0

:ERROR
echo Error while creating archive file, please check archive.txt for further logs.
exit /b 1
