@ECHO OFF

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)


echo.
echo %1
echo [0]=%0 [1]=%1 [2]=%2 [3]=%3 [4]=%4 [5]=%5
SET fileName=%1\%outputDir%\%Score_rpt%%modelName%_score.csv
SET modelType=%modelType%
REM SET modelName=%3
SET outputFile=%1\%outputDir%\%KPI_rpt%Rpt_%modelType%_KPI.csv
SET delimeter=%delimeter%
SET metadataFile=%1\code\model\models\%modelName%\metadata.json
SET risk_out=%1\%outputDir%\%RiskSt_rpt%Rpt_%modelType%_risk.csv
SET prevalence=%prevalence%
echo.
echo %_Date% %time% Running R script for %modelType% Model KPI 
echo %_Date% %time% input file  name for %modelType% is %fileName%
echo %_Date% %time% Filename of %modelType% scoring %fileName%
SET lib=%userprofile%\Documents\R\win-library\3.5\
SET Rlib="%Rlib%"
SET lib="%lib%"
SET low_cut=%DRE_Threshold%
SET high_cut=%DRE_highcut%

REM SET confusion_out=%1\%outputDir%\%Conf_rpt%Rpt_%modelType%_conf_matrix.csv

REM print(paste("arg 1 filename",args[1]))
REM print(paste("arg 2 modelType",args[2]))
REM print(paste("arg 3 KPIoutputFile",args[3]))
REM print(paste("arg 4 delimeter",args[4]))
REM print(paste("arg 5 modelmetadatafile",args[5]))
REM print(paste("arg 6 riskfile",args[6]))
REM print(paste("arg 7 prevalence",args[7]))



echo %_Date% %time% Model KPI Results for %modelType%
REM "C:\Program Files\R\R-3.5.0\bin\R.exe" --vanila -e CMD BATCH KPI.R "output.txt" "model"
REM IF "%modelType%" EQU "TDS" (GOTO TDS_Postprocess) ELSE (GOTO DRE_Postprocess)

:DRE_Postprocess

RScript --vanilla --no-save code\R\risk_postprocess_1yr.R  %fileName% %modelType% %outputFile% %delimiter% %metadataFile% %risk_out% %prevalence%

IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR )
GOTO END


REM If any error is encountered 
:ERROR
echo %_Date% %time%  Pipeline: You encountered Error! Check log\model_reports.txt log file for further detail
exit /b 1

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed R KPI bat
IF %ERRORLEVEL%==0 ( echo %_Date% %time% SUCCESSFULLY executed R function )
exit /b 0

REM https://www.rdocumentation.org/packages/utils/versions/3.5.1/topics/Rscript