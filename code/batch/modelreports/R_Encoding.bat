@ECHO OFF

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
REM call code\batch\common\constants.bat
echo.
echo [0]=%0 [1]=%1 [2]=%2 [3]=%3 [4]=%4 [5]=%5
SET modelType=%4
SET modelName=%3
SET fileName=%1\%Report%\
REM generate output to input folder
SET outputFolder=%1\input\
SET outputFileName=%modelType%_feature_en.csv
SET metadataFile=%1\code\model\models\%modelName%\

echo.
echo %_Date% %time% Running R script for %modelType% Feature Preprocess
echo %_Date% %time% input file  name for %modelType% is %fileName%


REM Rscript --vanilla --no-environ -e "rmarkdown::render('code\\R\\Encoding_Data.Rmd',clean=TRUE)" %fileName% %outputFolder% %outputFileName% %delimiter% >>log\%modelType%_encoding.txt 2>&1
RScript --vanilla --no-save code\R\TDS_Preprocess.R  %fileName% %metadataFile% %outputFolder% %outputFileName% %delimiter% >>log\%modelType%_encoding.txt 2>&1
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR )
GOTO END

REM If any error is encountered 
:ERROR
echo %_Date% %time%  Pipeline: You encountered Error! Check %modelType%_encoding.txt output file for further detail
exit /b 1

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed R Feature encoding bat
exit /b 0

REM https://www.rdocumentation.org/packages/utils/versions/3.5.1/topics/Rscript