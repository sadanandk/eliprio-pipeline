IF "%RunDRECohort%"=="Y" (GOTO KPI_DRE) else (GOTO TDS_REPORTS)
:KPI_DRE
echo.
echo %_Date% %time% DRE:: Generate KPI report file
IF not exist %outputDir%\%DREScore_rpt%%DREmodel%_score_2_5yr.csv ( echo %_Date% %time% WARN Message: score file for DRE does not exist! >>log\model_reports.txt && GOTO Error )
Call %BatchDir%\modelreports\RModelKPI.bat %rootDir% %outputDir% %DREmodel% %DRE% %_DT% %delimiter% >>log\model_reports.txt  2>&1
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model:: ERRORLEVEL %ERRORLEVEL% in DRE RModelKPI.bat && GOTO ERROR )
IF %ERRORLEVEL% EQU 0 ( ECHO %_Date% %time% SUCCESSFULLY executed DRE RModelKPI.bat, check output directory )


:Summary_DRE
echo.
echo %_Date% %time% DRE:: Generate Summary report file
IF not exist %Report%\DRE_feature.csv ( echo %_Date% %time% WARN Message: feature file for DRE does not exist! >>log\model_reports.txt && GOTO Error )
Call %BatchDir%\modelreports\RModelSummary.bat %rootDir% %outputDir% %DREmodel% %DRE% %_DT% %Report% %delimiter% >>log\model_reports.txt  2>&1
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model:: ERRORLEVEL %ERRORLEVEL% in DRE RModelSummary.bat && GOTO ERROR )
IF %ERRORLEVEL% EQU 0 ( ECHO %_Date% %time% SUCCESSFULLY executed DRE RModelSummary.bat, check output directory )

:TDS_REPORTS
IF "%RunTDSCohort%"=="Y" (GOTO KPI_TDS) ELSE (GOTO END)

:KPI_TDS
echo.
echo %_Date% %time% TDS:: Generate KPI report file 
IF not exist %outputDir%\%Score_rpt%%TDSmodel%_score.csv ( echo %_Date% %time% WARN Message: score file for TDS does not exist! >>log\model_reports.txt && GOTO Error)
Call %BatchDir%\modelreports\RModelKPI.bat %rootDir% %outputDir% %TDSmodel% %TDS% %_DT% %delimiter% >>log\model_reports.txt  2>&1
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model:: ERRORLEVEL %ERRORLEVEL% in TDS RModelKPI.bat && GOTO ERROR) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS RModelKPI.bat, check output directory  )
echo.


:Summary_TDS
echo.
echo %_Date% %time% TDS:: Generate data Summary report file
IF not exist %Report%\TDS_feature.csv ( echo %_Date% %time% WARN Message: feature file for TDS does not exist! >>log\model_reports.txt && GOTO Error)
Call %BatchDir%\modelreports\RModelSummary.bat %rootDir% %outputDir% %TDSmodel% %TDS% %_DT% %Report% %delimiter% >>log\model_reports.txt  2>&1
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model:: ERRORLEVEL %ERRORLEVEL% in TDS RModelSummary.bat && GOTO ERROR) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS RModelSummary.bat, check output directory  )
echo.



GOTO END

:ERROR
echo Error in generating model reports, check log\model_reports.txt for further details
exit /b 1

REM On successful execution
:End
exit /b 0