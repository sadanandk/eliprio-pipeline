@ECHO OFF

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
REM call code\batch\common\constants.bat
echo.
echo [0]=%0 [1]=%1 [2]=%2 [3]=%3 [4]=%4 [5]=%5
SET modelType=%4
SET modelName=%3
SET outputFile=%1\%outputDir%\%KPI_rpt%Rpt_%modelType%_KPI.csv
echo.
echo %_Date% %time% Running R script for %modelType% Model KPI 
echo %_Date% %time% input file  name for %modelType% is %fileName%
echo %_Date% %time% Filename of %modelType% scoring %fileName%
SET lib=%userprofile%\Documents\R\win-library\3.5\
SET Rlib="%Rlib%"
SET lib="%lib%"
REM SET low_cut=%DRE_Threshold%
REM SET high_cut=%DRE_highcut%
SET risk_out=%1\%outputDir%\%RiskSt_rpt%Rpt_%modelType%_risk.csv
REM SET confusion_out=%1\%outputDir%\%Conf_rpt%Rpt_%modelType%_conf_matrix.csv
SET metadataFile=%1\code\model\models\%modelName%\metadata.json
SET prevalence=%prevalence%


echo %_Date% %time% Model KPI Results for %modelType%
REM "C:\Program Files\R\R-3.5.0\bin\R.exe" --vanila -e CMD BATCH KPI.R "output.txt" "model"
IF "%modelType%" EQU "TDS" (GOTO TDS_Postprocess) ELSE (GOTO DRE_Postprocess)

:DRE_Postprocess

REM SET outputFile1=%1\%outputDir%\%KPI_rpt%Rpt_%modelType%_KPI_1yr.csv

SET kpi=%1\%outputDir%\KPI.csv
SET subpop=%1\%outputDir%\d6.1_rpt_dre_subpop.csv
SET calibration=%1\%outputDir%\d6.1_rpt_dre_calibration.jpg
SET fileNameDRE25=%1\%outputDir%\%DREScore_rpt%%modelName%_score_2_5yr.csv
RScript --vanilla --no-save code\R\DRE_Postprocess.R  %fileNameDRE25% %modelType% %outputFile% %delimiter% %metadataFile% %risk_out% %prevalence% %kpi% %subpop% %calibration%



IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR )
GOTO END

:TDS_Postprocess

REM IF "%Environment%"=="PHC" ( GOTO TDS_PostprocessPHC ) ELSE ( GOTO TDS_PostprocessUCB )

IF "%Environment%" EQU "PHC" (GOTO TDS_PostprocessPHC) ELSE (GOTO TDS_PostprocessUCB)

:TDS_PostprocessPHC
SET fileNameTDS=%1\%outputDir%\%%%modelName%_score.csv
RScript --vanilla --no-save code\R\TDS_Postprocess_PHC.R  %fileNameTDS% %modelType% %outputFile% %delimiter%
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR )
GOTO END

:TDS_PostprocessUCB
SET fileNameTDS=%1\%outputDir%\%Score_rpt%%modelName%_score.csv
RScript --vanilla --no-save code\R\TDS_Postprocess_UCB.R  %fileNameTDS% %modelType% %outputFile% %delimiter% 

IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR )
GOTO END

REM If any error is encountered 
:ERROR
echo %_Date% %time%  Pipeline: You encountered Error! Check log\model_reports.txt log file for further detail
exit /b 1

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed R KPI bat
IF %ERRORLEVEL%==0 ( echo %_Date% %time% SUCCESSFULLY executed R function )
exit /b 0

REM https://www.rdocumentation.org/packages/utils/versions/3.5.1/topics/Rscript