@ECHO OFF

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
REM call code\batch\common\constants.bat
echo.
echo [0]=%0 [1]=%1 [2]=%2 [3]=%3 [4]=%4 [5]=%5
SET modelType=%4
SET modelName=%3
SET fileName=%1\%Report%\%modelType%_feature.csv
SET outputFileName=%1\output\%Summ_rpt%rpt_%modelType%_summary.txt
SET metadataFile=%1\code\model\models\%modelName%\metadata.json

echo.
echo %_Date% %time% Running R script for %modelType% Model Diagnostics 
echo %_Date% %time% input file  name for %modelType% is %fileName%
echo %_Date% %time% output file name %outputFile%

echo %_Date% %time% Model Summary Results for %modelType%
REM "C:\Program Files\R\R-3.5.0\bin\R.exe" --vanila -e CMD BATCH Summary.R "output.txt" "model"
RScript --vanilla --no-save code\R\getDataSummary.R  %fileName% %outputFileName% %delimiter% %metadataFile%
REM Rscript --vanilla --no-environ -e "rmarkdown::render('code\\R\\getDataSummary.Rmd',clean=TRUE)" %fileName% %outputFileName%>>log\%modelType%_summary.txt 2>&1

IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR )
GOTO END

REM If any error is encountered 
:ERROR
echo %_Date% %time%  Pipeline: You encountered Error! Check log\model_reports.txt output file for further detail
exit /b 1

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed R Summary bat
IF %ERRORLEVEL%==0 ( echo %_Date% %time% SUCCESSFULLY executed R function )
exit /b 0

REM https://www.rdocumentation.org/packages/utils/versions/3.5.1/topics/Rscript