
@ECHO OFF


IF "%SERVER%"=="" GOTO Abort
IF "%DB%"=="" GOTO Abort
IF "%Login%"=="" GOTO Abort
IF "%PASSWORD%"=="" GOTO Abort
IF "%TargetSchema%"=="" GOTO Abort


Rem "C:\Program Files\Microsoft SQL Server\110\Tools\Binn"  
Rem "C:\Program Files (x86)\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn"
Rem "C:\Program Files\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn"
REM SET SQLCMD="C:\Program Files (x86)\Microsoft SQL Server\Client SDK\ODBC\130\Tools\Binn\SQLCMD.EXE"
REM SET PATH=F:\VikasS\E\IMS_DRE_End_to_End_Pipeline
rem Set Findstr="C:\Windows\system32\findstr.exe"
SET SERVER=%SERVER%
SET DB=%DB%
SET Login=%Login%
SET PASSWORD=%PASSWORD%
SET TargetSchema=%TargetSchema%
SET SQLCMD=%SQLCMD%
SET INPUT=%ScriptPATH%
SET OUTPUT=%OUTPUT%
SET Report=%Report%
SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET B=N
SET mypath=%~dp0
setlocal enabledelayedexpansion
REm set mylist=%1

REM IF "%SQLCMD%"=="" GOTO Abort
IF "%INPUT%"=="" GOTO Abort
IF "%OUTPUT%"=="" GOTO Abort


:DeleteLogFiles
if exist %OUTPUT%\*.txt ( del %OUTPUT%\*.txt )

:Step1GapElimination
echo.
echo %_Date% %time%  PreProcess execution started
echo.
for /F "tokens=*" %%A in ('type "%mypath%\%PreProcessList%"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\PreProcess\%%A.sql -v TargetSchema=%TargetSchema% Src_Schema=%Src_Schema% -o %OUTPUT%\%%A.txt -I -e -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( ECHO Please check the log file %OUTPUT%\%%A.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)
echo %_Date% %time%  PreProcess execution completed.
echo.
Rem GOTO End

GOTO End

:Abort
echo Enter login and password 
echo Exiting...
Goto End
Rem Exit

:Error
setlocal disabledelayedexpansion
echo You encountered Error! Please check the log file for further details... 
exit /b 1

:End
setlocal disabledelayedexpansion
exit /b 0
Rem echo %_Date% %time% Execution of SQL Batch file completed




