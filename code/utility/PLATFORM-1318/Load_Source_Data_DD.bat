@ECHO OFF

REM SET PASSWORD=admin123
IF "%PASSWORD%"=="" GOTO MissingPasswd

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

if "%nwrootPath%" NEQ "" (
SET SampleDataPath=%nwrootPath%\code\sql\Load_Source_Data\CSV\
echo setting up n/w path to %nwrootPath% >Batchlog.txt
) ELSE (
SET SampleDataPath=%~dp0\code\sql\Load_Source_Data\CSV\
echo setting up SampleDataPath to %SampleDataPath% >Batchlog.txt
)

echo %_Date% %time% Started Loading Process >Batchlog.txt | type Batchlog.txt

:DeleteLogFiles
call %BatchDir%\common\deletefiles.bat %_Log%

REM delete old output files
call %BatchDir%\common\deletefiles.bat %outputDir%

copy Config.txt %OUTPUT%\Config.txt >>Batchlog.txt

setlocal enabledelayedexpansion

:Setup
echo %_Date% %time%  Loading started ....... >>Batchlog.txt
for /F "tokens=*" %%A in ('type "%BatchDir%\Load_Source_Data_DD.txt"') do (  
echo %_Date% !time! %%A is in progress... >>Batchlog.txt
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -W -h-1 -i %SQLDir%\Load_Source_Data\%%A.sql -v SrcSchema=%SrcSchema% -o log\%%A.txt -I -b >>Batchlog.txt
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A ) >>Batchlog.txt
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file log\%%A.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)
GOTO End

:Error

echo %_Date% %time%  Pipeline: You encountered Error! Check log file for further detail.

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto End

:End
echo %_Date% %time% SUCCESSFULLY executed Loading Batch File
cmd /k