-- ------------------------------------------------------------------
-- - AUTHOR    : Vikas S
-- - USED BY   : 
-- - PURPOSE   : Create structure for Data Dictionary Source tables
-- - NOTE      : 

-- -			--				 
-- - Execution : 
-- - Date      : 18-Apr-2019		
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ----------------------------------------------------------------------
Set Nocount ON

--Drop Schema Test
--:Setvar SrcSchema Test
--:SetVar SampleDataPath F:\Vikas\Pipeline\TDS_Pipeline\scripts\SampleDataRun\CSV_for_Tables\

--Create Schema if it does not exists
IF NOT EXISTS (select * from sys.Schemas where name = '$(SrcSchema)')
BEGIN
EXEC sp_executesql N'CREATE Schema $(SrcSchema)'
END

Print 'Created Schema $(SrcSchema)'


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_RX_CLAIMS_STG]') AND type in (N'U'))

BEGIN


CREATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS_STG]
(
	[SERVICE_DATE] [date] NULL,
	[DAYS_SUPPLY] [float] NULL,
	[NDC] [varchar](30) NULL, -- from 11 to 30 size
	[PATIENT_ID] [varchar](30) NULL,-- from 10 30 
	[SERVICE_CHARGE] float NULL,--varchar to float
	[PAYER_TYPE] [varchar](1) NULL,
	[QUAN] float NULL -- numeric 18,2 to float 
)


END

IF  NOT EXISTS (SELECT * FROM SYS.OBJECTS 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_RX_CLAIMS]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS]
(
	[SERVICE_DATE] [date] NULL,
	[AUTHORIZED_REFILLS] [float] NULL,
	[DAW] [varchar](2) NULL,
	[DAYS_SUPPLY] [float] NULL,
	[FILL_NBR] varchar(10) NULL, -- from float to varchar
	[NDC] [varchar](30) NULL, -- from 11 to 30 size
	[PATIENT_ID] [varchar](30) NULL,-- from 10 30 
	[SERVICE_CHARGE] float NULL,--varchar to float
	[PAYER_TYPE] [varchar](1) NULL,
	[PHARMACY_ID] [varchar](30) NULL,
	[PROVIDER_ID] [varchar](30) NULL, -- from 10 to 30
	[QUAN] float NULL -- numeric 18,2 to float 
)

END 

Print 'Created & Populated Table [SRC_RX_CLAIMS]'



IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_DX_CLAIMS_STG]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS_STG]
(
	[SERVICE_DATE] [date] NULL,
	[DIAG1] [varchar](10) NULL,
	[PATIENT_ID] [varchar](30) NULL, -- 10 to 30 
	[PAYER_TYPE] [varchar](10) NULL,
	[PLACE_OF_SERVICE] [varchar](30) NULL,
	[PROC_CD] [varchar](10) NULL, -- 5 to 10 
	[SERVICE_CHARGE] [float] NULL,
	[DIAG_VERS_TYP_ID1] float NULL -- int to float

)

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_DX_CLAIMS]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS]
(
	[SERVICE_DATE] [date] NULL,
	[DIAG1] [varchar](10) NULL,
	[DIAG2] [varchar](10) NULL,
	[DIAG3] [varchar](10) NULL,
	[DIAG4] [varchar](10) NULL,
	[DIAG5] [varchar](10) NULL,
	[DIAG6] [varchar](10) NULL,
	[DIAG7] [varchar](10) NULL,
	[DIAG8] [varchar](10) NULL,
	[PATIENT_ID] [varchar](30) NULL, -- 10 to 30 
	[PAYER_TYPE] [varchar](10) NULL,
	[PLACE_OF_SERVICE] [varchar](30) NULL,
	[PROC_CD] [varchar](10) NULL, -- 5 to 10 
	[PROVIDER_ID] [varchar](30) NULL, -- 10 to 30 
	[SERVICE_CHARGE] [float] NULL,
	[QUAN] [float] NULL,
	[VISIT_ID] [varchar](60) NULL,
	[DIAG_VERS_TYP_ID1] float NULL, -- int to float
	[DIAG_VERS_TYP_ID2] float NULL, -- int to float
	[DIAG_VERS_TYP_ID3] float NULL, -- int to float
	[DIAG_VERS_TYP_ID4] float NULL, -- int to float
	[DIAG_VERS_TYP_ID5] float NULL, -- int to float
	[DIAG_VERS_TYP_ID6] float NULL, -- int to float
	[DIAG_VERS_TYP_ID7] float NULL, -- int to float
	[DIAG_VERS_TYP_ID8] float NULL  -- int to float
)

END
Print 'Created & Populated Table [SRC_DX_CLAIMS]'


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_REF_PATIENT_STG]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_REF_PATIENT_STG]
(
	[PATIENT_ID] [varchar](100) NULL,
	[YOB] [float] NULL,
	[SEX] [varchar](1) NULL,
	[ZIP3] [varchar](3) NULL,
)

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_REF_PATIENT]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_REF_PATIENT]
(
	[PATIENT_ID] [varchar](100) NULL,
	[YOB] [float] NULL,
	[SEX] [varchar](1) NULL,
	[GROUP_IND] [float] NULL,
	[ZIP3] [varchar](3) NULL,
	[REGION] [varchar](1) NULL
)

END

Print 'Created & Populated Table [SRC_REF_PATIENT]'

