:Setvar TargetSchema  Test_DQ_Final
:Setvar SrcSChema Demo_eliprio_pipeline

SET NOCOUNT ON

SELECT 'Data Quality Report for input data'
SELECT 'Summary of record counts and invalid/missing values'
SELECT 'Highlight first 5 invalid/missing values'
SELECT ''
SELECT 'TABLE: SRC_SRC_REF_PATIENT'
SELECT 'RECORD COUNT:', count(*) AS COUNT FROM [$(TargetSchema)].SRC_REF_PATIENT
SELECT 'Duplicate Patient IDs' AS DUPLICATE_PATIENT_ID, count(*) AS COUNT
		FROM [$(TargetSchema)].SRC_REF_PATIENT
		group by patient_id having count(*)>1 


SELECT ''


SELECT 'Duplicate Patient IDs' AS DUPLICATE_PATIENT_ID, patient_id
		FROM [$(TargetSchema)].SRC_REF_PATIENT
		group by patient_id having count(*)>1 

SELECT ''

SELECT 'SEX:' 

select '       ' ,'Count', 'Percentage' 
SELECT 'INVALID' AS INVALID_GENDER
,concat('   ' , sum(case when  [SEX] not in('M','F','U') then 1 else 0 end)) AS INVALID_COUNT
,concat('   ', format(cast((sum(case when  [SEX] not in('M','F','U') then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
FROM [$(TargetSchema)].SRC_REF_PATIENT WHERE SEX IS NOT NULL

SELECT 'MISSING' AS MISSING_GENDER
,concat('   ' , sum(case when  [SEX] in('','NULL') then 1 when [SEX] is NULL then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(case when  [SEX] in('','NULL') then 1 when [SEX] is NULL then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
FROM [$(TargetSchema)].SRC_REF_PATIENT

SELECT ''

select '       ' ,'Patient_ID', 'Sex'
SELECT top(5) 'INVALID' AS INVALID_GENDER, concat('   ' , PATIENT_ID) as PATIENT_ID, concat('  ' , SEX) AS SEX
FROM [$(TargetSchema)].SRC_REF_PATIENT
where  [SEX] not in('M','F','U') AND SEX IS NOT NULL
SELECT top(5) 'MISSING' AS MISSING_GENDER, concat('   ' , PATIENT_ID) as PATIENT_ID, concat('  ' , SEX) AS SEX
FROM [$(TargetSchema)].SRC_REF_PATIENT
where [SEX] in('','NULL') or [SEX] is null

SELECT ''

SELECT 'YOB:' 
select '       ' ,'Count', 'Percentage'
SELECT 'INVALID' AS INVALID_YOB
,concat('   ' , sum(CASE WHEN [YOB] between DATEPART(YYYY,GETDATE())-120 and DATEPART(YYYY,GETDATE()) then 0 else 1 end)) AS INVALID_COUNT
,concat('   ' , format(cast((sum(CASE WHEN [YOB] between DATEPART(YYYY,GETDATE())-120 and DATEPART(YYYY,GETDATE()) then 0 else 1 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCNETAGE
FROM [$(TargetSchema)].SRC_REF_PATIENT
SELECT 'MISSING' AS MISSING_YOB
,concat('   ' , sum(CASE WHEN [YOB] is null then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(CASE WHEN [YOB] is null then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCNETAGE
FROM [$(TargetSchema)].SRC_REF_PATIENT

SELECT ''

select '       ' ,'Patient_ID', 'YOB'
SELECT top(5) 'INVALID' AS INVALID_YOB, concat('  ' , PATIENT_ID) as PATIENT_ID, concat(' ' , YOB) AS YOB
FROM [$(TargetSchema)].SRC_REF_PATIENT
where  [YOB] not between DATEPART(YYYY,GETDATE())-120 and DATEPART(YYYY,GETDATE())
SELECT top(5) 'MISSING' AS MISSING_YOB, concat('  ' , PATIENT_ID) as PATIENT_ID, concat(' ' , YOB) AS YOB
FROM [$(TargetSchema)].SRC_REF_PATIENT
where [YOB] is null

SELECT ''

SELECT 'ZIP3:' 
select '       ' ,'Count', 'Percentage'
SELECT 'INVALID' AS INVALID_ZIP3
,concat('   ' , sum(CASE WHEN LEN(ZIP3) <> 3 then 1 else 0 end)) AS INVALID_COUNT
,concat('   ' , format(cast((sum(CASE WHEN LEN(ZIP3) <> 3 then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCNETAGE
FROM [$(TargetSchema)].SRC_REF_PATIENT

SELECT 'MISSING' AS MISSING_ZIP3
,concat('   ' , sum(CASE WHEN ZIP3 IN ('','NULL') then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(CASE WHEN ZIP3 IN ('','NULL') then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCNETAGE
FROM [$(TargetSchema)].SRC_REF_PATIENT

SELECT ''

select '       ' ,'Patient_ID', 'ZIP3'
SELECT top(5) 'INVALID' AS INVALID_ZIP3, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('   ' , ZIP3) AS ZIP3
FROM [$(TargetSchema)].SRC_REF_PATIENT
where  LEN(ZIP3) <> 3

SELECT top(5) 'MISSING' AS MISSING_ZIP3, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('   ' , ZIP3) AS ZIP3
FROM [$(TargetSchema)].SRC_REF_PATIENT
where ZIP3 IN ('','NULL')

SELECT ''


SELECT 'TABLE: SRC_DX_CLAIMS'
SELECT 'RECORD COUNT:', count(*) AS COUNT FROM [$(TargetSchema)].[SRC_DX_CLAIMS]

SELECT ''

SELECT 'PAYER_TYPE:' 
select '       ' ,'Count', 'Percentage'
SELECT 'INVALID' AS INVALID_PAYER_TYPE
,concat('   ' , sum(case when  PAYER_TYPE not in ('M','C','O','R') then 1 else 0 end)) AS INVALID_COUNT
,concat('   ' , format(cast((sum(case when  PAYER_TYPE not in ('M','C','O','R') then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
FROM [$(TargetSchema)].SRC_DX_CLAIMS WHERE PAYER_TYPE IS NOT NULL

SELECT 'MISSING' AS MISSING_PAYER_TYPE
,concat('   ' , sum(case when  PAYER_TYPE in('','NULL') then 1 when PAYER_TYPE is NULL then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(case when  PAYER_TYPE in('','NULL') then 1 when PAYER_TYPE is NULL then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
FROM [$(TargetSchema)].SRC_DX_CLAIMS

SELECT ''

select '       ' ,'Patient_ID', 'PAYER TYPE'
SELECT top(5) 'INVALID' AS INVALID_PAYER_TYPE, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('     ' , PAYER_TYPE) AS PAYER_TYPE
FROM [$(TargetSchema)].SRC_DX_CLAIMS
where  PAYER_TYPE not in ('M','C','O','R') AND PAYER_TYPE IS NOT NULL

SELECT top(5) 'MISSING' AS MISSING_PAYER_TYPE, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('     ' , PAYER_TYPE) AS PAYER_TYPE
FROM [$(TargetSchema)].SRC_DX_CLAIMS
where PAYER_TYPE in('','NULL') or PAYER_TYPE is null


SELECT ''

SELECT 'SERVICE_DATE:' 
SELECT 'MIN: ',MIN(service_date) AS SERVICE_DATE,'MAX: ',MAX(service_date) AS SERVICE_DATE FROM [$(TargetSchema)].[SRC_DX_CLAIMS]

select ''

select '       ' ,'Count', 'Percentage'
SELECT 'INVALID' AS INVALID_SERVICE_DATE,
		 concat('   ' , SUM(CASE 
		 WHEN CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),1,4)) NOT BETWEEN 0000 AND 9999
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),5,1) <> '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),6,2)) NOT BETWEEN 1 AND 12
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),8,1) <>  '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),9,2)) NOT BETWEEN 1 AND 31 
		 THEN 1 ELSE 0 END )) AS INVALID_COUNT,
		concat('   ' , format(cast(SUM(CASE 
		 WHEN CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),1,4)) NOT BETWEEN 0000 AND 9999
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),5,1) <> '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),6,2)) NOT BETWEEN 1 AND 12
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),8,1) <>  '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),9,2)) NOT BETWEEN 1 AND 31
		 THEN 1 ELSE 0 end)*100 / COUNT(*) as DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
FROM [$(TargetSchema)].[SRC_DX_CLAIMS]
where service_date is not null

SELECT 'MISSING' AS MISSING_SERVICE_DATE
,concat('   ' , sum(case when service_date is NULL then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(case when service_date is NULL then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
FROM [$(TargetSchema)].[SRC_DX_CLAIMS]

-- ADD TOP 5 INVALID SERVICE DATE
SELECT ''
select '       ' ,'Patient_ID', 'SERVICE DATE'
SELECT TOP(5) 'INVALID' AS INVALID_SERVICE_DATE, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , SERVICE_DATE) AS SERVICE_DATE
FROM [$(TargetSchema)].[SRC_DX_CLAIMS]
WHERE CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),1,4)) NOT BETWEEN 0000 AND 9999
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),5,1) <> '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),6,2)) NOT BETWEEN 1 AND 12
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),8,1) <>  '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),9,2)) NOT BETWEEN 1 AND 31 and service_date is not NULL

SELECT top(5) 'MISSING' AS MISSING_SERVICE_DATE,concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , SERVICE_DATE) AS SERVICE_DATE
FROM [$(TargetSchema)].[SRC_DX_CLAIMS]
where service_date is null
    
  SELECT ''

  
  SELECT 'DIAG:' ;
  select '       ' ,'Count', 'Percentage';
  WITH TEST AS
  (
  SELECT
  DIAG1,
  COUNT(*) AS COUNTS
  FROM [$(TargetSchema)].[SRC_DX_CLAIMS] c WITH(NOLOCK)
  WHERE NOT EXISTS(SELECT DIAG1 FROM [$(SrcSchema)].std_REF_ICD_Code_All a WHERE REPLACE(a.ICD_CODE,'.','') = REPLACE(C.DIAG1,'.',''))
  AND DIAG1 IS NOT NULL
  GROUP BY DIAG1
  ),
  TEST2 AS
  (
  SELECT 
  COUNT(*) AS TOTAL_COUNT
  FROM [$(TargetSchema)].[SRC_DX_CLAIMS] c WITH(NOLOCK)
  )
     SELECT 
  DISTINCT
  'INVALID' AS INVALID_DIAG_CODE,
   concat('    ' , TEMP.COUNTS) AS INVALID_COUNT,
   concat('   ' ,format(cast((TEMP.COUNTS*100/TEMP2.TOTAL_COUNT) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
   FROM [$(TargetSchema)].[SRC_DX_CLAIMS] DX
   LEFT OUTER JOIN TEST2 TEMP2 
   ON 1=1
   LEFT JOIN TEST TEMP
   ON 1=1
   GROUP BY TEMP.COUNTS, TEMP2.TOTAL_COUNT

  SELECT 'MISSING' AS MISSING_DIAG_CODE
,concat('   ' , sum(case when DIAG1 is NULL then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(case when ((DIAG1 is NULL) AND (PROC_CD IS NULL)) then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
FROM [$(TargetSchema)].[SRC_DX_CLAIMS]

  SELECT '' 

  select '       ' ,'Patient_ID', 'DAIG1'
  SELECT 
  DISTINCT
  TOP 5 'INVALID' AS INVALID_DIAG_CODE,
   concat('  ' , DX.PATIENT_ID) as PATIENT_ID, concat('    ' , DX.DIAG1) AS DIAG1
   FROM [$(TargetSchema)].[SRC_DX_CLAIMS] DX
   WHERE NOT EXISTS(SELECT DIAG1 FROM [$(SrcSchema)].std_REF_ICD_Code_All a WHERE REPLACE(a.ICD_CODE,'.','') = REPLACE(DX.DIAG1,'.','')) AND DIAG1 IS NOT NULL
   
  SELECT top(5) 'MISSING' AS MISSING_DIAG1, concat('  ' , DX.PATIENT_ID) as PATIENT_ID, concat('    ' , DX.DIAG1) AS DIAG1
FROM [$(TargetSchema)].[SRC_DX_CLAIMS] DX
where DIAG1 is null AND PROC_CD IS NULL
  
  SELECT '';

  SELECT 'PROC_CD:' ;
  select '       ' ,'Count', 'Percentage';
  WITH TEST AS
  (
  SELECT
  COUNT(*) AS COUNTS
  FROM [$(TargetSchema)].[SRC_DX_CLAIMS] c WITH(NOLOCK)
  WHERE NOT EXISTS(SELECT DIAG1 FROM [$(SrcSchema)].std_REF_CPT_Code_All a WHERE REPLACE(a.CPT_CODE,'.','') = REPLACE(C.PROC_CD,'.',''))
  AND PROC_CD IS NOT NULL
  ),
  TEST2 AS
  (
  SELECT 
  COUNT(*) AS TOTAL_COUNT
  FROM [$(TargetSchema)].[SRC_DX_CLAIMS] c WITH(NOLOCK)
  )
   
  SELECT 
  DISTINCT
  'INVALID' AS INVALID_PROC_CODE,
   concat('   ' , TEMP.COUNTS) AS INVALID_COUNT,
   concat('   ' , format(cast((TEMP.COUNTS*100/TEMP2.TOTAL_COUNT) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
   FROM [$(TargetSchema)].[SRC_DX_CLAIMS] DX
   LEFT OUTER JOIN TEST2 TEMP2 
   ON 1=1
   LEFT JOIN TEST TEMP
   ON 1=1
   GROUP BY TEMP.COUNTS, TEMP2.TOTAL_COUNT
  
   SELECT 'MISSING' AS MISSING_PROC_CODE
,concat('   ' , sum(case when PROC_CD is NULL then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(case when PROC_CD is NULL then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
FROM [$(TargetSchema)].[SRC_DX_CLAIMS]
  
  SELECT ''

   select '       ' ,'Patient_ID', 'PROC CD'
  SELECT 
  DISTINCT
  TOP 5 'INVALID' AS INVALID_PROC_CD,
   concat('  ' , DX.PATIENT_ID) as PATIENT_ID, concat('   ' , DX.PROC_CD) AS PROC_CD
   FROM [$(TargetSchema)].[SRC_DX_CLAIMS] DX
   WHERE NOT EXISTS(SELECT PROC_CD FROM [$(SrcSchema)].std_REF_CPT_Code_All a WHERE REPLACE(a.CPT_CODE,'.','') = REPLACE(DX.PROC_CD,'.','')) and proc_cd not in ('','NULL')

  SELECT top(5) 'MISSING' AS MISSING_PROC_CD,concat('  ' , DX.PATIENT_ID) as PATIENT_ID, concat('   ' , DX.PROC_CD) AS PROC_CD
FROM [$(TargetSchema)].[SRC_DX_CLAIMS] DX
where PROC_CD IS NULL

  SELECT ''

  SELECT 'TABLE: SRC_SRC_RX_CLAIMS'
  SELECT 'RECORD COUNT:', COUNT(*) AS COUNT FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  
  SELECT ''

 SELECT 'SERVICE_DATE:' 
SELECT 'MIN: ',MIN(service_date) AS SERVICE_DATE,'MAX: ',MAX(service_date) AS SERVICE_DATE FROM [$(TargetSchema)].[SRC_RX_CLAIMS]

select ''

select '       ' ,'Count', 'Percentage'
SELECT 'INVALID' AS INVALID_SERVICE_DATE,
		 concat('   ' ,SUM(CASE 
		 WHEN CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),1,4)) NOT BETWEEN 0000 AND 9999
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),5,1) <> '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),6,2)) NOT BETWEEN 1 AND 12
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),8,1) <>  '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),9,2)) NOT BETWEEN 1 AND 31 
		 THEN 1 ELSE 0 END )) AS INVALID_COUNT,
		 concat('   ' , format(cast(SUM(CASE 
		 WHEN CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),1,4)) NOT BETWEEN 0000 AND 9999
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),5,1) <> '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),6,2)) NOT BETWEEN 1 AND 12
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),8,1) <>  '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),9,2)) NOT BETWEEN 1 AND 31
		 THEN 1 ELSE 0 end)*100 / COUNT(*) as DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
FROM [$(TargetSchema)].[SRC_RX_CLAIMS]

SELECT 'MISSING' AS MISSING_SERVICE_DATE
,concat('   ' , sum(case when service_date is NULL then 1 else 0 end)) AS MISSING_COUNT
,concat('   ' , format(cast((sum(case when service_date is NULL then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
FROM [$(TargetSchema)].[SRC_RX_CLAIMS]

-- ADD TOP 5 INVALID SERVICE DATE
SELECT ''

 select '       ' ,'Patient_ID', 'SERVICE DATE'
SELECT TOP(5) 'INVALID' AS INVALID_SERVICE_DATE, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , SERVICE_DATE) AS SERVICE_DATE
FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
WHERE CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),1,4)) NOT BETWEEN 0000 AND 9999
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),5,1) <> '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),6,2)) NOT BETWEEN 1 AND 12
		 OR SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),8,1) <>  '-'
		 OR CONVERT(INT, SUBSTRING(CONVERT (VARCHAR(20),SERVICE_DATE),9,2)) NOT BETWEEN 1 AND 31

SELECT top(5) 'MISSING' AS MISSING_SERVICE_DATE, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , SERVICE_DATE) AS SERVICE_DATE
FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
where service_date is null
    
  SELECT ''

  SELECT 'NDC:' ;
  select '       ' ,'Count', 'Percentage';
  WITH TEST AS
  (
  SELECT
  COUNT(*) AS COUNTS
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS] c WITH(NOLOCK)
  WHERE NOT EXISTS(SELECT NDC FROM [$(SrcSchema)].SRC_REF_PRODUCT a WHERE a.ndc=c.ndc)
  AND NDC IS NOT NULL
  ),
  TEST2 AS
  (
  SELECT 
  COUNT(*) AS TOTAL_COUNT
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS] c WITH(NOLOCK)
  )
  SELECT 
  DISTINCT
  'INVALID' AS INVALID_NDC,
   concat(' ' , TEMP.COUNTS) AS INVALID_COUNT,
   concat('    ' , format(cast((TEMP.COUNTS*100/TEMP2.TOTAL_COUNT) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
   FROM [$(TargetSchema)].[SRC_RX_CLAIMS] RX
   LEFT OUTER JOIN TEST2 TEMP2 
   ON 1=1
   LEFT JOIN TEST TEMP
   ON 1=1
   WHERE RX.NDC IS NOT NULL
   GROUP BY TEMP.COUNTS, TEMP2.TOTAL_COUNT
  
  SELECT 'MISSING' AS MISSING_NDC ,
  concat('   ' , sum(case when ndc is null or ndc in ('','NULL') then 1 else 0 end )) AS MISSING_COUNT
  ,concat('   ' , format(cast((sum(case when ndc is null or ndc in ('','NULL') then 1 else 0 end )*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  
   SELECT ''

  SELECT '       ' ,'Patient_ID', 'NDC'
  SELECT 
  DISTINCT
  TOP 5 'INVALID' AS INVALID_NDC,
   concat('  ' , PATIENT_ID) as PATIENT_ID, concat('   ' , NDC) AS PROCNDC_CD
   FROM [$(TargetSchema)].[SRC_RX_CLAIMS] C
   WHERE NOT EXISTS(SELECT NDC FROM [$(SrcSchema)].SRC_REF_PRODUCT a WHERE A.NDC = C.NDC and NDC not in ('','NULL'))

   SELECT top(5) 'MISSING' AS MISSING_NDC, concat('   ' , PATIENT_ID) as PATIENT_ID, concat('   ' , NDC) AS NDC
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  where NDC is null or NDC in ('','NULL')

  SELECT ''

  SELECT 'DAYS_SUPPLY:' 
  select '       ' ,'Count', 'Percentage'
  SELECT 'INVALID' AS INVALID_DAYS_SUPPLY
  ,concat('   ' , sum(case when [DAYS_SUPPLY] < 0 then 1 else 0 end)) AS INVALID_COUNT
  ,concat('   ' , format(cast((sum(case when [DAYS_SUPPLY] < 0 then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS INVALID_COUNT_PERCENTAGE
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  
  SELECT 'MISSING' AS MISSING_DAYS_SUPPLY
  ,concat('   ' , sum(case when [DAYS_SUPPLY] is NULL then 1 else 0 end)) AS MISSING_COUNT
  ,concat('   ' , format(cast((sum(case when [DAYS_SUPPLY] is NULL then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]

  SELECT ''

   SELECT '       ' ,'Patient_ID', 'DAYS SUPPLY'
  SELECT top(5) 'INVALID' AS INVALID_DAYS_SUPPLY,concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , DAYS_SUPPLY) AS DAYS_SUPPLY
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  where [DAYS_SUPPLY] < 0

  SELECT top(5) 'MISSING' AS MISSING_DAYS_SUPPLY, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , DAYS_SUPPLY) AS DAYS_SUPPLY
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  where [DAYS_SUPPLY] is null

  SELECT ''

  SELECT 'QUAN:' 
  select '       ' ,'Count', 'Percentage'
  SELECT 'INVALID' AS INVALID_QUAN,
  concat('   ' , convert(varchar(15), sum(case when [QUAN] < 0 then 1 else 0 end))) AS INVALID_COUNT
  ,concat('   ' , convert(varchar(15), format(cast((sum(case when [QUAN] < 0 then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00'))) AS INVALID_COUNT_PERCENTAGE
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]

  SELECT 'MISSING' AS MISSING_QUAN,
  concat('   ' , sum(case when [QUAN] is NULL then 1 else 0 end)) AS MISSING_COUNT
  ,concat('   ' , format(cast((sum(case when [QUAN] is NULL then 1 else 0 end)*100 / COUNT(*) ) AS DECIMAL(5,2)),'00.00')) AS MISSING_COUNT_PERCENTAGE
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  
  SELECT ''

   SELECT '       ' ,'Patient_ID', 'QUAN'
 SELECT top(5) 'INVALID' AS INVALID_QUAN, concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , QUAN) AS QUAN
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  where [QUAN] < 0

  SELECT top(5) 'MISSING' AS MISSING_QUAN,concat('  ' , PATIENT_ID) as PATIENT_ID, concat('    ' , QUAN) AS QUAN
  FROM [$(TargetSchema)].[SRC_RX_CLAIMS]
  where [QUAN] is null
 



