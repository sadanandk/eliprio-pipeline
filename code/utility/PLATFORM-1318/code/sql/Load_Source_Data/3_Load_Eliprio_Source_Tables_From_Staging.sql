-- ------------------------------------------------------------------
-- - AUTHOR    : Vikas S
-- - USED BY   : 
-- - PURPOSE   : Load data into Eliprio formatted table using staging data
-- - NOTE      : 

-- -			--				 
-- - Execution : 
-- - Date      : 18-Apr-2019		
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ----------------------------------------------------------------------
Set Nocount ON

--Drop Schema Test
--:Setvar SrcSchema Test
--:SetVar SampleDataPath SampleDataRun\CSV_for_Tables\

TRUNCATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS]

INSERT INTO [$(SrcSchema)].[SRC_RX_CLAIMS] 
(SERVICE_DATE,DAYS_SUPPLY,NDC,PATIENT_ID,SERVICE_CHARGE,PAYER_TYPE,QUAN)
SELECT SERVICE_DATE,DAYS_SUPPLY,NDC,PATIENT_ID,SERVICE_CHARGE,PAYER_TYPE,QUAN 
FROM [$(SrcSchema)].[SRC_RX_CLAIMS_STG]

Print 'Created & Populated Table [SRC_RX_CLAIMS]'




TRUNCATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS]

INSERT INTO [$(SrcSchema)].[SRC_DX_CLAIMS] 
(SERVICE_DATE,DIAG1,PATIENT_ID,PAYER_TYPE,PLACE_OF_SERVICE,PROC_CD,SERVICE_CHARGE,DIAG_VERS_TYP_ID1)
SELECT SERVICE_DATE,DIAG1,PATIENT_ID,PAYER_TYPE,PLACE_OF_SERVICE,PROC_CD,SERVICE_CHARGE,DIAG_VERS_TYP_ID1 
From [$(SrcSchema)].[SRC_DX_CLAIMS_STG]

Print 'Created & Populated Table [SRC_DX_CLAIMS]'


Truncate Table [$(SrcSchema)].[SRC_REF_PATIENT]

Insert into [$(SrcSchema)].[SRC_REF_PATIENT] ([PATIENT_ID],[YOB],[SEX],[ZIP3])
Select [PATIENT_ID],[YOB],[SEX],[ZIP3] From [$(SrcSchema)].[SRC_REF_PATIENT_STG]

Print 'Created & Populated Table [SRC_REF_PATIENT]'
