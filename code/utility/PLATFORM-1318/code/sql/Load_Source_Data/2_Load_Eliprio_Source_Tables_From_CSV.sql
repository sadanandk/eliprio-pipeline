-- ------------------------------------------------------------------
-- - AUTHOR    : Vikas S
-- - USED BY   : 
-- - PURPOSE   : Load data from CSV in staging tables 
-- - NOTE      : 

-- -			--				 
-- - Execution : 
-- - Date      : 18-Apr-2019		
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ----------------------------------------------------------------------
Set Nocount ON

--Drop Schema Test
--:Setvar SrcSchema Test
--:SetVar SampleDataPath SampleDataRun\CSV_for_Tables\

--Create Schema if it does not exists



TRUNCATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS_STG]

BULK INSERT [$(SrcSchema)].[SRC_RX_CLAIMS_STG]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_rx_claims.csv'
'$(SampleDataPath)\src_sample_rx_claims.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
); 

Print 'Populated Table [SRC_RX_CLAIMS_STG]'



TRUNCATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS_STG]

BULK INSERT [$(SrcSchema)].[SRC_DX_CLAIMS_STG]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_dx_claims.csv'
'$(SampleDataPath)\src_sample_dx_claims.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
);

Print 'Created & Populated Table [SRC_DX_CLAIMS_STG]'


TRUNCATE TABLE [$(SrcSchema)].[SRC_REF_PATIENT_STG]

BULK INSERT [$(SrcSchema)].[SRC_REF_PATIENT_STG]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_ref_patient.csv'
'$(SampleDataPath)\src_sample_ref_patient.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
);

Print 'Created & Populated Table [SRC_REF_PATIENT_STG]'

