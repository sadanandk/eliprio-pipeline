REM Read config files for required values  
IF not exist Config.txt ( echo %_Date% %time% Error Message: Config.txt file does not exist! && GOTO Error )
FOR /F "tokens=1,2 delims==" %%G IN (Config.txt) DO (set %%G=%%H)

SET TargetSchema=%TargetSchema%
exit /b 0