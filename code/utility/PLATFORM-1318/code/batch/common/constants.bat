
REM Set Date_=%date:~10,4%%date:~4,2%%date:~7,2%

REM Set Date_time_stamp for the log folder
SET _Date=%date:~10,4%%date:~4,2%%date:~7,2%
SET _Time=%time:~0,2%%time:~3,2%

REM Set the log path
SET SQLCMD=SQLCMD
SET ScriptPath=code\sql
SET _Log=log
SET _DT=%_Date%%_Time: =0%
SET Output=%_Log%
SET INPUT=%ScriptPath%
SET Report=input
SET PASSWORD=admin123
REM SET PreProcessList=PreProcess.txt
REM SET SampleDataList=SampleData.txt
REM SET DREList=DRE.txt
REM SET TDSList=TDS.txt
SET BatchDir=code\batch
REM SET ModelDir=code\model
SET SQLDir=code\sql
SET outputDir=output
REM SET utilityDir=code\utility 
REM REM REM echo %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %Step% %SQLCMD% %ScriptPATH% %OUTPUT%
REM REM SET SmokeTestList=SmokeTest.txt
REM REM SET DREmodel=DRE_SHA
REM REM SET TDSmodel=TDS_AED_SHA
REM REM SET TDS=TDS
REM REM SET DRE=DRE
REM REM SET BulkInsertScript=DML_Script
REM REM Rem linux path
REM REM SET Rlib=C:/Program Files/R/R-3.5.1/library
REM REM Rem windows path
REM REM SET RScript=C:\Program Files\R\R-3.5.1\bin
SET 7Zip=C:\Program Files\7-Zip\
REM REM SET delimiter=","
REM Rem Cohort Characterization for DRE and TDS
REM SET CC_Rpt=d6.4_
REM SET TDSCList=TDS_CohortCharacterization.txt
REM SET TDSCCList=TDS_CohortCharacterizationReport.txt
REM SET DRECCList=DRE_CohortCharacterization.txt
REM SET DRECCCList=DRE_CohortCharacterizationReport.txt
REM Rem Pipeline Metric for DRE and TDS
REM SET PM_Rpt=d6.3_
REM SET PMMList=PipelineMetric.txt

REM REM DRE model threshold .. do not change this without notifying to concern 
REM SET DRE_Threshold=0.0949
REM SET DRE_highcut=0.2975
REM REM delimiter to save files
REM SET delimiter=","
REM SET Score_rpt=D5_
REM SET KPI_rpt=D6.1_
REM SET DIAG_rpt=D6.2_
REM SET Summ_rpt=D6.2_
REM SET RiskSt_rpt=D6.5_
REM SET Conf_rpt=D6.1_

REM Existing batch after read 
exit /b 0