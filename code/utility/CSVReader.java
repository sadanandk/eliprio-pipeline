import java.io.*;

public class CSVReader {

    public static final String delimiter = ",";

    public static void read(String csvFile, String outFile) {
        try {
            File file = new File(csvFile);
            File oF = new File(outFile);
            FileReader fr = new FileReader(file);
            FileWriter oFR = new FileWriter(oF);
            BufferedReader br = new BufferedReader(fr);
            BufferedWriter bw = new BufferedWriter(oFR);

            String line = "";
            line = br.readLine();
            bw.write(line);
            br.close();
            bw.close();
            fr.close();
            oFR.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
    public static void main(String[] args) {
        // csv file to read
		String headerFile = System.getProperty("headerF");
		String outputFile = System.getProperty("outputF");
		
        String csvFile = headerFile;
        String outFile = outputFile;
        CSVReader.read(csvFile,outFile);
    }

}