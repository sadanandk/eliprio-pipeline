@ECHO OFF
REM CLS
REM java -Dmodel=DRESolution_top30 -jar libs/ModelPipeline.jar
REM Set ReportCSV="F:\VikasS\E\IMS_DRE_End_to_End_Pipeline\report\Report.csv"
REM input -Dinput=file name
REM logfolder options -DlogF=folder name
REM delimeter -Ddem="|"
REM output file name =Doutput=file name
REM -Xmx2084m
REM read first parameter as modelType
SET modelType=%1
REM read second parameter as modelFileName
SET modelName=%2
SET outputFile=%Score_rpt%%modelName%_score.csv
IF not exist %ModelDir%\libs\h2o-genmodel.jar ( 
echo %_Date% %time% Error Message: h2o-genmodel lib does not exist! >>%_Log%\model_error.txt
echo Error occurred: Please check model_error file for more details.
GOTO ERROR
) 
IF not exist %ModelDir%\libs\ModelScoringPipeline.jar ( 
echo %_Date% %time% Error Message: ModelScoringPipeline lib does not exist! >>%_Log%\model_error.txt
echo Error occurred: Please check model_error file for more details.
GOTO ERROR
)
java -Xms2g -Xmx8g -Dmodel="code/model/models/%modelName%" -Dinput=%Report%/"%featureFilePre%".csv -Ddem=%delimiter% -DlogF="" -Dloglevel="SEVERE" -DoutputDir=%outputDir% -DcurrentTime="%_DT%" -Dthreshold=%Threshold% -DmodelType=%modelType% -DoutputFile=%outputFile% -jar code/model/libs/ModelScoringPipeline.jar 
IF %ERRORLEVEL% NEQ 0 ( ECHO ERROR occured,check logs files && GOTO ERROR ) ELSE ( GOTO END)

Rem cmd /k

:ERROR
echo ERROR occured while scoring  %modelName% model
exit /b 1

:END
exit /b 0