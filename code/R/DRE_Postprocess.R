#!/usr/bin/env Rscript

#DRE_Postprocess.R
# Script R file to carry out post processing on VOTES DRE model score file for 2.5 year and create
# primary and secondary endpoints
# This script does not perform risk stratification as well. risk labels are carried from scores from 1 year

#----------------------------------------------------
#Input: CSV filename (It should be in character with suffix as .csv).
#Output : KPI metrics to evaluate binary classification model performance.

#Note: 1. Request to remove pipe delimiter "|" placed after the last column name
#      2. Column names should not change 
#      3. Upper/lower case should be maintained as below: AED: Success/Failure DRE: DRE/Non_DRE	

#------------------------------------------------------


#--- READ DATA FROM COMMAND LINES -------


#Suppress warnings
options(warn = 0)


args = commandArgs(trailingOnly = TRUE)

# check if there is at least one argument: if not, return an error
if (length(args)==0) {
  stop("At least one argument must be supplied (input file).n", call.=FALSE)
} 

print(paste("arg 1 filename",args[1]))
print(paste("arg 2 modelType",args[2]))
print(paste("arg 3 KPIoutputFile",args[3]))
print(paste("arg 4 delimeter",args[4]))
print(paste("arg 5 modelmetadatafile",args[5]))
print(paste("arg 6 riskfile",args[6]))
print(paste("arg 7 prevalence",args[7]))

# following arguments are static tables and their csv to load
print(paste("arg 8 kpitraintest",args[8]))
print(paste("args 9 writesubpopfile",args[9]))
print(paste("args 10 calibrationplot",args[10]))

#--- Set up environment ----

#Load packages

suppressMessages(library(data.table,  lib.loc=.libPaths()))
suppressMessages(library(Metrics,	    lib.loc=.libPaths()))
suppressMessages(library(DescTools,   lib.loc=.libPaths()))
suppressMessages(library(base,        lib.loc=.libPaths()))
suppressMessages(library(caret,       lib.loc=.libPaths()))		
suppressMessages(library(ggplot2,     lib.loc=.libPaths()))	
suppressMessages(library(R.utils,     lib.loc=.libPaths()))
suppressMessages(library(jsonlite,    lib.loc=.libPaths()))
suppressMessages(library(epiR,        lib.loc=.libPaths()))
suppressMessages(library(pROC,        lib.loc=.libPaths()))



#load all utility function
R.utils::sourceDirectory("code/R/utilities")


#-- READ DATA ---
filename <- args[1]
solution <- args[2]
modelmetadata <- jsonlite::read_json(args[5])
modeltype <- args[2]
prevalence <- args[7]

# extra csv to read
filenamekpi  <- args[8]            
filenamesubpop <- args[9]          
filenameplot <- args[10]            

output.dir <- "output"


file.names <- c("d5_dre_sha_score_1_0yr_Train.csv","d6_5_d5_dre_sha_score_2_5yr_Train.csv",
                "d5_dre_sha_score_1_0yr_Test.csv","d6_5_d5_dre_sha_score_2_5yr_Test.csv",
                "d5_dre_sha_score.csv","d6.5_rpt_dre_subpop_cohorts.csv","d6.5_rpt_dre_subpop_cohorts_2_5Y.csv")

scoredadatsets <- lapply(file.names, function(z) {
  data.table::fread(input = file.path(output.dir,z))
})


# check if file exist
if(length(filename) !=1 | length(solution) !=1)
{
  MSG <- sprintf("Kindly pass 1 value to each parameter")
  stop(MSG)
}


if(!is.character(filename))                                                     #TO CHECK IF FILENAME IS OF CHARACTER TYPE
{
  MSG <- sprintf("Kindly pass the file name as character")
  stop(MSG)
}

# Loading file
if(   file.exists(filename)
      &&
      file.exists(filenamekpi)
      
)                                                      
{
  
  data <- fread(input = filename,colClasses=list(character=c(tolower("patient_id"))),sep = ",")
  sprintf("Scored 2.5 yr data has %d rows and %d columns",dim(data)[1],dim(data)[2])
  
  
  scoredata1yr <- scoredadatsets[[5]]
  sprintf("Scored 1 yr data has %d rows and %d columns",dim(scoredata1yr)[1],dim(scoredata1yr)[2])
  
  # load remaining static files
  
  traindata1yr <- scoredadatsets[[1]]
  sprintf("Train Score 1 yr data has %d rows and %d columns",dim(traindata1yr)[1],dim(traindata1yr)[2])
  
  traindata25yr <- scoredadatsets[[2]]
  sprintf("Train Score 2.5 yr data has %d rows and %d columns",dim(traindata25yr)[1],dim(traindata25yr)[2])
  
  testdata1yr <- scoredadatsets[[3]]
  sprintf("Test Score 1 yr data has %d rows and %d columns",dim(testdata1yr)[1],dim(testdata1yr)[2])
  
  testdata25yr <- scoredadatsets[[4]]
  sprintf("Test Score 2.5 yr data has %d rows and %d columns",dim(testdata25yr)[1],dim(testdata25yr)[2])
  
  kpidata <- fread(input = filenamekpi,sep = "|")
  sprintf("train test kpi data has %d rows and %d columns",dim(kpidata)[1],dim(kpidata)[2])
  
  subpoppid.1yr <- scoredadatsets[[6]]
  sprintf("subpop 1 yr data has %d rows and %d columns",dim(subpoppid.1yr)[1],dim(subpoppid.1yr)[2])
  
  subpoppid.2.5yr <- scoredadatsets[[7]]
  sprintf("subpop 2.5 yr data has %d rows and %d columns",dim(subpoppid.2.5yr)[1],dim(subpoppid.2.5yr)[2])
  
  
}else{
  
  MSG <- sprintf("please verify score file and also train test score files, either of them do not exist")
  stop(MSG)
}



##
# overlap and nonoverlap with train
data$patient_id <- as.numeric(data$patient_id)
traindata1yr$patient_id <- as.numeric(traindata1yr$patient_id)
traindata25yr$patient_id <- as.numeric(traindata25yr$patient_id)

phc.overlapped <- dplyr::semi_join(data,traindata1yr,by="patient_id")
phc.nonoverlapped <- dplyr::anti_join(data,traindata1yr,by="patient_id")

phc.overlapped <- as.data.table(phc.overlapped)
phc.nonoverlapped <- as.data.table(phc.nonoverlapped)


phc.overlapped[,Outcome_variable := base::ifelse(phc.overlapped$Outcome_variable == "NULL","Indeterminate",phc.overlapped$Outcome_variable)]
phc.nonoverlapped[,Outcome_variable := base::ifelse(phc.nonoverlapped$Outcome_variable == "NULL","Indeterminate",phc.nonoverlapped$Outcome_variable)]


phc.overlapped <- phc.overlapped[!(Outcome_variable=="Indeterminate")]
phc.nonoverlapped <- phc.nonoverlapped[!(Outcome_variable=="Indeterminate")]


phc.overlapped <- phc.overlapped[,-c("DRERiskStratification")]
phc.nonoverlapped <- phc.nonoverlapped[,-c("DRERiskStratification")]


num.var <- c("Class probabilities")
phc.overlapped[,(num.var):= lapply(.SD,as.numeric), .SDcols = num.var]
phc.nonoverlapped[,(num.var):= lapply(.SD,as.numeric), .SDcols = num.var]

phc.overlapped <- na.omit(phc.overlapped)
phc.nonoverlapped <- na.omit(phc.nonoverlapped)

phc.overlapped.kpi  <- obtainKPImodel(phc.overlapped,"DRE")
phc.nonoverlapped.kpi  <- obtainKPImodel(phc.nonoverlapped,"DRE")



# saving the copy to handle nondre differently

datanondre <- data

# TO generate KPI considering without indeterminantes

data[,Outcome_variable := base::ifelse(data$Outcome_variable == "NULL","Indeterminate",data$Outcome_variable)]

num.var <- c("Class probabilities")

data[,(num.var):= lapply(.SD,as.numeric), .SDcols = num.var]

data <- na.omit(data)

print(paste("threshold",modelmetadata$Threshold))

temp2 <- as.data.table(data)

# to write prevalence based threshold from risk stratification function.

risk.threshold <- RiskStratification(scoredata1yr,threshold=modelmetadata$Threshold,prevalence)

risk.threshold.high <- risk.threshold[[2]]
risk.threshold.low <-  risk.threshold[[3]]

filtered.temp2 <- temp2[!(Outcome_variable=="Indeterminate")]

# DRE file name will have added column (DRERiskStratification) post risk stratification so needs to be re-read in following fn
temp3 <- RiskConfusion(filtered.temp2)


# TO generate KPI considering indeterminantes as NON DRE

datanondre[,Outcome_variable := base::ifelse(datanondre$Outcome_variable == "Indeterminate","Non_DRE",datanondre$Outcome_variable)]

datanondre[,(num.var):= lapply(.SD,as.numeric), .SDcols = num.var]

datanondre <- na.omit(datanondre)

print(paste("threshold",modelmetadata$Threshold))

temp2nondre <- as.data.table(datanondre)

temp3nondre <- RiskConfusion(temp2nondre)



if (   
  (
    (dim(temp3[[1]])[1] > 0)
    &&
    (dim(temp3[[2]])[1] > 0)
    &&
    (dim(temp3[[3]])[1] > 0)
    &&
    (dim(filtered.temp2)[1] > 0)
  )
  &&
  (
    (dim(temp3nondre[[1]])[1] > 0)
    &&
    (dim(temp3nondre[[2]])[1] > 0)
    &&
    (dim(temp3nondre[[3]])[1] > 0)
    &&
    (dim(temp2nondre)[1] > 0)
  )
  
)
{
  
  # TO generate table for KPI without indeterminants
  
  high.result <- obtainKPImodel(temp3[[1]],"DRE")
  med.result <- obtainKPImodel(temp3[[2]],"DRE")
  low.result <- obtainKPImodel(temp3[[3]],"DRE")
  all.result  <- obtainKPImodel(filtered.temp2,"DRE")
  nothigh.result <- obtainKPImodel(temp3[[4]],"DRE")
  row.names(nothigh.result[[2]]) <- c("High","NotHigh")
  
  all.df <- as.data.frame(all.result[[1]]$`Model performance for DRE scored dataset`)
  high.df<-as.data.frame(high.result[[1]]$`Model performance for DRE scored dataset`)
  med.df<-as.data.frame(med.result[[1]]$`Model performance for DRE scored dataset`)
  low.df<-as.data.frame(low.result[[1]]$`Model performance for DRE scored dataset`)
  nothigh.df <- as.data.frame(nothigh.result[[1]]$`Model performance for DRE scored dataset`)
  
  # TO generate table for KPI with indeterminants
  high.result.nondre <- obtainKPImodel(temp3nondre[[1]],"DRE")
  med.result.nondre <- obtainKPImodel(temp3nondre[[2]],"DRE")
  low.result.nondre <- obtainKPImodel(temp3nondre[[3]],"DRE")
  all.result.nondre  <- obtainKPImodel(temp2nondre,"DRE")
  nothigh.result.nondre <- obtainKPImodel(temp3nondre[[4]],"DRE")
  row.names(nothigh.result.nondre[[2]]) <- c("High","NotHigh")
  
  all.df.nondre <- as.data.frame(all.result.nondre[[1]]$`Model performance for DRE scored dataset`)
  high.df.nondre<-as.data.frame(high.result.nondre[[1]]$`Model performance for DRE scored dataset`)
  med.df.nondre<-as.data.frame(med.result.nondre[[1]]$`Model performance for DRE scored dataset`)
  low.df.nondre<-as.data.frame(low.result.nondre[[1]]$`Model performance for DRE scored dataset`)
  nothigh.df.nondre <- as.data.frame(nothigh.result.nondre[[1]]$`Model performance for DRE scored dataset`)
  
  
  colnames(kpidata) <- c("kpi","Train","sha")
  
  final.df <- cbind(all.df,nothigh.df,phc.nonoverlapped.kpi$`KPI Table`,phc.overlapped.kpi$`KPI Table`,all.df.nondre,nothigh.df.nondre)
  
  col.vector <- rownames(all.result[[1]])
  
  row.names(final.df) <- col.vector
  
  
  print(paste("stratification result ",final.df))
  
  outRiskConfusion <- tolower(args[3])
  
  
  # calculate final n for each subcohort
  
  vec.n <- c(dim(filtered.temp2)[1],dim(filtered.temp2)[1],dim(phc.nonoverlapped)[1],dim(phc.overlapped)[1],dim(traindata25yr)[1],"353395",dim(datanondre)[1],dim(datanondre)[1])
  
  
  # vector to calculate prevalence for 2.5 year
  prev.25.1 <- table(filtered.temp2$Outcome_variable)[[1]]/dim(filtered.temp2)[1]
  prev.25.2 <- table(phc.nonoverlapped$Outcome_variable)[[1]]/dim(phc.nonoverlapped)[1]
  prev.25.3 <- table(phc.overlapped$Outcome_variable)[[1]]/dim(phc.overlapped)[1]
  prev.25.4 <- table(traindata25yr$Outcome_variable)[[1]]/dim(traindata25yr)[1]
  prev.25.5 <- table(temp2$Outcome_variable)[[1]]/dim(temp2)[1]
  
  
  # vector to calculate prevalence for 1 year
  scoredata1yr.1 <- scoredata1yr[!(Outcome_variable=="Indeterminate")]
  phc.overlapped.1 <- dplyr::semi_join(scoredata1yr.1,traindata1yr,by="patient_id")
  phc.nonoverlapped.1 <- dplyr::anti_join(scoredata1yr.1,traindata1yr,by="patient_id")
  
  phc.overlapped.1 <- as.data.table(phc.overlapped.1)
  phc.nonoverlapped.1 <- as.data.table(phc.nonoverlapped.1)
  
  phc.overlapped.1[,Outcome_variable := base::ifelse(phc.overlapped.1$Outcome_variable == "NULL","Indeterminate",phc.overlapped.1$Outcome_variable)]
  phc.nonoverlapped.1[,Outcome_variable := base::ifelse(phc.nonoverlapped.1$Outcome_variable == "NULL","Indeterminate",phc.nonoverlapped.1$Outcome_variable)]
  
  
  phc.overlapped.1 <- na.omit(phc.overlapped.1)
  phc.nonoverlapped.1 <- na.omit(phc.nonoverlapped.1)
  
  
  phc.overlapped.1 <- phc.overlapped.1[!(Outcome_variable=="Indeterminate")]
  phc.nonoverlapped.1 <- phc.nonoverlapped.1[!(Outcome_variable=="Indeterminate")]
  
  prev.1.1 <- table(scoredata1yr.1$Outcome_variable)[[1]]/dim(scoredata1yr.1)[1]
  prev.1.2 <- table(phc.nonoverlapped.1$Outcome_variable)[[1]]/dim(phc.nonoverlapped.1)[1]
  prev.1.3 <- table(phc.overlapped.1$Outcome_variable)[[1]]/dim(phc.overlapped.1)[1]
  prev.1.4 <- table(traindata1yr$Outcome_variable)[[1]]/dim(traindata1yr)[1]
  prev.1.5 <- table(scoredata1yr$Outcome_variable)[[1]]/dim(scoredata1yr)[1]
  
  
  vec.prevalence.25 <- c(prev.25.1,prev.25.2,prev.25.3,prev.25.4,prev.25.5)  
  vec.prevalence.1  <- c(prev.1.1,prev.1.2,prev.1.3,prev.1.4,prev.1.5)
  
  vec.prevalence.25 <- sapply(vec.prevalence.25,function(x) paste(sprintf("%.2f", round(x*100,2)),"%"))
  vec.prevalence.1  <- sapply(vec.prevalence.1,function(x) paste(sprintf("%.2f", round(x*100,2)),"%"))
  
 
  
  #subpop analysis
  subpoppid.1yr <- subpoppid.1yr[1:dim(subpoppid.1yr)[1],lapply(.SD, as.numeric)]
  subpoppid.1yr <- as.data.frame(subpoppid.1yr)
  subpoppid.2.5yr <- subpoppid.2.5yr[1:dim(subpoppid.2.5yr)[1],lapply(.SD, as.numeric)]
  subpoppid.2.5yr <- as.data.frame(subpoppid.2.5yr)
  df.subpop.25yr <- getSubpopKPI(filtered.temp2,subpoppid.2.5yr)
  print("subpop kpi computed for 2.5 yr")
  
  scoredata1yr.1$patient_id <- as.numeric(scoredata1yr.1$patient_id)
  df.subpop.1yr <- getSubpopKPI(scoredata1yr.1,subpoppid.1yr)
  print("subpop kpi computed for 1 yr")
  outSubpop <- args[9]
  
  
  # this section is to remove % prevalence in PPV and trim extra spaces. also remove NA from gini index and brier score
  
  
  for (z in (1:4)){
    
    # remove % from PPV
    
    df.subpop.1yr$`Threshold depedent KPI for model `[4,z] <- 
      paste( trimws(strsplit(df.subpop.1yr$`Threshold depedent KPI for model `[4,z],"at")[[1]][1]),
             trimws(strsplit(df.subpop.1yr$`Threshold depedent KPI for model `[4,z],"prevalence")[[1]][2])
             )
    
    df.subpop.25yr$`Threshold depedent KPI for model `[4,z] <- 
      paste( trimws(strsplit(df.subpop.25yr$`Threshold depedent KPI for model `[4,z],"at")[[1]][1]),
             trimws(strsplit(df.subpop.25yr$`Threshold depedent KPI for model `[4,z],"prevalence")[[1]][2])
             )
    
    
  }
  
  
  
  # adjust subpops 1 yr and 2.5 yr to add prevalence 2.5 yrs and prevalence for 1 yr in line
  
  #STEP-1
  org.colnames <- names(df.subpop.1yr[[1]])
  
  #Step2: Combine datasets
  
  order_rows <- c( "Diagnostic odds ratio", "Sensitivity/Recall","Specificity","PPV/Precision","prevalence1",
                   "prevalence","NPV","Youden's J-statistic","F1 statistic")
  
  
  yr1_data <- df.subpop.1yr[[1]][order_rows,] #Create dataframe as per the required order 
  names(yr1_data) <- paste("Col",c(1,2,3,4),sep = "")
  
  yr25_data <- df.subpop.25yr[[1]]["prevalence",]
  names(yr25_data) <- paste("Col",c(1,2,3,4),sep = "")
  
  comb.data <- rbind(yr1_data,yr25_data)
  
  #Step3: Order them
  
  comb.data <- comb.data[order_rows,]
  
  rownames(comb.data) <- c( "Diagnostic odds ratio", "Sensitivity/Recall","Specificity","PPV/Precision","prevalence 2.5 yrs",
                            "prevalence 1 yr","NPV","Youden's J-statistic","F1 statistic")
  
  
  names(comb.data) <- c(org.colnames)
  
  
  # this is to create train and test 1 yr and 2.5 yr KPIs file for supplementary analysis
  
  traindata1yrkpi <- obtainKPImodel(traindata1yr,"DRE")
  traindata25yrkpi <- obtainKPImodel(traindata25yr,"DRE")
  testdata1yrkpi <- obtainKPImodel(testdata1yr,"DRE")
  testdata25yrkpi <- obtainKPImodel(testdata25yr,"DRE")
  
  train1 <- paste("Train 1 yr KPI"," (n= ",nrow(traindata1yr),")",sep="")
  test1 <- paste("Test  1 yr KPI"," (n= ",nrow(testdata1yr),")",sep="")
  train25 <- paste("Train 2.5 yrs KPI"," (n= ",nrow(traindata25yr),")",sep="")
  test25 <- paste("Test 2.5 yrs KPI"," (n= ",nrow(testdata25yr),")",sep="")
  
  
  cbind.kpi <- cbind(traindata1yrkpi$`KPI Table`,testdata1yrkpi$`KPI Table`,traindata25yrkpi$`KPI Table`,testdata25yrkpi$`KPI Table`)
  colnames(cbind.kpi) <- c(train1,test1,train25,test25)
  
  
  
  # function to write results to csv in sequential-manner
  suppressWarnings(writeDREResults(outRiskConfusion,vec.n,vec.prevalence.25,vec.prevalence.1,nothigh.df,final.df,
                                   all.result,high.result,med.result,low.result,nothigh.result,
                                   all.result.nondre,high.result.nondre,med.result.nondre,
                                   low.result.nondre,nothigh.result.nondre,kpidata,df.subpop.1yr,
                                   comb.data,cbind.kpi,outSubpop,risk.threshold.high,risk.threshold.low,prevalence))
  
  # save calibration plot
  outCalibrationjpg <- args[10]  
  
  jpeg(filename = outCalibrationjpg,
       quality=100,
       width=1024,
       height = 768)
  
  par(mfcol=c(2,1))
  cplot <- CalibrationPlot(filtered.temp2,"DRE",15,"PHS without indeterminantes")
  cplot.test <- CalibrationPlot(testdata1yr,"DRE",15,"Model validation set")
  dev.off()
  
  
  
}else{
  
  warning("There should be atleast one observation in high, medium or low risk patients to score")
  
}


