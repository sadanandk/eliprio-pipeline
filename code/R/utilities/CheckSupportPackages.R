#'CheckSupportPackages
#'Checks Supporting Packages and installs them if not available
#'@description To check if required packages are already installed in RStudio,
#'             or/and install the required packages along with other package mentioned by user.
#'@usage CheckSupportPackages(userlist = NULL, support = TRUE)
#'@author Manan Thunia
#'@param userlist : the packages we wish to install like("rmarkdown","data.table","rstudioapi",
#'"jsonlite","DT","xlsx","Metrics",#'"verification","DescTools","gbm",#'"kableExtra","pROC","ggplot2")
#'@param support : TRUE /FALSE gives logical value if RStudio has the above packages installed.
#' If we wish to install additional package with the existing user list, we can mention it as first argument.
#'When support = FALSE the only package mentioned in the first argument is installed.
#'@examples
#'\dontrun{
#'CheckSupportPackages()
#'}
CheckSupportPackages<- function(userlist = NULL,support= TRUE)
{
  #Checks for required packages in Rstudio,installs if not supported.
  #Installs any other package along with specific package specified by user
  #Args:
  # userlist: Any package user wishes to check if installed or keep empty.
  # support : TRUE/FALSE
  if(!is.null(userlist) | support)
  {
    #If userlist or the packages we need is not empty
    if (support==TRUE)
    {
      #And if support is TRUE then makes vector containing packages in list along with
      #any additional user specified package
      list.of.packages <- c("rmarkdown","data.table","rstudioapi",
                            "jsonlite","DT","xlsx","Metrics",
                            "verification","DescTools","gbm",
                            "kableExtra","pROC","ggplot2","R.utils",
                            userlist)
    }
    # If support is False the package is only the one specified by user
    else
    {
      list.of.packages <- userlist
    }
  }
  # Creates vector of packages that are not in list and installs them
  new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]

  if(length(new.packages)) install.packages(new.packages)

  # Load Required Libraries

  lapply(list.of.packages, require, character.only = TRUE)


}
