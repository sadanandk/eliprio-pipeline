
#' writeDistribution.R: Function to write Age, CCI, ECI, line therapy distribution for eliprio and matched cohort to a file
#'
#' @param eliprio list containting Age, CCI, ECI, line therapy values for eliprio group obtained from getDistribution function
#' @param matchcohort list containting Age, CCI, ECI, line therapy values for matched cohort for eliprio group
#' @param threshold threshold used for eliprio group (used for title)
#' @param outputfile output file to which distributions are written. Note file should exists
#'
#' @return None
#' @export
#'
#' @examples
#' writeDistribution(eliprio = eliprio.distributionmaxF1,matchcohort = practice.distributionmaxF1,threshold = "max(F1)",outputfile = kpioutput)
writeDistribution <- function(eliprio,matchcohort,threshold, outputfile)
{
  #TO DO
  # Add check for existence of required columns in the input data
## Age distribution for given gender
temp <- cbind(eliprio$`Age Distribution`,matchcohort$`Age Distribution`)
temp <- rbind(colnames(temp),temp) #Add male, female as row
temp <- cbind(rownames(temp),temp) #Add age group as column
rownames(temp) <- NULL
#Specify groups eliprio and matched cohort for each of the column
colnames(temp) <- c("Age Group","eliprio","eliprio","Matched cohort","Matched cohot")

title <- sprintf("Table: Age and Gender distribution, for threshold, p= %s ", as.character(threshold))
  
utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = title,file = outputfile,append = TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table("--------------------------------------",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = temp,sep=",",file = outputfile,append = TRUE,row.names = FALSE,col.names = TRUE)
utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)

#-------------------------------
#ECI distribution for p = 0.5
#-------------------------------
temp <- dplyr::bind_rows(eliprio$`ECI Distribution`,matchcohort$`ECI Distribution`)
temp <- cbind(c("eliprio","matched cohort"),temp)
colnames(temp)[1] <- "Study Group / ECI index"
title <- sprintf("Table: ECI distribution, for threshold, p = %s", as.character(threshold))

utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = title ,file = outputfile,append = TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table("--------------------------------------",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = temp,sep=",",file = outputfile,append = TRUE,row.names = FALSE,col.names = TRUE)
utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)

#CCI distribution for p = 0.5
temp <- dplyr::bind_rows(eliprio$`CCI Distribution`,matchcohort$`CCI Distribution`)
temp[is.na(temp)] <- 0
temp <- cbind(c("eliprio","matched cohort"),temp)
colnames(temp)[1] <- "Study Group / CCI index->"
title <- sprintf("Table: CCI distribution, for threshold, p = %s", as.character(threshold))

utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = title,file = outputfile,append = TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table("--------------------------------------",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = temp,sep=",",file = outputfile,append = TRUE,row.names = FALSE,col.names = TRUE)
utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)


#Line therapy distribution for p = 0.5
temp <- dplyr::bind_rows(eliprio$`Line therapy Distribution`,matchcohort$`Line therapy Distribution`)
temp[is.na(temp)] <- 0

temp <- cbind(c("eliprio","matched cohort"),temp)
colnames(temp)[1] <- "Study Group / Line therapy->"
title <- sprintf("Table: Line therapy distribution, for threshold, p = %s", as.character(threshold))


utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = title,file = outputfile,append = TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table("--------------------------------------",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
utils::write.table(x = temp,sep=",",file = outputfile,append = TRUE,row.names = FALSE,col.names = TRUE)
utils::write.table("\n",file=outputfile,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)

}
