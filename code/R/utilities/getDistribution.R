#' getDistribution
#' Provides a distribution of Age, CCI, ECI score and line therapy as a table
#' @param data data.frame with columns "Age", "Gender","CCI Score","ECI score","linetherapy"
#'
#' @return tables with distribution for genderwise age distribution, ECI, CCI and linetherapy
#' @export
#'
#' @examples
getDistribution <- function(data)
{
  
  req.columns <- c("Age", "Gender","CCI Score","ECI score","linetherapy")
  
  #Check if all required columns are available, else stop
  if( !all(req.columns %in% names(data)))
  {
        sprintf("Required columns: %s not present in the data",paste(req.columns[req.columns %in% names(data)],collapse = ","))
        stop("Not all required columns available")
  }
  
  
  #AgeGroup requird for Age distribution 
  Agebreaks <- c(18,35,65,79,100)
  AgeGrouplabels <- c("18-35","35-65","65-79","79-100")
  
  #ECCI, CCI levels requried
  # Levels: 0 1 2 3 4  >4
  ECICCILevels = c(as.character(0:4),">4")
  
  #Line thereapy levels
  LinetherapyLevels = c("First index event", 
                        "Second index event",
                        "Third index event",
                        "Other index event")
  
  # Age distribution 
  #Ensure that bins have following bindings
  #  "18-35": 18 <= Age < 35
  #  "35-65": 35 <= Age < 65
  #  "65-79": 65 <= Age < 79
  # "79-100": 79 <= Age < 100
  
  data$ageBin <- cut(data$Age,breaks = Agebreaks, labels = AgeGrouplabels,
                     include.lowest = TRUE,right = FALSE)
  
  age.distrb <- table(data$ageBin,data$Gender)
  
  #ECI distribution 
  #Ensure all requisite levels are reported
  temp <- data$`ECI score`
  levels.required <- ECICCILevels[1:5]
  temp[!(temp %in% levels.required )] <- ECICCILevels[6]
  temp <- factor(temp, levels = c(levels.required, ECICCILevels[6]),
                 labels = ECICCILevels)
  
  temp2 <- table(temp)
  
  temp3 <-  round(prop.table(temp2)*100,2) #Get percentage with max 2 digits
  
  #concatenate number and percentage and create a row matrix with column names
  temp4 <- setNames(sprintf("%d (%4.2f%%)",temp2,temp3), names(temp2))
  
  ECI.distrb <- temp4
  
  #CCI
  temp <- data$`CCI Score`
  levels.required <- ECICCILevels[1:5]
  temp[!(temp %in% levels.required )] <- ECICCILevels[6]
  temp <- factor(temp, levels = c(levels.required, ECICCILevels[6]),
                 labels = ECICCILevels)
  
  temp2 <- table(temp)
  
  temp3 <-  round(prop.table(temp2)*100,2) #Get percentage with max 2 digits
  
  #concatenate number and percentage and create a row matrix with column names
  temp4 <- setNames(sprintf("%d (%4.2f%%)",temp2,temp3), names(temp2))
  
  CCI.distrb <- temp4
 
  
  #LineTherapy
  temp <- as.character(data$linetherapy)
  #line index greater than 3 marked to other
  levels.required <- as.character(1:3)
  temp[!(temp %in% levels.required )] <- LinetherapyLevels[4]
  temp <- factor(temp, levels = c(levels.required, LinetherapyLevels[4]),labels = LinetherapyLevels)
  
  temp2 <- table(temp)

  temp3 <-  round(prop.table(temp2)*100,2) #Get percentage with max 2 digits
  
  #concatenate number and percentage and create a row matrix with column names
  temp4 <- setNames(sprintf("%d (%4.2f%%)",temp2,temp3), names(temp2))
                    
  
  linetherapy.distrib <- temp4
 
  
  #Consolidate the output
  output <- list("Age Distribution" = age.distrb,
                 "ECI Distribution" = ECI.distrb,
                 "CCI Distribution" = CCI.distrb,
                 "Line therapy Distribution" = linetherapy.distrib)
  
 
  
  return(output)
  
}
