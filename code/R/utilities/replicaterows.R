#' replicaterows
#' @description Function to create input data for scoring multiple AED performances using TDS model
#' 
#' For scoring multiple AED performance for given record of patient ID, The scoring pipeline expects 
#' one row with given AED under AED column. This function takes individual record and replicates and adds different 
#' AED to be scored for each of the record
#' @param data Provide the processed TDS feature csv file (data.table class object) 
#' @param aednames List of independent AED's name to be scored against each patientId and Index date combination  
#' @return data.table object replicating rows by as many as independent AED for each combination of patient id and index date
#' 
#' @author Manan Thunia
#' 
#' 
#' 
#' TODO : Check for following features
#' "patient_id"                               "Valid_Index"                                    
#' "Last_AED"                                 "AED"                                    
#' ,"CCI_Score"                               "ECI_Score"                              
#' ,"Gender"                                  "AFFECDISOR"                             
#' ,"COTREAT30_PHARMACA"                      "OTHRMENT"                               
#' ,"SUBSTABUSE"                              "SERMENTILL"                             
#' ,"Cerebrovascular_Disease"                 "Chronic_Pulmonary_Disease"              
#' ,"Dementia"                                "SLEEP"                                  
#' ,"OBESITY"                                 "NEUROCMD"                               
#' ,"Mild_Liver_Disease"                      "Hemiplegia_or_Paraplegia"               
#' ,"SEIZURE345OR78039"                       "MENTRETRD"                              
#' ,"Connective_Tissue_Disease"               "Last_365d_ICD_78039"                    
#' ,"Solid_Tumor_Without_Metastases"          "Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin"
#' ,"AUTOIMMUNE"                              "Last_365d_ICD_34590"                    
#' ,"LIVER"                                   "Peptic_Ulcer_Disease"                   
#' ,"Hypertension"                            "CARDIO"                                 
#' ,"Cardiac_Arrhythmias"                     "RENALINSUFF"                            
#' ,"COTREAT30_ANTIBIOTICS"                   "Peripheral_Vascular_Disease"            
#' ,"Metastatic_Solid_Tumor"                  "Congestive_Heart_Failure"               
#' ,"Mod_Severe_Renal_Disease"                "Brain_Tumor"                            
#' ,"Aspiration_Pneumonia"                    "DIAB"                                   
#' ,"Last_365d_ICD_34541"                     "Last_365d_ICD_34550"                    
#' ,"Last_365d_ICD_34540"                     "OSTEO"                                  
#' ,"Myocardial_Infarction"                   "Last_365d_ICD_34501"                    
#' ,"Diabetes_With_Organ_Damage"              "Last_365d_ICD_34511"                    
#' ,"Age"                                     "NumTreatmentChange"
#' , "Outcome_variable
#' 
#' 
replicaterows <- function(data,aednames)
{
  require(splitstackshape)

  #Check for the list of independent AED's
  if(length(aednames) < 1)
  {
    stop("No AED is passed to score")
  }

  data.copy <- data

  rows.in.data <- dim(data)[1]
  len <- length(aednames)


  data[,freq := len]
  data <- expandRows(data, "freq")

  aedlist <- rep(aednames,rows.in.data)
  data[,AED := aedlist]

  data.copy[,freq := NULL]
  data <- rbind(data.copy,data)

  data <- data[!duplicated(data),]

  data.copy <- NULL
  return(data)
}