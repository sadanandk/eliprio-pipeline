#' getsubpopKPI
#' @description This function KPI for given subpopulations 
#'
#' @param scoredata model score data having "Outcome_variable" ,"Model Label" and "Class probabilities" columns
#' @param subpopID a daframe containig patientID for each subpopulation arranged in columns
#' for example 
#'   Age65Above  Medicaid  Medicare  Private
# 1   97038615  99195586  95782776 95284759
# 2   97213770 114722344  96294228 95322406
# 3   97306072 119581863  97306072 95541999
# 4   98447603 126951947  98447603 95569507
# 5   99171904 130196888  99171904 95578933
# 6  100381451 146865210 100381451 96060184
#' @return list of data frame for KPI and confusion matrix
#' @author Yogesh PARTE
#'
#' @examples
# scoredata <- fread("output/d5_dre_sha_score.csv")
# 
# #Retain only DRE and non-DRE data
# scoredata <- scoredata %>% filter(Outcome_variable %in% c("DRE","Non_DRE"))
# 
# subpoppid.1yr <- fread("input/d6.5_rpt_dre_subpop_cohorts.csv")
# subpoppid.2.5yr <- fread("input/d6.5_rpt_dre_subpop_cohorts_2_5Y.csv")
# 
# subpoppid.1yr <- subpoppid.1yr[2:dim(subpoppid.1yr)[1],lapply(.SD, as.numeric)]
# 
# subpoppid.1yr <- as.data.frame(subpoppid.1yr)
# 
# subpoppid.2.5yr <- subpoppid.2.5yr[2:dim(subpoppid.2.5yr)[1],lapply(.SD, as.numeric)]
# 
# subpoppid.2.5yr <- as.data.frame(subpoppid.2.5yr)

# getSubpopKPI(scoredata = scoredata,subpopID = subpoppid.1yr)
# $`Threshold depedent KPI for model `
# Age65Above n= (64)            Medicaid n= (29)            Medicare n= (60)            Private n= (407)
# Diagnostic odds ratio (DOR)                         Inf                     15.7189                      8.0219                      2.9363
# Sensitivity/Recall                               0.2222                      0.9167                      0.5789                      0.6667
# Specificity                                           1                      0.5882                      0.8537                      0.5948
# PPV/Precision               1.0000 at 14.06% prevalence 0.6111 at 41.38% prevalence 0.6471 at 31.67% prevalence 0.4577 at 33.91% prevalence
# NPV                                              0.8871                      0.9091                       0.814                      0.7767
# Youden's J-statistic                             0.2222                      0.5049                      0.4326                      0.2615
# F1 statistic                                     0.3636                      0.7333                      0.6111                      0.5428
# 
# $`Threshold indepedent KPIs for model`
#             Age65Above n= (64) Medicaid n= (29) Medicare n= (60) Private n= (407)
# ROC-AUC                 0.6838           0.7843           0.7824            0.688
# Gini index              0.3677           0.5686           0.5648            0.376
# Brier score              0.129           0.2786           0.2597           0.2577
# Accuracy                0.8906           0.7241           0.7667           0.6192
# Kappa                   0.3293           0.4703           0.4452           0.2353

# getSubpopKPI(scoredata = scoredata,subpopID = subpoppid.2.5yr)
# $`Threshold depedent KPI for model `
# Age65Above n= (54)            Medicaid n= (27)            Medicare n= (50)            Private n= (371)
# Diagnostic odds ratio (DOR)                         Inf                     16.5072                     15.1808                      2.7938
# Sensitivity/Recall                               0.2222                      0.9167                      0.6111                      0.6617
# Specificity                                           1                         0.6                      0.9062                      0.5882
# PPV/Precision               1.0000 at 16.67% prevalence 0.6471 at 44.44% prevalence 0.7857 at 36.00% prevalence 0.4731 at 35.85% prevalence
# NPV                                              0.8654                         0.9                      0.8056                      0.7568
# Youden's J-statistic                             0.2222                      0.5167                      0.5173                      0.2499
# F1 statistic                                     0.3636                      0.7586                      0.6875                      0.5517
# 
# $`Threshold indepedent KPIs for model`
# Age65Above n= (54) Medicaid n= (27) Medicare n= (50) Private n= (371)
# ROC-AUC                 0.6704           0.7889           0.8116           0.6836
# Gini index              0.3407           0.5778           0.6233           0.3673
# Brier score             0.1527           0.2982           0.2924           0.2718
# Accuracy                0.8704           0.7407              0.8           0.6146
# Kappa                   0.3226            0.496           0.5438           0.2297

getSubpopKPI <- function(scoredata,subpopID = NULL) 
{
  require(dplyr)
  
  KPI=list()
  # Compute KPI for AgeAbove65
  criteria <- colnames(subpopID)
  variable <- NULL
  for (variable in criteria) {
    
    
    INDX <- subpopID[(subpopID[,variable] %in% scoredata$patient_id),variable]
    
    temp <- scoredata %>% filter( patient_id %in% INDX)
    
    prevalence <- prop.table(table(temp$Outcome_variable))[1]*100
    
    var <- paste(variable," (n= ",length(INDX),")",sep="")
    
    temp <- obtainKPImodel(as.data.table(temp),solution = "DRE")[[1]]
    
    temp["prevalence",1] <- paste(sprintf("%.2f", round(prevalence,2)),"%")
    
    KPI[[var]] <- temp
  }
  
  
  var <- names(KPI)
  KPI <- do.call(cbind,KPI)
  names(KPI) <- var
  
  
  #Formatting 
  #Threshold independent KPI
  #Diagnostic odds ratio, Sensitivity/Recall, Specificity, PPV/Precision, NPV, J statistic, F1, Prevelance
  indx1  <- c(12,9,10,14,15,13,8,16)
  
  #Threshold dependependent KPI
  #ROC-AUC, Gini Index, Brier Score,  Accuracy, Cohen's Kappa
  
  indx2  <- c(1,2,5,11,7)
  
  output  <- list("Threshold depedent KPI for model "= KPI[indx1,],
                  "Threshold indepedent KPIs for model"= KPI[indx2,])
  
  return(output)
  
}