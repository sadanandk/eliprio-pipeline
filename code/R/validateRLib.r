############################
#### Validate if pkgs were installed or not
#Input: Lib loc
#Output : validation msg
############################
options(warn=-1)

args = commandArgs(trailingOnly = TRUE)
#print(paste("arg 1 libloc", args[1]))

pkgs <- c("Metrics","data.table","DescTools","base","caret","ggplot2","backports","dlookr","curl","rstudioapi","httr","memoise","summarytools","knitr","R.utils","splitstackshape","epiR","pROC","tidyr","BiasedUrn","dplyr","PresenceAbsence")
for(pkg in pkgs) {
  pkg.installed<-gsub("\"","",pkg)
  if (suppressMessages(!require(pkg.installed, lib.loc=.libPaths(),character.only = TRUE))) {
    msg <- paste('The package ', pkg ,' was not installed')
    stop(msg)
  }
}