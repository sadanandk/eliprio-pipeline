#Example : 
#Description Script to supplementary analysis
##Input: 
##Output: Statistical test information in a csv


#-- Read the input arguments ----

      #Suppress warnings
      options(warn = 0)
      
      
      args = commandArgs(trailingOnly = TRUE)
      args[1] <- "Z:/Study_Synoma_Reports/PHS_SHA2_Reports/TDS_New/d5_tds_aed_sha_score.csv"
      args[2] <- "Z:/Suresh/TDS_Payer_Type_PatientList.csv"
      args[3] <- "Z:/Manan"
      
#-- check if there are four arguments: if not, return an error ----
      if(length(args) != 3)
      {
        stop(print("Wrong number of inputs to TDS_StatisticalAnalysis.R\n
                   Require 3 arguments namely,\n
                   1. path for the cohort characteristic csv file\n 
                   2. path for the KPI csv file\n
                   3. path for output csv file"))
      }
      
      print(paste("arg 1 Subpop file location",args[1]))
      print(paste("arg 2 PHC scored dataset location",args[2]))
      print(paste("arg 3 TDS Supplementary analysis output file location",args[3]))

      filename <- args[1]
      sup.filename.loc <- args[2]
      output.filename <- "TDS_SupplementaryAnalysis.csv"

#Check for consistency/ environment, etc.,
      suppressMessages(library(data.table,  lib.loc=.libPaths()))
      suppressMessages(library(Metrics,	  lib.loc=.libPaths()))
      suppressMessages(library(DescTools,   lib.loc=.libPaths()))
      suppressMessages(library(base,        lib.loc=.libPaths()))
      suppressMessages(library(caret,       lib.loc=.libPaths()))		
      suppressMessages(library(ggplot2,     lib.loc=.libPaths()))	
      suppressMessages(library(R.utils,     lib.loc=.libPaths()))
      suppressMessages(library(jsonlite,     lib.loc=.libPaths()))
      suppressMessages(library(e1071,     lib.loc=.libPaths()))
      suppressMessages(library(dplyr,     lib.loc=.libPaths()))
      suppressMessages(library(epiR,     lib.loc=.libPaths()))
      suppressMessages(library(pROC,     lib.loc=.libPaths()))
      
      #load all utility function
      #R.utils::sourceDirectory("code/R/utilities")
      R.utils::sourceDirectory("F:/MananT/UCB_AtlassianCloud_Repository/Newcommit2/eliprio-pipeline/code/R/utilities")
      #file.list <- file.path("code/R/utilities", list.files(path="utilities/",pattern = '*.R'))
      #sapply(file.list,source)

      #Read the scoring file
      if(!is.character(filename))                                                     #TO CHECK IF FILENAME IS OF CHARACTER TYPE
      {
        MSG <- sprintf("Kindly pass the file name as character")
        stop(MSG)
      }
      
      #Loading file
      if(file.exists(filename)) #TO CHECK IF FILE EXIST                                                      
      {
        data.scored <- fread(input = filename,colClasses=list(character=c(tolower("Patient_ID"))),sep = ",",stringsAsFactors = FALSE)
        sprintf("Scored data has %d rows and %d columns",dim(data.scored)[1],dim(data.scored)[2])
        
      }else{
        
        MSG <- sprintf("File doesn't exist")
        stop(MSG)
      }
      
      #For TDS
      #Consider only 1st rows of every combination of patient_id and valid_index
      num.var <- c("Class probabilities")
      data.scored[,(num.var):= lapply(.SD,as.numeric), .SDcols = num.var]
      data.scored <- na.omit(data.scored)
      
      
      #Create data for Match Analysis
      
      data.for.match <- ProcessData4MatchAnalysis(data.scored)
      data.for.match <- cbind(data.for.match,data.scored[1:dim(data.for.match)[1],c("Model Label","Class probabilities"),with = FALSE])
      
      
      #Loading sub population file
      if(file.exists(sup.filename.loc)) #TO CHECK IF FILE EXIST                       
      {
        subgroupinfo <- fread(input = sup.filename.loc,sep = ",",stringsAsFactors = FALSE,colClasses=list(character=c(tolower("Patient_ID"))))
        sprintf("Subpop data has %d rows and %d columns",dim(subgroupinfo)[1],dim(subgroupinfo)[2])
        
      }else{
        
        MSG <- sprintf("File doesn't exist")
        stop(MSG)
      }
      
      subgroupinfo$Valid_Index <- as.Date(x = subgroupinfo$Valid_Index,tryFormats = "%Y-%m-%d")
      data.for.match$`Index date` <- as.Date(x = data.for.match$`Index date`,tryFormats = "%Y-%m-%d")
      
      names(data.for.match)[2] <- "Valid_Index"
      
      #KPI for subgropus based on Payer type
      payer.info <- unique(subgroupinfo$Payer_Type)
      
      subgroup.KPI <- list()
      
      join.variables <- c("patient_id","Valid_Index")
      
      subgroup.KPI <- lapply(payer.info, function(z) {
        
        PID_VID_Info <- subgroupinfo[subgroupinfo$Payer_Type %in% z,join.variables, with = FALSE]
        
        sub.data <- merge(x = data.for.match,y = PID_VID_Info,by = join.variables)
        
        obtainKPImodel(data = sub.data,solution = "TDS")
        
      })
      
      
      #KPI for Age>65
      age.subpop <- data.for.match[data.for.match$Age > 65,]
      
      age.subpopKPI <- obtainKPImodel(data = age.subpop,solution = "TDS")
      

      #Write KPI for all sub populations
      combine.KPI <- age.subpopKPI$`KPI Table`
      for(i in 1: length(subgroup.KPI))
      {
        combine.KPI <- cbind(combine.KPI,subgroup.KPI[[i]]$`KPI Table`)
      }
      
      names(combine.KPI) <- c("Model KPI for Age>65",paste("Model KPI on payer type",payer.info))
      combine.KPI$Metric <- rownames(combine.KPI)
      combine.KPI <- combine.KPI[,c(6,1:5)]
      
      #Writing results to file
      output.dir <- file.path(args[3],output.filename)
      
      utils::write.table(x = "Table: KPI for subpopulations",file = output.dir,append = FALSE,row.names = FALSE,col.names = FALSE)
      utils::write.table("\n",file=output.dir,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
    
      utils::write.table(x = combine.KPI,sep=",",file = output.dir,append = TRUE,row.names = FALSE,col.names = FALSE)
      utils::write.table("--------------------------------------",file=output.dir,sep=",",append=TRUE,row.names = FALSE,col.names = FALSE)
      
      
      #Writing confusion matrix for each subpopulation
      utils::write.table(x = "Table: Confusion Matrix for Age>65",file =output.dir,append = TRUE,row.names = FALSE,col.names = FALSE)
      utils::write.table(x = age.subpopKPI$`Confusion Matrix`,sep=",",file = output.dir,append = TRUE,row.names = TRUE,col.names = TRUE)
      
      i <- NULL
      for (i in 1:length(subgroup.KPI)) {
        
        utils::write.table(x = paste("Table: Confusion Matrix for payer type",payer.info[i]),file = output.dir,append = TRUE,row.names = FALSE,col.names = FALSE)
        
        # vec = c("Model Label",colnames(subgroup.KPI[[i]]$`Confusion Matrix`))
        # 
        # vec <- read.table(text="",col.names = vec)
        # 
        # utils::write.table(vec,file=output.dir,sep=",",append=TRUE,row.names = FALSE,col.names = TRUE)
        utils::write.table(x = subgroup.KPI[[i]]$`Confusion Matrix`,sep=",",file = output.dir,append = TRUE,row.names = TRUE,col.names = TRUE)
      }
      
      
      