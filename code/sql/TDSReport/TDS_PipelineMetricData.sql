
SET NOCOUNT ON
SELECT 
	 [Criteria] 
	,[Meets criteria Patient count n(%)] 
	,[Do not meet criteria Patient count n(%)] 
	,[Do not meet criteria Patient count n(%) with SHA] 
	,[Meets criteria Patient count n(%) with SHA] 
FROM [$(TargetSchema)].[TDS_PHCPipelineMetricReport] WITH(NOLOCK)
ORDER BY ID
--***********************************************************************************--