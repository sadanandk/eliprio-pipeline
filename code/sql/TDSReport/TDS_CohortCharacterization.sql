-- - Tables Required: [$(TargetSchema)].[app_tds_Cohort], [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] and [$(TargetSchema)].[app_tds_int_master_cohort_population]
-- - OutPut Table	: [$(TargetSchema)].[TDS_CohortCharacterization]
-- - Change History:	Change Date				Change Description
-- -					26-MAR-2019             Written code to generate cohort characterization report for TDS
-- -					12-Apr-2019			Written code  to add insurance type distribution_Patient
-- -                    23-Apr-2019			Updated code to make report in new format 
-- -					04-Jun-2019			Defect: PLATFORM-1541 : AED distribution counts not matching change table from all index to first index
-- -					06-Jun-2019			Rectify the Insurance type logic (taking counts of payer type for only AED at Index) , Considered original start date while calculating insurance type count.
 -- -					22-Aug-2019         Modified condition [Service_date] > [Valid_Index] for DX/RX and PX post index --condition updated as per shweta's suggestion 
 -- -					09-Oct-2019			Added Quartile range for Age in reports
 --						10-Oct-2019			Added comorbidity Psychoses and Depression
 --						07-Nov-2019			Logic to calculate Insurance type is changed as given in ticket PLATFORM-1946
 --						08-NOV-2019			filtered out only 16 AED while calculating Top Medication 
 ---------------------------------------------------------------------- 
--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_update_New_4_Nov

SET NOCOUNT ON

SELECT CURRENT_TIMESTAMP AS START_TIMESTAMP

DECLARE @TotalPatientCount INT;
DECLARE @PatientCount INT;
DECLARE @PercentagePatient DECIMAL(5, 2);
DECLARE @PatientPercentage DECIMAL(5, 2);
DECLARE @MAXID INT
DECLARE @PatientAgeMedian FLOAT;
DECLARE @PatientAgeMean FLOAT;
DECLARE @PatientAgeStdDev FLOAT;

IF OBJECT_ID('[$(TargetSchema)].[TDS_CohortCharacterization]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[TDS_CohortCharacterization]
CREATE TABLE [$(TargetSchema)].[TDS_CohortCharacterization]
(
 [Id] DECIMAL(5,1)
,[Variable] VARCHAR (50)
,[Label] VARCHAR (100)
,[Value] VARCHAR(50) 
,[% of PatientCount] float
)

IF OBJECT_ID('tempdb..#distribution_Patient') IS NOT NULL
	DROP TABLE #distribution_Patient

SELECT A.[patient_id]
	,B.[Age]
	,B.[Gender]
	,A.[State] as [Index_State]
INTO #distribution_Patient
FROM [$(TargetSchema)].[app_tds_Cohort] A 
inner JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index

SET @TotalPatientCount = (
		SELECT count(DISTINCT patient_id) AS TotalPatientCount
		FROM #distribution_Patient
		)

IF OBJECT_ID('tempdb..#Age') IS NOT NULL
	DROP TABLE #Age

SELECT Row_Number() OVER (
		ORDER BY age
		) AS ID
	,*
INTO #Age
FROM #distribution_Patient


SET @PatientAgeMean = (
		SELECT Sum(Age) / @TotalPatientCount
		FROM #distribution_Patient
		)

SET @PatientAgeStdDev = (
		SELECT STDEV(Age)
		FROM #distribution_Patient
		)


BEGIN 

DECLARE @MAXID_2 INT 
DECLARE @Lower_2_Min INT
DECLARE @Lower_2_Max INT
DECLARE @Upper_2_Min INT
DECLARE @Upper_2_Max INT
DECLARE @MAXID_2_Lower_Odd INT
DECLARE @Median_2_Lower_Odd Float
DECLARE @MAXID_2_Upper_Odd INT
DECLARE @Median_2_Upper_Odd Float
DECLARE @MAXID_2_Lower_Even INT
DECLARE @Median_2_Lower_Even Float
DECLARE @MAXID_2_Upper_Even INT
DECLARE @Median_2_Upper_Even Float
DECLARE @Age_Q1 Float
DECLARE @Age_Q3 Float
DECLARE @Median_2_All FLOAT




select @MAXID_2 = MAX(ID) from #Age
PRINT @MAXID_2
PRINT @MAXID_2%2

IF @MAXID_2%2 =0
		BEGIN
			   SET @Lower_2_Min = 1
			   SET @Lower_2_Max = @MAXID_2/2
			   SET @Upper_2_Min = (@MAXID_2/2)+1
			   SET @Upper_2_Max = @MAXID_2

			     SELECT @Median_2_All = AVG(CAST([age] AS FLOAT)) FROM #Age WHERE ID IN ((@MAXID_2/2),(@MAXID_2/2)+1)
				
				IF OBJECT_ID('tempdb..#LowerHalf_even') IS NOT NULL
				DROP TABLE #LowerHalf_even
			    SELECT  Row_NUmber()over (order by [age]) as ID_Lower,* INTO #LowerHalf_even FROM #Age WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max

				SELECT @MAXID_2_Lower_Even = MAX(ID_Lower) FROM #LowerHalf_even
				IF @MAXID_2_Lower_Even%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Even = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even/2),(@MAXID_2_Lower_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Even = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even+1)/2)


				IF OBJECT_ID('tempdb..#UpperHalf_even') IS NOT NULL
				DROP TABLE #UpperHalf_even
			    SELECT  Row_NUmber()over (order by [age]) as ID_Upper,* INTO #UpperHalf_even FROM #Age WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Even = MAX(ID_Upper) FROM #UpperHalf_even
				IF @MAXID_2_Upper_Even%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Even = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even/2),(@MAXID_2_Upper_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Even = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even+1)/2)
				

			SELECT @PatientAgeMedian = @Median_2_All , @Age_Q1 = @Median_2_Lower_Even ,@Age_Q3 = @Median_2_Upper_Even    
		END
ELSE 
		BEGIN 
			  SET @Lower_2_Min = 1
			  SET @Lower_2_Max = (@MAXID_2+1)/2
			  SET @Upper_2_Min = (@MAXID_2+1)/2
			  SET @Upper_2_Max = @MAXID_2

			   SELECT @Median_2_All = AVG(CAST([age] AS FLOAT)) FROM #Age WHERE ID IN ((@MAXID_2+1)/2)

			    IF OBJECT_ID('tempdb..#LowerHalf_odd') IS NOT NULL
				DROP TABLE #LowerHalf_odd
			    SELECT  Row_NUmber()over (order by [age]) as ID_Lower,* INTO #LowerHalf_odd FROM #Age WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max
				
				SELECT @MAXID_2_Lower_Odd = MAX(ID_Lower) FROM #LowerHalf_odd
				IF @MAXID_2_Lower_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Odd = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd/2),(@MAXID_2_Lower_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Odd = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd+1)/2)
				

			    IF OBJECT_ID('tempdb..#UpperHalf_odd') IS NOT NULL
				DROP TABLE #UpperHalf_odd
			    SELECT  Row_NUmber()over (order by [age]) as ID_Upper,* INTO #UpperHalf_odd FROM #Age WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Odd = MAX(ID_Upper) FROM #UpperHalf_odd
				IF @MAXID_2_Upper_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Odd = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd/2),(@MAXID_2_Upper_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Odd = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd+1)/2)
				
				
			SELECT @PatientAgeMedian = @Median_2_All , @Age_Q1 = @Median_2_Lower_Odd ,@Age_Q3 = @Median_2_Upper_Odd    
		END
		
END

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 1.0 AS [ID],'Total Patients in TDS Cohort (First IndexDate)' AS [Variable], NULL AS [Label], @TotalPatientCount as [Value], '100.00' AS  [% of PatientCount]
		
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 1.1 AS [ID],'"Age continuous"' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 1.2 AS [ID],NULL AS [Variable], 'Mean' AS [Label], CONVERT(Decimal(5,2),@PatientAgeMean) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 1.3 AS [ID],NULL AS [Variable], 'Median' AS [Label], CONVERT(Decimal(5,2),@PatientAgeMedian) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 1.4 AS [ID],NULL AS [Variable], 'SD' AS [Label], CONVERT(Decimal(5,2),@PatientAgeStdDev) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 1.5 AS [ID],NULL AS [Variable], 'Quartile' AS [Label], 'Q1 (25%)='+CAST(CAST(@Age_Q1 AS DECIMAL(5,2)) AS VARCHAR(50))+' ; Q3 (75%)='+CAST(CAST(@Age_Q3  AS DECIMAL(5,2)) AS VARCHAR(50)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 2.0 AS [ID],'Age category' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 
		CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN 2.1
		WHEN age BETWEEN 35
				AND 49
			THEN 2.2
		WHEN age BETWEEN 50
				AND 64
			THEN 2.3
		WHEN age >= 65
			THEN 2.4
		END AS [ID]
	   ,NULL AS [Variable]
	   ,CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN '18 - 34'
		WHEN age BETWEEN 35
				AND 49
			THEN '35 - 49'
		WHEN age BETWEEN 50
				AND 64
			THEN '50 - 64'
		WHEN age >= 65
			THEN '>=65'
		END AS [Label]
	,COUNT(DISTINCT Patient_id) 
	,cast(round(((COUNT(DISTINCT Patient_id) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #distribution_Patient
GROUP BY CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN 2.1
		WHEN age BETWEEN 35
				AND 49
			THEN 2.2
		WHEN age BETWEEN 50
				AND 64
			THEN 2.3
		WHEN age >= 65
			THEN 2.4
		END 
		,CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN '18 - 34'
		WHEN age BETWEEN 35
				AND 49
			THEN '35 - 49'
		WHEN age BETWEEN 50
				AND 64
			THEN '50 - 64'
		WHEN age >= 65
			THEN '>=65'
		END

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 3.0 AS [ID],'Gender' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 
		CASE 
		WHEN Gender='F'
		THEN 3.1
		WHEN Gender='M'
		THEN 3.2
		ELSE
		3.3
		END  AS [ID]
	   ,NULL AS [Variable]
	   ,CASE 
		WHEN Gender='F'
		THEN 'Female'
		WHEN Gender='M'
		THEN 'Male'
		ELSE
		'Unknown'
		END  AS [Label]
	   ,Count(DISTINCT patient_id)
	   ,cast(round(((COUNT(DISTINCT Patient_id) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #distribution_Patient P
GROUP BY Gender

/* AED Treatment */
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 4.0 AS [ID],'AED Treatment' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 4.1 AS [ID] 
	,NULL AS [Variable]
	,UPPER(LEFT(B.aed,1))+LOWER(SUBSTRING(B.aed, 2, LEN(B.aed)))AS [Label]
	,COUNT(DISTINCT A.patient_id) AS [Value]
	,CAST(ROUND(((COUNT(DISTINCT A.Patient_id) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] A WITH(NOLOCK)
LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] B WITH(NOLOCK)
ON A.AED = B.Abbreviation
GROUP BY B.aed


/* Insurance Type */
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 5.0 AS [ID],'Insurance (at Index)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]


IF OBJECT_ID('tempdb..#InsurnaceTypeDistribution') IS NOT NULL
	DROP TABLE #InsurnaceTypeDistribution

SELECT [Payer_Type],
COUNT([Patient_id]) AS [PatientCount]
INTO #InsurnaceTypeDistribution
FROM [$(TargetSchema)].[TDS_Payer_Type_PatientList]
WHERE [Index_ID] = 1
GROUP BY  [Payer_Type]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 
		CASE WHEN [Payer_Type] = 'Commercial'
			THEN '5.2'
		WHEN [Payer_Type] = 'Cash'
			THEN '5.1'
		WHEN [Payer_Type] = 'Medicare'
			THEN '5.4'
		WHEN [Payer_Type] = 'Medicaid'
			THEN '5.3'
		ELSE
			'5.5'
		END AS [ID]
	   ,NULL AS [Variable]
	   ,UPPER(LEFT([Payer_Type],1))+LOWER(SUBSTRING([Payer_Type], 2, LEN([Payer_Type]))) AS [Label]
	   ,[PatientCount] AS [Value]
	  ,cast(round(((PatientCount * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #InsurnaceTypeDistribution P

/* Prevelance of outcome variable*/
DECLARE @TotalRowCount INT;
SELECT @TotalRowCount = COUNT(1) 
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_input_FirstIndex] WITH (NOLOCK)
WHERE [Valid_Index] IS NOT NULL

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 6.0 AS [ID],'Distribution of outcome variables' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 6.1 AS [ID],'TDS solution' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT CASE 
		WHEN [Outcome_Variable] = 'Success'
			THEN 6.2
		ELSE 6.3
		END AS [ID]
	,NULL AS [Variable]
	,CASE WHEN [Outcome_Variable] = 'Success'
			THEN 'Stable'
		ELSE 'Unstable'
		END AS [Label]
	,COUNT(Outcome_Variable) as [Value] 
	,CAST(round(((COUNT(Outcome_Variable) * 100.0) / @TotalRowCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_input_FirstIndex] WITH (NOLOCK)
WHERE [Valid_Index] IS NOT NULL
GROUP BY [Outcome_Variable]

/* AED Generation */
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 7.0 AS [ID],'AED Generation (at Index)' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization] (
	[Id]
	,[Variable]
	,[Label]
	,[Value]
	,[% of PatientCount]
	)
SELECT CASE WHEN Abb.[AED_Generation]='First'
		THEN 7.1 
	WHEN Abb.[AED_Generation]='Second'
		THEN 7.2 
	WHEN Abb.[AED_Generation]='Third'
		THEN 7.3 
	 END AS [ID]
	,NULL AS [Variable]
	,CASE WHEN Abb.[AED_Generation]='First'
		THEN 'First'
	WHEN Abb.[AED_Generation]='Second'
		THEN 'Second' 
	WHEN Abb.[AED_Generation]='Third'
		THEN 'Third'
	 END AS [Label]
	,COUNT(DISTINCT AD.patient_id) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT AD.[patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] AD WITH (NOLOCK)
INNER JOIN [$(TargetSchema)].[app_tds_int_Regiments_Treatment] RG(NOLOCK)
	ON AD.patient_id = RG.patient_id
		AND AD.Valid_Index = RG.Start_point
LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] Abb
	ON RG.Abbreviation = Abb.Abbreviation
GROUP BY Abb.AED_Generation
		
/* CCI Score*/
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 8.0 AS [ID],'Charlson Comorbidity Index' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

IF OBJECT_ID('tempdb..#PatientCCIScore') IS NOT NULL
	DROP TABLE #PatientCCIScore

SELECT CI.CCI_Score AS [CCIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
INTO #PatientCCIScore
FROM [$(TargetSchema)].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN  [$(TargetSchema)].[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
GROUP BY CI.CCI_Score

UPDATE #PatientCCIScore
SET CCIScore = 5
WHERE CCIScore >= 5

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization] (
	[Id]
	,[Variable]
	,[Label]
	,[Value]
	,[% of PatientCount]
	)
SELECT CASE 
		WHEN CCIScore = 0
			THEN 8.1
		WHEN CCIScore = 1
			THEN 8.2
		WHEN CCIScore = 2
			THEN 8.3
		WHEN CCIScore = 3
			THEN 8.4
		WHEN CCIScore = 4
			THEN 8.5
		WHEN CCIScore = 5
			THEN 8.6
		END AS  [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN CCIScore = 0
			THEN '0'
		WHEN CCIScore = 1
			THEN '1'
		WHEN CCIScore = 2
			THEN '2'
		WHEN CCIScore = 3
			THEN '3'
		WHEN CCIScore = 4
			THEN '4'
		WHEN CCIScore = 5
			THEN '>=5'
		END AS [Charlson Comorbidity Index (CCI)]
	,SUM([PatientCount]) AS [PatientCount]
	,CAST(ROUND(((SUM([PatientCount]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [PatientPercentage]
FROM #PatientCCIScore
GROUP BY CCIScore


SELECT @TotalPatientCount = COUNT(DISTINCT A.Patient_ID)
FROM [$(TargetSchema)].[app_tds_Cohort] A 
inner JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index

IF OBJECT_ID('tempdb..#PatientCohortComb') IS NOT NULL
	DROP TABLE #PatientCohortComb

SELECT Patient_ID
	,Valid_Index
	,Comorbidity
	,ComorbidityValue
INTO #PatientCohortComb
FROM (
	SELECT CI.[patient_id]
		,CI.Valid_Index
		,[Myocardial_Infarction]
		,[Hemiplegia_or_Paraplegia]
		,[Mod_Severe_liver_Disease]
		,[Cerebrovascular_Disease]
		,[Anoxic_Brain_Injury]
		,[Metastatic_Solid_Tumor]
		,[Cardiac_Arrhythmias]
		,[Peripheral_Vascular_Disease]
		,[Mod_Severe_Renal_Disease]
		,[Congestive_Heart_Failure]
		,[Chronic_Pulmonary_Disease]
		,[AIDS_HIV]
		,[Dementia]
		,[Diabetes_With_Organ_Damage]
		,[Pulmonary_Circulation_Disorders]
		,[Hypertension]
		,[Aspiration_Pneumonia]
		,[Brain_Tumor]
		,[Solid_Tumor_Without_Metastases]
		,[Peptic_Ulcer_Disease]
		,[Connective_Tissue_Disease]
		,[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]
		,[Diabetes_Without_Organ_Damage]
		,[Mild_Liver_Disease]
		,[Depression]
		,[Psychoses]
	FROM [$(TargetSchema)].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
	INNER JOIN  [$(TargetSchema)].[app_tds_Cohort] A WITH(NOLOCK)
	ON CI.Patient_ID = A.Patient_ID
	AND CI.Valid_index = A.INDEX_DATE
	INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
	ON A.patient_id = B.patient_id
	AND A.INDEX_DATE = B.Valid_index
	) p
UNPIVOT(ComorbidityValue FOR Comorbidity IN (
			[Myocardial_Infarction]
			,[Hemiplegia_or_Paraplegia]
			,[Mod_Severe_liver_Disease]
			,[Cerebrovascular_Disease]
			,[Anoxic_Brain_Injury]
			,[Metastatic_Solid_Tumor]
			,[Cardiac_Arrhythmias]
			,[Peripheral_Vascular_Disease]
			,[Mod_Severe_Renal_Disease]
			,[Congestive_Heart_Failure]
			,[Chronic_Pulmonary_Disease]
			,[AIDS_HIV]
			,[Dementia]
			,[Diabetes_With_Organ_Damage]
			,[Pulmonary_Circulation_Disorders]
			,[Hypertension]
			,[Aspiration_Pneumonia]
			,[Brain_Tumor]
			,[Solid_Tumor_Without_Metastases]
			,[Peptic_Ulcer_Disease]
			,[Connective_Tissue_Disease]
			,[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]
			,[Diabetes_Without_Organ_Damage]
			,[Mild_Liver_Disease]
			,[Depression]
			,[Psychoses]
			)) AS unpvt;


IF OBJECT_ID('tempdb..#PatientComorbidityDistribution') IS NOT NULL
	DROP TABLE #PatientComorbidityDistribution

SELECT 9.1 AS [ID]
	,NULL AS [Variable]
	,UPPER(LEFT(Comorbidity,1))+LOWER(SUBSTRING(Comorbidity, 2, LEN(Comorbidity))) AS [Label]
	,Count(DISTINCT Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT [Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #PatientComorbidityDistribution
FROM #PatientCohortComb
WHERE ComorbidityValue = 1
GROUP BY Comorbidity

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 9.0 AS [ID],'Top 10 Co morbidities*' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

IF OBJECT_ID('tempdb..#Top10PatientComorbidityDistribution') IS NOT NULL
	DROP TABLE #Top10PatientComorbidityDistribution

SELECT TOP 10 *
INTO #Top10PatientComorbidityDistribution
FROM #PatientComorbidityDistribution
ORDER BY PatientCount DESC, [Label] ASC

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT *
FROM #Top10PatientComorbidityDistribution

/* Comedication Report*/
SELECT @TotalPatientCount = COUNT(DISTINCT B.Patient_ID)
FROM [$(TargetSchema)].[app_tds_Cohort] A 
inner JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index

IF OBJECT_ID('tempdb..#PatientUSCCohort') IS NOT NULL
DROP TABLE #PatientUSCCohort

SELECT 9.1 as [ID], 'USC Group' AS [Variable],UPPER(LEFT(C.USC_Name,1))+LOWER(SUBSTRING(C.USC_Name, 2, LEN(C.USC_Name)))AS [Label]
,COUNT(DISTINCT A.Patient_Id) as [PatientCount]
,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #PatientUSCCohort
FROM [$(TargetSchema)].[app_tds_Cohort] A 
INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  BI WITH (NOLOCK)
ON A.patient_id = BI.patient_id
AND A.INDEX_DATE = BI.Valid_index
LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
ON BI.patient_id=B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.INDEX_DATE) AND DATEADD(DD,-1,A.INDEX_DATE) 
AND B.Days_Supply > 0
LEFT JOIN  [$(SrcSchema)].[src_ref_Product](NOLOCK) C
ON B.NDC = C.NDC
GROUP BY C.USC_Name


IF OBJECT_ID('tempdb..#PatientComedicationCohortWithoutAED') IS NOT NULL
DROP TABLE #PatientComedicationCohortWithoutAED

SELECT 10.1 as [ID], NULL AS [Variable], UPPER(LEFT(C.USC_Name,1))+LOWER(SUBSTRING(C.USC_Name, 2, LEN(C.USC_Name)))AS [Label]
,COUNT(DISTINCT A.Patient_Id) as [PatientCount]
,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #PatientComedicationCohortWithoutAED
FROM [$(TargetSchema)].[app_tds_Cohort] A 
INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  BI WITH (NOLOCK)
ON A.patient_id = BI.patient_id
AND A.INDEX_DATE = BI.Valid_index
LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
ON  BI.patient_id = B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.INDEX_DATE) AND DATEADD(DD,-1,A.INDEX_DATE)  
AND B.Days_Supply > 0
LEFT JOIN  [$(SrcSchema)].[src_ref_Product](NOLOCK) C
ON B.NDC = C.NDC
WHERE C.[GENERIC_NAME] NOT IN
	(
'CARBAMAZEPINE'
,'CARBAMAZEPINE (ANTIPSYCHOTIC)'
,'CLOBAZAM'
,'DIVALPROEX SODIUM'
,'ESLICARBAZEPINE ACETATE'
,'ETHOSUXIMIDE'
,'ETHOSUXIMIDE (BULK)'
,'FOSPHENYTOIN SODIUM'
,'LACOSAMIDE'
,'LAMOTRIGINE'
,'LAMOTRIGINE (BULK)'
,'LEVETIRACETAM'
,'LEVETIRACETAM (BULK)'
,'LEVETIRACETAM IN NACL (ISO-OS)'
,'LEVETIRACETAM IN SODIUM CHLORIDE'
,'OXCARBAZEPINE'
,'PERAMPANEL'
,'PHENOBARBITAL'
,'PHENOBARBITAL SODIUM'
,'PHENTERMINE/TOPIRAMATE'
,'PHENYTOIN'
,'PHENYTOIN (BULK)'
,'PHENYTOIN SODIUM'
,'PHENYTOIN SODIUM EXTENDED'
,'PHENYTOIN SODIUM PROMPT'
,'PREGABALIN'
,'RUFINAMIDE'
,'TOPIRAMATE'
,'TOPIRAMATE (BULK)'
,'VALPROATE SODIUM'
,'VALPROATE SODIUM (BULK)'
,'VALPROIC ACID'
,'VALPROIC ACID (AS SODIUM SALT)'
,'VALPROIC ACID (BULK)'
,'ZONISAMIDE'
,'ZONISAMIDE (BULK)'

)
GROUP BY C.USC_Name

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 10.0 AS [ID],'Top 10 Medication*' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

IF OBJECT_ID('tempdb..#Top10PatientComedicationCohortWithoutAED') IS NOT NULL
DROP TABLE #Top10PatientComedicationCohortWithoutAED

SELECT TOP 10 * 
INTO #Top10PatientComedicationCohortWithoutAED
FROM #PatientComedicationCohortWithoutAED
ORDER BY PatientCount DESC, [Label] ASC

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization] 
SELECT * FROM #Top10PatientComedicationCohortWithoutAED

/* TDS Clinical Stats Report */

SELECT @TotalPatientCount = COUNT(DISTINCT Patient_ID)
FROM [$(TargetSchema)].[app_tds_Cohort] WITH (NOLOCK)
WHERE [INdex_Date] IS NOT NULL
	AND GenPop = 1

IF OBJECT_ID('tempdb..#Patient_Valid_Index') IS NOT NULL
	DROP TABLE #Patient_Valid_Index

SELECT patient_id
	,MIN(Index_date) AS Valid_Index
INTO #Patient_Valid_Index
FROM [$(TargetSchema)].[app_tds_cohort](NOLOCK) A
WHERE Index_date IS NOT NULL
	AND GenPop = 1
GROUP BY patient_Id

/* Generate Cohort Table */
IF OBJECT_ID('tempdb..#PatientCohort') IS NOT NULL
	DROP TABLE #PatientCohort

SELECT A.Patient_ID
	,A.INDEX_DATE AS [Valid_Index]
INTO [#PatientCohort]
FROM [$(TargetSchema)].[app_tds_Cohort] A WITH(NOLOCK)
INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index

/* Count of Rx prescription of Patients for Pre-Index */
/* Count of Rx prescription for Patients */
IF OBJECT_ID('tempdb..#Rx_Density') IS NOT NULL
	DROP TABLE #Rx_Density

SELECT PC.Patient_ID
	,COUNT(DISTINCT NDC) AS [Rx_Num]
INTO #Rx_Density
FROM  [#PatientCohort] PC WITH(NOLOCK)
LEFT JOIN [$(SrcSchema)].[src_rx_claims] SR WITH(NOLOCK)
	ON SR.[Patient_ID] = PC.[Patient_ID]
    AND SR.[Service_date] BETWEEN DATEADD(DD, - 365, PC.[Valid_Index])
		AND DATEADD(DD, - 1, PC.[Valid_Index])
		AND SR.Days_Supply > 0
Group by PC.Patient_ID

/* Inserting dummy enteries for getting all the group */
Insert into #Rx_Density
Select 
'-1' as [Patient_ID]
,0 as [Rx_Num]

Insert into #Rx_Density
Select 
'-1' as [Patient_ID]
,1 as [Rx_Num]

Insert into #Rx_Density
Select 
'-1' as [Patient_ID]
,6 as [Rx_Num]

Insert into #Rx_Density
Select 
'-1' as [Patient_ID]
,11 as [Rx_Num]

/* Count of Rx prescription of Patients for Post-Index */
/* Count of Rx prescription for Patients */
IF OBJECT_ID('tempdb..#Rx_DensityPostIndex') IS NOT NULL
	DROP TABLE #Rx_DensityPostIndex

SELECT PC.Patient_ID
	,COUNT(DISTINCT NDC) AS [Rx_Num]
INTO #Rx_DensityPostIndex
FROM  [#PatientCohort] PC WITH(NOLOCK)
LEFT JOIN [$(SrcSchema)].[src_rx_claims] SR WITH(NOLOCK)
	ON SR.[Patient_ID] = PC.[Patient_ID]
    AND SR.[Service_date] > PC.[Valid_Index]---condition updated as per shweta's suggestion 
	AND SR.Days_Supply > 0
Group by PC.Patient_ID

/* Inserting dummy enteries for getting all the group */
Insert into #Rx_DensityPostIndex
Select 
'-1' as [Patient_ID]
,0 as [Rx_Num]

Insert into #Rx_DensityPostIndex
Select 
'-1' as [Patient_ID]
,1 as [Rx_Num]

Insert into #Rx_DensityPostIndex
Select 
'-1' as [Patient_ID]
,6 as [Rx_Num]

Insert into #Rx_DensityPostIndex
Select 
'-1' as [Patient_ID]
,11 as [Rx_Num]


IF OBJECT_ID('tempdb..#DX_Base') IS NOT NULL
	DROP TABLE #DX_Base

SELECT *
INTO #DX_Base
FROM [$(SrcSchema)].[src_dx_claims] WITH (NOLOCK)
WHERE [PROC_CD] IS NOT NULL

/* Count of Dx prescription of Patients for Pre-Index */

IF OBJECT_ID('tempdb..#Dx_Density') IS NOT NULL
	DROP TABLE #Dx_Density

SELECT PC.Patient_ID
	,COUNT(DISTINCT DX_Codes) AS [Dx_Num]
INTO #Dx_Density
FROM [#PatientCohort] PC
LEFT JOIN [$(TargetSchema)].[app_tds_int_DX_8_to_1_Claims_Pivot] DB with(nolock)
	ON DB.[Patient_ID] = PC.[Patient_ID]
	AND DB.[Service_date] BETWEEN DATEADD(DD, - 365, PC.[Valid_Index])
		AND DATEADD(DD, - 1, PC.[Valid_Index])
GROUP BY PC.[Patient_ID]


/* Inserting dummy enteries for getting all the group */
Insert into #Dx_Density
Select 
'-1' as [Patient_ID]
,0 as [Dx_Num]

Insert into #Dx_Density

Select 
'-1' as [Patient_ID]
,1 as [Dx_Num]

Insert into #Dx_Density
Select 
'-1' as [Patient_ID]
,6 as [Dx_Num]

Insert into #Dx_Density
Select 
'-1' as [Patient_ID]
,11 as [Dx_Num]


/* Count of Dx prescription of Patients for Post-Index */
IF OBJECT_ID('tempdb..#Dx_DensityPostIndex') IS NOT NULL
	DROP TABLE #Dx_DensityPostIndex

SELECT PC.Patient_ID
	,COUNT(DISTINCT DX_Codes) AS [Dx_Num]
INTO #Dx_DensityPostIndex
FROM [#PatientCohort] PC
LEFT JOIN [$(TargetSchema)].[app_tds_int_DX_8_to_1_Claims_Pivot] DB with(nolock)
	ON DB.[Patient_ID] = PC.[Patient_ID]
    AND DB.[Service_date] > PC.[Valid_Index]---condition updated as per shweta's suggestion 
GROUP BY PC.[Patient_ID]


/* Inserting dummy enteries for getting all the group */
Insert into #Dx_DensityPostIndex
Select 
'-1' as [Patient_ID]
,0 as [Dx_Num]

Insert into #Dx_DensityPostIndex

Select 
'-1' as [Patient_ID]
,1 as [Dx_Num]

Insert into #Dx_DensityPostIndex
Select 
'-1' as [Patient_ID]
,6 as [Dx_Num]

Insert into #Dx_DensityPostIndex
Select 
'-1' as [Patient_ID]
,11 as [Dx_Num]

/* Count of Procedure prescription of Patients for Pre-Index */
IF OBJECT_ID('tempdb..#Px_Density') IS NOT NULL
	DROP TABLE #Px_Density

SELECT PC.Patient_ID
	,COUNT(DISTINCT  DB.[PROC_CD]) AS [Proc_Num]
INTO #Px_Density
FROM [#PatientCohort] PC
LEFT JOIN #DX_Base DB
	ON DB.[Patient_ID] = PC.[Patient_ID]
	AND DB.[Service_date] BETWEEN DATEADD(DD, - 365, PC.[Valid_Index])
		AND DATEADD(DD, - 1, PC.[Valid_Index])
	AND DB.DIAG1 IS NULL
	AND DB.[PROC_CD] IS NOT NULL
GROUP BY PC.[Patient_ID]


/* Inserting dummy enteries for getting all the group */
Insert into #Px_Density
Select 
'-1' as [Patient_ID]
,0 as [Proc_Num]

Insert into #Px_Density
Select 
'-1' as [Patient_ID]
,1 as [Proc_Num]

Insert into #Px_Density
Select 
'-1' as [Patient_ID]
,6 as [Proc_Num]

Insert into #Px_Density
Select 
'-1' as [Patient_ID]
,11 as [Proc_Num]

/* Count of Procedure prescription of Patients for Post-Index */
IF OBJECT_ID('tempdb..#Px_DensityPostIndex') IS NOT NULL
	DROP TABLE #Px_DensityPostIndex

SELECT PC.Patient_ID
	,COUNT(DISTINCT  DB.[PROC_CD]) AS [Proc_Num]
INTO #Px_DensityPostIndex
FROM [#PatientCohort] PC
LEFT JOIN #DX_Base DB
	ON DB.[Patient_ID] = PC.[Patient_ID]
    AND DB.[Service_date] > PC.[Valid_Index]---condition updated as per shweta's suggestion 
	AND DB.DIAG1 IS NULL
	AND DB.[PROC_CD] IS NOT NULL
GROUP BY PC.[Patient_ID]

/* Inserting dummy enteries for getting all the group */
Insert into #Px_DensityPostIndex
Select 
'-1' as [Patient_ID]
,0 as [Proc_Num]

Insert into #Px_DensityPostIndex
Select 
'-1' as [Patient_ID]
,1 as [Proc_Num]

Insert into #Px_DensityPostIndex
Select 
'-1' as [Patient_ID]
,6 as [Proc_Num]

Insert into #Px_DensityPostIndex
Select 
'-1' as [Patient_ID]
,11 as [Proc_Num]

SET @TotalPatientCount = (
		SELECT count(DISTINCT PATIENT_ID) AS TotalPatientCount
		FROM #PatientCohort
		)

/* Insert data for Rx Pre-Index */
IF OBJECT_ID('tempdb..#Rx_DensityInter') IS NOT NULL
	DROP TABLE #Rx_DensityInter

SELECT CASE 
		WHEN Rx_Num = 0
			THEN 12.1
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN 12.2
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN 12.3
		WHEN Rx_Num > 10
			THEN 12.4
		END AS [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN Rx_Num = 0
			THEN ' 0 '
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN Rx_Num > 10
			THEN ' >10 '
		END AS [Label]
	,(COUNT(RD.[Patient_ID])-1) AS [PatientCount]
	,CAST(ROUND((((COUNT(RD.[Patient_id])-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #Rx_DensityInter
FROM #Rx_Density RD
GROUP BY CASE 
		WHEN Rx_Num = 0
			THEN 12.1
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN 12.2
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN 12.3
		WHEN Rx_Num > 10
			THEN 12.4
		END 
		,CASE 
		WHEN Rx_Num = 0
			THEN ' 0 '
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN Rx_Num > 10
			THEN ' >10 '
		END

/* Insert data for Rx Post-Index */
IF OBJECT_ID('tempdb..#Rx_DensityInterPostIndex') IS NOT NULL
	DROP TABLE #Rx_DensityInterPostIndex

SELECT CASE 
		WHEN Rx_Num = 0
			THEN 15.1
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN 15.2
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN 15.3
		WHEN Rx_Num > 10
			THEN 15.4
		END AS [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN Rx_Num = 0
			THEN ' 0 '
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN Rx_Num > 10
			THEN ' >10 '
		END AS [Label]
	,(COUNT(RD.[Patient_ID])-1) AS [PatientCount]
	,CAST(ROUND((((COUNT(RD.[Patient_id])-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #Rx_DensityInterPostIndex
FROM #Rx_DensityPostIndex RD
GROUP BY CASE 
		WHEN Rx_Num = 0
			THEN 15.1
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN 15.2
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN 15.3
		WHEN Rx_Num > 10
			THEN 15.4
		END 
		,CASE 
		WHEN Rx_Num = 0
			THEN ' 0 '
		WHEN Rx_Num BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN Rx_Num BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN Rx_Num > 10
			THEN ' >10 '
		END
		
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 12.0 AS [ID],'Number of Rx records (Pre-Index) ' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 15.0 AS [ID],'Number of Rx records (Post-Index) ' AS [Variable], NULL AS [Label], NULL AS [Value], NULL 

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT *
FROM #Rx_DensityInter RD

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT *
FROM #Rx_DensityInterPostIndex RD

/* Insert rows for DX Pre-Index */
IF OBJECT_ID('tempdb..#Dx_DensityInter') IS NOT NULL
	DROP TABLE #Dx_DensityInter

SELECT 	 CASE 
		WHEN [Dx_Num] = 0
			THEN 11.1
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN 11.2
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN 11.3
		WHEN [Dx_Num] > 10
			THEN 11.4
		END AS [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN [Dx_Num] = 0
			THEN ' 0 '
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Dx_Num] > 10
			THEN ' >10 '
		END AS [Label]
	,(COUNT(DD.Patient_ID)-1) AS [PatientCount]
	,CAST(ROUND((((COUNT(DD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #Dx_DensityInter
FROM #Dx_Density DD
GROUP BY 	 CASE 
		WHEN [Dx_Num] = 0
			THEN 11.1
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN 11.2
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN 11.3
		WHEN [Dx_Num] > 10
			THEN 11.4
		END 
		,CASE 
		WHEN [Dx_Num] = 0
			THEN ' 0 '
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Dx_Num] > 10
			THEN ' >10 '
		END

/* Insert rows for DX Post-Index */
IF OBJECT_ID('tempdb..#Dx_DensityInterPostIndex') IS NOT NULL
	DROP TABLE #Dx_DensityInterPostIndex

SELECT 	 CASE 
		WHEN [Dx_Num] = 0
			THEN 14.1
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN 14.2
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN 14.3
		WHEN [Dx_Num] > 10
			THEN 14.4
		END AS [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN [Dx_Num] = 0
			THEN ' 0 '
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Dx_Num] > 10
			THEN ' >10 '
		END AS [Label]
	,(COUNT(DD.Patient_ID)-1) AS [PatientCount]
	,CAST(ROUND((((COUNT(DD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #Dx_DensityInterPostIndex
FROM #Dx_DensityPostIndex DD
GROUP BY 	 CASE 
		WHEN [Dx_Num] = 0
			THEN 14.1
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN 14.2
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN 14.3
		WHEN [Dx_Num] > 10
			THEN 14.4
		END 
		,CASE 
		WHEN [Dx_Num] = 0
			THEN ' 0 '
		WHEN [Dx_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Dx_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Dx_Num] > 10
			THEN ' >10 '
		END

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 11.0 AS [ID],'Number of Dx records (Pre-Index) ' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 14.0 AS [ID],'Number of Dx records (Post-Index)' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT *
FROM #Dx_DensityInter PD

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT *
FROM #Dx_DensityInterPostIndex PD

/* Insert rows for Proc pre-Index */
IF OBJECT_ID('tempdb..#Px_DensityInter') IS NOT NULL
	DROP TABLE #Px_DensityInter

SELECT CASE 
		WHEN [Proc_Num] = 0
			THEN 13.1
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN 13.2
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN 13.3
		WHEN [Proc_Num] > 10
			THEN 13.4
		END AS [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN [Proc_Num] = 0
			THEN ' 0 '
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Proc_Num] > 10
			THEN ' >10 '
		END AS [Label]
	,(COUNT(PD.Patient_ID)-1) AS [PatientCount]
	,CAST(ROUND((((COUNT(PD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #Px_DensityInter
FROM #Px_Density PD
GROUP BY CASE 
		WHEN [Proc_Num] = 0
			THEN 13.1
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN 13.2
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN 13.3
		WHEN [Proc_Num] > 10
			THEN 13.4
		END
		,CASE 
		WHEN [Proc_Num] = 0
			THEN ' 0 '
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Proc_Num] > 10
			THEN ' >10 '
		END

/* Insert rows for Proc pre-Index */
IF OBJECT_ID('tempdb..#Px_DensityInterPostIndex') IS NOT NULL
	DROP TABLE #Px_DensityInterPostIndex

SELECT CASE 
		WHEN [Proc_Num] = 0
			THEN 16.1
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN 16.2
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN 16.3
		WHEN [Proc_Num] > 10
			THEN 16.4
		END AS [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN [Proc_Num] = 0
			THEN ' 0 '
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Proc_Num] > 10
			THEN ' >10 '
		END AS [Label]
	,(COUNT(PD.Patient_ID)-1) AS [PatientCount]
	,CAST(ROUND((((COUNT(PD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #Px_DensityInterPostIndex
FROM #Px_DensityPostIndex PD
GROUP BY CASE 
		WHEN [Proc_Num] = 0
			THEN 16.1
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN 16.2
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN 16.3
		WHEN [Proc_Num] > 10
			THEN 16.4
		END
		,CASE 
		WHEN [Proc_Num] = 0
			THEN ' 0 '
		WHEN [Proc_Num] BETWEEN 1
				AND 5
			THEN ' 1-5 '
		WHEN [Proc_Num] BETWEEN 6
				AND 10
			THEN ' 6-10 '
		WHEN [Proc_Num] > 10
			THEN ' >10 '
		END
		
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 13.0 AS [ID],'Number of Procedures (Pre-Index) ' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT *
FROM #Px_DensityInter PD

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 16.0 AS [ID],'Number of Procedures (Post-Index) ' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT *
FROM #Px_DensityInterPostIndex PD

/* Data Avaibility Report */

IF OBJECT_ID('tempdb..#PreIndexPatient') IS NOT NULL
	DROP TABLE #PreIndexPatient
SELECT Patient_ID, MIN(Service_Date) as [Service_Date]
INTO #PreIndexPatient
FROM [$(SrcSchema)].[src_rx_claims] SR WITH(NOLOCK)
WHERE SR.Days_Supply > 0
GROUP BY Patient_ID
UNION ALL
SELECT Patient_ID, MIN(Service_Date) as [Service_Date]
FROM [$(SrcSchema)].[src_dx_claims] WITH(NOLOCK)
GROUP BY Patient_ID

IF OBJECT_ID('tempdb..#PreIndexPatientInter') IS NOT NULL
	DROP TABLE #PreIndexPatientInter
SELECT Patient_ID, MIN([Service_Date]) as [Service_Date]
INTO #PreIndexPatientInter
FROM #PreIndexPatient
GROUP BY Patient_ID

IF OBJECT_ID('tempdb..#PostIndexPatient') IS NOT NULL
	DROP TABLE #PostIndexPatient
SELECT Patient_ID, MAX([Service_Date]) as [Service_Date]
INTO #PostIndexPatient
FROM [$(SrcSchema)].[src_rx_claims] SR WITH(NOLOCK)
WHERE SR.Days_Supply > 0
GROUP BY Patient_ID
UNION ALL
SELECT Patient_ID, MAX([Service_Date]) as [Service_Date]
FROM [$(SrcSchema)].[src_dx_claims] WITH(NOLOCK)
GROUP BY Patient_ID

IF OBJECT_ID('tempdb..#PostIndexPatientInter') IS NOT NULL
	DROP TABLE #PostIndexPatientInter
SELECT Patient_ID, MAX([Service_Date]) as [Service_Date]
INTO #PostIndexPatientInter
FROM #PostIndexPatient
GROUP BY Patient_ID

/* Get Month between Index Date and First Date of Rx, Dx */
IF OBJECT_ID('tempdb..#PreIndexPatientCohort') IS NOT NULL
	DROP TABLE #PreIndexPatientCohort
SELECT PC.PATIENT_ID,PC.[VALID_INDEX],Pre.[Service_Date],DATEDIFF(MM, Pre.[Service_Date], PC.[VALID_INDEX]) AS [MOS]
INTO #PreIndexPatientCohort
FROM [#PatientCohort] PC WITH(NOLOCK)
INNER JOIN #PreIndexPatientInter Pre WITH(NOLOCK)
ON PC.[Patient_ID] = Pre.[Patient_ID]

/* Get Month between Index Date and Last Date of Rx, Dx */
IF OBJECT_ID('tempdb..#PostIndexPatientCohort') IS NOT NULL
	DROP TABLE #PostIndexPatientCohort
SELECT PC.PATIENT_ID,PC.[VALID_INDEX],POS.[Service_Date],DATEDIFF(MM, PC.[VALID_INDEX],POS.[Service_Date]) AS [MOS]
INTO #PostIndexPatientCohort
FROM [#PatientCohort] PC WITH(NOLOCK)
INNER JOIN #PostIndexPatientInter POS WITH(NOLOCK)
ON PC.[Patient_ID] = POS.[Patient_ID]

DECLARE @PreIndexMIN INT;
DECLARE @PreIndexMAX INT;
DECLARE @PreIndexMEAN DECIMAL (5,2);
DECLARE @PreIndexMEDIAN DECIMAL (5,2);
DECLARE @PreIndexSD DECIMAL(5,2);

DECLARE @PostIndexMIN INT;
DECLARE @PostIndexMAX INT;
DECLARE @PostIndexMEAN DECIMAL (5,2);
DECLARE @PostIndexMEDIAN DECIMAL (5,2);
DECLARE @PostIndexSD DECIMAL(5,2);

SELECT @PreIndexMIN = MIN([MOS]) FROM #PreIndexPatientCohort;

SELECT @PreIndexMAX = MAX([MOS]) FROM #PreIndexPatientCohort;

SELECT @PreIndexSD = STDEV([MOS]) FROM #PreIndexPatientCohort;

SELECT @PostIndexMIN = MIN([MOS]) FROM #PostIndexPatientCohort;

SELECT @PostIndexMAX = MAX([MOS]) FROM #PostIndexPatientCohort;

SELECT @PostIndexSD = STDEV([MOS]) FROM #PostIndexPatientCohort;

SET @TotalPatientCount = (
		SELECT count(DISTINCT PATIENT_ID) AS TotalPatientCount
		FROM #PatientCohort
		)

/* Mean, Median and Standard deviation distribution for pre index observation */
IF OBJECT_ID('tempdb..#PreIndexPatientStats') IS NOT NULL
	DROP TABLE #PreIndexPatientStats

SELECT Row_Number() OVER (
		ORDER BY [MOS]
		) AS ID
	,*
INTO #PreIndexPatientStats
FROM #PreIndexPatientCohort

SELECT @MAXID = MAX(ID)
FROM #PreIndexPatientStats

IF (@MAXID % 2 = 0)
BEGIN
	SELECT @PreIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
	FROM #PreIndexPatientStats
	WHERE ID IN (
			(@MAXID / 2)
			,(@MAXID / 2) + 1
			)
END
ELSE
BEGIN
	SELECT @PreIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
	FROM #PreIndexPatientStats
	WHERE ID IN ((@MAXID + 1) / 2)
END

SET @PreIndexMEAN = (
		SELECT Sum([MOS]) / @TotalPatientCount
		FROM #PreIndexPatientStats
		)

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 17.0 AS [ID],'Pre-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 17.1 AS [ID],NULL AS [Variable], 'n' AS [Label], @TotalPatientCount as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 17.2 AS [ID],NULL AS [Variable], 'Mean' AS [Label], CAST(@PreIndexMEAN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 17.3 AS [ID],NULL AS [Variable], 'Median' AS [Label], CAST(@PreIndexMEDIAN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 17.4 AS [ID],NULL AS [Variable], 'SD' AS [Label], CAST(@PreIndexSD AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 17.5 AS [ID],NULL AS [Variable], 'Min' AS [Label], CAST(@PreIndexMIN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 17.6 AS [ID], NULL AS [Variable], 'Max' AS [Label], CAST(@PreIndexMAX AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 18.0 AS [ID],'Pre-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]


/* Inserting dummy enteries for getting all the group */
Insert into #PreIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,12 as [MOS]

Insert into #PreIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,16 as [MOS]

Insert into #PreIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,19 as [MOS]

Insert into #PreIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,25 as [MOS]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN 18.1
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN 18.2
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN 18.3
		WHEN [MOS] > 24 
			THEN 18.4
		END AS [ID]
      ,NULL AS [Variable]
	,CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN ' 12 - 15 '
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN ' 16 - 18 '
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN ' 19 - 24 '
		WHEN [MOS] > 24 
			THEN ' 24+ '
		END as [Label]
	,(COUNT(DISTINCT Patient_id)-1) AS PatientCount
	,CAST(ROUND((((COUNT(DISTINCT Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #PreIndexPatientCohort
GROUP BY 	CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN 18.1
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN 18.2
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN 18.3
		WHEN [MOS] > 24 
			THEN 18.4
		END
		,CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN ' 12 - 15 '
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN ' 16 - 18 '
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN ' 19 - 24 '
		WHEN [MOS] > 24 
			THEN ' 24+ '
		END
	
/* Mean, Median and Standard deviation distribution for post index observation */
IF OBJECT_ID('tempdb..#PostIndexPatientStats') IS NOT NULL
	DROP TABLE #PostIndexPatientStats

SELECT Row_Number() OVER (
		ORDER BY [MOS]
		) AS ID
	,*
INTO #PostIndexPatientStats
FROM #PostIndexPatientCohort

SELECT @MAXID = MAX(ID)
FROM #PostIndexPatientStats

IF (@MAXID % 2 = 0)
BEGIN
	SELECT @PostIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
	FROM #PostIndexPatientStats
	WHERE ID IN (
			(@MAXID / 2)
			,(@MAXID / 2) + 1
			)
END
ELSE
BEGIN
	SELECT @PostIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
	FROM #PostIndexPatientStats
	WHERE ID IN ((@MAXID + 1) / 2)
END

SET @PostIndexMEAN = (
		SELECT Sum([MOS]) / @TotalPatientCount
		FROM #PostIndexPatientStats
		)

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 19.0 AS [ID],'Post-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 19.1 AS [ID],NULL AS [Variable], 'n' AS [Label], @TotalPatientCount as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 19.2 AS [ID],NULL AS [Variable], 'Mean' AS [Label], CAST(@PostIndexMEAN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 19.3 AS [ID],NULL AS [Variable], 'Median' AS [Label], CAST(@PostIndexMEDIAN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 19.4 AS [ID],NULL AS [Variable], 'SD' AS [Label], CAST(@PostIndexSD AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 19.5 AS [ID],NULL AS [Variable], 'Min' AS [Label], CAST(@PostIndexMIN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 19.6 AS [ID],NULL AS [Variable], 'Max' AS [Label], CAST(@PostIndexMAX AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT 20.0 AS [ID],'Post-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

/* Inserting dummy enteries for getting all the group */
Insert into #PostIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,12 as [MOS]

Insert into #PostIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,16 as [MOS]

Insert into #PostIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,19 as [MOS]

Insert into #PostIndexPatientCohort
Select 
'-1' as [Patient_ID]
,'2008-01-01' AS [Valid_Index]
,'2008-01-01' AS [Service_Date]
,25 as [MOS]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization]
SELECT CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN 20.1
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN 20.2
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN 20.3
		WHEN [MOS] > 24 
			THEN 20.4
		END  AS [ID]
      ,NULL AS [Variable]
	,CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN ' 12 - 15 '
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN ' 16 - 18 '
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN ' 19 - 24 '
		WHEN [MOS] > 24 
			THEN ' 24+ '
		END   as [Label]
	,(COUNT(DISTINCT Patient_id)-1) PatientCount
	,CAST(ROUND((((COUNT(DISTINCT Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #PostIndexPatientCohort
GROUP BY CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN 20.1
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN 20.2
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN 20.3
		WHEN [MOS] > 24 
			THEN 20.4
		END,
		CASE 
		WHEN [MOS] BETWEEN 12
				AND 15
			THEN ' 12 - 15 '
		WHEN [MOS] BETWEEN 16
				AND 18
			THEN ' 16 - 18 '
		WHEN [MOS] BETWEEN 19
				AND 24
			THEN ' 19 - 24 '
		WHEN [MOS] > 24 
			THEN ' 24+ '
END


SELECT CURRENT_TIMESTAMP AS END_TIMESTAMP

--************************************************************************************--