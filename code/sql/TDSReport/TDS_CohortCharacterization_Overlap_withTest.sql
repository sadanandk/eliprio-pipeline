-- - Tables Required: [$(TargetSchema)].[app_tds_Cohort], [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] and [$(TargetSchema)].[app_tds_int_master_cohort_population]
-- - OutPut Table	: [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
-- - Change History:	Change Date				Change Description
--						07-Nov-2019			Logic to calculate Insurance type is changed as given in ticket PLATFORM-1946
--						08-NOV-2019			filtered out only 16 AED while calculating Top Medication 
 ---------------------------------------------------------------------- 
--:Setvar SrcSchema Sha2_synoma_id_src
--:setvar TargetSchema SHA2_Synoma_update_New_4_Nov

SET NOCOUNT ON

SELECT CURRENT_TIMESTAMP AS START_TIMESTAMP

DECLARE @TotalPatientCount INT;
DECLARE @PatientCount INT;
DECLARE @PercentagePatient DECIMAL(5, 2);
DECLARE @PatientPercentage DECIMAL(5, 2);
DECLARE @MAXID INT
DECLARE @PatientAgeMedian FLOAT;
DECLARE @PatientAgeMean FLOAT;
DECLARE @PatientAgeStdDev FLOAT;
DECLARE @PHSPatientCount INT;

IF OBJECT_ID('[$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
CREATE TABLE [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
(
 [Id] DECIMAL(5,1)
,[Variable] VARCHAR (50)
,[Label] VARCHAR (100)
,[Value] VARCHAR(50) 
,[% of PatientCount] float
)

IF OBJECT_ID('tempdb..#Std_Rpt_TDS_Test_patient_list_firstIndex','U') IS NOT NULL
DROP TABLE #Std_Rpt_TDS_Test_patient_list_firstIndex

SELECT DISTINCT patient_id,MIN(Valid_index) AS Valid_index 
INTO #Std_Rpt_TDS_Test_patient_list_firstIndex
FROM [$(TargetSchema)].[Std_Rpt_TDS_Test_patient_list] 
GROUP BY Patient_id

IF OBJECT_ID('tempdb..#distribution_Patient') IS NOT NULL
	DROP TABLE #distribution_Patient

SELECT A.[patient_id]
	,B.[Age]
	,B.[Gender]
	,A.[State] as [Index_State]
INTO #distribution_Patient
FROM [$(TargetSchema)].[app_tds_Cohort] A 
inner JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
inner join #Std_Rpt_TDS_Test_patient_list_firstIndex C WITH (NOLOCK)
ON A.patient_id = C.patient_id
AND A.INDEX_DATE = C.Valid_index

SET @TotalPatientCount = (
		SELECT count(DISTINCT patient_id) AS TotalPatientCount
		FROM #distribution_Patient
		)

IF OBJECT_ID('tempdb..#Age') IS NOT NULL
	DROP TABLE #Age

SELECT Row_Number() OVER (
		ORDER BY age
		) AS ID
	,*
INTO #Age
FROM #distribution_Patient


SET @PatientAgeMean = (
		SELECT Sum(Age) / @TotalPatientCount
		FROM #distribution_Patient
		)

SET @PatientAgeStdDev = (
		SELECT STDEV(Age)
		FROM #distribution_Patient
		)


BEGIN 

DECLARE @MAXID_2 INT 
DECLARE @Lower_2_Min INT
DECLARE @Lower_2_Max INT
DECLARE @Upper_2_Min INT
DECLARE @Upper_2_Max INT
DECLARE @MAXID_2_Lower_Odd INT
DECLARE @Median_2_Lower_Odd Float
DECLARE @MAXID_2_Upper_Odd INT
DECLARE @Median_2_Upper_Odd Float
DECLARE @MAXID_2_Lower_Even INT
DECLARE @Median_2_Lower_Even Float
DECLARE @MAXID_2_Upper_Even INT
DECLARE @Median_2_Upper_Even Float
DECLARE @Age_Q1 Float
DECLARE @Age_Q3 Float
DECLARE @Median_2_All FLOAT




select @MAXID_2 = MAX(ID) from #Age
PRINT @MAXID_2
PRINT @MAXID_2%2

IF @MAXID_2%2 =0
		BEGIN
			   SET @Lower_2_Min = 1
			   SET @Lower_2_Max = @MAXID_2/2
			   SET @Upper_2_Min = (@MAXID_2/2)+1
			   SET @Upper_2_Max = @MAXID_2

			     SELECT @Median_2_All = AVG(CAST([age] AS FLOAT)) FROM #Age WHERE ID IN ((@MAXID_2/2),(@MAXID_2/2)+1)
				
				IF OBJECT_ID('tempdb..#LowerHalf_even') IS NOT NULL
				DROP TABLE #LowerHalf_even
			    SELECT  Row_NUmber()over (order by [age]) as ID_Lower,* INTO #LowerHalf_even FROM #Age WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max

				SELECT @MAXID_2_Lower_Even = MAX(ID_Lower) FROM #LowerHalf_even
				IF @MAXID_2_Lower_Even%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Even = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even/2),(@MAXID_2_Lower_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Even = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even+1)/2)


				IF OBJECT_ID('tempdb..#UpperHalf_even') IS NOT NULL
				DROP TABLE #UpperHalf_even
			    SELECT  Row_NUmber()over (order by [age]) as ID_Upper,* INTO #UpperHalf_even FROM #Age WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Even = MAX(ID_Upper) FROM #UpperHalf_even
				IF @MAXID_2_Upper_Even%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Even = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even/2),(@MAXID_2_Upper_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Even = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even+1)/2)
				

			SELECT @PatientAgeMedian = @Median_2_All , @Age_Q1 = @Median_2_Lower_Even ,@Age_Q3 = @Median_2_Upper_Even    
		END
ELSE 
		BEGIN 
			  SET @Lower_2_Min = 1
			  SET @Lower_2_Max = (@MAXID_2+1)/2
			  SET @Upper_2_Min = (@MAXID_2+1)/2
			  SET @Upper_2_Max = @MAXID_2

			   SELECT @Median_2_All = AVG(CAST([age] AS FLOAT)) FROM #Age WHERE ID IN ((@MAXID_2+1)/2)

			    IF OBJECT_ID('tempdb..#LowerHalf_odd') IS NOT NULL
				DROP TABLE #LowerHalf_odd
			    SELECT  Row_NUmber()over (order by [age]) as ID_Lower,* INTO #LowerHalf_odd FROM #Age WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max
				
				SELECT @MAXID_2_Lower_Odd = MAX(ID_Lower) FROM #LowerHalf_odd
				IF @MAXID_2_Lower_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Odd = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd/2),(@MAXID_2_Lower_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Odd = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd+1)/2)
				

			    IF OBJECT_ID('tempdb..#UpperHalf_odd') IS NOT NULL
				DROP TABLE #UpperHalf_odd
			    SELECT  Row_NUmber()over (order by [age]) as ID_Upper,* INTO #UpperHalf_odd FROM #Age WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Odd = MAX(ID_Upper) FROM #UpperHalf_odd
				IF @MAXID_2_Upper_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Odd = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd/2),(@MAXID_2_Upper_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Odd = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd+1)/2)
				
				
			SELECT @PatientAgeMedian = @Median_2_All , @Age_Q1 = @Median_2_Lower_Odd ,@Age_Q3 = @Median_2_Upper_Odd    
		END
		
END

SELECT @PHSPatientCount = count(DISTINCT patient_id) FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]
SELECT @PatientPercentage = (@TotalPatientCount * 100.00)/ @PHSPatientCount

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 1.0 AS [ID],'Total Patients in TDS Cohort (First IndexDate)' AS [Variable], NULL AS [Label], @TotalPatientCount as [Value], CAST(round(@PatientPercentage,2) AS VARCHAR(300)) AS  [% of PatientCount]
		
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 1.1 AS [ID],'"Age, continuous"' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 1.2 AS [ID],NULL AS [Variable], 'Mean' AS [Label], CONVERT(Decimal(5,2),@PatientAgeMean) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 1.3 AS [ID],NULL AS [Variable], 'Median' AS [Label], CONVERT(Decimal(5,2),@PatientAgeMedian) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 1.4 AS [ID],NULL AS [Variable], 'SD' AS [Label], CONVERT(Decimal(5,2),@PatientAgeStdDev) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 1.5 AS [ID],NULL AS [Variable], 'Quartile' AS [Label], 'Q1 (25%)='+CAST(CAST(@Age_Q1 AS DECIMAL(5,2)) AS VARCHAR(50))+' ; Q3 (75%)='+CAST(CAST(@Age_Q3  AS DECIMAL(5,2)) AS VARCHAR(50)) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 2.0 AS [ID],'Age category' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 
		CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN 2.1
		WHEN age BETWEEN 35
				AND 49
			THEN 2.2
		WHEN age BETWEEN 50
				AND 64
			THEN 2.3
		WHEN age >= 65
			THEN 2.4
		END AS [ID]
	   ,NULL AS [Variable]
	   ,CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN '18 - 34'
		WHEN age BETWEEN 35
				AND 49
			THEN '35 - 49'
		WHEN age BETWEEN 50
				AND 64
			THEN '50 - 64'
		WHEN age >= 65
			THEN '>=65'
		END AS [Label]
	,COUNT(DISTINCT Patient_id) 
	,cast(round(((COUNT(DISTINCT Patient_id) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #distribution_Patient
GROUP BY CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN 2.1
		WHEN age BETWEEN 35
				AND 49
			THEN 2.2
		WHEN age BETWEEN 50
				AND 64
			THEN 2.3
		WHEN age >= 65
			THEN 2.4
		END 
		,CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN '18 - 34'
		WHEN age BETWEEN 35
				AND 49
			THEN '35 - 49'
		WHEN age BETWEEN 50
				AND 64
			THEN '50 - 64'
		WHEN age >= 65
			THEN '>=65'
		END

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 3.0 AS [ID],'Gender' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 
		CASE 
		WHEN Gender='F'
		THEN 3.1
		WHEN Gender='M'
		THEN 3.2
		ELSE
		3.3
		END  AS [ID]
	   ,NULL AS [Variable]
	   ,CASE 
		WHEN Gender='F'
		THEN 'Female'
		WHEN Gender='M'
		THEN 'Male'
		ELSE
		'Unknown'
		END  AS [Label]
	   ,Count(DISTINCT patient_id)
	   ,cast(round(((COUNT(DISTINCT Patient_id) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #distribution_Patient P
GROUP BY Gender

/* AED Treatment */
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 4.0 AS [ID],'AED Treatment' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 4.1 AS [ID] 
	,NULL AS [Variable]
	,UPPER(LEFT(B.aed,1))+LOWER(SUBSTRING(B.aed, 2, LEN(B.aed)))AS [Label]
	,COUNT(DISTINCT A.patient_id) AS [Value]
	,CAST(ROUND(((COUNT(DISTINCT A.Patient_id) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] A WITH(NOLOCK)
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex C WITH (NOLOCK)
ON A.patient_id = C.patient_id
AND A.Valid_index = C.Valid_index
LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] B WITH(NOLOCK)
ON A.AED = B.Abbreviation
GROUP BY B.aed


/* Insurance Type */
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 5.0 AS [ID],'Insurance (at Index)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]


IF OBJECT_ID('tempdb..#InsurnaceTypeDistribution') IS NOT NULL
	DROP TABLE #InsurnaceTypeDistribution

SELECT		[Payer_Type],
			COUNT(A.[Patient_id]) AS [PatientCount]
INTO		#InsurnaceTypeDistribution
FROM		[$(TargetSchema)].[TDS_Payer_Type_PatientList] A
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex B WITH (NOLOCK)
ON			A.Patient_id	= B.Patient_id
AND			A.Valid_Index	= B.Valid_index
WHERE		A.[Index_ID]	= 1
GROUP BY	[Payer_Type]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 
		CASE WHEN [Payer_Type] = 'Commercial'
			THEN '5.2'
		WHEN [Payer_Type] = 'Cash'
			THEN '5.1'
		WHEN [Payer_Type] = 'Medicare'
			THEN '5.4'
		WHEN [Payer_Type] = 'Medicaid'
			THEN '5.3'
		ELSE
			'5.5'
		END AS [ID]
	   ,NULL AS [Variable]
	   ,UPPER(LEFT([Payer_Type],1))+LOWER(SUBSTRING([Payer_Type], 2, LEN([Payer_Type]))) AS [Label]
	   ,[PatientCount] AS [Value]
	  ,CAST(ROUND(((PatientCount * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM #InsurnaceTypeDistribution P


/* Prevelance of outcome variable*/
DECLARE @TotalRowCount INT;
SELECT @TotalRowCount = COUNT(1) 
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_input_FirstIndex] A WITH (NOLOCK)
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex B WITH(NOLOCK)
ON A.Patient_ID = B.PATIENT_ID
AND A.VALID_INDEX = B.VALID_INDEX
WHERE A.[Valid_Index] IS NOT NULL

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 6.0 AS [ID],'Distribution of outcome variables' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 6.1 AS [ID],'TDS solution' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT CASE 
		WHEN [Outcome_Variable] = 'Success'
			THEN 6.2
		ELSE 6.3
		END AS [ID]
	,NULL AS [Variable]
	,CASE WHEN [Outcome_Variable] = 'Success'
			THEN 'Stable'
		ELSE 'Unstable'
		END AS [Label]
	,COUNT(Outcome_Variable) as [Value] 
	,CAST(round(((COUNT(Outcome_Variable) * 100.0) / @TotalRowCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_input_FirstIndex] A WITH (NOLOCK)
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex B WITH(NOLOCK)
ON A.Patient_ID = B.PATIENT_ID
AND A.VALID_INDEX = B.VALID_INDEX
WHERE A.[Valid_Index] IS NOT NULL
GROUP BY [Outcome_Variable]

/* AED Generation */
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 7.0 AS [ID],'AED Generation (at Index)' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest] (
	[Id]
	,[Variable]
	,[Label]
	,[Value]
	,[% of PatientCount]
	)
SELECT CASE WHEN Abb.[AED_Generation]='First'
		THEN 7.1 
	WHEN Abb.[AED_Generation]='Second'
		THEN 7.2 
	WHEN Abb.[AED_Generation]='Third'
		THEN 7.3 
	 END AS [ID]
	,NULL AS [Variable]
	,CASE WHEN Abb.[AED_Generation]='First'
		THEN 'First'
	WHEN Abb.[AED_Generation]='Second'
		THEN 'Second' 
	WHEN Abb.[AED_Generation]='Third'
		THEN 'Third'
	 END AS [Label]
	,COUNT(DISTINCT AD.patient_id) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT AD.[patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] AD WITH (NOLOCK)
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex B WITH(NOLOCK)
ON AD.Patient_ID = B.PATIENT_ID
AND AD.VALID_INDEX = B.VALID_INDEX
INNER JOIN [$(TargetSchema)].[app_tds_int_Regiments_Treatment] RG(NOLOCK)
	ON AD.patient_id = RG.patient_id
		AND AD.Valid_Index = RG.Start_point
LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] Abb
	ON RG.Abbreviation = Abb.Abbreviation
GROUP BY Abb.AED_Generation
		
/* CCI Score*/
INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 8.0 AS [ID],'Charlson Comorbidity Index' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

IF OBJECT_ID('tempdb..#PatientCCIScore') IS NOT NULL
	DROP TABLE #PatientCCIScore

SELECT CI.CCI_Score AS [CCIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
INTO #PatientCCIScore
FROM [$(TargetSchema)].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN  [$(TargetSchema)].[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex C WITH(NOLOCK)
ON A.Patient_ID = C.PATIENT_ID
AND A.INDEX_DATE = C.VALID_INDEX
GROUP BY CI.CCI_Score

UPDATE #PatientCCIScore
SET CCIScore = 5
WHERE CCIScore >= 5

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest] (
	[Id]
	,[Variable]
	,[Label]
	,[Value]
	,[% of PatientCount]
	)
SELECT CASE 
		WHEN CCIScore = 0
			THEN 8.1
		WHEN CCIScore = 1
			THEN 8.2
		WHEN CCIScore = 2
			THEN 8.3
		WHEN CCIScore = 3
			THEN 8.4
		WHEN CCIScore = 4
			THEN 8.5
		WHEN CCIScore = 5
			THEN 8.6
		END AS  [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN CCIScore = 0
			THEN '0'
		WHEN CCIScore = 1
			THEN '1'
		WHEN CCIScore = 2
			THEN '2'
		WHEN CCIScore = 3
			THEN '3'
		WHEN CCIScore = 4
			THEN '4'
		WHEN CCIScore = 5
			THEN '>=5'
		END AS [Charlson Comorbidity Index (CCI)]
	,SUM([PatientCount]) AS [PatientCount]
	,CAST(ROUND(((SUM([PatientCount]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [PatientPercentage]
FROM #PatientCCIScore
GROUP BY CCIScore


SELECT @TotalPatientCount = COUNT(DISTINCT A.Patient_ID)
FROM [$(TargetSchema)].[app_tds_Cohort] A 
inner JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex C WITH(NOLOCK)
ON A.Patient_ID = C.PATIENT_ID
AND A.INDEX_DATE = C.VALID_INDEX

IF OBJECT_ID('tempdb..#PatientCohortComb') IS NOT NULL
	DROP TABLE #PatientCohortComb

SELECT Patient_ID
	,Valid_Index
	,Comorbidity
	,ComorbidityValue
INTO #PatientCohortComb
FROM (
	SELECT CI.[patient_id]
		,CI.Valid_Index
		,[Myocardial_Infarction]
		,[Hemiplegia_or_Paraplegia]
		,[Mod_Severe_liver_Disease]
		,[Cerebrovascular_Disease]
		,[Anoxic_Brain_Injury]
		,[Metastatic_Solid_Tumor]
		,[Cardiac_Arrhythmias]
		,[Peripheral_Vascular_Disease]
		,[Mod_Severe_Renal_Disease]
		,[Congestive_Heart_Failure]
		,[Chronic_Pulmonary_Disease]
		,[AIDS_HIV]
		,[Dementia]
		,[Diabetes_With_Organ_Damage]
		,[Pulmonary_Circulation_Disorders]
		,[Hypertension]
		,[Aspiration_Pneumonia]
		,[Brain_Tumor]
		,[Solid_Tumor_Without_Metastases]
		,[Peptic_Ulcer_Disease]
		,[Connective_Tissue_Disease]
		,[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]
		,[Diabetes_Without_Organ_Damage]
		,[Mild_Liver_Disease]
		,[Depression]
		,[Psychoses]
	FROM [$(TargetSchema)].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
	INNER JOIN  [$(TargetSchema)].[app_tds_Cohort] A WITH(NOLOCK)
	ON CI.Patient_ID = A.Patient_ID
	AND CI.Valid_index = A.INDEX_DATE
	INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
	ON A.patient_id = B.patient_id
	AND A.INDEX_DATE = B.Valid_index
	INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex C WITH(NOLOCK)
	ON A.Patient_ID = C.PATIENT_ID
	AND A.INDEX_DATE = C.VALID_INDEX
	) p
UNPIVOT(ComorbidityValue FOR Comorbidity IN (
			[Myocardial_Infarction]
			,[Hemiplegia_or_Paraplegia]
			,[Mod_Severe_liver_Disease]
			,[Cerebrovascular_Disease]
			,[Anoxic_Brain_Injury]
			,[Metastatic_Solid_Tumor]
			,[Cardiac_Arrhythmias]
			,[Peripheral_Vascular_Disease]
			,[Mod_Severe_Renal_Disease]
			,[Congestive_Heart_Failure]
			,[Chronic_Pulmonary_Disease]
			,[AIDS_HIV]
			,[Dementia]
			,[Diabetes_With_Organ_Damage]
			,[Pulmonary_Circulation_Disorders]
			,[Hypertension]
			,[Aspiration_Pneumonia]
			,[Brain_Tumor]
			,[Solid_Tumor_Without_Metastases]
			,[Peptic_Ulcer_Disease]
			,[Connective_Tissue_Disease]
			,[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]
			,[Diabetes_Without_Organ_Damage]
			,[Mild_Liver_Disease]
			,[Depression]
			,[Psychoses]
			)) AS unpvt;


IF OBJECT_ID('tempdb..#PatientComorbidityDistribution') IS NOT NULL
	DROP TABLE #PatientComorbidityDistribution

SELECT 9.1 AS [ID]
	,NULL AS [Variable]
	,UPPER(LEFT(Comorbidity,1))+LOWER(SUBSTRING(Comorbidity, 2, LEN(Comorbidity))) AS [Label]
	,Count(DISTINCT Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT [Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #PatientComorbidityDistribution
FROM #PatientCohortComb
WHERE ComorbidityValue = 1
GROUP BY Comorbidity

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 9.0 AS [ID],'Top 10 Co morbidities*' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

IF OBJECT_ID('tempdb..#Top10PatientComorbidityDistribution') IS NOT NULL
	DROP TABLE #Top10PatientComorbidityDistribution

SELECT TOP 15 *
INTO #Top10PatientComorbidityDistribution
FROM #PatientComorbidityDistribution
ORDER BY PatientCount DESC, [Label] ASC

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT *
FROM #Top10PatientComorbidityDistribution

/* Comedication Report*/
SELECT @TotalPatientCount = COUNT(DISTINCT B.Patient_ID)
FROM [$(TargetSchema)].[app_tds_Cohort] A 
inner JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex C WITH(NOLOCK)
ON A.Patient_ID = C.PATIENT_ID
AND A.INDEX_DATE = C.VALID_INDEX

IF OBJECT_ID('tempdb..#PatientUSCCohort') IS NOT NULL
DROP TABLE #PatientUSCCohort

SELECT 9.1 as [ID], 'USC Group' AS [Variable],UPPER(LEFT(C.USC_Name,1))+LOWER(SUBSTRING(C.USC_Name, 2, LEN(C.USC_Name)))AS [Label]
,COUNT(DISTINCT A.Patient_Id) as [PatientCount]
,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #PatientUSCCohort
FROM [$(TargetSchema)].[app_tds_Cohort] A 
INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  BI WITH (NOLOCK)
ON A.patient_id = BI.patient_id
AND A.INDEX_DATE = BI.Valid_index
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex D WITH(NOLOCK)
ON A.Patient_ID = D.PATIENT_ID
AND A.INDEX_DATE = D.VALID_INDEX
LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
ON BI.patient_id=B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.INDEX_DATE) AND DATEADD(DD,-1,A.INDEX_DATE) 
AND B.Days_Supply > 0
LEFT JOIN  [$(SrcSchema)].[src_ref_Product](NOLOCK) C
ON B.NDC = C.NDC
GROUP BY C.USC_Name


IF OBJECT_ID('tempdb..#PatientComedicationCohortWithoutAED') IS NOT NULL
DROP TABLE #PatientComedicationCohortWithoutAED

SELECT 10.1 as [ID], NULL AS [Variable],UPPER(LEFT(C.USC_Name,1))+LOWER(SUBSTRING(C.USC_Name, 2, LEN(C.USC_Name)))AS [Label]
,COUNT(DISTINCT A.Patient_Id) as [PatientCount]
,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
INTO #PatientComedicationCohortWithoutAED
FROM [$(TargetSchema)].[app_tds_Cohort] A 
INNER JOIN  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]  BI WITH (NOLOCK)
ON A.patient_id = BI.patient_id
AND A.INDEX_DATE = BI.Valid_index
INNER JOIN #Std_Rpt_TDS_Test_patient_list_firstIndex D WITH(NOLOCK)
ON A.Patient_ID = D.PATIENT_ID
AND A.INDEX_DATE = D.VALID_INDEX
LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
ON  BI.patient_id = B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.INDEX_DATE) AND DATEADD(DD,-1,A.INDEX_DATE)  
AND B.Days_Supply > 0
LEFT JOIN  [$(SrcSchema)].[src_ref_Product](NOLOCK) C
ON B.NDC = C.NDC
WHERE C.[GENERIC_NAME] NOT IN
	(
'CARBAMAZEPINE'
,'CARBAMAZEPINE (ANTIPSYCHOTIC)'
,'CLOBAZAM'
,'DIVALPROEX SODIUM'
,'ESLICARBAZEPINE ACETATE'
,'ETHOSUXIMIDE'
,'ETHOSUXIMIDE (BULK)'
,'FOSPHENYTOIN SODIUM'
,'LACOSAMIDE'
,'LAMOTRIGINE'
,'LAMOTRIGINE (BULK)'
,'LEVETIRACETAM'
,'LEVETIRACETAM (BULK)'
,'LEVETIRACETAM IN NACL (ISO-OS)'
,'LEVETIRACETAM IN SODIUM CHLORIDE'
,'OXCARBAZEPINE'
,'PERAMPANEL'
,'PHENOBARBITAL'
,'PHENOBARBITAL SODIUM'
,'PHENTERMINE/TOPIRAMATE'
,'PHENYTOIN'
,'PHENYTOIN (BULK)'
,'PHENYTOIN SODIUM'
,'PHENYTOIN SODIUM EXTENDED'
,'PHENYTOIN SODIUM PROMPT'
,'PREGABALIN'
,'RUFINAMIDE'
,'TOPIRAMATE'
,'TOPIRAMATE (BULK)'
,'VALPROATE SODIUM'
,'VALPROATE SODIUM (BULK)'
,'VALPROIC ACID'
,'VALPROIC ACID (AS SODIUM SALT)'
,'VALPROIC ACID (BULK)'
,'ZONISAMIDE'
,'ZONISAMIDE (BULK)'

)
GROUP BY C.USC_Name

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]
SELECT 10.0 AS [ID],'Top 10 Medication*' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

IF OBJECT_ID('tempdb..#Top10PatientComedicationCohortWithoutAED') IS NOT NULL
DROP TABLE #Top10PatientComedicationCohortWithoutAED

SELECT TOP 15 * 
INTO #Top10PatientComedicationCohortWithoutAED
FROM #PatientComedicationCohortWithoutAED
ORDER BY PatientCount DESC, [Label] ASC

INSERT INTO [$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest] 
SELECT * FROM #Top10PatientComedicationCohortWithoutAED


SELECT CURRENT_TIMESTAMP AS END_TIMESTAMP

--************************************************************************************--