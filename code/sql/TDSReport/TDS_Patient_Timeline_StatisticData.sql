-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : select patient timeline data for reports
-- - NOTE      : 
-- -			 
-- - Execution : 
-- - Date      : 19-Oct-2019
-- - Change History:	Change Date				Change Description
--						
-- ----------------------------------------------------------------------
--:setvar TargetSchema SHA2_Synoma_update_New_17_Oct

SET NOCOUNT ON

SELECT	A.Variable 
		,'Median = '+B.[Train data]		AS [Train data] 
		,'Median = '+B.[Test data]		AS [Test data] 
		,'Median = '+B.[SHA data]		AS [SHA data] 
		,'Median = '+A.[Value]			AS [PHS data] 
FROM	[$(TargetSchema)].[TDS_Patient_timeline_statistic] A
JOIN	[$(TargetSchema)].[std_Rpt_tds_patient_timeline_statistic] B
ON		A.Id = B.Id
ORDER BY A.ID

--************************************************************************************--

