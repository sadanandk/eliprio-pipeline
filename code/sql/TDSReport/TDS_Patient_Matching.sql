-- ---------------------------------------------------------------------
-- - AUTHOR			: Supriya S
-- - USED BY		: 
-- - 
-- - PURPOSE		: Creating TDS_Patient_matching table 
-- - NOTE			: ELREFPRED-977
-- - Execution		: 
-- - Date			: 23-Aug-2019			
-- - Tables Required: [$(SrcSchema)].[PHPH_SynomaIDs], [$(SrcSchema)].[SRC_Ref_Patient],[$(TargetSchema)].[app_TDS_Cohort]
-- - Change History:			
-- ---------------------------------------------------------------------- 


--:setvar SrcSchema SHA_SYNOMA_ID_SRC
--:Setvar TargetSchema SHA_SYNOMA_ID_TARGET

SET NOCOUNT ON

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[TDS_patient_matching]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[TDS_patient_matching]
(
	[SYNOMA_PATIENT_ID] [varchar](100) NULL,
	[SHA_MATCH_FLAG] [varchar](1) NULL,
	[TDS_MATCH_FLAG] [varchar](1) NULL
)
END

Truncate table [$(TargetSchema)].[TDS_patient_matching]

Insert into [$(TargetSchema)].[TDS_patient_matching] (SYNOMA_PATIENT_ID)
select distinct SHS_patient_id from [$(SrcSchema)].[PHPH_SynomaIDs]

Update [$(TargetSchema)].[TDS_patient_matching]
set [SHA_MATCH_FLAG] = 0

Update [$(TargetSchema)].[TDS_patient_matching]
set [SHA_MATCH_FLAG] = 1 
from [$(SrcSchema)].[SRC_Ref_Patient]
where [$(TargetSchema)].[TDS_patient_matching].[SYNOMA_PATIENT_ID] = replace(ltrim(replace([$(SrcSchema)].[SRC_Ref_Patient].[PATIENT_ID], '0',' ')),' ','0')

Update [$(TargetSchema)].[TDS_patient_matching]
set [TDS_MATCH_FLAG] = 0
where [SHA_MATCH_FLAG] = 1


Update [$(TargetSchema)].[TDS_patient_matching]
set [TDS_MATCH_FLAG] = 1 
from [$(TargetSchema)].[app_TDS_Cohort]
where [$(TargetSchema)].[TDS_patient_matching].[SYNOMA_PATIENT_ID] = replace(ltrim(replace([$(TargetSchema)].[app_TDS_Cohort].[PATIENT_ID], '0',' ')),' ','0')
and  Index_date is not null and Genpop=1

SELECT * FROM [$(TargetSchema)].[TDS_patient_matching] 



