-- - Tables Required: [$(TargetSchema)].[app_tds_Cohort], [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] and [$(TargetSchema)].[app_tds_int_master_cohort_population]
-- - OutPut Table	: [$(TargetSchema)].[TDS_Match_Flag_condition]
-- - Change History:	Change Date				Change Description
-- -					07-Oct-2019             Written code to generate Match Flag report for TDS

 ---------------------------------------------------------------------- 
--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_update_New_17_Oct

--Match with SHA
--select count(distinct patient_id) from [$(SrcSchema)].[PHPH_SynomaIDs] 
--where patient_id in (SELECT DISTINCT Patient_id from [$(TargetSchema)].[app_int_Patient_Profile])

---IsEpilepsy

--SELECT count(DISTINCT Patient_id) from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 

---AtleastOneAED
IF OBJECT_ID('Tempdb..#atleastOneAED','U') IS NOT NULL
DROP TABLE #atleastOneAED
select distinct patient_id into #atleastOneAED from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] 
WHERE  AED_Days >30 AND Patient_id IN (SELECT DISTINCT Patient_id from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 )


IF OBJECT_ID('Tempdb..#Index_date','U') IS NOT NULL
DROP TABLE #Index_date
select distinct patient_id into #Index_date from  [$(TargetSchema)].[app_tds_cohort] 
WHERE ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') AND  Stable_Eligible =1  AND LEN(AED) = 3  


IF OBJECT_ID('Tempdb..#age','U') IS NOT NULL
DROP TABLE #age
select distinct patient_id into #age from  [$(TargetSchema)].[app_tds_cohort] 
WHERE ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') AND  Stable_Eligible =1  AND LEN(AED) = 3  and age>=18


IF OBJECT_ID('Tempdb..#Lookback','U') IS NOT NULL
DROP TABLE #Lookback
select distinct patient_id into #Lookback from  [$(TargetSchema)].[app_tds_cohort] 
WHERE ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') AND  Stable_Eligible =1  AND LEN(AED) = 3  and age>=18 and lookback_present = 1


IF OBJECT_ID('Tempdb..#Lookforward','U') IS NOT NULL
DROP TABLE #Lookforward
select distinct patient_id into #Lookforward from  [$(TargetSchema)].[app_tds_cohort] 
WHERE ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') AND  Stable_Eligible =1  AND LEN(AED) = 3  and age>=18 
and lookback_present = 1 and lookforward_present = 1 

IF OBJECT_ID('Tempdb..#16AED','U') IS NOT NULL
DROP TABLE #16AED
select distinct patient_id into #16AED from  [$(TargetSchema)].[app_tds_cohort] 
WHERE ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') AND  Stable_Eligible =1  AND LEN(AED) = 3  and age>=18 
and lookback_present = 1 and lookforward_present = 1 and index_date is not null

IF OBJECT_ID('Tempdb..#TrainPatients','U') IS NOT NULL
DROP TABLE #TrainPatients
SELECT DISTINCT K.PATIENT_id INTO #TrainPatients FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] k
INNER JOIN [$(TargetSchema)].[Std_Rpt_TDS_Train_patient_list] I
on K.patient_id = I.patient_id and K.VALID_INDEX = I.VALID_INDEX
where Index_ID=1

IF OBJECT_ID('Tempdb..#TestPatients','U') IS NOT NULL
DROP TABLE #TestPatients
SELECT DISTINCT K.PATIENT_id INTO #TestPatients FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] K
INNER JOIN [$(TargetSchema)].[Std_Rpt_TDS_Test_patient_list] I
on K.patient_id = I.patient_id and K.VALID_INDEX = I.VALID_INDEX
where Index_ID=1


IF OBJECT_ID('[$(TargetSchema)].[TDS_Match_Flag]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[TDS_Match_Flag]
CREATE TABLE [$(TargetSchema)].[TDS_Match_Flag]
(
 [Patient_id]  VARCHAR (20)
,[Patient_Match_flag] VARCHAR (1)
,[IsEpilepsy] VARCHAR (1)
,[atleastOneaed_with30] VARCHAR(1) 
,[Index_date_Qualification] VARCHAR(1)
,[Age>=18] VARCHAR(1)
,[Lookback_present] VARCHAR(1)
,[lookforward_present] VARCHAR(1)
,[Amongst_16_AED] VARCHAR(1)
,[Train_Patient] VARCHAR(1)
,[Test_Patient] VARCHAR(1)
)

INSERT INTO [$(TargetSchema)].[TDS_Match_Flag]
select distinct a.patient_id, case when b.patient_id is null then 0 else 1 end as Patient_Match_flag, 
case when b.[IsEpilepsy] is null then 0 else b.[IsEpilepsy] end as [IsEpilepsy], case when c.aed_days>30 then 1 else 0 end as [atleastOneaed_with30 dyas],
case when d.patient_id is null then 0 else 1 end as Index_date_Qualification,
case when e.patient_id is null then 0 else 1 end as [Age>=18],
case when f.patient_id is null then 0 else 1 end as Lookback_present,
case when g.patient_id is null then 0 else 1 end as lookforward_present,
case when h.patient_id is null then 0 else 1 end as Amongst_16_AED ,
case when I.patient_id is null then 0 else 1 end as Train_Patient, 
case when J.patient_id is null then 0 else 1 end as Test_Patient 
from [$(SrcSchema)].[PHPH_SynomaIDs] a
left join [$(TargetSchema)].[app_int_Patient_Profile] b
on a.patient_id = b.patient_id
left join [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] c
on a.patient_id = c.patient_id and b.[isepilepsy] = 1 and c.aed_days>30 
left join #Index_date d
on a.patient_id = d.patient_id
left join #age e
on a.patient_id = e.patient_id
left join #lookback f
on a.patient_id = f.patient_id
left join #lookforward g
on a.patient_id = g.patient_id
left join #16AED h
on a.patient_id = h.patient_id
left join #TrainPatients I
ON A.PATIENT_ID = I.PATIENT_ID
LEFT JOIN #TestPatients j
ON A.PATIENT_ID = J.PATIENT_ID
order by a.patient_id
