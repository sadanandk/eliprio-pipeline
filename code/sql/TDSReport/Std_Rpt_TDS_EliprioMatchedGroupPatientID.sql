IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_EliprioMatchedGroupPatientID]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[Std_Rpt_TDS_EliprioMatchedGroupPatientID]
(
	[eliprio_K3_maxF1] VARCHAR(50) NULL
   ,[matchedCohort_K3_maxF1] VARCHAR(50) NULL
   ,[eliprio_K3_p05] VARCHAR(50) NULL
   ,[matchedCohort_K3_p05] VARCHAR(50) NULL
)

END

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_EliprioMatchedGroupPatientID]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE [$(TargetSchema)].[Std_Rpt_TDS_EliprioMatchedGroupPatientID]
	BULK INSERT [$(TargetSchema)].[Std_Rpt_TDS_EliprioMatchedGroupPatientID]
	FROM '$(outDataPath)\Std_Rpt_TDS_EliprioMatchedGroupPatientID.csv'
	WITH 
	(
	FIRSTROW = 2,
	rowterminator = '\n',
	fieldterminator =',',
	FIELDQUOTE = '"'
	); 
END