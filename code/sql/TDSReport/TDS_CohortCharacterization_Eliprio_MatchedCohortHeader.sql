-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : select cohort characterization header informaion for reports
-- - NOTE      : 
-- -			 
-- - Execution : 
-- - Date      : 19-Oct-2019
-- - Change History:	Change Date				Change Description
--						08-Nov-2019				Rename Seizure disorder with AED not in study					
-- ---------------------------------------------------------------------- 
--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_update_New_4_Nov
SET NOCOUNT ON


SELECT		 CAST(ISNULL(A.[Variable], '')									AS VARCHAR(70))	AS [Variable]
			,CAST(ISNULL(Replace(Replace(CASE WHEN A.[Label] = 'Seizure disorders' THEN 'Other AEDs' ELSE A.[Label] END,',',';'),'_',' '), '')	AS VARCHAR(60))	AS [Label]
			,CASE WHEN	A.[Label] = 'Quartile'						THEN A.[Value]
				  WHEN A.[Variable] IS NULL AND A.[Value] IS NULL	THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN A.[Value]
			 ELSE
				  CASE	WHEN ((A.[% of PatientCount] IS NULL) OR (A.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(A.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(A.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(A.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [PHS sub-cohort]
			 
			 ,CASE WHEN	A.[Label] = 'Quartile'						THEN B.[Value]
				  WHEN A.[Variable] IS NULL AND B.[Value] IS NULL	THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN B.[Value]
			 ELSE
				  CASE	WHEN ((B.[% of PatientCount] IS NULL) OR (B.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(B.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [Eliprio MaxF1 cohort]
			 
			 ,CASE WHEN	A.[Label] = 'Quartile'						THEN C.[Value]
				  WHEN A.[Variable] IS NULL AND C.[Value] IS NULL	THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN C.[Value]
			 ELSE
				  CASE	WHEN ((C.[% of PatientCount] IS NULL) OR (C.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(C.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(C.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(C.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [Matched MaxF1 cohort]
			 
			 ,CASE WHEN	A.[Label] = 'Quartile'						THEN D.[Value]
				  WHEN A.[Variable] IS NULL AND D.[Value] IS NULL	THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN D.[Value]
			 ELSE
				  CASE	WHEN ((D.[% of PatientCount] IS NULL) OR (D.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(D.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(D.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(D.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [Eliprio p05 cohort]
			
			 ,CASE WHEN	A.[Label] = 'Quartile'						THEN E.[Value]
				  WHEN A.[Variable] IS NULL AND E.[Value] IS NULL	THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN E.[Value]
			 ELSE
				  CASE	WHEN ((E.[% of PatientCount] IS NULL) OR (E.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(E.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(E.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(E.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [Matched p05 cohort]

FROM		[$(TargetSchema)].[TDS_Cohortcharacterization] A
LEFT JOIN  [$(TargetSchema)].[TDS_CohortCharacterization_Eliprio_MaxF1] B
ON			ISNULL(A.label,'')		= ISNULL(B.LABEL,'')
AND			ISNULL(A.VARIABLE,'')	= ISNULL(B.VARIABLE,'') 
AND			A.ID = B.ID
LEFT JOIN  [$(TargetSchema)].[TDS_CohortCharacterization_MatchedCohort_MaxF1] C
ON			ISNULL(A.label,'')		= ISNULL(C.LABEL,'')
AND			ISNULL(A.VARIABLE,'')	= ISNULL(C.VARIABLE,'') 
AND			A.ID = C.ID
LEFT JOIN  [$(TargetSchema)].[TDS_CohortCharacterization_Eliprio_p05] D
ON			ISNULL(A.label,'')		= ISNULL(D.LABEL,'')
AND			ISNULL(A.VARIABLE,'')	= ISNULL(D.VARIABLE,'') 
AND			A.ID = D.ID
LEFT JOIN  [$(TargetSchema)].[TDS_CohortCharacterization_MatchedCohort_p05] E
ON			ISNULL(A.label,'')		= ISNULL(E.LABEL,'')
AND			ISNULL(A.VARIABLE,'')	= ISNULL(E.VARIABLE,'') 
AND			A.ID = E.ID
WHERE		1<>1
