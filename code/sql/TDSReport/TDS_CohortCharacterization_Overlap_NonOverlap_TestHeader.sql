-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : select cohort characterization header informaion for reports for test data
-- - NOTE      : 
-- -			 
-- - Execution : 
-- - Date      : 19-Oct-2019
-- - Change History:	Change Date				Change Description
--						08-Nov-2019				Rename Seizure disorder with AED not in study		
-- -					11-Nov-2019				Removed _ from comorbidity.						
-- ----------------------------------------------------------------------
--:Setvar SrcSchema sha_pipeline_all
--:setvar Schema sha_pipeline_all_target
SET NOCOUNT ON

SELECT		 CAST(ISNULL(A.[Variable], '')					AS VARCHAR(70))	AS [Variable]
			,CAST(ISNULL(REPLACE(REPLACE(CASE WHEN A.[Label] = 'Seizure disorders' THEN 'Other AEDs' ELSE A.[Label] END ,',',';'),'_',' '), '')	AS VARCHAR(60))	AS [Label]
			,CASE WHEN	A.[Label] = 'Quartile' 
				  THEN	B.[Value]
			 ELSE
				  CASE	WHEN ((B.[% of PatientCount] IS NULL) OR (B.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),''),'(',ISNULL(CAST(B.[% of PatientCount] AS VARCHAR(100)),''),'%)')
				  END  
			 END AS [PHS Overlap with Test Data] 
			 ,CASE WHEN	A.[Label] = 'Quartile' 
				  THEN	C.[Value]
			 ELSE
				  CASE	WHEN ((C.[% of PatientCount] IS NULL) OR (C.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(C.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(C.[Value] ),1),'.00',''),''),'(',ISNULL(CAST(C.[% of PatientCount] AS VARCHAR(100)),''),'%)')
				  END  
			 END AS [PHS NonOverlap with Test Data] 

FROM		[$(TargetSchema)].[TDS_Cohortcharacterization]  A
LEFT JOIN	[$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTest]	 B
ON			ISNULL(A.label,'')		= ISNULL(B.LABEL,'') 
AND			ISNULL(A.VARIABLE,'')	= ISNULL(B.VARIABLE,'') 
AND			A.ID = B.ID
LEFT JOIN   [$(TargetSchema)].[TDS_CohortCharacterization_NonOverlap_withTest] C
ON			ISNULL(A.label,'')		= ISNULL(C.LABEL,'') 
AND			ISNULL(A.VARIABLE,'')	= ISNULL(C.VARIABLE,'') 
AND			A.ID = C.ID
WHERE 1<>1

	
