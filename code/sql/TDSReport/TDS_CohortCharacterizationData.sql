-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : select cohort characterization data for reports
-- - NOTE      : 
-- -			 
-- - Execution : 
-- - Date      : 19-Oct-2019
-- - Change History:	Change Date				Change Description
--						08-Nov-2019				Rename Seizure disorder with AED not in study						
-- ---------------------------------------------------------------------- 

SET NOCOUNT ON

UPDATE [$(TargetSchema)].[TDS_CohortCharacterization] 
SET [Variable] = SUBSTRING([Variable], 2, LEN([Variable]) - 2)
WHERE [Variable] LIKE '"%"'

SELECT		 CAST(ISNULL(A.[Variable], '')									AS VARCHAR(70))	AS [Variable]
			,CAST(ISNULL(Replace(Replace(CASE WHEN B.[Label] = 'Seizure disorders' THEN 'Other AEDs' ELSE B.[Label] END ,',',';'),'_',' '), '')	AS VARCHAR(60))	AS [Label]
			,CAST(ISNULL(A.Train,'')										AS VARCHAR(50))	AS [Train data for TDS solution]
			,CAST(ISNULL(A.Test,'')											AS VARCHAR(50))	AS [Test data for TDS solution]
			,CAST(ISNULL(A.SHA,'')											AS VARCHAR(50))	AS [SHA]
			,CASE WHEN	A.[Label] = 'Quartile'						THEN B.[Value]
				  WHEN A.[Variable] IS NULL AND B.[Value] IS NULL	THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN B.[Value]
			 ELSE
				  CASE	WHEN ((B.[% of PatientCount] IS NULL) OR (B.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(B.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [PHS sub-cohort]
FROM		[$(TargetSchema)].[TDS_Cohortcharacterization] B
LEFT JOIN   [$(TargetSchema)].[std_Rpt_TDS_Cohort_characterization_Train_Test_SHA] A
ON			REPLACE(ISNULL(B.LABEL,''),',',';') = ISNULL(A.label,'')	
AND			ISNULL(B.VARIABLE,'') 				= ISNULL(A.VARIABLE,'')
AND			B.ID = A.ID
WHERE		a.[ID]	<	11.0
ORDER BY	 A.ID, B.[% of PatientCount] DESC, A.[Label] ASC 



--************************************************************************************--
