-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : select cohort characterization data for reports for train 
-- - NOTE      : 
-- -			 
-- - Execution : 
-- - Date      : 19-Oct-2019
-- - Change History:	Change Date				Change Description
--						08-Nov-2019				Rename Seizure disorder with AED not in study
-- -					11-Nov-2019				Removed _ from comorbidity.				
-- ---------------------------------------------------------------------- 
SET NOCOUNT ON

SELECT		 CAST(ISNULL(A.[Variable], '')					AS VARCHAR(70))	AS [Variable]
			,CAST(ISNULL(REPLACE(REPLACE(CASE WHEN A.[Label] = 'Seizure disorders' THEN 'Other AEDs' ELSE A.[Label] END ,',',';'),'_',' '), '')	AS VARCHAR(60))	AS [Label]
			,CASE WHEN	A.[Label] = 'Quartile' 
				  THEN	B.[Value]
				  WHEN A.[Variable] IS NULL AND B.[Value] IS NULL THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN B.[Value]
			 ELSE
				  CASE	WHEN ((B.[% of PatientCount] IS NULL) OR (B.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(B.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(B.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [PHS Overlap with Train Data] 
			 ,CASE WHEN	A.[Label] = 'Quartile' 
				  THEN	C.[Value]
				  WHEN A.[Variable] IS NULL AND C.[Value] IS NULL THEN '0 (0.00%)'
				  WHEN A.[Label]  IN ('MEAN', 'MEDIAN', 'SD')		THEN C.[Value]
			 ELSE
				  CASE	WHEN ((C.[% of PatientCount] IS NULL) OR (C.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(CONVERT(VARCHAR(100),(C.[Value] ),1),'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(CONVERT(VARCHAR(100),(C.[Value] ),1),'.00',''),''),' (',ISNULL(CAST(C.[% of PatientCount] AS DECIMAL(5,2)),''),'%)')
				  END  
			 END AS [PHS NonOverlap with Train Data] 

FROM		[$(TargetSchema)].[TDS_Cohortcharacterization]  A
LEFT JOIN	[$(TargetSchema)].[TDS_CohortCharacterization_Overlap_withTrain]	 B
ON			ISNULL(A.label,'')		= ISNULL(B.LABEL,'') 
AND			ISNULL(A.VARIABLE,'')	= ISNULL(B.VARIABLE,'') 
AND			A.ID = B.ID
LEFT JOIN   [$(TargetSchema)].[TDS_CohortCharacterization_NonOverlap_withTrain] C
ON			ISNULL(A.label,'')		= ISNULL(C.LABEL,'') 
AND			ISNULL(A.VARIABLE,'')	= ISNULL(C.VARIABLE,'') 
AND			A.ID = C.ID
WHERE A.ID < 11.0
ORDER BY A.ID,A.[% of PatientCount] DESC , A.Label

--************************************************************************************--

