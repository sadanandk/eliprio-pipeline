-- - 
-- - PURPOSE		: Pipeline Metrics for DRE and TDS.
-- - NOTE			: ELREFPRED-977
-- - Execution		: 
-- - Date			: 25-March-2019			
-- - Tables Required: [$(SrcSchema)].[src_REF_patient], [$(TargetSchema)].[app_int_Patient_Profile],[$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] ,[$(TargetSchema)].[app_tds_cohort],[$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]
-- - OutPut Table	: [$(TargetSchema)].[TDS_PHCPipelineMetricReport],[$(TargetSchema)].[TDS_PHCPipelineMetricReport]
-- - Change History:	Change Date		Changed by		Change Description
-- -					28-Mar-2019		SureshG			Modified TDS pipeline : Removed Twelve_Months_Eligible criteria	
-- -					29-Mar-2019		SureshG			Added filter criteria for Monotherapy/Polytherapy records	
-- -					14-Oct-2019		SupriyaS		Updated code for TDS metric to reflect the report format as per reported to PHC			
-- ---------------------------------------------------------------------- 
--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_update_New_17_Oct

SET NOCOUNT ON

SELECT CURRENT_TIMESTAMP AS START_TIMESTAMP

DECLARE @TotalPatientCount INT;
--DECLARE @TotalPatientCountDRECohort INT;
DECLARE @PatientInTDSCohort INT;
DECLARE @TotalPatientCountTDSCohort INT;
DECLARE @PatientInDRECohort INT;
DECLARE @AtLeastAED INT;
DECLARE @SynomaPatient INT; 
DECLARE @IndexDateQualifiedPatientCount INT;
DECLARE @ObservationPeriodPatientCnt INT;
DECLARE @EvalutionPeriodPatientCnt INT;
--DECLARE @DREPatientCount INT;
--DECLARE @NonDREPatientCount INT;
DECLARE @AgeQualifiedPatientCnt INT;
DECLARE @MeetPatientCount INT;
DECLARE @DoNotMeetPatientCount INT;
DECLARE @MeetPatientPercentage FLOAT;
DECLARE @DoNotMeetPatientPercentage FLOAT;
DECLARE @MeetCriteria Varchar (300) ;
DECLARE @DoNotMeetCriteria Varchar (300) ;
DECLARE @MeetPatientPercentageSHA FLOAT;
DECLARE @DoNotMeetPatientPercentageSHA FLOAT;
DECLARE @MeetCriteriaSHA Varchar (300) ;
DECLARE @DoNotMeetCriteriaSHA Varchar (300) ;
DECLARE @IndexEventCnt INT;
DECLARE @16AEDPatientCnt INT;
DECLARE @FailurePatientCount INT;
DECLARE @SuccessPatientCount INT;


IF OBJECT_ID('[$(TargetSchema)].[TDS_PHCPipelineMetricReport]','U') IS NOT NULL
	DROP TABLE [$(TargetSchema)].[TDS_PHCPipelineMetricReport]
	CREATE TABLE [$(TargetSchema)].[TDS_PHCPipelineMetricReport]
	(
	 [ID] DECIMAL(5,1)
	,[Criteria] VARCHAR (300)
	,[Meets criteria Patient count n(%)] VARCHAR (300) 
	,[Do not meet criteria Patient count n(%)] VARCHAR (300) 
	,[Meets criteria Patient count n(%) with SHA] VARCHAR (300) 
	,[Do not meet criteria Patient count n(%) with SHA] VARCHAR (300) 
	)



	
	/* Total patients(SynomaID) */
	select @SynomaPatient = COUNT(Distinct Patient_id) FROM [$(SrcSchema)].[PHPH_SynomaIDs] WITH(NOLOCK)
	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)],[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (1.0, 'Total Patients (SynomaID)', @SynomaPatient,'NA','NA','NA')

	SELECT @TotalPatientCount = COUNT(Distinct Patient_id) FROM [$(SrcSchema)].[src_REF_patient] WITH(NOLOCK)
	SELECT @MeetCriteria = CAST(@TotalPatientCount AS varchar(300))  +' ('+CAST(100 AS VARCHAR(300))+'%)';
	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)],[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (2.0, 'Match with SHA', @MeetCriteria,'',@MeetCriteria,'')

	/* Epilepsy Diagnosis */
	SELECT @MeetPatientCount = COUNT(Distinct Patient_id) from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 
	SELECT @DoNotMeetPatientCount = @TotalPatientCount -@MeetPatientCount
	SELECT @MeetPatientPercentage = (@MeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetPatientPercentageSHA = (@MeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetCriteria = CAST(@MeetPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @MeetCriteriaSHA = CAST(@MeetPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (2.1,'Epilepsy Diagnosis',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

	/* Atleast One AED */
	SELECT @AtLeastAED = COUNT(Distinct Patient_id) from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] WHERE  AED_Days >30 AND Patient_id IN (SELECT DISTINCT Patient_id from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 )
	SELECT @DoNotMeetPatientCount = @MeetPatientCount -@AtLeastAED
	SELECT @MeetPatientPercentage = (@AtLeastAED * 100.00)/ @MeetPatientCount
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @MeetPatientCount
	SELECT @MeetPatientPercentageSHA = (@AtLeastAED * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetCriteria = CAST(@AtLeastAED AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @MeetCriteriaSHA = CAST(@AtLeastAED AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (2.2,'At Least One AED(With > 30 days supply)',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)


	/* TDS Cohort Inclusions */
	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.0,'TDS Cohort Inclusions','','','','')

	/* Index Date Qualification */
	SELECT @IndexDateQualifiedPatientCount = COUNT(Distinct Patient_id) from [$(TargetSchema)].[app_tds_cohort] WITH(NOLOCK) WHERE ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late')   /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3  
	SELECT @DoNotMeetPatientCount = @AtLeastAED -@IndexDateQualifiedPatientCount
	SELECT @MeetPatientPercentage = (@IndexDateQualifiedPatientCount * 100.00)/ @AtLeastAED
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @AtLeastAED
	SELECT @MeetPatientPercentageSHA = (@IndexDateQualifiedPatientCount * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetCriteria = CAST(@IndexDateQualifiedPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @MeetCriteriaSHA = CAST(@IndexDateQualifiedPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.1,'Index Date Qualification',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

	/* Age >= 18 */
	SELECT @AgeQualifiedPatientCnt = COUNT(Distinct Patient_id) FROM [$(TargetSchema)].[app_tds_cohort] CM WITH(NOLOCK) WHERE  ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late')   /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3 AND Age >=18
	SELECT @DoNotMeetPatientCount = @IndexDateQualifiedPatientCount -@AgeQualifiedPatientCnt 
	SELECT @MeetPatientPercentage = (@AgeQualifiedPatientCnt * 100.00)/ @IndexDateQualifiedPatientCount
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @IndexDateQualifiedPatientCount
	SELECT @MeetPatientPercentageSHA = (@AgeQualifiedPatientCnt * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetCriteria = CAST(@AgeQualifiedPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @MeetCriteriaSHA = CAST(@AgeQualifiedPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.2,'Age >=18 at Index date',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

	/* Observation Period */
	SELECT @ObservationPeriodPatientCnt = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_tds_cohort] CM WITH(NOLOCK) WHERE ISEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late')  /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3  AND Age >=18 AND LOOKBACK_PRESENT = 1
	SELECT @DoNotMeetPatientCount = @AgeQualifiedPatientCnt -@ObservationPeriodPatientCnt 
	SELECT @MeetPatientPercentage = (@ObservationPeriodPatientCnt * 100.00)/ @AgeQualifiedPatientCnt
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @AgeQualifiedPatientCnt
	SELECT @MeetPatientPercentageSHA = (@ObservationPeriodPatientCnt * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetCriteria = CAST(@ObservationPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @MeetCriteriaSHA = CAST(@ObservationPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.3,'Data availability in Observation period',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

	/* Evaluation Period */
	SELECT @EvalutionPeriodPatientCnt = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_tds_cohort] CM WITH(NOLOCK) WHERE IsEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3 AND Age >=18 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1
	SELECT @DoNotMeetPatientCount = @ObservationPeriodPatientCnt -@EvalutionPeriodPatientCnt 
	SELECT @MeetPatientPercentage = (@EvalutionPeriodPatientCnt * 100.00)/ @ObservationPeriodPatientCnt
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @ObservationPeriodPatientCnt
	SELECT @MeetPatientPercentageSHA = (@EvalutionPeriodPatientCnt * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetCriteria = CAST(@EvalutionPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @MeetCriteriaSHA = CAST(@EvalutionPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.4,'Data availability in Evaluation period',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

	
	/* Index AED amongst16 AED */
	SELECT @16AEDPatientCnt = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_tds_cohort] CM WITH(NOLOCK) WHERE IsEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3 AND Age >=18 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1 and index_date is not null
	SELECT @DoNotMeetPatientCount = @EvalutionPeriodPatientCnt -@16AEDPatientCnt
	SELECT @MeetPatientPercentage = (@16AEDPatientCnt * 100.00)/ @EvalutionPeriodPatientCnt
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @EvalutionPeriodPatientCnt
	SELECT @MeetPatientPercentageSHA = (@16AEDPatientCnt * 100.00)/ @TotalPatientCount
	SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
	SELECT @MeetCriteria = CAST(@16AEDPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	SELECT @MeetCriteriaSHA = CAST(@16AEDPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';
	SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.5,'Index is AED amongst16 AED',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

	/*TDS Cohort(Unique Patient Count)*/
	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.5,'TDS Cohort(Unique Patient Count)',@16AEDPatientCnt,'', '', '')

	/*TDS Cohort(Index Events)*/
	SELECT @IndexEventCnt = COUNT(1) from [$(TargetSchema)].[app_tds_cohort] CM WITH(NOLOCK) WHERE IsEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3 AND Age >=18 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1 and index_date is not null
	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.6,'TDS Cohort(All Index Events)',@IndexEventCnt,'', '', '')

	/* TDS Events Count */
	SELECT @SuccessPatientCount = COUNT(1)  FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] (NOLOCK) WHERE Outcome_variable = 'Success'
	SELECT @DoNotMeetPatientCount = @IndexEventCnt - @SuccessPatientCount
	SELECT @MeetPatientPercentage = (@SuccessPatientCount * 100.00)/ @IndexEventCnt
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @IndexEventCnt
	SELECT @MeetCriteria = CAST(@SuccessPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'% of total index events)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.7,'Stable',@MeetCriteria,'NA','','')

	/* Failure Patient Count */
	SELECT @FailurePatientCount = COUNT(1)  FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] (NOLOCK) WHERE Outcome_variable = 'Failure'
	SELECT @DoNotMeetPatientCount = @IndexEventCnt - @FailurePatientCount
	SELECT @MeetPatientPercentage = (@FailurePatientCount * 100.00)/ @IndexEventCnt
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @IndexEventCnt
	SELECT @MeetCriteria = CAST(@FailurePatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'% of total index events)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.7,'Unstable',@MeetCriteria,'NA','','')

	SELECT @IndexEventCnt = COUNT(1)  FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] (NOLOCK) WHERE Outcome_variable is not NULL
	SELECT @SuccessPatientCount = COUNT(1)  FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] (NOLOCK) WHERE Outcome_variable = 'Success'
	SELECT @DoNotMeetPatientCount = @IndexEventCnt - @SuccessPatientCount
	SELECT @MeetPatientPercentage = (@SuccessPatientCount * 100.00)/ @IndexEventCnt
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @IndexEventCnt
	SELECT @MeetCriteria = CAST(@SuccessPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'% of First index events)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	
	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.8,'TDS Cohort(First Index Events)',@IndexEventCnt,'', '', '')

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.9,'Stable',@MeetCriteria,'NA','','')

	/* Failure Patient Count */
	SELECT @FailurePatientCount = COUNT(1)  FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] (NOLOCK) WHERE Outcome_variable = 'Failure'
	SELECT @DoNotMeetPatientCount = @IndexEventCnt - @FailurePatientCount
	SELECT @MeetPatientPercentage = (@FailurePatientCount * 100.00)/ @IndexEventCnt
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @IndexEventCnt
	SELECT @MeetCriteria = CAST(@FailurePatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'% of First index events)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';

	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (4.9,'Unstable',@MeetCriteria,'NA','','')

IF('$(RunDRECohort)'='Y')
Begin
	/* Patient In DRE Cohort
	Need to make sure that latest cohort table available for DRE and TDS should be in same schema
	 */
	SELECT @TotalPatientCountTDSCohort = Count(DISTINCT cm.Patient_id ) FROM [$(TargetSchema)].[app_tds_cohort] CM WHERE IsEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late')   /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3 AND Age >=18 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1 and index_date is not null
	SELECT @PatientInDRECohort = Count(DISTINCT cm.Patient_id) FROM [$(TargetSchema)].[app_tds_cohort] CM WHERE IsEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late')   /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1  AND LEN(AED) = 3 AND Age >=18 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1 AND Patient_ID IN(SELECT DISTINCT cm.Patient_id FROM [$(TargetSchema)].[app_dre_Cohort] CM WITH(NOLOCK) )
	SELECT @DoNotMeetPatientCount = @TotalPatientCountTDSCohort - @PatientInDRECohort
	SELECT @MeetPatientPercentage = (@PatientInDRECohort * 100.00)/ @TotalPatientCountTDSCohort
	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCountTDSCohort
	SELECT @MeetCriteria = CAST(@PatientInDRECohort AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'% of patient count in TDS cohort)';
	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+'%)';
	
	INSERT INTO [$(TargetSchema)].[TDS_PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n(%)], [Do not meet criteria Patient count n(%)] ,[Do not meet criteria Patient count n(%) with SHA],[Meets criteria Patient count n(%) with SHA])
	VALUES (5.0,'Patients Present in DRE Cohort',@MeetCriteria,'NA','','')
	select @MeetCriteria

End

