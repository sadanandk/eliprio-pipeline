-- - AUTHOR			: Suresh Gajera 
-- - USED BY		: 
-- - 
-- - PURPOSE		: Generate the patient distribution based on different age groups
-- - NOTE			:  PLATFORM-1772
-- - Execution		: 
-- - Date			: 10-Sep-2019			

-- - Change History:	Change Date				Change Description

-- ---------------------------------------------------------------------- 


--********************************************************************************START : CCI Distribution************************************************************************************


-------------------------------------------------------------START: Train Data (CCI)---------------------------------------------------------------------

 DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] WHERE REPLACE(LTRIM(REPLACE(patient_id,'0',' ')),' ','0') IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) ))


SELECT CASE 
			WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN b.age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN CI.CCI_Score = 0 THEN '0'
			WHEN CI.CCI_Score = 1 THEN '1'
			WHEN CI.CCI_Score = 2 THEN '2'
			WHEN CI.CCI_Score = 3 THEN '3'
			WHEN CI.CCI_Score = 4 THEN '4'
			WHEN CI.CCI_Score >= 5 THEN '>=5'
		END [CCIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)]
FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
WHERE REPLACE(LTRIM(REPLACE(CI.patient_id,'0',' ')),' ','0') IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) )
GROUP BY 
	CASE 
		WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN b.age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN CI.CCI_Score = 0 THEN '0'
		WHEN CI.CCI_Score = 1 THEN '1'
		WHEN CI.CCI_Score = 2 THEN '2'
		WHEN CI.CCI_Score = 3 THEN '3'
		WHEN CI.CCI_Score = 4 THEN '4'
		WHEN CI.CCI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2

-------------------------------------------------------------END: Train Data (CCI)---------------------------------------------------------------------

-------------------------------------------------------------START: Test Data (CCI)---------------------------------------------------------------------

 DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] WHERE REPLACE(LTRIM(REPLACE(patient_id,'0',' ')),' ','0') NOT IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) ))


SELECT CASE 
			WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN b.age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN CI.CCI_Score = 0 THEN '0'
			WHEN CI.CCI_Score = 1 THEN '1'
			WHEN CI.CCI_Score = 2 THEN '2'
			WHEN CI.CCI_Score = 3 THEN '3'
			WHEN CI.CCI_Score = 4 THEN '4'
			WHEN CI.CCI_Score >= 5 THEN '>=5'
		END [CCIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)]
FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
WHERE REPLACE(LTRIM(REPLACE(CI.patient_id,'0',' ')),' ','0') NOT IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) )
GROUP BY 
	CASE 
		WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN b.age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN CI.CCI_Score = 0 THEN '0'
		WHEN CI.CCI_Score = 1 THEN '1'
		WHEN CI.CCI_Score = 2 THEN '2'
		WHEN CI.CCI_Score = 3 THEN '3'
		WHEN CI.CCI_Score = 4 THEN '4'
		WHEN CI.CCI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2

-------------------------------------------------------------END: Test Data (CCI)---------------------------------------------------------------------

-------------------------------------------------------------START: SHA2 Data(CCI)---------------------------------------------------------------------

 DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA2_pipeline_target_new].[app_tds_feature_IndexDate_CCI_ECI] )

SELECT CASE 
			WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN b.age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN CI.CCI_Score = 0 THEN '0'
			WHEN CI.CCI_Score = 1 THEN '1'
			WHEN CI.CCI_Score = 2 THEN '2'
			WHEN CI.CCI_Score = 3 THEN '3'
			WHEN CI.CCI_Score = 4 THEN '4'
			WHEN CI.CCI_Score >= 5 THEN '>=5'
		END [CCIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)]
FROM [SHA2_pipeline_target_new].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN [SHA2_pipeline_target_new].[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  [SHA2_pipeline_target_new].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
GROUP BY 
	CASE 
		WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN b.age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN CI.CCI_Score = 0 THEN '0'
		WHEN CI.CCI_Score = 1 THEN '1'
		WHEN CI.CCI_Score = 2 THEN '2'
		WHEN CI.CCI_Score = 3 THEN '3'
		WHEN CI.CCI_Score = 4 THEN '4'
		WHEN CI.CCI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2

-------------------------------------------------------------END: SHA2 Data(CCI)---------------------------------------------------------------------

-------------------------------------------------------------START: PHS Data(CCI)---------------------------------------------------------------------
 DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA2_Synoma_ID_TGT].[app_tds_feature_IndexDate_CCI_ECI] )

SELECT CASE 
			WHEN age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN CCI_Score = 0 THEN '0'
			WHEN CCI_Score = 1 THEN '1'
			WHEN CCI_Score = 2 THEN '2'
			WHEN CCI_Score = 3 THEN '3'
			WHEN CCI_Score = 4 THEN '4'
			WHEN CCI_Score >= 5 THEN '>=5'
		END [CCIScore]
	,COUNT(DISTINCT A.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]

	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT A.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT A.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)] 
FROM [SHA2_Synoma_ID_TGT].[app_tds_feature_IndexDate_CCI_ECI] A
JOIN (SELECT patient_id ,MIN(Valid_index) AS Valid_Index from [SHA2_Synoma_ID_TGT].[app_tds_feature_IndexDate_CCI_ECI] A GROUP BY patient_id) B
ON A.patient_Id = B.patient_Id
AND a.Valid_Index = B.Valid_index
GROUP BY 
	CASE 
		WHEN age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN CCI_Score = 0 THEN '0'
		WHEN CCI_Score = 1 THEN '1'
		WHEN CCI_Score = 2 THEN '2'
		WHEN CCI_Score = 3 THEN '3'
		WHEN CCI_Score = 4 THEN '4'
		WHEN CCI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2

-------------------------------------------------------------END: PHS Data(CCI)---------------------------------------------------------------------


--********************************************************************************END : CCI Distribution************************************************************************************

--********************************************************************************START : ECI Distribution************************************************************************************



-------------------------------------------------------------START: Train Data(ECI)---------------------------------------------------------------------

DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] WHERE REPLACE(LTRIM(REPLACE(patient_id,'0',' ')),' ','0') IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) ))

SELECT CASE 
			WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN b.age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN CI.ECI_Score = 0 THEN '0'
			WHEN CI.ECI_Score = 1 THEN '1'
			WHEN CI.ECI_Score = 2 THEN '2'
			WHEN CI.ECI_Score = 3 THEN '3'
			WHEN CI.ECI_Score = 4 THEN '4'
			WHEN CI.ECI_Score >= 5 THEN '>=5'
		END [ECIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)]
FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
WHERE REPLACE(LTRIM(REPLACE(CI.patient_id,'0',' ')),' ','0') IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) )
GROUP BY 
	CASE 
		WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN b.age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN CI.ECI_Score = 0 THEN '0'
		WHEN CI.ECI_Score = 1 THEN '1'
		WHEN CI.ECI_Score = 2 THEN '2'
		WHEN CI.ECI_Score = 3 THEN '3'
		WHEN CI.ECI_Score = 4 THEN '4'
		WHEN CI.ECI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2


-------------------------------------------------------------END: Train Data(ECI)---------------------------------------------------------------------

-------------------------------------------------------------START: Test Data(ECI)---------------------------------------------------------------------

DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] WHERE REPLACE(LTRIM(REPLACE(patient_id,'0',' ')),' ','0') NOT IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) ))

SELECT CASE 
			WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN b.age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN CI.ECI_Score = 0 THEN '0'
			WHEN CI.ECI_Score = 1 THEN '1'
			WHEN CI.ECI_Score = 2 THEN '2'
			WHEN CI.ECI_Score = 3 THEN '3'
			WHEN CI.ECI_Score = 4 THEN '4'
			WHEN CI.ECI_Score >= 5 THEN '>=5'
		END [ECIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)]
FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
WHERE REPLACE(LTRIM(REPLACE(CI.patient_id,'0',' ')),' ','0') NOT IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK) )
GROUP BY 
	CASE 
		WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN b.age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN CI.ECI_Score = 0 THEN '0'
		WHEN CI.ECI_Score = 1 THEN '1'
		WHEN CI.ECI_Score = 2 THEN '2'
		WHEN CI.ECI_Score = 3 THEN '3'
		WHEN CI.ECI_Score = 4 THEN '4'
		WHEN CI.ECI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2

-------------------------------------------------------------END: Test Data(ECI)---------------------------------------------------------------------

-------------------------------------------------------------START: SHA2 Data(ECI)---------------------------------------------------------------------

DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA2_pipeline_target_new].[app_tds_feature_IndexDate_CCI_ECI] )

SELECT CASE 
			WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN b.age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN CI.ECI_Score = 0 THEN '0'
			WHEN CI.ECI_Score = 1 THEN '1'
			WHEN CI.ECI_Score = 2 THEN '2'
			WHEN CI.ECI_Score = 3 THEN '3'
			WHEN CI.ECI_Score = 4 THEN '4'
			WHEN CI.ECI_Score >= 5 THEN '>=5'
		END [ECIScore]
	,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT CI.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT CI.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)] 
FROM SHA2_pipeline_target_new.[app_tds_feature_IndexDate_CCI_ECI] CI WITH(NOLOCK)
INNER JOIN SHA2_pipeline_target_new.[app_tds_Cohort] A WITH(NOLOCK)
ON CI.Patient_ID = A.Patient_ID
AND CI.Valid_index = A.INDEX_DATE
INNER JOIN  SHA2_pipeline_target_new.[app_tds_int_BaselineModel_Input_FirstIndex]  B WITH (NOLOCK)
ON A.patient_id = B.patient_id
AND A.INDEX_DATE = B.Valid_index
GROUP BY 
	CASE 
		WHEN b.age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN b.age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN b.age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN b.age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN CI.ECI_Score = 0 THEN '0'
		WHEN CI.ECI_Score = 1 THEN '1'
		WHEN CI.ECI_Score = 2 THEN '2'
		WHEN CI.ECI_Score = 3 THEN '3'
		WHEN CI.ECI_Score = 4 THEN '4'
		WHEN CI.ECI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2

-------------------------------------------------------------END: SHA2 Data(ECI)---------------------------------------------------------------------

-------------------------------------------------------------START: PHC Data(ECI)---------------------------------------------------------------------

 
DECLARE @TotalPatientCount As int
SET @TotalPatientCount = (SELECT COUNT(DISTINCT patient_id)  FROM [SHA2_Synoma_ID_TGT].[app_tds_feature_IndexDate_CCI_ECI] )
 
 SELECT CASE 
			WHEN age BETWEEN 18 AND 34 THEN '18 - 34'
			WHEN age BETWEEN 35 AND 49 THEN '35 - 49'
			WHEN age BETWEEN 50 AND 64 THEN '50 - 64'
			WHEN age >= 65 THEN '>=65'
	   END AS Age,
	   CASE 
			WHEN ECI_Score = 0 THEN '0'
			WHEN ECI_Score = 1 THEN '1'
			WHEN ECI_Score = 2 THEN '2'
			WHEN ECI_Score = 3 THEN '3'
			WHEN ECI_Score = 4 THEN '4'
			WHEN ECI_Score >= 5 THEN '>=5'
		END [ECIScore]
	,COUNT(DISTINCT A.Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]

	,CASE WHEN ((CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) IS NULL) or (CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))=0))
	  THEN ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT A.Patient_ID) AS MONEY)),1),'.00',''),'')
	  ELSE
	  concat(ISNULL(REPLACE(CONVERT(Varchar(100),(CAST(COUNT(DISTINCT A.Patient_ID) AS MONEY)),1),'.00',''),''),'(',ISNULL(CAST(CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2))  AS varchar(100)),''),'%)')
	  END  AS [Statistic (n%)] 
from [SHA2_Synoma_ID_TGT].[app_tds_feature_IndexDate_CCI_ECI] A
JOIN (SELECT patient_id ,MIN(Valid_index) AS Valid_Index from [SHA2_Synoma_ID_TGT].[app_tds_feature_IndexDate_CCI_ECI] A GROUP BY patient_id) B
ON A.patient_Id = B.patient_Id
AND a.Valid_Index = B.Valid_index
GROUP BY 
	CASE 
		WHEN age BETWEEN 18 AND 34 THEN '18 - 34'
		WHEN age BETWEEN 35 AND 49 THEN '35 - 49'
		WHEN age BETWEEN 50 AND 64 THEN '50 - 64'
		WHEN age >= 65 THEN '>=65'
	END,
	CASE 
		WHEN ECI_Score = 0 THEN '0'
		WHEN ECI_Score = 1 THEN '1'
		WHEN ECI_Score = 2 THEN '2'
		WHEN ECI_Score = 3 THEN '3'
		WHEN ECI_Score = 4 THEN '4'
		WHEN ECI_Score >= 5 THEN '>=5'
	END
ORDER BY 1 ,2

-------------------------------------------------------------END: PHC Data(ECI)---------------------------------------------------------------------

--********************************************************************************END : ECI Distribution************************************************************************************