-- ---------------------------------------------------------------------
-- - CITIUS TECH
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create scripts for 
-- - NOTE      :	* First Rx/Dx to Last Rx/Dx (median , Quartile)
-- -				* index date to last rx (median , Quartile)
-- -				* First encounter to index(median , Quartile)

-- -			    
-- - Execution : 
-- - Date      : 18-Oct-2019
-- - Change History:	Change Date				Change Description
-- - 					18-Nov-2019				Added 2 digits after decimal and added space between values and bracket
-- ----------------------------------------------------------------------

:setvar TargetSchema SHA_Pipeline_All_Target_DRE6_5_TDS4_4
SET NOCOUNT ON 

IF OBJECT_ID('tempdb..#Days_Analysis') IS NOT NULL
DROP TABLE #Days_Analysis
SELECT A.Patient_id,Valid_index
		,[Age]
		,[First_Encounter]
		,[Last_Encounter]
		,YEAR(Valid_index)								AS [Index_Year]
		,DATEDIFF(dd,Valid_index,Last_Encounter)		AS [Evaluation period]
		,DATEDIFF(dd,First_Encounter,Last_Encounter)	AS [Entire timeline]
		,DATEDIFF(dd,First_Encounter,Valid_index)		AS [Observation period]
INTO #Days_Analysis
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] A
JOIN [$(TargetSchema)].[app_int_Patient_Profile] B
ON A.patient_id = B.patient_id
WHERE REPLACE(LTRIM(REPLACE(A.patient_id,'0',' ')),' ','0')  NOT IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK))


--select * from #Days_Analysis
----*********************************************First encounter to last encounter*******************************************

IF OBJECT_ID('tempdb..#First_Last_Enc_days') IS NOT NULL
DROP TABLE #First_Last_Enc_days

SELECT ROW_NUMBER()OVER (ORDER BY [Entire timeline]) AS ID,* INTO #First_Last_Enc_days FROM #Days_Analysis

BEGIN 

DECLARE @MAXID INT 
DECLARE @Lower_Min INT
DECLARE @Lower_Max INT
DECLARE @Upper_Min INT
DECLARE @Upper_Max INT
DECLARE @MAXID_Lower_Odd INT
DECLARE @Median_Lower_Odd DECIMAL(10,2)
DECLARE @MAXID_Upper_Odd INT
DECLARE @Median_Upper_Odd DECIMAL(10,2)
DECLARE @MAXID_Lower_Even INT
DECLARE @Median_Lower_Even DECIMAL(10,2)
DECLARE @MAXID_Upper_Even INT
DECLARE @Median_Upper_Even DECIMAL(10,2)
DECLARE @Median_All DECIMAL(10,2)
DECLARE @Entire Varchar(50)
DECLARE @Evaluation Varchar(50)
DECLARE @Observation Varchar(50)


select @MAXID = MAX(ID) from #First_Last_Enc_days
PRINT @MAXID
PRINT @MAXID%2

IF @MAXID%2 =0
		BEGIN
			   SET @Lower_Min = 1
			   SET @Lower_Max = @MAXID/2
			   SET @Upper_Min = (@MAXID/2)+1
			   SET @Upper_Max = @MAXID

			     SELECT @Median_All = AVG(CAST([Entire timeline] AS FLOAT)) FROM #First_Last_Enc_days WHERE ID IN ((@MAXID/2),(@MAXID/2)+1)
				
				IF OBJECT_ID('tempdb..#LowerHalf_even') IS NOT NULL
				DROP TABLE #LowerHalf_even
			    SELECT  Row_NUmber()over (order by [Entire timeline]) as ID_Lower,* INTO #LowerHalf_even FROM #First_Last_Enc_days WHERE ID BETWEEN @Lower_Min AND @Lower_Max

				SELECT @MAXID_Lower_Even = MAX(ID_Lower) FROM #LowerHalf_even
				IF @MAXID_Lower_Even%2 =0
						BEGIN
							   SELECT @Median_Lower_Even = AVG(CAST([Entire timeline] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_Lower_Even/2),(@MAXID_Lower_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_Lower_Even = AVG(CAST([Entire timeline] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_Lower_Even+1)/2)


				IF OBJECT_ID('tempdb..#UpperHalf_even') IS NOT NULL
				DROP TABLE #UpperHalf_even
			    SELECT  Row_NUmber()over (order by [Entire timeline]) as ID_Upper,* INTO #UpperHalf_even FROM #First_Last_Enc_days WHERE ID BETWEEN @Upper_Min AND @Upper_Max

				SELECT @MAXID_Upper_Even = MAX(ID_Upper) FROM #UpperHalf_even
				IF @MAXID_Upper_Even%2 =0
						BEGIN
							   SELECT @Median_Upper_Even = AVG(CAST([Entire timeline] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_Upper_Even/2),(@MAXID_Upper_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_Upper_Even = AVG(CAST([Entire timeline] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_Upper_Even+1)/2)
				
			SET @Entire = concat(CAST(@Median_All AS VARCHAR(10)),' ( Q1 = ', CAST(@Median_Lower_Even AS VARCHAR(10)),' ; Q3 = '+CAST(@Median_Upper_Even AS VARCHAR(10)), ')') 
			

		END
ELSE 
		BEGIN 
			  SET @Lower_Min = 1
			  SET @Lower_Max = (@MAXID+1)/2
			  SET @Upper_Min = (@MAXID+1)/2
			  SET @Upper_Max = @MAXID

			   SELECT @Median_All = AVG(CAST([Entire timeline] AS FLOAT)) FROM #First_Last_Enc_days WHERE ID IN ((@MAXID+1)/2)

			    IF OBJECT_ID('tempdb..#LowerHalf_odd') IS NOT NULL
				DROP TABLE #LowerHalf_odd
			    SELECT  Row_NUmber()over (order by [Entire timeline]) as ID_Lower,* INTO #LowerHalf_odd FROM #First_Last_Enc_days WHERE ID BETWEEN @Lower_Min AND @Lower_Max
				
				SELECT @MAXID_Lower_Odd = MAX(ID_Lower) FROM #LowerHalf_odd
				IF @MAXID_Lower_Odd%2 =0
						BEGIN
							   SELECT @Median_Lower_Odd = AVG(CAST([Entire timeline] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_Lower_Odd/2),(@MAXID_Lower_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_Lower_Odd = AVG(CAST([Entire timeline] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_Lower_Odd+1)/2)
				

			    IF OBJECT_ID('tempdb..#UpperHalf_odd') IS NOT NULL
				DROP TABLE #UpperHalf_odd
			    SELECT  Row_NUmber()over (order by [Entire timeline]) as ID_Upper,* INTO #UpperHalf_odd FROM #First_Last_Enc_days WHERE ID BETWEEN @Upper_Min AND @Upper_Max

				SELECT @MAXID_Upper_Odd = MAX(ID_Upper) FROM #UpperHalf_odd
				IF @MAXID_Upper_Odd%2 =0
						BEGIN
							   SELECT @Median_Upper_Odd = AVG(CAST([Entire timeline] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_Upper_Odd/2),(@MAXID_Upper_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_Upper_Odd = AVG(CAST([Entire timeline] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_Upper_Odd+1)/2)
				
			SET @Entire = concat(CAST(@Median_All AS VARCHAR(10)),' ( Q1 = ', CAST(@Median_Lower_Odd AS VARCHAR(10)),' ; Q3 = '+CAST(@Median_Upper_Odd AS VARCHAR(10)), ')') 
		END
		
END



----*********************************************Index date to last encounter*******************************************


IF OBJECT_ID('tempdb..#Index_Last_Enc_days') IS NOT NULL
DROP TABLE #Index_Last_Enc_days

SELECT ROW_NUMBER()OVER (ORDER BY [Evaluation period]) AS ID,* INTO #Index_Last_Enc_days FROM #Days_Analysis

BEGIN 

DECLARE @MAXID_1 INT 
DECLARE @Lower_1_Min INT
DECLARE @Lower_1_Max INT
DECLARE @Upper_1_Min INT
DECLARE @Upper_1_Max INT
DECLARE @MAXID_1_Lower_Odd INT
DECLARE @Median_1_Lower_Odd DECIMAL(10,2)
DECLARE @MAXID_1_Upper_Odd INT
DECLARE @Median_1_Upper_Odd DECIMAL(10,2)
DECLARE @MAXID_1_Lower_Even INT
DECLARE @Median_1_Lower_Even DECIMAL(10,2)
DECLARE @MAXID_1_Upper_Even INT
DECLARE @Median_1_Upper_Even DECIMAL(10,2)
DECLARE @Median_1_All DECIMAL(10,2)


select @MAXID_1 = MAX(ID) from #Index_Last_Enc_days
PRINT @MAXID_1
PRINT @MAXID_1%2

IF @MAXID_1%2 =0
		BEGIN
			   SET @Lower_1_Min = 1
			   SET @Lower_1_Max = @MAXID_1/2
			   SET @Upper_1_Min = (@MAXID_1/2)+1
			   SET @Upper_1_Max = @MAXID_1

			     SELECT @Median_1_All = AVG(CAST([Evaluation period] AS FLOAT)) FROM #Index_Last_Enc_days WHERE ID IN ((@MAXID_1/2),(@MAXID_1/2)+1)
				
				IF OBJECT_ID('tempdb..#LowerHalf_1_even') IS NOT NULL
				DROP TABLE #LowerHalf_1_even
			    SELECT  Row_NUmber()over (order by [Evaluation period]) as ID_Lower,* INTO #LowerHalf_1_even FROM #Index_Last_Enc_days WHERE ID BETWEEN @Lower_1_Min AND @Lower_1_Max

				SELECT @MAXID_1_Lower_Even = MAX(ID_Lower) FROM #LowerHalf_1_even
				IF @MAXID_1_Lower_Even%2 =0
						BEGIN
							   SELECT @Median_1_Lower_Even = AVG(CAST([Evaluation period] AS FLOAT)) FROM #LowerHalf_1_even WHERE ID_Lower IN ((@MAXID_1_Lower_Even/2),(@MAXID_1_Lower_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_1_Lower_Even = AVG(CAST([Evaluation period] AS FLOAT)) FROM #LowerHalf_1_even WHERE ID_Lower IN ((@MAXID_1_Lower_Even+1)/2)


				IF OBJECT_ID('tempdb..#UpperHalf_1_even') IS NOT NULL
				DROP TABLE #UpperHalf_1_even
			    SELECT  Row_NUmber()over (order by [Evaluation period]) as ID_Upper,* INTO #UpperHalf_1_even FROM #Index_Last_Enc_days WHERE ID BETWEEN @Upper_1_Min AND @Upper_1_Max

				SELECT @MAXID_1_Upper_Even = MAX(ID_Upper) FROM #UpperHalf_1_even
				IF @MAXID_1_Upper_Even%2 =0
						BEGIN
							   SELECT @Median_1_Upper_Even = AVG(CAST([Evaluation period] AS FLOAT)) FROM #UpperHalf_1_even WHERE ID_Upper IN ((@MAXID_1_Upper_Even/2),(@MAXID_1_Upper_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_1_Upper_Even = AVG(CAST([Evaluation period] AS FLOAT)) FROM #UpperHalf_1_even WHERE ID_Upper IN ((@MAXID_1_Upper_Even+1)/2)
				

				SET @Evaluation = concat(CAST(@Median_1_All AS VARCHAR(10)),' ( Q1 = ', CAST(@Median_1_Lower_Even AS VARCHAR(10)),' ; Q3 = '+CAST(@Median_1_Upper_Even AS VARCHAR(10)), ')') 
				   
		END
ELSE 
		BEGIN 
			  SET @Lower_1_Min = 1
			  SET @Lower_1_Max = (@MAXID_1+1)/2
			  SET @Upper_1_Min = (@MAXID_1+1)/2
			  SET @Upper_1_Max = @MAXID_1

			   SELECT @Median_1_All = AVG(CAST([Evaluation period] AS FLOAT)) FROM #Index_Last_Enc_days WHERE ID IN ((@MAXID_1+1)/2)

			    IF OBJECT_ID('tempdb..#LowerHalf_1_odd') IS NOT NULL
				DROP TABLE #LowerHalf_1_odd
			    SELECT  Row_NUmber()over (order by [Evaluation period]) as ID_Lower,* INTO #LowerHalf_1_odd FROM #Index_Last_Enc_days WHERE ID BETWEEN @Lower_1_Min AND @Lower_1_Max
				
				SELECT @MAXID_1_Lower_Odd = MAX(ID_Lower) FROM #LowerHalf_1_odd
				IF @MAXID_1_Lower_Odd%2 =0
						BEGIN
							   SELECT @Median_1_Lower_Odd = AVG(CAST([Evaluation period] AS FLOAT)) FROM #LowerHalf_1_odd WHERE ID_Lower IN ((@MAXID_1_Lower_Odd/2),(@MAXID_1_Lower_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_1_Lower_Odd = AVG(CAST([Evaluation period] AS FLOAT)) FROM #LowerHalf_1_odd WHERE ID_Lower IN ((@MAXID_1_Lower_Odd+1)/2)
				

			    IF OBJECT_ID('tempdb..#UpperHalf_1_odd') IS NOT NULL
				DROP TABLE #UpperHalf_1_odd
			    SELECT  Row_NUmber()over (order by [Evaluation period]) as ID_Upper,* INTO #UpperHalf_1_odd FROM #Index_Last_Enc_days WHERE ID BETWEEN @Upper_1_Min AND @Upper_1_Max

				SELECT @MAXID_1_Upper_Odd = MAX(ID_Upper) FROM #UpperHalf_1_odd
				IF @MAXID_1_Upper_Odd%2 =0
						BEGIN
							   SELECT @Median_1_Upper_Odd = AVG(CAST([Evaluation period] AS FLOAT)) FROM #UpperHalf_1_odd WHERE ID_Upper IN ((@MAXID_1_Upper_Odd/2),(@MAXID_1_Upper_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_1_Upper_Odd = AVG(CAST([Evaluation period] AS FLOAT)) FROM #UpperHalf_1_odd WHERE ID_Upper IN ((@MAXID_1_Upper_Odd+1)/2)
				
				
			SET @Evaluation = concat(CAST(@Median_1_All AS VARCHAR(10)),' ( Q1 = ', CAST(@Median_1_Lower_Odd AS VARCHAR(10)),' ; Q3 = '+CAST(@Median_1_Upper_Odd AS VARCHAR(10)), ')') 
		END
		
END


----*********************************************First encounter to index date*******************************************


IF OBJECT_ID('tempdb..#First_enc_Index_days') IS NOT NULL
DROP TABLE #First_enc_Index_days

SELECT ROW_NUMBER()OVER (ORDER BY [Observation period]) AS ID,* INTO #First_enc_Index_days FROM #Days_Analysis

BEGIN 

DECLARE @MAXID_2 INT 
DECLARE @Lower_2_Min INT
DECLARE @Lower_2_Max INT
DECLARE @Upper_2_Min INT
DECLARE @Upper_2_Max INT
DECLARE @MAXID_2_Lower_Odd INT
DECLARE @Median_2_Lower_Odd DECIMAL(10,2)
DECLARE @MAXID_2_Upper_Odd INT
DECLARE @Median_2_Upper_Odd DECIMAL(10,2)
DECLARE @MAXID_2_Lower_Even INT
DECLARE @Median_2_Lower_Even DECIMAL(10,2)
DECLARE @MAXID_2_Upper_Even INT
DECLARE @Median_2_Upper_Even DECIMAL(10,2)
DECLARE @Median_2_All DECIMAL(10,2)


select @MAXID_2 = MAX(ID) from #First_enc_Index_days
PRINT @MAXID_2
PRINT @MAXID_2%2

IF @MAXID_2%2 =0
		BEGIN
			   SET @Lower_2_Min = 1
			   SET @Lower_2_Max = @MAXID_2/2
			   SET @Upper_2_Min = (@MAXID_2/2)+1
			   SET @Upper_2_Max = @MAXID_2

			     SELECT @Median_2_All = AVG(CAST([Observation period] AS FLOAT)) FROM #First_enc_Index_days WHERE ID IN ((@MAXID_2/2),(@MAXID_2/2)+1)
				
				IF OBJECT_ID('tempdb..#LowerHalf_2_even') IS NOT NULL
				DROP TABLE #LowerHalf_2_even
			    SELECT  Row_NUmber()over (order by [Observation period]) as ID_Lower,* INTO #LowerHalf_2_even FROM #First_enc_Index_days WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max

				SELECT @MAXID_2_Lower_Even = MAX(ID_Lower) FROM #LowerHalf_2_even
				IF @MAXID_2_Lower_Even%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Even = AVG(CAST([Observation period] AS FLOAT)) FROM #LowerHalf_2_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even/2),(@MAXID_2_Lower_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Even = AVG(CAST([Observation period] AS FLOAT)) FROM #LowerHalf_2_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even+1)/2)


				IF OBJECT_ID('tempdb..#UpperHalf_2_even') IS NOT NULL
				DROP TABLE #UpperHalf_2_even
			    SELECT  Row_NUmber()over (order by [Observation period]) as ID_Upper,* INTO #UpperHalf_2_even FROM #First_enc_Index_days WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Even = MAX(ID_Upper) FROM #UpperHalf_2_even
				IF @MAXID_2_Upper_Even%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Even = AVG(CAST([Observation period] AS FLOAT)) FROM #UpperHalf_2_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even/2),(@MAXID_2_Upper_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Even = AVG(CAST([Observation period] AS FLOAT)) FROM #UpperHalf_2_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even+1)/2)
				

			SET @Observation = concat(CAST(@Median_2_All AS VARCHAR(10)),' ( Q1 = ', CAST(@Median_2_Lower_Even AS VARCHAR(10)),' ; Q3 = '+CAST(@Median_2_Upper_Even AS VARCHAR(10)), ')') 
		END
ELSE 
		BEGIN 
			  SET @Lower_2_Min = 1
			  SET @Lower_2_Max = (@MAXID_2+1)/2
			  SET @Upper_2_Min = (@MAXID_2+1)/2
			  SET @Upper_2_Max = @MAXID_2

			   SELECT @Median_2_All = AVG(CAST([Observation period] AS FLOAT)) FROM #First_enc_Index_days WHERE ID IN ((@MAXID_2+1)/2)

			    IF OBJECT_ID('tempdb..#LowerHalf_2_odd') IS NOT NULL
				DROP TABLE #LowerHalf_2_odd
			    SELECT  Row_NUmber()over (order by [Observation period]) as ID_Lower,* INTO #LowerHalf_2_odd FROM #First_enc_Index_days WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max
				
				SELECT @MAXID_2_Lower_Odd = MAX(ID_Lower) FROM #LowerHalf_2_odd
				IF @MAXID_2_Lower_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Odd = AVG(CAST([Observation period] AS FLOAT)) FROM #LowerHalf_2_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd/2),(@MAXID_2_Lower_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Odd = AVG(CAST([Observation period] AS FLOAT)) FROM #LowerHalf_2_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd+1)/2)
				

			    IF OBJECT_ID('tempdb..#UpperHalf_2_odd') IS NOT NULL
				DROP TABLE #UpperHalf_2_odd
			    SELECT  Row_NUmber()over (order by [Observation period]) as ID_Upper,* INTO #UpperHalf_2_odd FROM #First_enc_Index_days WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Odd = MAX(ID_Upper) FROM #UpperHalf_2_odd
				IF @MAXID_2_Upper_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Odd = AVG(CAST([Observation period] AS FLOAT)) FROM #UpperHalf_2_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd/2),(@MAXID_2_Upper_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Odd = AVG(CAST([Observation period] AS FLOAT)) FROM #UpperHalf_2_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd+1)/2)
				
				
			SET @Observation = concat(CAST(@Median_2_All AS VARCHAR(10)),' ( Q1 = ', CAST(@Median_2_Lower_Odd AS VARCHAR(10)),' ; Q3 = '+CAST(@Median_2_Upper_Odd AS VARCHAR(10)), ')') 
		END
		
END


IF OBJECT_ID('[$(TargetSchema)].[TDS_Patient_timeline_statistic_Test]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[TDS_Patient_timeline_statistic_Test]

CREATE TABLE [$(TargetSchema)].[TDS_Patient_timeline_statistic_Test]
(
 [Id] DECIMAL(5,1)
,[Variable] VARCHAR (50)
,[Value] VARCHAR (100)
)

INSERT INTO [$(TargetSchema)].[TDS_Patient_timeline_statistic_Test] ([ID],[Variable], [Value])
VALUES (1.1, 'Entire Timeline', @Entire)

INSERT INTO [$(TargetSchema)].[TDS_Patient_timeline_statistic_Test] ([ID],[Variable], [Value])
VALUES (1.2, 'Evaluation Period', @Evaluation)

INSERT INTO [$(TargetSchema)].[TDS_Patient_timeline_statistic_Test] ([ID],[Variable], [Value])
VALUES (1.3, 'Observation Period', @Observation)
