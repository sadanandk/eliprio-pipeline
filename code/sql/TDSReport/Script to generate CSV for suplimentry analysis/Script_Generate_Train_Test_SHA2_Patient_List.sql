/* This script is used to generate list of patients used in pipeline for cohort characterization */


----SHA1 Train data   Std_Rpt_TDS_Train_patient_list.csv
SELECT Patient_id ,Valid_index ,ROW_NUMBER() OVER (PARTITION BY A.Patient_id ORDER BY Valid_Index) AS Index_id 
FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_int_BaselineModel_Input_IndexDate] A
WHERE REPLACE(LTRIM(REPLACE(A.patient_id,'0',' ')),' ','0') IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK))
ORDER BY 1,2,3

--SHA2 data  std_Rpt_TDS_SHA_patient_list.csv
SELECT Patient_id ,Valid_index ,ROW_NUMBER() OVER (PARTITION BY A.Patient_id ORDER BY Valid_Index) AS Index_id 
FROM [SHA2_pipeline_target_new].[app_tds_int_BaselineModel_Input_IndexDate] A
ORDER BY 1,2,3

--SHA1 Test data Std_Rpt_TDS_Test_patient_list.csv

SELECT Patient_id ,Valid_index ,ROW_NUMBER() OVER (PARTITION BY A.Patient_id ORDER BY Valid_Index) AS Index_id 
FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[app_tds_int_BaselineModel_Input_IndexDate] A
WHERE REPLACE(LTRIM(REPLACE(A.patient_id,'0',' ')),' ','0') NOT IN (SELECT DISTINCT Patient_id FROM [SHA_Pipeline_All_Target_DRE6_5_TDS4_4].[TDS_AED_SHA_C4.2_AllIndex_Mono_Train](NOLOCK))
ORDER BY 1,2,3



 

