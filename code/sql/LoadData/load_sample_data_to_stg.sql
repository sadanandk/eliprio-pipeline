-- ------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Load Stg data to pipeline tables 
-- - NOTE      : 
-- -			 	 
--				 
-- - Execution : 
-- - Date      : 23-Sept-2018		
-- - Change History:	
-- - Change Date		: 03-Dec-2018
-- - Change Description	: Patient_Eligibility & Time Dimension script updated. Instead of considering the data from csv files the script is updated to populate it from the raw data.
 					
-- ---------------------------------------------------------------------- 


--:Setvar Schema TestPipeline_stg

truncate table [$(TargetSchema)].[src_rx_claims]
insert into [$(TargetSchema)].[src_rx_claims]
select *
from [$(TargetSchema)].[sample_src_rx_claims]

truncate table [$(TargetSchema)].[src_hosp_encounter]
insert into [$(TargetSchema)].[src_hosp_encounter]
select *
from [$(TargetSchema)].[sample_src_hosp_encounter]

truncate table [$(TargetSchema)].[src_hosp_procedure]
insert into [$(TargetSchema)].[src_hosp_procedure]
select *
from [$(TargetSchema)].[sample_src_hosp_procedure]

truncate table [$(TargetSchema)].[src_dx_claims]
insert into [$(TargetSchema)].[src_dx_claims]
select *
from [$(TargetSchema)].[sample_src_dx_claims]

truncate table [$(TargetSchema)].[src_REF_patient]
insert into [$(TargetSchema)].[src_REF_patient]
select *
from  [$(TargetSchema)].[sample_src_REF_patient]

truncate table [$(TargetSchema)].[src_REF_Provider]
insert into [$(TargetSchema)].[src_REF_Provider]
select *
from  [$(TargetSchema)].[sample_src_REF_Provider]

truncate table [$(TargetSchema)].[src_REF_Product]
insert into [$(TargetSchema)].[src_REF_Product]
select *
from  [$(TargetSchema)].[sample_src_REF_Product]


truncate table [$(TargetSchema)].[src_ptnt_eligibility]
Select distinct convert(varchar(6),Service_date,112)Month_ID,Patient_ID,'Y' as RX 
into #tmpRX
from [$(TargetSchema)].Src_RX_Claims 

Select distinct convert(varchar(6),Service_date,112)Month_ID,Patient_ID, 'Y' as DX 
into #tmpDX
from [$(TargetSchema)].Src_DX_Claims 

insert into [$(TargetSchema)].[src_ptnt_eligibility] (Month_ID,Patient_ID)
Select distinct Month_ID,Patient_ID From #tmpRX
union 
Select distinct Month_ID,Patient_ID From #tmpDX -- 

Update [$(TargetSchema)].src_ptnt_eligibility Set [RX_ACTIVITY]='N',[DX_ACTIVITY]='N',[HOSP_ACTIVITY]='N'

Update [$(TargetSchema)].src_ptnt_eligibility
Set [$(TargetSchema)].src_ptnt_eligibility.[RX_ACTIVITY]='Y'
From  #tmpRX B
Where [$(TargetSchema)].src_ptnt_eligibility.Month_ID=B.Month_ID
And [$(TargetSchema)].src_ptnt_eligibility.Patient_ID=B.Patient_ID -- 331278


Update [$(TargetSchema)].src_ptnt_eligibility
Set [$(TargetSchema)].src_ptnt_eligibility.[DX_ACTIVITY]='Y'
From  #tmpDX B
Where [$(TargetSchema)].src_ptnt_eligibility.Month_ID=B.Month_ID
And [$(TargetSchema)].src_ptnt_eligibility.Patient_ID=B.Patient_ID -- 279088


Truncate Table [$(TargetSchema)].std_REF_time_dimension
Select * into #tmpstd_REF_time_dimension
From 
(
Select distinct Year(service_date)Year,datepart(mm,service_date) month,'Q'+convert(varchar,datepart(qq,service_date)) quarter,convert(varchar(6),Service_date,112) YYYYMM
From [$(TargetSchema)].Src_RX_Claims
union
Select distinct Year(service_date)Year,datepart(mm,service_date) month,'Q'+convert(varchar,datepart(qq,service_date)) quarter,convert(varchar(6),Service_date,112) YYYYMM
From [$(TargetSchema)].Src_DX_Claims
)A

Insert into [$(TargetSchema)].std_REF_time_dimension
Select dense_rank() over(order by Year,quarter),Row_number() over(order by Year,month),* from #tmpstd_REF_time_dimension --order by Year,quarter,month