--:setvar SCHEMA SHA_Pipeline_All
SET NOCOUNT ON
SELECT 
	   CAST(ISNULL([Variable],'') AS varchar(40)) AS [Variable]
      ,CAST(ISNULL(Replace([Label],',',';'),'') AS VARCHAR(60)) AS [Label]
      ,CASE WHEN (([% of PatientCount] IS NULL) or ([% of PatientCount]=0))
	   THEN ISNULL(CAST([Value] AS varchar(10)),'')
	   ELSE
	   concat(ISNULL(CAST([Value] AS varchar(10)),''),' (',round(ISNULL(CAST([% of PatientCount] AS varchar(10)),''),1),')')
	   END  AS [Statistic (n%)]
	   FROM [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant] WITH(NOLOCK)
  ORDER BY ID, Value DESC, [Label] asc