---:SetVar TargetSchema Boston_UCB_Test
---:SetVar SrcSchema Boston_UCB_SRC

SET NOCOUNT ON
---patients with 2.5 year evaluation period
select 
replace(replace(patient_id, char(10), ''), char(13), '') AS patient_id,
replace(replace(Index_Date, char(10), ''), char(13), '') AS Index_Date,
replace(replace(Age, char(10), ''), char(13), '') AS Age,
replace(replace(Gender, char(10), ''), char(13), '') AS Gender,
replace(replace(CCI_Score, char(10), ''), char(13), '') AS CCI_Score,
replace(replace(Outcome_variable, char(10), ''), char(13), '') AS Outcome_variable,
replace(replace([Model Label], char(10), ''), char(13), '') AS [Model Label],
replace(replace([Class probabilities], char(10), ''), char(13), '') AS [Class probabilities],
replace(replace([DRERiskStratification], char(10), ''), char(13), '') AS [DRERiskStratification]
from [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] 