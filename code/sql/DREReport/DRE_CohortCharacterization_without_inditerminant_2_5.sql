-- - Author: Priti Priya
-- - OutPut Table	: [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
-- - Change History:	Change Date				Change Description
-- -					26-MAR-2019			Written code  to create cohort characterization report for DRE
-- -                    23-Apr-2019			Updated code to make report in new format 
-- -					06-Jun-2019			Rectify the Insurance type logic (taking counts of payer type for only AED at index)
-- -                    27-Jul-2019         AND DB.[Service_date] >PC.[Valid_Index]---condition updated as per shweta's suggestion 
-- -					22-Aug-2019         Modified condition [Service_date] > [Valid_Index] for RX and PX post index --condition updated as per shweta's suggestion 
-- ---------------------------------------------------------------------- 
--:SetVar TargetSchema SHA2_SYNOMA_ID_INTEGRATION
--:SetVar SrcSchema SHA2_Synoma_ID_SRC
SET NOCOUNT ON
SELECT CURRENT_TIMESTAMP AS START_TIMESTAMP

DECLARE @TotalPatientCount INT;
DECLARE @PatientCount INT;
DECLARE @PercentagePatient DECIMAL(5, 1);
DECLARE @PatientPercentage DECIMAL(5, 1); 
DECLARE @MAXID INT
DECLARE @PatientAgeMedian FLOAT;
DECLARE @PatientAgeMean FLOAT;
DECLARE @PatientAgeStdDev FLOAT;
DECLARE @PreIndexMIN INT;
DECLARE @PreIndexMAX INT;
DECLARE @PreIndexMEAN DECIMAL (5,1);
DECLARE @PreIndexMEDIAN DECIMAL (5,1);
DECLARE @PreIndexSD DECIMAL(5,1);
DECLARE @PostIndexMIN INT;
DECLARE @PostIndexMAX INT;
DECLARE @PostIndexMEAN DECIMAL (5,1);
DECLARE @PostIndexMEDIAN DECIMAL (5,1);
DECLARE @PostIndexSD DECIMAL(5,1);

IF OBJECT_ID('[$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
CREATE TABLE [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
(
 [Id] DECIMAL(5,1)
,[Variable] VARCHAR (300)
,[Label] VARCHAR (300)
,[Value] VARCHAR (30) 
,[% of PatientCount] float
)

IF OBJECT_ID('tempdb..#distribution_Patient') IS NOT NULL
	DROP TABLE #distribution_Patient

SELECT A.patient_id
	,a.age
	,a.gender
INTO #distribution_Patient
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A WITH (NOLOCK)
INNER JOIN [$(TargetSchema)].[app_dre_Cohort] B WITH (NOLOCK) ON A.patient_id = B.patient_id
	AND A.valid_index = B.Valid_index and outcome_variable is not null and b.valid_index is not null
inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on A.patient_id = d.patient_id

SET @TotalPatientCount = (
		SELECT count(DISTINCT patient_id) AS TotalPatientCount
		FROM #distribution_Patient
		)

IF OBJECT_ID('tempdb..#PatientAgeRow') IS NOT NULL
	DROP TABLE #PatientAgeRow

SELECT Row_Number() OVER (
		ORDER BY age
		) AS ID
	,*
INTO #PatientAgeRow
FROM #distribution_Patient

SELECT @MAXID = MAX(ID)
FROM #PatientAgeRow

IF (@MAXID % 2 = 0)
BEGIN
	SELECT @PatientAgeMedian = round(AVG(CAST(age AS FLOAT)),1)
	FROM #PatientAgeRow
	WHERE ID IN (
			(@MAXID / 2)
			,(@MAXID / 2) + 1
			)
END
ELSE
BEGIN
	SELECT @PatientAgeMedian = round(AVG(CAST(age AS FLOAT)),1)
	FROM #PatientAgeRow
	WHERE ID IN ((@MAXID + 1) / 2)
END

SET @PatientAgeMean = (
		SELECT round ((Sum(Age) / @TotalPatientCount),1)
		FROM #distribution_Patient
		)

SET @PatientAgeStdDev = (
		SELECT round(STDEV(Age),1)
		FROM #distribution_Patient
		)
INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 1.0 AS [ID],'Total Patients in DRE Cohort' AS [Variable], NULL AS [Label], @TotalPatientCount as [Value], 100.00 AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 1.1 AS [ID],'"Age, continuous"' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 1.2 AS [ID],NULL AS [Variable], 'Mean' AS [Label], Convert(DECIMAL(5,1),@PatientAgeMean) as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 1.3 AS [ID],NULL AS [Variable], 'Median' AS [Label], Convert(DECIMAL(5,1),@PatientAgeMedian) AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 1.4 AS [ID],NULL AS [Variable], 'SD' AS [Label], Convert(DECIMAL(5,1),@PatientAgeStdDev) AS [Value], NULL AS  [% of PatientCount]

--- AGE Quartile Start-- 


IF OBJECT_ID('tempdb..#Age') IS NOT NULL
DROP TABLE #Age

SELECT ROW_NUMBER()OVER (ORDER BY age) AS ID,* INTO #Age FROM #distribution_Patient

BEGIN 

DECLARE @MAXID_2 INT 
DECLARE @Lower_2_Min INT
DECLARE @Lower_2_Max INT
DECLARE @Upper_2_Min INT
DECLARE @Upper_2_Max INT
DECLARE @MAXID_2_Lower_Odd INT
DECLARE @Median_2_Lower_Odd DECIMAL(5,2)
DECLARE @MAXID_2_Upper_Odd INT
DECLARE @Median_2_Upper_Odd DECIMAL(5,2)
DECLARE @MAXID_2_Lower_Even INT
DECLARE @Median_2_Lower_Even DECIMAL(5,2)
DECLARE @MAXID_2_Upper_Even INT
DECLARE @Median_2_Upper_Even DECIMAL(5,2)
DECLARE @Median_2_All DECIMAL(5,2)


select @MAXID_2 = MAX(ID) from #Age
PRINT @MAXID_2
PRINT @MAXID_2%2



IF @MAXID_2%2 =0
		BEGIN
			   SET @Lower_2_Min = 1
			   SET @Lower_2_Max = @MAXID_2/2
			   SET @Upper_2_Min = (@MAXID_2/2)+1
			   SET @Upper_2_Max = @MAXID_2

			     SELECT @Median_2_All = AVG(CAST([age] AS FLOAT)) FROM #Age WHERE ID IN ((@MAXID_2/2),(@MAXID_2/2)+1)
				
				IF OBJECT_ID('tempdb..#LowerHalf_even') IS NOT NULL
				DROP TABLE #LowerHalf_even
			    SELECT  Row_NUmber()over (order by [age]) as ID_Lower,* INTO #LowerHalf_even FROM #Age WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max

				SELECT @MAXID_2_Lower_Even = MAX(ID_Lower) FROM #LowerHalf_even
				IF @MAXID_2_Lower_Even%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Even = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even/2),(@MAXID_2_Lower_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Even = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_even WHERE ID_Lower IN ((@MAXID_2_Lower_Even+1)/2)


				IF OBJECT_ID('tempdb..#UpperHalf_even') IS NOT NULL
				DROP TABLE #UpperHalf_even
			    SELECT  Row_NUmber()over (order by [age]) as ID_Upper,* INTO #UpperHalf_even FROM #Age WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Even = MAX(ID_Upper) FROM #UpperHalf_even
				IF @MAXID_2_Upper_Even%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Even = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even/2),(@MAXID_2_Upper_Even/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Even = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_even WHERE ID_Upper IN ((@MAXID_2_Upper_Even+1)/2)
				

			SELECT @Median_2_All AS Median_Age, @Median_2_Lower_Even AS Median_Q1 ,@Median_2_Upper_Even AS Median_Q3		   
		END
ELSE 
		BEGIN 
			  SET @Lower_2_Min = 1
			  SET @Lower_2_Max = (@MAXID_2+1)/2
			  SET @Upper_2_Min = (@MAXID_2+1)/2
			  SET @Upper_2_Max = @MAXID_2

			   SELECT @Median_2_All = AVG(CAST([age] AS FLOAT)) FROM #Age WHERE ID IN ((@MAXID_2+1)/2)

			    IF OBJECT_ID('tempdb..#LowerHalf_odd') IS NOT NULL
				DROP TABLE #LowerHalf_odd
			    SELECT  Row_NUmber()over (order by [age]) as ID_Lower,* INTO #LowerHalf_odd FROM #Age WHERE ID BETWEEN @Lower_2_Min AND @Lower_2_Max
				
				SELECT @MAXID_2_Lower_Odd = MAX(ID_Lower) FROM #LowerHalf_odd
				IF @MAXID_2_Lower_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Lower_Odd = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd/2),(@MAXID_2_Lower_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Lower_Odd = AVG(CAST([age] AS FLOAT)) FROM #LowerHalf_odd WHERE ID_Lower IN ((@MAXID_2_Lower_Odd+1)/2)
				

			    IF OBJECT_ID('tempdb..#UpperHalf_odd') IS NOT NULL
				DROP TABLE #UpperHalf_odd
			    SELECT  Row_NUmber()over (order by [age]) as ID_Upper,* INTO #UpperHalf_odd FROM #Age WHERE ID BETWEEN @Upper_2_Min AND @Upper_2_Max

				SELECT @MAXID_2_Upper_Odd = MAX(ID_Upper) FROM #UpperHalf_odd
				IF @MAXID_2_Upper_Odd%2 =0
						BEGIN
							   SELECT @Median_2_Upper_Odd = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd/2),(@MAXID_2_Upper_Odd/2)+1)
						END
				ELSE 
					   SELECT @Median_2_Upper_Odd = AVG(CAST([age] AS FLOAT)) FROM #UpperHalf_odd WHERE ID_Upper IN ((@MAXID_2_Upper_Odd+1)/2)
				
				
			SELECT @Median_2_All AS Median,@Median_2_Lower_Odd AS Median_Q1,@Median_2_Upper_Odd AS Median_Q3
		END
		
END


INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 1.5 AS [ID],NULL AS [Variable], 'Q1' AS [Label], CASE WHEN @MAXID_2%2=0 then Convert(DECIMAL(5,1),@Median_2_Lower_Even) ELSE Convert(DECIMAL(5,1),@Median_2_Lower_Odd) END  AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 1.6 AS [ID],NULL AS [Variable], 'Q3' AS [Label], CASE WHEN @MAXID_2%2=0 then Convert(DECIMAL(5,1),@Median_2_Upper_Even) ELSE Convert(DECIMAL(5,1),@Median_2_Upper_Odd) END AS [Value], NULL AS  [% of PatientCount]

--- AGE Quartile End

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 2.0 AS [ID],'Age category' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 
	CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN 2.1
		WHEN age BETWEEN 35
				AND 49
			THEN 2.2
		WHEN age BETWEEN 50
				AND 64
			THEN 2.3
		WHEN age >= 65
			THEN 2.4
		END AS [ID]
	   ,NULL AS [Variable]
	   ,CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN '18 - 34'
		WHEN age BETWEEN 35
				AND 49
			THEN '35 - 49'
		WHEN age BETWEEN 50
				AND 64
			THEN '50 - 64'
		WHEN age >= 65
			THEN '>=65'
		END AS [Label]
	,COUNT(DISTINCT Patient_id) as [PatientCount] 
	,CAST(round(((COUNT(DISTINCT Patient_id) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
FROM #distribution_Patient
GROUP BY 	CASE 
		WHEN age BETWEEN 18
				AND 34
			THEN 2.1
		WHEN age BETWEEN 35
				AND 49
			THEN 2.2
		WHEN age BETWEEN 50
				AND 64
			THEN 2.3
		WHEN age >= 65
			THEN 2.4
		END 
	   ,CASE
		WHEN age BETWEEN 18
				AND 34
			THEN '18 - 34'
		WHEN age BETWEEN 35
				AND 49
			THEN '35 - 49'
		WHEN age BETWEEN 50
				AND 64
			THEN '50 - 64'
		WHEN age >= 65
			THEN '>=65'
		END 

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 3.0 AS [ID],'Gender' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 
		CASE 
		WHEN Gender='F'
		THEN 3.1
		WHEN Gender='M'
		THEN 3.2
		ELSE
		3.3
		END  AS [ID]
	   ,NULL AS [Variable]
	   ,CASE 
		WHEN Gender='F'
		THEN 'Female'
		WHEN Gender='M'
		THEN 'Male'
		ELSE
		'Unknown'
		END  AS [Label]
	   ,Count(DISTINCT patient_id)
	  ,CAST(round(((COUNT(DISTINCT Patient_id) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
FROM #distribution_Patient P
GROUP BY Gender

/* AED Treatment */
INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 4.0 AS [ID],'AED Treatment' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 4.1 AS [ID] 
	,NULL AS [Variable]
	,ad.AED AS [Label]
	,COUNT(DISTINCT ad.Patient_id) as [Value] 
	,CAST(round(((COUNT(DISTINCT ad.Patient_id) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_dre_Cohort] AD WITH(NOLOCK) inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on AD.patient_id = d.patient_id
WHERE [Valid_Index] IS NOT NULL and ad.outcome_variable is not null
AND AED_DAYS>=30
GROUP BY AED


/* Insurance Type */
INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 5.0 AS [ID],'Insurance' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

/* Get patient and thier valid index for DRE from latest cohort*/
IF OBJECT_ID('tempdb..#Cohort') IS NOT NULL
	DROP TABLE #Cohort

SELECT a.Patient_ID
	,a.[Valid_Index]
	,a.AED
INTO [#Cohort]
FROM [$(TargetSchema)].[app_dre_Cohort] A WITH (NOLOCK) inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on A.patient_id = d.patient_id
WHERE [Valid_Index] IS NOT NULL and a.outcome_variable is not null

DECLARE @TotalPatientInCohort INT;
SELECT  @TotalPatientInCohort = COUNT(DISTINCT Patient_ID) FROM  [#Cohort]

/* Get all the data for patients available in DRE Cohort from RX Table where service_date = index_date*/
IF OBJECT_ID('tempdb..#DREDxProc') IS NOT NULL
	DROP TABLE #DREDxProc

SELECT distinct  A.[Patient_ID] AS [CohortPatientID],
	   A.Valid_Index,
	   B.Service_Date,
	   B.Patient_ID,
	   B.[Payer_Type]
INTO #DREDxProc
FROM [#Cohort] A
JOIN [$(SrcSchema)].[src_rx_claims] B WITH(NOLOCK)
ON B.[Patient_ID] = A.[Patient_ID]
AND B.[Service_Date] = A.[Valid_Index]
AND B.Days_supply > 0
JOIN [$(SrcSchema)].[src_REF_product]  C WITH(NOLOCK)
ON B.NDC = C.NDC
JOIN [$(TargetSchema)].[std_REF_AEDName] D  WITH(NOLOCK)
ON C.generic_name = D.generic_name
AND A.aed = D.aed
	

/* Per insurance type count of patients */
IF OBJECT_ID('tempdb..#InsurnaceType') IS NOT NULL
DROP TABLE #InsurnaceType

SELECT 
	CASE 
	WHEN b.[Payer_Type] = 'V'
		THEN 'Commercial'
	WHEN b.[Payer_Type] = 'O'
		THEN 'Cash'
	WHEN b.[Payer_Type] = 'R'  or b.[Payer_Type] = 'G'
		THEN 'Medicare'
	WHEN b.[Payer_Type] = 'M' 
		THEN 'Medicaid'
	ELSE
		'Others'
	END AS [Payer_Type]
	,COUNT (DISTINCT [CohortPatientID]) AS [PatientCount]
INTO #InsurnaceType
FROM #DREDxProc a inner join [$(TargetSchema)].[Payer_Identification] b on a.patient_id=b.patient_id 
GROUP BY b.[Payer_Type]

/*
SELECT 
	CASE 
	WHEN [Payer_Type] = 'COMMERCIAL'
		THEN 'COMMERCIAL'
	WHEN [Payer_Type] = 'CASH'
		THEN 'CASH'
	WHEN [Payer_Type] = 'MEDICARE'
		THEN 'MEDICARE'
	WHEN [Payer_Type] = 'MEDICAID'
		THEN 'MEDICAID'
	ELSE
		'Others'
	END AS [Payer_Type]
	,COUNT (DISTINCT [CohortPatientID]) AS [PatientCount]
INTO #InsurnaceType
FROM #DREDxProc
GROUP BY [Payer_Type]
*/
/* Per insurance type count of patients */

IF OBJECT_ID('tempdb..#InsurnaceTypeDistribution') IS NOT NULL
	DROP TABLE #InsurnaceTypeDistribution

Select [Payer_Type],
Sum([PatientCount]) AS [PatientCount]
INTO #InsurnaceTypeDistribution
FROM #InsurnaceType
GROUP BY [Payer_Type]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 
		CASE WHEN [Payer_Type] = 'COMMERCIAL'
		THEN '5.1'
		WHEN [Payer_Type] = 'CASH'
		THEN '5.4'
		WHEN [Payer_Type] = 'MEDICARE'
		THEN '5.3'
		WHEN [Payer_Type] = 'MEDICAID'
		THEN '5.2'
		ELSE
		'5.5'
		END  AS [ID]
	   ,NULL AS [Variable]
	   ,[Payer_Type]  AS [Label]
	   ,[PatientCount] AS [Value]
	  ,cast(round(((PatientCount * 100.0) / @TotalPatientInCohort), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
FROM #InsurnaceTypeDistribution P

/* Prevelance of outcome variable*/
INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 6.0 AS [ID],'Distribution of outcome variables' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 6.1 AS [ID],'DRE solution' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
Select 
CASE WHEN a.[Outcome_Variable] = 'DRE'
THEN 6.2 
ELSE
	6.3
END AS [ID]
,NULL AS [Variable]
--,[Outcome_Variable] AS [Label]
,ISNULL(cast(a.[Outcome_Variable] as varchar(30)), 'INDETERMINATE') AS [Label]
,COUNT(DISTINCT a.Patient_id) as [Value] 
,CAST(round(((COUNT(DISTINCT a.Patient_id) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_dre_cohort] A WITH(NOLOCK) inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on A.patient_id = d.patient_id
WHERE [Valid_Index] IS NOT NULL  and a.outcome_variable is not null
GROUP BY a.[Outcome_Variable]

/* AED Generation */
INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 7.0 AS [ID],'AED Generation' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5] (
	[Id]
	,[Variable]
	,[Label]
	,[Value]
	,[% of PatientCount]
	)
SELECT 
	CASE WHEN AED.[AED_Generation]='First'
		THEN 7.1 
	WHEN AED.[AED_Generation]='Second'
		THEN 7.2 
	WHEN AED.[AED_Generation]='Third'
		THEN 7.3 
	 END AS [ID]
	,NULL AS [Variable]
	,CASE WHEN AED.[AED_Generation]='First'
		THEN 'First'
	WHEN AED.[AED_Generation]='Second'
		THEN 'Second'
	WHEN AED.[AED_Generation]='Third'
		THEN 'Third'
	 END  AS [Label]
	,COUNT(DISTINCT AD.[patient_id]) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT AD.[patient_id]) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
FROM [$(TargetSchema)].[app_dre_Cohort] AD WITH (NOLOCK) inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on AD.patient_id = d.patient_id
INNER JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] AED WITH (NOLOCK)
	ON AD.[AED] = AED.[AED]
WHERE [Valid_Index] IS NOT NULL and ad.outcome_variable is not null
GROUP BY AED.[AED_Generation]

/* CCI Score */

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 8.0 AS [ID],'Charlson Comorbidity Index (CCI)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

IF OBJECT_ID('tempdb..#PatientCCIScore') IS NOT NULL
	DROP TABLE #PatientCCIScore

SELECT CI.CCI_Score AS CCIScore
,COUNT(DISTINCT CI.Patient_ID) AS [PatientCount]
INTO #PatientCCIScore
FROM [$(TargetSchema)].[app_dre_features_IndexDate_CCI_ECI] CI WITH (NOLOCK)
INNER JOIN [$(TargetSchema)].[app_dre_Cohort] AD WITH (NOLOCK)
	ON AD.[Patient_ID] = CI.[Patient_ID] inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on AD.patient_id = d.patient_id
WHERE AD.[Valid_Index] IS NOT NULL and ad.outcome_variable is not null
GROUP BY CI.CCI_Score

UPDATE #PatientCCIScore
SET CCIScore = 5
WHERE CCIScore >= 5

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5] (
	[Id]
	,[Variable]
	,[Label]
	,[Value]
	,[% of PatientCount]
	)
SELECT 	CASE 
		WHEN CCIScore = 0
			THEN 8.1
		WHEN CCIScore = 1
			THEN 8.2
		WHEN CCIScore = 2
			THEN 8.3
		WHEN CCIScore = 3
			THEN 8.4
		WHEN CCIScore = 4
			THEN 8.5
		WHEN CCIScore = 5
			THEN 8.6
		END AS [ID]
	,NULL AS [Variable]
	,CASE 
		WHEN CCIScore = 0
			THEN '0'
		WHEN CCIScore = 1
			THEN '1'
		WHEN CCIScore = 2
			THEN '2'
		WHEN CCIScore = 3
			THEN '3'
		WHEN CCIScore = 4
			THEN '4'
		WHEN CCIScore = 5
			THEN '>=5'
		END AS [Charlson Comorbidity Index (CCI)]
	,SUM([PatientCount]) AS [PatientCount]
	,CAST(ROUND(((SUM([PatientCount]) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [PatientPercentage]
FROM #PatientCCIScore
GROUP BY CCIScore

/* Comorbidity Report */

IF OBJECT_ID('tempdb..#PatientCohortCom') IS NOT NULL
	DROP TABLE #PatientCohortCom

SELECT Patient_ID
	,Valid_Index
	,Comorbidity
	,ComorbidityValue
INTO #PatientCohortCom
FROM (
	SELECT CI.[patient_id]
		,CI.Valid_Index
		,[Cerebrovascular_Disease]
		,[Chronic_Pulmonary_Disease]
		,[Connective_Tissue_Disease]
		,[Diabetes_Without_Organ_Damage]
		,[Mild_Liver_Disease]
		,[Myocardial_Infarction]
		,[Peptic_Ulcer_Disease]
		,[Congestive_Heart_Failure]
		,[Dementia]
		,[Peripheral_Vascular_Disease]
		,[Diabetes_With_Organ_Damage]
		,[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]
		,[Hemiplegia_or_Paraplegia]
		,[Mod_Severe_Renal_Disease]
		,[Mod_Severe_liver_Disease]
		,[AIDS_HIV]
		,[Metastatic_Solid_Tumor]
		,[Cardiac_Arrhythmias]
		,[Hypertension]
		,[Pulmonary_Circulation_Disorders]
		,[Aspiration_Pneumonia]
		,[Solid_Tumor_Without_Metastases]
		,[Anoxic_Brain_Injury]
		,[Brain_Tumor]
		,[Psychoses]
		,[Depression]
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_CCI_ECI] CI
	INNER JOIN [$(TargetSchema)].[app_dre_Cohort] AD WITH (NOLOCK)
		ON AD.[Patient_ID] = CI.[Patient_ID] inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on AD.patient_id = d.patient_id
	WHERE ad.[Valid_Index] IS NOT NULL and ad.outcome_variable is not null
	) p
UNPIVOT(ComorbidityValue FOR Comorbidity IN (
			 [Cerebrovascular_Disease]
			,[Chronic_Pulmonary_Disease]
			,[Connective_Tissue_Disease]
			,[Diabetes_Without_Organ_Damage]
			,[Mild_Liver_Disease]
			,[Myocardial_Infarction]
			,[Peptic_Ulcer_Disease]
			,[Congestive_Heart_Failure]
			,[Dementia]
			,[Peripheral_Vascular_Disease]
			,[Diabetes_With_Organ_Damage]
			,[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]
			,[Hemiplegia_or_Paraplegia]
			,[Mod_Severe_Renal_Disease]
			,[Mod_Severe_liver_Disease]
			,[AIDS_HIV]
			,[Metastatic_Solid_Tumor]
			,[Cardiac_Arrhythmias]
			,[Hypertension]
			,[Pulmonary_Circulation_Disorders]
			,[Aspiration_Pneumonia]
			,[Solid_Tumor_Without_Metastases]
			,[Anoxic_Brain_Injury]
			,[Brain_Tumor]
			,[Psychoses]
			,[Depression]
			)) AS unpvt;

IF OBJECT_ID('tempdb..#PatientCohortFinal') IS NOT NULL
	DROP TABLE #PatientCohortFinal

SELECT *
INTO #PatientCohortFinal
FROM #PatientCohortCom
WHERE Comorbidity IN (
		'Hypertension'
		,'Cerebrovascular_Disease'
		,'Cardiac_Arrhythmias'
		,'Diabetes_Without_Organ_Damage'
		,'Chronic_Pulmonary_Disease'
		,'Peripheral_Vascular_Disease'
		,'Congestive_Heart_Failure'
		,'Mod_Severe_Renal_Disease'
		,'Hemiplegia_or_Paraplegia'
		,'Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin'
		,'Solid_Tumor_Without_Metastases'
		,'Diabetes_With_Organ_Damage'
		,'Dementia'
		,'Myocardial_Infarction'
		,'Mild_Liver_Disease'
		,'Pulmonary_Circulation_Disorders'
		,'Aspiration_Pneumonia'
		,'Brain_Tumor'
		,'Connective_Tissue_Disease'
		,'Peptic_Ulcer_Disease'
		,'Psychoses'
		,'Depression'
		)

IF OBJECT_ID('tempdb..#PatientComorbidityDistribution') IS NOT NULL
	DROP TABLE #PatientComorbidityDistribution

SELECT 9.1 AS [ID]
	,NULL AS [Variable]
	,Comorbidity AS [Label]
	,Count(DISTINCT Patient_ID) AS [PatientCount]
	,CAST(ROUND(((COUNT(DISTINCT [Patient_id]) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
INTO #PatientComorbidityDistribution
FROM #PatientCohortFinal
WHERE ComorbidityValue = 1
GROUP BY Comorbidity

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 9.0 AS [ID],'Top Comorbidities*' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT   *
FROM #PatientComorbidityDistribution
Order by [PatientCount] DESC,[Label] ASC

/* Comedication Report*/

IF OBJECT_ID('tempdb..#PatientUSCCohort') IS NOT NULL
DROP TABLE #PatientUSCCohort

SELECT 10.1 as [ID], 'USC Group' AS [Variable],C.USC_Name AS [Label]
,COUNT(DISTINCT A.Patient_Id) as [PatientCount]
,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
INTO #PatientUSCCohort
FROM [$(TargetSchema)].[app_dre_Cohort] A WITH (NOLOCK) inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on A.patient_id = d.patient_id
LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
ON A.patient_id=B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
AND B.Days_Supply > 0
LEFT JOIN  [$(SrcSchema)].[src_ref_Product](NOLOCK) C
ON B.NDC = C.NDC
WHERE A.[Valid_Index] IS NOT NULL and a.outcome_variable is not null
GROUP BY C.USC_Name

IF OBJECT_ID('tempdb..#PatientComedicationCohortWithoutAED') IS NOT NULL
DROP TABLE #PatientComedicationCohortWithoutAED

SELECT 10.1 as [ID], NULL AS [Variable],C.USC_Name AS [Label]
,COUNT(DISTINCT A.Patient_Id) as [PatientCount]
,CAST(ROUND(((COUNT(DISTINCT A.[Patient_id]) * 100.0) / @TotalPatientCount), 1) AS DECIMAL(5, 1)) AS [% of PatientCount]
INTO #PatientComedicationCohortWithoutAED
FROM [$(TargetSchema)].[app_dre_Cohort] A WITH (NOLOCK) inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] d on A.patient_id = d.patient_id
LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
ON A.patient_id=B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
AND B.Days_Supply > 0
LEFT JOIN  [$(SrcSchema)].[src_ref_Product](NOLOCK) C
ON B.NDC = C.NDC
WHERE A.[Valid_Index] IS NOT NULL and a.outcome_variable is not null
    AND C.[GENERIC_NAME] NOT IN
	(
'BRIVARACETAM'
,'CARBAMAZEPINE'
,'CARBAMAZEPINE (ANTIPSYCHOTIC)'
,'CLOBAZAM'
,'DIVALPROEX SODIUM'
,'ESLICARBAZEPINE ACETATE'
,'ETHOSUXIMIDE'
,'ETHOSUXIMIDE (BULK)'
,'ETHOTOIN'
,'EZOGABINE'
,'FELBAMATE'
,'FOSPHENYTOIN SODIUM'
,'LACOSAMIDE'
,'LAMOTRIGINE'
,'LAMOTRIGINE (BULK)'
,'LEVETIRACETAM'
,'LEVETIRACETAM (BULK)'
,'LEVETIRACETAM IN NACL (ISO-OS)'
,'LEVETIRACETAM IN SODIUM CHLORIDE'
,'OXCARBAZEPINE'
,'PERAMPANEL'
,'PHENOBARBITAL'
,'PHENOBARBITAL SODIUM'
,'PHENTERMINE/TOPIRAMATE'
,'PHENYTOIN'
,'PHENYTOIN (BULK)'
,'PHENYTOIN SODIUM'
,'PHENYTOIN SODIUM EXTENDED'
,'PHENYTOIN SODIUM PROMPT'
,'PREGABALIN'
,'PRIMIDONE'
,'PRIMIDONE (BULK)'
,'RETIGABINE'
,'RUFINAMIDE'
,'TOPIRAMATE'
,'TOPIRAMATE (BULK)'
,'VALPROATE SODIUM'
,'VALPROATE SODIUM (BULK)'
,'VALPROIC ACID'
,'VALPROIC ACID (AS SODIUM SALT)'
,'VALPROIC ACID (BULK)'
,'VIGABATRIN'
,'ZONISAMIDE'
,'ZONISAMIDE (BULK)'
)
GROUP BY C.USC_Name

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
SELECT 10.0 AS [ID],'Top 10 Medication' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]

INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5] 
SELECT TOP 20 *
FROM #PatientComedicationCohortWithoutAED
Order by PatientCount DESC, [Label] ASC

--/* Rx Density Report */
--/* Generate Cohort Table */
--IF OBJECT_ID('tempdb..#PatientCohort') IS NOT NULL
--	DROP TABLE #PatientCohort
--
--SELECT Patient_ID
--	,[Valid_Index]
--INTO [#PatientCohort]
--FROM [$(TargetSchema)].[app_dre_Cohort] WITH (NOLOCK)
--WHERE [Valid_Index] IS NOT NULL and outcome_variable is not null
--
--/* Count of Rx prescription of Patients for Pre-Index */
--IF OBJECT_ID('tempdb..#Rx_Density') IS NOT NULL
--	DROP TABLE #Rx_Density
--
--SELECT PC.Patient_ID
--	,COUNT(DISTINCT NDC) AS [Rx_Num]
--INTO #Rx_Density
--FROM  [#PatientCohort] PC WITH(NOLOCK)
--LEFT JOIN [$(SrcSchema)].[src_rx_claims] SR WITH(NOLOCK)
--	ON SR.[Patient_ID] = PC.[Patient_ID]
--    AND SR.[Service_date] BETWEEN DATEADD(DD, - 365, PC.[Valid_Index])
--		AND DATEADD(DD, - 1, PC.[Valid_Index])
--		AND SR.Days_Supply > 0
--GROUP BY PC.[Patient_ID]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #Rx_Density
--Select 
--'-1' as [Patient_ID]
--,0 as [Rx_Num]
--
--Insert into #Rx_Density
--Select 
--'-1' as [Patient_ID]
--,1 as [Rx_Num]
--
--Insert into #Rx_Density
--Select 
--'-1' as [Patient_ID]
--,6 as [Rx_Num]
--
--Insert into #Rx_Density
--Select 
--'-1' as [Patient_ID]
--,11 as [Rx_Num]
--
--
--/* Count of Rx prescription of Patients for Post-Index */
--IF OBJECT_ID('tempdb..#Rx_DensityPostIndex') IS NOT NULL
--	DROP TABLE #Rx_DensityPostIndex
--
--SELECT PC.Patient_ID
--	,COUNT(DISTINCT NDC) AS [Rx_Num]
--INTO #Rx_DensityPostIndex
--FROM  [#PatientCohort] PC WITH(NOLOCK)
--LEFT JOIN [$(SrcSchema)].[src_rx_claims] SR WITH(NOLOCK)
--	ON SR.[Patient_ID] = PC.[Patient_ID]
--    AND SR.[Service_date] > PC.[Valid_Index]---condition updated as per shweta's suggestion 
--	AND SR.Days_Supply > 0
--GROUP BY PC.[Patient_ID]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #Rx_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,0 as [Rx_Num]
--
--Insert into #Rx_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,1 as [Rx_Num]
--
--Insert into #Rx_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,6 as [Rx_Num]
--
--Insert into #Rx_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,11 as [Rx_Num]
--
--IF OBJECT_ID('tempdb..#DX_Base') IS NOT NULL
--	DROP TABLE #DX_Base
--
--SELECT SD.*
--INTO #DX_Base
--FROM [$(SrcSchema)].[src_dx_claims] SD WITH (NOLOCK)
--WHERE [PROC_CD] IS NOT NULL
-- AND SD.Patient_ID in ( Select Patient_ID from [#PatientCohort])
--
--/* Count of Dx prescription of Patients for Pre-Index */
--IF OBJECT_ID('tempdb..#Dx_Density') IS NOT NULL
--	DROP TABLE #Dx_Density
--
--SELECT PC.Patient_ID
--	,COUNT(DISTINCT DX_Codes) AS [Dx_Num]
--INTO #Dx_Density
--FROM [#PatientCohort] PC
--LEFT JOIN [$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] DB WITH(NOLOCK)
--	ON DB.[Patient_ID] = PC.[Patient_ID]
--    AND DB.[Service_date] BETWEEN DATEADD(DD, - 365, PC.[Valid_Index])
--		AND DATEADD(DD, - 1, PC.[Valid_Index])
--GROUP BY PC.[Patient_ID]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #Dx_Density
--Select 
--'-1' as [Patient_ID]
--,0 as [Dx_Num]
--
--Insert into #Dx_Density
--
--Select 
--'-1' as [Patient_ID]
--,1 as [Dx_Num]
--
--Insert into #Dx_Density
--Select 
--'-1' as [Patient_ID]
--,6 as [Dx_Num]
--
--Insert into #Dx_Density
--Select 
--'-1' as [Patient_ID]
--,11 as [Dx_Num]
--
--
--/* Count of Dx prescription of Patients for Post-Index */
--IF OBJECT_ID('tempdb..#Dx_DensityPostIndex') IS NOT NULL
--	DROP TABLE #Dx_DensityPostIndex
--
--SELECT PC.Patient_ID
--	,COUNT(DISTINCT DX_Codes) AS [Dx_Num]
--INTO #Dx_DensityPostIndex
--FROM [#PatientCohort] PC
--LEFT JOIN [$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] DB WITH(NOLOCK)
--	ON DB.[Patient_ID] = PC.[Patient_ID]
--    AND DB.[Service_date] >PC.[Valid_Index]---condition updated as per shweta's suggestion 
--GROUP BY PC.[Patient_ID]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #Dx_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,0 as [Dx_Num]
--
--Insert into #Dx_DensityPostIndex
--
--Select 
--'-1' as [Patient_ID]
--,1 as [Dx_Num]
--
--Insert into #Dx_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,6 as [Dx_Num]
--
--Insert into #Dx_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,11 as [Dx_Num]
--
--/* Count of Procedure prescription of Patients for Pre-Index */
--IF OBJECT_ID('tempdb..#Px_Density') IS NOT NULL
--	DROP TABLE #Px_Density
--
--SELECT PC.Patient_ID
--	,COUNT(DISTINCT  DB.[PROC_CD]) AS [Proc_Num]
--INTO #Px_Density
--FROM [#PatientCohort] PC
--LEFT JOIN #DX_Base DB
--	ON DB.[Patient_ID] = PC.[Patient_ID]
--    AND  DB.[Service_date] BETWEEN DATEADD(DD, - 365, PC.[Valid_Index])
--		AND DATEADD(DD, - 1, PC.[Valid_Index])
--	AND DB.DIAG1 IS NULL
--	AND DB.[PROC_CD] IS NOT NULL
--GROUP BY PC.[Patient_ID]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #Px_Density
--Select 
--'-1' as [Patient_ID]
--,0 as [Proc_Num]
--
--Insert into #Px_Density
--Select 
--'-1' as [Patient_ID]
--,1 as [Proc_Num]
--
--
--Insert into #Px_Density
--Select 
--'-1' as [Patient_ID]
--,6 as [Proc_Num]
--
--Insert into #Px_Density
--Select 
--'-1' as [Patient_ID]
--,11 as [Proc_Num]
--
--/* Count of Procedure prescription of Patients for Post-Index */
--IF OBJECT_ID('tempdb..#Px_DensityPostIndex') IS NOT NULL
--	DROP TABLE #Px_DensityPostIndex
--
--SELECT PC.Patient_ID
--	,COUNT(DISTINCT  DB.[PROC_CD]) AS [Proc_Num]
--INTO #Px_DensityPostIndex
--FROM [#PatientCohort] PC
--LEFT JOIN #DX_Base DB
--	ON DB.[Patient_ID] = PC.[Patient_ID]
--    AND DB.[Service_date] > PC.[Valid_Index]---condition updated as per shweta's suggestion 
--	AND DB.DIAG1 IS NULL
--	AND DB.[PROC_CD] IS NOT NULL
--GROUP BY PC.[Patient_ID]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #Px_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,0 as [Proc_Num]
--
--Insert into #Px_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,1 as [Proc_Num]
--
--
--Insert into #Px_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,6 as [Proc_Num]
--
--Insert into #Px_DensityPostIndex
--Select 
--'-1' as [Patient_ID]
--,11 as [Proc_Num]
--
--
--SET @TotalPatientCount = (
--		SELECT count(DISTINCT PATIENT_ID) AS TotalPatientCount
--		FROM #PatientCohort
--		)
--
--/* Rx Pre-Index Intermediate table */
--IF OBJECT_ID('tempdb..#Rx_DensityInter') IS NOT NULL
--	DROP TABLE #Rx_DensityInter
--
--SELECT CASE 
--		WHEN Rx_Num = 0
--			THEN 12.1
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN 12.2
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN 12.3
--		WHEN Rx_Num > 10
--			THEN 12.4
--		END AS [ID]
--	,NULL AS [Variable]
--	,CASE 
--		WHEN Rx_Num = 0
--			THEN ' 0 '
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN Rx_Num > 10
--			THEN ' >10 '
--		END AS [Label]
--	,(COUNT(RD.[Patient_ID])-1) AS [PatientCount]
--	,CAST(ROUND((((COUNT(RD.[Patient_id])-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--INTO #Rx_DensityInter
--FROM #Rx_Density RD
--GROUP BY 
--	CASE 
--		WHEN Rx_Num = 0
--			THEN 12.1
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN 12.2
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN 12.3
--		WHEN Rx_Num > 10
--			THEN 12.4
--		END
--		,CASE 
--		WHEN Rx_Num = 0
--			THEN ' 0 '
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN Rx_Num > 10
--			THEN ' >10 '
--		END
--		
--/* Rx Post-Index Intermediate table */
--IF OBJECT_ID('tempdb..#Rx_DensityInterPostIndex') IS NOT NULL
--	DROP TABLE #Rx_DensityInterPostIndex
--
--SELECT CASE 
--		WHEN Rx_Num = 0
--			THEN 15.1
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN 15.2
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN 15.3
--		WHEN Rx_Num > 10
--			THEN 15.4
--		END AS [ID]
--	,NULL AS [Variable]
--	,CASE 
--		WHEN Rx_Num = 0
--			THEN ' 0 '
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN Rx_Num > 10
--			THEN ' >10 '
--		END AS [Label]
--	,(COUNT(RD.[Patient_ID])-1) AS [PatientCount]
--	,CAST(ROUND((((COUNT(RD.[Patient_id])-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--INTO #Rx_DensityInterPostIndex
--FROM #Rx_DensityPostIndex RD
--GROUP BY 
--	CASE 
--		WHEN Rx_Num = 0
--			THEN 15.1
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN 15.2
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN 15.3
--		WHEN Rx_Num > 10
--			THEN 15.4
--		END
--		,CASE 
--		WHEN Rx_Num = 0
--			THEN ' 0 '
--		WHEN Rx_Num BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN Rx_Num BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN Rx_Num > 10
--			THEN ' >10 '
--		END
--		
-- INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 12.0 AS [ID],'Number of Rx records (Pre-Index)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 15.0 AS [ID],'Number of Rx records (Post-Index)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT *
--FROM #Rx_DensityInter RD
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT *
--FROM #Rx_DensityInterPostIndex RD
--
--/* Dx Pre-Index Intermediate table */
--IF OBJECT_ID('tempdb..#Dx_DensityInter') IS NOT NULL
--	DROP TABLE #Dx_DensityInter
--
--SELECT 
--	 CASE 
--		WHEN [Dx_Num] = 0
--			THEN 11.1
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN 11.2
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN 11.3
--		WHEN [Dx_Num] > 10
--			THEN 11.4
--		END AS [ID]
--	,NULL AS [Variable]
--	,CASE 
--		WHEN [Dx_Num] = 0
--			THEN ' 0 '
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Dx_Num] > 10
--			THEN ' >10 '
--		END AS [Label]
--	,(COUNT(DD.Patient_ID)-1) AS [PatientCount]
--	,CAST(ROUND((((COUNT(DD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--INTO #Dx_DensityInter
--FROM #Dx_Density DD
--GROUP BY 
--CASE 
--		WHEN [Dx_Num] = 0
--			THEN 11.1
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN 11.2
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN 11.3
--		WHEN [Dx_Num] > 10
--			THEN 11.4
--		END
--		,CASE 
--		WHEN [Dx_Num] = 0
--			THEN ' 0 '
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Dx_Num] > 10
--			THEN ' >10 '
--		END
--
--/* Dx Post-Index Intermediate table */
--IF OBJECT_ID('tempdb..#Dx_DensityInterPostIndex') IS NOT NULL
--	DROP TABLE #Dx_DensityInterPostIndex
--
--SELECT 
--	 CASE 
--		WHEN [Dx_Num] = 0
--			THEN 14.1
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN 14.2
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN 14.3
--		WHEN [Dx_Num] > 10
--			THEN 14.4
--		END AS [ID]
--	,NULL AS [Variable]
--	,CASE 
--		WHEN [Dx_Num] = 0
--			THEN ' 0 '
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Dx_Num] > 10
--			THEN ' >10 '
--		END AS [Label]
--	,(COUNT(DD.Patient_ID)-1) AS [PatientCount]
--	,CAST(ROUND((((COUNT(DD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--INTO #Dx_DensityInterPostIndex
--FROM #Dx_DensityPostIndex DD
--GROUP BY 
--CASE 
--		WHEN [Dx_Num] = 0
--			THEN 14.1
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN 14.2
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN 14.3
--		WHEN [Dx_Num] > 10
--			THEN 14.4
--		END
--		,CASE 
--		WHEN [Dx_Num] = 0
--			THEN ' 0 '
--		WHEN [Dx_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Dx_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Dx_Num] > 10
--			THEN ' >10 '
--		END
--		
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 11.0 AS [ID],'Number of Dx records (Pre-Index) ' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 14.0 AS [ID],'Number of Dx records (Post-Index) ' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT *
--FROM #Dx_DensityInter PD
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT *
--FROM #Dx_DensityInterPostIndex PD
--
--/* Proc Pre-Index Intermediate table */
--IF OBJECT_ID('tempdb..#Px_DensityInter') IS NOT NULL
--	DROP TABLE #Px_DensityInter
--
--SELECT CASE 
--		WHEN [Proc_Num] = 0
--			THEN 13.1
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN 13.2
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN 13.3
--		WHEN [Proc_Num] > 10
--			THEN 13.4
--		END AS [ID]
--	,NULL AS [Variable]
--	,CASE 
--		WHEN [Proc_Num] = 0
--			THEN ' 0 '
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Proc_Num] > 10
--			THEN ' >10 '
--		END AS [Label]
--	,(COUNT(PD.Patient_ID)-1) AS [PatientCount]
--	,CAST(ROUND((((COUNT(PD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--INTO #Px_DensityInter
--FROM #Px_Density PD
--GROUP BY 
--	CASE 
--		WHEN [Proc_Num] = 0
--			THEN 13.1
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN 13.2
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN 13.3
--		WHEN [Proc_Num] > 10
--			THEN 13.4
--		END
--	,CASE 
--		WHEN [Proc_Num] = 0
--			THEN ' 0 '
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Proc_Num] > 10
--			THEN ' >10 '
--		END
--
--/* Proc Pre-Index Intermediate table */
--IF OBJECT_ID('tempdb..#Px_DensityInterPostIndex') IS NOT NULL
--	DROP TABLE #Px_DensityInterPostIndex
--
--SELECT CASE 
--		WHEN [Proc_Num] = 0
--			THEN 16.1
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN 16.2
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN 16.3
--		WHEN [Proc_Num] > 10
--			THEN 16.4
--		END AS [ID]
--	,NULL AS [Variable]
--	,CASE 
--		WHEN [Proc_Num] = 0
--			THEN ' 0 '
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Proc_Num] > 10
--			THEN ' >10 '
--		END AS [Label]
--	,(COUNT(PD.Patient_ID)-1) AS [PatientCount]
--	,CAST(ROUND((((COUNT(PD.Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--INTO #Px_DensityInterPostIndex
--FROM #Px_DensityPostIndex PD
--GROUP BY 
--	CASE 
--		WHEN [Proc_Num] = 0
--			THEN 16.1
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN 16.2
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN 16.3
--		WHEN [Proc_Num] > 10
--			THEN 16.4
--		END
--	,CASE 
--		WHEN [Proc_Num] = 0
--			THEN ' 0 '
--		WHEN [Proc_Num] BETWEEN 1
--				AND 5
--			THEN ' 1-5 '
--		WHEN [Proc_Num] BETWEEN 6
--				AND 10
--			THEN ' 6-10 '
--		WHEN [Proc_Num] > 10
--			THEN ' >10 '
--		END
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 13.0 AS [ID],'Number of Procedures (Pre-Index) ' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 16.0 AS [ID],'Number of Procedures (Post-Index) ' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT *
--FROM #Px_DensityInter PD
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT *
--FROM #Px_DensityInterPostIndex PD
--/*
--Data availbility report */
--
--IF OBJECT_ID('tempdb..#PreIndexPatient') IS NOT NULL
--	DROP TABLE #PreIndexPatient
--SELECT Patient_ID, MIN(Service_Date) as [Service_Date]
--INTO #PreIndexPatient
--FROM [$(SrcSchema)].[src_rx_claims] WITH(NOLOCK)
--WHERE Days_Supply > 0
--GROUP BY Patient_ID
--UNION ALL
--SELECT Patient_ID, MIN(Service_Date) as [Service_Date]
--FROM [$(SrcSchema)].[src_dx_claims] WITH(NOLOCK)
--GROUP BY Patient_ID
--
--IF OBJECT_ID('tempdb..#PreIndexPatientInter') IS NOT NULL
--	DROP TABLE #PreIndexPatientInter
--SELECT Patient_ID, MIN([Service_Date]) as [Service_Date]
--INTO #PreIndexPatientInter
--FROM #PreIndexPatient
--GROUP BY Patient_ID
--
--IF OBJECT_ID('tempdb..#PostIndexPatient') IS NOT NULL
--	DROP TABLE #PostIndexPatient
--SELECT Patient_ID, MAX([Service_Date]) as [Service_Date]
--INTO #PostIndexPatient
--FROM [$(SrcSchema)].[src_rx_claims]
--WHERE Days_Supply > 0
--GROUP BY Patient_ID
--UNION ALL
--SELECT Patient_ID, MAX([Service_Date]) as [Service_Date]
--FROM [$(SrcSchema)].[src_dx_claims]
--GROUP BY Patient_ID
--
--IF OBJECT_ID('tempdb..#PostIndexPatientInter') IS NOT NULL
--	DROP TABLE #PostIndexPatientInter
--SELECT Patient_ID, MAX([Service_Date]) as [Service_Date]
--INTO #PostIndexPatientInter
--FROM #PostIndexPatient
--GROUP BY Patient_ID
--
--/* Get Month between Index Date and First Date of Rx, Dx */
--IF OBJECT_ID('tempdb..#PreIndexPatientCohort') IS NOT NULL
--	DROP TABLE #PreIndexPatientCohort
--SELECT PC.PATIENT_ID,PC.[VALID_INDEX],Pre.[Service_Date],DATEDIFF(MM, Pre.[Service_Date], PC.[VALID_INDEX]) AS [MOS]
--INTO #PreIndexPatientCohort
--FROM [#PatientCohort] PC WITH(NOLOCK)
--INNER JOIN #PreIndexPatientInter Pre WITH(NOLOCK)
--ON PC.[Patient_ID] = Pre.[Patient_ID]
--
--/* Get Month between Index Date and Last Date of Rx, Dx */
--IF OBJECT_ID('tempdb..#PostIndexPatientCohort') IS NOT NULL
--	DROP TABLE #PostIndexPatientCohort
--SELECT PC.PATIENT_ID,PC.[VALID_INDEX],POS.[Service_Date],DATEDIFF(MM, PC.[VALID_INDEX],POS.[Service_Date]) AS [MOS]
--INTO #PostIndexPatientCohort
--FROM [#PatientCohort] PC WITH(NOLOCK)
--INNER JOIN #PostIndexPatientInter POS WITH(NOLOCK)
--ON PC.[Patient_ID] = POS.[Patient_ID]
--
--SELECT @PreIndexMIN = MIN([MOS]) FROM #PreIndexPatientCohort;
--
--SELECT @PreIndexMAX = MAX([MOS]) FROM #PreIndexPatientCohort;
--
--SELECT @PreIndexSD = STDEV([MOS]) FROM #PreIndexPatientCohort;
--
--SELECT @PostIndexMIN = MIN([MOS]) FROM #PostIndexPatientCohort;
--
--SELECT @PostIndexMAX = MAX([MOS]) FROM #PostIndexPatientCohort;
--
--SELECT @PostIndexSD = STDEV([MOS]) FROM #PostIndexPatientCohort;
--
--SET @TotalPatientCount = (
--		SELECT count(DISTINCT PATIENT_ID) AS TotalPatientCount
--		FROM #PatientCohort
--		)
--
--/* Mean, Median and Standard deviation distribution for pre index observation */
--IF OBJECT_ID('tempdb..#PreIndexPatientStats') IS NOT NULL
--	DROP TABLE #PreIndexPatientStats
--
--SELECT Row_Number() OVER (
--		ORDER BY [MOS]
--		) AS ID
--	,*
--INTO #PreIndexPatientStats
--FROM #PreIndexPatientCohort
--
--SELECT @MAXID = MAX(ID)
--FROM #PreIndexPatientStats
--
--IF (@MAXID % 2 = 0)
--BEGIN
--	SELECT @PreIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
--	FROM #PreIndexPatientStats
--	WHERE ID IN (
--			(@MAXID / 2)
--			,(@MAXID / 2) + 1
--			)
--END
--ELSE
--BEGIN
--	SELECT @PreIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
--	FROM #PreIndexPatientStats
--	WHERE ID IN ((@MAXID + 1) / 2)
--END
--
--SET @PreIndexMEAN = (
--		SELECT Sum([MOS]) / @TotalPatientCount
--		FROM #PreIndexPatientStats
--		)
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 17.0 AS [ID],'Pre-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 17.1 AS [ID],NULL AS [Variable], 'n' AS [Label], @TotalPatientCount as [Value], NULL AS  [% of PatientCount]
--		
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 17.2 AS [ID],NULL AS [Variable], 'Mean' AS [Label], CAST(@PreIndexMEAN AS decimal(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 17.3 AS [ID],NULL AS [Variable], 'Median' AS [Label], CAST(@PreIndexMEDIAN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 17.4 AS [ID],NULL AS [Variable], 'SD' AS [Label], CAST(@PreIndexSD AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 17.5 AS [ID],NULL AS [Variable], 'Min' AS [Label], CAST(@PreIndexMIN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 17.6 AS [ID],NULL AS [Variable], 'Max' AS [Label], CAST(@PreIndexMAX AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 18.0 AS [ID],'Pre-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #PreIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,12 as [MOS]
--
--Insert into #PreIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,16 as [MOS]
--
--Insert into #PreIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,19 as [MOS]
--
--Insert into #PreIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,25 as [MOS]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN 18.1
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN 18.2
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN 18.3
--		WHEN [MOS] > 24 
--			THEN 18.4
--		END AS [ID]
--      ,NULL  AS [Variable]
--	,CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN ' 12 - 15 '
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN ' 16 - 18 '
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN ' 19 - 24 '
--		WHEN [MOS] > 24 
--			THEN ' 24+ '
--		END as [Label]
--	,(COUNT(DISTINCT Patient_id)-1) PatientCount
--	,CAST(ROUND((((COUNT(DISTINCT Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--FROM #PreIndexPatientCohort
--GROUP BY 
--	CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN 18.1
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN 18.2
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN 18.3
--		WHEN [MOS] > 24 
--			THEN 18.4
--		END
--	,CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN ' 12 - 15 '
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN ' 16 - 18 '
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN ' 19 - 24 '
--		WHEN [MOS] > 24 
--			THEN ' 24+ '
--		END
--
--/* Mean, Median and Standard deviation distribution for pre index observation */
--IF OBJECT_ID('tempdb..#PostIndexPatientStats') IS NOT NULL
--	DROP TABLE #PostIndexPatientStats
--
--SELECT Row_Number() OVER (
--		ORDER BY [MOS]
--		) AS ID
--	,*
--INTO #PostIndexPatientStats
--FROM #PostIndexPatientCohort
--
--
--SELECT @MAXID = MAX(ID)
--FROM #PostIndexPatientStats
--
--IF (@MAXID % 2 = 0)
--BEGIN
--	SELECT @PostIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
--	FROM #PostIndexPatientStats
--	WHERE ID IN (
--			(@MAXID / 2)
--			,(@MAXID / 2) + 1
--			)
--END
--ELSE
--BEGIN
--	SELECT @PostIndexMEDIAN = AVG(CAST([MOS] AS FLOAT))
--	FROM #PostIndexPatientStats
--	WHERE ID IN ((@MAXID + 1) / 2)
--END
--
--SET @PostIndexMEAN = (
--		SELECT Sum([MOS]) / @TotalPatientCount
--		FROM #PostIndexPatientStats
--		)
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 19.0 AS [ID],'Post-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 19.1 AS [ID],NULL AS [Variable], 'n' AS [Label], @TotalPatientCount AS [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 19.2 AS [ID],NULL AS [Variable], 'Mean' AS [Label], CAST(@PostIndexMEAN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 19.3 AS [ID],NULL AS [Variable], 'Median' AS [Label], CAST(@PostIndexMEDIAN AS DECIMAL(5,2))as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 19.4 AS [ID],NULL AS [Variable], 'SD' AS [Label], CAST(@PostIndexSD AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 19.5 AS [ID],NULL AS [Variable], 'Min' AS [Label], CAST( @PostIndexMIN AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 19.6 AS [ID],NULL AS [Variable], 'Max' AS [Label], CAST(@PostIndexMAX AS DECIMAL(5,2)) as [Value], NULL AS  [% of PatientCount]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT 20.0 AS [ID],'Post-Index observation period per record (mos)' AS [Variable], NULL AS [Label], NULL AS [Value], NULL AS  [% of PatientCount]
--
--/* Inserting dummy enteries for getting all the group */
--Insert into #PostIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,12 as [MOS]
--
--Insert into #PostIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,16 as [MOS]
--
--Insert into #PostIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,19 as [MOS]
--
--Insert into #PostIndexPatientCohort
--Select 
--'-1' as [Patient_ID]
--,'2008-01-01' AS [Valid_Index]
--,'2008-01-01' AS [Service_Date]
--,25 as [MOS]
--
--INSERT INTO [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5]
--SELECT CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN 20.1
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN 20.2
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN 20.3
--		WHEN [MOS] > 24 
--			THEN 20.4
--		END  AS [ID]
--      ,NULL  AS [Variable]
--	,CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN ' 12 - 15 '
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN ' 16 - 18 '
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN ' 19 - 24 '
--		WHEN [MOS] > 24 
--			THEN ' 24+ '
--		END as [Label]
--	,(COUNT(DISTINCT Patient_id)-1) PatientCount
--	,CAST(ROUND((((COUNT(DISTINCT Patient_id)-1) * 100.0) / @TotalPatientCount), 2) AS DECIMAL(5, 2)) AS [% of PatientCount]
--FROM #PostIndexPatientCohort
--GROUP BY 
--   CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN 20.1
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN 20.2
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN 20.3
--		WHEN [MOS] > 24 
--			THEN 20.4
--		END
--	,CASE 
--		WHEN [MOS] BETWEEN 12
--				AND 15
--			THEN ' 12 - 15 '
--		WHEN [MOS] BETWEEN 16
--				AND 18
--			THEN ' 16 - 18 '
--		WHEN [MOS] BETWEEN 19
--				AND 24
--			THEN ' 19 - 24 '
--		WHEN [MOS] > 24 
--			THEN ' 24+ '
--END
--
--SELECT CURRENT_TIMESTAMP AS END_TIMESTAMP
----************************************************************************************--