SELECT 
[Criteria]
,[Meets criteria Patient count n (%)] 
,[Do not meet criteria Patient count n (%)] 
,[Meets criteria Patient count n (%) with SHA] 
,[Do not meet criteria Patient count n (%) with SHA]
FROM [$(TargetSchema)].[PHCPipelineMetricReport]  WITH(NOLOCK)
Where 1<>1