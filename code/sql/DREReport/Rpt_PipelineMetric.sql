-- ---------------------------------------------------------------------
-- - AUTHOR			: Priti Priya
-- - USED BY		: 
-- - 
-- - PURPOSE		: Pipeline Metrics for DRE and TDS.
-- - NOTE			: ELREFPRED-977
-- - Execution		: 
-- - Date			: 25-March-2019			
-- - Tables Required: [$(SrcSchema)].[src_REF_patient], [$(TargetSchema)].[app_int_Patient_Profile],[$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] ,[$(TargetSchema)].[app_tds_cohort],[$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]
-- - OutPut Table	: #TDS_PHCPipelineMetricReport,#TDS_PHCPipelineMetricReport
-- - Change History:	Change Date		Changed by		Change Description
-- -					28-Mar-2019		SureshG			Modified TDS pipeline : Removed Twelve_Months_Eligible criteria	
-- -					29-Mar-2019		SureshG			Added filter criteria for Monotherapy/Polytherapy records	
-- -					14-Oct-2019		SupriyaS		Updated code for TDS metric to reflect the report format as per reported to PHC			
-- ---------------------------------------------------------------------- 
--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_update_New_17_Oct

SET NOCOUNT ON

SELECT CURRENT_TIMESTAMP AS START_TIMESTAMP

IF OBJECT_ID('[$(TargetSchema)].[PHCPipelineMetricReport]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[PHCPipelineMetricReport]
CREATE TABLE [$(TargetSchema)].[PHCPipelineMetricReport]
(
 [ID] DECIMAL(5,1)
,[Criteria] VARCHAR (300)
,[Meets criteria Patient count n (%)] VARCHAR (300) 
,[Do not meet criteria Patient count n (%)] VARCHAR (300) 
,[Meets criteria Patient count n (%) with SHA] VARCHAR (300) 
,[Do not meet criteria Patient count n (%) with SHA] VARCHAR (300)
)

DECLARE @TotalPatientCount INT;
DECLARE @TotalPatientCountDRECohort INT;
DECLARE @PatientInTDSCohort INT;
DECLARE @TotalPatientCountTDSCohort INT;
DECLARE @PatientInDRECohort INT;
DECLARE @AtLeastAED INT;
DECLARE @SynomaPatient INT; 
DECLARE @IndexDateQualifiedPatientCount INT;
DECLARE @ObservationPeriodPatientCnt INT;
DECLARE @EvalutionPeriodPatientCnt INT;
DECLARE @DREPatientCount INT;
DECLARE @NonDREPatientCount INT;
DECLARE @AgeQualifiedPatientCnt INT;
DECLARE @MeetPatientCount INT;
DECLARE @DoNotMeetPatientCount INT;
DECLARE @MeetPatientPercentage Decimal(5,1);
DECLARE @DoNotMeetPatientPercentage Decimal(5,1);
DECLARE @MeetCriteria Varchar (300) ;
DECLARE @DoNotMeetCriteria Varchar (300) ;
DECLARE @MeetPatientPercentageSHA Decimal(5,1);
DECLARE @DoNotMeetPatientPercentageSHA Decimal(5,1);
DECLARE @MeetCriteriaSHA Varchar (300) ;
DECLARE @DoNotMeetCriteriaSHA Varchar (300) ;
DECLARE @IndexEventCnt INT;
DECLARE @16AEDPatientCnt INT;
DECLARE @FailurePatientCount INT;
DECLARE @SuccessPatientCount INT;
DECLARE @IndeterminatePatientCount INT;
DECLARE @WithoutIndeterminatePatientCnt INT;


/* Total patients(SynomaID) */
select @SynomaPatient = COUNT(Distinct Patient_id) FROM [$(SrcSchema)].[PHPH_SynomaIDs] WITH(NOLOCK)
INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (1.0, 'Total Patients (SynomaID)', @SynomaPatient,'NA','NA','NA')

SELECT @TotalPatientCount = COUNT(Distinct Patient_id) FROM [$(SrcSchema)].[src_REF_patient] WITH(NOLOCK)
SELECT @MeetCriteria = CAST(@TotalPatientCount AS varchar(300))  +' ('+CAST(100.0 AS VARCHAR(300))+'%)';
INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (1.1, 'Match with SHA', @MeetCriteria,'',@MeetCriteria, '')

/* Patient Pool */
INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (2.0,'Patient Pool','','','','')

/* Epilepsy Diagnosis */
SELECT @MeetPatientCount = COUNT(Distinct Patient_id) from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 
SELECT @DoNotMeetPatientCount = @TotalPatientCount -@MeetPatientCount
SELECT @MeetPatientPercentage = (@MeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @MeetCriteria = CAST(@MeetPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (2.1,'Epilepsy Diagnosis',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteria,@DoNotMeetCriteria)

/* Atleast One AED */
SELECT @AtLeastAED = COUNT(Distinct Patient_id) from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] WHERE  AED_Days >30 AND Patient_id IN (SELECT DISTINCT Patient_id from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 )
SELECT @DoNotMeetPatientCount = @MeetPatientCount -@AtLeastAED
SELECT @MeetPatientPercentage = (@AtLeastAED * 100.00)/ @MeetPatientCount
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @MeetPatientCount
SELECT @MeetPatientPercentageSHA = (@AtLeastAED * 100.00)/ @TotalPatientCount
SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @MeetCriteria = CAST(@AtLeastAED AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @MeetCriteriaSHA = CAST(@AtLeastAED AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (2.2,'At Least One AED(With > 30 days supply)',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

/* Match with SynomaID */
IF OBJECT_ID('Tempdb..#src_Ref_Patient_SynomaID','U') IS NOT NULL
DROP TABLE #src_Ref_Patient_SynomaID

Select distinct Patient_ID INTO #src_Ref_Patient_SynomaID  from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] WHERE  AED_Days >30 AND Patient_id IN (SELECT DISTINCT Patient_id from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 )
SELECT @SynomaPatient = COUNT(Distinct Patient_id) from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] WHERE  AED_Days >30 AND Patient_id IN (SELECT DISTINCT Patient_id from [$(TargetSchema)].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 ) and Patient_ID in ( Select distinct Patient_ID from #src_Ref_Patient_SynomaID with(nolock))
SELECT @DoNotMeetPatientCount = @AtLeastAED -@SynomaPatient
SELECT @MeetPatientPercentage = (@SynomaPatient * 100.00)/ @AtLeastAED
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @AtLeastAED
SELECT @MeetPatientPercentageSHA = (@SynomaPatient * 100.00)/ @TotalPatientCount
SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @MeetCriteria = CAST(@SynomaPatient AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @MeetCriteriaSHA = CAST(@SynomaPatient AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (2.3,'Match With SynomaID',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

/* DRE Cohort Inclusions */
INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.0,'DRE Cohort Inclusions','','','','')

/* Index Date Qualification */
SELECT @IndexDateQualifiedPatientCount = COUNT(Distinct Patient_id) from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] WITH(NOLOCK) WHERE Valid_Index is not null
SELECT @DoNotMeetPatientCount = @SynomaPatient -@IndexDateQualifiedPatientCount
SELECT @MeetPatientPercentage = (@IndexDateQualifiedPatientCount * 100.00)/ @SynomaPatient
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @SynomaPatient
SELECT @MeetCriteria = CAST(@IndexDateQualifiedPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @MeetPatientPercentageSHA = (@IndexDateQualifiedPatientCount * 100.00)/ @TotalPatientCount
SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @MeetCriteriaSHA = CAST(@IndexDateQualifiedPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.1,'Index Date Qualification',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

/* Age >= 18 */
SELECT @AgeQualifiedPatientCnt = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] CM WITH(NOLOCK)
 INNER JOIN [$(SrcSchema)].[Src_REF_patient] patient 
 ON CM.Patient_ID = patient.Patient_ID WHERE Valid_Index is not null
 and CAST(SUBSTRING(CAST(CM.Valid_Index AS VARCHAR),1,4)  AS INT )- patient.YOB >=18
SELECT @DoNotMeetPatientCount = @IndexDateQualifiedPatientCount -@AgeQualifiedPatientCnt 
SELECT @MeetPatientPercentage = (@AgeQualifiedPatientCnt * 100.00)/ @IndexDateQualifiedPatientCount
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @IndexDateQualifiedPatientCount
SELECT @MeetCriteria = CAST(@AgeQualifiedPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @MeetPatientPercentageSHA = (@AgeQualifiedPatientCnt * 100.00)/ @TotalPatientCount
SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @MeetCriteriaSHA = CAST(@AgeQualifiedPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.2,'Age >=18 at Index date',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

/* Observation Period */
SELECT @ObservationPeriodPatientCnt = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] CM WITH(NOLOCK)
 INNER JOIN [$(SrcSchema)].[Src_REF_patient] patient 
 ON CM.Patient_ID = patient.Patient_ID WHERE Valid_Index is not null and Lookback_Present = 1
 and CAST(SUBSTRING(CAST(CM.Valid_Index AS VARCHAR),1,4)  AS INT )- patient.YOB >=18
SELECT @DoNotMeetPatientCount = @AgeQualifiedPatientCnt -@ObservationPeriodPatientCnt 
SELECT @MeetPatientPercentage = (@ObservationPeriodPatientCnt * 100.00)/ @AgeQualifiedPatientCnt
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @AgeQualifiedPatientCnt
SELECT @MeetCriteria = CAST(@ObservationPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @MeetPatientPercentageSHA = (@ObservationPeriodPatientCnt * 100.00)/ @TotalPatientCount
SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @MeetCriteriaSHA = CAST(@ObservationPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.3,'Data availability in Observation period',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

/* Evaluation Period */
SELECT @EvalutionPeriodPatientCnt = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] CM WITH(NOLOCK)
 INNER JOIN [$(SrcSchema)].[Src_REF_patient] patient 
 ON CM.Patient_ID = patient.Patient_ID WHERE Valid_Index is not null and [Lookforward_Present] = 1 and  Lookback_Present = 1
 and CAST(SUBSTRING(CAST(CM.Valid_Index AS VARCHAR),1,4)  AS INT )- patient.YOB >=18

SELECT @DoNotMeetPatientCount = @ObservationPeriodPatientCnt -@EvalutionPeriodPatientCnt 
SELECT @MeetPatientPercentage = (@EvalutionPeriodPatientCnt * 100.00)/ @ObservationPeriodPatientCnt
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @ObservationPeriodPatientCnt
SELECT @MeetCriteria = CAST(@EvalutionPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @MeetPatientPercentageSHA = (@EvalutionPeriodPatientCnt * 100.00)/ @TotalPatientCount
SELECT @DoNotMeetPatientPercentageSHA = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCount
SELECT @MeetCriteriaSHA = CAST(@EvalutionPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteriaSHA = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentageSHA,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.4,'Data availability in Evaluation period',@MeetCriteria,@DoNotMeetCriteria,@MeetCriteriaSHA,@DoNotMeetCriteriaSHA)

/* DRE Cohort With Indeterminate Patient Count */
SELECT @MeetPatientPercentage = (@EvalutionPeriodPatientCnt * 100.00)/ @EvalutionPeriodPatientCnt
SELECT @MeetCriteria = CAST(@EvalutionPeriodPatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.5,'DRE Cohort - With Indeterminate',@MeetCriteria,'','','')

/* DRE Patient Count */
SELECT @DREPatientCount = COUNT(DISTINCT patient_id)  FROM [$(TargetSchema)].[app_dre_Cohort] WHERE Outcome_variable = 'DRE'
SELECT @DoNotMeetPatientCount = @EvalutionPeriodPatientCnt - @DREPatientCount
SELECT @MeetPatientPercentage = (@DREPatientCount * 100.00)/ @EvalutionPeriodPatientCnt
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @EvalutionPeriodPatientCnt
SELECT @MeetCriteria = CAST(@DREPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.6,'			DRE',@MeetCriteria,@DoNotMeetCriteria,'','')



/* Non DRE Patient Count */
SELECT @NonDREPatientCount = COUNT(DISTINCT patient_id)  FROM [$(TargetSchema)].[app_dre_Cohort] WHERE  Outcome_variable = 'Non_DRE'
SELECT @DoNotMeetPatientCount = @EvalutionPeriodPatientCnt - @NonDREPatientCount
SELECT @MeetPatientPercentage = (@NonDREPatientCount * 100.00)/ @EvalutionPeriodPatientCnt
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @EvalutionPeriodPatientCnt
SELECT @MeetCriteria = CAST(@NonDREPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.6,'			Non DRE',@MeetCriteria,@DoNotMeetCriteria,'','')

/* Indeterminate Patient Count */
SELECT @IndeterminatePatientCount = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] CM WITH(NOLOCK)
 INNER JOIN [$(SrcSchema)].[Src_REF_patient] patient 
 ON CM.Patient_ID = patient.Patient_ID WHERE Valid_Index is not null and [Lookforward_Present] = 1 and  Lookback_Present = 1
 and CAST(SUBSTRING(CAST(CM.Valid_Index AS VARCHAR),1,4)  AS INT )- patient.YOB >=18 and outcome_variable is null
SELECT @DoNotMeetPatientCount = @EvalutionPeriodPatientCnt - @IndeterminatePatientCount
SELECT @MeetPatientPercentage = (@IndeterminatePatientCount * 100.00)/ @EvalutionPeriodPatientCnt
SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @EvalutionPeriodPatientCnt
SELECT @MeetCriteria = CAST(@IndeterminatePatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';
SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.61,'			Indeterminate',@MeetCriteria,'','','')

/* DRE Cohort Without Indeterminate Period */
SELECT @WithoutIndeterminatePatientCnt = COUNT(Distinct cm.Patient_id) from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] CM WITH(NOLOCK)
 INNER JOIN [$(SrcSchema)].[Src_REF_patient] patient 
 ON CM.Patient_ID = patient.Patient_ID WHERE Valid_Index is not null and [Lookforward_Present] = 1 and  Lookback_Present = 1
 and CAST(SUBSTRING(CAST(CM.Valid_Index AS VARCHAR),1,4)  AS INT )- patient.YOB >=18 and outcome_variable is not null

SELECT @MeetPatientPercentage = (@WithoutIndeterminatePatientCnt * 100.00)/ @WithoutIndeterminatePatientCnt
SELECT @MeetCriteria = CAST(@WithoutIndeterminatePatientCnt AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';


INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.7,'Data Cohort - without Indeterminate',@MeetCriteria,'','','')


/* DRE Patient Count */
SELECT @DREPatientCount = COUNT(DISTINCT patient_id)  FROM [$(TargetSchema)].[app_dre_Cohort] WHERE Outcome_variable = 'DRE'
SELECT @MeetPatientPercentage = (@DREPatientCount * 100.00)/ @WithoutIndeterminatePatientCnt
SELECT @MeetCriteria = CAST(@DREPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.8,'			DRE',@MeetCriteria,'','','')



/* Non DRE Patient Count */
SELECT @NonDREPatientCount = COUNT(DISTINCT patient_id)  FROM [$(TargetSchema)].[app_dre_Cohort] WHERE  Outcome_variable = 'Non_DRE'
SELECT @MeetPatientPercentage = (@NonDREPatientCount * 100.00)/ @WithoutIndeterminatePatientCnt
SELECT @MeetCriteria = CAST(@NonDREPatientCount AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,1) AS VARCHAR(300))+'%)';

INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
VALUES (3.8,'			Non DRE',@MeetCriteria,'','','')

/* Patient In TDS Cohort
Need to make sure that latest cohort table available for DRE and TDS should be in same schema*/

--IF('$(RunTDSCohort)'='Y')
--Begin
--	SELECT @TotalPatientCountDRECohort = COUNT(DISTINCT patient_id)  FROM [$(TargetSchema)].[app_dre_Cohort]
--	SELECT @PatientInTDSCohort = COUNT(Distinct Patient_ID) FROM  [$(TargetSchema)].[app_dre_Cohort] WHERE Patient_ID IN(SELECT Distinct cm.Patient_id from [$(TargetSchema)].[app_tds_cohort] CM WITH(NOLOCK) WHERE IsEpilepsy =1 AND Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') /* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1  */ AND  Stable_Eligible =1 AND LEN(AED) = 3 AND Age >=18 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1 )
--	SELECT @DoNotMeetPatientCount = @TotalPatientCountDRECohort - @PatientInTDSCohort
--	SELECT @MeetPatientPercentage = (@PatientInTDSCohort * 100.00)/ @TotalPatientCountDRECohort
--	SELECT @DoNotMeetPatientPercentage = (@DoNotMeetPatientCount * 100.00)/ @TotalPatientCountDRECohort
--	SELECT @MeetCriteria = CAST(@PatientInTDSCohort AS varchar(300)) +' ('+CAST(round(@MeetPatientPercentage,2) AS VARCHAR(300))+')';
--	SELECT @DoNotMeetCriteria = CAST(@DoNotMeetPatientCount AS varchar(300))  +' ('+CAST(round(@DoNotMeetPatientPercentage,2) AS VARCHAR(300))+')';

--	INSERT INTO [$(TargetSchema)].[PHCPipelineMetricReport] ([ID],[Criteria], [Meets criteria Patient count n (%)], [Do not meet criteria Patient count n (%)],[Meets criteria Patient count n (%) with SHA],[Do not meet criteria Patient count n (%) with SHA])
--	VALUES (3.9,'Present in TDS Cohort',@MeetCriteria,@DoNotMeetCriteria,NULL,NULL)
--End

SELECT CURRENT_TIMESTAMP AS END_TIMESTAMP

