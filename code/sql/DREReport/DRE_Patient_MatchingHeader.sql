-- ---------------------------------------------------------------------
-- - AUTHOR			: Supriya S
-- - USED BY		: 
-- - 
-- - PURPOSE		: Creating DRE_Patient_matching table
-- - NOTE			: ELREFPRED-977
-- - Execution		: 
-- - Date			: 23-Aug-2019			
-- - Tables Required: [$(SrcSchema)].[PHPH_SynomaIDs], [$(SrcSchema)].[SRC_Ref_Patient],[$(TargetSchema)].[app_dre_Cohort]
-- - Change History:			
-- ---------------------------------------------------------------------- 

--:setvar SrcSchema SHA2_Synoma_ID_SRC
--:Setvar TargetSchema SHA2_Synoma_ID_TGT

SET NOCOUNT ON

IF OBJECT_ID('dbo.DRE_patient_matching','U') IS NOT NULL
DROP TABLE dbo.DRE_patient_matching

CREATE TABLE dbo.DRE_patient_matching
(
	[SYNOMA_PATIENT_ID] [varchar](100) NULL,
	[SHA_MATCH_FLAG] [varchar](1) NULL,
	[DRE_MATCH_FLAG] [varchar](1) NULL,
	[TRAIN_MATCH_FLAG] [varchar](1) NULL,
	[TEST_MATCH_FLAG] [varchar](1) NULL
)

--Truncate table #DRE_patient_matching

Insert into dbo.DRE_patient_matching (SYNOMA_PATIENT_ID)
select distinct SHS_patient_id from [$(SrcSchema)].[PHPH_SynomaIDs]

Update dbo.DRE_patient_matching
set [SHA_MATCH_FLAG] = 0

Update dbo.DRE_patient_matching
set [SHA_MATCH_FLAG] = 1 
from [$(SrcSchema)].[SRC_Ref_Patient]
where dbo.DRE_patient_matching.[SYNOMA_PATIENT_ID] = replace(ltrim(replace([$(SrcSchema)].[SRC_Ref_Patient].[PATIENT_ID], '0',' ')),' ','0')

Update dbo.DRE_patient_matching
set [DRE_MATCH_FLAG] = 0
where [SHA_MATCH_FLAG] = 1


Update dbo.DRE_patient_matching
set [DRE_MATCH_FLAG] = 1 
from [$(TargetSchema)].[app_dre_Cohort]
where dbo.DRE_patient_matching.[SYNOMA_PATIENT_ID] = replace(ltrim(replace([$(TargetSchema)].[app_dre_Cohort].[PATIENT_ID], '0',' ')),' ','0')
and  valid_index is not null

Update dbo.DRE_patient_matching
set [TRAIN_MATCH_FLAG] = 0
where [SHA_MATCH_FLAG] = 1

Update dbo.DRE_patient_matching
set [TRAIN_MATCH_FLAG] = 1 
from dbo.[DRETrain6.5]
where dbo.DRE_patient_matching.[SYNOMA_PATIENT_ID] = replace(ltrim(replace(dbo.[DRETrain6.5].[PATIENT_ID], '0',' ')),' ','0')
AND DRE_MATCH_FLAG = 1

Update dbo.DRE_patient_matching
set [TEST_MATCH_FLAG] = 0
where [SHA_MATCH_FLAG] = 1


Update dbo.DRE_patient_matching
set [TEST_MATCH_FLAG] = 1 
from dbo.[DRETest6.5]
where dbo.DRE_patient_matching.[SYNOMA_PATIENT_ID] = replace(ltrim(replace(dbo.[DRETest6.5].[PATIENT_ID], '0',' ')),' ','0')
AND DRE_MATCH_FLAG = 1

IF OBJECT_ID('dbo.atleastOneAED','U') IS NOT NULL
DROP TABLE dbo.atleastOneAED
select distinct patient_id into dbo.atleastOneAED from [SHA2_Synoma_ID_TGT].[app_int_AED_Analysis_Continuous_Prescription] 
WHERE  AED_Days >30 AND Patient_id IN (SELECT DISTINCT Patient_id from [SHA2_Synoma_ID_TGT].[app_int_Patient_Profile] WHERE [IsEpilepsy] =1 )


select a.patient_id, case when d.patient_id is null then 0 else 1 end as SHA_Match_Flag , 
case when DRE_MATCH_FLAG = 1 then 1 else 0 end as DRE_Match_Flag,
case when TRAIN_MATCH_FLAG = 1 then 1 else 0 end as TRAIN_Match_Flag, 
case when TEST_MATCH_FLAG = 1 then 1 else 0 end as TEST_Match_Flag,
case when d.[IsEpilepsy] is null then 0 else d.[IsEpilepsy] end as [Has_Epi_Dx], 
case when e.patient_id is null then 0 else 1 end as [AED>30],
case when b.valid_index is null then 0 else 1 end as Valid_Index_Date,
case when age>=18 then 1 else 0 end as [Age>=18],
case when b.lookback_present is null then 0 else b.lookback_present end as Lookback_present, 
case when b.Lookforward_Present is null then 0 else b.Lookforward_Present end as Lookforward_present--, f.outcome_variable
--into #temp11
from [SHA_Synoma_ID_SRC].[PHPH_SynomaIDs] a
left join [SHA2_Synoma_ID_TGT].[app_int_Patient_Profile] d
on a.patient_id = d.patient_id
left join dbo.atleastOneAED e
on a.patient_id = e.patient_id
left join [SHA2_Synoma_ID_TGT].[app_dre_int_Cohort_Monotherapy] b
on a.patient_id = b.patient_id and b.valid_index is not null
left join [SHA2_Synoma_ID_TGT].[app_dre_int_Pipeline_Metrix_Patient_Counts] c
on a.patient_id = c.patient_id
Left join dbo.DRE_patient_matching abc
on replace(str(abc.[SYNOMA_PATIENT_ID], 20), ' ','0') = d.patient_id
where 1=2--where train_match_flag=1




