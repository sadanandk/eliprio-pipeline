---:SetVar TargetSchema boston_ucb_test
---:SetVar SrcSchema Boston_UCB_SRC

IF OBJECT_ID('[$(TargetSchema)].[DRE_Scores]') IS NOT NULL
	DROP TABLE [$(TargetSchema)].[DRE_Scores]

CREATE TABLE [$(TargetSchema)].[DRE_Scores] (
	[patient_id] [varchar](50) NULL
	,[Index_Date] [varchar](50) NULL
	,[Age] [varchar](50) NULL
	,[Gender] [varchar](50) NULL
	,[CCI_Score] [varchar](50) NULL
	,[Outcome_variable] [varchar](200) NULL
	,[Model Label] [varchar](100) NULL
	,[Class probabilities] [varchar](100) NULL
	,[DRERiskStratification] [varchar] (30) NULL
	)


BULK INSERT [$(TargetSchema)].[DRE_Scores]
FROM 
'$(outDataPath)\d5_dre_sha_score.csv'
WITH (
		rowterminator = '\n'
		,fieldterminator = ','
		,FIRSTROW = 2
		);


--update [$(TargetSchema)].dre_scores 
--set patient_id = replace(patient_id,'"',''),
--Index_Date = replace(Index_Date,'"',''),
--Age = replace(Age,'"',''),
--Gender = replace(Gender,'"',''),
--CCI_Score = replace(CCI_Score,'"',''),
--Outcome_variable = replace(Outcome_variable,'"','')


IF OBJECT_ID('[$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr]


---patients with 2.5 year evaluation period
select * into [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr]
from [$(TargetSchema)].[DRE_Scores] where patient_id in (
SELECT distinct patient_id
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 
WHERE lookback_present = 1
	AND lookforward_present = 1
	AND valid_index IS NOT NULL 
	AND DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913)

