---------------------------------End point 2.2---------------------------------------

--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_ID_4NOV

IF OBJECT_ID('tempdb..#PT') IS NOT NULL
DROP TABLE #PT

select * into #PT  from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913 and lookback_present=1 and lookforward_present=1 and valid_index is not null---1221

IF OBJECT_ID('tempdb..#1Yr_Mono') IS NOT NULL
DROP TABLE #1Yr_Mono

select * into #1Yr_Mono  from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where patient_id in (select distinct patient_id from #PT)---3272653

IF OBJECT_ID('tempdb..#AED') IS NOT NULL
DROP TABLE #AED

select  a.patient_id,a.valid_index,count (distinct b.AED) Unique_Failed_AED1Yr,a.outcome_variable into #AED from #PT a left join #1Yr_Mono b on a.patient_id=b.patient_id
where (datediff(dd,a.valid_index,b.maxdate) between 0 and 365 ) and b.aed_failure_flag=1
group by  a.patient_id,a.valid_index,a.outcome_variable

IF OBJECT_ID('tempdb..#AED_NO') IS NOT NULL
DROP TABLE #AED_NO

select  a.patient_id,a.valid_index,count (distinct b.AED) Unique_AED1Yr,a.outcome_variable into #AED_NO from #PT a left join #1Yr_Mono b on a.patient_id=b.patient_id
where (datediff(dd,a.valid_index,b.maxdate) between 0 and 365 ) ---and b.aed_failure_flag=1
group by  a.patient_id,a.valid_index,a.outcome_variable

IF OBJECT_ID('tempdb..#Failed_Aed_1yrEval') IS NOT NULL
DROP TABLE #Failed_Aed_1yrEval

select a.patient_id,a.valid_index,isnull(b.Unique_Failed_AED1Yr,0) Unique_Failed_AED1Yr,a.outcome_variable,a.Patient_Exclusion_Flag into #Failed_Aed_1yrEval
from #1Yr_Mono a left join #AED b on a.patient_id=b.patient_id and  a.valid_index=b.valid_index where  a.valid_index is not null

IF OBJECT_ID('tempdb..#Aed_1yrEval') IS NOT NULL
DROP TABLE #Aed_1yrEval

select a.patient_id,a.valid_index,case when isnull(b.Unique_AED1Yr,0)=1 and a.aed_failure_flag=0 then 0 else b.Unique_AED1Yr end as unique_AED,
Unique_AED1Yr,a.outcome_variable,a.Patient_Exclusion_Flag into #Aed_1yrEval
from #1Yr_Mono a left join #AED_NO b on a.patient_id=b.patient_id and  a.valid_index=b.valid_index where  a.valid_index is not null

IF OBJECT_ID('[$(TargetSchema)].Table11') IS NOT NULL
DROP TABLE [$(TargetSchema)].Table11

create table [$(TargetSchema)].Table11 (
Evaluation_Window varchar(50),
[DRERiskStratification] varchar(50) ,
AED_Failure bigint,
Patient_Count bigint);



--------------------------2 aed failure-----------------------
--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_ID_4NOV
IF OBJECT_ID('tempdb..#2_AEd_Failure') IS NOT NULL
DROP TABLE #2_AEd_Failure

select [DRERiskStratification],count (distinct dre.patient_id) patient_count into  #2_AEd_Failure from 
(select a.* 
 from #Failed_Aed_1yrEval a inner join [$(SrcSchema)].[src_REF_patient] b on a.patient_id=convert (float,b.patient_id)
-----where Zip3  IN ('010', '019', '020', '021', '022', '024', '025', '038')
) main inner join 
[$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where Unique_Failed_AED1Yr>1 group by [DRERiskStratification]

---------------------no aed failure---------------------------

IF OBJECT_ID('tempdb..#No_AED_Failure') IS NOT NULL
DROP TABLE #No_AED_Failure

select [DRERiskStratification],count (distinct dre.patient_id) patient_count into #No_AED_Failure from 
(select a.* 
 from  #Aed_1yrEval a inner join [$(SrcSchema)].[src_REF_patient] b on a.patient_id=convert (float,b.patient_id)
----where Zip3  IN ('010', '019', '020', '021', '022', '024', '025', '038')
) main  join 
[$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where  
 main.patient_id not in (select distinct patient_id from #Failed_Aed_1yrEval where Unique_Failed_AED1Yr>1)
 group by [DRERiskStratification]
-----------------------------------------2.5 year-----------------------------------------
IF OBJECT_ID('tempdb..#2_5') IS NOT NULL
DROP TABLE #2_5

select * into #2_5 from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913 and valid_index is not null and Lookback_Present=1 and Lookforward_Present=1  ---1056125

IF OBJECT_ID('tempdb..#Base') IS NOT NULL
DROP TABLE #Base

select * into #Base  from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where patient_id in (select distinct patient_id from #2_5)
------------------------------Calcualte failed AED count  -----------------------------------

IF OBJECT_ID('tempdb..#AED1') IS NOT NULL
DROP TABLE #AED1

select b.patient_id,p.valid_index,count (distinct b.AED) Failed_unique_AED,count (b.AED) Failed_AED,p.outcome_variable 
 into #AED1 from  #base B left join #2_5 P on P.patient_id=convert (float,b.patient_id)
where (datediff(dd,p.valid_index,b.maxdate) between 0 and 913 ) and
   b.aed_failure_flag=1 and b.maxdate>=p.valid_index
group by  b.patient_id,p.valid_index,p.outcome_variable----647452

IF OBJECT_ID('tempdb..#AED1_NO') IS NOT NULL
DROP TABLE #AED1_NO

select b.patient_id,p.valid_index,count (distinct b.AED) unique_AED,count (b.AED) Failed_AED,p.outcome_variable 
 into #AED1_NO from  #base B left join #2_5 P on P.patient_id=convert (float,b.patient_id)
where (datediff(dd,p.valid_index,b.maxdate) between 0 and 913 ) and
  --- b.aed_failure_flag=1   and 
  b.maxdate>=p.valid_index group by  b.patient_id,p.valid_index,p.outcome_variable----647452

-----------------Create subcohort with required columns----------------------------------

--- drop table [SHA_Pipeline_All_Target].Failed_Aed_2_5yrEval
IF OBJECT_ID('tempdb..#Failed_Aed_2_5') IS NOT NULL
DROP TABLE #Failed_Aed_2_5

select a.patient_id,a.valid_index,isnull(b.Failed_unique_AED,0) Failed_unique_AED,isnull(b.Failed_AED,0) Failed_AED,a.outcome_variable,a.Patient_Exclusion_Flag into #Failed_Aed_2_5 from #2_5 a left join #AED1 b on a.patient_id=convert (float,b.patient_id)
inner join #base c on a.patient_id=c.patient_id and a.valid_index=c.valid_index

IF OBJECT_ID('tempdb..#NO_Aed_2_5') IS NOT NULL
DROP TABLE #NO_Aed_2_5

select a.patient_id,a.valid_index,case when isnull(b.unique_AED,0)=1 and a.aed_failure_flag=0 then 0 else b.unique_AED end as unique_AED,isnull(b.Failed_AED,0) Failed_AED,a.outcome_variable,a.Patient_Exclusion_Flag into #NO_Aed_2_5 from #2_5 a left join #AED1_NO b on a.patient_id=convert (float,b.patient_id)
inner join #base c on a.patient_id=c.patient_id and a.valid_index=c.valid_index

---drop table [SHA_Pipeline_All_Target].[Failed_Aed_2_5yrBoston]

IF OBJECT_ID('tempdb..#Failed_Aed_2_5yrBoston') IS NOT NULL
DROP TABLE #Failed_Aed_2_5yrBoston

select a.* into #Failed_Aed_2_5yrBoston from #Failed_Aed_2_5 a inner join  [$(SrcSchema)].[src_REF_patient] b on a.patient_id=convert (float,b.patient_id)
----where Zip3  IN ('010', '019', '020', '021', '022', '024', '025', '038')

IF OBJECT_ID('tempdb..#2_AEd_Failure_2_5') IS NOT NULL
DROP TABLE #2_AEd_Failure_2_5

select [DRERiskStratification],count (distinct dre.patient_id) patient_count into #2_AEd_Failure_2_5 from 
(select a.* 
 from #Failed_Aed_2_5yrBoston a inner join [$(SrcSchema)].[src_REF_patient] b on a.patient_id=convert (float,b.patient_id)
---where Zip3  IN ('010', '019', '020', '021', '022', '024', '025', '038')
) main inner join 
[$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where Failed_unique_AED>1 group by [DRERiskStratification]

IF OBJECT_ID('tempdb..#No_AED_Failure_2_5') IS NOT NULL
DROP TABLE #No_AED_Failure_2_5

select [DRERiskStratification],count (distinct dre.patient_id) patient_count into #No_AED_Failure_2_5 from 
(select a.* 
 from #NO_Aed_2_5 a inner join [$(SrcSchema)].[src_REF_patient] b on a.patient_id=convert (float,b.patient_id)
---where Zip3  IN ('010', '019', '020', '021', '022', '024', '025', '038')
) main inner join 
[$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where 
 main.patient_id not in (select distinct patient_id from #Failed_Aed_2_5 where Failed_unique_AED>1)
 group by [DRERiskStratification]



Insert into [$(TargetSchema)].Table11 select 'Within 1 year',[DRERiskStratification],2,patient_count from #2_AEd_Failure
Insert into [$(TargetSchema)].Table11 select 'Within 1 year',[DRERiskStratification],0,patient_count from #No_AED_Failure
Insert into [$(TargetSchema)].Table11 select 'Within 2.5 year',[DRERiskStratification],2,patient_count from #2_AEd_Failure_2_5
Insert into [$(TargetSchema)].Table11 select 'Within 2.5 year',[DRERiskStratification],0,patient_count from #No_AED_Failure_2_5