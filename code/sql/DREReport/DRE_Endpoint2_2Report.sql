--:setvar TargetSchema SHA_Pipeline_All_Target
SET NOCOUNT ON

IF OBJECT_ID('tempdb..#Tot') IS NOT NULL
	DROP TABLE #Tot

SELECT sum(patient_count) AS tot_cnt
	,Evaluation_Window Evaluation
	,DRERiskStratification RiskStratification
INTO #Tot
FROM [$(TargetSchema)].Table11
GROUP BY Evaluation_Window
	,DRERiskStratification

SET NOCOUNT ON

SELECT ''
WHERE 1 <> 1

SELECT CAST('  ' AS VARCHAR(30))
	,'        				' [DRE Endpoint 2.2]
	,CAST('' AS VARCHAR(19))
WHERE 1 <> 1

SELECT CAST('  ' AS VARCHAR(30))
	,'        				' Evaluation
	,CAST('' AS VARCHAR(23)) Window
WHERE 1 <> 1

SELECT CAST(' ' AS VARCHAR(30))
	,CAST('' AS VARCHAR(25)) AS [Within 1 Year]
	,CAST('' AS VARCHAR(5))
	,CAST('' AS VARCHAR(24)) AS [Within 2.5 Years]
WHERE 1 <> 1

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
	DROP TABLE #TEMP

SELECT ID
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([2 AED failure1] AS VARCHAR(30)) AS [2 AED failure]
	,CAST([No AED failure1] AS VARCHAR(30)) AS [No AED failure]
	,CAST([2 AED failure2] AS VARCHAR(30)) AS [2 AED failure1]
	,CAST([No AED failure2] AS VARCHAR(30)) AS [No AED failure1]
INTO #TEMP
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,a.patient_count AS [2 AED failure1]
		,b.patient_count AS [No AED failure1]
		,c.patient_count AS [2 AED failure2]
		,d.patient_count AS [No AED failure2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].[Table11] a
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[Table11] a
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[Table11] a
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[Table11] a
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

DECLARE @Sensityvity1year DECIMAL(5, 2);
DECLARE @Sensityvity2year DECIMAL(5, 2);
DECLARE @Specivity1year DECIMAL(5, 2);
DECLARE @Specivity2year DECIMAL(5, 2);
DECLARE @Total2AEDFailure INT;
DECLARE @2AEDFailureHigh INT;
DECLARE @2AEDFailureMedium INT;
DECLARE @2AEDFailureLow INT;
DECLARE @Total2AEDFailure1 INT;
DECLARE @2AEDFailureHigh1 INT;
DECLARE @2AEDFailureMedium1 INT;
DECLARE @2AEDFailureLow1 INT;

SELECT @2AEDFailureHigh = [2 AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @2AEDFailureMedium = [2 AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @2AEDFailureLow = [2 AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @Total2AEDFailure = @2AEDFailureHigh + @2AEDFailureMedium + @2AEDFailureLow

SELECT @Sensityvity1year = (((@2AEDFailureHigh + @2AEDFailureMedium) * 1.0) / @Total2AEDFailure) * 100

SELECT @2AEDFailureHigh1 = [2 AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @2AEDFailureMedium1 = [2 AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @2AEDFailureLow1 = [2 AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @Total2AEDFailure1 = @2AEDFailureHigh1 + @2AEDFailureMedium1 + @2AEDFailureLow1

SELECT @Sensityvity2year = (((@2AEDFailureHigh1 + @2AEDFailureMedium1) * 1.0) / @Total2AEDFailure1) * 100

INSERT INTO #TEMP
SELECT 4 AS [id]
	,CAST('Sensitivity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST(@Sensityvity1year AS VARCHAR(30)) AS [2 AED failure]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure]
	,CAST(@Sensityvity2year AS VARCHAR(30)) AS [2 AED failur1e]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure1]

/* Specivity Details*/
DECLARE @TotalNoAEDFailure INT;
DECLARE @NoAEDFailureHigh INT;
DECLARE @NoAEDFailureMedium INT;
DECLARE @NoAEDFailureLow INT;
DECLARE @TotalNoAEDFailure1 INT;
DECLARE @NoAEDFailureHigh1 INT;
DECLARE @NoAEDFailureMedium1 INT;
DECLARE @NoAEDFailureLow1 INT;

SELECT @NoAEDFailureHigh = [No AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NoAEDFailureMedium = [No AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NoAEDFailureLow = [No AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNoAEDFailure = @NoAEDFailureHigh + @NoAEDFailureMedium + @NoAEDFailureLow

SELECT @Specivity1year = (((@NoAEDFailureLow) * 1.0) / @TotalNoAEDFailure) * 100

SELECT @NoAEDFailureHigh1 = [No AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NoAEDFailureMedium1 = [No AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NoAEDFailureLow1 = [No AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNoAEDFailure1 = @NoAEDFailureHigh1 + @NoAEDFailureMedium1 + @NoAEDFailureLow1

SELECT @Specivity2year = (((@NoAEDFailureLow1) * 1.0) / @TotalNoAEDFailure1) * 100

INSERT INTO #TEMP
SELECT 5 AS [id]
	,CAST('Specificity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST(@Specivity1year AS VARCHAR(30)) AS [2 AED failure]
	,CAST(NULL AS VARCHAR(30)) AS [2 AED failure]
	,CAST(@Specivity2year AS VARCHAR(30)) AS [DRE1]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure1]

IF OBJECT_ID('tempdb..#TEMP1') IS NOT NULL
	DROP TABLE #TEMP1

SELECT ID
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([2 AED failure1] AS VARCHAR(30)) AS [2 AED failure]
	,CAST([No AED failure1] AS VARCHAR(30)) AS [No AED failure]
	,CAST([2 AED failure2] AS VARCHAR(30)) AS [2 AED failure1]
	,CAST([No AED failure2] AS VARCHAR(30)) AS [No AED failure1]
INTO #TEMP1
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,CONCAT (
			a.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((a.patient_count * 1.0 / a.tot_cnt) * 100)))
			,')'
			) AS [2 AED failure1]
		,CONCAT (
			b.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((b.patient_count * 1.0 / b.tot_cnt) * 100)))
			,')'
			) AS [No AED failure1]
		,CONCAT (
			c.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((c.patient_count * 1.0 / c.tot_cnt) * 100)))
			,')'
			) AS [2 AED failure2]
		,CONCAT (
			d.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((d.patient_count * 1.0 / d.tot_cnt) * 100)))
			,')'
			) AS [No AED failure2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
		ON a.DRERiskStratification = t.RiskStratification
			AND a.Evaluation_Window = t.Evaluation
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
		ON a.DRERiskStratification = t.RiskStratification
			AND a.Evaluation_Window = t.Evaluation
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
		ON a.DRERiskStratification = t.RiskStratification
			AND a.Evaluation_Window = t.Evaluation
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
		ON a.DRERiskStratification = t.RiskStratification
			AND a.Evaluation_Window = t.Evaluation
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification

	) p

UNION

SELECT ID AS [ID]
	,[DRE Risk Stratification]
	,[2 AED failure] AS [2 AED failure]
	,[No AED failure] AS [No AED failure]
	,[2 AED failure1] AS [2 AED failure1]
	,[No AED failure1] AS [No AED failure1]
FROM #TEMP
WHERE ID IN (
		4
		,5
		)

SELECT [DRE Risk Stratification]
	,[2 AED failure] AS [2 AED failure]
	,ISNULL([No AED failure], '') AS [No AED failure]
	,[2 AED failure1] AS [2 AED failure1]
	,ISNULL([No AED failure1], '') AS [No AED failure1]
FROM #Temp1
ORDER BY [ID]