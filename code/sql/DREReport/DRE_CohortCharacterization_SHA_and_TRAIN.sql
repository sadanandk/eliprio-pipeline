
Print 'Populate Cohort_Char_SHA_1_Yr train '
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_1Y]') IS  NULL

CREATE TABLE [$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_1Y] (
	ID Decimal(5,1)
	,[Variable] VARCHAR(500)
	,[Statistic] VARCHAR(500)
	,[Train] VARCHAR(500)
	);

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_1Y]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].dre_cohortcharacterization_SHA_TRAIN_1Y

	BULK INSERT [$(TargetSchema)].dre_cohortcharacterization_SHA_TRAIN_1Y
	FROM '$(CsvSHAPath)Cohort_Char_SHA_Train_1_Yr.csv'
	WITH 
	(
	FirstRow = 2,
	rowterminator = '\n',
	fieldterminator =','
	);

END 


Print 'Populate Cohort_Char_SHA_1_Yr'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[dre_cohortcharacterization_SHA_1Y]') IS  NULL

CREATE TABLE [$(TargetSchema)].[dre_cohortcharacterization_SHA_1Y] (
	ID Decimal(5,1)
	,[Variable] VARCHAR(500)
	,[Statistic] VARCHAR(500)
	,[SHA] VARCHAR(500)
	);

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[dre_cohortcharacterization_SHA_1Y]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].dre_cohortcharacterization_SHA_1Y

	BULK INSERT [$(TargetSchema)].dre_cohortcharacterization_SHA_1Y
	FROM '$(CsvSHAPath)Cohort_Char_SHA_1_Yr.csv'
	WITH 
	(
	FirstRow = 2,
	rowterminator = '\n',
	fieldterminator =','
	
	);

END 

Print 'Populate Cohort_Char_SHA_Train_2_5_Yr'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_2_5Y]') IS  NULL

CREATE TABLE [$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_2_5Y] (
	 ID Decimal(5,1)
	,[Variable] VARCHAR(500)
	,[Statistic] VARCHAR(500)
	,[Train] VARCHAR(500)
	);

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_2_5Y]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].dre_cohortcharacterization_SHA_TRAIN_2_5Y

	BULK INSERT [$(TargetSchema)].dre_cohortcharacterization_SHA_TRAIN_2_5Y
	FROM '$(CsvSHAPath)Cohort_Char_SHA_Train_2_5_Yr.csv'
	WITH 
	(
	FirstRow = 2,
	rowterminator = '\n',
	fieldterminator =','
	);

END 


Print 'Populate Cohort_Char_SHA_2_5_Yr'
Print Getdate()


IF OBJECT_ID('[$(TargetSchema)].[dre_cohortcharacterization_SHA_2_5Y]') IS  NULL

CREATE TABLE [$(TargetSchema)].[dre_cohortcharacterization_SHA_2_5Y] (
	ID Decimal(5,1)
	,[Variable] VARCHAR(500)
	,[Statistic] VARCHAR(500)
	,[SHA] VARCHAR(500)
	);

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[dre_cohortcharacterization_SHA_2_5Y]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].dre_cohortcharacterization_SHA_2_5Y

	BULK INSERT [$(TargetSchema)].dre_cohortcharacterization_SHA_2_5Y
	FROM '$(CsvSHAPath)Cohort_Char_SHA_2_5_Yr.csv'
	WITH 
	(
	FirstRow = 2,
	rowterminator = '\n',
	fieldterminator =','
	);

END 