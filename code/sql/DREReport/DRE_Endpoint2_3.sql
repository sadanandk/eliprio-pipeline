-------------------------------------
----Table used :[$(TargetSchema)].[app_dre_int_Cohort_Monotherapy],[$(TargetSchema)].[DRE_Scores]
----Logic: Number of failed AED within 2.5 year and number of failed aed for patient qualifying for 2.5 year 

---------------------------------------
--:SetVar TargetSchema SHA2_Synoma_ID_Endpt_Int
--:SetVar SrcSchema SHA2_Synoma_ID_SRC



----------------------------------------2.5 year-----------------------------------------
IF OBJECT_ID('tempdb..#2_5') IS NOT NULL
DROP TABLE #2_5

select * into #2_5 from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913 and valid_index is not null and Lookback_Present=1 and Lookforward_Present=1  ---1056125

IF OBJECT_ID('tempdb..#Base') IS NOT NULL
DROP TABLE #Base

select * into #Base  from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where patient_id in (select distinct patient_id from #2_5)
------------------------------Calcualte failed AED count  -----------------------------------

IF OBJECT_ID('tempdb..#AED1') IS NOT NULL
DROP TABLE #AED1

select b.patient_id,p.valid_index,count (distinct b.AED) Failed_unique_AED,count (b.AED) Failed_AED,p.outcome_variable 
 into #AED1 from  #base B left join #2_5 P on P.patient_id=convert (float,b.patient_id)
where (datediff(dd,p.valid_index,b.maxdate) between 0 and 913 ) and
   b.aed_failure_flag=1 and b.maxdate>=p.valid_index
group by  b.patient_id,p.valid_index,p.outcome_variable----647452


IF OBJECT_ID('tempdb..#AED1_NO') IS NOT NULL
DROP TABLE #AED1_NO

select b.patient_id,p.valid_index,count (distinct b.AED) unique_AED,count (b.AED) Failed_AED,p.outcome_variable 
 into #AED1_NO from  #base B left join #2_5 P on P.patient_id=convert (float,b.patient_id)
where (datediff(dd,p.valid_index,b.maxdate) between 0 and 913 ) and
  --- b.aed_failure_flag=1   and 
  b.maxdate>=p.valid_index group by  b.patient_id,p.valid_index,p.outcome_variable----647452
-----------------Create subcohort with required columns----------------------------------

--- drop table [SHA_Pipeline_All_Target].Failed_Aed_2_5yrEval
IF OBJECT_ID('tempdb..#Failed_Aed_2_5_Within') IS NOT NULL
DROP TABLE #Failed_Aed_2_5_Within

select a.patient_id,a.valid_index,isnull(b.Failed_unique_AED,0) Failed_unique_AED,isnull(b.Failed_AED,0) Failed_AED,a.outcome_variable,a.Patient_Exclusion_Flag into #Failed_Aed_2_5_Within from #2_5 a left join #AED1 b on a.patient_id=convert (float,b.patient_id)
inner join #base c on a.patient_id=c.patient_id and a.valid_index=c.valid_index

IF OBJECT_ID('tempdb..#NO_Aed_2_5_Within') IS NOT NULL
DROP TABLE #NO_Aed_2_5_Within

select a.patient_id,a.valid_index,case when isnull(b.unique_AED,0)=1 and a.aed_failure_flag=0 then 0 else b.unique_AED end as unique_AED,isnull(b.Failed_AED,0) Failed_AED,a.outcome_variable,a.Patient_Exclusion_Flag into #NO_Aed_2_5_Within from #2_5 a left join #AED1_NO b on a.patient_id=convert (float,b.patient_id)
inner join #base c on a.patient_id=c.patient_id and a.valid_index=c.valid_index

---drop table [SHA_Pipeline_All_Target].[Failed_Aed_2_5yrBoston]

--IF OBJECT_ID('tempdb..#Failed_Aed_2_5yrBoston') IS NOT NULL
--DROP TABLE #Failed_Aed_2_5yrBoston

--select a.* into #Failed_Aed_2_5yrBoston from #Failed_Aed_2_5_Within a inner join  [$(SrcSchema)].[src_REF_patient] b on a.patient_id=convert (float,b.patient_id)
----where Zip3  IN ('010', '019', '020', '021', '022', '024', '025', '038')

IF OBJECT_ID('tempdb..#2_AEd_Failure_2_5_Within') IS NOT NULL
DROP TABLE #2_AEd_Failure_2_5_Within

select [DRERiskStratification],count (distinct dre.patient_id) patient_count into #2_AEd_Failure_2_5_Within from 
(select a.* 
 from #Failed_Aed_2_5_Within a inner join [$(SrcSchema)].[src_REF_patient] b on a.patient_id=convert (float,b.patient_id)
---where Zip3  IN ('010', '019', '020', '021', '022', '024', '025', '038')
) main inner join 
[$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where Failed_unique_AED>1 group by [DRERiskStratification]

IF OBJECT_ID('tempdb..#No_AED_Failure_2_5_Within') IS NOT NULL
DROP TABLE #No_AED_Failure_2_5_Within

select [DRERiskStratification],count (distinct dre.patient_id) patient_count into #No_AED_Failure_2_5_Within from 
(select * from #NO_Aed_2_5_Within ) main inner join 
 [$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where 
 main.patient_id not in (select distinct patient_id from #Failed_Aed_2_5_Within where Failed_unique_AED>1)
 group by [DRERiskStratification]


IF OBJECT_ID('[$(TargetSchema)].Table12') IS Not NULL
DROP TABLE    [$(TargetSchema)].Table12

create table [$(TargetSchema)].Table12 (
Evaluation_Window varchar(50),
[DRERiskStratification] varchar(50) ,
AED_Failure bigint,
Patient_Count bigint);

Insert into [$(TargetSchema)].Table12 select 'Within 2.5 year',[DRERiskStratification],2,patient_count from #2_AEd_Failure_2_5_Within
Insert into [$(TargetSchema)].Table12 select 'Within 2.5 year',[DRERiskStratification],0,patient_count from #No_AED_Failure_2_5_Within

------------------------------------------------------->=2.5-------------------------------------------------

--:SetVar TargetSchema SHA2_Synoma_ID_pankaj
--:SetVar SrcSchema SHA2_Synoma_ID_SRC
------------------------------------2.5 year period--------------------------------

IF OBJECT_ID('tempdb..#Patient2') IS NOT NULL
DROP TABLE #Patient2 

select * into #Patient2 from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913 and valid_index is not null and Lookback_Present=1 and Lookforward_Present=1  ---1112

IF OBJECT_ID('tempdb..#Base1') IS NOT NULL
DROP TABLE #Base1 

select * into #Base1  from [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] where patient_id in (select distinct patient_id from #Patient2)---3716
------------------------------Calcualte failed AED count  -----------------------------------
---drop table #AED_CNT

IF OBJECT_ID('tempdb..#AED2') IS NOT NULL
DROP TABLE #AED2 
 
 select b.patient_id,p.valid_index,count (distinct b.AED) Failed_unique_AED,count (b.AED) Failed_AED,p.outcome_variable into #AED2 from  #base1 B left join #Patient2 P on P.patient_id=b.patient_id 
 where ---(datediff(dd,p.valid_index,b.maxdate) between 0 and 365 ) and
   b.aed_failure_flag=1 and b.maxdate>=p.valid_index group by  b.patient_id,p.valid_index,p.outcome_variable---752

IF OBJECT_ID('tempdb..#AED2_NO') IS NOT NULL
DROP TABLE #AED2_NO 

select b.patient_id,p.valid_index,count (distinct b.AED) Failed_unique_AED,count (b.AED) Failed_AED,p.outcome_variable into #AED2_NO from  #base1 B left join #Patient2 P on P.patient_id=b.patient_id 
where ---(datediff(dd,p.valid_index,b.maxdate) between 0 and 365 ) and
---b.aed_failure_flag=1 and
b.maxdate>=p.valid_index group by  b.patient_id,p.valid_index,p.outcome_variable---1112

-----------------Create subcohort with required columns----------------------------------

IF OBJECT_ID('tempdb..#Failed_Aed_2_5') IS NOT NULL
DROP TABLE #Failed_Aed_2_5

select a.patient_id,a.valid_index,isnull(b.Failed_unique_AED,0) Failed_unique_AED,isnull(b.Failed_AED,0) Failed_AED,a.outcome_variable,a.Patient_Exclusion_Flag into #Failed_Aed_2_5 from #Patient2 a left join #AED2 b on a.patient_id=b.patient_id
inner join #base1 c on a.patient_id=c.patient_id and a.valid_index=c.valid_index

IF OBJECT_ID('tempdb..#No_Aed_2_5') IS NOT NULL
DROP TABLE #No_Aed_2_5

select a.patient_id,a.valid_index,case when isnull(b.Failed_unique_AED,0)=1 and a.aed_failure_flag=0 then 0 else b.Failed_unique_AED end as Failed_unique_AED,isnull(b.Failed_AED,0) Failed_AED,a.outcome_variable,a.Patient_Exclusion_Flag into #No_Aed_2_5 
from #Patient2 a left join #AED2_NO b on a.patient_id=b.patient_id
inner join #base1 c on a.patient_id=c.patient_id and a.valid_index=c.valid_index
---drop table [SHA_Pipeline_All_Target].[Failed_Aed_2_5yr]



IF OBJECT_ID('[$(TargetSchema)].[Failed_Aed_2_5yr]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[Failed_Aed_2_5yr]

select a.* into [$(TargetSchema)].[Failed_Aed_2_5yr] from #Failed_Aed_2_5 a 
 inner join  [$(SrcSchema)].[src_REF_patient] b
on a.patient_id=CONVERT (FLOAT,b.patient_id)
/*
IF OBJECT_ID('[$(TargetSchema)].[IndPat_scores]') IS not NULL
drop table [$(TargetSchema)].[IndPat_scores]

CREATE TABLE [$(TargetSchema)].[IndPat_scores]
(
                [patient_id] [varchar](50) NULL,
                [Index_Date] [varchar](50) NULL,
                [Age] [varchar](50) NULL,
                [Gender] [varchar](50) NULL,
                [CCI_Score] [varchar](50) NULL,
                [Outcome_variable] [varchar](50) NULL,
                [Model Label] [varchar](50) NULL,
                [Class probabilities] [varchar](50) NULL,
                [DRERiskStratification] [varchar](50) NULL
)
                BULK INSERT [$(TargetSchema)].[IndPat_scores]
                FROM '$(outDataPath)\d5_dre_sha_score.csv'
                WITH 
                (
                rowterminator = '\n',
                fieldterminator =',',
                FIRSTROW=2
                );  */

IF OBJECT_ID('tempdb..#2_AEd_Failure_2_5') IS NOT NULL
DROP TABLE #2_AEd_Failure_2_5


select [DRERiskStratification],count (distinct dre.patient_id) PT_CNT into #2_AEd_Failure_2_5 from 
(select * 
 from [$(TargetSchema)].[Failed_Aed_2_5yr] ) main inner join 
[$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where Failed_unique_AED>1 group by [DRERiskStratification]

IF OBJECT_ID('tempdb..#No_AED_Failure_2_5') IS NOT NULL
DROP TABLE #No_AED_Failure_2_5

select [DRERiskStratification],count (distinct dre.patient_id) PT_CNT into #No_AED_Failure_2_5 from 
(select * 
 from #No_Aed_2_5  ) main inner join 
[$(TargetSchema)].[DRE_Scores] dre on convert (float,main.patient_id)=dre.patient_id where 
main.patient_id not in (select distinct patient_id from [$(TargetSchema)].[Failed_Aed_2_5yr] where Failed_unique_AED>1)
 group by [DRERiskStratification]

 
Insert into [$(TargetSchema)].Table12 select 'At least 2.5 year',DRERiskStratification,2,PT_CNT from #2_AEd_Failure_2_5
Insert into [$(TargetSchema)].Table12 select 'At least 2.5 year',DRERiskStratification,0,PT_CNT from #No_AED_Failure_2_5


-----------------------------------Data file ------------------------------------------------

--select a.patient_id,case when  DATEDIFF(dd,m.Valid_Index,LastRxDxServiceDate)+1 >  913 then 1 else 0 end as [>2.5],
--case when b.patient_id is null then 0 else 1 end as [within_2_5_AED_Fail],
--case when a.failed_unique_AED >1 then 1 else 0 end as [2+AED fail in >2.5],
--a.failed_unique_AED  [cnt_failed_AED_<2_5]
-- from SHA2_Synoma_ID_pankaj.[app_dre_int_Cohort_Monotherapy] m inner join SHA2_Synoma_ID_pankaj.[Failed_Aed_2_5yr] a on m.patient_id=a.patient_id
-- left join #Failed_Aed_2_5_Within b on a.patient_id=b.patient_id 
-- where DATEDIFF(dd,m.Valid_Index,m.LastRxDxServiceDate)+1 >  913 and m.valid_index is not null and Lookback_Present=1 and Lookforward_Present=1


--select a.patient_id,case when  DATEDIFF(dd,m.Valid_Index,LastRxDxServiceDate)+1 >  913 then 1 else 0 end as [>2.5],
--case when b.patient_id is null then 0 else 1 end as [within_2_5_AED_Fail],
--case when a.failed_unique_AED >1 then 1 else 0 end as [2+AED fail in >2.5],
--a.failed_unique_AED  [cnt_failed_AED_<2_5],[DRERiskStratification]
-----b.failed_unique_AED [cnt_failed_AED_within2_5]
-- into #TMP2
-- from SHA2_Synoma_ID_pankaj.[app_dre_int_Cohort_Monotherapy] m 
-- left  join SHA2_Synoma_ID_pankaj.[Failed_Aed_2_5yr] a on m.patient_id=a.patient_id
-- left join #Failed_Aed_2_5_Within b on a.patient_id=b.patient_id and b.Failed_unique_AED>1
--  join SHA2_Synoma_ID_pankaj.[DRE_Scores] dre on convert (float,m.patient_id)=dre.patient_id
-- where DATEDIFF(dd,m.Valid_Index,m.LastRxDxServiceDate)+1 >  913 and m.valid_index is not null and Lookback_Present=1 and Lookforward_Present=1