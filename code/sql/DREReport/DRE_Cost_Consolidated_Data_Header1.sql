SET NOCOUNT ON

SELECT 
 '' [Age Group]
,'' [Total Patients - High]
,'' [Total Patients - Not High] 
,'' [Total Cost - High ($)] 
,'' [Total Cost - Not High ($)] 
,'' [Difference (%)] 
,'',''
,'' [Age Group]
,'' [Total Patients - High]
,'' [Total Patients - Low] 
,'' [Total Cost - High ($)] 
,'' [Total Cost - Low ($)] 
,'' [Difference (%)] 
,'',''
,'' [Age Group]
,'' [Total Patients - High Medium]
,'' [Total Patients - Low] 
,'' [Total Cost - High Medium ($)] 
,'' [Total Cost - Low ($)] 
,'' [Difference (%)] 
WHERE 1<>1