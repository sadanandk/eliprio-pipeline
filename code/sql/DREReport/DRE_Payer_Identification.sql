--:setvar TargetSchema SHA2_Synoma_ID_4NOV
--:setvar SrcSchema SHA2_SYNOMA_ID_SRC

SET NOCOUNT ON

------------------------------------
--Patients with Px Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#PX_DRE') IS NOT NULL
DROP TABLE #PX_DRE

--1881

select  distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date ,Outcome_variable into #PX_DRE from (
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date,row_number() over(partition by dre.patient_id order by dre.patient_id,service_date,payer_type) num
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G'))g where num=1


IF OBJECT_ID('tempdb..#PX1') IS NOT NULL
DROP TABLE #PX1

--5131
select distinct D.*
into #PX1
from 
(
select C.* ,Payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,Payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #PX_DRE)
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B
)C
inner join [$(SrcSchema)].[src_dx_claims] PX on C.patient_id = PX.patient_id and PX.service_date = C.service_date
where  Payer_type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#PX_Payer') IS NOT NULL
DROP TABLE #PX_Payer

--6969
select *
into #PX_Payer
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date,Payer_type) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX_DRE
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX1
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1

------------------------------------
--Patients with Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#RX_DRE') IS NOT NULL
DROP TABLE #RX_DRE

--10551
select *
into #RX_DRE
from
(
select * , row_number() over(partition by patient_id order by patient_id,service_date,payer_type) Num
from 
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G') 
and days_supply>0
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#RX1') IS NOT NULL
DROP TABLE #RX1

--150
select distinct D.*
into #RX1
from 
(
select C.* ,payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_Type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #RX_DRE)
and days_supply>0
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B 
)C 
inner join [$(SrcSchema)].[src_rx_claims] RX on C.patient_id = RX.patient_id and RX.service_date = C.service_date
where Payer_Type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#RX_Payer') IS NOT NULL
DROP TABLE #RX_Payer

--10701
select *
into #RX_Payer
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date,payer_type) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX_DRE
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX1
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1
and patient_id not in (select patient_id from #PX_Payer)

------------------------------------
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping') IS NOT NULL
DROP TABLE #Payer_Mapping

--10784
select *
into #Payer_Mapping
from
(
select * from #PX_Payer
union
select * from #RX_Payer where patient_id not in (select patient_id from #PX_Payer )
)A

IF OBJECT_ID('tempdb..#PX_DRE_1') IS NOT NULL
DROP TABLE #PX_DRE_1

--1881
select  distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date ,Outcome_variable into #PX_DRE_1 from (
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date,row_number() over(partition by DRE.patient_id order by DRE.patient_id,service_date,payer_type) num
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and  Payer_type in ('O'))g where num=1



IF OBJECT_ID('tempdb..#PX1_1') IS NOT NULL
DROP TABLE #PX1_1

--5131
select distinct D.*
into #PX1_1
from 
(
select C.* ,Payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,Payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in  ('O')
and dre.patient_id not in (select distinct patient_id from #PX_DRE_1)
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B
)C
inner join [$(SrcSchema)].[src_dx_claims] PX on C.patient_id = PX.patient_id and PX.service_date = C.service_date
where  Payer_type in  ('O')
)D
where Num =1


IF OBJECT_ID('tempdb..#PX_Payer_1') IS NOT NULL
DROP TABLE #PX_Payer_1

--6969
select *
into #PX_Payer_1
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date,payer_type) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX_DRE_1
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX1_1
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1

------------------------------------
--Patients with Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#RX_DRE_1') IS NOT NULL
DROP TABLE #RX_DRE_1

--10551
select *
into #RX_DRE_1
from
(
select * , row_number() over(partition by patient_id order by patient_id,service_date,payer_type) Num
from 
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in  ('O') 
and days_supply>0
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#RX1_1') IS NOT NULL
DROP TABLE #RX1_1

--150
select distinct D.*
into #RX1_1
from 
(
select C.* ,payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_Type in  ('O')
and dre.patient_id not in (select distinct patient_id from #RX_DRE_1)
and days_supply>0
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B 
)C 
inner join [$(SrcSchema)].[src_rx_claims] RX on C.patient_id = RX.patient_id and RX.service_date = C.service_date
where Payer_Type in  ('O')
)D
where Num =1


IF OBJECT_ID('tempdb..#RX_Payer_1') IS NOT NULL
DROP TABLE #RX_Payer_1

--10701
select *
into #RX_Payer_1
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date,payer_type) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX_DRE_1
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX1_1
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1
and patient_id not in (select patient_id from #PX_Payer_1)

------------------------------------
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping_1') IS NOT NULL
DROP TABLE #Payer_Mapping_1

--10784
select *
into #Payer_Mapping_1
from
(
select * from #PX_Payer_1
union
select * from #RX_Payer_1 where patient_id not in (select patient_id from #PX_Payer_1 )
)A
where patient_id not in (select patient_id from #Payer_Mapping)

IF OBJECT_ID('[$(TargetSchema)].[Payer_Identification]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[Payer_Identification]

select * into [$(TargetSchema)].[Payer_Identification]
from #Payer_Mapping
union 
select * from #Payer_Mapping_1
