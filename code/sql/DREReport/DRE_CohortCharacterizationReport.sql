--:setvar SCHEMA SHA_Pipeline_All
SET NOCOUNT ON
UPDATE [$(TargetSchema)].[DRE_CohortCharacterization] 
SET [Variable] = SUBSTRING([Variable], 2, LEN([Variable]) - 2)
WHERE [Variable] LIKE '"%"'

SELECT 
      CAST(ISNULL([Variable],'') AS varchar(40)) AS [Variable]
      ,CAST(ISNULL([Label],'') AS VARCHAR(60)) AS [Label]
      ,CASE WHEN (([% of PatientCount] IS NULL) or ([% of PatientCount]=0))
	   THEN ISNULL(CAST([Value] AS varchar(10)),'')
	   ELSE
	   concat(ISNULL(CAST([Value] AS varchar(10)),''),'(',ISNULL(CAST([% of PatientCount] AS varchar(10)),''),')')
	   END  AS [Statistic (n%)]
  FROM [$(TargetSchema)].[DRE_CohortCharacterization] WITH(NOLOCK)
  ORDER BY ID,[Value] DESC, [Label] ASC