
--:setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_update_sup

SET NOCOUNT ON

-- Patients with Age 65+ at index date

IF OBJECT_ID('tempdb..#Age65Above') IS NOT NULL
DROP TABLE #Age65Above

select C.patient_id,valid_index,aed,aed_days,Outcome_variable = case when Outcome_Variable is null then 'Indeterminate' else Outcome_Variable end
,YOB
into #Age65Above
from [$(TargetSchema)].[app_dre_Cohort] C
inner join [$(SrcSchema)].[SRC_REF_PATIENT] P on C.patient_id = P.patient_id
where Valid_index is not null and Lookback_Present = 1 and Lookforward_present=1 --and Outcome_Variable is not null 
and datepart(year,valid_index)-YOB >=65

-- Patients with Payer Info

IF OBJECT_ID('tempdb..#PX_DRE') IS NOT NULL
DROP TABLE #PX_DRE

select *
into #PX_DRE
from
(
select *,row_number() over(partition by patient_id order by patient_id,Payer_type) Num
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G')
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#PX1') IS NOT NULL
DROP TABLE #PX1

select distinct D.*
into #PX1
from 
(
select C.* ,Payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,Payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #PX_DRE)
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B
)C
inner join [$(SrcSchema)].[src_dx_claims] PX on C.patient_id = PX.patient_id and PX.service_date = C.service_date
where  Payer_type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#PX_Payer') IS NOT NULL
DROP TABLE #PX_Payer

select *
into #PX_Payer
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,A.Outcome_Variable
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX_DRE
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX1
)A
)B
where num = 1

------------------------------------
--Patients with Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#RX_DRE') IS NOT NULL
DROP TABLE #RX_DRE

select *
into #RX_DRE
from
(
select * , row_number() over(partition by patient_id order by patient_id,Payer_Type) Num
from 
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G') 
and days_supply>0
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#RX1') IS NOT NULL
DROP TABLE #RX1

select distinct D.*
into #RX1
from 
(
select C.* ,payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_Type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #RX_DRE)
and days_supply>0
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B 
)C 
inner join [$(SrcSchema)].[src_rx_claims] RX on C.patient_id = RX.patient_id and RX.service_date = C.service_date
where Payer_Type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#RX_Payer') IS NOT NULL
DROP TABLE #RX_Payer

select *
into #RX_Payer
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,A.Outcome_Variable
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX_DRE
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX1
)A
)B
where num = 1
and patient_id not in (select patient_id from #PX_Payer)


------------------------------------
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping') IS NOT NULL
DROP TABLE #Payer_Mapping

select *
into #Payer_Mapping
from
(
select * from #PX_Payer
union
select * from #RX_Payer where patient_id not in (select patient_id from #PX_Payer )
)A

-- Medicare Patients

IF OBJECT_ID('tempdb..#Medicare') IS NOT NULL
DROP TABLE #Medicare

select * 
into #Medicare
from #Payer_Mapping
where Payer_type in ('R','G')

-- Medicaid Patients

IF OBJECT_ID('tempdb..#Medicaid') IS NOT NULL
DROP TABLE #Medicaid

select * 
into #Medicaid
from #Payer_Mapping
where Payer_type in ('M')

-- Medicaid Patients

IF OBJECT_ID('tempdb..#Private') IS NOT NULL
DROP TABLE #Private

select * 
into #Private
from #Payer_Mapping
where Payer_type in ('V')

;with A as
(
select row_number() over(order by patient_id) Rid, patient_id Age65Above from #Age65Above
),

B as
(
select row_number() over(order by patient_id) Rid, patient_id Medicaid from #Medicaid
),

C as
(
select row_number() over(order by patient_id) Rid, patient_id Medicare from #Medicare
),

D as
(
select row_number() over(order by patient_id) Rid, patient_id [Private] from #private
)

select 
 isnull(Age65Above,'') Age65Above
,isnull(Medicaid ,'')	Medicaid
,isnull(Medicare,''	)  Medicare
,isnull([Private],'')	[Private]
from A 
full join B  on A.Rid=B.Rid
full join C  on A.Rid=C.Rid
full join D  on A.Rid=D.Rid



