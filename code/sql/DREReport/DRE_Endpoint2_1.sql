
--:SetVar TargetSchema SHA2_SYNOMA_ID_ENDPT_INT
--:SetVar SrcSchema SHA2_Synoma_ID_SRC


IF OBJECT_ID('tempdb..#PatientCohort') IS NOT NULL
	DROP TABLE #PatientCohort

SELECT *
INTO #PatientCohort
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] a
WHERE lookback_present = 1
	AND lookforward_present = 1
	AND valid_index IS NOT NULL 
	AND Patient_Exclusion_Flag = 0
    AND DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913---452
/* 487297 */

IF OBJECT_ID('tempdb..#PatientCohortFullTimeline1Year') IS NOT NULL
	DROP TABLE #PatientCohortFullTimeline1Year

SELECT *
INTO #PatientCohortFullTimeline1Year
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] a
WHERE patient_id IN (
		SELECT DISTINCT patient_id
		FROM #PatientCohort
		) 

/* Unique AED count on 1 Year Evaluation Period*/
IF OBJECT_ID('tempdb..#AED1YearEval') IS NOT NULL
	DROP TABLE #AED1YearEval

SELECT a.patient_id
	,a.valid_index
	,count(DISTINCT b.AED) UniqueAedCount
INTO #AED1YearEval
FROM #PatientCohort a
LEFT JOIN #PatientCohortFullTimeline1Year b
	ON a.patient_id = b.patient_id
WHERE  (datediff(dd,a.valid_index,b.maxdate) between 366 and 913 )
--and  b.aed_failure_flag=1
GROUP BY a.patient_id,a.valid_index

IF OBJECT_ID('tempdb..#NonDre_Aed') IS NOT NULL
DROP TABLE #NonDre_Aed

select a.patient_id,a.valid_index,isnull(b.UniqueAedCount,0) UniqueAedCount,a.outcome_variable,a.Patient_Exclusion_Flag into #NonDre_Aed
from #PatientCohortFullTimeline1Year a left join #AED1YearEval b on a.patient_id=b.patient_id and  a.valid_index=b.valid_index where  a.valid_index is not null ---and a.patient_id='00000000000146012773'

IF OBJECT_ID('tempdb..#ALL_Aed_1') IS NOT NULL
DROP TABLE #ALL_Aed_1

select a.patient_id,a.valid_index,case when isnull(b.UniqueAedCount,0)=1 then 0 else b.UniqueAedCount end as UniqueAedCount,a.outcome_variable,a.Patient_Exclusion_Flag into #ALL_Aed_1
from #PatientCohortFullTimeline1Year a left join #NonDre_Aed b on a.patient_id=b.patient_id and  a.valid_index=b.valid_index where  a.valid_index is not null ---and a.patient_id='00000000000146012773'



--------------------------------

	IF OBJECT_ID('tempdb..#AED1YearEvalStartificationDRE') IS NOT NULL
	DROP TABLE #AED1YearEvalStartificationDRE

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AED1YearEvalStartificationDRE
FROM (
	SELECT a.*
	FROM #AED1YearEval a
	INNER JOIN [$(SrcSchema)].[src_REF_patient] b
		ON a.patient_id = convert(FLOAT, b.patient_id)
	) main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE UniqueAedCount >= 4
GROUP BY [DRERiskStratification]

--/* Non-DRE Stratification */
--IF OBJECT_ID('tempdb..#AED1YearEvalStartificationNonDRE') IS NOT NULL
--	DROP TABLE #AED1YearEvalStartificationNonDRE

--SELECT [DRERiskStratification]
--	,count(DISTINCT dre.patient_id) patient_count
--INTO #AED1YearEvalStartificationNonDRE
--FROM (
--	SELECT a.*
--	FROM #AED1YearEval a
--	INNER JOIN [$(SrcSchema)].[src_REF_patient] b
--		ON a.patient_id = convert(FLOAT, b.patient_id)
--	) main
--INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
--	ON convert(FLOAT, main.patient_id) = dre.patient_id
--WHERE UniqueAedCount < 4
--GROUP BY [DRERiskStratification]

/* Non-DRE Stratification */
IF OBJECT_ID('tempdb..#AED1YearEvalStartificationNonDRE') IS NOT NULL
	DROP TABLE #AED1YearEvalStartificationNonDRE

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AED1YearEvalStartificationNonDRE
FROM #ALL_Aed_1 main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE UniqueAedCount < 4
GROUP BY [DRERiskStratification]



----------------------------2.5 year-------------------------------


/* Patients with lookforward=1, lookbackward=1 and Patient_Exclusion_Flag = 0  and 2.5 year evaluation period*/
IF OBJECT_ID('tempdb..#PatientCohort2') IS NOT NULL
	DROP TABLE #PatientCohort2

SELECT *
INTO #PatientCohort2
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] a
WHERE lookback_present = 1
	AND lookforward_present = 1
	AND valid_index IS NOT NULL 
	AND Patient_Exclusion_Flag = 0
    AND DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913
			
/* 487297 */

IF OBJECT_ID('tempdb..#PatientCohortFullTimeline2Year') IS NOT NULL
	DROP TABLE #PatientCohortFullTimeline2Year

SELECT *
INTO #PatientCohortFullTimeline2Year
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
WHERE patient_id IN (
		SELECT DISTINCT patient_id
		FROM #PatientCohort2
		) 


/* Unique AED count within 2.5 Year Evaluation Period*/
IF OBJECT_ID('tempdb..#AEDYearEval_2_5') IS NOT NULL
	DROP TABLE #AEDYearEval_2_5

SELECT a.patient_id
	,a.valid_index
	,count(DISTINCT b.AED) [UniqueAedCount] 
INTO #AEDYearEval_2_5
FROM #PatientCohort2 a
LEFT JOIN #PatientCohortFullTimeline2Year b
	ON a.patient_id = b.patient_id
WHERE b.maxdate >= a.valid_index
--(datediff(dd,a.valid_index,b.maxdate) between 0 and 913 )
--and  b.aed_failure_flag=1
GROUP BY a.patient_id
	,a.valid_index

	--------------------------2.5 Year Evalution period startification distribution ----------------------
/* DRE Stratification*/
IF OBJECT_ID('tempdb..#AEDYearEvalStartificationDRE') IS NOT NULL
	DROP TABLE #AEDYearEvalStartificationDRE

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AEDYearEvalStartificationDRE
FROM (
	SELECT a.*
	FROM  #AEDYearEval_2_5 a
	INNER JOIN [$(SrcSchema)].[src_REF_patient] b
		ON a.patient_id = convert(FLOAT, b.patient_id)
	) main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE [UniqueAedCount] >= 4
GROUP BY [DRERiskStratification]

/* Non-DRE Stratification */
IF OBJECT_ID('tempdb..#AEDYearEvalStartificationNonDRE') IS NOT NULL
	DROP TABLE #AEDYearEvalStartificationNonDRE

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AEDYearEvalStartificationNonDRE
FROM (
	SELECT a.*
	FROM  #AEDYearEval_2_5 a
	INNER JOIN [$(SrcSchema)].[src_REF_patient] b
		ON a.patient_id = convert(FLOAT, b.patient_id)
	) main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE [UniqueAedCount] < 4
GROUP BY [DRERiskStratification]



IF OBJECT_ID('[$(TargetSchema)].[DRE_EndPoint2.1]') IS NOT NULL
	DROP TABLE [$(TargetSchema)].[DRE_EndPoint2.1]

CREATE TABLE [$(TargetSchema)].[DRE_EndPoint2.1] (
	Evaluation_Window VARCHAR(50)
	,[DRERiskStratification] VARCHAR(50)
	,Unique_AED_Count VARCHAR(10)
	,Patient_Count BIGINT
	);



	
INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1]
SELECT 'Within 1 year'
	,[DRERiskStratification]
	,'<4'
	,patient_count
FROM #AED1YearEvalStartificationNonDRE

INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1]
SELECT 'Within 1 year'
	,[DRERiskStratification]
	,'>=4'
	,patient_count
FROM #AED1YearEvalStartificationDRE

INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1]
SELECT
	 'Within 2.5 year'
	,[DRERiskStratification]
	,'<4'
	,patient_count
FROM #AEDYearEvalStartificationNonDRE

INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1]
SELECT 
	'Within 2.5 year'
	,[DRERiskStratification]
	,'>=4'
	,patient_count
FROM #AEDYearEvalStartificationDRE

-------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID('tempdb..#PatientCohort_WO') IS NOT NULL
	DROP TABLE #PatientCohort_WO

SELECT *
INTO #PatientCohort_WO
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] a
WHERE lookback_present = 1
	AND lookforward_present = 1
	AND valid_index IS NOT NULL 
	---AND Patient_Exclusion_Flag = 0
    AND DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913---1112
/* 487297 */

IF OBJECT_ID('tempdb..#PatientCohort_FullTimeline1Year_WO') IS NOT NULL
	DROP TABLE #PatientCohort_FullTimeline1Year_WO

SELECT *
INTO #PatientCohort_FullTimeline1Year_WO
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] a
WHERE patient_id IN (
		SELECT DISTINCT patient_id
		FROM #PatientCohort_WO
		) ---3716 

/* Unique AED count on 1 Year Evaluation Period*/
IF OBJECT_ID('tempdb..#AED1YearEval_WO') IS NOT NULL
	DROP TABLE #AED1YearEval_WO

SELECT a.patient_id
	,a.valid_index
	,count(DISTINCT b.AED) UniqueAedCount
INTO #AED1YearEval_WO
FROM #PatientCohort_WO a
LEFT JOIN #PatientCohort_FullTimeline1Year_WO b
	ON a.patient_id = b.patient_id
WHERE  (datediff(dd,a.valid_index,b.maxdate) between 366 and 913 )
--and  b.aed_failure_flag=1
GROUP BY a.patient_id,a.valid_index--480


--IF OBJECT_ID('tempdb..#NonDre_Aed_WO') IS NOT NULL
--DROP TABLE #NonDre_Aed_WO

--select a.patient_id,a.valid_index,case when isnull(b.UniqueAedCount,0)=1 then 0 else b.UniqueAedCount end as UniqueAedCount,a.outcome_variable,a.Patient_Exclusion_Flag into #NonDre_Aed_WO
--from #PatientCohort_FullTimeline1Year_WO a left join #AED1YearEval_WO b on a.patient_id=b.patient_id and  a.valid_index=b.valid_index where  a.valid_index is not null ---1112


IF OBJECT_ID('tempdb..#NonDre_Aed_WO') IS NOT NULL
DROP TABLE #NonDre_Aed_WO

select a.patient_id,a.valid_index,isnull(b.UniqueAedCount,0) UniqueAedCount,a.outcome_variable,a.Patient_Exclusion_Flag into #NonDre_Aed_WO
from #PatientCohort_FullTimeline1Year_WO a left join #AED1YearEval_WO b on a.patient_id=b.patient_id and  a.valid_index=b.valid_index where  a.valid_index is not null ---and a.patient_id='00000000000146012773'

IF OBJECT_ID('tempdb..#ALL_Aed_1_WO') IS NOT NULL
DROP TABLE #ALL_Aed_1_WO

select a.patient_id,a.valid_index,case when isnull(b.UniqueAedCount,0)=1 then 0 else b.UniqueAedCount end as UniqueAedCount,a.outcome_variable,a.Patient_Exclusion_Flag into #ALL_Aed_1_WO
from #PatientCohort_FullTimeline1Year_WO a left join #NonDre_Aed_WO b on a.patient_id=b.patient_id and  a.valid_index=b.valid_index where  a.valid_index is not null ---and a.patient_id='00000000000146012773'



--------------------------------

	IF OBJECT_ID('tempdb..#AED1YearEval_StartificationDRE_WO') IS NOT NULL
	DROP TABLE #AED1YearEval_StartificationDRE_WO

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AED1YearEval_StartificationDRE_WO
FROM (
	SELECT a.*
	FROM #AED1YearEval_WO a
	INNER JOIN [$(SrcSchema)].[src_REF_patient] b
		ON a.patient_id = convert(FLOAT, b.patient_id)
	) main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE UniqueAedCount >= 4
GROUP BY [DRERiskStratification]

/* Non-DRE Stratification */
IF OBJECT_ID('tempdb..#AED1YearEval_StartificationNonDRE_WO') IS NOT NULL
	DROP TABLE #AED1YearEval_StartificationNonDRE_WO

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AED1YearEval_StartificationNonDRE_WO
FROM #ALL_Aed_1_WO main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE UniqueAedCount < 4
GROUP BY [DRERiskStratification]


----------------------------2.5 year-------------------------------


/* Patients with lookforward=1, lookbackward=1 and Patient_Exclusion_Flag = 0  and 2.5 year evaluation period*/
IF OBJECT_ID('tempdb..#PatientCohort2_WO') IS NOT NULL
	DROP TABLE #PatientCohort2_WO

SELECT *
INTO #PatientCohort2_WO
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] a
WHERE lookback_present = 1
	AND lookforward_present = 1
	AND valid_index IS NOT NULL 
	---AND Patient_Exclusion_Flag = 0
    AND DATEDIFF(dd,Valid_Index,LastRxDxServiceDate)+1 >  913
			
/* 487297 */

IF OBJECT_ID('tempdb..#PatientCohort_FullTimeline2Year_WO') IS NOT NULL
	DROP TABLE #PatientCohort_FullTimeline2Year_WO

SELECT *
INTO #PatientCohort_FullTimeline2Year_WO
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
WHERE patient_id IN (
		SELECT DISTINCT patient_id
		FROM #PatientCohort2_WO
		) 


/* Unique AED count within 2.5 Year Evaluation Period*/
IF OBJECT_ID('tempdb..#AEDYearEval_2_5_WO') IS NOT NULL
	DROP TABLE #AEDYearEval_2_5_WO

SELECT a.patient_id
	,a.valid_index
	,count(DISTINCT b.AED) [UniqueAedCount] 
INTO #AEDYearEval_2_5_WO
FROM #PatientCohort2_WO a
LEFT JOIN #PatientCohort_FullTimeline2Year_WO b
	ON a.patient_id = b.patient_id
WHERE b.maxdate >= a.valid_index
--(datediff(dd,a.valid_index,b.maxdate) between 0 and 913 )
--and  b.aed_failure_flag=1
GROUP BY a.patient_id
	,a.valid_index

	--------------------------2.5 Year Evalution period startification distribution ----------------------
/* DRE Stratification*/
IF OBJECT_ID('tempdb..#AEDYearEvalStartificationDRE_WO') IS NOT NULL
	DROP TABLE #AEDYearEvalStartificationDRE_WO

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AEDYearEvalStartificationDRE_WO
FROM (
	SELECT a.*
	FROM  #AEDYearEval_2_5_WO a
	INNER JOIN [$(SrcSchema)].[src_REF_patient] b
		ON a.patient_id = convert(FLOAT, b.patient_id)
	) main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE [UniqueAedCount] >= 4
GROUP BY [DRERiskStratification]

/* Non-DRE Stratification */
IF OBJECT_ID('tempdb..#AEDYearEvalStartificationNonDRE_WO') IS NOT NULL
	DROP TABLE #AEDYearEvalStartificationNonDRE_WO

SELECT [DRERiskStratification]
	,count(DISTINCT dre.patient_id) patient_count
INTO #AEDYearEvalStartificationNonDRE_WO
FROM (
	SELECT a.*
	FROM  #AEDYearEval_2_5_WO a
	INNER JOIN [$(SrcSchema)].[src_REF_patient] b
		ON a.patient_id = convert(FLOAT, b.patient_id)
	) main
INNER JOIN [$(TargetSchema)].[DRE_Scores] DRE
	ON convert(FLOAT, main.patient_id) = dre.patient_id
WHERE [UniqueAedCount] < 4
GROUP BY [DRERiskStratification]

-------------------------------------------------------------------------------------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_EndPoint2.1_With_INT]') IS NOT NULL
	DROP TABLE [$(TargetSchema)].[DRE_EndPoint2.1_With_INT]

CREATE TABLE [$(TargetSchema)].[DRE_EndPoint2.1_With_INT] (
	Evaluation_Window VARCHAR(50)
	,[DRERiskStratification] VARCHAR(50)
	,Unique_AED_Count VARCHAR(10)
	,Patient_Count BIGINT
	);



	
INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1_With_INT]
SELECT 'Within 1 year'
	,[DRERiskStratification]
	,'<4'
	,patient_count
FROM #AED1YearEval_StartificationNonDRE_WO

INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1_With_INT]
SELECT 'Within 1 year'
	,[DRERiskStratification]
	,'>=4'
	,patient_count
FROM #AED1YearEval_StartificationDRE_WO

INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1_With_INT]
SELECT
	 'Within 2.5 year'
	,[DRERiskStratification]
	,'<4'
	,patient_count
FROM #AEDYearEvalStartificationNonDRE_WO

INSERT INTO [$(TargetSchema)].[DRE_EndPoint2.1_With_INT]
SELECT 
	'Within 2.5 year'
	,[DRERiskStratification]
	,'>=4'
	,patient_count
FROM #AEDYearEvalStartificationDRE_WO