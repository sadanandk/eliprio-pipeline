--:SetVar TargetSchema SHA2_Synoma_ID_Endpt_Int
--:SetVar SrcSchema SHA2_Synoma_ID_SRC
SET NOCOUNT ON

IF OBJECT_ID('tempdb..#Tot') IS NOT NULL
	DROP TABLE #Tot

SELECT sum(patient_count) AS [tot_cnt]
	,[Evaluation_Window] AS [Evaluation]
	,[DRERiskStratification] AS [RiskStratification]
INTO #Tot
FROM [$(TargetSchema)].Table11
GROUP BY [Evaluation_Window]
	,[DRERiskStratification]

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
	DROP TABLE #TEMP

SELECT ID
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([2 AED failure1] AS VARCHAR(30)) AS [2 AED failure]
	,CAST([No AED failure1] AS VARCHAR(30)) AS [No AED failure]
	,CAST([2 AED failure2] AS VARCHAR(30)) AS [2 AED failure1]
	,CAST([No AED failure2] AS VARCHAR(30)) AS [No AED failure1]
INTO #TEMP
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,a.patient_count AS [2 AED failure1]
		,b.patient_count AS [No AED failure1]
		,c.patient_count AS [2 AED failure2]
		,d.patient_count AS [No AED failure2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

DECLARE @Sensityvity1year DECIMAL(5, 2);
DECLARE @Sensityvity2year DECIMAL(5, 2);
DECLARE @Specivity1year DECIMAL(5, 2);
DECLARE @Specivity2year DECIMAL(5, 2);
DECLARE @Total2AEDFailure DECIMAL(5, 2);
DECLARE @2AEDFailureHigh DECIMAL(5, 2);
DECLARE @2AEDFailureMedium DECIMAL(5, 2);
DECLARE @2AEDFailureLow DECIMAL(5, 2);
DECLARE @Total2AEDFailure1 DECIMAL(5, 2);
DECLARE @2AEDFailureHigh1 DECIMAL(5, 2);
DECLARE @2AEDFailureMedium1 DECIMAL(5, 2);
DECLARE @2AEDFailureLow1 DECIMAL(5, 2);
DECLARE @DOR1year DECIMAL(5, 2);
DECLARE @DOR2year DECIMAL(5, 2);
DECLARE @SE_SEN1y DECIMAL(5, 2);
DECLARE @SE_DOR1y DECIMAL(5, 2);
DECLARE @SE_SEN2y DECIMAL(5, 2);
DECLARE @SE_DOR2y DECIMAL(5, 2);
DECLARE @SE_SenUpper1y DECIMAL(5, 2);
DECLARE @SE_SenLower1y DECIMAL(5, 2);
DECLARE @SE_SenUpper2y DECIMAL(5, 2);
DECLARE @SE_SenLower2y DECIMAL(5, 2);


SELECT @2AEDFailureHigh = [2 AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @2AEDFailureMedium = [2 AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @2AEDFailureLow = [2 AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @Total2AEDFailure = @2AEDFailureHigh + @2AEDFailureMedium + @2AEDFailureLow

SELECT @Sensityvity1year = (((@2AEDFailureHigh + @2AEDFailureMedium) * 1.0) / @Total2AEDFailure) * 100

SELECT @2AEDFailureHigh1 = [2 AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @2AEDFailureMedium1 = [2 AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @2AEDFailureLow1 = [2 AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @Total2AEDFailure1 = @2AEDFailureHigh1 + @2AEDFailureMedium1 + @2AEDFailureLow1

SELECT @Sensityvity2year = (((@2AEDFailureHigh1 + @2AEDFailureMedium1) * 1.0) / @Total2AEDFailure1) * 100

select @SE_SEN1y=sqrt((@Sensityvity1year*(100-@Sensityvity1year))/(@Total2AEDFailure))
select @SE_SEN2y=sqrt((@Sensityvity2year*(100-@Sensityvity2year))/(@Total2AEDFailure1))
select @SE_SenUpper1y=@Sensityvity1year+(@SE_SEN1y*1.96)
select @SE_SenLower1y=@Sensityvity1year-(@SE_SEN1y*1.96)
select @SE_SenUpper2y=@Sensityvity2year+(@SE_SEN2y*1.96)
select @SE_SenLower2y=@Sensityvity2year-(@SE_SEN2y*1.96)

INSERT INTO #TEMP
SELECT 4 AS [id]
	,CAST('Sensitivity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,concat (CAST(round (@Sensityvity1year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SenLower1y,1) as decimal (5,1)),'% ub=',cast (round (@SE_SenUpper1y ,1) as decimal (5,1)),'%)') AS [2 AED failure]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure]
	,concat (CAST(round (@Sensityvity2year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SenLower2y,1) as decimal (5,1)),'% ub=',cast (round (@SE_SenUpper2y ,1) as decimal (5,1)),'%)') AS [2 AED failur1e]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure1]	

/* Specivity Details*/
DECLARE @TotalNoAEDFailure INT;
DECLARE @NoAEDFailureHigh INT;
DECLARE @NoAEDFailureMedium INT;
DECLARE @NoAEDFailureLow INT;
DECLARE @TotalNoAEDFailure1 INT;
DECLARE @NoAEDFailureHigh1 INT;
DECLARE @NoAEDFailureMedium1 INT;
DECLARE @NoAEDFailureLow1 INT;
DECLARE @SE_SPE1y DECIMAL(5, 2);
DECLARE @SE_SPE2y DECIMAL(5, 2);
DECLARE @SE_SpeUpper1y DECIMAL(5, 2);
DECLARE @SE_SpeLower1y DECIMAL(5, 2);
DECLARE @SE_SpeUpper2y DECIMAL(5, 2);
DECLARE @SE_SpeLower2y DECIMAL(5, 2);

SELECT @NoAEDFailureHigh = [No AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NoAEDFailureMedium = [No AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NoAEDFailureLow = [No AED failure]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNoAEDFailure = @NoAEDFailureHigh + @NoAEDFailureMedium + @NoAEDFailureLow

SELECT @Specivity1year = (((@NoAEDFailureLow ) * 1.0) / @TotalNoAEDFailure) * 100

SELECT @NoAEDFailureHigh1 = [No AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NoAEDFailureMedium1 = [No AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NoAEDFailureLow1 = [No AED failure1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNoAEDFailure1 = @NoAEDFailureHigh1 + @NoAEDFailureMedium1 + @NoAEDFailureLow1

SELECT @Specivity2year = (((@NoAEDFailureLow1 ) * 1.0) / @TotalNoAEDFailure1) * 100


select @SE_SPE1y=sqrt((@Specivity1year*(100-@Specivity1year))/(@TotalNoAEDFailure))
select @SE_SPE2y=sqrt((@Specivity2year*(100-@Specivity2year))/(@TotalNoAEDFailure1))
select @SE_SpeUpper1y=@Specivity1year+(@SE_SPE1y*1.96)
select @SE_SpeLower1y=@Specivity1year-(@SE_SPE1y*1.96)
select @SE_SpeUpper2y=@Specivity2year+(@SE_SPE2y*1.96)
select @SE_SpeLower2y=@Specivity2year-(@SE_SPE2y*1.96)
       
INSERT INTO #TEMP
SELECT 5 AS [id]
	,CAST('Specificity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,Concat (CAST(round (@Specivity1year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SpeLower1y,1) as decimal (5,1)),'% ub=',cast (round (@SE_SpeUpper1y ,1) as decimal (5,1)),'%)') AS [2 AED failure]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure]	
	,Concat (CAST(round (@Specivity2year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SpeLower2y,1) as decimal (5,1)),'% ub=',cast (round (@SE_SpeUpper2y ,1) as decimal (5,1)),'%)') AS [2 AED failur1e]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure1]	

---------------------------------------Diagnosis odd ratio------------------------
select @DOR1year=(@Specivity1year*@Sensityvity1year)/((100-@Sensityvity1year)*(100-@Specivity1year))
select @DOR2year=(@Specivity2year*@Sensityvity2year)/((100-@Sensityvity2year)*(100-@Specivity2year))


DECLARE @SE_DORUpper1y DECIMAL(5, 2);
DECLARE @SE_DORLower1y DECIMAL(5, 2);
DECLARE @SE_DORUpper2y DECIMAL(5, 2);
DECLARE @SE_DORLower2y DECIMAL(5, 2);

 select @SE_DOR1y=sqrt( 
 (1/(@2AEDFailureHigh + @2AEDFailureMedium))---TP
 +(1/(@NoAEDFailureHigh + @NoAEDFailureMedium)) ---FP
 +(1/@2AEDFailureLow)----FN
 +(1/@NoAEDFailureLow))---TN

  select @SE_DOR2y=sqrt( 
 (1/(@2AEDFailureHigh1 + @2AEDFailureMedium1))---TP
 +(1/(@NoAEDFailureHigh1 + @NoAEDFailureMedium1)) ---FP
 +(1/@2AEDFailureLow1)----FN
 +(1/@NoAEDFailureLow1))---TN


 select @SE_DORUpper1y=exp(log (@DOR1year)+(1.96*@SE_DOR1y))
 select @SE_DORUpper2y=exp(log (@DOR2year)+(1.96*@SE_DOR2y))
 select @SE_DORLower1y=exp(log (@DOR1year)-(1.96*@SE_DOR1y))
 select @SE_DORLower2y=exp(log (@DOR2year)-(1.96*@SE_DOR2y))


INSERT INTO #TEMP
SELECT 6 AS [id]
	,CAST('Diagnostic_Odd_Ratio' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,Concat (CAST(round (@DOR1year,1) AS decimal(5,1)),' (lb=',cast (round (@SE_DORLower1y,1) as decimal (5,1)),' ub=',cast (round (@SE_DORUpper1y ,1) as decimal (5,1)),')') AS [2 AED failure]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure]	
	,Concat (CAST(round (@DOR2year,1) AS decimal(5,1)),' (lb=',cast (round (@SE_DORLower2y,1) as decimal (5,1)),' ub=',cast (round (@SE_DORUpper2y ,1) as decimal (5,1)),')')  AS [2 AED failur1e]
	,CAST(NULL AS VARCHAR(30)) AS [No AED failure1]	
	
-----------------------------------------------	-------------

IF OBJECT_ID('tempdb..#TEMP1') IS NOT NULL
	DROP TABLE #TEMP1
	
SELECT ID
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([2 AED failure1] AS VARCHAR(30)) AS [2 AED failure]
	,CAST([No AED failure1] AS VARCHAR(30)) AS [No AED failure]
	,CAST([2 AED failure2] AS VARCHAR(30)) AS [2 AED failure1]
	,CAST([No AED failure2] AS VARCHAR(30)) AS [No AED failure1]
INTO #TEMP1
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,CONCAT (
			a.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((a.patient_count * 1.0 / a.tot_cnt) * 100)))
			,'%)'
			) AS [2 AED failure1]
		,CONCAT (
			b.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((b.patient_count * 1.0 / b.tot_cnt) * 100)))
			,'%)'
			) AS [No AED failure1]
		,CONCAT (
			c.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((c.patient_count * 1.0 / c.tot_cnt) * 100)))
			,'%)'
			) AS [2 AED failure2]
		,CONCAT (
			d.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((d.patient_count * 1.0 / d.tot_cnt) * 100)))
			,'%)'
			) AS [No AED failure2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = T.Evaluation
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = t.Evaluation
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = t.Evaluation
		WHERE AED_Failure = 2
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].Table11 a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = t.Evaluation
		WHERE AED_Failure = 0
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

UNION

SELECT ID AS [ID]
	,[DRE Risk Stratification]
	,[2 AED failure] AS [2 AED failure]
	,[No AED failure] AS [No AED failure]
	,[2 AED failure1] AS [2 AED failure1]
	,[No AED failure1] AS [No AED failure1]
FROM #TEMP
WHERE ID IN (
		4
		,5,6
		)

SELECT [DRE Risk Stratification]
	,[2 AED failure] AS [2 AED failure]
	,isnull([No AED failure], '') AS [No AED failure]
	,[2 AED failure1] AS [2 AED failure1]
	,isnull([No AED failure1], '') AS [No AED failure1]
FROM #Temp1
ORDER BY [ID]