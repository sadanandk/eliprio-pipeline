SELECT 
patient_id,
Index_Date,
Age,
Gender,
CCI_Score,
Outcome_variable,
[Model Label],
[Class probabilities],
[DRERiskStratification]
FROM [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] WITH(NOLOCK)
WHERE 1<>1
