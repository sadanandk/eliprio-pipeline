SET NOCOUNT ON
--IF OBJECT_ID('[$(TargetSchema)].[PipelineMetricReport]','U') IS NOT NULL
--DROP TABLE [$(TargetSchema)].[PipelineMetricReport]

--IF('$(RunDRECohort)'='Y' And '$(RunTDSCohort)'='Y')
--Begin
--SELECT CAST([ID] AS DECIMAL(5, 1)) AS [ID]
--	,ISNULL([Criteria], '') AS [Criteria]
--	,CASE 
--		WHEN ([Meets criteria Patient count n (%)] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Meets criteria Patient count n (%)], '')
--		END AS [Meets criteria Patient count n (%)]
--	,CASE 
--		WHEN ([Do not meet criteria Patient count n (%)] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Do not meet criteria Patient count n (%)], '')
--		END AS [Do not meet criteria Patient count n (%)]
--	,CASE 
--		WHEN ([Meets criteria Patient count n (%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Meets criteria Patient count n (%) with SHA], '')
--		END AS [Meets criteria Patient count n (%) with SHA]
--	,CASE 
--		WHEN ([Do not meet criteria Patient count n (%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Do not meet criteria Patient count n (%) with SHA], '')
--		END AS [Do not meet criteria Patient count n (%) with SHA]
--INTO [$(TargetSchema)].[PipelineMetricReport]
--FROM [$(TargetSchema)].[PHCPipelineMetricReport] WITH (NOLOCK)

--UNION

--SELECT CAST([ID] AS DECIMAL(5, 1)) AS [ID]
--	,ISNULL([Criteria], '') AS [Criteria]
--	,CASE 
--		WHEN ([Meets criteria Patient count n(%)] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Meets criteria Patient count n(%)], '')
--		END AS [Meets criteria Patient count n(%)]
--	,CASE 
--		WHEN ([Do not meet criteria Patient count n(%)] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Do not meet criteria Patient count n(%)], '')
--		END AS [Do not meet criteria Patient count n(%)]
--	,CASE 
--		WHEN ([Meets criteria Patient count n(%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Meets criteria Patient count n(%) with SHA], '')
--		END AS [Meets criteria Patient count n(%) with SHA]
--	,CASE 
--		WHEN ([Do not meet criteria Patient count n(%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Do not meet criteria Patient count n(%) with SHA], '')
--		END AS [Do not meet criteria Patient count n(%) with SHA]
--FROM [$(TargetSchema)].[TDS_PHCPipelineMetricReport] WITH (NOLOCK)
--WHERE ID >= 4
--ORDER BY ID
--End

--IF('$(RunDRECohort)'='Y' And '$(RunTDSCohort)'='N')
--Begin
--	SELECT CAST([ID] AS DECIMAL(5, 1)) AS [ID]
--		,ISNULL([Criteria], '') AS [Criteria]
--		,CASE 
--			WHEN ([Meets criteria Patient count n (%)] = '0 (0)')
--				THEN '0'
--			ELSE ISNULL([Meets criteria Patient count n (%)], '')
--			END AS [Meets criteria Patient count n (%)]
--		,CASE 
--			WHEN ([Do not meet criteria Patient count n (%)] = '0 (0)')
--				THEN '0'
--			ELSE ISNULL([Do not meet criteria Patient count n (%)], '')
--			END AS [Do not meet criteria Patient count n (%)]
--	,CASE 
--		WHEN ([Meets criteria Patient count n (%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Meets criteria Patient count n (%) with SHA], '')
--		END AS [Meets criteria Patient count n (%) with SHA]
--	,CASE 
--		WHEN ([Do not meet criteria Patient count n (%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Do not meet criteria Patient count n (%) with SHA], '')
--		END AS [Do not meet criteria Patient count n (%) with SHA]
--	INTO [$(TargetSchema)].[PipelineMetricReport]
--	FROM [$(TargetSchema)].[PHCPipelineMetricReport] WITH (NOLOCK)
--	ORDER BY ID
--End

--IF('$(RunDRECohort)'='N' And '$(RunTDSCohort)'='Y')
--Begin
--	SELECT CAST([ID] AS DECIMAL(5, 1)) AS [ID]
--		,ISNULL([Criteria], '') AS [Criteria]
--		,CASE 
--			WHEN ([Meets criteria Patient count n(%)] = '0 (0)')
--				THEN '0'
--			ELSE ISNULL([Meets criteria Patient count n(%)], '')
--			END AS [Meets criteria Patient count n (%)]
--		,CASE 
--			WHEN ([Do not meet criteria Patient count n(%)] = '0 (0)')
--				THEN '0'
--			ELSE ISNULL([Do not meet criteria Patient count n(%)], '')
--			END AS [Do not meet criteria Patient count n(%)]
--	,CASE 
--		WHEN ([Meets criteria Patient count n(%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Meets criteria Patient count n(%) with SHA], '')
--		END AS [Meets criteria Patient count n (%) with SHA]
--	,CASE 
--		WHEN ([Do not meet criteria Patient count n(%) with SHA] = '0 (0)')
--			THEN '0'
--		ELSE ISNULL([Do not meet criteria Patient count n(%) with SHA], '')
--		END AS [Do not meet criteria Patient count n(%) with SHA]
--	INTO [$(TargetSchema)].[PipelineMetricReport]
--	FROM [$(TargetSchema)].[TDS_PHCPipelineMetricReport] WITH (NOLOCK)
--	WHERE ID >= 4
--	ORDER BY ID
--End



SELECT 
	[Criteria]
	,[Meets criteria Patient count n (%)]
	,[Do not meet criteria Patient count n (%)]
	,[Meets criteria Patient count n (%) with SHA]
	,[Do not meet criteria Patient count n (%) with SHA]
FROM [$(TargetSchema)].[PHCPipelineMetricReport]  WITH(NOLOCK)