--:setvar TargetSchema SHA2_Synoma_ID_ENdpt_Int
declare @Total varchar(20); 
declare @sql varchar(max); 
Select @Total = sum (patient_count) from [$(TargetSchema)].[DRE_EndPoint2.1] where Evaluation_Window='Within 1 year'
declare @Total1 varchar(20); 
declare @sql1 varchar(max); 
Select @Total1 = sum (patient_count) from [$(TargetSchema)].[DRE_EndPoint2.1] where Evaluation_Window='Within 2.5 year'

declare @Total2 varchar(20); 
declare @sql2 varchar(max); 
Select @Total2 = sum (patient_count) from [$(TargetSchema)].[DRE_EndPoint2.1_With_Int] where Evaluation_Window='Within 1 year'

declare @Total3 varchar(20); 
declare @sql3 varchar(max); 
Select @Total3 = sum (patient_count) from [$(TargetSchema)].[DRE_EndPoint2.1_With_Int] where Evaluation_Window='Within 2.5 year'

--sum( from [$(TargetSchema)].[DRE_EndPoint2.1_With_int] 

Set @sql='
SELECT ''''
	,'''' [1.0-2.5 Years]
	,'''' [n='+@Total + ']
	,'''' [At least 2.5 year] 
	,'''' [n='+@Total1 +']
	,'''' [1.0-2.5 Years]
	,'''' [n='+@Total2 + ']
	,'''' [At least 2.5 year] 
	,'''' [n='+@Total3 +']

WHERE 1 <> 1 '
Exec(@sql)
