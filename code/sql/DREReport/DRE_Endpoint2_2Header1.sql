--:setvar TargetSchema SHA2_Synoma_ID_Endpt_Int

declare @Total varchar(20); 
declare @sql varchar(max); 
Select @Total = sum (patient_count) from [$(TargetSchema)].TABLE11 where Evaluation_Window='Within 1 year'

declare @Total1 varchar(20); 
declare @sql1 varchar(max); 
Select @Total1 = sum (patient_count) from [$(TargetSchema)].TABLE11 where Evaluation_Window='Within 2.5 year'

--sum( from [$(TargetSchema)].[DRE_EndPoint2.1_With_int] 

Set @sql='
SELECT ''''
	,'''' [Within 1 Year]
	,'''' [n='+@Total + ']
	,'''' [Within 2.5 Year] 
	,'''' [n='+@Total1 +']
WHERE 1 <> 1 '
Exec(@sql)

