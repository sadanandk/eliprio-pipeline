

--:SetVar TargetSchema SHA2_SYNOMA_ID_SEF
--:SetVar SrcSchema SHA2_Synoma_ID_SRC


SET NOCOUNT ON

IF OBJECT_ID('tempdb..#Tot') IS NOT NULL
	DROP TABLE #Tot

select sum (patient_count) as tot_cnt,
Evaluation_Window Evaluation,
DRERiskStratification RiskStratification 
into #Tot 
from [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]
group by Evaluation_Window,
DRERiskStratification

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
	DROP TABLE #TEMP

SELECT id
	,CAST(DRERiskStratification AS VARCHAR(50)) AS [DRE Risk Stratification]
	,CAST([DRE1] AS VARCHAR(50)) AS [DRE]
	,CAST([NonDRE1] AS VARCHAR(50)) AS [Non DRE]
	,CAST([DRE2] AS VARCHAR(50)) AS [DRE1]
	,CAST([NonDRE2] AS VARCHAR(50)) AS [Non DRE1]
INTO #TEMP
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,a.patient_count AS [DRE1]
		,b.patient_count AS [NonDRE1]
		,c.patient_count AS [DRE2]
		,d.patient_count AS [NonDRE2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]a
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]a
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]a
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]a
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

DECLARE @Sensityvity1year DECIMAL(5, 2);
DECLARE @Sensityvity2year DECIMAL(5, 2);
DECLARE @Specivity1year DECIMAL(5, 2);
DECLARE @Specivity2year DECIMAL(5, 2);
DECLARE @DOR1year DECIMAL(5, 2);
DECLARE @DOR2year DECIMAL(5, 2);
DECLARE @TotalDRE  DECIMAL(5, 2);
DECLARE @DREHigh  DECIMAL(5, 2);
DECLARE @DREMedium  DECIMAL(5, 2);
DECLARE @DRELow  DECIMAL(5, 2);
DECLARE @TotalDRE1  DECIMAL(5, 2);
DECLARE @DREHigh1  DECIMAL(5, 2);
DECLARE @DREMedium1  DECIMAL(5, 2);
DECLARE @DRELow1  DECIMAL(5, 2);
DECLARE @SE_SEN1y DECIMAL(5, 2);
DECLARE @SE_DOR1y DECIMAL(5, 2);
DECLARE @SE_SEN2y DECIMAL(5, 2);
DECLARE @SE_DOR2y DECIMAL(5, 2);
DECLARE @SE_SenUpper1y DECIMAL(5, 2);
DECLARE @SE_SenLower1y DECIMAL(5, 2);
DECLARE @SE_SenUpper2y DECIMAL(5, 2);
DECLARE @SE_SenLower2y DECIMAL(5, 2);

SELECT @DREHigh = [DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @DREMedium = [DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @DRELow = [DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalDRE = @DREHigh + @DREMedium + @DRELow

SELECT @Sensityvity1year = (((@DREHigh + @DREMedium) * 1.0) / @TotalDRE) * 100

SELECT @DREHigh1 = [DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @DREMedium1 = [DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @DRELow1 = [DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalDRE1 = @DREHigh1 + @DREMedium1 + @DRELow1

SELECT @Sensityvity2year = (((@DREHigh1 + @DREMedium1) * 1.0) / @TotalDRE1) * 100

select @SE_SEN1y=sqrt((@Sensityvity1year*(100-@Sensityvity1year))/(@TotalDRE))
select @SE_SEN2y=sqrt((@Sensityvity2year*(100-@Sensityvity2year))/(@TotalDRE1))
select @SE_SenUpper1y=@Sensityvity1year+(@SE_SEN1y*1.96)
select @SE_SenLower1y=@Sensityvity1year-(@SE_SEN1y*1.96)
select @SE_SenUpper2y=@Sensityvity2year+(@SE_SEN2y*1.96)
select @SE_SenLower2y=@Sensityvity2year-(@SE_SEN2y*1.96)

INSERT INTO #TEMP
SELECT 4 AS [id]
	,CAST('Sensitivity' AS VARCHAR(50)) AS [DRE Risk Stratification]
	,concat (CAST(round (@Sensityvity1year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SenLower1y,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SenUpper1y ,1) as Decimal (5,1)),'%)') AS [DRE]
	,CAST(NULL AS VARCHAR(50)) AS [Non DRE]
	,concat (CAST(round (@Sensityvity2year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SenLower2y,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SenUpper2y ,1) as Decimal (5,1)),'%)') AS [DRE1]
	,CAST(NULL AS VARCHAR(50)) AS [Non DRE1]

/* Specivity Details*/
DECLARE @TotalNonDRE INT;
DECLARE @NonDREHigh INT;
DECLARE @NonDREMedium INT;
DECLARE @NonDRELow INT;
DECLARE @TotalNonDRE1 INT;
DECLARE @NonDREHigh1 INT;
DECLARE @NonDREMedium1 INT;
DECLARE @NonDRELow1 INT;
DECLARE @SE_SPE1y DECIMAL(5, 2);
DECLARE @SE_SPE2y DECIMAL(5, 2);
DECLARE @SE_SpeUpper1y DECIMAL(5, 2);
DECLARE @SE_SpeLower1y DECIMAL(5, 2);
DECLARE @SE_SpeUpper2y DECIMAL(5, 2);
DECLARE @SE_SpeLower2y DECIMAL(5, 2);


SELECT @NonDREHigh = [Non DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NonDREMedium = [Non DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NonDRELow = [Non DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNonDRE = @NonDREHigh + @NonDREMedium + @NonDRELow

SELECT @Specivity1year = (((@NonDRELow) * 1.0) / @TotalNonDRE) * 100

SELECT @NonDREHigh1 = [Non DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NonDREMedium1 = [Non DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NonDRELow1 = [Non DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNonDRE1 = @NonDREHigh1 + @NonDREMedium1 + @NonDRELow1

SELECT @Specivity2year = (((@NonDRELow1) * 1.0) / @TotalNonDRE1) * 100

select @SE_SPE1y=sqrt((@Specivity1year*(100-@Specivity1year))/(@TotalNonDRE))
select @SE_SPE2y=sqrt((@Specivity2year*(100-@Specivity2year))/(@TotalNonDRE1))
select @SE_SpeUpper1y=@Specivity1year+(@SE_SPE1y*1.96)
select @SE_SpeLower1y=@Specivity1year-(@SE_SPE1y*1.96)
select @SE_SpeUpper2y=@Specivity2year+(@SE_SPE2y*1.96)
select @SE_SpeLower2y=@Specivity2year-(@SE_SPE2y*1.96)
       
INSERT INTO #TEMP
SELECT 5 AS [id]
	,CAST('Specificity' AS VARCHAR(50)) AS [DRE Risk Stratification]
	,Concat (CAST(round (@Specivity1year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SpeLower1y,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SpeUpper1y ,2) as Decimal (5,1)),'%)') AS [DRE]
	,CAST(NULL AS VARCHAR(50)) AS [Non DRE]
	,Concat (CAST(round (@Specivity2year,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SpeLower2y,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SpeUpper2y ,2) as Decimal (5,1)),'%)') AS [DRE1]
	,CAST(NULL AS VARCHAR(50)) AS [Non DRE1] 

---------------------------------------Diagnosis odd ratio------------------------
select @DOR1year=(@Specivity1year*@Sensityvity1year)/((100-@Sensityvity1year)*(100-@Specivity1year))
select @DOR2year=(@Specivity2year*@Sensityvity2year)/((100-@Sensityvity2year)*(100-@Specivity2year))


DECLARE @SE_DORUpper1y DECIMAL(5, 2);
DECLARE @SE_DORLower1y DECIMAL(5, 2);
DECLARE @SE_DORUpper2y DECIMAL(5, 2);
DECLARE @SE_DORLower2y DECIMAL(5, 2);

 select @SE_DOR1y=sqrt( 
 (1/(@DREHigh + @DREMedium))---TP
 +(1/(@NonDREHigh + @NonDREMedium)) ---FP
 +(1/@DRELow)----FN
 +(1/@NonDRELow))---TN

  select @SE_DOR2y=sqrt( 
 (1/(@DREHigh1 + @DREMedium1))---TP
 +(1/(@NonDREHigh1 + @NonDREMedium1)) ---FP
 +(1/@DRELow1)----FN
 +(1/@NonDRELow1))---TN

 select @SE_DORUpper1y=exp(log (@DOR1year)+(1.96*@SE_DOR1y))
 select @SE_DORUpper2y=exp(log (@DOR2year)+(1.96*@SE_DOR2y))
 select @SE_DORLower1y=exp(log (@DOR1year)-(1.96*@SE_DOR1y))
 select @SE_DORLower2y=exp(log (@DOR2year)-(1.96*@SE_DOR2y))


INSERT INTO #TEMP
SELECT 6 AS [id]
	,CAST('Diagnostic_Odd_Ratio' AS VARCHAR(50)) AS [DRE Risk Stratification]
	,Concat (CAST(round (@DOR1year,1) AS  Decimal (5,1)),' (lb=',cast (round (@SE_DORLower1y,1) as Decimal (5,1)),' ub=',cast (round (@SE_DORUpper1y ,1) as Decimal (5,1)),')') AS [DRE]
	,CAST(NULL AS VARCHAR(50)) AS [Non DRE]
	,Concat (CAST(round (@DOR2year,1) AS  Decimal (5,1)),' (lb=',cast (round (@SE_DORLower2y,1) as Decimal (5,1)),' ub=',cast (round (@SE_DORUpper2y ,1) as Decimal (5,1)),')')  AS [DRE1]
	,CAST(NULL AS VARCHAR(50)) AS [Non DRE1]

IF OBJECT_ID('[$(TargetSchema)].Within_Data') IS NOT NULL
	DROP TABLE [$(TargetSchema)].Within_Data

SELECT [id]
	,CAST(DRERiskStratification AS VARCHAR(50)) AS [DRE Risk Stratification]
	,CAST([DRE1] AS VARCHAR(50)) AS [DRE]
	,CAST([NonDRE1] AS VARCHAR(50)) AS [Non DRE]
	,CAST([DRE2] AS VARCHAR(50)) AS [DRE1]
	,CAST([NonDRE2] AS VARCHAR(50)) AS [Non DRE1]
INTO [$(TargetSchema)].Within_Data
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,CONCAT (
			a.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((a.patient_count * 1.0 / a.tot_cnt) * 100)))
			,'%)'
			) AS [DRE1]
		,CONCAT (
			b.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((b.patient_count * 1.0 / b.tot_cnt) * 100)))
			,'%)'
			) AS [NonDRE1]
		,CONCAT (
			c.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((c.patient_count * 1.0 / c.tot_cnt) * 100)))
			,'%)'
			) AS [DRE2]
		,CONCAT (
			d.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((d.patient_count * 1.0 / d.tot_cnt) * 100)))
			,'%)'
			) AS [NonDRE2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int] a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1_With_Int]a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

UNION

SELECT id AS [id]
	,[DRE Risk Stratification]
	,[DRE] AS [DRE]
	,[Non DRE] AS [Non DRE]
	,[DRE1] AS [DRE1]
	,[Non DRE1] AS [Non DRE1]
FROM #TEMP
WHERE ID IN (
		4
		,5,6
		)

--SELECT [DRE Risk Stratification]
--	,[DRE] AS [DRE]
--	,isnull([Non DRE], '') AS [Non DRE]
--	,[DRE1] AS [DRE]
--	,isnull([Non DRE1], '') AS [Non DRE]
--FROM [$(TargetSchema)].Within_Data
--ORDER BY [ID]

------------------------------------------------------------------------------------------



--:SetVar TargetSchema [$(TargetSchema)]
--:SetVar SrcSchema SHA2_Synoma_ID_SRC


SET NOCOUNT ON

IF OBJECT_ID('tempdb..#Tot_WO') IS NOT NULL
	DROP TABLE #Tot_WO

select sum (patient_count) as tot_cnt,
Evaluation_Window Evaluation,
DRERiskStratification RiskStratification 
into #Tot_WO 
from [$(TargetSchema)].[DRE_EndPoint2.1] 
group by Evaluation_Window,
DRERiskStratification

IF OBJECT_ID('tempdb..#TEMP_WO') IS NOT NULL
	DROP TABLE #TEMP_WO

SELECT id
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([DRE1] AS VARCHAR(30)) AS [DRE]
	,CAST([NonDRE1] AS VARCHAR(30)) AS [Non DRE]
	,CAST([DRE2] AS VARCHAR(30)) AS [DRE1]
	,CAST([NonDRE2] AS VARCHAR(30)) AS [Non DRE1]
INTO #TEMP_WO
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,a.patient_count AS [DRE1]
		,b.patient_count AS [NonDRE1]
		,c.patient_count AS [DRE2]
		,d.patient_count AS [NonDRE2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

DECLARE @Sensityvity1year_WO DECIMAL(5, 2);
DECLARE @Sensityvity2year_WO DECIMAL(5, 2);
DECLARE @Specivity1year_WO DECIMAL(5, 2);
DECLARE @Specivity2year_WO DECIMAL(5, 2);
DECLARE @DOR1year_WO DECIMAL(5, 2);
DECLARE @DOR2year_WO DECIMAL(5, 2);
DECLARE @TotalDRE_WO  DECIMAL(5, 2);
DECLARE @DREHigh_WO  DECIMAL(5, 2);
DECLARE @DREMedium_WO DECIMAL(5, 2);
DECLARE @DRELow_WO DECIMAL(5, 2);
DECLARE @TotalDRE_WO1  DECIMAL(5, 2);
DECLARE @DREHigh_WO1  DECIMAL(5, 2);
DECLARE @DREMedium1_WO  DECIMAL(5, 2);
DECLARE @DRELow_WO1  DECIMAL(5, 2);
DECLARE @SE_SEN1y_WO DECIMAL(5, 2);
DECLARE @SE_DOR1y_WO DECIMAL(5, 2);
DECLARE @SE_SEN2y_WO DECIMAL(5, 2);
DECLARE @SE_DOR2y_WO DECIMAL(5, 2);
DECLARE @SE_SenUpper1y_WO DECIMAL(5, 2);
DECLARE @SE_SenLower1y_WO DECIMAL(5, 2);
DECLARE @SE_SenUpper2y_WO DECIMAL(5, 2);
DECLARE @SE_SenLower2y_WO DECIMAL(5, 2);

SELECT @DREHigh_WO = [DRE]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'High'

SELECT @DREMedium_WO= [DRE]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @DRELow_WO = [DRE]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalDRE_WO = @DREHigh_WO + @DREMedium_WO+ @DRELow_WO

SELECT @Sensityvity1year_WO = (((@DREHigh_WO + @DREMedium_WO) * 1.0) / @TotalDRE_WO) * 100

SELECT @DREHigh_WO1 = [DRE1]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'High'

SELECT @DREMedium1_WO = [DRE1]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @DRELow_WO1 = [DRE1]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalDRE_WO1 = @DREHigh_WO1 + @DREMedium1_WO + @DRELow_WO1

SELECT @Sensityvity2year_WO = (((@DREHigh_WO1 + @DREMedium1_WO) * 1.0) / @TotalDRE_WO1) * 100

select @SE_SEN1y_WO=sqrt((@Sensityvity1year_WO*(100-@Sensityvity1year_WO))/(@TotalDRE_WO))
select  @SE_SEN2y_WO=sqrt((@Sensityvity2year_WO*(100-@Sensityvity2year_WO))/(@TotalDRE_WO1))
select @SE_SenUpper1y_WO=@Sensityvity1year_WO+(@SE_SEN1y_WO*1.96)
select @SE_SenLower1y_WO=@Sensityvity1year_WO-(@SE_SEN1y_WO*1.96)
select @SE_SenUpper2y_WO=@Sensityvity2year_WO+( @SE_SEN2y_WO*1.96)
select @SE_SenLower2y_WO=@Sensityvity2year_WO-( @SE_SEN2y_WO*1.96)

INSERT INTO #TEMP_WO
SELECT 4 AS [id]
	,CAST('Sensitivity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,concat (CAST(round (@Sensityvity1year_WO,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SenLower1y_WO,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SenUpper1y_WO,1) as Decimal (5,1)),'%)') AS [DRE]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE]
	,concat (CAST(round (@Sensityvity2year_WO,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SenLower2y_WO,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SenUpper2y_WO,1) as Decimal (5,1)),'%)') AS [DRE1]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE1]

/* Specivity Details*/
DECLARE @TotalNonDRE_WO INT;
DECLARE @NonDREHigh_WO INT;
DECLARE @NonDREMedium_WO INT;
DECLARE @NonDRELow_WO INT;
DECLARE @TotalNonDRE_WO1 INT;
DECLARE @NonDREHigh_WO1 INT;
DECLARE @NonDREMedium_WO1 INT;
DECLARE @NonDRELow_WO1 INT;
DECLARE @SE_SPE1y_WO  DECIMAL(5, 2);
DECLARE @SE_SPE2y_WO DECIMAL(5, 2);
DECLARE @SE_SpeUpper1y_WO DECIMAL(5, 2);
DECLARE @SE_SpeLower1y_WO DECIMAL(5, 2);
DECLARE @SE_SpeUpper2y_WO DECIMAL(5, 2);
DECLARE @SE_SpeLower2y_WO DECIMAL(5, 2);


SELECT @NonDREHigh_WO = [Non DRE]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'High'

SELECT @NonDREMedium_WO = [Non DRE]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NonDRELow_WO = [Non DRE]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNonDRE_WO = @NonDREHigh_WO + @NonDREMedium_WO + @NonDRELow_WO

SELECT @Specivity1year_WO = (((@NonDRELow_WO) * 1.0) / @TotalNonDRE_WO) * 100

SELECT @NonDREHigh_WO1 = [Non DRE1]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'High'

SELECT @NonDREMedium_WO1 = [Non DRE1]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Medium'

SELECT @NonDRELow_WO1 = [Non DRE1]
FROM #TEMP_WO
WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNonDRE_WO1 = @NonDREHigh_WO1 + @NonDREMedium_WO1 + @NonDRELow_WO1

SELECT @Specivity2year_WO = (((@NonDRELow_WO1) * 1.0) / @TotalNonDRE_WO1) * 100

select @SE_SPE1y_WO =sqrt((@Specivity1year_WO*(100-@Specivity1year_WO))/(@TotalNonDRE_WO))
select @SE_SPE2y_WO=sqrt((@Specivity2year_WO*(100-@Specivity2year_WO))/(@TotalNonDRE_WO1))
select @SE_SpeUpper1y_WO=@Specivity1year_WO+(@SE_SPE1y_WO *1.96)
select @SE_SpeLower1y_WO=@Specivity1year_WO-(@SE_SPE1y_WO *1.96)
select @SE_SpeUpper2y_WO=@Specivity2year_WO+(@SE_SPE2y_WO*1.96)
select @SE_SpeLower2y_WO=@Specivity2year_WO-(@SE_SPE2y_WO*1.96)
       
INSERT INTO #TEMP_WO
SELECT 5 AS [id]
	,CAST('Specificity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,Concat (CAST(round (@Specivity1year_WO,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SpeLower1y_WO,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SpeUpper1y_WO,2) as Decimal (5,1)),'%)') AS [DRE]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE]
	,Concat (CAST(round (@Specivity2year_WO,1) AS Decimal (5,1)),'% (lb=',cast (round (@SE_SpeLower2y_WO,1) as Decimal (5,1)),'% ub=',cast (round (@SE_SpeUpper2y_WO,2) as Decimal (5,1)),'%)') AS [DRE1]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE1] 

---------------------------------------Diagnosis odd ratio------------------------
select @DOR1year_WO=(@Specivity1year_WO*@Sensityvity1year_WO)/((100-@Sensityvity1year_WO)*(100-@Specivity1year_WO))
select @DOR2year_WO=(@Specivity2year_WO*@Sensityvity2year_WO)/((100-@Sensityvity2year_WO)*(100-@Specivity2year_WO))


DECLARE @SE_DORUpper1y_WO DECIMAL(5, 2);
DECLARE @SE_DORLower1y_WO DECIMAL(5, 2);
DECLARE @SE_DORUpper2y_WO DECIMAL(5, 2);
DECLARE @SE_DORLower2y_WO DECIMAL(5, 2);

 select @SE_DOR1y_WO=sqrt( 
 (1/(@DREHigh_WO + @DREMedium_WO))---TP
 +(1/(@NonDREHigh_WO + @NonDREMedium_WO)) ---FP
 +(1/@DRELow_WO)----FN
 +(1/@NonDRELow_WO))---TN

  select @SE_DOR2y_WO=sqrt( 
 (1/(@DREHigh_WO1 + @DREMedium1_WO))---TP
 +(1/(@NonDREHigh_WO1 + @NonDREMedium_WO1)) ---FP
 +(1/@DRELow_WO1)----FN
 +(1/@NonDRELow_WO1))---TN

 select @SE_DORUpper1y_WO=exp(log (@DOR1year_WO)+(1.96*@SE_DOR1y_WO))
 select @SE_DORUpper2y_WO=exp(log (@DOR2year_WO)+(1.96*@SE_DOR2y_WO))
 select @SE_DORLower1y_WO=exp(log (@DOR1year_WO)-(1.96*@SE_DOR1y_WO))
 select @SE_DORLower2y_WO=exp(log (@DOR2year_WO)-(1.96*@SE_DOR2y_WO))


INSERT INTO #TEMP_WO
SELECT 6 AS [id]
	,CAST('Diagnostic_Odd_Ratio' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,Concat (CAST(round (@DOR1year_WO,1) AS  Decimal (5,1)),' (lb=',cast (round (@SE_DORLower1y_WO,1) as Decimal (5,1)),' ub=',cast (round (@SE_DORUpper1y_WO,1) as Decimal (5,1)),')') AS [DRE]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE]
	,Concat (CAST(round (@DOR2year_WO,1) AS  Decimal (5,1)),' (lb=',cast (round (@SE_DORLower2y_WO,1) as Decimal (5,1)),' ub=',cast (round (@SE_DORUpper2y_WO,1) as Decimal (5,1)),')')  AS [DRE1]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE1]

IF OBJECT_ID('tempdb..#Temp1_WO') IS NOT NULL
	DROP TABLE #Temp1_WO

SELECT [id]
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([DRE1] AS VARCHAR(30)) AS [DRE]
	,CAST([NonDRE1] AS VARCHAR(30)) AS [Non DRE]
	,CAST([DRE2] AS VARCHAR(30)) AS [DRE1]
	,CAST([NonDRE2] AS VARCHAR(30)) AS [Non DRE1]
INTO #Temp1_WO
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,CONCAT (
			a.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((a.patient_count * 1.0 / a.tot_cnt) * 100)))
			,'%)'
			) AS [DRE1]
		,CONCAT (
			b.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((b.patient_count * 1.0 / b.tot_cnt) * 100)))
			,'%)'
			) AS [NonDRE1]
		,CONCAT (
			c.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((c.patient_count * 1.0 / c.tot_cnt) * 100)))
			,'%)'
			) AS [DRE2]
		,CONCAT (
			d.patient_count
			,' ('
			,convert(VARCHAR(10), convert(NUMERIC(5, 1), ((d.patient_count * 1.0 / d.tot_cnt) * 100)))
			,'%)'
			) AS [NonDRE2]
	FROM (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN Tot_WO t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN #Tot_WO t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN #Tot_WO t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT *
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN #Tot_WO t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

UNION

SELECT id AS [id]
	,[DRE Risk Stratification]
	,[DRE] AS [DRE]
	,[Non DRE] AS [Non DRE]
	,[DRE1] AS [DRE1]
	,[Non DRE1] AS [Non DRE1]
FROM #TEMP_WO
WHERE ID IN (
		4
		,5,6
		)

--SELECT [DRE Risk Stratification]
--	,[DRE] AS [DRE]
--	,isnull([Non DRE], '') AS [Non DRE]
--	,[DRE1] AS [DRE]
--	,isnull([Non DRE1], '') AS [Non DRE]
--FROM #Temp1_WO
--ORDER BY [ID]


SELECT WO.[DRE Risk Stratification], WO.DRE, ISNULL(WO.[Non DRE],'') AS [NON DRE], WO.DRE1, ISNULL(WO.[Non DRE1],'') AS [NON DRE1], 
WI.DRE, ISNULL(WI.[Non DRE],'') AS [NON DRE], WI.DRE1, ISNULL(WI.[Non DRE1], '') AS [NON DRE1]
 FROM #Temp1_WO WO 
FULL OUTER JOIN [$(TargetSchema)].WithiN_Data WI ON WO.ID=WI.ID