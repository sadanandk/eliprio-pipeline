--:SetVar TargetSchema SHA2_SYNOMA_ID_INTEGRATION


Select IsNULL(A.VARIABLE,'') as Variable, IsNULL(A.Statistic,'') Statistic,isNULL(b.Train,'') Train,isNULL(A.SHA,'') SHA,
CASE WHEN	C.[Value] IS NULL AND A.Statistic IS NOT NULL THEN '0' ELSE 
CASE WHEN ((c.[% of PatientCount] IS NULL) OR (c.[% of PatientCount]=0)) THEN ISNULL(REPLACE(c.[Value],'.00',''),'')
	ELSE
		CONCAT(ISNULL(REPLACE(c.[Value],'.00',''),''),' (',ISNULL(CAST(c.[% of PatientCount] AS VARCHAR(100)),''),')') END
		END  as [PHS w/ Indeterminates],
CASE WHEN	d.[Value] IS NULL AND A.Statistic IS NOT NULL THEN '0' ELSE 
CASE WHEN ((d.[% of PatientCount] IS NULL) OR (d.[% of PatientCount]=0))
	 THEN ISNULL(REPLACE(d.[Value],'.00',''),'')
	ELSE
		CONCAT(ISNULL(REPLACE(d.[Value],'.00',''),''),' (',ISNULL(CAST(d.[% of PatientCount] AS VARCHAR(100)),''),')') END
		END  as [PHS w/o Indeterminates],
CASE WHEN	e.[Value] IS NULL AND A.Statistic IS NOT NULL THEN '0' ELSE 
CASE WHEN ((e.[% of PatientCount] IS NULL) OR (e.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(e.[Value],'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(e.[Value],'.00',''),''),' (',ISNULL(CAST(e.[% of PatientCount] AS VARCHAR(100)),''),')') END
				  END  as [Only PHS Indeterminates]
				  from [$(TargetSchema)].[dre_cohortcharacterization_SHA_1Y] a
inner join [$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_1Y] b
on isnull(a.Statistic,'') = isnull(b.Statistic,'') and isnull(a.variable,'')= isnull(b.variable,'') AND	A.ID = B.ID
left join [$(TargetSchema)].[DRE_CohortCharacterization] c
on isnull(a.Statistic,'') = isnull(C.Label,'') and isnull(a.variable,'')= isnull(c.variable,'') AND	A.ID = c.ID
left join [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant] d
on isnull(c.Label,'') = isnull(d.Label,'') and isnull(c.variable,'')= isnull(d.variable,'') AND	A.ID = d.ID
left join [$(TargetSchema)].[DRE_CohortCharacterization_Only_inditerminant] e
on isnull(c.Label,'') = isnull(e.Label,'') and isnull(c.variable,'')= isnull(e.variable,'')  AND	A.ID = e.ID
where 1<>1
order by a.[id] 
