--:setvar TargetSchema Synoma_Target
--:setvar SrcSchema SHA_Synoma_ID_SRC

SET NOCOUNT ON


;with HighTotalPatients as
(
select 'Overall Group' [Age Group], [Weighted N] [Total Pt High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_2_5Y]
where Type=''
union
select '18-44' [Age Group], [Weighted N] [Total Pt High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type=''
union
select '45-64' [Age Group], [Weighted N] [Total Pt High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type=''
union
select '65+' [Age Group], [Weighted N] [Total Pt High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type=''
),

HighTotalCost as
(
select 'Overall Group' [Age Group], [Weighted N] [TC - High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_2_5Y]
where Type='Total direct cost '
union
select '18-44' [Age Group], [Weighted N] [TC - High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type='Total direct cost '
union
select '45-64' [Age Group], [Weighted N] [TC - High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type='Total direct cost '
union
select '65+' [Age Group], [Weighted N] [TC - High]
from [$(TargetSchema)].[DRE_High_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type='Total direct cost '
),

NotHighTotalPatients as
(
select 'Overall Group' [Age Group], [Weighted N] [Total Pt Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_2_5Y]
where Type=''
union
select '18-44' [Age Group], [Weighted N] [Total Pt Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type=''
union
select '45-64' [Age Group], [Weighted N] [Total Pt Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type=''
union
select '65+' [Age Group], [Weighted N] [Total Pt Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type=''
),

NotHighTotalCost as
(
select 'Overall Group' [Age Group], [Weighted N] [TC - Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_2_5Y]
where Type='Total direct cost '
union
select '18-44' [Age Group], [Weighted N] [TC - Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type='Total direct cost '
union
select '45-64' [Age Group], [Weighted N] [TC - Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type='Total direct cost '
union
select '65+' [Age Group], [Weighted N] [TC - Not High]
from [$(TargetSchema)].[DRE_NonHigh_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type='Total direct cost '
),

LowTotalPatients as
(
select 'Overall Group' [Age Group], [Weighted N] [Total Pt Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_2_5Y]
where Type=''
union
select '18-44' [Age Group], [Weighted N] [Total Pt Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type=''
union
select '45-64' [Age Group], [Weighted N] [Total Pt Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type=''
union
select '65+' [Age Group], [Weighted N] [Total Pt Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type=''
),

LowTotalCost as
(
select 'Overall Group' [Age Group], [Weighted N] [TC - Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_2_5Y]
where Type='Total direct cost '
union
select '18-44' [Age Group], [Weighted N] [TC - Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type='Total direct cost '
union
select '45-64' [Age Group], [Weighted N] [TC - Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type='Total direct cost '
union
select '65+' [Age Group], [Weighted N] [TC - Low]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type='Total direct cost '
),

HighMedTotalPatients as
(
select 'Overall Group' [Age Group], [Weighted N] [Total Pt High_Med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_2_5Y]
where Type=''
union
select '18-44' [Age Group], [Weighted N] [Total Pt High_Med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type=''
union
select '45-64' [Age Group], [Weighted N] [Total Pt High_Med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type=''
union
select '65+' [Age Group], [Weighted N] [Total Pt High_Med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type=''
),

HighMedTotalCost as
(
select 'Overall Group' [Age Group], [Weighted N] [TC - High_med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_2_5Y]
where Type='Total direct cost '
union
select '18-44' [Age Group], [Weighted N] [TC - High_med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_Age18_44_2_5Y]
where Type='Total direct cost '
union
select '45-64' [Age Group], [Weighted N] [TC - High_med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_Age45_64_2_5Y]
where Type='Total direct cost '
union
select '65+' [Age Group], [Weighted N] [TC - High_med]
from [$(TargetSchema)].[DRE_High_Medium_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
where Type='Total direct cost '
)



select 
A.[Age Group]
,format(isnull(A.[Total Pt High],0),'#0.0') [Total Pt High]
,format(isnull(C.[Total Pt Not High],0),'#0.0') [Total Pt Not High]
,format(isnull(B.[TC - High],0),'#0.0')  [TC - High]
,format(isnull(D.[TC - Not High],0),'#0.0') [TC - Not High]
,format((isnull(B.[TC - High],0) - isnull(D.[TC - Not High],0)) / nullif(isnull(D.[TC - Not High],0),0)*100.0,'#0.0') [Difference (%)]
,'','',A.[Age Group]
,format(isnull(A.[Total Pt High],0),'#0.0') [Total Pt High]
,format(isnull(E.[Total Pt Low],0),'#0.0') [Total Pt Low]
,format(isnull(B.[TC - High],0),'#0.0')  [TC - High]
,format(isnull(F.[TC - Low],0),'#0.0') [TC - Low]
,format((isnull(B.[TC - High],0) - isnull(F.[TC - Low],0)) / nullif(isnull(F.[TC - Low],0),0)*100.0,'#0.0') [Difference (%)]
,'','',A.[Age Group]
,format(isnull(G.[Total Pt High_med],0),'#0.0') [Total Pt High_med]
,format(isnull(E.[Total Pt Low],0),'#0.0') [Total Pt Low]
,format(isnull(H.[TC - High_med],0),'#0.0')  [TC - High_med]
,format(isnull(F.[TC - Low],0),'#0.0') [TC - Low]
,format((isnull(H.[TC - High_med],0) - isnull(F.[TC - Low],0)) / nullif(isnull(F.[TC - Low],0),0)*100.0,'#0.0') [Difference (%)]

from HighTotalPatients A
inner join HighTotalCost B	  on A.[Age Group] = B.[Age Group]
inner join NotHighTotalPatients C on A.[Age Group] = C.[Age Group]
inner join NotHighTotalCost D on A.[Age Group] = D.[Age Group]
inner join LowTotalPatients  E on A.[Age Group] = E.[Age Group] 
inner join LowTotalCost F	  on A.[Age Group] = F.[Age Group]
inner join HighmedTotalPatients  G on A.[Age Group] = G.[Age Group] 
inner join HighmedTotalCost H	  on A.[Age Group] = H.[Age Group]

order by 1

