

--:SetVar TargetSchema SHA2_Synoma_ID_TGT
--:SetVar SrcSchema SHA_Synoma_ID_SRC
--Print '[$(TargetSchema)]'

Set Nocount On
--------------------- PHC Start -------------------------
IF OBJECT_ID('tempdb..#Days_Analysis') IS NOT NULL
DROP TABLE #Days_Analysis

SELECT A.Patient_id,A.Valid_index
		,[Age]
		,[First_Encounter]
		,[Last_Encounter]
		,YEAR(A.Valid_index)								AS [Index_Year]
		,DATEDIFF(dd,A.Valid_index,Last_Encounter)		AS [Index_to_Last_Encounter]
		,DATEDIFF(dd,First_Encounter,Last_Encounter)	AS [First_Encounter_to_Last_Encounter]
INTO #Days_Analysis
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A
JOIN [$(TargetSchema)].[app_int_Patient_Profile] B
ON A.patient_id = B.patient_id
--JOIN [$(TargetSchema)].app_dre_int_Cohort_Monotherapy M
--ON A.patient_id = M.patient_id 
--AND datediff(dd,M.valid_index,lastrxdxservicedate)+1>913


--------------------- PHC End -------------------------


--------------------- PHC 2.5 Yr Start-------------------------
IF OBJECT_ID('tempdb..#Days_Analysis_2_5') IS NOT NULL
DROP TABLE #Days_Analysis_2_5

SELECT A.Patient_id,A.Valid_index
		,[Age]
		,[First_Encounter]
		,[Last_Encounter]
		,YEAR(A.Valid_index)								AS [Index_Year]
		,DATEDIFF(dd,A.Valid_index,Last_Encounter)		AS [Index_to_Last_Encounter]
		,DATEDIFF(dd,First_Encounter,Last_Encounter)	AS [First_Encounter_to_Last_Encounter]
INTO #Days_Analysis_2_5
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A
JOIN [$(TargetSchema)].[app_int_Patient_Profile] B
ON A.patient_id = B.patient_id
JOIN [$(TargetSchema)].app_dre_int_Cohort_Monotherapy M
ON A.patient_id = M.patient_id 
AND datediff(dd,M.valid_index,lastrxdxservicedate)+1>913

----------------------------------------------------------------

IF OBJECT_ID('[$(TargetSchema)].DRE_Index_Event') IS NOT NULL
DROP TABLE [$(TargetSchema)].DRE_Index_Event


--Select 'Index Event'
SELECT Index_Year,COUNT(1) AS Patient_Count
into [$(TargetSchema)].DRE_Index_Event
FROM #Days_Analysis
GROUP BY  Index_year --ORDER BY 1

IF OBJECT_ID('[$(TargetSchema)].DRE_Index_Event_SHA') IS NOT NULL
DROP TABLE [$(TargetSchema)].DRE_Index_Event_SHA

Select '2009' Index_year,	'53352' Patient_Count 
into [$(TargetSchema)].DRE_Index_Event_SHA
union All
Select '2010',	'162510'
union All
Select '2011',	'220841'
union All
Select '2012',	'177457'
union All
Select '2013',	'184243'
union All
Select '2014',	'159555'
union All
Select '2015',	'148990'
union All
Select '2016',	'158449'
union All
Select '2017',	'122334'
union All
Select '2018',	'14255'


--Select char(13)

IF OBJECT_ID('[$(TargetSchema)].DRE_Index_Event_2_5') IS NOT NULL
DROP TABLE [$(TargetSchema)].DRE_Index_Event_2_5


--Select 'Index Event 2.5 Yr'
SELECT Index_Year,COUNT(1) AS Patient_Count 
into [$(TargetSchema)].DRE_Index_Event_2_5
FROM #Days_Analysis_2_5
GROUP BY  Index_year --ORDER BY 1


IF OBJECT_ID('[$(TargetSchema)].DRE_Index_Event_SHA_2_5') IS NOT NULL
DROP TABLE [$(TargetSchema)].DRE_Index_Event_SHA_2_5

Select '2009' Index_year,	'50427' Patient_Count 
into [$(TargetSchema)].DRE_Index_Event_SHA_2_5
Union ALL
Select '2010',	'153388'
Union ALL
Select '2011',	'208316'
Union ALL
Select '2012',	'165783'
Union ALL
Select '2013',	'170282'
Union ALL
Select '2014',	'145561'
Union ALL
Select '2015',	'132368'
Union ALL
Select '2016',	'89098'


IF OBJECT_ID('[$(TargetSchema)].DRE_Index_Event_SHA_Train') IS NOT NULL
DROP TABLE [$(TargetSchema)].DRE_Index_Event_SHA_Train

Select '2009' Index_year,	'12248' Patient_Count 
into [$(TargetSchema)].DRE_Index_Event_SHA_Train
Union ALL
Select '2010',	'41790'
Union ALL
Select '2011',	'67309'
Union ALL
Select '2012',	'50234'
Union ALL
Select '2013',	'52596'
Union ALL
Select '2014',	'43202'
Union ALL
Select '2015',	'42124'
Union ALL
Select '2016',	'49526'
Union ALL
Select '2017',	'18198'

IF OBJECT_ID('[$(TargetSchema)].DRE_rpt_Index_Event') IS NOT NULL
DROP TABLE [$(TargetSchema)].DRE_rpt_Index_Event

Select A.Index_Year, A.Patient_Count as PHS_Patient_Count_1_Yr, 
isnull(B.Patient_Count,0) as PHS_Patient_Count_2_5_Yr,
isnull(C.Patient_Count,0) as SHA_Patient_Count_1_Yr,
isnull(D.Patient_Count,0) as SHA_Patient_Count_2_5_Yr,
isnull(E.Patient_Count,0) as SHA_Train_Patient_Count
into [$(TargetSchema)].DRE_rpt_Index_Event
From [$(TargetSchema)].DRE_Index_Event A
Left join [$(TargetSchema)].DRE_Index_Event_2_5 B
on A.Index_year=B.Index_year
Left join [$(TargetSchema)].DRE_Index_Event_SHA C
on A.Index_year=C.Index_year
Left join [$(TargetSchema)].DRE_Index_Event_SHA_2_5 D
on A.Index_year=D.Index_year
Left join [$(TargetSchema)].DRE_Index_Event_SHA_Train E
on A.Index_year=E.Index_year


--Select * from SHA2_Synoma_ID_TGT.DRE_rpt_Index_Event order by 1