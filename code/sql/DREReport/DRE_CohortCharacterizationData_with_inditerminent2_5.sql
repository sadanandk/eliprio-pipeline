--:setvar SCHEMA SHA_Pipeline_All

--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_ID_4NOV

Set NOCOUNT ON

Select IsNULL(A.VARIABLE,'') as Variable, 
case when (ISNULL(Replace(Replace(UPPER(LEFT(a.Statistic,1))+LOWER(SUBSTRING(a.Statistic, 2, LEN(a.Statistic))),',',';'),'_',' '), ''))='Seizure disorders' then 'Other AEDs'
Else ISNULL(Replace(Replace(UPPER(LEFT(a.Statistic,1))+LOWER(SUBSTRING(a.Statistic, 2, LEN(a.Statistic))),',',';'),'_',' '), '') 
end AS Statistic,
isNULL(b.Train,'') Train,isNULL(A.SHA,'') SHA,
CASE WHEN	C.[Value] IS NULL AND A.Statistic IS NOT NULL THEN '0 (0.0%)' ELSE 
CASE WHEN ((c.[% of PatientCount] IS NULL) OR (c.[% of PatientCount]=0)) THEN ISNULL(REPLACE(c.[Value],'.00',''),'')
	ELSE
		CONCAT(ISNULL(REPLACE(c.[Value],'.00',''),''),' (',ISNULL(CAST(c.[% of PatientCount] AS DECIMAL(5, 1)),''),'%)') END
		END  as [PHS w/ Indeterminates],
CASE WHEN	d.[Value] IS NULL AND A.Statistic IS NOT NULL THEN '0 (0.0%)' ELSE 
CASE WHEN ((d.[% of PatientCount] IS NULL) OR (d.[% of PatientCount]=0))
	 THEN ISNULL(REPLACE(d.[Value],'.00',''),'')
	ELSE
		CONCAT(ISNULL(REPLACE(d.[Value],'.00',''),''),' (',ISNULL(CAST(d.[% of PatientCount] AS DECIMAL(5, 1)),''),'%)') END
		END  as [PHS w/o Indeterminates],
CASE WHEN	e.[Value] IS NULL AND A.Statistic IS NOT NULL THEN '0 (0.0%)' ELSE 
CASE WHEN ((e.[% of PatientCount] IS NULL) OR (e.[% of PatientCount]=0))
						THEN ISNULL(REPLACE(e.[Value],'.00',''),'')
				  ELSE
						CONCAT(ISNULL(REPLACE(e.[Value],'.00',''),''),' (',ISNULL(CAST(e.[% of PatientCount] AS DECIMAL(5, 1)),''),'%)') END
				  END  as [Only PHS Indeterminates]
				  from [$(TargetSchema)].[dre_cohortcharacterization_SHA_2_5Y] a
inner join [$(TargetSchema)].[dre_cohortcharacterization_SHA_TRAIN_2_5Y] b
on isnull(a.Statistic,'') = isnull(b.Statistic,'') and isnull(a.variable,'')= isnull(b.variable,'') AND	A.ID = B.ID
left join [$(TargetSchema)].[DRE_CohortCharacterization_2_5] c
on isnull(a.Statistic,'') = replace (isnull(C.Label,''),',',';') and isnull(a.variable,'')= isnull(c.variable,'') AND	A.ID = c.ID
left join [$(TargetSchema)].[DRE_CohortCharacterization_WO_inditerminant_2_5] d
on isnull (a.Statistic,'') = replace (isnull(d.Label,''),',',';') and isnull(c.variable,'')= isnull(d.variable,'') AND	A.ID = d.ID
left join [$(TargetSchema)].[DRE_CohortCharacterization_Only_inditerminant_2_5] e
on isnull(a.Statistic,'') = replace (isnull(e.Label,''),',',';') and isnull(c.variable,'')= isnull(e.variable,'')  AND	A.ID = e.ID
order by a.[id] ,c.[% of PatientCount] DESC


-----------------------------old-------------------------------
--SET NOCOUNT ON
--SELECT 
--	   CAST(ISNULL([Variable],'') AS varchar(40)) AS [Variable]
--      ,CAST(ISNULL(Replace([Label],',',';'),'') AS VARCHAR(60)) AS [Label]
--      ,CASE WHEN (([% of PatientCount] IS NULL) or ([% of PatientCount]=0))
--	   THEN ISNULL(CAST([Value] AS varchar(10)),'')
--	   ELSE
--	   concat(ISNULL(CAST([Value] AS varchar(10)),''),' (',round(ISNULL(CAST([% of PatientCount] AS varchar(10)),''),1),')')
--	   END  AS [Statistic (n%)]
--	   FROM [$(TargetSchema)].[DRE_CohortCharacterization_2_5] WITH(NOLOCK)
--  ORDER BY ID, Value DESC, [Label] asc