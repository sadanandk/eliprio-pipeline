-- :setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_ID_TGT



SET NOCOUNT ON

------------------------------------
--Patients with Px Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#PX_DRE') IS NOT NULL
DROP TABLE #PX_DRE

select *
into #PX_DRE
from
(
select *,row_number() over(partition by patient_id order by patient_id,Payer_type) Num
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G')
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#PX1') IS NOT NULL
DROP TABLE #PX1

--5131
select distinct D.*
into #PX1
from 
(
select C.* ,Payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,Payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #PX_DRE)
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B
)C
inner join [$(SrcSchema)].[src_dx_claims] PX on C.patient_id = PX.patient_id and PX.service_date = C.service_date
where  Payer_type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#PX_Payer') IS NOT NULL
DROP TABLE #PX_Payer

--6969
select *
into #PX_Payer
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX_DRE
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX1
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1

------------------------------------
--Patients with Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#RX_DRE') IS NOT NULL
DROP TABLE #RX_DRE

--10551
select *
into #RX_DRE
from
(
select * , row_number() over(partition by patient_id order by patient_id,Payer_Type) Num
from 
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G') 
and days_supply>0
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#RX1') IS NOT NULL
DROP TABLE #RX1

--150
select distinct D.*
into #RX1
from 
(
select C.* ,payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_Type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #RX_DRE)
and days_supply>0
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B 
)C 
inner join [$(SrcSchema)].[src_rx_claims] RX on C.patient_id = RX.patient_id and RX.service_date = C.service_date
where Payer_Type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#RX_Payer') IS NOT NULL
DROP TABLE #RX_Payer

--10701
select *
into #RX_Payer
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX_DRE
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX1
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1
and patient_id not in (select patient_id from #PX_Payer)


------------------------------------
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping') IS NOT NULL
DROP TABLE #Payer_Mapping

--10784
select *
into #Payer_Mapping
from
(
select * from #PX_Payer
union
select * from #RX_Payer where patient_id not in (select patient_id from #PX_Payer )
)A

-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints

CREATE TABLE #HEOR_Economic_EndPoints
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints
set 
  #HEOR_Economic_EndPoints.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints]
from #HEOR_Economic_EndPoints_Low_DRE

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints


select '' [HEOR Economic Endpoints with Low Risk Stratification]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints]
order by Id


------------------------------------
--Age 18-44 
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping1') IS NOT NULL
DROP TABLE #Payer_Mapping1

--10784
select *
into #Payer_Mapping1
from
(
select * from #PX_Payer
union
select * from #RX_Payer where patient_id not in (select patient_id from #PX_Payer )
)A
where age between 18 and 44


-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE1') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE1

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE1
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping1 P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints1') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints1

CREATE TABLE #HEOR_Economic_EndPoints1
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints1
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints1
set 
  #HEOR_Economic_EndPoints1.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints1.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints1.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints1.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints1.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints1.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints1.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints1.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints1.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE1 T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44]
from #HEOR_Economic_EndPoints_Low_DRE1

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints1

select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for Age 18-44]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44]
order by Id

------------------------------------
--Age 45 - 64
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping2') IS NOT NULL
DROP TABLE #Payer_Mapping2

--10784
select *
into #Payer_Mapping2
from
(
select * from #PX_Payer
union
select * from #RX_Payer where patient_id not in (select patient_id from #PX_Payer )
)A
where age between 45 and 64


-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE2') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE2

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE2
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping2 P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints2') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints2

CREATE TABLE #HEOR_Economic_EndPoints2
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints2
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints2
set 
  #HEOR_Economic_EndPoints2.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints2.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints2.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints2.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints2.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints2.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints2.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints2.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints2.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE2 T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64]
from #HEOR_Economic_EndPoints_Low_DRE2

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints2

select '' 
select '' [HEOR Economic Endpoints with Low Risk Stratification for Age 45-64] 
select
 Id 
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64]
order by Id


------------------------------------
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping3') IS NOT NULL
DROP TABLE #Payer_Mapping3

--10784
select *
into #Payer_Mapping3
from
(
select * from #PX_Payer
union
select * from #RX_Payer where patient_id not in (select patient_id from #PX_Payer )
)A
where age >= 65


-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE3') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE3

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE3
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping3 P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints3') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints3

CREATE TABLE #HEOR_Economic_EndPoints3
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints3
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints3
set 
  #HEOR_Economic_EndPoints3.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints3.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints3.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints3.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints3.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints3.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints3.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints3.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints3.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE3 T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above]
from #HEOR_Economic_EndPoints_Low_DRE3

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints3


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for Age 65 and Above]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above]
order by Id

-------------------------------------------
-- Without Indeterminate
-------------------------------------------


IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I

CREATE TABLE #HEOR_Economic_EndPoints_I
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I
set 
  #HEOR_Economic_EndPoints_I.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate]
from #HEOR_Economic_EndPoints_Low_DRE_I

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification without Indeterminate]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate]
order by Id


-----------------------------
-- Age 18 - 44 Without Indeterminate
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I1') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I1

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I1
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping1 P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I1') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I1

CREATE TABLE #HEOR_Economic_EndPoints_I1
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I1
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I1
set 
  #HEOR_Economic_EndPoints_I1.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I1.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I1.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I1.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I1.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I1.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I1.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I1.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I1.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I1 T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate]
from #HEOR_Economic_EndPoints_Low_DRE_I1

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0)  +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0)  + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I1


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification without Indeterminate for Age 18-44]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate]
order by Id

-----------------------------
-- Age 45-64
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I2') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I2

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I2
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping2 P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I2') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I2

CREATE TABLE #HEOR_Economic_EndPoints_I2
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I2
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I2
set 
  #HEOR_Economic_EndPoints_I2.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I2.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I2.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I2.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I2.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I2.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I2.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I2.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I2.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I2 T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate]
from #HEOR_Economic_EndPoints_Low_DRE_I2

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0)  +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0)  + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I2


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification without Indeterminate for Age 44-64]
select
 Id 
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate]
order by Id


-----------------------------
-- Age 65 & Above
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I3') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I3

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I3
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping3 P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I3') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I3

CREATE TABLE #HEOR_Economic_EndPoints_I3
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I3
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I3
set 
  #HEOR_Economic_EndPoints_I3.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I3.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I3.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I3.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I3.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I3.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I3.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I3.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I3.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I3 T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate]
from #HEOR_Economic_EndPoints_Low_DRE_I3

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I3


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification without Indeterminate for Age 65 and Above]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate]
order by Id

/***************************************************/
-- 2.5 Y
/***************************************************/

------------------------------------
--Patients with Px Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#PX_DRE_Y') IS NOT NULL
DROP TABLE #PX_DRE_Y

select *
into #PX_DRE_Y
from
(
select *,row_number() over(partition by patient_id order by patient_id,Payer_type) Num
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] Y on DRE.patient_id = Y.patient_id
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G')
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#PX1_Y') IS NOT NULL
DROP TABLE #PX1_Y

--5131
select distinct D.*
into #PX1_Y
from 
(
select C.* ,Payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,Payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, Payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] Y on DRE.patient_id = Y.patient_id
inner join [$(SrcSchema)].[src_dx_claims] PX on DRE.patient_id = PX.patient_id and PX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #PX_DRE)
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B
)C
inner join [$(SrcSchema)].[src_dx_claims] PX on C.patient_id = PX.patient_id and PX.service_date = C.service_date
where  Payer_type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#PX_Payer_Y') IS NOT NULL
DROP TABLE #PX_Payer_Y

--6969
select *
into #PX_Payer_Y
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX_DRE_Y
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,Payer_type,Outcome_Variable from #PX1_Y
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1

------------------------------------
--Patients with Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#RX_DRE_Y') IS NOT NULL
DROP TABLE #RX_DRE_Y

--10551
select *
into #RX_DRE_Y
from
(
select * , row_number() over(partition by patient_id order by patient_id,Payer_Type) Num
from 
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
from [$(TargetSchema)].[app_dre_cohort] DRE inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] Y on DRE.patient_id = Y.patient_id
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date = valid_index
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_type in ('V','R','M','G') 
and days_supply>0
)A
)B
where Num=1


IF OBJECT_ID('tempdb..#RX1_Y') IS NOT NULL
DROP TABLE #RX1_Y

--150
select distinct D.*
into #RX1_Y
from 
(
select C.* ,payer_type, row_number() over(partition by C.patient_id order by C.patient_id,daysdiff,payer_type) Num
from 
(
select *,DaysDiff = case when valid_index < service_date then datediff(day,valid_index,service_date) else datediff(day,service_date,valid_index)  end
from
(
select distinct patient_id,maxdate,enddate,aed,aed_days,valid_index,Outcome_Variable
,service_date = case when flag = 0 then max(service_date) else min(service_date) end
from
(
select  distinct dre.patient_id,maxdate,enddate,aed,aed_days,valid_index,unique_Aed_count, payer_type,service_date
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Flag = case when service_date >= valid_index then 1 else 0 end
from [$(TargetSchema)].[app_dre_cohort] DRE inner join [$(TargetSchema)].[d6.5_d5_dre_sha_score_2_5yr] Y on DRE.patient_id = Y.patient_id
inner join [$(SrcSchema)].[src_rx_claims] RX on DRE.patient_id = RX.patient_id and RX.service_date between dateadd(day,-30,valid_index) and dateadd(day,30,valid_index)
where valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and Payer_Type in ('V','R','M','G')
and dre.patient_id not in (select distinct patient_id from #RX_DRE)
and days_supply>0
)A
group by patient_id,maxdate,enddate,aed,aed_days,valid_index,flag,Outcome_Variable
)B 
)C 
inner join [$(SrcSchema)].[src_rx_claims] RX on C.patient_id = RX.patient_id and RX.service_date = C.service_date
where Payer_Type in ('V','R','M','G')
)D
where Num =1


IF OBJECT_ID('tempdb..#RX_Payer_Y') IS NOT NULL
DROP TABLE #RX_Payer_Y

--10701
select *
into #RX_Payer_Y
from
(
select A.patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,A.Outcome_Variable,Age,Gender,CCI_Score,[Model Label],[class probabilities],DREriskStratification
,Row_number() over(partition by a.patient_id order by A.patient_id,service_date) Num
from
(
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX_DRE_Y
union
select patient_id,maxdate,enddate,aed,aed_days,valid_index,service_date,payer_type,Outcome_Variable from #RX1_Y
)A
left join [$(TargetSchema)].[DRE_Scores] Scores on A.patient_id = Scores.patient_id
)B
where num = 1
and patient_id not in (select patient_id from #PX_Payer_Y)


------------------------------------
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping_Y') IS NOT NULL
DROP TABLE #Payer_Mapping_Y

--10784
select *
into #Payer_Mapping_Y
from
(
select * from #PX_Payer_Y
union
select * from #RX_Payer_Y where patient_id not in (select patient_id from #PX_Payer_Y )
)A

-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Y

CREATE TABLE #HEOR_Economic_EndPoints_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_Y
set 
  #HEOR_Economic_EndPoints_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_Y


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for 2.5Y]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_2_5Y]
order by Id

------------------------------------
--Patients with Px or Rx Payer mapping
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping1_Y') IS NOT NULL
DROP TABLE #Payer_Mapping1_Y

--10784
select *
into #Payer_Mapping1_Y
from
(
select * from #PX_Payer_Y
union
select * from #RX_Payer_Y where patient_id not in (select patient_id from #PX_Payer_Y )
)A
where age between 18 and 44


-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE1_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE1_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE1_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping1_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints1_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints1_Y

CREATE TABLE #HEOR_Economic_EndPoints1_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints1_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints1_Y
set 
  #HEOR_Economic_EndPoints1_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints1_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints1_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints1_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints1_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints1_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints1_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints1_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints1_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE1_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE1_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints1_Y


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for 2.5Y for Age 18-44]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_2_5Y]
order by Id


------------------------------------
--Age 44-64
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping2_Y') IS NOT NULL
DROP TABLE #Payer_Mapping2_Y

--10784
select *
into #Payer_Mapping2_Y
from
(
select * from #PX_Payer_Y
union
select * from #RX_Payer_Y where patient_id not in (select patient_id from #PX_Payer_Y )
)A
where age between 45 and 64


-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE2_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE2_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE2_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping2_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints2_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints2_Y

CREATE TABLE #HEOR_Economic_EndPoints2_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints2_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints2_Y
set 
  #HEOR_Economic_EndPoints2_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints2_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints2_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints2_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints2_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints2_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints2_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints2_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints2_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE2_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE2_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints2_Y


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for 2.5Y for Age 45-64]
select
 Id 
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_2_5Y]
order by Id


------------------------------------
--65 & Above
------------------------------------

IF OBJECT_ID('tempdb..#Payer_Mapping3_Y') IS NOT NULL
DROP TABLE #Payer_Mapping3_Y

--10784
select *
into #Payer_Mapping3_Y
from
(
select * from #PX_Payer_Y
union
select * from #RX_Payer_Y where patient_id not in (select patient_id from #PX_Payer_Y )
)A
where age >= 65


-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE3_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE3_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicaid, Indeterminate],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Medicare, Indeterminate],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)+isnull([Private, Indeterminate],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE3_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping3_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints3_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints3_Y

CREATE TABLE #HEOR_Economic_EndPoints3_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints3_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints3_Y
set 
  #HEOR_Economic_EndPoints3_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints3_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints3_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints3_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints3_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints3_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints3_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints3_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints3_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE3_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicaid, Indeterminate],' (',[%Medicaid, Indeterminate],'%)') [Medicaid, Indeterminate]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Medicare, Indeterminate],' (',[%Medicare, Indeterminate],'%)') [Medicare, Indeterminate]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,concat([Private, Indeterminate],' (',[%Private, Indeterminate]	,'%)') [Private, Indeterminate]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE3_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicaid, Indeterminate] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Medicare, Indeterminate] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast([Private, Indeterminate]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) + isnull([Medicaid, Indeterminate]*[%Medicaid, Indeterminate],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + isnull([Medicare, Indeterminate]*[%Medicare, Indeterminate],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0) + isnull([Private, Indeterminate]*[%Private, Indeterminate],0)
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints3_Y


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for 2.5Y for Age 65 and Above]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicaid, Indeterminate] as varchar(50)),'') [Medicaid Indeterminate]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Medicare, Indeterminate] as varchar(50)),'') [Medicare Indeterminate]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Private, Indeterminate] as varchar(50)),'') [Commercial Indeterminate]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_2_5Y]
order by Id


------------------------------
-- Without Indeterminate
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I_Y

CREATE TABLE #HEOR_Economic_EndPoints_I_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I_Y
set 
  #HEOR_Economic_EndPoints_I_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE_I_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0) +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0) + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I_Y


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for 2.5Y without Indeterminate]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_without_Indeterminate_2_5Y]
order by Id


-----------------------------
-- Age 18-44
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I1_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I1_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I1_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping1_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I1_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I1_Y

CREATE TABLE #HEOR_Economic_EndPoints_I1_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I1_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I1_Y
set 
  #HEOR_Economic_EndPoints_I1_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I1_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I1_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE_I1_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0)  +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0)  + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I1_Y


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for 2.5Y without Indeterminate for Age 18-44]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age18_44_without_Indeterminate_2_5Y]
order by Id


-----------------------------
-- Calculating Weighted N
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I2_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I2_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I2_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping2_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I2_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I2_Y

CREATE TABLE #HEOR_Economic_EndPoints_I2_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I2_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I2_Y
set 
  #HEOR_Economic_EndPoints_I2_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I2_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I2_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE_I2_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0)  +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0)  + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I2_Y


select ''
select '' [HEOR Economic Endpoints with Low Risk Stratification for 2.5Y without Indeterminate for Age 45-64]
select
 Id 
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age45_64_without_Indeterminate_2_5Y]
order by Id

-----------------------------
-- Age 65 & Above
-----------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_Low_DRE_I3_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_Low_DRE_I3_Y

select 0 Id,'' [Type]
,[Medicaid, Non_DRE],			cast(round( [Medicaid, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Non_DRE]
,[Medicaid, DRE],				cast(round( [Medicaid, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, DRE]
,[Medicaid, Indeterminate],		cast(round( [Medicaid, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicaid, Indeterminate]
,[Medicare, Non_DRE],			cast(round( [Medicare, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Non_DRE]
,[Medicare, DRE],				cast(round( [Medicare, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, DRE]
,[Medicare, Indeterminate],		cast(round( [Medicare, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Medicare, Indeterminate]
,[Private, Non_DRE],			cast(round( [Private, Non_DRE]*100.0			/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Non_DRE]
,[Private, DRE],				cast(round( [Private, DRE]*100.0				/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, DRE]
,[Private, Indeterminate],		cast(round( [Private, Indeterminate]*100.0		/nullif(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)),0),2) as numeric(10,2))	 [%Private, Indeterminate]
,cast(sum(isnull([Medicaid, Non_DRE],0)+isnull([Medicaid, DRE],0)+isnull([Medicare, Non_DRE],0)+isnull([Medicare, DRE],0)+isnull([Private, Non_DRE],0)+isnull([Private, DRE],0)) as numeric(10,2)) [Weighted N]
into #HEOR_Economic_EndPoints_Low_DRE_I3_Y
from
(
select concat(Payer_Type,', ',Outcome_Variable) Type, count(distinct patient_id) Patients
from
(
select 
 distinct DRE.patient_id
,Outcome_variable = case when dre.Outcome_variable is null then 'Indeterminate' else dre.Outcome_variable end
,Payer_Type = case 
					when Payer_Type in ('V') then 'Private' 
					when Payer_Type in ('M') then 'Medicaid'
					when Payer_Type in ('R','G') then 'Medicare'
					else Payer_Type 
			  end
from [$(TargetSchema)].[dre_scores] Score
inner join [$(TargetSchema)].[app_dre_cohort] DRE with(nolock) on convert(numeric,Score.patient_id) = convert(numeric,DRE.patient_id)
inner join #Payer_Mapping3_Y P	with(nolock) on DRE.patient_id = P.patient_id and DRE.valid_index = P.valid_index
where P.valid_index is not null and  Lookback_Present = 1 and LookForward_Present = 1
and P.[DRERiskStratification]in ('Low')
and Payer_Type in ('V','R','M','G')
)A
group by outcome_variable,Payer_Type
)A
PIVOT
(
sum(patients)
FOR Type in ( [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate])
) as PivotDRE
group by [Medicaid, Non_DRE],[Medicaid, DRE],[Medicaid, Indeterminate],[Medicare, Non_DRE],[Medicare, DRE],[Medicare, Indeterminate],[Private, Non_DRE],[Private, DRE],[Private, Indeterminate]

------------------------------
--Economic EndPoints
------------------------------

IF OBJECT_ID('tempdb..#HEOR_Economic_EndPoints_I3_Y') IS NOT NULL
DROP TABLE #HEOR_Economic_EndPoints_I3_Y

CREATE TABLE #HEOR_Economic_EndPoints_I3_Y
(
Id int identity(1,1) NOT NULL,
[Type] varchar(50) NULL,
[Medicaid, Non_DRE] numeric NULL,
[%Medicaid, Non_DRE] numeric(10,2) NULL,
[Medicaid, DRE] numeric NULL,
[%Medicaid, DRE] numeric(10,2) NULL,
[Medicaid, Indeterminate] numeric NULL,
[%Medicaid, Indeterminate] numeric(10,2) NULL,
[Medicare, Non_DRE] numeric NULL,
[%Medicare, Non_DRE] numeric(10,2) NULL,
[Medicare, DRE] numeric NULL,
[%Medicare, DRE] numeric(10,2) NULL,
[Medicare, Indeterminate] numeric NULL,
[%Medicare, Indeterminate] numeric(10,2) NULL,
[Private, Non_DRE] numeric NULL,
[%Private, Non_DRE] numeric(10,2) NULL,
[Private, DRE] numeric NULL,
[%Private, DRE] numeric(10,2) NULL,
[Private, Indeterminate] numeric NULL,
[%Private, Indeterminate] numeric(10,2) NULL,
[Weighted Average] int NULL
)

insert into #HEOR_Economic_EndPoints_I3_Y
select [Type],
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0, 
cast([Government Non_DRE] as int),0,cast([Government DRE] as int),0,cast([Government Indeterminate] as int),0,
cast([Private Non_DRE] as int),0,cast([Private DRE] as int),0 ,cast([Private Indeterminate] as int),0,0
from [$(TargetSchema)].[std_ref_economic_endpoints]

-- Updating the Percentage Value to be multiplied

update #HEOR_Economic_EndPoints_I3_Y
set 
  #HEOR_Economic_EndPoints_I3_Y.[%Medicaid, Non_DRE] =		isnull(T.[%Medicaid, Non_DRE],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Medicaid, DRE] =			isnull(T.[%Medicaid, DRE],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Medicaid, Indeterminate] = isnull(T.[%Medicaid, Indeterminate],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Medicare, Non_DRE] =		isnull(T.[%Medicare, Non_DRE],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Medicare, DRE] =			isnull(T.[%Medicare, DRE],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Medicare, Indeterminate] = isnull(T.[%Medicare, Indeterminate],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Private, Non_DRE] =		isnull(T.[%Private, Non_DRE],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Private, DRE] =			isnull(T.[%Private, DRE],0)
, #HEOR_Economic_EndPoints_I3_Y.[%Private, Indeterminate] =	isnull(T.[%Private, Indeterminate],0)
from #HEOR_Economic_EndPoints_Low_DRE_I3_Y T


--------------------------
--Final Table
--------------------------

IF OBJECT_ID('[$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate_2_5Y]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate_2_5Y]

select Id,[Type]
,concat([Medicaid, Non_DRE]		,' (',[%Medicaid, Non_DRE]		 ,'%)') [Medicaid, Non_DRE]
,concat([Medicaid, DRE]			,' (',[%Medicaid, DRE]			 ,'%)') [Medicaid, DRE]
,concat([Medicare, Non_DRE]		,' (',[%Medicare, Non_DRE]		 ,'%)') [Medicare, Non_DRE]
,concat([Medicare, DRE]			,' (',[%Medicare, DRE]			 ,'%)') [Medicare, DRE]
,concat([Private, Non_DRE]	,' (',[%Private, Non_DRE]	 ,'%)') [Private, Non_DRE]
,concat([Private, DRE]		,' (',[%Private, DRE]		 ,'%)') [Private, DRE]
,[Weighted N]
into [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate_2_5Y]
from #HEOR_Economic_EndPoints_Low_DRE_I3_Y

union

select Id,[Type], 
 cast([Medicaid, Non_DRE]		as varchar(50))
,cast([Medicaid, DRE] 			as varchar(50))
,cast([Medicare, Non_DRE] 		as varchar(50))
,cast([Medicare, DRE] 			as varchar(50))
,cast([Private, Non_DRE] 	as varchar(50))
,cast([Private, DRE]  		as varchar(50))
,cast(round((
isnull([Medicaid, Non_DRE]*[%Medicaid, Non_DRE],0) + isnull([Medicaid, DRE]*[%Medicaid, DRE],0)  +
isnull([Medicare, Non_DRE]*[%Medicare, Non_DRE],0) + isnull([Medicare, DRE]*[%Medicare, DRE],0)  + 
isnull([Private, Non_DRE]*[%Private, Non_DRE],0) + isnull([Private, DRE]*[%Private, DRE],0)		
)/100.0,2) as numeric(10,2)) [Weighted Average]
from #HEOR_Economic_EndPoints_I3_Y

select ''
select '' [HEOR Economic Endpoint Report for Low Risk Stratification for 2.5Y without Indeterminate Age 65 and Above]
select 
 Id
,[Type]
,isnull(cast([Medicaid, Non_DRE] as varchar(50)),'') [Medicaid Non_DRE]
,isnull(cast([Medicaid, DRE] as varchar(50)),'') [Medicaid DRE]
,isnull(cast([Medicare, Non_DRE] as varchar(50)),'') [Medicare Non_DRE]
,isnull(cast([Medicare, DRE] as varchar(50)),'') [Medicare DRE]
,isnull(cast([Private, Non_DRE] as varchar(50)),'') [Commercial Non_DRE]
,isnull(cast([Private, DRE] as varchar(50)),'') [Commercial DRE]
,isnull(cast([Weighted N]as varchar(50)),'') [Weighted Average]
from [$(TargetSchema)].[DRE_Low_Stratification_Economic_EndPoints_Age65_Above_without_Indeterminate_2_5Y]
order by Id

select 'Medicare = R Medicaid = M Commercial = C
Total Direct Cost = TDC

% M_DRE = (DRE Patients covered under Medicaid/Total Patients)*100 (%)
% M_NonDRE = (Non DRE Patients covered under Medicaid/Total Patients)*100 (%)
% M_Indeterminate = (Indeterminate Patients covered under Medicaid/Total Patients)*100 (%)

Weighted Average =
(%M_DRE * TDC M_DRE) + (%M_NonDRE * TDC M_Non_DRE) + (%M_Indeterminate * TDC M_Indeterminate) +
(%R_DRE * TDC R_DRE) + (%R_NonDRE * TDC R_Non_DRE) + (%R_Indeterminate * TDC R_Indeterminate) +
(%C_DRE * TDC C_DRE) + (%C_NonDRE * TDC C_Non_DRE) + (%C_Indeterminate * TDC C_Indeterminate)'