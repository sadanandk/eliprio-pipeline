----:setvar TargetSchema SHA_Pipeline_All_Target
--:setvar TargetSchema SHA_Pipeline_All_Target_DRE6_5_TDS4_4
SET NOCOUNT ON

IF OBJECT_ID('tempdb..#Tot') IS NOT NULL
	DROP TABLE #Tot

SELECT sum(patient_count) AS tot_cnt
	,Evaluation_Window Evaluation
	,DRERiskStratification RiskStratification
INTO #Tot
FROM [$(TargetSchema)].[DRE_EndPoint2.1]
GROUP BY Evaluation_Window
	,DRERiskStratification

SET NOCOUNT ON

SELECT CAST('  ' AS VARCHAR(30))
	,'        				' [DRE Endpoint 2.1]
	,CAST('' AS VARCHAR(19))
WHERE 1 <> 1

SELECT CAST('  ' AS VARCHAR(30))
	,'        				' Evaluation
	,CAST('' AS VARCHAR(23)) Window
WHERE 1 <> 1

SELECT CAST(' ' AS VARCHAR(30))
	,CAST('' AS VARCHAR(10)) AS [1 Year]
	,CAST('' AS VARCHAR(5))
	,CAST('' AS VARCHAR(19)) AS [2.5 Years]
WHERE 1 <> 1

IF OBJECT_ID('tempdb..#TEMP') IS NOT NULL
	DROP TABLE #TEMP

SELECT id
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([DRE1] AS VARCHAR(30)) AS [DRE]
	,CAST([NonDRE1] AS VARCHAR(30)) AS [Non DRE]
	,CAST([DRE2] AS VARCHAR(30)) AS [DRE1]
	,CAST([NonDRE2] AS VARCHAR(30)) AS [Non DRE1]
INTO #TEMP
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,a.patient_count AS [DRE1]
		,b.patient_count AS [NonDRE1]
		,c.patient_count AS [DRE2]
		,d.patient_count AS [NonDRE2]
	FROM (
		SELECT Evaluation_Window,
		case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count
	FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '>=4' AND Evaluation_Window = 'Within 1 year' 
		group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
		) a
	INNER JOIN (
		SELECT Evaluation_Window,
		case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count
	FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '<4' AND Evaluation_Window = 'Within 1 year' 
		group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		SELECT Evaluation_Window,
		case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count
	FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '>=4' AND Evaluation_Window = 'Within 2.5 year' 
		group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT Evaluation_Window,
		case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count
	FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		WHERE Unique_AED_Count = '<4' AND Evaluation_Window = 'Within 2.5 year' 
		group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

DECLARE @Sensityvity1year DECIMAL(5, 2);
DECLARE @Sensityvity2year DECIMAL(5, 2);
DECLARE @Specivity1year DECIMAL(5, 2);
DECLARE @Specivity2year DECIMAL(5, 2);
DECLARE @TotalDRE INT;
DECLARE @DREHigh INT;
DECLARE @DREMedium INT;
DECLARE @DRELow INT;
DECLARE @TotalDRE1 INT;
DECLARE @DREHigh1 INT;
DECLARE @DREMedium1 INT;
DECLARE @DRELow1 INT;

SELECT @DREHigh = [DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @DREMedium = [DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Not-High'

--SELECT @DRELow = [DRE]
--FROM #TEMP
--WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalDRE = @DREHigh + @DREMedium --+ @DRELow

SELECT @Sensityvity1year = (((@DREHigh /*+ @DREMedium*/) * 1.0) / @TotalDRE) * 100

SELECT @DREHigh1 = [DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @DREMedium1 = [DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] =  'Not-High'

--SELECT @DRELow1 = [DRE1]
--FROM #TEMP
--WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalDRE1 = @DREHigh1 + @DREMedium1 --+ @DRELow1

SELECT @Sensityvity2year = (((@DREHigh1 /*+ @DREMedium1*/) * 1.0) / @TotalDRE1) * 100

INSERT INTO #TEMP
SELECT 4 AS [id]
	,CAST('Sensitivity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST(@Sensityvity1year AS VARCHAR(30)) AS [DRE]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE]
	,CAST(@Sensityvity2year AS VARCHAR(30)) AS [DRE1]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE1]

/* Specivity Details*/
DECLARE @TotalNonDRE INT;
DECLARE @NonDREHigh INT;
DECLARE @NonDREMedium INT;
DECLARE @NonDRELow INT;
DECLARE @TotalNonDRE1 INT;
DECLARE @NonDREHigh1 INT;
DECLARE @NonDREMedium1 INT;
DECLARE @NonDRELow1 INT;

SELECT @NonDREHigh = [Non DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NonDREMedium = [Non DRE]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Not-High'

--SELECT @NonDRELow = [Non DRE]
--FROM #TEMP
--WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNonDRE = @NonDREHigh + @NonDREMedium --+ @NonDRELow

SELECT @Specivity1year = (((/*@NonDREHigh +*/ @NonDREMedium) * 1.0) / @TotalNonDRE) * 100

SELECT @NonDREHigh1 = [Non DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'High'

SELECT @NonDREMedium1 = [Non DRE1]
FROM #TEMP
WHERE [DRE Risk Stratification] = 'Not-High'

--SELECT @NonDRELow1 = [Non DRE1]
--FROM #TEMP
--WHERE [DRE Risk Stratification] = 'Low'

SELECT @TotalNonDRE1 = @NonDREHigh1 + @NonDREMedium1 --+ @NonDRELow1

SELECT @Specivity2year = (((/*@NonDREHigh1 +*/ @NonDREMedium1) * 1.0) / @TotalNonDRE1) * 100

INSERT INTO #TEMP
SELECT 5 AS [id]
	,CAST('specificity' AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST(@Specivity1year AS VARCHAR(30)) AS [DRE]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE]
	,CAST(@Specivity2year AS VARCHAR(30)) AS [DRE1]
	,CAST(NULL AS VARCHAR(30)) AS [Non DRE1]

IF OBJECT_ID('tempdb..#TEMP1') IS NOT NULL
	DROP TABLE #TEMP1

SELECT [id]
	,CAST(DRERiskStratification AS VARCHAR(20)) AS [DRE Risk Stratification]
	,CAST([DRE1] AS VARCHAR(30)) AS [DRE]
	,CAST([NonDRE1] AS VARCHAR(30)) AS [Non DRE]
	,CAST([DRE2] AS VARCHAR(30)) AS [DRE1]
	,CAST([NonDRE2] AS VARCHAR(30)) AS [Non DRE1]
INTO #TEMP1
FROM (
	SELECT CASE 
			WHEN a.DRERiskStratification = 'High'
				THEN 1
			WHEN a.DRERiskStratification = 'Medium'
				THEN 2
			WHEN a.DRERiskStratification = 'Low'
				THEN 3
			END AS ID
		,a.DRERiskStratification
		,CONCAT (
			a.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((a.patient_count * 1.0 / a.tot_cnt) * 100)))
			,')'
			) AS [DRE1]
		,CONCAT (
			b.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((b.patient_count * 1.0 / b.tot_cnt) * 100)))
			,')'
			) AS [NonDRE1]
		,CONCAT (
			c.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((c.patient_count * 1.0 / c.tot_cnt) * 100)))
			,')'
			) AS [DRE2]
		,CONCAT (
			d.patient_count
			,' ( '
			,convert(VARCHAR(10), convert(NUMERIC(5, 2), ((d.patient_count * 1.0 / d.tot_cnt) * 100)))
			,')'
			) AS [NonDRE2]
	FROM (
	
	SELECT  Evaluation_Window,
	case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count,sum (t.tot_cnt) tot_cnt
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 1 year'
			group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
			
		--SELECT *
		--FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		--INNER JOIN #tot t
		--ON a.DRERiskStratification = t.RiskStratification
		--	AND a.Evaluation_Window = t.Evaluation
		--WHERE Unique_AED_Count = '>=4'
		--	AND Evaluation_Window = 'Within 1 year'
		) a
	INNER JOIN (
	
		SELECT  Evaluation_Window,
	case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count,sum (t.tot_cnt) tot_cnt
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 1 year'
			group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
			
		--SELECT *
		--FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		--INNER JOIN #tot t
		--ON a.DRERiskStratification = t.RiskStratification
		--	AND a.Evaluation_Window = t.Evaluation
		--WHERE Unique_AED_Count = '<4'
		--	AND Evaluation_Window = 'Within 1 year'
		) b
		ON a.DRERiskStratification = b.DRERiskStratification
	INNER JOIN (
		
			SELECT  Evaluation_Window,
	case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count,sum (t.tot_cnt) tot_cnt
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '>=4'
			AND Evaluation_Window = 'Within 2.5 year'
			group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
		
		--SELECT *
		--FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		--INNER JOIN #tot t
		--ON a.DRERiskStratification = t.RiskStratification
		--	AND a.Evaluation_Window = t.Evaluation
		--WHERE Unique_AED_Count = '>=4'
		--	AND Evaluation_Window = 'Within 2.5 year'
		) c
		ON a.DRERiskStratification = c.DRERiskStratification
	INNER JOIN (
		SELECT  Evaluation_Window,
	case when DRERiskStratification = 'High' 
		then 'High' 
		else 'Not-High' 
		end as DRERiskStratification,
		Unique_AED_Count,
		sum(patient_count)  patient_count,sum (t.tot_cnt) tot_cnt
		FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		INNER JOIN #tot t
			ON a.DRERiskStratification = t.RiskStratification
				AND a.Evaluation_Window = Evaluation
		WHERE Unique_AED_Count = '<4'
			AND Evaluation_Window = 'Within 2.5 year'
			group by Evaluation_Window,case when DRERiskStratification = 'High' then 'High' else 'Not-High' end ,Unique_AED_Count
		
		---SELECT *
		---FROM [$(TargetSchema)].[DRE_EndPoint2.1] a
		---INNER JOIN #tot t
		---ON a.DRERiskStratification = t.RiskStratification
		---	AND a.Evaluation_Window = t.Evaluation
		---WHERE Unique_AED_Count = '<4'
		---	AND Evaluation_Window = 'Within 2.5 year'
		) d
		ON a.DRERiskStratification = d.DRERiskStratification
	) p

UNION

SELECT id AS [id]
	,[DRE Risk Stratification]
	,[DRE] AS [DRE]
	,[Non DRE] AS [Non DRE]
	,[DRE1] AS [DRE1]
	,[Non DRE1] AS [Non DRE1]
FROM #TEMP
WHERE ID IN (
		4
		,5
		)

SELECT [DRE Risk Stratification]
	,[DRE] AS [DRE]
	,ISNULL([Non DRE], '') AS [Non DRE]
	,[DRE1] AS [DRE]
	,ISNULL([Non DRE1], '') AS [Non DRE]
FROM #Temp1
ORDER BY [ID]