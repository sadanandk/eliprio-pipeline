-- ------------------------------------------------------------------
-- - AUTHOR    : Vikas M
-- - USED BY   : 
-- - PURPOSE   : Create structure & load data for Source tables 
-- - NOTE      : 

-- -			--				 
-- - Execution : 
-- - Date      : 11-Jan-2019		
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ----------------------------------------------------------------------
Set Nocount ON

--Drop Schema Test
--:Setvar Src_Schema Test
--:SetVar SampleDataPath F:\Vikas\Pipeline\TDS_Pipeline\scripts\SampleDataRun\CSV_for_Tables\

--Create schema if it does not exists
IF NOT EXISTS (select * from sys.schemas where name = '$(SrcSchema)')
BEGIN
EXEC sp_executesql N'CREATE SCHEMA $(SrcSchema)'
END

Print 'Created Schema $(SrcSchema)'

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_Filtered_Ref_Patient]') AND type in (N'U'))

BEGIN


CREATE TABLE [$(SrcSchema)].[SRC_Filtered_Ref_Patient]
(
	[PATIENT_ID] [varchar](50) 
	
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_Filtered_Ref_Patient]

BULK INSERT [$(SrcSchema)].[SRC_Filtered_Ref_Patient]
FROM --'F:\VikasS\E\SHA\CSV\src_Filtered_ref_patient.csv'
'$(SampleDataPath)\src_Filtered_ref_patient.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
); 

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_RX_CLAIMS_STG]') AND type in (N'U'))

BEGIN


CREATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS_STG]
(
	[SERVICE_DATE] [date] NULL,
	[DAYS_SUPPLY] [float] NULL,
	[NDC] [varchar](30) NULL, -- from 11 to 30 size
	[PATIENT_ID] [varchar](30) NULL,-- from 10 30 
	[SERVICE_CHARGE] float NULL,--varchar to float
	[PAYER_TYPE] [varchar](1) NULL,
	[QUAN] float NULL -- numeric 18,2 to float 
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS_STG]

BULK INSERT [$(SrcSchema)].[SRC_RX_CLAIMS_STG]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_rx_claims.csv'
'$(SampleDataPath)\src_sample_rx_claims.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
); 

END

IF  NOT EXISTS (SELECT * FROM SYS.OBJECTS 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_RX_CLAIMS]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS]
(
	[SERVICE_DATE] [date] NULL,
	[AUTHORIZED_REFILLS] [float] NULL,
	[DAW] [varchar](2) NULL,
	[DAYS_SUPPLY] [float] NULL,
	[FILL_NBR] varchar(10) NULL, -- from float to varchar
	[NDC] [varchar](30) NULL, -- from 11 to 30 size
	[PATIENT_ID] [varchar](30) NULL,-- from 10 30 
	[SERVICE_CHARGE] float NULL,--varchar to float
	[PAYER_TYPE] [varchar](1) NULL,
	[PHARMACY_ID] [varchar](30) NULL,
	[PROVIDER_ID] [varchar](30) NULL, -- from 10 to 30
	[QUAN] float NULL -- numeric 18,2 to float 
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_RX_CLAIMS]

INSERT INTO [$(SrcSchema)].[SRC_RX_CLAIMS] 
(SERVICE_DATE,DAYS_SUPPLY,NDC,PATIENT_ID,SERVICE_CHARGE,PAYER_TYPE,QUAN)
SELECT SERVICE_DATE,DAYS_SUPPLY,NDC,PATIENT_ID,SERVICE_CHARGE,PAYER_TYPE,QUAN 
FROM [$(SrcSchema)].[SRC_RX_CLAIMS_STG] A
WHERE EXISTS (SELECT 1 From [$(SrcSchema)].[SRC_Filtered_Ref_Patient] B WHERE A.PATIENT_ID=B.PATIENT_ID)

END 

Print 'Created & Populated Table [SRC_RX_CLAIMS]'


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_HOSP_ENCOUNTER_STG]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_HOSP_ENCOUNTER_STG]
(
	[FROM_DT] [date] NULL,
	[TO_DT] [date] NULL,
	[DIAG1] [varchar](10) NULL,
	[VISIT_ID] [varchar](25) NULL,
	[SERVICE_CHARGE] [float] NULL,
	[PATIENT_ID] [varchar](10) NULL,
	[PAYER_TYPE] [varchar](1) NULL,
	[DIAG_VERS_TYP_ID1] [float] NULL 
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_HOSP_ENCOUNTER_STG]

BULK INSERT [$(SrcSchema)].[SRC_HOSP_ENCOUNTER_STG]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_hosp_encounter.csv'
'$(SampleDataPath)\src_sample_hosp_encounter.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
); 

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_HOSP_ENCOUNTER]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_HOSP_ENCOUNTER]
(
	[FROM_DT] [date] NULL,
	[TO_DT] [date] NULL,
	[ADMIT_DIAG] [varchar](10) NULL,
	[ADMIT_SRC] [varchar](10) NULL,
	[DIAG1] [varchar](10) NULL,
	[DIAG2] [varchar](10) NULL,
	[DIAG3] [varchar](10) NULL,
	[DIAG4] [varchar](10) NULL,
	[DIAG5] [varchar](10) NULL,
	[DIAG6] [varchar](10) NULL,
	[DIAG7] [varchar](10) NULL,
	[DIAG8] [varchar](10) NULL,
	[DISCHARGE_DISP] [varchar](130) NULL,
	[VISIT_ID] [varchar](25) NULL,
	[SERVICE_CHARGE] [float] NULL,
	[FACILITY_ID] [varchar](10) NULL,
	[PATIENT_ID] [varchar](10) NULL,
	[IP_OP_FLAG] [varchar](1) NULL,
	[PAYER_TYPE] [varchar](1) NULL,
	[DIAG_VERS_TYP_ID1] [float] NULL,
	[DIAG_VERS_TYP_ID2] [float] NULL,
	[DIAG_VERS_TYP_ID3] [float] NULL,
	[DIAG_VERS_TYP_ID4] [float] NULL,
	[DIAG_VERS_TYP_ID5] [float] NULL,
	[DIAG_VERS_TYP_ID6] [float] NULL,
	[DIAG_VERS_TYP_ID7] [float] NULL,
	[DIAG_VERS_TYP_ID8] [float] NULL,
	[ADMIT_DIAG_VERS_TYP_ID] [float] NULL
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_HOSP_ENCOUNTER]

INSERT INTO [$(SrcSchema)].[SRC_HOSP_ENCOUNTER] 
(FROM_DT,TO_DT,DIAG1,VISIT_ID,SERVICE_CHARGE,PATIENT_ID,PAYER_TYPE,DIAG_VERS_TYP_ID1)
SELECT FROM_DT,TO_DT,DIAG1,VISIT_ID,SERVICE_CHARGE,PATIENT_ID,PAYER_TYPE,DIAG_VERS_TYP_ID1
FROM [$(SrcSchema)].[SRC_HOSP_ENCOUNTER_STG] A
WHERE EXISTS (SELECT 1 From [$(SrcSchema)].[SRC_Filtered_Ref_Patient] B WHERE A.PATIENT_ID=B.PATIENT_ID)

END

Print 'Created & Populated Table [SRC_HOSP_ENCOUNTER]'

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_HOSP_PROCEDURE]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(SrcSchema)].[SRC_HOSP_PROCEDURE]
(
	[PROC_DATE] [date] NULL,
	[VISIT_ID] [varchar](10) NULL,
	[PROC_CD] [varchar](10) NULL,
	[PROC_VERS_TYP_ID] [float] NULL
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_HOSP_PROCEDURE]

BULK INSERT [$(SrcSchema)].[SRC_HOSP_PROCEDURE]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_hosp_procedure.csv'
'$(SampleDataPath)\src_sample_hosp_procedure.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
);

END 

Print 'Created & Populated Table [SRC_HOSP_PROCEDURE]'

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_DX_CLAIMS_STG]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS_STG]
(
	[SERVICE_DATE] [date] NULL,
	[DIAG1] [varchar](10) NULL,
	[PATIENT_ID] [varchar](30) NULL, -- 10 to 30 
	[PAYER_TYPE] [varchar](10) NULL,
	[PLACE_OF_SERVICE] [varchar](30) NULL,
	[PROC_CD] [varchar](10) NULL, -- 5 to 10 
	[SERVICE_CHARGE] [float] NULL,
	[DIAG_VERS_TYP_ID1] float NULL -- int to float

)

TRUNCATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS_STG]

BULK INSERT [$(SrcSchema)].[SRC_DX_CLAIMS_STG]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_dx_claims.csv'
'$(SampleDataPath)\src_sample_dx_claims.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
);

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_DX_CLAIMS]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS]
(
	[SERVICE_DATE] [date] NULL,
	[DIAG1] [varchar](10) NULL,
	[DIAG2] [varchar](10) NULL,
	[DIAG3] [varchar](10) NULL,
	[DIAG4] [varchar](10) NULL,
	[DIAG5] [varchar](10) NULL,
	[DIAG6] [varchar](10) NULL,
	[DIAG7] [varchar](10) NULL,
	[DIAG8] [varchar](10) NULL,
	[PATIENT_ID] [varchar](30) NULL, -- 10 to 30 
	[PAYER_TYPE] [varchar](10) NULL,
	[PLACE_OF_SERVICE] [varchar](30) NULL,
	[PROC_CD] [varchar](10) NULL, -- 5 to 10 
	[PROVIDER_ID] [varchar](30) NULL, -- 10 to 30 
	[SERVICE_CHARGE] [float] NULL,
	[QUAN] [float] NULL,
	[VISIT_ID] [varchar](60) NULL,
	[DIAG_VERS_TYP_ID1] float NULL, -- int to float
	[DIAG_VERS_TYP_ID2] float NULL, -- int to float
	[DIAG_VERS_TYP_ID3] float NULL, -- int to float
	[DIAG_VERS_TYP_ID4] float NULL, -- int to float
	[DIAG_VERS_TYP_ID5] float NULL, -- int to float
	[DIAG_VERS_TYP_ID6] float NULL, -- int to float
	[DIAG_VERS_TYP_ID7] float NULL, -- int to float
	[DIAG_VERS_TYP_ID8] float NULL  -- int to float
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_DX_CLAIMS]

INSERT INTO [$(SrcSchema)].[SRC_DX_CLAIMS] 
(SERVICE_DATE,DIAG1,PATIENT_ID,PAYER_TYPE,PLACE_OF_SERVICE,PROC_CD,SERVICE_CHARGE,DIAG_VERS_TYP_ID1)
SELECT SERVICE_DATE,DIAG1,PATIENT_ID,PAYER_TYPE,PLACE_OF_SERVICE,PROC_CD,SERVICE_CHARGE,DIAG_VERS_TYP_ID1 
From [$(SrcSchema)].[SRC_DX_CLAIMS_STG] A
WHERE EXISTS (SELECT 1 From [$(SrcSchema)].[SRC_Filtered_Ref_Patient] B WHERE A.PATIENT_ID=B.PATIENT_ID)

END
Print 'Created & Populated Table [SRC_DX_CLAIMS]'


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_REF_PATIENT_STG]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_REF_PATIENT_STG]
(
	[PATIENT_ID] [varchar](100) NULL,
	[YOB] [float] NULL,
	[SEX] [varchar](1) NULL,
	[ZIP3] [varchar](3) NULL,
)

TRUNCATE TABLE [$(SrcSchema)].[SRC_REF_PATIENT_STG]

BULK INSERT [$(SrcSchema)].[SRC_REF_PATIENT_STG]
FROM --'F:\VikasS\E\SHA\CSV\src_sample_ref_patient.csv'
'$(SampleDataPath)\src_sample_ref_patient.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
);

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_REF_PATIENT]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_REF_PATIENT]
(
	[PATIENT_ID] [varchar](100) NULL,
	[YOB] [float] NULL,
	[SEX] [varchar](1) NULL,
	[GROUP_IND] [float] NULL,
	[ZIP3] [varchar](3) NULL,
	[REGION] [varchar](1) NULL
)

Truncate Table [$(SrcSchema)].[SRC_REF_PATIENT]

Insert into [$(SrcSchema)].[SRC_REF_PATIENT] ([PATIENT_ID],[YOB],[SEX],[ZIP3])
Select [PATIENT_ID],[YOB],[SEX],[ZIP3] From [$(SrcSchema)].[SRC_REF_PATIENT_STG] A
WHERE EXISTS (SELECT 1 From [$(SrcSchema)].[SRC_Filtered_Ref_Patient] B WHERE A.PATIENT_ID=B.PATIENT_ID)

END

Print 'Created & Populated Table [SRC_REF_PATIENT]'

--Select top 1* From [DRE_Test].[SRC_REF_PRODUCT]

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[SRC_REF_PRODUCT_STG]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_REF_PRODUCT_STG]
(
	[AHFS_CLASS] [varchar](40) NULL,
	[AHFS_DESC] [varchar](100) NULL,
	[BRAND_NAME] [varchar](80) NULL,
	[GENERIC_NAME] [varchar](100) NULL,
	[NDC] [varchar](30) NULL,
	[STRENGTH] [varchar](80) NULL,
	[USC_CD] [varchar](50) NULL,
	[USC_NAME] [varchar](100) NULL
)

Truncate Table [$(SrcSchema)].[SRC_REF_PRODUCT_STG]
BULK INSERT [$(SrcSchema)].[SRC_REF_PRODUCT_STG]
FROM --'F:\VIKASS\E\SHA\CSV\SRC_SAMPLE_REF_PRODUCT.CSV'
'$(SampleDataPath)\SRC_SAMPLE_REF_PRODUCT.csv'
WITH 
(
rowterminator = '\n',
fieldterminator =','
);


END



IF  NOT EXISTS (SELECT * FROM SYS.OBJECTS 
WHERE OBJECT_ID = OBJECT_ID(N'[$(SrcSchema)].[SRC_REF_PRODUCT]') AND TYPE IN (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[SRC_REF_PRODUCT]
(
	[AHFS_CLASS] [varchar](40) NULL,
	[AHFS_DESC] [varchar](100) NULL,
	[BRAND_GENERIC_FLAG] [varchar](20) NULL,
	[BRAND_NAME] [varchar](80) NULL,
	[FORM_CD] [varchar](4) NULL,
	[FORM_DESC] [varchar](100) NULL,
	[GENERIC_NAME] [varchar](100) NULL,
	[GPI14] [varchar](30) NULL,
	[MFG_NAME] [varchar](50) NULL,
	[NDC] [varchar](30) NULL,
	[PACKSIZE] [float] NULL,
	[PACKSIZE_UOM] [varchar](2) NULL,
	[ROUTE_DESC] [varchar](30) NULL,
	[RX_OTC_CD] [varchar](1) NULL,
	[STRENGTH] [varchar](80) NULL,
	[USC_CD] [varchar](50) NULL,
	[USC_NAME] [varchar](100) NULL
)

Truncate Table [$(SrcSchema)].[SRC_REF_PRODUCT]

INSERT INTO [$(SrcSchema)].[SRC_REF_PRODUCT]
(AHFS_CLASS,AHFS_DESC,BRAND_NAME,GENERIC_NAME,NDC,STRENGTH,USC_CD,USC_NAME)
SELECT AHFS_CLASS,AHFS_DESC,BRAND_NAME,GENERIC_NAME,NDC,STRENGTH,USC_CD,USC_NAME
FROM [$(SrcSchema)].[SRC_REF_PRODUCT_STG]

END

Print 'Created & Populated Table [SRC_REF_PRODUCT]'
