-- ---------------------------------------------------------------------
-- - CITIUS TECH
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create the tables to identify patients for General ,Newly diagnosed,older,younger population for AED-TDS cohort
-- - NOTE      :  
-- - Execution : Final table  name: IMS_Master_Cohort_Population
-- - Date      : 12-SEP-2018
-- - Change History:	Change Date				Change Description
--						06-Dec-2018				Added REPLACE function to DX codes.
-- -					11-Jan-2019				ELCDS-996 : Iteration4 : Updatedthe code for all flag , Replaced First_rx_date with First_encounter
-- -					15-Mar-2019				ELCDS-1173 : Code changes from R569 to R56*
-- -					17-May-2019				Updated logic of Newlydiagnosis as per TDS iteration 4.4 (convulsion codes)
-- ---------------------------------------------------------------------- 

--:setvar Schema Patient_Target
--:Setvar SrcSchema 10_Patient

/********************************************************************** GenPop ,OlderPop ,YoungerPop *****************************************************************/

Print 'Populate app_tds_int_Master_Cohort_Population'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Master_Cohort_Population]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Master_Cohort_Population]

SELECT DISTINCT ROW_NUMBER() OVER (Partition BY Patient_id ORDER BY Index_Date) AS Index_ID 
				,patient_id 
				,Index_Date
				,Genpop
				,CASE WHEN genpop =1 AND Age >= 65 THEN 1 ELSE 0 END AS OlderPop 
				,CASE WHEN genpop =1 AND Age < 65 THEN 1 ELSE 0 END YoungPop  
INTO  [$(TargetSchema)].[app_tds_int_Master_Cohort_Population] 
FROM [$(TargetSchema)].[app_tds_cohort] 
WHERE index_date IS NOT NULL 
AND genpop =1 

Print 'Populate #FirstIndexDT'
Print Getdate()

IF OBJECT_ID('tempdb..#FirstIndexDT') IS NOT NULL
DROP TABLE #FirstIndexDT

SELECT Patient_Id ,Index_Date AS First_Index_date 
INTO #FirstIndexDT
FROM [$(TargetSchema)].[app_tds_int_Master_Cohort_Population]  (NOLOCK) 
WHERE Index_ID = 1



/********************************************************************** NEWLY DIAGNOSED QUERY *****************************************************************/

Print 'Populate #GENPOP_COHORT'
Print Getdate()


IF OBJECT_ID('tempdb..#GENPOP_COHORT') IS NOT NULL
DROP TABLE #GENPOP_COHORT

SELECT ROW_NUMBER() OVER(PARTITION BY A.PATIENT_ID ORDER BY A.PATIENT_ID,MAXDATE,ENDDATE) AS RECORD_NUM,A.* ,B.First_Index_date
INTO #GENPOP_COHORT
FROM [$(TargetSchema)].[app_tds_cohort]   A
JOIN #FirstIndexDT   B
ON A.patient_id =B.patient_id



Print 'Populate #CHECK1456'
Print Getdate()

IF OBJECT_ID('tempdb..#CHECK1456') IS NOT NULL
DROP TABLE #CHECK1456
--CONSTRAINT - I,IV,V,VI (Takes care of first AED on Index Date, Stability, 12 Months Eligibility and 2 yrs of observation period before it)

SELECT DISTINCT Patient_id,First_Index_date as INDEX_DATE 
INTO #CHECK1456
FROM #GENPOP_COHORT  WHERE RECORD_NUM =1 AND Maxdate = First_Index_date  AND DATEDIFF(dd,First_Encounter,index_Date)+1  > 730


Print 'Populate #NonEpibase3'
Print Getdate()

--New Result: 50,862 (12:33 Min)
IF OBJECT_ID('tempdb..#NonEpibase3') IS NOT NULL
DROP TABLE #NonEpibase3
		 
SELECT DISTINCT patient_id,[index_date]
INTO #NonEpibase3
FROM (
		SELECT DISTINCT  patient_id,[index_date],convert(date,service_date) service_date,dx_codes
		FROM
			(
				SELECT dx.[PATIENT_ID],[index_date], [SERVICE_DATE] ,[DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8]
				FROM   [$(SrcSchema)].[src_dx_claims] dx with(nolock)
				JOIN #CHECK1456 B 
				ON dx.patient_id = B.patient_id AND CONVERT(DATE,dx.service_date) < B.index_date where dx.PROC_CD IS NULL
				
			)strw
			UNPIVOT
			(
				Dx_Codes FOR Codes_Column IN ([DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8])
			) AS A
		WHERE ISNULL(REPLACE(Dx_Codes,'.',''),'') not like '345%' AND  ISNULL(REPLACE(Dx_Codes,'.',''),'') not like 'G40%'		
	)A
WHERE replace(Dx_Codes,'.','') NOT IN ('7803','78033','78039') AND   replace(Dx_Codes,'.','') NOT IN ('R56','R561','R569')	

--Patients has 345 or G40 in lookback of 40 days prior to index
Print 'Populate #PatExclude345_40Day'
Print Getdate()

IF OBJECT_ID('tempdb..#PatExclude345_40Day') IS NOT NULL
DROP TABLE #PatExclude345_40Day

SELECT DISTINCT B.patient_id
INTO #PatExclude345_40Day
FROM [$(SrcSchema)].[src_dx_claims] dx 
INNER JOIN #NonEpibase3 B ON dx.patient_id = B.patient_id
WHERE  CONVERT(DATE, dx.service_date) between DATEADD(DAY,-40,B.index_date) and DATEADD(DAY,-1,B.index_date)  and 
(
	ISNULL(REPLACE(dx.DIAG1,'.',''),'') like '345%' OR ISNULL(REPLACE(dx.DIAG2,'.',''),'') like '345%' OR ISNULL(REPLACE(dx.DIAG3,'.',''),'') like '345%' OR ISNULL(REPLACE(dx.DIAG4,'.',''),'') like '345%' OR
	ISNULL(REPLACE(dx.DIAG5,'.',''),'') like '345%' OR ISNULL(REPLACE(dx.DIAG6,'.',''),'') like '345%' OR ISNULL(REPLACE(dx.DIAG7,'.',''),'') like '345%' OR ISNULL(REPLACE(dx.DIAG8,'.',''),'') like '345%' OR
	
	
	ISNULL(REPLACE(dx.DIAG1,'.',''),'') like 'G40%' OR ISNULL(REPLACE(dx.DIAG2,'.',''),'') like 'G40%' OR ISNULL(REPLACE(dx.DIAG3,'.',''),'') like 'G40%' OR ISNULL(REPLACE(dx.DIAG4,'.',''),'') like 'G40%' OR
	ISNULL(REPLACE(dx.DIAG5,'.',''),'') like 'G40%' OR ISNULL(REPLACE(dx.DIAG6,'.',''),'') like 'G40%' OR ISNULL(REPLACE(dx.DIAG7,'.',''),'') like 'G40%' OR ISNULL(REPLACE(dx.DIAG8,'.',''),'') like 'G40%' 
)


--Patients has 7803* or R567 in lookback of 40 days prior to index
Print 'Populate #PatExclude780All_40Day'
Print Getdate()

IF OBJECT_ID('tempdb..#PatExclude780All_40Day') IS NOT NULL
DROP TABLE #PatExclude780All_40Day


SELECT DISTINCT patient_id,service_date 
INTO #PatExclude780All_40Day
FROM (
		SELECT DISTINCT  patient_id,INDEX_DATE,convert(date,service_date) service_date,dx_codes
	
		FROM
			(
				SELECT DISTINCT dx.[PATIENT_ID], B.INDEX_DATE,SERVICE_DATE ,[DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8]
    			FROM   [$(SrcSchema)].[src_dx_claims] dx with(nolock)
				JOIN #CHECK1456 B 
				ON dx.patient_id = B.patient_id AND CONVERT(DATE, dx.service_date) between DATEADD(DAY,-40,B.index_date) and DATEADD(DAY,-1,B.index_date) 
				where (  replace(dx.DIAG1,'.','') like '7803%' or replace(dx.DIAG2,'.','') like '7803%' or replace(dx.DIAG3,'.','') like '7803%' or replace(dx.DIAG4,'.','') like '7803%' or
						replace(dx.DIAG5,'.','') like '7803%' or replace(dx.DIAG6,'.','') like '7803%' or replace(dx.DIAG7,'.','') like '7803%' or replace(dx.DIAG8,'.','') like '7803%' or
						replace(dx.DIAG1,'.','') like 'R56%'  or replace(dx.DIAG2,'.','') like 'R56%'  or replace(dx.DIAG3,'.','') like 'R56%'	or replace(dx.DIAG4,'.','') like 'R56%' or
						replace(dx.DIAG5,'.','') like 'R56%'  or replace(dx.DIAG6,'.','') like 'R56%'  or replace(dx.DIAG7,'.','') like 'R56%'	or replace(dx.DIAG8,'.','') like 'R56%')
				AND  dx.PROC_CD IS NULL 
			)strw
			UNPIVOT
			(
				Dx_Codes FOR Codes_Column IN ([DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8])
			) AS A
		WHERE replace(Dx_Codes,'.','') LIKE '7803%' OR replace(Dx_Codes,'.','') LIKE 'R56%'
	)A
WHERE replace(Dx_Codes,'.','') NOT IN ('78031','78032') AND   replace(Dx_Codes,'.','') NOT LIKE 'R560%'		



--Patient having 7803* or R567 more than 1 in lookback of 40 days
--New Result : 73,69
Print 'Populate #PatExclude780_More_than1_40Day'
Print Getdate()

IF OBJECT_ID('tempdb..#PatExclude780_More_than1_40Day') IS NOT NULL
DROP TABLE #PatExclude780_More_than1_40Day

SELECT Patient_id
INTO #PatExclude780_More_than1_40Day
FROM (
SELECT *,ROW_NUMBER() OVER(PARTITION BY patient_id ORDER BY patient_id,service_date)Num
FROM #PatExclude780All_40Day)A
GROUP BY patient_id
HAVING MAX(Num)>=2

--37,985
--Final patients satisfying Newly diagnosis criteria
Print 'Populate #NonEPibase'
Print Getdate()

IF OBJECT_ID('tempdb..#NonEPibase') IS NOT NULL
DROP TABLE #NonEPibase

SELECT DISTINCT patient_id 
INTO #NonEPibase
FROM #NonEpibase3
EXCEPT
( 
SELECT patient_id FROM #PatExclude345_40Day
UNION
SELECT patient_id FROM #PatExclude780_More_than1_40Day
)


ALTER TABLE [$(TargetSchema)].[app_tds_int_Master_Cohort_Population]  
ADD	NewlyDiagnosed INT

UPDATE [$(TargetSchema)].[app_tds_int_Master_Cohort_Population]   
SET NewlyDiagnosed = 0

UPDATE [$(TargetSchema)].[app_tds_int_Master_Cohort_Population]   
SET NewlyDiagnosed = 1 
FROM #NonEPibase 
WHERE [$(TargetSchema)].[app_tds_int_Master_Cohort_Population].patient_id = #NonEPibase.patient_id
AND [$(TargetSchema)].[app_tds_int_Master_Cohort_Population].Index_ID =1

