-- ------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Validation of Source Tables mandatory columns 
-- - NOTE      : 
-- -			 	 
--				 
-- - Execution : 
-- - Date      : 31-Aug-2018		
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ---------------------------------------------------------------------- 

/* Source Tables */
Set Nocount on;

--:setvar Schema TestPipeline_Sneha
--:Setvar SrcSchema Src_Schema

-- Table data is null

--if isnull((select top 1 'x' from [$(SrcSchema)].[src_hosp_procedure]	),'y')		<>'x'		RaisError('[src_hosp_procedure] Table is not populated in [$(SrcSchema)]',16,1)
if isnull((select top 1 'x' from [$(SrcSchema)].[src_ptnt_eligibility]),'y')		<>'x'		RaisError('[src_ptnt_eligibility] Table is not populated in [$(SrcSchema)]',16,1)
if isnull((select top 1 'x' from [$(SrcSchema)].[src_REF_patient]),'y')		<>'x'		RaisError('[src_ref_patient] Table is not populated in [$(SrcSchema)]',16,1)
if isnull((select top 1 'x' from [$(SrcSchema)].[src_REF_Product]),'y')		<>'x'		RaisError('[src_ref_Product] Table is not populated in [$(SrcSchema)]',16,1)
--if isnull((select top 1 'x' from [$(SrcSchema)].[src_REF_Provider]),'y')		<>'x'		RaisError('[src_ref_Provider] Table is not populated in [$(SrcSchema)]',16,1)
if isnull((select top 1 'x' from [$(SrcSchema)].[src_dx_claims]),'y')			<>'x'		RaisError('[src_dx_claims] Table is not populated in [$(SrcSchema)]',16,1)
if isnull((select top 1 'x' from [$(SrcSchema)].[src_rx_claims]),'y')			<>'x'		RaisError('[src_rx_claims] Table is not populated in [$(SrcSchema)]',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_icd9_icd10_cmgem]),'y')		<>'x'		RaisError('[std_REF_icd9_icd10_cmgem] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_MedicationCodesUSP]),'y')   <>'x'		RaisError('[std_REF_MedicationCodesUSP] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_AEDName]),'y')			<>'x'		RaisError('[std_REF_AEDName] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_CPT_Code_All]),'y')			<>'x'		RaisError('[std_REF_CPT_Code_All] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_AED_Abbreviation]),'y')		<>'x'		RaisError('[std_REF_AED_Abbreviation] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_CCI]),'y')			<>'x'		RaisError('[std_REF_CCI] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_ICD_Code_ALL]),'y')			<>'x'		RaisError('[std_REF_ICD_Code_ALL] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_ICD9_To_ICD10_Conversion]),'y') <>'x'	RaisError('[std_REF_ICD9_To_ICD10_Conversion] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_USPS_zip3zip1codes]),'y')	<>'x'		RaisError('[std_REF_USPS_zip3zip1codes] Table is not populated in $(TargetSchema)',16,1)
if isnull((select top 1 'x' from $(TargetSchema).[std_REF_time_dimension]),'y') <>'x' RaisError('[std_REF_time_dimension] Table is not populated in $(TargetSchema)',16,1)
--if isnull((select top 1 'x' from [$(SrcSchema)].[src_hosp_encounter]	),'y') <>'x' RaisError('[src_hosp_encounter] Table is not populated in [$(SrcSchema)]',16,1)


IF (
select distinct first_value([Status]) over(order by [Status])
from
(
select [Status] 
from 
(
select 
 [Status] = 
case 
	when 
			isnull(count(distinct [patient_id]),0) >0  
		AND isnull(count(distinct [service_date]),0)>0
		AND isnull(count(distinct [Days_Supply]),0)>0
		--AND isnull(count(distinct [Payer_Type]),0)>0
		AND isnull(count(distinct [NDC]),0)>0
	then 'Pass' else 'Fail' 
  end 
from [$(SrcSchema)].[src_rx_claims]
)A

union
select [Status] 
from
(
select 
[Status] =
case 
when
	 isnull(count(distinct [DIAG1]),0) >0 
 AND isnull(count(distinct [service_date]),0) >0
 AND isnull(count(distinct [PROC_CD]),0) >0
	then 'Pass' else 'Fail' 
  end 
from [$(SrcSchema)].[src_dx_claims]
)A
union
select [Status] 
from
(
select 
[Status] =
case 
when
    isnull(count(distinct [NDC]),0) >0 
AND isnull(count(distinct [USC_Name]),0) >0
AND isnull(count(distinct [generic_name]),0) >0 
AND isnull(count(distinct [brand_name]),0) >0
then 'Pass' else 'Fail' 
  end 
from [$(SrcSchema)].[src_ref_Product]
)A
union
select [Status] 
from
(
select 
[Status] =
case 
when
	isnull(count(distinct [YOB]),0) >0
AND isnull(count(distinct [sex]),0) >0
AND isnull(count(distinct [Zip3]),0) >0
then 'Pass' else 'Fail' 
  end 
from [$(SrcSchema)].[src_ref_patient]
)A
/*union
select [Status] 
from
(
select 
[Status] =
case 
when 
    isnull(count(distinct [PRI_SPECIALTY_DESC]),0) >0
AND isnull(count(distinct [Provider_id]),0) >0
then 'Pass' else 'Fail' 
  end 
from [$(SrcSchema)].[src_ref_Provider]
)A
*/
--union
--select [Status] 
--from
--(
--select 
--[Status] =
--case 
--when 
--	isnull(count(distinct [FROM_DT]),0) >0
--AND isnull(count(distinct [visit_id]),0) >0
--then 'Pass' else 'Fail' 
--  end
--from [$(SrcSchema)].[src_hosp_encounter]
--)A

/* union
select [Status] 
from
(
select 
[Status] =
case 
when 
	isnull(count(distinct [PROC_DATE]),0) >0
AND isnull(count(distinct [visit_id]),0) >0
then 'Pass' else 'Fail' 
  end
from [$(SrcSchema)].[src_hosp_procedure]
)A */
union
select [Status] 
from
(
select 
[Status] =
case when  isnull(count(distinct [USP Class]),0) >0   then 'Pass' else 'Fail' end
from $(TargetSchema).[std_ref_MedicationCodesUSP]
)A
)B
)='Fail'
RaisError('One or more mandatory columns are populated with only Null values in $(TargetSchema)',16,1)


-- Additional generic names if present in dataset

select distinct generic_name , count(distinct patient_id) PatientCount, count(*) RecordCount
into #temp
from [$(SrcSchema)].[src_ref_product] P
inner join [$(SrcSchema)].[src_rx_claims] rx on P.ndc = rx.ndc
where 
   generic_name like 'briva%'
or generic_name like 'carbama%'
or generic_name like 'cloba%'
or generic_name like 'dival%'
or generic_name like 'eslicar%'
or generic_name like 'ethosu%'
or generic_name like 'etho%'
or generic_name like 'ezo%'
or generic_name like 'Felb%'
or generic_name like 'Laco%'
or generic_name like 'Lamo%'
or generic_name like 'Leve%'
or generic_name like 'oxcar%'
or generic_name like 'Peramp%'
or generic_name like 'Phenob%'
or generic_name like 'Phenyt%'
or generic_name like 'Prega%'
or generic_name like 'Primi%'
or generic_name like 'Retig%'
or generic_name like 'Rufi%'
or generic_name like 'Topi%'
or generic_name like 'Valp%'
or generic_name like 'Viga%'
or generic_name like 'Zoni%'
group by generic_name

-- Additional generic names to be added

IF Exists(select generic_name from #temp
except
select generic_name from [$(TargetSchema)].[std_ref_AEDName]
)
PRINT ('Warning!! Additional Generic Name found in RX Data, std_REF_AEDName object needs to be populated on consultation with a CLinical Expert')

Print ('Validation Successfully Completed')