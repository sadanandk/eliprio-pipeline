-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Identify and create treatment change events,End Events,Early/Late,Index date(multiple) and Treatmentfailure outcome for Entire dataset  at regimen level
-- - NOTE      : This Query is implementd with reference to JIRA: ANALYTICS-588.(Molecule treatment <=30 are filtered out in this query but Multiple index date logic has been implemented for Treatment_outcome at regimen level)
-- -			 Please run and populate [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]   table (molecule level) before running current script as these script uses some of the tables that were created by molecule level script.
-- - Execution : Final table  name: [$(TargetSchema)].[app_tds_cohort]
-- - Date      : 10-OCT-2017
-- - Change History:	Change Date				Change Description
--						31-Oct-2017				Modify Treatment Outcome logic for Defect DT-116					
--						28-Nov-2017				Zip3_Zip1_State Population (ANALYTICS-984)
--						04-Jul-2018				Update script for Iteration-2 (ANALYTICS-2034)
--						09-Aug-2018				Populated [DS_0_LessThan_0] column to identify patients with days_supply<=0	(ELREFPRED-553)	
-- -					11-Jan-2019				ELCDS-954 : Iteration4 : Replaced First_Rx_Date and Last_Rx_Date with First_enconter and Last_Encounter	
-- -					15-Mar-2019				ELCDS-1173 : Iteration4.2 : Affed lookforward filter , removed from calculation :  12 month patient elg ,3 month rx elg , continues elg , Q_rx_elg
-- -					29-Mar-2019							: Added filter criteria of monotherapy
-- ---------------------------------------------------------------------- 

--:setvar Schema Patient_Target
--:Setvar SrcSchema 10_Patient

--------------------------------------Populated data at regimen level-------------------------------------

Print 'Populate Regiments_Stable'
Print Getdate()

IF OBJECT_ID('tempdb..#Regiments_Stable') IS NOT NULL
DROP TABLE #Regiments_Stable

SELECT ROW_NUMBER() OVER(PARTITION BY Patient_id,Start_Point ORDER BY  Patient_id,Start_Point,Abbreviation) AS ID,* 
INTO #Regiments_Stable 
FROM [$(TargetSchema)].[app_tds_int_Regiments_Treatment]

Print 'Populate app_tds_cohort'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_cohort]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_cohort]

SELECT * INTO [$(TargetSchema)].[app_tds_cohort] FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]   WHERE 1=2

INSERT INTO  [$(TargetSchema)].[app_tds_cohort] (Patient_id,Maxdate,enddate,AED,AED_Days)
SELECT DISTINCT Patient_id,start_point,End_Point,AED,DATEDIFF(dd,start_Point,End_Point)+1 AS AED_Days FROM (
				SELECT Patient_id,start_Point,End_Point, ISNULL([1],'') +ISNULL('__'+[2],'')+ISNULL('__'+[3],'')+ISNULL('__'+[4],'')+ISNULL('__'+[5],'')+ISNULL('__'+[6],'')+ISNULL('__'+[7],'')+ISNULL('__'+[8],'')+ISNULL('__'+[9],'')+ISNULL('__'+[10],'')+ISNULL('__'+[11],'')+ISNULL('__'+[12],'')+ISNULL('__'+[13],'')+ISNULL('__'+[14],'')+ISNULL('__'+[15],'')+ISNULL('__'+[16],'')+ISNULL('__'+[17],'')+ISNULL('__'+[18],'')+ISNULL('__'+[19],'')+ISNULL('__'+[20],'') AS AED
	
					FROM
					(SELECT ID,Patient_id,start_Point,End_Point,B.Abbreviation AS AED1
					FROM #Regiments_Stable A LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] B
					ON	 A.aed=B.aed 
					) AS SourceTable
					PIVOT
					(
					max(AED1)
					FOR ID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
					) AS PivotTable
				) A
					WHERE AED <> ''


----------------------------First and Last Rx encounter and Lookback flag populate-----------------------------------------

Print 'Populate #temp_first_Last_Encounters'
Print Getdate()


UPDATE [$(TargetSchema)].[app_tds_cohort]
SET [$(TargetSchema)].[app_tds_cohort].First_Encounter =  [$(TargetSchema)].[app_int_Patient_Profile].First_Encounter
,[$(TargetSchema)].[app_tds_cohort].Last_Encounter =  [$(TargetSchema)].[app_int_Patient_Profile].Last_Encounter
FROM [$(TargetSchema)].[app_int_Patient_Profile] 
WHERE [$(TargetSchema)].[app_tds_cohort].Patient_id = [$(TargetSchema)].[app_int_Patient_Profile].Patient_id




UPDATE [$(TargetSchema)].[app_tds_cohort] 
SET Lookback_Present = CASE WHEN DATEDIFF(dd,First_Encounter,Maxdate)+1  >  365 THEN 1  ELSE 0 END
, Lookforward_Present = CASE WHEN DATEDIFF(dd,Maxdate,Last_Encounter)+1  >  365 THEN 1  ELSE 0 END



--------------------------------Treatment status as regimen level-----------------------------
Print 'Populate #Set_S'
Print Getdate()

IF OBJECT_ID('tempdb..#Set_S') IS NOT NULL
	DROP TABLE #Set_S
	
	SELECT DISTINCT a.patient_id,a.maxdate, a.aed AS SET_S INTO #Set_S FROM [$(TargetSchema)].[app_tds_cohort] A
	LEFT JOIN [$(TargetSchema)].[app_tds_cohort] B
	ON A.PAtient_id =  B.PAtient_id
	AND A.Maxdate > B.Maxdate
	--ORDER BY 1 


	IF OBJECT_ID('tempdb..#BM') IS NOT NULL
	DROP TABLE #BM
	 
	SELECT DISTINCT A.patient_id,a.maxdate, b.maxdate As BM_DATE,B.enddate , B.aed, A.lookback_Present   INTO #BM  FROM [$(TargetSchema)].[app_tds_cohort] A
	LEFT JOIN [$(TargetSchema)].[app_tds_cohort] B
	ON A.PAtient_id =  B.PAtient_id
	AND  A.maxdate > b.maxdate
	AND b.Enddate  > DATEADD(dd,-365,a.maxdate)  
	--ORDER BY 2

	IF OBJECT_ID('tempdb..#CBM') IS NOT NULL
	DROP TABLE #CBM

	SELECT * INTO #CBM  FROM #BM
	WHERE  enddate < DATEADD(dd,90,maxdate) 


		
	IF OBJECT_ID('tempdb..#markcancelled') IS NOT NULL
	DROP TABLE #markcancelled

	SELECT DISTINCT  A.patient_id,a.maxdate,b.BM_Date,b.enddate ,B.aed,a.lookback_Present  INTO #markcancelled FROM #BM  a
	LEFT JOIN #CBM b
	ON A.patient_id = B.patient_id
	AND a.maxdate > B.maxdate
	--ORDER BY a.maxdate				
	
	IF OBJECT_ID('tempdb..#FINAL_BM') IS NOT NULL
	DROP TABLE #FINAL_BM

	SELECT * INTO #FINAL_BM FROM #BM
	EXCEPT
	SELECT * FROM #markcancelled
	--order by 2

	IF OBJECT_ID('tempdb..#FINAL_CBM') IS NOT NULL
	DROP TABLE #FINAL_CBM

	SELECT * INTO #FINAL_CBM FROM #CBM
	EXCEPT
	SELECT * FROM #MARKCANCELLED
	--order by 2



	IF OBJECT_ID('tempdb..#maintbl') IS NOT NULL
	DROP TABLE #maintbl

	SELECT DISTINCT  patient_id,maxdate,lookback_Present INTO #maintbl FROM [$(TargetSchema)].[app_tds_cohort]

	Print 'Populate #finalStatus_Regimen'
	Print Getdate()

	IF OBJECT_ID('tempdb..#finalStatus_Regimen') IS NOT NULL
	DROP TABLE #finalStatus_Regimen

 
	SELECT *,               	CASE WHEN BM_AED_COUNT = 0 THEN
														   CASE WHEN  SET_S_COUNT = MATCHED_AED_S_PAST THEN  'Restart_After_365_Days'
											   			   ELSE  'New'
														   END
									ELSE	
												 CASE WHEN CBM_AED_COUNT  = 0 THEN 'Add'
													  ELSE  
														  CASE WHEN  SET_S_COUNT = MATCHED_AED_S_CBM THEN  'Restart_Past_AED_treatment'
															   ELSE 'Switch'
															   END
													   END
									END AS Treatment_status INTO #finalStatus_Regimen
	FROM (
	SELECT A.*,	COUNT(DISTINCT SET_S) AS SET_S_COUNT,
				COUNT(DISTINCT BM.AED) AS BM_AED_COUNT , 
				COUNT(DISTINCT CBM.AED) AS CBM_AED_COUNT ,
				COUNT(DISTINCT PAST_AED.PAST_AED) AS MATCHED_AED_S_PAST , 
				COUNT(DISTINCT CBM_MATCHED_AED.MATCHED_AED) AS MATCHED_AED_S_CBM
				
	FROM #MainTbl A
	LEFT JOIN #set_s B
	ON A.Patient_id =B.patient_id
	AND A.MAxDate = B.MaxDate
	LEFT JOIN #FINAL_BM BM
	ON A.Patient_id =BM.patient_id
	AND A.MAxDate = BM.MaxDate
	LEFT JOIN #FINAL_CBM CBM
	ON A.Patient_id =CBM.patient_id
	AND A.MAxDate = CBM.MaxDate
	LEFT JOIN (SELECT DISTINCT A.patient_id,A.maxdate, B.SET_S AS PAST_AED FROM #set_s A
				LEFT JOIN #set_S B
				ON A.patient_id =B.Patient_id
				AND A.maxdate > B.maxdate
			  ) PAST_AED
	ON B.Patient_id = PAST_AED.patient_id
	AND B.MAxDate = PAST_AED.MaxDate
	AND B.SET_S = PAST_AED.PAST_AED
	LEFT JOIN (select distinct A.patient_id,A.maxdate, B.AED AS MATCHED_AED from #set_s A
				LEFT JOIN #FINAL_CBM B
				ON A.patient_id =	B.Patient_id
				AND A.maxdate	=	B.maxdate
				AND A.SET_S		=	B.aed
			  ) CBM_MATCHED_AED
	ON A.Patient_id =	CBM_MATCHED_AED.Patient_id
	AND A.maxDate	=	CBM_MATCHED_AED.MaxDate
	GROUP BY A.patient_id,a.maxdate,a.lookback_Present

	) A
	--ORDER BY Patient_id,maxdate

	
	UPDATE	[$(TargetSchema)].[app_tds_cohort] 
	SET		[$(TargetSchema)].[app_tds_cohort].Treatment_Status = #finalStatus_Regimen.Treatment_Status
	FROM	#finalStatus_Regimen
	WHERE [$(TargetSchema)].[app_tds_cohort].patient_ID = #finalStatus_Regimen.Patient_ID
	AND [$(TargetSchema)].[app_tds_cohort].maxdate = #finalStatus_Regimen.maxdate

--	select * from [$(TargetSchema)].[app_tds_cohort] where Treatment_Status is null

-------------------------------------------------------------------------- Treatment status End Event Starts  -------------------------------------------------------------------
	Print 'Populate #EndEvent_Regimen'
	Print Getdate()

	IF OBJECT_ID('tempdb..#EndEvent_Regimen') IS NOT NULL
	DROP TABLE #EndEvent_Regimen

	SELECT		A.patient_id,
						A.enddate,
						CASE WHEN SUM(CASE WHEN a.enddate BETWEEN b.maxdate AND B.enddate THEN 1 ELSE 0 END) >0 THEN 'Discontinue' ELSE 'Off_AED' END AS EndTreatmentChangeEvent 
						INTO #EndEvent_Regimen
			FROM		[$(TargetSchema)].[app_tds_cohort] A
			LEFT JOIN	(	
							SELECT	* 
							FROM	[$(TargetSchema)].[app_tds_cohort]
						) B
			ON			A.patient_id	= B.patient_id 
			AND			A.enddate		< B.enddate
			GROUP BY	A.patient_id,
						a.enddate


	UPDATE  [$(TargetSchema)].[app_tds_cohort] 
	SET		[$(TargetSchema)].[app_tds_cohort].EndTreatmentChangeEvent = #EndEvent_Regimen.EndTreatmentChangeEvent
	FROM	#EndEvent_Regimen 
	WHERE	[$(TargetSchema)].[app_tds_cohort].Patient_id = #EndEvent_Regimen.patient_id
	AND		[$(TargetSchema)].[app_tds_cohort].Enddate = #EndEvent_Regimen.Enddate 


	
----------------------------------------------------------------------------Early Late-----------------------------------------------------------

UPDATE	[$(TargetSchema)].[app_tds_cohort]
SET		Early_Late = CASE WHEN MAxdate < DATEADD(dd,90,First_Encounter) THEN 'Early' 
			 WHEN MAxdate > DATEADD(dd,-90,Last_Encounter) THEN 'Late'
			 ELSE Treatment_Status 
			 END
			 			 
--------------------------------------------------------Index Date Start----------------------------------------------------

Print 'Populate #12MonthEligibility_regimen'
Print Getdate()			

IF OBJECT_ID('tempdb..#12MonthEligibility_regimen') IS NOT NULL
DROP TABLE #12MonthEligibility_regimen
		 
SELECT	A.patient_id ,A.maxdate,SUM(CASE WHEN Patient_Eligibility = 'Y' THEN 1 ELSE 0 END) AS Flag INTO #12MonthEligibility_regimen
FROM [$(TargetSchema)].[app_tds_cohort](NOLOCK) A
JOIN [$(TargetSchema)].[app_tds_int_Eligibility_Data] B
ON A.patient_id = B.Patient_id
AND	B.month_id BETWEEN 	REPLACE(SUBSTRING(CAST(A.maxdate AS Varchar),1,7),'-','')  AND REPLACE(SUBSTRING(CAST(dateadd(yy,1,A.maxdate) AS VARCHAR),1,7),'-','') 
GROUP BY A.patient_id ,A.maxdate
--ORDER BY 1,2

UPDATE [$(TargetSchema)].[app_tds_cohort]
SET [$(TargetSchema)].[app_tds_cohort].Twelve_Months_Eligible = CASE WHEN #12MonthEligibility_regimen.flag = 13 THEN 1 ELSE 0 END
FROM #12MonthEligibility_regimen
WHERE [$(TargetSchema)].[app_tds_cohort].PAtient_id = #12MonthEligibility_regimen.patient_id
AND [$(TargetSchema)].[app_tds_cohort].maxdate =	#12MonthEligibility_regimen.maxdate

Print 'Populate #3MonthEligibility_Regimen'
Print Getdate()			

IF OBJECT_ID('tempdb..#3MonthEligibility_Regimen') IS NOT NULL
DROP TABLE #3MonthEligibility_Regimen

SELECT	A.patient_id ,A.maxdate,SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) AS Flag   INTO #3MonthEligibility_Regimen
FROM [$(TargetSchema)].[app_tds_cohort](NOLOCK) A
JOIN [$(TargetSchema)].[app_tds_int_Eligibility_Data] B
ON A.patient_id = B.Patient_id
AND	B.month_id Between REPLACE(SUBSTRING(CAST(dateadd(mm,-3,A.maxdate) AS VARCHAR),1,7),'-','')  AND REPLACE(SUBSTRING(CAST(A.maxdate AS VARCHAR),1,7),'-','') 
GROUP BY A.patient_id ,A.maxdate
--ORDER BY 1,2


UPDATE	[$(TargetSchema)].[app_tds_cohort]
SET		[$(TargetSchema)].[app_tds_cohort].Three_Months_Eligible = CASE WHEN #3MonthEligibility_Regimen.flag = 4 THEN 1 ELSE 0 END
FROM	#3MonthEligibility_Regimen
WHERE	[$(TargetSchema)].[app_tds_cohort].PAtient_id	=	#3MonthEligibility_Regimen.patient_id
AND		[$(TargetSchema)].[app_tds_cohort].maxdate		=	#3MonthEligibility_Regimen.maxdate



UPDATE [$(TargetSchema)].[app_tds_cohort] 
SET [$(TargetSchema)].[app_tds_cohort].Stable_Eligible = CASE WHEN aed_days >=31 THEN 1 ELSE 0 END

Print 'Populate #Index_Date'
Print Getdate()			

IF OBJECT_ID('tempdb..#Index_Date') IS NOT NULL
DROP TABLE #Index_Date
BEGIN
SELECT patient_id,maxdate AS INDEX_DATE INTO #Index_Date
FROM [$(TargetSchema)].[app_tds_cohort](NOLOCK) 
WHERE  Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') 
/* AND Twelve_Months_Eligible =1 AND Three_Months_Eligible = 1 */  AND  Stable_Eligible =1 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1 AND LEN(AED) = 3
AND		AED IN (
				SELECT DISTINCT Abbreviation FROM [$(TargetSchema)].[std_ref_aed_abbreviation]
				WHERE			AED IN (	'LEVETIRACETAM','LAMOTRIGINE','VALPROATE SODIUM','CARBAMAZEPINE','OXCARBAZEPINE','TOPIRAMATE','PHENYTOIN','LACOSAMIDE','ZONISAMIDE'
											,'CLOBAZAM','PHENOBARBITAL','PREGABALIN','ETHOSUXIMIDE','ESLICARBAZEPINE ACETATE','PERAMPANEL','RUFINAMIDE'
									)
				)
GROUP BY patient_id,maxdate
END



UPDATE	[$(TargetSchema)].[app_tds_cohort]
SET		[$(TargetSchema)].[app_tds_cohort].Index_Date =  #Index_Date.INDEX_DATE
FROM	#Index_Date
WHERE	[$(TargetSchema)].[app_tds_cohort].Patient_id	= #Index_Date.Patient_id
AND		[$(TargetSchema)].[app_tds_cohort].maxdate		= #Index_Date.INDEX_DATE

--------------------------------------------------------------------------Treatment Outcome-------------------------------------------------------------------

--Logic at Regimen Level
UPDATE [$(TargetSchema)].[app_tds_cohort]
SET Outcome_variable =  CASE WHEN AED_DAYS >= 365 THEN 'Success' 
							  WHEN AED_DAYS < 365 THEN 'Failure'
							  END
WHERE INDEX_DATE IS NOT NULL

/*

			IF OBJECT_ID('tempdb..#Treatment_Outcome_Regimen') IS NOT NULL
			DROP TABLE #Treatment_Outcome_Regimen

			
			SELECT distinct A.patient_id,A.index_date,CASE WHEN B.cnt IS NULL AND AED_Days >=365 THEN 0 
														   WHEN B.cnt IS NULL AND AED_Days < 365 THEN 1
														   ELSE B.cnt END AS Cnt INTO #Treatment_Outcome_Regimen 
			FROM		[$(TargetSchema)].[app_tds_cohort] A
			LEFT JOIN (
						SELECT A.patient_id,B.index_date,COUNT(DISTINCT A.Patient_id)    AS Cnt 
						FROM (
								SELECT patient_id,maxdate,Treatment_status  FROM [$(TargetSchema)].[app_tds_cohort]   
								UNION
								SELECT patient_id,Enddate,EndTreatmentChangeEvent FROM [$(TargetSchema)].[app_tds_cohort]  WHERE EndTreatmentChangeEvent <> 'Off_AED'  
							) A
						JOIN [$(TargetSchema)].[app_tds_cohort] B
						ON a.patient_id = B.patient_id
						AND A.maxdate BETWEEN DATEADD(dd,1,B.INDEX_DATE) AND DATEADD(DD,365,B.INDEX_DATE)
						GROUP BY A.patient_id,B.index_date
						) B
			ON A.Patient_id =B.patient_id
			AND a.index_date=B.index_date
			WHERE a.index_date IS NOT NULL 
			ORDER BY 2,3 

		
			UPDATE	[$(TargetSchema)].[app_tds_cohort]
			SET		[$(TargetSchema)].[app_tds_cohort].Treatment_Failure = #Treatment_Outcome_Regimen.cnt
			FROM	#Treatment_Outcome_Regimen
			WHERE	[$(TargetSchema)].[app_tds_cohort].patient_id	=	#Treatment_Outcome_Regimen.patient_id
			AND		[$(TargetSchema)].[app_tds_cohort].index_date	=	#Treatment_Outcome_Regimen.index_date	

			*/ -- Old logic used at molecule level 

-----------------------------------------------------------------Zip3_Zip1_State Population-----------------------------------------------------
Print 'Populate #ZipInfo'
Print Getdate()			

IF OBJECT_ID('tempdb..#ZipInfo') IS NOT NULL
DROP TABLE #ZipInfo

SELECT A.patient_id,A.maxdate,PP.ZIP3 AS Zip3_Code,Zip.[Zip1_code],Zip.[State],[YOB] INTO #ZipInfo
FROM [$(TargetSchema)].[app_tds_cohort] A
LEFT JOIN [$(SrcSchema)].[src_ref_patient](nolock)  PP
ON  A.Patient_id =PP.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_USPS_zip3zip1codes](nolock) Zip
ON PP.Zip3 =Zip.Zip3_Code



UPDATE [$(TargetSchema)].[app_tds_cohort]
SET  [$(TargetSchema)].[app_tds_cohort].Zip3	 = #ZipInfo.Zip3_Code
	,[$(TargetSchema)].[app_tds_cohort].Zip1	 = #ZipInfo.Zip1_code 
	,[$(TargetSchema)].[app_tds_cohort].State = #ZipInfo.[State]
FROM #ZipInfo 
WHERE [$(TargetSchema)].[app_tds_cohort].patient_id = #ZipInfo.patient_id
AND [$(TargetSchema)].[app_tds_cohort].maxdate = #ZipInfo.maxdate


---------------------------------------------Age----------------------------------------
Print 'Populate age'
Print Getdate()	

UPDATE [$(TargetSchema)].[app_tds_cohort] 
SET  [$(TargetSchema)].[app_tds_cohort].Age = CAST(SUBSTRING(CAST([$(TargetSchema)].[app_tds_cohort].Maxdate AS VARCHAR),1,4)  AS INT )- YOB 
FROM #ZipInfo 
WHERE [$(TargetSchema)].[app_tds_cohort].patient_id = #ZipInfo.patient_id
AND [$(TargetSchema)].[app_tds_cohort].maxdate = #ZipInfo.maxdate



--------------------------------------------Q_Rx_Elg-----------------------------------------------

Print 'Populate Q_Rx_Elg'
Print Getdate()	



--##Identifying quarters for each record of the patient 

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
DROP TABLE #Temp1

SELECT *,SUBSTRING(CAST(MONTH_ID AS VARCHAR),1,4) AS YEAR_, SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) AS MONTH_ ,
                     CASE WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('01','02','03') THEN 'Q1'--,
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('04','05','06') THEN 'Q2'--,
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('07','08','09') THEN 'Q3'--,
                           ELSE 'Q4' END AS QUARTER_INFO
INTO #Temp1
FROM [$(SrcSchema)].[src_ptnt_eligibility] 


IF OBJECT_ID('tempdb..#IndexQuarterNo') IS NOT NULL
DROP TABLE #IndexQuarterNo

select DISTINCT A.PATIENT_ID,B.Index_Date,REPLACE(SUBSTRING(CAST(B.Index_Date AS Varchar),1,7),'-','') AS MonthID,CAST([Sr_No_Q] AS INT) AS [Sr_No_Q]
INTO #IndexQuarterNo
from #temp1 A
JOIN [$(TargetSchema)].[app_tds_cohort] B
ON A.patient_id = B.patient_id
AND A.YEAR_+A.Month_=REPLACE(SUBSTRING(CAST(B.Index_Date AS Varchar),1,7),'-','')
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
WHERE B.Index_Date IS NOT NULL


---------------------------------------------------------------------------------------

IF OBJECT_ID('tempdb..#preIndex') IS NOT NULL
DROP TABLE #preIndex

select A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,CAST(C.[Sr_No_Q] AS INT) AS [Sr_No_Q] ,CASE WHEN SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS QUARTER_RX_ELIG
INTO #preIndex
from #temp1 A
JOIN  #IndexQuarterNo B
ON A.patient_id =B.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
where C.Sr_No_Q BETWEEN B.Sr_No_Q-4 AND B.Sr_No_Q
GROUP BY A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,C.[Sr_No_Q]


IF OBJECT_ID('tempdb..#postIndex') IS NOT NULL
DROP TABLE #postIndex

select A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,CAST(C.[Sr_No_Q] AS INT) AS [Sr_No_Q] ,CASE WHEN SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS QUARTER_RX_ELIG
INTO #postIndex
from #temp1 A
JOIN  #IndexQuarterNo B
ON A.patient_id =B.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
where C.Sr_No_Q BETWEEN B.Sr_No_Q AND B.Sr_No_Q+4
GROUP BY A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,C.[Sr_No_Q]


------------------------------------------------------------------------------------------

Print 'Populate #Elg_4_5_Q'
Print Getdate()

IF OBJECT_ID('tempdb..#Elg_4_5_Q_Pre') IS NOT NULL
DROP TABLE #Elg_4_5_Q_Pre

select patient_id,Index_date ,MAX(Sr_No_Q) Maxx ,MIN(Sr_No_Q) Minn,CASE WHEN MAX(Sr_No_Q)-3 <0 THEN 0 ELSE MAX(Sr_No_Q)-3  END as Minus4 
,CASE WHEN MAX(Sr_No_Q)-4 <0 THEN 0 ELSE MAX(Sr_No_Q)-4 END  Minus5 
,CASE WHEN MIN(Sr_No_Q) <= CASE WHEN MAX(Sr_No_Q)-3 <0 THEN 0 ELSE MAX(Sr_No_Q)-3  END THEN  1 ELSE 0 END AS Elg_4 
,CASE WHEN MIN(Sr_No_Q) <= CASE WHEN MAX(Sr_No_Q)-4 <0 THEN 0 ELSE MAX(Sr_No_Q)-4  END THEN  1 ELSE 0 END AS Elg_5
,COUNT(*) cuntt , SUM(QUARTER_RX_ELIG) AS Summ
INTO #Elg_4_5_Q_Pre
from #preIndex 
GROUP BY  patient_id,Index_date




IF OBJECT_ID('tempdb..#Elg_4_5_Q_Post') IS NOT NULL
DROP TABLE #Elg_4_5_Q_Post

select patient_id,Index_date ,MAX(Sr_No_Q) Maxx ,MIN(Sr_No_Q) Minn,MIN(Sr_No_Q)+3 Plus4 
, MIN(Sr_No_Q)+4 Plus5 
,CASE WHEN MAX(Sr_No_Q) >= MIN(Sr_No_Q)+3 THEN  1 ELSE 0 END AS Elg_4 
,CASE WHEN MAX(Sr_No_Q) >= MIN(Sr_No_Q)+4 THEN  1 ELSE 0 END AS Elg_5
,COUNT(*) cuntt , SUM(QUARTER_RX_ELIG) AS Summ
INTO #Elg_4_5_Q_Post
from #postIndex 
GROUP BY  patient_id,Index_date


--------------------------------------------------------------------------------------------------------

--CASE 2 : Patient with 5 quarter eligibility  without ignoring missing values

Print 'Populate #Patient_5Q_Final_List'
Print Getdate()


IF OBJECT_ID('tempdb..#Patient_5Q_Final_List_Pre') IS NOT NULL
DROP TABLE #Patient_5Q_Final_List_Pre

SELECT DISTINCT A.patient_id,A.Index_Date
INTO #Patient_5Q_Final_List_Pre
FROM  #Elg_4_5_Q_Pre A
WHERE Elg_5 = 1 AND Cuntt = 5 AND SUMM = 5


IF OBJECT_ID('tempdb..#Patient_5Q_Final_List_Post') IS NOT NULL
DROP TABLE #Patient_5Q_Final_List_Post

SELECT DISTINCT A.patient_id,A.Index_Date
INTO #Patient_5Q_Final_List_Post
FROM  #Elg_4_5_Q_Post A
WHERE Elg_5 = 1 AND Cuntt = 5 AND SUMM = 5

-----------------------------------------------------------------------------------------------------


SELECT A.patient_id,A.Index_Date,CASE WHEN B.Patient_id IS NOT NULL AND C.Patient_id IS NOT NULL THEN 1 ELSE  0 END As [Q_Rx_Elg]
INTO #FinaiList_Q_Rx_Elg
FROM [$(TargetSchema)].[app_tds_cohort]  A
LEFT JOIN #Patient_5Q_Final_List_Pre B
ON A.patient_id =B.patient_id
AND A.Index_Date = B.Index_Date
LEFT JOIN #Patient_5Q_Final_List_Post C
ON A.patient_id =C.patient_id
AND A.Index_Date = C.Index_Date
WHERE A.Index_Date is not NULL 

UPDATE [$(TargetSchema)].[app_tds_cohort] 
SET  [$(TargetSchema)].[app_tds_cohort].[Q_Rx_Elg] = #FinaiList_Q_Rx_Elg.[Q_Rx_Elg]
FROM #FinaiList_Q_Rx_Elg 
WHERE [$(TargetSchema)].[app_tds_cohort].patient_id = #FinaiList_Q_Rx_Elg.patient_id
AND [$(TargetSchema)].[app_tds_cohort].Index_Date = #FinaiList_Q_Rx_Elg.Index_Date




---------------------------------------IsEpilepsy,PharmacyStability,ContinuousEligibility--------------------------
Print 'Populate IsEpilepsy,PharmacyStability,ContinuousEligibility flags'
Print Getdate()

UPDATE [$(TargetSchema)].[app_tds_cohort] 
SET [$(TargetSchema)].[app_tds_cohort].IsEpilepsy			=  CASE WHEN [$(TargetSchema)].[app_int_Patient_Profile].IsEpilepsy			  = 1 THEN 1 ELSE 0 END
,[$(TargetSchema)].[app_tds_cohort].PharmacyStability		=  CASE WHEN [$(TargetSchema)].[app_int_Patient_Profile].PharmacyStability      = 1 THEN 1 ELSE 0 END 
,[$(TargetSchema)].[app_tds_cohort].ContinuousEligibility =  CASE WHEN [$(TargetSchema)].[app_int_Patient_Profile].ContinuousEligibility  = 1 THEN 1 ELSE 0 END 
FROM [$(TargetSchema)].[app_int_Patient_Profile] 
WHERE [$(TargetSchema)].[app_tds_cohort].Patient_id = [$(TargetSchema)].[app_int_Patient_Profile].Patient_id



--------------------------------------------------------GenPop--------------------------------------------------------
Print 'Populate Genpop flags'
Print Getdate()

/* Note: Pharmacy data is not available in current pipeline hencs commented the pharmacy related filter criteria for genpop */

UPDATE [$(TargetSchema)].[app_tds_cohort] 
SET [$(TargetSchema)].[app_tds_cohort].[GenPop] = CASE WHEN IsEpilepsy = 1 AND Age >= 18 /*  AND PharmacyStability =1  AND  ContinuousEligibility = 1 AND  Q_Rx_Elg = 1 */ THEN 1 ELSE 0 END
WHERE [$(TargetSchema)].[app_tds_cohort].[Index_date] IS NOT NULL


------------------------------------------------------------------------------------------------------------------------------------
