-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create the tables for patietns cohort for FirstIndex,LastIndex,LongestIndex
-- - NOTE      :  
-- - Execution : Final table  name: [$(TargetSchema)].app_tds_int_BaselineModel_Input_FirstIndex , [$(TargetSchema)].app_tds_int_BaselineModel_Input_LastIndex , PSPA_BaselineModel_Input_LongestIndex
-- - Date      : 12-SEP-2018
-- - Change History:	Change Date				Change Description
--						
-- ---------------------------------------------------------------------- 

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate #FirstIndex'
Print Getdate()

IF OBJECT_ID('tempdb..#FirstIndex') IS NOT NULL
DROP TABLE #FirstIndex

SELECT Patient_id,MIN(Valid_Index) AS FirstIndex INTO #FirstIndex  FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]
GROUP BY Patient_id

Print 'Populate app_tds_int_BaselineModel_Input_FirstIndex'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]


SELECT	A.* 
INTO	[$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] 
FROM	[$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
JOIN	#FirstIndex B
ON		A.patient_id = B.patient_id
AND		A.Valid_Index = B.FirstIndex




IF OBJECT_ID('tempdb..#Patient_Longest_Index') IS NOT NULL
DROP TABLE #Patient_Longest_Index

SELECT DISTINCT  A.Patient_id ,Valid_Index AS Longest_Valid_Index INTO #Patient_Longest_Index 
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK)  A
JOIN (
		SELECT Patient_id ,MAX(AED_DAYS) AS MAX_AED_DAYS 
		FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK) 
		GROUP BY patient_id
	 ) B
ON		A.Patient_id	= B.patient_id
AND		A.AED_DAYS		= B.MAX_AED_DAYS


Print 'Populate app_tds_int_BaselineModel_Input_LongestIndex'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_BaselineModel_Input_LongestIndex]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_BaselineModel_Input_LongestIndex]

SELECT	A.* 
INTO	[$(TargetSchema)].[app_tds_int_BaselineModel_Input_LongestIndex] 
FROM	[$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
JOIN	#Patient_Longest_Index B
ON		A.patient_id = B.patient_id
AND		A.Valid_Index = B.Longest_Valid_Index



IF OBJECT_ID('tempdb..#LastIndex') IS NOT NULL
DROP TABLE #LastIndex

SELECT Patient_id,MAX(Valid_Index) AS LastIndex INTO #LastIndex  FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]
GROUP BY Patient_id

Print 'Populate app_tds_int_BaselineModel_Input_LastIndex'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_BaselineModel_Input_LastIndex]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_BaselineModel_Input_LastIndex]

SELECT A.* 
INTO [$(TargetSchema)].[app_tds_int_BaselineModel_Input_LastIndex] 
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
JOIN #LastIndex B
ON A.patient_id = B.patient_id
AND A.Valid_Index = B.LastIndex