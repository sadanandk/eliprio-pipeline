﻿-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Create scropts for DRE patients statistics
-- - NOTE      :   Part 1: Scripts for Q_Rx_Elg =1 
-- - Execution : 
-- - Date      : 14-Sept-2018
-- - Change History:	Change Date				Change Description
-- -					
-- ---------------------------------------------------------------------- 

SET NOcount ON
--:setvar Schema SHA_Pipeline_Sample
--:Setvar SrcSchema SHA_test_100pt

--******************************************* START: Confluence Page Graphs with Q_Rx_Elg**************************************************--

SELECT COUNT(DISTINCT patient_id) [Patients in Raw Dataset:] from [$(SrcSchema)].[src_ref_patient]
PRINT ''

PRINT 'Filtering Criterias for TDS Cohort:'
PRINT ''

select '1. Patient with Epilepsy and at least one AED ' Criteria,
COUNT(Distinct T.Patient_id) [#PatientCount]
from [$(TargetSchema)].app_tds_cohort T
inner join [$(TargetSchema)].[app_int_Patient_Profile] PP on T.patient_id = PP.patient_id
WHERE T.[IsEpilepsy] =1  and [First_AED] is not null
union
select '2. Number of AED regimen failures with first valid index date',
COUNT(Distinct T.Patient_id) 
from [$(TargetSchema)].app_tds_cohort T
inner join [$(TargetSchema)].[app_int_Patient_Profile] PP on T.patient_id = PP.patient_id
inner join  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] F on T.patient_id = F.patient_id
WHERE T.[IsEpilepsy] =1  and [First_AED] is not null and F.Outcome_variable='Failure'
union
select '3. Number of AED regimen Success with first valid index date',
COUNT(Distinct T.Patient_id) 
from [$(TargetSchema)].app_tds_cohort T
inner join [$(TargetSchema)].[app_int_Patient_Profile] PP on T.patient_id = PP.patient_id
inner join  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex] F on T.patient_id = F.patient_id
WHERE T.[IsEpilepsy] =1  and [First_AED] is not null and F.Outcome_variable='Success'
PRINT ''

PRINT ''
PRINT 'TDS Cohort Statistics - Summary:'
PRINT ''

select count(distinct patient_id) [Total Patients in TDS Cohort]
from 
(
select patient_id, maxdate, 
case when Twelve_Months_Eligible = 1 
and Three_Months_Eligible = 1 
and Stable_Eligible = 1 
and treatment_status in ('ADD', 'Switch','New') 
and early_late not in ('Early','Late') 
and lookback_present = 1 
then maxdate else null end as index_date 
from [$(TargetSchema)].app_tds_cohort
)A

declare @Total_Patient int
set @Total_Patient = 
(select count(distinct patient_id) [Total Patients in TDS Cohort]
from 
(
select patient_id, maxdate, 
case when Twelve_Months_Eligible = 1 
and Three_Months_Eligible = 1 
and Stable_Eligible = 1 
and treatment_status in ('ADD', 'Switch','New') 
and early_late not in ('Early','Late') 
and lookback_present = 1 
then maxdate else null end as index_date 
from [$(TargetSchema)].app_tds_cohort
)A
)

IF OBJECT_ID('tempdb..#distribution_QRX') IS NOT NULL 
DROP TABLE #distribution_QRX

SELECT T.patient_id,age, gender, outcome_variable
INTO #distribution_QRX
from [$(TargetSchema)].app_tds_cohort T
inner join[$(TargetSchema)].[app_int_Patient_Profile] P on t.patient_id = P.patient_id
where index_date is not null




PRINT ''
Print 'Cohort Gender Distribution:'
PRINT ''

SELECT Gender= case when gender='M' then 'Male' 
					when gender='F' then 'Female' 
					else Gender
				end
, count(distinct PAtient_id) [#PatientCount]
, (COUNT(DISTINCT patient_id))*100.0 /@Total_Patient   AS [%TotalCohortPatients]
from #distribution_QRX
group by gender

PRINT ''

PRINT ''
Print 'DRE Cohort Gender Distribution:'
PRINT ''


PRINT ''

PRINT 'Cohort Age Distribution:'
PRINT ''

IF OBJECT_ID('tempdb..#pop') IS NOT NULL 
DROP TABLE #pop

SELECT Population = CASE WHEN age between 0 AND 64 THEN 'Young (0-64)'
						 WHEN age >=65 THEN 'Senior(65 &Above)'
					END
, patient_id
into #pop
from #distribution_QRX

select [Population], count(distinct patient_id) [#PatientCount],(COUNT(DISTINCT patient_id))*100.0 /@Total_Patient   AS [%TotalCohortPatients]
from #pop
group by [Population]