-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create features related to demographic for TDS AED model  
-- - NOTE      :  Step 1 : Create app_tds_feature_IndexDate_Demographic table which has all the features related to Demographics.
-- - Execution : Final table  name: app_tds_feature_IndexDate_Demographic
-- - Date      : 22-JAN-2017
-- - Change History:	Change Date				Change Description
--						05-Jul-2018				Re run as per Iteration_2
-- ---------------------------------------------------------------------- 

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_IndexDate_Demographic]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_IndexDate_Demographic]

SELECT		A.patient_id,
			A.Valid_Index,
			Gender,
			A.Age, 
			A.Age + (A.Age*A.Age) AS AgePlusAge2 ,
			CAST(CASE WHEN Gender = 'F' AND A.Age BETWEEN 18 AND 45 THEN 1 ELSE 0 END AS BIT) AS isInChildbearingAge, 
			[State] AS Index_State ,
			Zip1 AS Index_Zip1
INTO       [$(TargetSchema)].[app_tds_feature_IndexDate_Demographic]
FROM		[$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](nolock) A
LEFT JOIN	[$(TargetSchema)].[app_tds_cohort](nolock)  B
ON			A.Patient_id =B.patient_id
AND			A.Valid_Index =  B.index_date



/*
   SELECT A.patient_id,A.Valid_Index,C.abbreviation, 1 AS COUNTT
	FROM PSPA_BaselineModel_Input_IndxDate A 	 WITH(NOLOCK)
	LEFT JOIN [dbo].PSPA_PHASE_III_OUTPUT_TABLE_IMS_Filtered30Day B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.maxdate <= DATEADD(DD,-1,A.Valid_Index) AND B.enddate >= DATEADD(DD,-365,A.Valid_Index) 
	LEFT JOIN [dbo].[AED_Abbreviation] C WITH(NOLOCK)
	ON B.AED = C.AED
*/