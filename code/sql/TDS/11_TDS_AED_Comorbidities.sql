-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create set 2 feature table for Other comorbidities for AED TDS  patients
-- - NOTE      : 
--				 
-- - Execution : 
-- - Date      : 10-Sep-2018		
-- - Change History:	Change Date				Change Description
-- -				   	
	-- ---------------------------------------------------------------------- 

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate #AFFECDISOR'
Print Getdate()

	-- Affective disorder comorbidity. Categorical.(past 365 days)
	IF OBJECT_ID('tempdb..#AFFECDISOR') IS NOT NULL
    DROP TABLE #AFFECDISOR

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  AFFECDISOR
	INTO #AFFECDISOR
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE ([icd9cm] >='29620' AND  [icd9cm] <='29640')  OR ([icd9cm] like '311%' or  [icd9cm] like '30000' or  [icd9cm] like '30002' OR  [icd9cm] like '30009'   OR  [icd9cm] like '30981')
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE ([icd9cm] >='29620' AND  [icd9cm] <='29640')  OR ([icd9cm] like '311%' or  [icd9cm] like '30000' or  [icd9cm] like '30002' OR  [icd9cm] like '30009'   OR  [icd9cm] like '30981')
					 )
	GROUP BY A.patient_id,A.valid_index


	
--Autoimmune disorder comorbidity. Categorical. AUTOIMMUNE
--(696* or 555* or 714 or 2830* or 57142*) and code length = 5

	Print 'Populate #AUTOIMMUNE'
	Print Getdate()

	IF OBJECT_ID('tempdb..#AUTOIMMUNE') IS NOT NULL
    DROP TABLE #AUTOIMMUNE

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  AUTOIMMUNE
	INTO #AUTOIMMUNE
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '696%' or  [icd9cm] like '555%' or  [icd9cm] like '714%' OR  [icd9cm] like '2830%'   OR ( [icd9cm] like '57142'  AND LEN([icd9cm]) =5)
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '696%' or  [icd9cm] like '555%' or  [icd9cm] like '714%' OR  [icd9cm] like '2830%'   OR ( [icd9cm] like '57142'  AND LEN([icd9cm]) =5)
					 )
	GROUP BY A.patient_id,A.valid_index

--Cardio vascular comorbidity. Categorical. :CARDIO
--390* to 459*

	Print 'Populate #CARDIO'
	Print Getdate()

	IF OBJECT_ID('tempdb..#CARDIO') IS NOT NULL
    DROP TABLE #CARDIO

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  CARDIO
	INTO #CARDIO
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] >='390' AND  [icd9cm] <='4599'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] >='390' AND  [icd9cm] <='4599'
					 )
	GROUP BY A.patient_id,A.valid_index



--Cotreatment with diab within last 30 days. Categorical. : DIAB
--250*

	Print 'Populate #DIAB'
	Print Getdate()

	IF OBJECT_ID('tempdb..#DIAB') IS NOT NULL
    DROP TABLE #DIAB

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  DIAB
	INTO #DIAB
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-30,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '250%'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '250%'
					 )
	GROUP BY A.patient_id,A.valid_index

--Liver disorder comorbidity. Categorical. : LIVER
--571*
	Print 'Populate #LIVER'
	Print Getdate()

	IF OBJECT_ID('tempdb..#LIVER') IS NOT NULL
    DROP TABLE #LIVER

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  LIVER
	INTO #LIVER
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '571%'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '571%'
					 )
	GROUP BY A.patient_id,A.valid_index

--Mental retardation comorbidity. Categorical.: MENTRETRD
--317* to 319*

	IF OBJECT_ID('tempdb..#MENTRETRD') IS NOT NULL
    DROP TABLE #MENTRETRD

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  MENTRETRD
	INTO #MENTRETRD
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] >='317' AND  [icd9cm] <='319'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] >='317' AND  [icd9cm] <='319'
					 )
	GROUP BY A.patient_id,A.valid_index


--Neuro disease comorbidity. Categorical. : NEUROCMD
-- CCS diagnosis codes match: 109, 55, 245, 78, 95, 84, 79, 81, 82, 85, 111, 112, 113 : 

	Print 'Populate #NEUROCMD'
	Print Getdate()

    IF OBJECT_ID('tempdb..#NEUROCMD') IS NOT NULL
    DROP TABLE #NEUROCMD

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  NEUROCMD
	INTO #NEUROCMD
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						select Distinct ICD_CODE from [$(TargetSchema)].std_REF_ICD_Code_All WHERE [CCS CATEGORY] in (109, 55, 245, 78, 95, 84, 79, 81, 82, 85, 111, 112, 113)
					 )
	GROUP BY A.patient_id,A.valid_index


--Obesiity comorbidity. Categorical. : OBESITY
--2780*

	Print 'Populate #OBESITY'
	Print Getdate()

    IF OBJECT_ID('tempdb..#OBESITY') IS NOT NULL
    DROP TABLE #OBESITY

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  OBESITY
	INTO #OBESITY
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '2780%'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '2780%'
					 )
	GROUP BY A.patient_id,A.valid_index


-- Osteo comorbidity. Categorical. OSTEO	
--7330*	


	Print 'Populate #OSTEO'
	Print Getdate()

    IF OBJECT_ID('tempdb..#OSTEO') IS NOT NULL
    DROP TABLE #OSTEO

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  OSTEO
	INTO #OSTEO
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '7330%'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '7330%'
					 )
	GROUP BY A.patient_id,A.valid_index

--Other mental disorders. Categorical.	: OTHRMENT
--29000* to 31299*

	Print 'Populate #OTHRMENT'
	Print Getdate()

    IF OBJECT_ID('tempdb..#OTHRMENT') IS NOT NULL
    DROP TABLE #OTHRMENT

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  OTHRMENT
	INTO #OTHRMENT
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] >='29000' AND  [icd9cm] <='31299'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] >='29000' AND  [icd9cm] <='31299'
					 )
	GROUP BY A.patient_id,A.valid_index

--POLYPHYRINMETABOLISM
--2771*

    IF OBJECT_ID('tempdb..#POLYPHYRINMETABOLISM') IS NOT NULL
    DROP TABLE #POLYPHYRINMETABOLISM

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  POLYPHYRINMETABOLISM
	INTO #POLYPHYRINMETABOLISM
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '2771%'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '2771%'
					 )
	GROUP BY A.patient_id,A.valid_index

--RENALINSUFF
--(403* or 404*) or (580* to 586*)

    IF OBJECT_ID('tempdb..#RENALINSUFF') IS NOT NULL
    DROP TABLE #RENALINSUFF

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  RENALINSUFF
	INTO #RENALINSUFF
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE ([icd9cm] like '403%' OR [icd9cm] like '404%' ) OR ([icd9cm] >='580' AND  [icd9cm] <='586')
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE ([icd9cm] like '403%' OR [icd9cm] like '404%' ) OR ([icd9cm] >='580' AND  [icd9cm] <='586')
					 )
	GROUP BY A.patient_id,A.valid_index



--SEIZURE345

	Print 'Populate #SEIZURE345'
	Print Getdate()

    IF OBJECT_ID('tempdb..#SEIZURE345') IS NOT NULL
    DROP TABLE #SEIZURE345

	SELECT A.patient_id ,A.Valid_index,CASE WHEN SEIZURE345 IS NOT NULL THEN 1 ELSE 0 END AS  SEIZURE345
	INTO #SEIZURE345
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN (
				SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  SEIZURE345 
				FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
				LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
				ON A.patient_id=B.patient_id
				AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
				WHERE  DX_Codes LIKE '345%' OR  DX_Codes LIKE 'G40%' 
				GROUP BY A.patient_id,A.valid_index
			 ) B
	ON  A.patient_id  = B.Patient_id
	AND A.Valid_Index = B.Valid_index


--SEIZURE345OR78039

    IF OBJECT_ID('tempdb..#SEIZURE345OR78039') IS NOT NULL
    DROP TABLE #SEIZURE345OR78039

	SELECT A.patient_id ,A.Valid_index,CASE WHEN SEIZURE345OR78039 IS NOT NULL THEN 1 ELSE 0 END AS  SEIZURE345OR78039
	INTO #SEIZURE345OR78039
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN (
				SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  SEIZURE345OR78039 
				FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
				LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
				ON A.patient_id=B.patient_id
				AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
				WHERE  DX_Codes LIKE '345%' OR  DX_Codes LIKE 'G40%' OR  DX_Codes LIKE '78039' OR  DX_Codes LIKE 'R569'
				GROUP BY A.patient_id,A.valid_index
			 ) B
	ON  A.patient_id  = B.Patient_id
	AND A.Valid_Index = B.Valid_index


--SERMENTILL
--(295* and code length =5 and last two digits != 50) or (29889 to 29700) or (29600 to 29620, not including 29620) or (29640 to 29680)

	Print 'Populate #SERMENTILL'
	Print Getdate()

	IF OBJECT_ID('tempdb..#SERMENTILL') IS NOT NULL
    DROP TABLE #SERMENTILL

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  SERMENTILL
	INTO #SERMENTILL
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE  ([icd9cm] LIKE '295%' AND LEN([icd9cm]) =5  AND SUBSTRING([icd9cm],4,2) <> 50) OR ([icd9cm] >='29700' AND  [icd9cm] <='29889') OR ([icd9cm] >='29600' AND  [icd9cm] <'29620') OR ([icd9cm] >='29640' AND  [icd9cm] <='29680') 
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE  ([icd9cm] LIKE '295%' AND LEN([icd9cm]) =5  AND SUBSTRING([icd9cm],4,2) <> 50) OR ([icd9cm] >='29700' AND  [icd9cm] <='29889') OR ([icd9cm] >='29600' AND  [icd9cm] <'29620') OR ([icd9cm] >='29640' AND  [icd9cm] <='29680') 
					 )
	GROUP BY A.patient_id,A.valid_index

--SLEEP
--7805*

	Print 'Populate #SLEEP'
	Print Getdate()

	IF OBJECT_ID('tempdb..#SLEEP') IS NOT NULL
    DROP TABLE #SLEEP

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  SLEEP
	INTO #SLEEP
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '7805%'
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE [icd9cm] like '7805%'
					 )
	GROUP BY A.patient_id,A.valid_index


--SUBSTABUSE
--((291* or 292*) and code length = 5) or (30300 to 30509) or (30520 to 30599)

	Print 'Populate #SUBSTABUSE'
	Print Getdate()

	IF OBJECT_ID('tempdb..#SUBSTABUSE') IS NOT NULL
    DROP TABLE #SUBSTABUSE

	SELECT  DISTINCT A.patient_id ,A.Valid_index,CASE WHEN SUM(CASE WHEN DX_Codes IS NOT NULL THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END  SUBSTABUSE
	INTO #SUBSTABUSE
	FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
	LEFT JOIN  [$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot B
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND REPLACE(DX_Codes,'.','') IN (
						SELECT DISTINCT [icd9cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE (([icd9cm] like '291%' OR [icd9cm] like '292%') AND LEN([icd9cm]) = 5 ) OR ([icd9cm] >='30300' AND  [icd9cm] <='30509') OR ([icd9cm] >='30520' AND  [icd9cm] <='30599') 
						UNION
						SELECT DISTINCT [icd10cm]
						FROM [$(TargetSchema)].std_REF_icd9_icd10_cmgem WHERE (([icd9cm] like '291%' OR [icd9cm] like '292%') AND LEN([icd9cm]) = 5 ) OR ([icd9cm] >='30300' AND  [icd9cm] <='30509') OR ([icd9cm] >='30520' AND  [icd9cm] <='30599') 
					 )
	GROUP BY A.patient_id,A.valid_index


----*************************************************Combine all table**************************************************----

Print 'Populate app_tds_feature_Comorbidity'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_Comorbidity') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_tds_feature_Comorbidity

SELECT  A.patient_id,A.Valid_Index
,AFFECDISOR
,AUTOIMMUNE
,CARDIO
,DIAB
,LIVER
,MENTRETRD
,NEUROCMD
,OBESITY
,OSTEO
,OTHRMENT
,POLYPHYRINMETABOLISM
,RENALINSUFF
,SEIZURE345
,SEIZURE345OR78039
,SERMENTILL
,SLEEP
,SUBSTABUSE
INTO [$(TargetSchema)].app_tds_feature_Comorbidity
FROM [$(TargetSchema)].app_tds_int_BaselineModel_Input_IndexDate A
left join #AFFECDISOR (nolock)			B
	ON A.patient_id   =  B.patient_id
	AND A.Valid_Index =  B.Valid_Index
	left join #AUTOIMMUNE (nolock)		C
	ON A.patient_id   =  C.patient_id
	AND A.Valid_Index =  C.Valid_Index
	left join #CARDIO (nolock)			D
	ON A.patient_id   =  D.patient_id
	AND A.Valid_Index =  D.Valid_Index
	left join #DIAB (nolock)			E
	ON A.patient_id   =  E.patient_id
	AND A.Valid_Index =  E.Valid_Index
	left join #LIVER (nolock)			F
	ON A.patient_id   =  F.patient_id
	AND A.Valid_Index =  F.Valid_Index
	left join #MENTRETRD (nolock)		G
	ON A.patient_id   =  G.patient_id
	AND A.Valid_Index =  G.Valid_Index
	left join #NEUROCMD (nolock)		H
	ON A.patient_id   =  H.patient_id
	AND A.Valid_Index =  H.Valid_Index
	left join #OBESITY (nolock)			I
	ON A.patient_id   =  I.patient_id
	AND A.Valid_Index =  I.Valid_Index
	left join #OSTEO (nolock)			J
	ON A.patient_id   =  J.patient_id
	AND A.Valid_Index =  J.Valid_Index
	left join #OTHRMENT (nolock)		K
	ON A.patient_id   =  K.patient_id
	AND A.Valid_Index =  K.Valid_Index
	left join #POLYPHYRINMETABOLISM (nolock) L
	ON A.patient_id   =  L.patient_id
	AND A.Valid_Index =  L.Valid_Index
	left join #RENALINSUFF (nolock)		M
	ON A.patient_id   =  M.patient_id
	AND A.Valid_Index =  M.Valid_Index
	left join #SEIZURE345 (nolock)		N
	ON A.patient_id   =  N.patient_id
	AND A.Valid_Index =  N.Valid_Index
	left join #SEIZURE345OR78039 (nolock)	O
	ON A.patient_id   =  O.patient_id
	AND A.Valid_Index =  O.Valid_Index
	left join #SERMENTILL (nolock)		P
	ON A.patient_id   =  P.patient_id
	AND A.Valid_Index =  P.Valid_Index
	left join #SLEEP (nolock)			Q
	ON A.patient_id   =  Q.patient_id
	AND A.Valid_Index =  Q.Valid_Index
	left join #SUBSTABUSE (nolock)		R
	ON A.patient_id   =  R.patient_id
	AND A.Valid_Index =  R.Valid_Index