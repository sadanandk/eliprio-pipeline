-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create feature table related to AED for TDS AED model at Valid Index which will be further used for Model generation. 
-- - NOTE      : Step 1 : Create app_tds_feature_IndexDate_AED table which has all the features related to AED.
--				 
-- - Execution : 
-- - Date      : 15-Nov-2017	
-- - Change History:	Change Date				Change Description
--						7-Sep-2018				Apply filter of Days_Supply >0 [ANALYTICS-2208]
--						27-Nov-2018				Filtered out Strength value like ('(' & '-') AND Modified the logic for Dosage change related features.	
	-- ---------------------------------------------------------------------- 
--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

--***************************************************** Start : Get the Date of previous regimen of indexdate***********************************************


--:setvar Schema SHA_Pipeline
Print 'Populate #Prev_AED_Date'
Print Getdate()

IF OBJECT_ID('tempdb..#Prev_AED_Date') IS NOT NULL
DROP TABLE #Prev_AED_Date

select A.patient_id,A.Valid_Index,Prev_date INTO #Prev_AED_Date
from  [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCk)  A
JOIN (select LAG(maxdate,1) over (partition by patient_id order by maxdate) AS Prev_date,* 
from [$(TargetSchema)].[app_tds_cohort](NOLOCK)) B
ON	  A.patient_id=B.patient_id
AND	 A.Valid_Index=B.index_date
--order by A.patient_id,maxdate

--***************************************************** End : Get the Date of previous regimen of indexdate***********************************************

--******************************************* Start : Fetch list of all the AED's that are present in last Regimen just before Indexdate******************
Print 'Populate #Previous_Aed'
Print Getdate()

IF OBJECT_ID('tempdb..#Previous_Aed') IS NOT NULL
DROP TABLE #Previous_Aed

SELECT ROW_NUMBER() OVER (PARTITION BY A.patient_id,B.Valid_Index ORDER BY A.patient_id,B.Valid_Index,A.Abbreviation) AS ID, A.patient_id,B.Valid_Index,A.Abbreviation INTO #Previous_Aed 
FROM [$(TargetSchema)].[app_tds_int_Regiments_Treatment] A
JOIN #Prev_AED_Date B
ON A.patient_id=B.patient_id
AND B.Prev_date BETWEEN A.start_Point AND A.end_point

--******************************************* End : Fetch list of all the AED's that are present in last Regimen just before Indexdate******************

--**************************************************** Start : Create table app_tds_feature_IndexDate_AED ****************************************
Print 'Populate #Last_AED_Regimen'
Print Getdate()

IF OBJECT_ID('tempdb..#Last_AED_Regimen') IS NOT NULL
DROP TABLE #Last_AED_Regimen

SELECT Patient_id,Valid_Index, ISNULL([1],'') +ISNULL('__'+[2],'')+ISNULL('__'+[3],'')+ISNULL('__'+[4],'')+ISNULL('__'+[5],'')+ISNULL('__'+[6],'')+ISNULL('__'+[7],'')+ISNULL('__'+[8],'')+ISNULL('__'+[9],'')+ISNULL('__'+[10],'')+ISNULL('__'+[11],'')+ISNULL('__'+[12],'')+ISNULL('__'+[13],'')+ISNULL('__'+[14],'')+ISNULL('__'+[15],'')+ISNULL('__'+[16],'')+ISNULL('__'+[17],'')+ISNULL('__'+[18],'')+ISNULL('__'+[19],'')+ISNULL('__'+[20],'') AS Last_AED
	into #Last_AED_Regimen
					FROM
					(SELECT ID,Patient_id,Valid_Index,Abbreviation AS AED1
					FROM #Previous_Aed 
					) AS SourceTable
					PIVOT
					(
					max(AED1)
					FOR ID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
					) AS PivotTable


Print 'Populate #PSPA_IndxDate_AED_Details1'
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_IndxDate_AED_Details1') IS NOT NULL
DROP TABLE #PSPA_IndxDate_AED_Details1

SELECT  patient_id,Valid_Index,
 CAST(ISNULL([BRV],0) AS BIT) AS [last_trt_had_BRV]
,CAST(ISNULL([CBZ],0) AS BIT) AS [last_trt_had_CBZ]
,CAST(ISNULL([CLB],0) AS BIT) AS [last_trt_had_CLB]
,CAST(ISNULL([DVP],0) AS BIT) AS [last_trt_had_DVP]
,CAST(ISNULL([ESL],0) AS BIT) AS [last_trt_had_ESL]
,CAST(ISNULL([ESM],0) AS BIT) AS [last_trt_had_ESM]
,CAST(ISNULL([ETN],0) AS BIT) AS [last_trt_had_ETN]
,CAST(ISNULL([EZG],0) AS BIT) AS [last_trt_had_EZG]
,CAST(ISNULL([FBM],0) AS BIT) AS [last_trt_had_FBM]
,CAST(ISNULL([LAC],0) AS BIT) AS [last_trt_had_LAC]
,CAST(ISNULL([LTG],0) AS BIT) AS [last_trt_had_LTG]
,CAST(ISNULL([LEV],0) AS BIT) AS [last_trt_had_LEV]
,CAST(ISNULL([OXG],0) AS BIT) AS [last_trt_had_OXG]
,CAST(ISNULL([PER],0) AS BIT) AS [last_trt_had_PER]
,CAST(ISNULL([PBT],0) AS BIT) AS [last_trt_had_PBT]
,CAST(ISNULL([PTN],0) AS BIT) AS [last_trt_had_PTN]
,CAST(ISNULL([PGB],0) AS BIT) AS [last_trt_had_PGB]
,CAST(ISNULL([PRM],0) AS BIT) AS [last_trt_had_PRM]
,CAST(ISNULL([RTG],0) AS BIT) AS [last_trt_had_RTG]
,CAST(ISNULL([RFM],0) AS BIT) AS [last_trt_had_RFM]
,CAST(ISNULL([TPM],0) AS BIT) AS [last_trt_had_TPM]
,CAST(ISNULL([VPA],0) AS BIT) AS [last_trt_had_VPA]
,CAST(ISNULL([VGB],0) AS BIT) AS [last_trt_had_VGB]
,CAST(ISNULL([ZNS],0) AS BIT) AS [last_trt_had_ZNS]
INTO #PSPA_IndxDate_AED_Details1
FROM 
(SELECT distinct A.patient_id,A.Valid_Index,Abbreviation, 1 AS COUNTT
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](nolock) A
LEFT JOIN #Previous_Aed B  WITH(NOLOCK)
ON a.patient_id =B.patient_id
AND a.Valid_Index = B.Valid_Index
 ) AS SourceTable 
PIVOT
(
COUNT(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable


--**************************************************** End : Create table app_tds_feature_IndexDate_AED ****************************************

Print 'Populate #PSPA_IndxDate_AED_Details2'
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_IndxDate_AED_Details2') IS NOT NULL
DROP TABLE #PSPA_IndxDate_AED_Details2

SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [Num_pres_BRV_past_12m]
,ISNULL([CBZ],0) AS [Num_pres_CBZ_past_12m]
,ISNULL([CLB],0) AS [Num_pres_CLB_past_12m]
,ISNULL([DVP],0) AS [Num_pres_DVP_past_12m]
,ISNULL([ESL],0) AS [Num_pres_ESL_past_12m]
,ISNULL([ESM],0) AS [Num_pres_ESM_past_12m]
,ISNULL([ETN],0) AS [Num_pres_ETN_past_12m]
,ISNULL([EZG],0) AS [Num_pres_EZG_past_12m]
,ISNULL([FBM],0) AS [Num_pres_FBM_past_12m]
,ISNULL([LAC],0) AS [Num_pres_LAC_past_12m]
,ISNULL([LTG],0) AS [Num_pres_LTG_past_12m]
,ISNULL([LEV],0) AS [Num_pres_LEV_past_12m]
,ISNULL([OXG],0) AS [Num_pres_OXG_past_12m]
,ISNULL([PER],0) AS [Num_pres_PER_past_12m]
,ISNULL([PBT],0) AS [Num_pres_PBT_past_12m]
,ISNULL([PTN],0) AS [Num_pres_PTN_past_12m]
,ISNULL([PGB],0) AS [Num_pres_PGB_past_12m]
,ISNULL([PRM],0) AS [Num_pres_PRM_past_12m]
,ISNULL([RTG],0) AS [Num_pres_RTG_past_12m]
,ISNULL([RFM],0) AS [Num_pres_RFM_past_12m]
,ISNULL([TPM],0) AS [Num_pres_TPM_past_12m]
,ISNULL([VPA],0) AS [Num_pres_VPA_past_12m]
,ISNULL([VGB],0) AS [Num_pres_VGB_past_12m]
,ISNULL([ZNS],0) AS [Num_pres_ZNS_past_12m]
INTO #PSPA_IndxDate_AED_Details2
FROM 
(
	SELECT A.patient_id,A.Valid_Index,C.abbreviation, 1 AS COUNTT
	FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A 	 WITH(NOLOCK)
	LEFT JOIN [$(TargetSchema)].app_tds_int_Output_Table_Filtered30Day B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.maxdate <= DATEADD(DD,-1,A.Valid_Index) AND B.enddate >= DATEADD(DD,-365,A.Valid_Index) 
	LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] C WITH(NOLOCK)
	ON B.AED = C.AED
) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable

Print 'Populate #PSPA_IndxDate_AED_Details3'
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_IndxDate_AED_Details3') IS NOT NULL
DROP TABLE #PSPA_IndxDate_AED_Details3

SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [Num_pres_BRV_past_6m]
,ISNULL([CBZ],0) AS [Num_pres_CBZ_past_6m]
,ISNULL([CLB],0) AS [Num_pres_CLB_past_6m]
,ISNULL([DVP],0) AS [Num_pres_DVP_past_6m]
,ISNULL([ESL],0) AS [Num_pres_ESL_past_6m]
,ISNULL([ESM],0) AS [Num_pres_ESM_past_6m]
,ISNULL([ETN],0) AS [Num_pres_ETN_past_6m]
,ISNULL([EZG],0) AS [Num_pres_EZG_past_6m]
,ISNULL([FBM],0) AS [Num_pres_FBM_past_6m]
,ISNULL([LAC],0) AS [Num_pres_LAC_past_6m]
,ISNULL([LTG],0) AS [Num_pres_LTG_past_6m]
,ISNULL([LEV],0) AS [Num_pres_LEV_past_6m]
,ISNULL([OXG],0) AS [Num_pres_OXG_past_6m]
,ISNULL([PER],0) AS [Num_pres_PER_past_6m]
,ISNULL([PBT],0) AS [Num_pres_PBT_past_6m]
,ISNULL([PTN],0) AS [Num_pres_PTN_past_6m]
,ISNULL([PGB],0) AS [Num_pres_PGB_past_6m]
,ISNULL([PRM],0) AS [Num_pres_PRM_past_6m]
,ISNULL([RTG],0) AS [Num_pres_RTG_past_6m]
,ISNULL([RFM],0) AS [Num_pres_RFM_past_6m]
,ISNULL([TPM],0) AS [Num_pres_TPM_past_6m]
,ISNULL([VPA],0) AS [Num_pres_VPA_past_6m]
,ISNULL([VGB],0) AS [Num_pres_VGB_past_6m]
,ISNULL([ZNS],0) AS [Num_pres_ZNS_past_6m]
INTO #PSPA_IndxDate_AED_Details3
FROM 
(
	SELECT A.patient_id,A.Valid_Index,C.abbreviation, 1 AS COUNTT
	FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A 	 WITH(NOLOCK)
	LEFT JOIN [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.maxdate <= DATEADD(DD,-1,A.Valid_Index) AND B.enddate >= DATEADD(DD,-180,A.Valid_Index) 
	LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] C WITH(NOLOCK)
	ON B.AED = C.AED
 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable


Print 'Populate #PSPA_IndxDate_AED_Details4'
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_IndxDate_AED_Details4') IS NOT NULL
DROP TABLE #PSPA_IndxDate_AED_Details4

SELECT A.patient_id,A.Valid_Index,COUNT(DISTINCT C.abbreviation) AS Num_AED_past_12m
INTO #PSPA_IndxDate_AED_Details4
	FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A 	 WITH(NOLOCK)
	LEFT JOIN [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.maxdate <= DATEADD(DD,-1,A.Valid_Index) AND B.enddate >= DATEADD(DD,-365,A.Valid_Index) 
	LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] C WITH(NOLOCK)
	ON B.AED = C.AED
	GROUP BY A.patient_id,A.Valid_Index
	

Print 'Populate #PSPA_IndxDate_AED_Details5'
Print Getdate()

	
IF OBJECT_ID('tempdb..#PSPA_IndxDate_AED_Details5') IS NOT NULL
DROP TABLE #PSPA_IndxDate_AED_Details5

SELECT A.patient_id,A.Valid_Index,COUNT(DISTINCT C.abbreviation) AS Num_AED_past_6m
INTO #PSPA_IndxDate_AED_Details5
	FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A 	 WITH(NOLOCK)
	LEFT JOIN [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.maxdate <= DATEADD(DD,-1,A.Valid_Index) AND B.enddate >= DATEADD(DD,-180,A.Valid_Index) 
	LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] C WITH(NOLOCK)
	ON B.AED = C.AED
	GROUP BY A.patient_id,A.Valid_Index

----------------------------------AED Dosage---------------------------------------
Print 'Populate #Pre_temp'
Print Getdate()

IF OBJECT_ID('tempdb..#Pre_temp') IS NOT NULL
DROP TABLE #Pre_temp
	
SELECT  patient_id,service_date,GENERIC_NAME,STRENGTH,QUAN,DAYS_SUPPLY,
CASE WHEN REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') LIKE '%/%' THEN  SUBSTRING(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') ,1,CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))-1 )
                           ELSE REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','')
                           END
AS strengthtrencate
INTO #Pre_temp
FROM [$(SrcSchema)].src_RX_CLAIMS A
JOIN [$(SrcSchema)].src_ref_Product B
ON A.NDC=B.NDC
WHERE  PATIENT_ID in (SELECT  DISTINCT patient_id FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate])
AND GENERIC_NAME IN (SELECT DISTINCT Generic_name FROM [$(TargetSchema)].[std_REF_AEDName]) AND  STRENGTH <> 'N/A' AND DAYS_SUPPLY >0 AND (STRENGTH NOT LIKE '%(%' AND STRENGTH NOT LIKE '%-%' AND STRENGTH NOT LIKE '%;%')

/* 
--Note: This code has been commented because of different patterns found in STRENGTH column and below code needs to change as and when new pattern found. THis code will fail when we encounter new patterns in STRENGTH column.
--To resolve this issue we need to have all possible patterns. New code will simply put all column values as 0

IF OBJECT_ID('tempdb..#Dosage_Change_365d') IS NOT NULL
DROP TABLE #Dosage_Change_365d


 SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [rx_AED_BRV_dose_Change_past_365d]
,ISNULL([CBZ],0) AS [rx_AED_CBZ_dose_Change_past_365d]
,ISNULL([CLB],0) AS [rx_AED_CLB_dose_Change_past_365d]
,ISNULL([DVP],0) AS [rx_AED_DVP_dose_Change_past_365d]
,ISNULL([ESL],0) AS [rx_AED_ESL_dose_Change_past_365d]
,ISNULL([ESM],0) AS [rx_AED_ESM_dose_Change_past_365d]
,ISNULL([ETN],0) AS [rx_AED_ETN_dose_Change_past_365d]
,ISNULL([EZG],0) AS [rx_AED_EZG_dose_Change_past_365d]
,ISNULL([FBM],0) AS [rx_AED_FBM_dose_Change_past_365d]
,ISNULL([LAC],0) AS [rx_AED_LAC_dose_Change_past_365d]
,ISNULL([LTG],0) AS [rx_AED_LTG_dose_Change_past_365d]
,ISNULL([LEV],0) AS [rx_AED_LEV_dose_Change_past_365d]
,ISNULL([OXG],0) AS [rx_AED_OXG_dose_Change_past_365d]
,ISNULL([PER],0) AS [rx_AED_PER_dose_Change_past_365d]
,ISNULL([PBT],0) AS [rx_AED_PBT_dose_Change_past_365d]
,ISNULL([PTN],0) AS [rx_AED_PTN_dose_Change_past_365d]
,ISNULL([PGB],0) AS [rx_AED_PGB_dose_Change_past_365d]
,ISNULL([PRM],0) AS [rx_AED_PRM_dose_Change_past_365d]
,ISNULL([RTG],0) AS [rx_AED_RTG_dose_Change_past_365d]
,ISNULL([RFM],0) AS [rx_AED_RFM_dose_Change_past_365d]
,ISNULL([TPM],0) AS [rx_AED_TPM_dose_Change_past_365d]
,ISNULL([VPA],0) AS [rx_AED_VPA_dose_Change_past_365d]
,ISNULL([VGB],0) AS [rx_AED_VGB_dose_Change_past_365d]
,ISNULL([ZNS],0) AS [rx_AED_ZNS_dose_Change_past_365d]
INTO #Dosage_Change_365d
FROM 
(
	
SELECT  A.patient_id,A.Valid_Index,abbreviation,Countt
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
LEFT JOIN ( 		
			SELECT patient_id,Valid_Index,abbreviation,CASE WHEN Dosage_Strength_Per_day <> NextDosage THEN 1 ELSE 0 END AS COUNTT
			FROM 
			(
				SELECT *,LEAD(Dosage_Strength_Per_day,1) OVER (PARTITION BY  patient_id,Valid_Index,abbreviation Order by patient_id,Valid_Index,service_date ) NextDosage FROM 
				(
								SELECT -- ROW_NUMBER () OVER (PARTITION BY patient_id,Valid_Index,abbreviation ORDER BY service_date ) AS ROWNUM ,
								patient_id,Valid_Index,service_date,abbreviation,SUM( CAST(([Quantity/(Volume*DaySupply)]+CASE WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.000 AND 0.249 THEN 0.000 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.250 AND 0.500 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.500 AND 0.749 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.750 AND 0.999 THEN 1.000 - [Quantity/(Volume*DaySupply)]%1
																		END)*(strengthtrencate) AS DECIMAL(12,3))) AS Dosage_Strength_Per_day 
								FROM (
										select patient_id,Valid_Index,service_date, GENERIC_NAME,abbreviation,STRENGTH,QUAN,DAYS_SUPPLY, strengthtrencate,CAST(QUAN/(volume_part*DAYS_SUPPLY) AS DECIMAL(9,3) )As  [Quantity/(Volume*DaySupply)]
										from (
												SELECT		A.patient_id,Valid_Index,service_date, C.AED AS GENERIC_NAME,c.abbreviation,STRENGTH,QUAN,DAYS_SUPPLY,CAST(strengthtrencate AS DECIMAL(12,3)) AS strengthtrencate,
												CASE WHEN REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') LIKE '%/' THEN '1'
                                                WHEN  REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') NOT LIKE '%/%' THEN '1' 
                                                ELSE  CASE WHEN STRENGTH LIKE '%HR%' THEN 1 ELSE  SUBSTRING(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''),CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))+1 ,(LEN(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))-CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','')))) END
                                                END AS volume_part									
												FROM        [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK) A
												LEFT JOIN	#Pre_Temp   B
												ON			A.patient_id = B.patient_id
												AND			B.Service_date BETWEEN  DATEADD(DD,-365,A.Valid_Index)  AND   DATEADD(DD,-1,A.Valid_Index)
												LEFT JOIN [$(TargetSchema)].std_REF_AEDName C WITH(NOLOCK)
												ON B.GENERIC_NAME = C.GENERIC_NAME
												WHERE		days_supply > 0 
											) A  	
									  )B
								GROUP BY  patient_id,Valid_Index,service_date,abbreviation
				)Sub
			)Main  WHERE NextDosage IS NOT NULL
		   ) B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable

*/

Print 'Populate #Dosage_Change_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#Dosage_Change_365d') IS NOT NULL
DROP TABLE #Dosage_Change_365d

SELECT  patient_id,Valid_Index
,0 AS [rx_AED_BRV_dose_Change_past_365d]
,0 AS [rx_AED_CBZ_dose_Change_past_365d]
,0 AS [rx_AED_CLB_dose_Change_past_365d]
,0 AS [rx_AED_DVP_dose_Change_past_365d]
,0 AS [rx_AED_ESL_dose_Change_past_365d]
,0 AS [rx_AED_ESM_dose_Change_past_365d]
,0 AS [rx_AED_ETN_dose_Change_past_365d]
,0 AS [rx_AED_EZG_dose_Change_past_365d]
,0 AS [rx_AED_FBM_dose_Change_past_365d]
,0 AS [rx_AED_LAC_dose_Change_past_365d]
,0 AS [rx_AED_LTG_dose_Change_past_365d]
,0 AS [rx_AED_LEV_dose_Change_past_365d]
,0 AS [rx_AED_OXG_dose_Change_past_365d]
,0 AS [rx_AED_PER_dose_Change_past_365d]
,0 AS [rx_AED_PBT_dose_Change_past_365d]
,0 AS [rx_AED_PTN_dose_Change_past_365d]
,0 AS [rx_AED_PGB_dose_Change_past_365d]
,0 AS [rx_AED_PRM_dose_Change_past_365d]
,0 AS [rx_AED_RTG_dose_Change_past_365d]
,0 AS [rx_AED_RFM_dose_Change_past_365d]
,0 AS [rx_AED_TPM_dose_Change_past_365d]
,0 AS [rx_AED_VPA_dose_Change_past_365d]
,0 AS [rx_AED_VGB_dose_Change_past_365d]
,0 AS [rx_AED_ZNS_dose_Change_past_365d]
INTO #Dosage_Change_365d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] (NOLOCK)



/*
IF OBJECT_ID('tempdb..#Dosage_Change_180d') IS NOT NULL
DROP TABLE #Dosage_Change_180d


 SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [rx_AED_BRV_dose_Change_past_180d]
,ISNULL([CBZ],0) AS [rx_AED_CBZ_dose_Change_past_180d]
,ISNULL([CLB],0) AS [rx_AED_CLB_dose_Change_past_180d]
,ISNULL([DVP],0) AS [rx_AED_DVP_dose_Change_past_180d]
,ISNULL([ESL],0) AS [rx_AED_ESL_dose_Change_past_180d]
,ISNULL([ESM],0) AS [rx_AED_ESM_dose_Change_past_180d]
,ISNULL([ETN],0) AS [rx_AED_ETN_dose_Change_past_180d]
,ISNULL([EZG],0) AS [rx_AED_EZG_dose_Change_past_180d]
,ISNULL([FBM],0) AS [rx_AED_FBM_dose_Change_past_180d]
,ISNULL([LAC],0) AS [rx_AED_LAC_dose_Change_past_180d]
,ISNULL([LTG],0) AS [rx_AED_LTG_dose_Change_past_180d]
,ISNULL([LEV],0) AS [rx_AED_LEV_dose_Change_past_180d]
,ISNULL([OXG],0) AS [rx_AED_OXG_dose_Change_past_180d]
,ISNULL([PER],0) AS [rx_AED_PER_dose_Change_past_180d]
,ISNULL([PBT],0) AS [rx_AED_PBT_dose_Change_past_180d]
,ISNULL([PTN],0) AS [rx_AED_PTN_dose_Change_past_180d]
,ISNULL([PGB],0) AS [rx_AED_PGB_dose_Change_past_180d]
,ISNULL([PRM],0) AS [rx_AED_PRM_dose_Change_past_180d]
,ISNULL([RTG],0) AS [rx_AED_RTG_dose_Change_past_180d]
,ISNULL([RFM],0) AS [rx_AED_RFM_dose_Change_past_180d]
,ISNULL([TPM],0) AS [rx_AED_TPM_dose_Change_past_180d]
,ISNULL([VPA],0) AS [rx_AED_VPA_dose_Change_past_180d]
,ISNULL([VGB],0) AS [rx_AED_VGB_dose_Change_past_180d]
,ISNULL([ZNS],0) AS [rx_AED_ZNS_dose_Change_past_180d]
INTO #Dosage_Change_180d
FROM 
(
	
SELECT  A.patient_id,A.Valid_Index,abbreviation,Countt
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
LEFT JOIN ( 		
			SELECT patient_id,Valid_Index,abbreviation,CASE WHEN Dosage_Strength_Per_day <> NextDosage THEN 1 ELSE 0 END AS COUNTT
			FROM 
			(
				SELECT *,LEAD(Dosage_Strength_Per_day,1) OVER (PARTITION BY  patient_id,Valid_Index,abbreviation Order by patient_id,Valid_Index,service_date ) NextDosage FROM 
				(
								SELECT -- ROW_NUMBER () OVER (PARTITION BY patient_id,Valid_Index,abbreviation ORDER BY service_date ) AS ROWNUM ,
								patient_id,Valid_Index,service_date,abbreviation,SUM( CAST(([Quantity/(Volume*DaySupply)]+CASE WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.000 AND 0.249 THEN 0.000 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.250 AND 0.500 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.500 AND 0.749 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.750 AND 0.999 THEN 1.000 - [Quantity/(Volume*DaySupply)]%1
																		END)*(strengthtrencate) AS DECIMAL(12,3))) AS Dosage_Strength_Per_day 
								FROM (
										select patient_id,Valid_Index,service_date, GENERIC_NAME,abbreviation,STRENGTH,QUAN,DAYS_SUPPLY, strengthtrencate,CAST(QUAN/(volume_part*DAYS_SUPPLY) AS DECIMAL(9,3) )As  [Quantity/(Volume*DaySupply)]
										from (
												SELECT		A.patient_id,Valid_Index,service_date, C.AED AS GENERIC_NAME,c.abbreviation,STRENGTH,QUAN,DAYS_SUPPLY,CAST(strengthtrencate AS DECIMAL(12,3)) AS strengthtrencate,
												CASE WHEN REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') LIKE '%/' THEN '1'
                                                WHEN  REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') NOT LIKE '%/%' THEN '1' 
                                                ELSE  CASE WHEN STRENGTH LIKE '%HR%' THEN 1 ELSE  SUBSTRING(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''),CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))+1 ,(LEN(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))-CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','')))) END
                                                END AS volume_part	
												FROM        [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK) A
												LEFT JOIN	#Pre_Temp   B
												ON			A.patient_id = B.patient_id
												AND			B.Service_date BETWEEN  DATEADD(DD,-180,A.Valid_Index)  AND   DATEADD(DD,-1,A.Valid_Index)
												LEFT JOIN [$(TargetSchema)].std_REF_AEDName C WITH(NOLOCK)
												ON B.GENERIC_NAME = C.GENERIC_NAME
												WHERE		days_supply > 0 
											) A  	
									  )B
								GROUP BY  patient_id,Valid_Index,service_date,abbreviation
				)Sub
			)Main  WHERE NextDosage IS NOT NULL
		   ) B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable
*/

Print 'Populate #Dosage_Change_180d'
Print Getdate()

IF OBJECT_ID('tempdb..#Dosage_Change_180d') IS NOT NULL
DROP TABLE #Dosage_Change_180d


 SELECT  patient_id,Valid_Index
,0 AS [rx_AED_BRV_dose_Change_past_180d]
,0 AS [rx_AED_CBZ_dose_Change_past_180d]
,0 AS [rx_AED_CLB_dose_Change_past_180d]
,0 AS [rx_AED_DVP_dose_Change_past_180d]
,0 AS [rx_AED_ESL_dose_Change_past_180d]
,0 AS [rx_AED_ESM_dose_Change_past_180d]
,0 AS [rx_AED_ETN_dose_Change_past_180d]
,0 AS [rx_AED_EZG_dose_Change_past_180d]
,0 AS [rx_AED_FBM_dose_Change_past_180d]
,0 AS [rx_AED_LAC_dose_Change_past_180d]
,0 AS [rx_AED_LTG_dose_Change_past_180d]
,0 AS [rx_AED_LEV_dose_Change_past_180d]
,0 AS [rx_AED_OXG_dose_Change_past_180d]
,0 AS [rx_AED_PER_dose_Change_past_180d]
,0 AS [rx_AED_PBT_dose_Change_past_180d]
,0 AS [rx_AED_PTN_dose_Change_past_180d]
,0 AS [rx_AED_PGB_dose_Change_past_180d]
,0 AS [rx_AED_PRM_dose_Change_past_180d]
,0 AS [rx_AED_RTG_dose_Change_past_180d]
,0 AS [rx_AED_RFM_dose_Change_past_180d]
,0 AS [rx_AED_TPM_dose_Change_past_180d]
,0 AS [rx_AED_VPA_dose_Change_past_180d]
,0 AS [rx_AED_VGB_dose_Change_past_180d]
,0 AS [rx_AED_ZNS_dose_Change_past_180d]
INTO #Dosage_Change_180d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] (NOLOCK)
--------------------Dosage Increase-------------------------

/*
IF OBJECT_ID('tempdb..#Dosage_Increase_365d') IS NOT NULL
DROP TABLE #Dosage_Increase_365d

 SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [rx_AED_BRV_dose_Increase_past_365d]
,ISNULL([CBZ],0) AS [rx_AED_CBZ_dose_Increase_past_365d]
,ISNULL([CLB],0) AS [rx_AED_CLB_dose_Increase_past_365d]
,ISNULL([DVP],0) AS [rx_AED_DVP_dose_Increase_past_365d]
,ISNULL([ESL],0) AS [rx_AED_ESL_dose_Increase_past_365d]
,ISNULL([ESM],0) AS [rx_AED_ESM_dose_Increase_past_365d]
,ISNULL([ETN],0) AS [rx_AED_ETN_dose_Increase_past_365d]
,ISNULL([EZG],0) AS [rx_AED_EZG_dose_Increase_past_365d]
,ISNULL([FBM],0) AS [rx_AED_FBM_dose_Increase_past_365d]
,ISNULL([LAC],0) AS [rx_AED_LAC_dose_Increase_past_365d]
,ISNULL([LTG],0) AS [rx_AED_LTG_dose_Increase_past_365d]
,ISNULL([LEV],0) AS [rx_AED_LEV_dose_Increase_past_365d]
,ISNULL([OXG],0) AS [rx_AED_OXG_dose_Increase_past_365d]
,ISNULL([PER],0) AS [rx_AED_PER_dose_Increase_past_365d]
,ISNULL([PBT],0) AS [rx_AED_PBT_dose_Increase_past_365d]
,ISNULL([PTN],0) AS [rx_AED_PTN_dose_Increase_past_365d]
,ISNULL([PGB],0) AS [rx_AED_PGB_dose_Increase_past_365d]
,ISNULL([PRM],0) AS [rx_AED_PRM_dose_Increase_past_365d]
,ISNULL([RTG],0) AS [rx_AED_RTG_dose_Increase_past_365d]
,ISNULL([RFM],0) AS [rx_AED_RFM_dose_Increase_past_365d]
,ISNULL([TPM],0) AS [rx_AED_TPM_dose_Increase_past_365d]
,ISNULL([VPA],0) AS [rx_AED_VPA_dose_Increase_past_365d]
,ISNULL([VGB],0) AS [rx_AED_VGB_dose_Increase_past_365d]
,ISNULL([ZNS],0) AS [rx_AED_ZNS_dose_Increase_past_365d]
INTO #Dosage_Increase_365d
FROM 
(
	
SELECT  A.patient_id,A.Valid_Index,abbreviation,Countt
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
LEFT JOIN ( 		
			SELECT patient_id,Valid_Index,abbreviation,CASE WHEN Dosage_Strength_Per_day - NextDosage  <0 THEN 1 ELSE 0 END AS COUNTT
			FROM 
			(
				SELECT *,LEAD(Dosage_Strength_Per_day,1) OVER (PARTITION BY  patient_id,Valid_Index,abbreviation Order by patient_id,Valid_Index,service_date ) NextDosage FROM 
				(
								SELECT -- ROW_NUMBER () OVER (PARTITION BY patient_id,Valid_Index,abbreviation ORDER BY service_date ) AS ROWNUM ,
								patient_id,Valid_Index,service_date,abbreviation,SUM( CAST(([Quantity/(Volume*DaySupply)]+CASE WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.000 AND 0.249 THEN 0.000 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.250 AND 0.500 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.500 AND 0.749 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.750 AND 0.999 THEN 1.000 - [Quantity/(Volume*DaySupply)]%1
																		END)*(strengthtrencate) AS DECIMAL(12,3))) AS Dosage_Strength_Per_day 
								FROM (
										select patient_id,Valid_Index,service_date, GENERIC_NAME,abbreviation,STRENGTH,QUAN,DAYS_SUPPLY, strengthtrencate,CAST(QUAN/(volume_part*DAYS_SUPPLY) AS DECIMAL(9,3) )As  [Quantity/(Volume*DaySupply)]
										from (
												SELECT		A.patient_id,Valid_Index,service_date, C.AED AS GENERIC_NAME,c.abbreviation,STRENGTH,QUAN,DAYS_SUPPLY,CAST(strengthtrencate AS DECIMAL(12,3)) AS strengthtrencate,
												CASE WHEN REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') LIKE '%/' THEN '1'
                                                WHEN  REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') NOT LIKE '%/%' THEN '1' 
                                                ELSE  CASE WHEN STRENGTH LIKE '%HR%' THEN 1 ELSE  SUBSTRING(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''),CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))+1 ,(LEN(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))-CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','')))) END
                                                END AS volume_part
												FROM        [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK) A
												LEFT JOIN	#Pre_Temp   B
												ON			A.patient_id = B.patient_id
												AND			B.Service_date BETWEEN  DATEADD(DD,-365,A.Valid_Index)  AND   DATEADD(DD,-1,A.Valid_Index)
												LEFT JOIN [$(TargetSchema)].std_REF_AEDName C WITH(NOLOCK)
												ON B.GENERIC_NAME = C.GENERIC_NAME
												WHERE		days_supply > 0  
											) A  	
									  )B
								GROUP BY  patient_id,Valid_Index,service_date,abbreviation
				)Sub
			)Main  WHERE NextDosage IS NOT NULL
		   ) B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable
*/

Print 'Populate #Dosage_Increase_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#Dosage_Increase_365d') IS NOT NULL
DROP TABLE #Dosage_Increase_365d

 SELECT  patient_id,Valid_Index
,0 AS [rx_AED_BRV_dose_Increase_past_365d]
,0 AS [rx_AED_CBZ_dose_Increase_past_365d]
,0 AS [rx_AED_CLB_dose_Increase_past_365d]
,0 AS [rx_AED_DVP_dose_Increase_past_365d]
,0 AS [rx_AED_ESL_dose_Increase_past_365d]
,0 AS [rx_AED_ESM_dose_Increase_past_365d]
,0 AS [rx_AED_ETN_dose_Increase_past_365d]
,0 AS [rx_AED_EZG_dose_Increase_past_365d]
,0 AS [rx_AED_FBM_dose_Increase_past_365d]
,0 AS [rx_AED_LAC_dose_Increase_past_365d]
,0 AS [rx_AED_LTG_dose_Increase_past_365d]
,0 AS [rx_AED_LEV_dose_Increase_past_365d]
,0 AS [rx_AED_OXG_dose_Increase_past_365d]
,0 AS [rx_AED_PER_dose_Increase_past_365d]
,0 AS [rx_AED_PBT_dose_Increase_past_365d]
,0 AS [rx_AED_PTN_dose_Increase_past_365d]
,0 AS [rx_AED_PGB_dose_Increase_past_365d]
,0 AS [rx_AED_PRM_dose_Increase_past_365d]
,0 AS [rx_AED_RTG_dose_Increase_past_365d]
,0 AS [rx_AED_RFM_dose_Increase_past_365d]
,0 AS [rx_AED_TPM_dose_Increase_past_365d]
,0 AS [rx_AED_VPA_dose_Increase_past_365d]
,0 AS [rx_AED_VGB_dose_Increase_past_365d]
,0 AS [rx_AED_ZNS_dose_Increase_past_365d]
INTO #Dosage_Increase_365d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A (NOLOCK)

/*
IF OBJECT_ID('tempdb..#Dosage_Increase_180d') IS NOT NULL
DROP TABLE #Dosage_Increase_180d

 SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [rx_AED_BRV_dose_Increase_past_180d]
,ISNULL([CBZ],0) AS [rx_AED_CBZ_dose_Increase_past_180d]
,ISNULL([CLB],0) AS [rx_AED_CLB_dose_Increase_past_180d]
,ISNULL([DVP],0) AS [rx_AED_DVP_dose_Increase_past_180d]
,ISNULL([ESL],0) AS [rx_AED_ESL_dose_Increase_past_180d]
,ISNULL([ESM],0) AS [rx_AED_ESM_dose_Increase_past_180d]
,ISNULL([ETN],0) AS [rx_AED_ETN_dose_Increase_past_180d]
,ISNULL([EZG],0) AS [rx_AED_EZG_dose_Increase_past_180d]
,ISNULL([FBM],0) AS [rx_AED_FBM_dose_Increase_past_180d]
,ISNULL([LAC],0) AS [rx_AED_LAC_dose_Increase_past_180d]
,ISNULL([LTG],0) AS [rx_AED_LTG_dose_Increase_past_180d]
,ISNULL([LEV],0) AS [rx_AED_LEV_dose_Increase_past_180d]
,ISNULL([OXG],0) AS [rx_AED_OXG_dose_Increase_past_180d]
,ISNULL([PER],0) AS [rx_AED_PER_dose_Increase_past_180d]
,ISNULL([PBT],0) AS [rx_AED_PBT_dose_Increase_past_180d]
,ISNULL([PTN],0) AS [rx_AED_PTN_dose_Increase_past_180d]
,ISNULL([PGB],0) AS [rx_AED_PGB_dose_Increase_past_180d]
,ISNULL([PRM],0) AS [rx_AED_PRM_dose_Increase_past_180d]
,ISNULL([RTG],0) AS [rx_AED_RTG_dose_Increase_past_180d]
,ISNULL([RFM],0) AS [rx_AED_RFM_dose_Increase_past_180d]
,ISNULL([TPM],0) AS [rx_AED_TPM_dose_Increase_past_180d]
,ISNULL([VPA],0) AS [rx_AED_VPA_dose_Increase_past_180d]
,ISNULL([VGB],0) AS [rx_AED_VGB_dose_Increase_past_180d]
,ISNULL([ZNS],0) AS [rx_AED_ZNS_dose_Increase_past_180d]
INTO #Dosage_Increase_180d
FROM 
(
	
SELECT  A.patient_id,A.Valid_Index,abbreviation,Countt
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
LEFT JOIN ( 		
			SELECT patient_id,Valid_Index,abbreviation,CASE WHEN Dosage_Strength_Per_day - NextDosage  <0 THEN 1 ELSE 0 END AS COUNTT
			FROM 
			(
				SELECT *,LEAD(Dosage_Strength_Per_day,1) OVER (PARTITION BY  patient_id,Valid_Index,abbreviation Order by patient_id,Valid_Index,service_date ) NextDosage FROM 
				(
								SELECT -- ROW_NUMBER () OVER (PARTITION BY patient_id,Valid_Index,abbreviation ORDER BY service_date ) AS ROWNUM ,
								patient_id,Valid_Index,service_date,abbreviation,SUM( CAST(([Quantity/(Volume*DaySupply)]+CASE WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.000 AND 0.249 THEN 0.000 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.250 AND 0.500 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.500 AND 0.749 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.750 AND 0.999 THEN 1.000 - [Quantity/(Volume*DaySupply)]%1
																		END)*(strengthtrencate) AS DECIMAL(12,3))) AS Dosage_Strength_Per_day 
								FROM (
										select patient_id,Valid_Index,service_date, GENERIC_NAME,abbreviation,STRENGTH,QUAN,DAYS_SUPPLY, strengthtrencate,CAST(QUAN/(volume_part*DAYS_SUPPLY) AS DECIMAL(9,3) )As  [Quantity/(Volume*DaySupply)]
										from (
												SELECT		A.patient_id,Valid_Index,service_date, C.AED AS GENERIC_NAME,c.abbreviation,STRENGTH,QUAN,DAYS_SUPPLY,CAST(strengthtrencate AS DECIMAL(12,3)) AS strengthtrencate,
												CASE WHEN REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') LIKE '%/' THEN '1'
                                                WHEN  REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') NOT LIKE '%/%' THEN '1' 
                                                ELSE  CASE WHEN STRENGTH LIKE '%HR%' THEN 1 ELSE  SUBSTRING(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''),CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))+1 ,(LEN(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))-CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','')))) END
                                                END AS volume_part
												FROM        [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK) A
												LEFT JOIN	#Pre_Temp   B
												ON			A.patient_id = B.patient_id
												AND			B.Service_date BETWEEN  DATEADD(DD,-180,A.Valid_Index)  AND   DATEADD(DD,-1,A.Valid_Index)
												LEFT JOIN [$(TargetSchema)].std_REF_AEDName C WITH(NOLOCK)
												ON B.GENERIC_NAME = C.GENERIC_NAME
												WHERE		days_supply > 0 
											) A  	
									  )B
								GROUP BY  patient_id,Valid_Index,service_date,abbreviation
				)Sub
			)Main  WHERE NextDosage IS NOT NULL
		   ) B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable
*/

Print 'Populate #Dosage_Increase_180d'
Print Getdate()

IF OBJECT_ID('tempdb..#Dosage_Increase_180d') IS NOT NULL
DROP TABLE #Dosage_Increase_180d

 SELECT  patient_id,Valid_Index
,0 AS [rx_AED_BRV_dose_Increase_past_180d]
,0 AS [rx_AED_CBZ_dose_Increase_past_180d]
,0 AS [rx_AED_CLB_dose_Increase_past_180d]
,0 AS [rx_AED_DVP_dose_Increase_past_180d]
,0 AS [rx_AED_ESL_dose_Increase_past_180d]
,0 AS [rx_AED_ESM_dose_Increase_past_180d]
,0 AS [rx_AED_ETN_dose_Increase_past_180d]
,0 AS [rx_AED_EZG_dose_Increase_past_180d]
,0 AS [rx_AED_FBM_dose_Increase_past_180d]
,0 AS [rx_AED_LAC_dose_Increase_past_180d]
,0 AS [rx_AED_LTG_dose_Increase_past_180d]
,0 AS [rx_AED_LEV_dose_Increase_past_180d]
,0 AS [rx_AED_OXG_dose_Increase_past_180d]
,0 AS [rx_AED_PER_dose_Increase_past_180d]
,0 AS [rx_AED_PBT_dose_Increase_past_180d]
,0 AS [rx_AED_PTN_dose_Increase_past_180d]
,0 AS [rx_AED_PGB_dose_Increase_past_180d]
,0 AS [rx_AED_PRM_dose_Increase_past_180d]
,0 AS [rx_AED_RTG_dose_Increase_past_180d]
,0 AS [rx_AED_RFM_dose_Increase_past_180d]
,0 AS [rx_AED_TPM_dose_Increase_past_180d]
,0 AS [rx_AED_VPA_dose_Increase_past_180d]
,0 AS [rx_AED_VGB_dose_Increase_past_180d]
,0 AS [rx_AED_ZNS_dose_Increase_past_180d]
INTO #Dosage_Increase_180d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] (NOLOCK)
----------------------------------Dosage Decrease-----------------------------------

/*
IF OBJECT_ID('tempdb..#Dosage_Decrease_365d') IS NOT NULL
DROP TABLE #Dosage_Decrease_365d

 SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [rx_AED_BRV_dose_Decrease_past_365d]
,ISNULL([CBZ],0) AS [rx_AED_CBZ_dose_Decrease_past_365d]
,ISNULL([CLB],0) AS [rx_AED_CLB_dose_Decrease_past_365d]
,ISNULL([DVP],0) AS [rx_AED_DVP_dose_Decrease_past_365d]
,ISNULL([ESL],0) AS [rx_AED_ESL_dose_Decrease_past_365d]
,ISNULL([ESM],0) AS [rx_AED_ESM_dose_Decrease_past_365d]
,ISNULL([ETN],0) AS [rx_AED_ETN_dose_Decrease_past_365d]
,ISNULL([EZG],0) AS [rx_AED_EZG_dose_Decrease_past_365d]
,ISNULL([FBM],0) AS [rx_AED_FBM_dose_Decrease_past_365d]
,ISNULL([LAC],0) AS [rx_AED_LAC_dose_Decrease_past_365d]
,ISNULL([LTG],0) AS [rx_AED_LTG_dose_Decrease_past_365d]
,ISNULL([LEV],0) AS [rx_AED_LEV_dose_Decrease_past_365d]
,ISNULL([OXG],0) AS [rx_AED_OXG_dose_Decrease_past_365d]
,ISNULL([PER],0) AS [rx_AED_PER_dose_Decrease_past_365d]
,ISNULL([PBT],0) AS [rx_AED_PBT_dose_Decrease_past_365d]
,ISNULL([PTN],0) AS [rx_AED_PTN_dose_Decrease_past_365d]
,ISNULL([PGB],0) AS [rx_AED_PGB_dose_Decrease_past_365d]
,ISNULL([PRM],0) AS [rx_AED_PRM_dose_Decrease_past_365d]
,ISNULL([RTG],0) AS [rx_AED_RTG_dose_Decrease_past_365d]
,ISNULL([RFM],0) AS [rx_AED_RFM_dose_Decrease_past_365d]
,ISNULL([TPM],0) AS [rx_AED_TPM_dose_Decrease_past_365d]
,ISNULL([VPA],0) AS [rx_AED_VPA_dose_Decrease_past_365d]
,ISNULL([VGB],0) AS [rx_AED_VGB_dose_Decrease_past_365d]
,ISNULL([ZNS],0) AS [rx_AED_ZNS_dose_Decrease_past_365d]
INTO #Dosage_Decrease_365d
FROM 
(
	
SELECT  A.patient_id,A.Valid_Index,abbreviation,Countt
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
LEFT JOIN ( 		
			SELECT patient_id,Valid_Index,abbreviation,CASE WHEN Dosage_Strength_Per_day - NextDosage  >0 THEN 1 ELSE 0 END AS COUNTT
			FROM 
			(
				SELECT *,LEAD(Dosage_Strength_Per_day,1) OVER (PARTITION BY  patient_id,Valid_Index,abbreviation Order by patient_id,Valid_Index,service_date ) NextDosage FROM 
				(
								SELECT -- ROW_NUMBER () OVER (PARTITION BY patient_id,Valid_Index,abbreviation ORDER BY service_date ) AS ROWNUM ,
								patient_id,Valid_Index,service_date,abbreviation,SUM( CAST(([Quantity/(Volume*DaySupply)]+CASE WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.000 AND 0.249 THEN 0.000 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.250 AND 0.500 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.500 AND 0.749 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.750 AND 0.999 THEN 1.000 - [Quantity/(Volume*DaySupply)]%1
																		END)*(strengthtrencate) AS DECIMAL(12,3))) AS Dosage_Strength_Per_day 
								FROM (
										select patient_id,Valid_Index,service_date, GENERIC_NAME,abbreviation,STRENGTH,QUAN,DAYS_SUPPLY, strengthtrencate,CAST(QUAN/(volume_part*DAYS_SUPPLY) AS DECIMAL(9,3) )As  [Quantity/(Volume*DaySupply)]
										from (
												SELECT		A.patient_id,Valid_Index,service_date, C.AED AS GENERIC_NAME,c.abbreviation,STRENGTH,QUAN,DAYS_SUPPLY,CAST(strengthtrencate AS DECIMAL(12,3)) AS strengthtrencate,
												CASE WHEN REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') LIKE '%/' THEN '1'
                                                WHEN  REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') NOT LIKE '%/%' THEN '1' 
                                                ELSE  CASE WHEN STRENGTH LIKE '%HR%' THEN 1 ELSE  SUBSTRING(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''),CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))+1 ,(LEN(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))-CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','')))) END
                                                END AS volume_part
												FROM        [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK) A
												LEFT JOIN	#Pre_Temp   B
												ON			A.patient_id = B.patient_id
												AND			B.Service_date BETWEEN  DATEADD(DD,-365,A.Valid_Index)  AND   DATEADD(DD,-1,A.Valid_Index)
												LEFT JOIN [$(TargetSchema)].std_REF_AEDName C WITH(NOLOCK)
												ON B.GENERIC_NAME = C.GENERIC_NAME
												WHERE		days_supply > 0  
											) A  	
									  )B
								GROUP BY  patient_id,Valid_Index,service_date,abbreviation
				)Sub
			)Main  WHERE NextDosage IS NOT NULL
		   ) B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index
--where a.patient_id = 50629136

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable

*/

Print 'Populate #Dosage_Decrease_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#Dosage_Decrease_365d') IS NOT NULL
DROP TABLE #Dosage_Decrease_365d

 SELECT  patient_id,Valid_Index
,0 AS [rx_AED_BRV_dose_Decrease_past_365d]
,0 AS [rx_AED_CBZ_dose_Decrease_past_365d]
,0 AS [rx_AED_CLB_dose_Decrease_past_365d]
,0 AS [rx_AED_DVP_dose_Decrease_past_365d]
,0 AS [rx_AED_ESL_dose_Decrease_past_365d]
,0 AS [rx_AED_ESM_dose_Decrease_past_365d]
,0 AS [rx_AED_ETN_dose_Decrease_past_365d]
,0 AS [rx_AED_EZG_dose_Decrease_past_365d]
,0 AS [rx_AED_FBM_dose_Decrease_past_365d]
,0 AS [rx_AED_LAC_dose_Decrease_past_365d]
,0 AS [rx_AED_LTG_dose_Decrease_past_365d]
,0 AS [rx_AED_LEV_dose_Decrease_past_365d]
,0 AS [rx_AED_OXG_dose_Decrease_past_365d]
,0 AS [rx_AED_PER_dose_Decrease_past_365d]
,0 AS [rx_AED_PBT_dose_Decrease_past_365d]
,0 AS [rx_AED_PTN_dose_Decrease_past_365d]
,0 AS [rx_AED_PGB_dose_Decrease_past_365d]
,0 AS [rx_AED_PRM_dose_Decrease_past_365d]
,0 AS [rx_AED_RTG_dose_Decrease_past_365d]
,0 AS [rx_AED_RFM_dose_Decrease_past_365d]
,0 AS [rx_AED_TPM_dose_Decrease_past_365d]
,0 AS [rx_AED_VPA_dose_Decrease_past_365d]
,0 AS [rx_AED_VGB_dose_Decrease_past_365d]
,0 AS [rx_AED_ZNS_dose_Decrease_past_365d]
INTO #Dosage_Decrease_365d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] (NOLOCK)


/*
IF OBJECT_ID('tempdb..#Dosage_Decrease_180d') IS NOT NULL
DROP TABLE #Dosage_Decrease_180d

 SELECT  patient_id,Valid_Index
,ISNULL([BRV],0) AS [rx_AED_BRV_dose_Decrease_past_180d]
,ISNULL([CBZ],0) AS [rx_AED_CBZ_dose_Decrease_past_180d]
,ISNULL([CLB],0) AS [rx_AED_CLB_dose_Decrease_past_180d]
,ISNULL([DVP],0) AS [rx_AED_DVP_dose_Decrease_past_180d]
,ISNULL([ESL],0) AS [rx_AED_ESL_dose_Decrease_past_180d]
,ISNULL([ESM],0) AS [rx_AED_ESM_dose_Decrease_past_180d]
,ISNULL([ETN],0) AS [rx_AED_ETN_dose_Decrease_past_180d]
,ISNULL([EZG],0) AS [rx_AED_EZG_dose_Decrease_past_180d]
,ISNULL([FBM],0) AS [rx_AED_FBM_dose_Decrease_past_180d]
,ISNULL([LAC],0) AS [rx_AED_LAC_dose_Decrease_past_180d]
,ISNULL([LTG],0) AS [rx_AED_LTG_dose_Decrease_past_180d]
,ISNULL([LEV],0) AS [rx_AED_LEV_dose_Decrease_past_180d]
,ISNULL([OXG],0) AS [rx_AED_OXG_dose_Decrease_past_180d]
,ISNULL([PER],0) AS [rx_AED_PER_dose_Decrease_past_180d]
,ISNULL([PBT],0) AS [rx_AED_PBT_dose_Decrease_past_180d]
,ISNULL([PTN],0) AS [rx_AED_PTN_dose_Decrease_past_180d]
,ISNULL([PGB],0) AS [rx_AED_PGB_dose_Decrease_past_180d]
,ISNULL([PRM],0) AS [rx_AED_PRM_dose_Decrease_past_180d]
,ISNULL([RTG],0) AS [rx_AED_RTG_dose_Decrease_past_180d]
,ISNULL([RFM],0) AS [rx_AED_RFM_dose_Decrease_past_180d]
,ISNULL([TPM],0) AS [rx_AED_TPM_dose_Decrease_past_180d]
,ISNULL([VPA],0) AS [rx_AED_VPA_dose_Decrease_past_180d]
,ISNULL([VGB],0) AS [rx_AED_VGB_dose_Decrease_past_180d]
,ISNULL([ZNS],0) AS [rx_AED_ZNS_dose_Decrease_past_180d]
INTO #Dosage_Decrease_180d
FROM 
(
	
SELECT  A.patient_id,A.Valid_Index,abbreviation,Countt
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] A
LEFT JOIN ( 		
			SELECT patient_id,Valid_Index,abbreviation,CASE WHEN Dosage_Strength_Per_day - NextDosage  >0 THEN 1 ELSE 0 END AS COUNTT
			FROM 
			(
				SELECT *,LEAD(Dosage_Strength_Per_day,1) OVER (PARTITION BY  patient_id,Valid_Index,abbreviation Order by patient_id,Valid_Index,service_date ) NextDosage FROM 
				(
								SELECT -- ROW_NUMBER () OVER (PARTITION BY patient_id,Valid_Index,abbreviation ORDER BY service_date ) AS ROWNUM ,
								patient_id,Valid_Index,service_date,abbreviation,SUM( CAST(([Quantity/(Volume*DaySupply)]+CASE WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.000 AND 0.249 THEN 0.000 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.250 AND 0.500 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.500 AND 0.749 THEN 0.500 - [Quantity/(Volume*DaySupply)]%1
																			  WHEN [Quantity/(Volume*DaySupply)]%1 BETWEEN 0.750 AND 0.999 THEN 1.000 - [Quantity/(Volume*DaySupply)]%1
																		END)*(strengthtrencate) AS DECIMAL(12,3))) AS Dosage_Strength_Per_day 
								FROM (
										select patient_id,Valid_Index,service_date, GENERIC_NAME,abbreviation,STRENGTH,QUAN,DAYS_SUPPLY, strengthtrencate,CAST(QUAN/(volume_part*DAYS_SUPPLY) AS DECIMAL(9,3) )As  [Quantity/(Volume*DaySupply)]
										from (
												SELECT		A.patient_id,Valid_Index,service_date, C.AED AS GENERIC_NAME,c.abbreviation,STRENGTH,QUAN,DAYS_SUPPLY,CAST(strengthtrencate AS DECIMAL(12,3)) AS strengthtrencate,
												CASE WHEN REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') LIKE '%/' THEN '1'
                                                WHEN  REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','') NOT LIKE '%/%' THEN '1' 
                                                ELSE  CASE WHEN STRENGTH LIKE '%HR%' THEN 1 ELSE  SUBSTRING(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''),CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))+1 ,(LEN(REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML',''))-CHARINDEX('/',REPLACE(REPLACE(REPLACE(STRENGTH,' ',''),'MG',''),'ML','')))) END
                                                END AS volume_part
												FROM        [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate](NOLOCK) A
												LEFT JOIN	#Pre_Temp   B
												ON			A.patient_id = B.patient_id
												AND			B.Service_date BETWEEN  DATEADD(DD,-180,A.Valid_Index)  AND   DATEADD(DD,-1,A.Valid_Index)
												LEFT JOIN [$(TargetSchema)].std_REF_AEDName C WITH(NOLOCK)
												ON B.GENERIC_NAME = C.GENERIC_NAME
												WHERE		days_supply > 0
											) A  	
									  )B
								GROUP BY  patient_id,Valid_Index,service_date,abbreviation
				)Sub
			)Main  WHERE NextDosage IS NOT NULL
		   ) B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable
*/

Print 'Populate #Dosage_Decrease_180d'
Print Getdate()

IF OBJECT_ID('tempdb..#Dosage_Decrease_180d') IS NOT NULL
DROP TABLE #Dosage_Decrease_180d

 SELECT  patient_id,Valid_Index
,0 AS [rx_AED_BRV_dose_Decrease_past_180d]
,0 AS [rx_AED_CBZ_dose_Decrease_past_180d]
,0 AS [rx_AED_CLB_dose_Decrease_past_180d]
,0 AS [rx_AED_DVP_dose_Decrease_past_180d]
,0 AS [rx_AED_ESL_dose_Decrease_past_180d]
,0 AS [rx_AED_ESM_dose_Decrease_past_180d]
,0 AS [rx_AED_ETN_dose_Decrease_past_180d]
,0 AS [rx_AED_EZG_dose_Decrease_past_180d]
,0 AS [rx_AED_FBM_dose_Decrease_past_180d]
,0 AS [rx_AED_LAC_dose_Decrease_past_180d]
,0 AS [rx_AED_LTG_dose_Decrease_past_180d]
,0 AS [rx_AED_LEV_dose_Decrease_past_180d]
,0 AS [rx_AED_OXG_dose_Decrease_past_180d]
,0 AS [rx_AED_PER_dose_Decrease_past_180d]
,0 AS [rx_AED_PBT_dose_Decrease_past_180d]
,0 AS [rx_AED_PTN_dose_Decrease_past_180d]
,0 AS [rx_AED_PGB_dose_Decrease_past_180d]
,0 AS [rx_AED_PRM_dose_Decrease_past_180d]
,0 AS [rx_AED_RTG_dose_Decrease_past_180d]
,0 AS [rx_AED_RFM_dose_Decrease_past_180d]
,0 AS [rx_AED_TPM_dose_Decrease_past_180d]
,0 AS [rx_AED_VPA_dose_Decrease_past_180d]
,0 AS [rx_AED_VGB_dose_Decrease_past_180d]
,0 AS [rx_AED_ZNS_dose_Decrease_past_180d]
INTO #Dosage_Decrease_180d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate] (NOLOCK)


------------------------------------------------------------------------------------------------Joining all temp table----------------------------------------------


	Print 'Populate app_tds_feature_IndexDate_AED'
	Print Getdate()


	IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_IndexDate_AED') IS NOT NULL 
    DROP TABLE [$(TargetSchema)].app_tds_feature_IndexDate_AED

	SELECT	A.patient_id,A.Valid_Index,isnull(L.Last_AED,'NO_AED') Last_AED,
			[last_trt_had_BRV],[last_trt_had_CBZ],[last_trt_had_CLB],[last_trt_had_DVP],[last_trt_had_ESL],[last_trt_had_ESM],[last_trt_had_ETN],[last_trt_had_EZG],[last_trt_had_FBM],[last_trt_had_LAC],[last_trt_had_LTG],[last_trt_had_LEV],[last_trt_had_OXG],[last_trt_had_PER],[last_trt_had_PBT],[last_trt_had_PTN],[last_trt_had_PGB],[last_trt_had_PRM],[last_trt_had_RTG],[last_trt_had_RFM],[last_trt_had_TPM],[last_trt_had_VPA],[last_trt_had_VGB],[last_trt_had_ZNS],
			[Num_pres_BRV_past_12m],[Num_pres_CBZ_past_12m],[Num_pres_CLB_past_12m],[Num_pres_DVP_past_12m],[Num_pres_ESL_past_12m],[Num_pres_ESM_past_12m],[Num_pres_ETN_past_12m],[Num_pres_EZG_past_12m],[Num_pres_FBM_past_12m],[Num_pres_LAC_past_12m],[Num_pres_LTG_past_12m],[Num_pres_LEV_past_12m],[Num_pres_OXG_past_12m],[Num_pres_PER_past_12m],[Num_pres_PBT_past_12m],[Num_pres_PTN_past_12m],[Num_pres_PGB_past_12m],[Num_pres_PRM_past_12m],[Num_pres_RTG_past_12m],[Num_pres_RFM_past_12m],[Num_pres_TPM_past_12m],[Num_pres_VPA_past_12m],[Num_pres_VGB_past_12m],[Num_pres_ZNS_past_12m],
			[Num_pres_BRV_past_6m],[Num_pres_CBZ_past_6m],[Num_pres_CLB_past_6m],[Num_pres_DVP_past_6m],[Num_pres_ESL_past_6m],[Num_pres_ESM_past_6m],[Num_pres_ETN_past_6m],[Num_pres_EZG_past_6m],[Num_pres_FBM_past_6m],[Num_pres_LAC_past_6m],[Num_pres_LTG_past_6m],[Num_pres_LEV_past_6m],[Num_pres_OXG_past_6m],[Num_pres_PER_past_6m],[Num_pres_PBT_past_6m],[Num_pres_PTN_past_6m],[Num_pres_PGB_past_6m],[Num_pres_PRM_past_6m],[Num_pres_RTG_past_6m],[Num_pres_RFM_past_6m],[Num_pres_TPM_past_6m],[Num_pres_VPA_past_6m],[Num_pres_VGB_past_6m],[Num_pres_ZNS_past_6m],
			Num_AED_past_12m,
			Num_AED_past_6m,
			[rx_AED_BRV_dose_Change_past_365d],[rx_AED_CBZ_dose_Change_past_365d],[rx_AED_CLB_dose_Change_past_365d],[rx_AED_DVP_dose_Change_past_365d],[rx_AED_ESL_dose_Change_past_365d],[rx_AED_ESM_dose_Change_past_365d],[rx_AED_ETN_dose_Change_past_365d],[rx_AED_EZG_dose_Change_past_365d],[rx_AED_FBM_dose_Change_past_365d],[rx_AED_LAC_dose_Change_past_365d],[rx_AED_LTG_dose_Change_past_365d],[rx_AED_LEV_dose_Change_past_365d],[rx_AED_OXG_dose_Change_past_365d],[rx_AED_PER_dose_Change_past_365d],[rx_AED_PBT_dose_Change_past_365d],[rx_AED_PTN_dose_Change_past_365d],[rx_AED_PGB_dose_Change_past_365d],[rx_AED_PRM_dose_Change_past_365d],[rx_AED_RTG_dose_Change_past_365d],[rx_AED_RFM_dose_Change_past_365d],[rx_AED_TPM_dose_Change_past_365d],[rx_AED_VPA_dose_Change_past_365d],[rx_AED_VGB_dose_Change_past_365d],[rx_AED_ZNS_dose_Change_past_365d],
			[rx_AED_BRV_dose_Change_past_180d],[rx_AED_CBZ_dose_Change_past_180d],[rx_AED_CLB_dose_Change_past_180d],[rx_AED_DVP_dose_Change_past_180d],[rx_AED_ESL_dose_Change_past_180d],[rx_AED_ESM_dose_Change_past_180d],[rx_AED_ETN_dose_Change_past_180d],[rx_AED_EZG_dose_Change_past_180d],[rx_AED_FBM_dose_Change_past_180d],[rx_AED_LAC_dose_Change_past_180d],[rx_AED_LTG_dose_Change_past_180d],[rx_AED_LEV_dose_Change_past_180d],[rx_AED_OXG_dose_Change_past_180d],[rx_AED_PER_dose_Change_past_180d],[rx_AED_PBT_dose_Change_past_180d],[rx_AED_PTN_dose_Change_past_180d],[rx_AED_PGB_dose_Change_past_180d],[rx_AED_PRM_dose_Change_past_180d],[rx_AED_RTG_dose_Change_past_180d],[rx_AED_RFM_dose_Change_past_180d],[rx_AED_TPM_dose_Change_past_180d],[rx_AED_VPA_dose_Change_past_180d],[rx_AED_VGB_dose_Change_past_180d],[rx_AED_ZNS_dose_Change_past_180d],
			[rx_AED_BRV_dose_Increase_past_365d],[rx_AED_CBZ_dose_Increase_past_365d],[rx_AED_CLB_dose_Increase_past_365d],[rx_AED_DVP_dose_Increase_past_365d],[rx_AED_ESL_dose_Increase_past_365d],[rx_AED_ESM_dose_Increase_past_365d],[rx_AED_ETN_dose_Increase_past_365d],[rx_AED_EZG_dose_Increase_past_365d],[rx_AED_FBM_dose_Increase_past_365d],[rx_AED_LAC_dose_Increase_past_365d],[rx_AED_LTG_dose_Increase_past_365d],[rx_AED_LEV_dose_Increase_past_365d],[rx_AED_OXG_dose_Increase_past_365d],[rx_AED_PER_dose_Increase_past_365d],[rx_AED_PBT_dose_Increase_past_365d],[rx_AED_PTN_dose_Increase_past_365d],[rx_AED_PGB_dose_Increase_past_365d],[rx_AED_PRM_dose_Increase_past_365d],[rx_AED_RTG_dose_Increase_past_365d],[rx_AED_RFM_dose_Increase_past_365d],[rx_AED_TPM_dose_Increase_past_365d],[rx_AED_VPA_dose_Increase_past_365d],[rx_AED_VGB_dose_Increase_past_365d],[rx_AED_ZNS_dose_Increase_past_365d],
			[rx_AED_BRV_dose_Increase_past_180d],[rx_AED_CBZ_dose_Increase_past_180d],[rx_AED_CLB_dose_Increase_past_180d],[rx_AED_DVP_dose_Increase_past_180d],[rx_AED_ESL_dose_Increase_past_180d],[rx_AED_ESM_dose_Increase_past_180d],[rx_AED_ETN_dose_Increase_past_180d],[rx_AED_EZG_dose_Increase_past_180d],[rx_AED_FBM_dose_Increase_past_180d],[rx_AED_LAC_dose_Increase_past_180d],[rx_AED_LTG_dose_Increase_past_180d],[rx_AED_LEV_dose_Increase_past_180d],[rx_AED_OXG_dose_Increase_past_180d],[rx_AED_PER_dose_Increase_past_180d],[rx_AED_PBT_dose_Increase_past_180d],[rx_AED_PTN_dose_Increase_past_180d],[rx_AED_PGB_dose_Increase_past_180d],[rx_AED_PRM_dose_Increase_past_180d],[rx_AED_RTG_dose_Increase_past_180d],[rx_AED_RFM_dose_Increase_past_180d],[rx_AED_TPM_dose_Increase_past_180d],[rx_AED_VPA_dose_Increase_past_180d],[rx_AED_VGB_dose_Increase_past_180d],[rx_AED_ZNS_dose_Increase_past_180d],
			[rx_AED_BRV_dose_Decrease_past_365d],[rx_AED_CBZ_dose_Decrease_past_365d],[rx_AED_CLB_dose_Decrease_past_365d],[rx_AED_DVP_dose_Decrease_past_365d],[rx_AED_ESL_dose_Decrease_past_365d],[rx_AED_ESM_dose_Decrease_past_365d],[rx_AED_ETN_dose_Decrease_past_365d],[rx_AED_EZG_dose_Decrease_past_365d],[rx_AED_FBM_dose_Decrease_past_365d],[rx_AED_LAC_dose_Decrease_past_365d],[rx_AED_LTG_dose_Decrease_past_365d],[rx_AED_LEV_dose_Decrease_past_365d],[rx_AED_OXG_dose_Decrease_past_365d],[rx_AED_PER_dose_Decrease_past_365d],[rx_AED_PBT_dose_Decrease_past_365d],[rx_AED_PTN_dose_Decrease_past_365d],[rx_AED_PGB_dose_Decrease_past_365d],[rx_AED_PRM_dose_Decrease_past_365d],[rx_AED_RTG_dose_Decrease_past_365d],[rx_AED_RFM_dose_Decrease_past_365d],[rx_AED_TPM_dose_Decrease_past_365d],[rx_AED_VPA_dose_Decrease_past_365d],[rx_AED_VGB_dose_Decrease_past_365d],[rx_AED_ZNS_dose_Decrease_past_365d],
			[rx_AED_BRV_dose_Decrease_past_180d],[rx_AED_CBZ_dose_Decrease_past_180d],[rx_AED_CLB_dose_Decrease_past_180d],[rx_AED_DVP_dose_Decrease_past_180d],[rx_AED_ESL_dose_Decrease_past_180d],[rx_AED_ESM_dose_Decrease_past_180d],[rx_AED_ETN_dose_Decrease_past_180d],[rx_AED_EZG_dose_Decrease_past_180d],[rx_AED_FBM_dose_Decrease_past_180d],[rx_AED_LAC_dose_Decrease_past_180d],[rx_AED_LTG_dose_Decrease_past_180d],[rx_AED_LEV_dose_Decrease_past_180d],[rx_AED_OXG_dose_Decrease_past_180d],[rx_AED_PER_dose_Decrease_past_180d],[rx_AED_PBT_dose_Decrease_past_180d],[rx_AED_PTN_dose_Decrease_past_180d],[rx_AED_PGB_dose_Decrease_past_180d],[rx_AED_PRM_dose_Decrease_past_180d],[rx_AED_RTG_dose_Decrease_past_180d],[rx_AED_RFM_dose_Decrease_past_180d],[rx_AED_TPM_dose_Decrease_past_180d],[rx_AED_VPA_dose_Decrease_past_180d],[rx_AED_VGB_dose_Decrease_past_180d],[rx_AED_ZNS_dose_Decrease_past_180d]
	INTO	[$(TargetSchema)].app_tds_feature_IndexDate_AED
	FROM	#PSPA_IndxDate_AED_Details1 A
	JOIN	#PSPA_IndxDate_AED_Details2 B
	ON		A.patient_id		=	B.patient_id
	AND		A.Valid_Index	=	B.Valid_Index 
	JOIN	#PSPA_IndxDate_AED_Details3 C
	ON		A.patient_id		=	C.patient_id
	AND		A.Valid_Index	=	C.Valid_Index
	JOIN	#PSPA_IndxDate_AED_Details4 D
	ON		A.patient_id		=	D.patient_id
	AND		A.Valid_Index	=	D.Valid_Index
	JOIN	#PSPA_IndxDate_AED_Details5 E
	ON		A.patient_id		=	E.patient_id
	AND		A.Valid_Index	=	E.Valid_Index
	JOIN	#Dosage_Change_365d F
	ON		A.patient_id		=	F.patient_id
	AND		A.Valid_Index	=	F.Valid_Index
	JOIN	#Dosage_Change_180d G
	ON		A.patient_id		=	G.patient_id
	AND		A.Valid_Index	=	G.Valid_Index
	JOIN	#Dosage_Increase_365d H
	ON		A.patient_id		=	H.patient_id
	AND		A.Valid_Index	=	H.Valid_Index
	JOIN	#Dosage_Increase_180d I
	ON		A.patient_id		=	I.patient_id
	AND		A.Valid_Index	=	I.Valid_Index
	JOIN	#Dosage_Decrease_365d J
	ON		A.patient_id		=	J.patient_id
	AND		A.Valid_Index	=	J.Valid_Index
	JOIN	#Dosage_Decrease_180d K
	ON		A.patient_id		=	K.patient_id
	AND		A.Valid_Index	=	K.Valid_Index
	LEFT JOIN #Last_AED_Regimen L
	ON		A.patient_id		=	L.patient_id
	AND		A.Valid_Index	=	L.Valid_Index
