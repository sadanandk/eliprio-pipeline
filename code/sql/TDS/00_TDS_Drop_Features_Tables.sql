-- ------------------------------------------------------------------
-- - AUTHOR    : Suresh G
-- - USED BY   : 
-- - PURPOSE   : Drop all tables related to TDS features
-- - NOTE      : 
-- -			 	 
--				 
-- - Execution : 
-- - Date      : 16-05-2019	
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ---------------------------------------------------------------------- 


IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_DX_Claims]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_DX_Claims]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_DX_Claims_Pivot]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_DX_Claims_Pivot]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_DX_180D_Within_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_DX_180D_Within_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_DX_180D_After_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_DX_180D_After_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Proc_Claims]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Proc_Claims]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Proc_Claims_Pivot]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Proc_Claims_Pivot]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_Proc_180D_Within_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_Proc_180D_Within_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_Proc_180D_After_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_Proc_180D_After_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Eligibility_Data]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_Eligibility_Data]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Regiments_Treatment]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Regiments_Treatment]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_IndexDate_Demographic]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_IndexDate_Demographic]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_cohort]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_cohort]

IF OBJECT_ID('[$(TargetSchema)].app_tds_int_DX_8_to_1_Claims_Pivot') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_DX_8_to_1_Claims_Pivot]

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_IndexDate_CCI_ECI') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_IndexDate_CCI_ECI]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Master_Cohort_Population]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Master_Cohort_Population]

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_IndexDate_AED') IS NOT NULL 
DROP TABLE [$(TargetSchema)].app_tds_feature_IndexDate_AED

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_IndexDate_Payer_Treatment_Provider]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_feature_IndexDate_Payer_Treatment_Provider]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_IndexDate_USC_Others_Epi_Code]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_feature_IndexDate_USC_Others_Epi_Code]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX]') IS NOT NULL
DROP TABLE  [$(TargetSchema)].[app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX]

IF OBJECT_ID('[$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX') IS NOT NULL
DROP TABLE  [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX

IF OBJECT_ID('[$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX') IS NOT NULL 
Drop table [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX

IF OBJECT_ID('[$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX') IS NOT NULL 
Drop table [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_IndexDate_PDC_AED_USC') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_tds_feature_IndexDate_PDC_AED_USC

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_IndexDate_DX') IS NOT NULL 
DROP TABLE [$(TargetSchema)].app_tds_feature_IndexDate_DX

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_IndexDate_Proc') IS NOT NULL 
DROP TABLE [$(TargetSchema)].app_tds_feature_IndexDate_Proc

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_Payer_Provider_Activity_Hospitalization_Demographics') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_tds_feature_Payer_Provider_Activity_Hospitalization_Demographics

IF OBJECT_ID('[$(TargetSchema)].[app_tds_feature_USP_Class]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_feature_USP_Class]

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_Comorbidity') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_tds_feature_Comorbidity

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_BaselineModel_Input_FirstIndex]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_BaselineModel_Input_LongestIndex]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_BaselineModel_Input_LongestIndex]

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_BaselineModel_Input_LastIndex]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_BaselineModel_Input_LastIndex]













