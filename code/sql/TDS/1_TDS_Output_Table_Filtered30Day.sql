-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Identify and create treatment change events,End Events,Early/Late,Index date(multiple) and Treatmentfailure outcome for Entire IMS data  at molecule level.
-- - NOTE      : This Query is implementd with reference to JIRA: ANALYTICS-588.(Molecule treatment <=30 are filtered out in this query but Multiple index date logic has been implemented for Treatment_outcome at regimen level)
-- -			 This table is used in calculating features related to AED and dashboard.
-- - Execution : Final table  name: [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
-- - Date      : 10-OCT-2017
-- - Change History:	Change Date				Change Description
--						31-Oct-2017				Modify Treatment Outcome logic for Defect DT-116	
-- -					04-Jul-2018				Updated for Iteration-2 cohort.	(ANALYTICS-2032)
--						09-Aug-2018				Populated [DS_0_LessThan_0] column to identify patients with days_supply<=0	 (ELREFPRED-553)
-- -					08-Jan-2019				ELCDS-970 : Iteration4 : Removed column 	[DS_0_LessThan_0] , Added columns IsEpilepsy INT,Age INT,PharmacyStability INT ,ContinuousEligibility INT,Q_Rx_Elg INT ,GenPop INT to adopt new criteria for genpop filter.			
-- -					11-Jan-2019				ELCDS-954 : Iteration4 : Replaced First_Rx_Date and Last_Rx_Date with First_enconter and Last_Encounter
-- -					15-Mar-2019				ELCDS-1173 : Iteration4.2 : Affed lookforward filter , removed from calculation :  12 month patient elg ,3 month rx elg , continues elg , Q_rx_elg
--						04-Apr-2019				ELCDS-1401 : Iteration4.2 : Filtered out the patients having overlapping diagnosis codes.
-- ---------------------------------------------------------------------- 

--:setvar Schema Patient_Target
--:Setvar SrcSchema 10_Patient

--------------------------------------------------------Data Prep Activity-----------------------------------------------------------------
Print 'Populate app_int_GE_14_Days_Rule_Copy'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy]

SELECT patient_id,startdate,enddate,aed,aed_days ,maxdate 
INTO [$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy]
FROM [$(TargetSchema)].[app_int_GE_14_Days_Rule] WHERE 1=2

ALTER TABLE [$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy]
ADD	 Prev_AED_Days INT	
	
INSERT INTO  [$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy] (patient_id,startdate,enddate,aed,aed_days ,maxdate,Prev_aed_days)
SELECT patient_id,startdate,enddate,aed,DATEDIFF(dd,maxdate,enddate) +1 AS aed_days ,maxdate,aed_days 
FROM [$(TargetSchema)].[app_int_GE_14_Days_Rule]

Print 'Ignore patients with overlapping diagnosis codes'
Print Getdate()

IF OBJECT_ID('tempdb..#IgnorePatientsWithOverlap') IS NOT NULL
	DROP TABLE #IgnorePatientsWithOverlap

SELECT  DISTINCT Patient_id , DIAG1 INTO #IgnorePatientsWithOverlap
FROM [$(SrcSchema)].[src_dx_claims](NOLOCK) 
WHERE REPLACE(DIAG1,'.','') IN ('E8581','E8582','E8589') 


Print 'Populate app_tds_int_Output_Table_Filtered30Day'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]

SELECT patient_id,startdate,enddate,aed,aed_days,maxdate 
INTO [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
FROM [$(TargetSchema)].[app_int_GE_14_Days_Rule_Copy] WHERE aed_days >=31   AND Patient_id NOT IN (SELECT DISTINCT patient_id FROM #IgnorePatientsWithOverlap )  ---Deviation 1 : Less then 31 days molecule are removed.  , filtered out patients tha has overlapping IDC dagnosis codes

ALTER TABLE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
ADD	First_Encounter DATE,Lookback_Present INT ,Lookforward_Present INT,
	Last_Encounter DATE,Treatment_status VARCHAR(100),
	EndTreatmentChangeEvent VARCHAR(50),Early_Late VARCHAR(100),
	Twelve_Months_Eligible INT , Three_Months_Eligible INT , Stable_Eligible INT ,Index_Date DATE ,Outcome_variable varchar(10) , Zip3 VARCHAR(3) ,Zip1 VARCHAR(1),[State] Varchar(10),IsEpilepsy INT,Age INT,PharmacyStability INT ,ContinuousEligibility INT,Q_Rx_Elg INT ,GenPop INT --,[DS_0_LessThan_0] BIT


-------------------------------------------------------------Updating First Rxand Last Rx date and lookback present flag--------------------------------------------------------------------

Print 'Populate #temp_first_Last_Encounters'
Print Getdate()

UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].First_Encounter =  [$(TargetSchema)].[app_int_Patient_Profile].First_Encounter
,[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Last_Encounter =  [$(TargetSchema)].[app_int_Patient_Profile].Last_Encounter
FROM [$(TargetSchema)].[app_int_Patient_Profile] 
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Patient_id = [$(TargetSchema)].[app_int_Patient_Profile].Patient_id

--select * from [[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] ] where  last_Rx_Date is null or first_Rx_Date is null

UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  
SET Lookback_Present = CASE WHEN DATEDIFF(dd,First_Encounter,Maxdate)+1  >  365 THEN 1  ELSE 0 END
, Lookforward_Present = CASE WHEN DATEDIFF(dd,Maxdate,Last_Encounter)+1  >  365 THEN 1  ELSE 0 END
--select top 1000 * from [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 

--------------------------------Treatment status at molecule level-----------------------------

Print 'Populate #Set_S'
Print Getdate()

IF OBJECT_ID('tempdb..#Set_S') IS NOT NULL
	DROP TABLE #Set_S
	
	SELECT DISTINCT a.patient_id,a.maxdate, a.aed AS SET_S INTO #Set_S 
	FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  A
	LEFT JOIN [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  B
	ON A.PAtient_id =  B.PAtient_id
	AND A.Maxdate > B.Maxdate
	--ORDER BY 1 

	IF OBJECT_ID('tempdb..#BM') IS NOT NULL
	DROP TABLE #BM
	 
	SELECT DISTINCT A.patient_id,a.maxdate, b.maxdate As BM_DATE,B.enddate , B.aed, A.lookback_Present   INTO #BM  FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  A
	LEFT JOIN [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  B
	ON A.PAtient_id =  B.PAtient_id
	AND  A.maxdate > b.maxdate
	AND b.Enddate  > DATEADD(dd,-365,a.maxdate)  


	IF OBJECT_ID('tempdb..#CBM') IS NOT NULL
	DROP TABLE #CBM

	SELECT * INTO #CBM  FROM #BM
	WHERE  enddate < DATEADD(dd,90,maxdate) 

	Print 'Populate #markcancelled'
	Print Getdate()
			
	IF OBJECT_ID('tempdb..#markcancelled') IS NOT NULL
	DROP TABLE #markcancelled

	SELECT DISTINCT  A.patient_id,a.maxdate,b.BM_Date,b.enddate ,B.aed,a.lookback_Present  
	INTO #markcancelled FROM #BM  a
	LEFT JOIN #CBM b
	ON A.patient_id = B.patient_id
	AND a.maxdate > B.maxdate
	--ORDER BY a.maxdate				
	
	IF OBJECT_ID('tempdb..#FINAL_BM') IS NOT NULL
	DROP TABLE #FINAL_BM

	SELECT * INTO #FINAL_BM FROM #BM
	EXCEPT
	SELECT * FROM #markcancelled
	--order by 2

	IF OBJECT_ID('tempdb..#FINAL_CBM') IS NOT NULL
	DROP TABLE #FINAL_CBM

	SELECT * INTO #FINAL_CBM FROM #CBM
	EXCEPT
	SELECT * FROM #MARKCANCELLED
	--order by 2

	Print 'Populate #maintbl'
	Print Getdate()

	IF OBJECT_ID('tempdb..#maintbl') IS NOT NULL
	DROP TABLE #maintbl

	SELECT DISTINCT  patient_id,maxdate,lookback_Present INTO #maintbl FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 

	Print 'Populate #finalStatus_Regimen'
	Print Getdate()

	IF OBJECT_ID('tempdb..#finalStatus_Regimen') IS NOT NULL
	DROP TABLE #finalStatus_Regimen
 
	SELECT *,               	CASE WHEN BM_AED_COUNT = 0 THEN
														   CASE WHEN  SET_S_COUNT = MATCHED_AED_S_PAST THEN  'Restart_After_365_Days'
											   			   ELSE  'New'
														   END
									ELSE	
												 CASE WHEN CBM_AED_COUNT  = 0 THEN 'Add'
													  ELSE  
														  CASE WHEN  SET_S_COUNT = MATCHED_AED_S_CBM THEN  'Restart_Past_AED_treatment'
															   ELSE 'Switch'
															   END
													   END
									END AS Treatment_status INTO #finalStatus_Regimen
	FROM (
	SELECT A.*,	COUNT(DISTINCT SET_S) AS SET_S_COUNT,
				COUNT(DISTINCT BM.AED) AS BM_AED_COUNT , 
				COUNT(DISTINCT CBM.AED) AS CBM_AED_COUNT ,
				COUNT(DISTINCT PAST_AED.PAST_AED) AS MATCHED_AED_S_PAST , 
				COUNT(DISTINCT CBM_MATCHED_AED.MATCHED_AED) AS MATCHED_AED_S_CBM
				
	FROM #MainTbl A
	LEFT JOIN #set_s B
	ON A.Patient_id =B.patient_id
	AND A.MAxDate = B.MaxDate
	LEFT JOIN #FINAL_BM BM
	ON A.Patient_id =BM.patient_id
	AND A.MAxDate = BM.MaxDate
	LEFT JOIN #FINAL_CBM CBM
	ON A.Patient_id =CBM.patient_id
	AND A.MAxDate = CBM.MaxDate
	LEFT JOIN (SELECT DISTINCT A.patient_id,A.maxdate, B.SET_S AS PAST_AED FROM #set_s A
				LEFT JOIN #set_S B
				ON A.patient_id =B.Patient_id
				AND A.maxdate > B.maxdate
			  ) PAST_AED
	ON B.Patient_id = PAST_AED.patient_id
	AND B.MAxDate = PAST_AED.MaxDate
	AND B.SET_S = PAST_AED.PAST_AED
	LEFT JOIN (select distinct A.patient_id,A.maxdate, B.AED AS MATCHED_AED from #set_s A
				LEFT JOIN #FINAL_CBM B
				ON A.patient_id =	B.Patient_id
				AND A.maxdate	=	B.maxdate
				AND A.SET_S		=	B.aed
			  ) CBM_MATCHED_AED
	ON A.Patient_id =	CBM_MATCHED_AED.Patient_id
	AND A.maxDate	=	CBM_MATCHED_AED.MaxDate
	GROUP BY A.patient_id,a.maxdate,a.lookback_Present

	) A
	--ORDER BY Patient_id,maxdate

	
	UPDATE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  
	SET		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Treatment_Status = #finalStatus_Regimen.Treatment_Status
	FROM	#finalStatus_Regimen
	WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].patient_ID = #finalStatus_Regimen.Patient_ID
	AND [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].maxdate = #finalStatus_Regimen.maxdate

--	select * from [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  where Treatment_Status is null

	-------------------------------------------------------------------------- Treatment status End Event Starts -------------------------------------------------------------------
	Print 'Populate #EndEvent_Regimen'
	Print Getdate()

	IF OBJECT_ID('tempdb..#EndEvent_Regimen') IS NOT NULL
	DROP TABLE #EndEvent_Regimen

	SELECT		A.patient_id,
						A.enddate,
						CASE WHEN SUM(CASE WHEN a.enddate BETWEEN b.maxdate AND B.enddate THEN 1 ELSE 0 END) >0 THEN 'Discontinue' ELSE 'Off_AED' END AS EndTreatmentChangeEvent 
						INTO #EndEvent_Regimen
			FROM		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  A
			LEFT JOIN	(	
							SELECT	* 
							FROM	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
						) B
			ON			A.patient_id	= B.patient_id 
			AND			A.enddate		< B.enddate
			GROUP BY	A.patient_id,
						a.enddate


	UPDATE  [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  
	SET		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].EndTreatmentChangeEvent = #EndEvent_Regimen.EndTreatmentChangeEvent
	FROM	#EndEvent_Regimen 
	WHERE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Patient_id = #EndEvent_Regimen.patient_id
	AND		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Enddate = #EndEvent_Regimen.Enddate 


	
----------------------------------------------------------------------------Early Late-----------------------------------------------------------

UPDATE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET		Early_Late = CASE WHEN MAxdate < DATEADD(dd,90,First_Encounter) THEN 'Early' 
			 WHEN MAxdate > DATEADD(dd,-90,Last_Encounter) THEN 'Late'
			 ELSE Treatment_Status 
			 END
			 			 
						
--------------------------------------------------------Index Date Start----------------------------------------------------
Print 'Populate app_tds_int_Eligibility_Data'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Eligibility_Data]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_tds_int_Eligibility_Data]

SELECT *,CASE WHEN RX_ACTIVITY ='Y' OR DX_ACTIVITY = 'Y' THEN 'Y' ELSE 'N' END AS Patient_Eligibility 
INTO [$(TargetSchema)].[app_tds_int_Eligibility_Data]
FROM [$(SrcSchema)].[src_ptnt_eligibility] WHERE patient_id in 
(SELECT DISTINCT patient_id FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] )

CREATE INDEX IX_PatElg_PatID ON [$(TargetSchema)].[app_tds_int_Eligibility_Data](patient_id)
CREATE INDEX IX_PatElg_MntId ON [$(TargetSchema)].[app_tds_int_Eligibility_Data](month_id)

			
Print 'Populate #12MonthEligibility_regimen'
Print Getdate()

IF OBJECT_ID('tempdb..#12MonthEligibility_regimen') IS NOT NULL
DROP TABLE #12MonthEligibility_regimen
		 
SELECT	A.patient_id ,A.maxdate,SUM(CASE WHEN Patient_Eligibility = 'Y' THEN 1 ELSE 0 END) AS Flag 
INTO #12MonthEligibility_regimen
FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] (NOLOCK) A
JOIN [$(TargetSchema)].[app_tds_int_Eligibility_Data] B
ON A.patient_id = B.Patient_id
AND	B.month_id BETWEEN 	REPLACE(SUBSTRING(CAST(A.maxdate AS Varchar),1,7),'-','')  AND REPLACE(SUBSTRING(CAST(dateadd(yy,1,A.maxdate) AS VARCHAR),1,7),'-','') 
GROUP BY A.patient_id ,A.maxdate
--ORDER BY 1,2

UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Twelve_Months_Eligible = CASE WHEN #12MonthEligibility_regimen.flag = 13 THEN 1 ELSE 0 END
FROM #12MonthEligibility_regimen
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].PAtient_id = #12MonthEligibility_regimen.patient_id
AND [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].maxdate =	#12MonthEligibility_regimen.maxdate


Print 'Populate #3MonthEligibility_Regimen'
Print Getdate()

IF OBJECT_ID('tempdb..#3MonthEligibility_Regimen') IS NOT NULL
DROP TABLE #3MonthEligibility_Regimen

SELECT	A.patient_id ,A.maxdate,SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) AS Flag   INTO #3MonthEligibility_Regimen
FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] (NOLOCK) A
JOIN [$(TargetSchema)].[app_tds_int_Eligibility_Data] B
ON A.patient_id = B.Patient_id
AND	B.month_id Between REPLACE(SUBSTRING(CAST(dateadd(mm,-3,A.maxdate) AS VARCHAR),1,7),'-','')  AND REPLACE(SUBSTRING(CAST(A.maxdate AS VARCHAR),1,7),'-','') 
GROUP BY A.patient_id ,A.maxdate
--ORDER BY 1,2


UPDATE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Three_Months_Eligible = CASE WHEN #3MonthEligibility_Regimen.flag = 4 THEN 1 ELSE 0 END
FROM	#3MonthEligibility_Regimen
WHERE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].PAtient_id	=	#3MonthEligibility_Regimen.patient_id
AND		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].maxdate		=	#3MonthEligibility_Regimen.maxdate


----------------------------------------Stable Eligible---------------------------------------------------------
Print 'Populate app_tds_int_Regiments_Treatment'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_Regiments_Treatment]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_tds_int_Regiments_Treatment]

SELECT  --ROW_NUMBER() over(partition by C.Patient_id,Start_Point order by  C.Patient_id,Start_Point,aed) as ID,
		C.Patient_id,
		Start_Point AS Start_Point_Original,
		CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point)  ELSE Start_Point END AS Start_Point,
		End_Point AS End_Point_Original,
		CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END   AS End_Point,
		Strt_End_Combination,
		D.AED,  Abbreviation,
		--DATEDIFF(dd,CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point) ELSE Start_Point END ,CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END ) +1 
		NULL AS Regiment_Days
		INTO [$(TargetSchema)].[app_tds_int_Regiments_Treatment]
FROM (
				SELECT		Patient_id,
							Event_date AS Start_Point, 
							LEAD(Event_Date) OVER (PARTITION BY patient_id ORDER BY Event_Date ) AS End_Point,
							eventstatus+'-'+LEAD(eventstatus) OVER (PARTITION BY patient_id ORDER BY Event_Date ) AS Strt_end_Combination
				FROM		(

							SELECT ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY Event_Date ) rownum,RANK() OVER (PARTITION BY patient_id,Event_Date ORDER BY eventstatus ) rankk,*
							FROM (
									SELECT patient_id,maxdate AS Event_Date , 'Start' AS eventstatus  FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  
									UNION 
									SELECT patient_id ,enddate, 'End'  AS eventstatus FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  
								 )A 
							)B WHERE Rankk =1
				) C
LEFT JOIN  (SELECT patient_id,maxdate,enddate,AED FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] (NOLOCK)) D
ON C.patient_id = D.patient_id
AND C.Start_Point >= D.Maxdate AND C.End_Point <= D.Enddate
LEFT JOIN [$(TargetSchema)].[std_REF_AED_Abbreviation] Abb
ON D.AED = Abb.AED
WHERE D.AED IS NOT NULL



UPDATE [$(TargetSchema)].[app_tds_int_Regiments_Treatment]
SET [$(TargetSchema)].[app_tds_int_Regiments_Treatment].Regiment_Days = DATEDIFF(dd,start_point, End_point) +1 

DELETE FROM [$(TargetSchema)].[app_tds_int_Regiments_Treatment]  WHERE Regiment_days <=0

Print 'Populate #Regiments_Stable'
Print Getdate()

IF OBJECT_ID('tempdb..#Regiments_Stable') IS NOT NULL
DROP TABLE #Regiments_Stable

select ROW_NUMBER() over(partition by Patient_id,Start_Point order by  Patient_id,Start_Point,aed) as ID,* 
INTO #Regiments_Stable 
from [$(TargetSchema)].[app_tds_int_Regiments_Treatment] 

UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  
SET [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Stable_Eligible = CASE WHEN #Regiments_Stable.Regiment_Days >=31 THEN 1 ELSE 0 END 
FROM #Regiments_Stable
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].patient_id = #Regiments_Stable.Patient_id
AND [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Maxdate = #Regiments_Stable.Start_Point_Original 



------------------------------------------Populate Index Date-----------------------------------------
Print 'Populate #Index_Date'
Print Getdate()


IF OBJECT_ID('tempdb..#Index_Date') IS NOT NULL
DROP TABLE #Index_Date

SELECT patient_id,maxdate AS INDEX_DATE INTO #Index_Date
FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] (NOLOCK) WHERE  Treatment_status in ('Add','Switch','New') AND Early_Late NOT IN ('Early','Late') 
/* AND Twelve_Months_Eligible =1  AND Three_Months_Eligible = 1 */ AND  Stable_Eligible =1 AND LOOKBACK_PRESENT = 1 AND Lookforward_Present = 1
GROUP BY patient_id,maxdate


UPDATE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Index_Date =  #Index_Date.INDEX_DATE
FROM	#Index_Date
WHERE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Patient_id	= #Index_Date.Patient_id
AND		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].maxdate	= #Index_Date.INDEX_DATE

--------------------------------------------------------------------------Treatment Outcome-------------------------------------------------------------------

--Logic at Regimen Level
/*UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET Treatment_Failure =  CASE WHEN AED_DAYS >= 365 THEN 0 
							  WHEN AED_DAYS < 365 THEN 1
							  END
WHERE INDEX_DATE IS NOT NULL

*/

			Print 'Populate #Treatment_Outcome_Regimen'
			Print Getdate()

			IF OBJECT_ID('tempdb..#Treatment_Outcome_Regimen') IS NOT NULL
			DROP TABLE #Treatment_Outcome_Regimen

			
			SELECT distinct A.patient_id,A.index_date,CASE WHEN B.cnt IS NULL AND AED_Days >=365 THEN 0 
														   WHEN B.cnt IS NULL AND AED_Days < 365 THEN 1
														   ELSE B.cnt END AS Cnt INTO #Treatment_Outcome_Regimen 
			FROM		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  A
			LEFT JOIN (
						SELECT A.patient_id,B.index_date,COUNT(DISTINCT A.Patient_id)    AS Cnt 
						FROM (
								SELECT patient_id,maxdate,Treatment_status  
								FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]    
								UNION
								SELECT patient_id,Enddate,EndTreatmentChangeEvent 
								FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]   WHERE EndTreatmentChangeEvent <> 'Off_AED'   
							) A
						JOIN [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  B
						ON a.patient_id = B.patient_id
						AND A.maxdate BETWEEN DATEADD(dd,1,B.INDEX_DATE) AND DATEADD(DD,364,B.INDEX_DATE)
						GROUP BY A.patient_id,B.index_date
						) B
			ON A.Patient_id =B.patient_id
			AND a.index_date=B.index_date
			WHERE a.index_date IS NOT NULL 
			--ORDER BY 2,3 

		
			UPDATE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
			SET		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Outcome_variable = CASE WHEN #Treatment_Outcome_Regimen.cnt = 1 THEN 'Failure' ELSE 'Success' END
			FROM	#Treatment_Outcome_Regimen
			WHERE	[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].patient_id	=	#Treatment_Outcome_Regimen.patient_id
			AND		[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].index_date	=	#Treatment_Outcome_Regimen.index_date	

			



-------------------------------------------Zip location -----------------------------------

Print 'Populate #ZipInfo'
Print Getdate()	

IF OBJECT_ID('tempdb..#ZipInfo') IS NOT NULL
DROP TABLE #ZipInfo

SELECT A.patient_id,A.maxdate,PP.ZIP3 AS Zip3_Code,Zip.[Zip1_code],Zip.[State],[YOB] INTO #ZipInfo
FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  A
LEFT JOIN [$(SrcSchema)].[src_ref_patient](nolock)  PP
ON  A.Patient_id =PP.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_USPS_zip3zip1codes] (nolock) Zip
ON PP.Zip3 =Zip.Zip3_Code



UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET  [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Zip3	 = #ZipInfo.Zip3_Code
	,[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Zip1	 = #ZipInfo.Zip1_code 
	,[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].State = #ZipInfo.[State]
FROM #ZipInfo 
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].patient_id = #ZipInfo.patient_id
AND [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].maxdate = #ZipInfo.maxdate


---------------------------------------------Age----------------------------------------
Print 'Populate age'
Print Getdate()	

UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET  [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Age = CAST(SUBSTRING(CAST([$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Maxdate AS VARCHAR),1,4)  AS INT )- YOB 
FROM #ZipInfo 
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].patient_id = #ZipInfo.patient_id
AND [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].maxdate = #ZipInfo.maxdate



--------------------------------------------Q_Rx_Elg-----------------------------------------------


Print 'Populate Q_Rx_Elg'
Print Getdate()	

--##Identifying quarters for each record of the patient 

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
DROP TABLE #Temp1

SELECT *,SUBSTRING(CAST(MONTH_ID AS VARCHAR),1,4) AS YEAR_, SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) AS MONTH_ ,
                     CASE WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('01','02','03') THEN 'Q1'--,
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('04','05','06') THEN 'Q2'--,
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('07','08','09') THEN 'Q3'--,
                           ELSE 'Q4' END AS QUARTER_INFO
INTO #Temp1
FROM [$(SrcSchema)].[src_ptnt_eligibility] 


IF OBJECT_ID('tempdb..#IndexQuarterNo') IS NOT NULL
DROP TABLE #IndexQuarterNo

select DISTINCT A.PATIENT_ID,B.Index_Date,REPLACE(SUBSTRING(CAST(B.Index_Date AS Varchar),1,7),'-','') AS MonthID,CAST([Sr_No_Q] AS INT) AS [Sr_No_Q]
INTO #IndexQuarterNo
from #temp1 A
JOIN [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] B
ON A.patient_id = B.patient_id
AND A.YEAR_+A.Month_=REPLACE(SUBSTRING(CAST(B.Index_Date AS Varchar),1,7),'-','')
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
WHERE B.Index_Date IS NOT NULL


---------------------------------------------------------------------------------------

IF OBJECT_ID('tempdb..#preIndex') IS NOT NULL
DROP TABLE #preIndex

select A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,CAST(C.[Sr_No_Q] AS INT) AS [Sr_No_Q] ,CASE WHEN SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS QUARTER_RX_ELIG
INTO #preIndex
from #temp1 A
JOIN  #IndexQuarterNo B
ON A.patient_id =B.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
where C.Sr_No_Q BETWEEN B.Sr_No_Q-4 AND B.Sr_No_Q
GROUP BY A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,C.[Sr_No_Q]


IF OBJECT_ID('tempdb..#postIndex') IS NOT NULL
DROP TABLE #postIndex

select A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,CAST(C.[Sr_No_Q] AS INT) AS [Sr_No_Q] ,CASE WHEN SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS QUARTER_RX_ELIG
INTO #postIndex
from #temp1 A
JOIN  #IndexQuarterNo B
ON A.patient_id =B.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
where C.Sr_No_Q BETWEEN B.Sr_No_Q AND B.Sr_No_Q+4
GROUP BY A.PATIENT_ID,Index_Date,YEAR_,QUARTER_INFO,C.[Sr_No_Q]


------------------------------------------------------------------------------------------

Print 'Populate #Elg_4_5_Q'
Print Getdate()

IF OBJECT_ID('tempdb..#Elg_4_5_Q_Pre') IS NOT NULL
DROP TABLE #Elg_4_5_Q_Pre

select patient_id,Index_date ,MAX(Sr_No_Q) Maxx ,MIN(Sr_No_Q) Minn,CASE WHEN MAX(Sr_No_Q)-3 <0 THEN 0 ELSE MAX(Sr_No_Q)-3  END as Minus4 
,CASE WHEN MAX(Sr_No_Q)-4 <0 THEN 0 ELSE MAX(Sr_No_Q)-4 END  Minus5 
,CASE WHEN MIN(Sr_No_Q) <= CASE WHEN MAX(Sr_No_Q)-3 <0 THEN 0 ELSE MAX(Sr_No_Q)-3  END THEN  1 ELSE 0 END AS Elg_4 
,CASE WHEN MIN(Sr_No_Q) <= CASE WHEN MAX(Sr_No_Q)-4 <0 THEN 0 ELSE MAX(Sr_No_Q)-4  END THEN  1 ELSE 0 END AS Elg_5
,COUNT(*) cuntt , SUM(QUARTER_RX_ELIG) AS Summ
INTO #Elg_4_5_Q_Pre
from #preIndex 
GROUP BY  patient_id,Index_date




IF OBJECT_ID('tempdb..#Elg_4_5_Q_Post') IS NOT NULL
DROP TABLE #Elg_4_5_Q_Post

select patient_id,Index_date ,MAX(Sr_No_Q) Maxx ,MIN(Sr_No_Q) Minn,MIN(Sr_No_Q)+3 Plus4 
, MIN(Sr_No_Q)+4 Plus5 
,CASE WHEN MAX(Sr_No_Q) >= MIN(Sr_No_Q)+3 THEN  1 ELSE 0 END AS Elg_4 
,CASE WHEN MAX(Sr_No_Q) >= MIN(Sr_No_Q)+4 THEN  1 ELSE 0 END AS Elg_5
,COUNT(*) cuntt , SUM(QUARTER_RX_ELIG) AS Summ
INTO #Elg_4_5_Q_Post
from #postIndex 
GROUP BY  patient_id,Index_date


--------------------------------------------------------------------------------------------------------

--CASE 2 : Patient with 5 quarter eligibility  without ignoring missing values

Print 'Populate #Patient_5Q_Final_List'
Print Getdate()


IF OBJECT_ID('tempdb..#Patient_5Q_Final_List_Pre') IS NOT NULL
DROP TABLE #Patient_5Q_Final_List_Pre

SELECT DISTINCT A.patient_id,A.Index_Date
INTO #Patient_5Q_Final_List_Pre
FROM  #Elg_4_5_Q_Pre A
WHERE Elg_5 = 1 AND Cuntt = 5 AND SUMM = 5


IF OBJECT_ID('tempdb..#Patient_5Q_Final_List_Post') IS NOT NULL
DROP TABLE #Patient_5Q_Final_List_Post

SELECT DISTINCT A.patient_id,A.Index_Date
INTO #Patient_5Q_Final_List_Post
FROM  #Elg_4_5_Q_Post A
WHERE Elg_5 = 1 AND Cuntt = 5 AND SUMM = 5

-----------------------------------------------------------------------------------------------------


SELECT A.patient_id,A.Index_Date,CASE WHEN B.Patient_id IS NOT NULL AND C.Patient_id IS NOT NULL THEN 1 ELSE  0 END As [Q_Rx_Elg]
INTO #FinaiList_Q_Rx_Elg
FROM [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]  A
LEFT JOIN #Patient_5Q_Final_List_Pre B
ON A.patient_id =B.patient_id
AND A.Index_Date = B.Index_Date
LEFT JOIN #Patient_5Q_Final_List_Post C
ON A.patient_id =C.patient_id
AND A.Index_Date = C.Index_Date
WHERE A.Index_Date is not NULL 

UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET  [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].[Q_Rx_Elg] = #FinaiList_Q_Rx_Elg.[Q_Rx_Elg]
FROM #FinaiList_Q_Rx_Elg 
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].patient_id = #FinaiList_Q_Rx_Elg.patient_id
AND [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Index_Date = #FinaiList_Q_Rx_Elg.Index_Date




---------------------------------------IsEpilepsy,PharmacyStability,ContinuousEligibility--------------------------
Print 'Populate IsEpilepsy,PharmacyStability,ContinuousEligibility flags'
Print Getdate()


UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].IsEpilepsy			=  CASE WHEN [$(TargetSchema)].[app_int_Patient_Profile].IsEpilepsy			  = 1 THEN 1 ELSE 0 END
,[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].PharmacyStability		=  CASE WHEN [$(TargetSchema)].[app_int_Patient_Profile].PharmacyStability      = 1 THEN 1 ELSE 0 END 
,[$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].ContinuousEligibility =  CASE WHEN [$(TargetSchema)].[app_int_Patient_Profile].ContinuousEligibility  = 1 THEN 1 ELSE 0 END 
FROM [$(TargetSchema)].[app_int_Patient_Profile] 
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].Patient_id = [$(TargetSchema)].[app_int_Patient_Profile].Patient_id



--------------------------------------------------------GenPop--------------------------------------------------------

Print 'Populate Genpop flags'
Print Getdate()


/* Note: Pharmacy data is not available in current pipeline hence commented the pharmacy related filter criteria for genpop */
UPDATE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day] 
SET [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].[GenPop] = CASE WHEN IsEpilepsy = 1 AND Age >= 18 /*  AND PharmacyStability =1  AND  ContinuousEligibility = 1 AND  Q_Rx_Elg = 1 */ THEN 1 ELSE 0 END
WHERE [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day].[Index_date] IS NOT NULL


------------------------------------------------------------------------------------------------------------------------------------
