---********************************************************* PDC AT OVERALL AED LEVEL********************************************************************
-- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create the table to store the features related MPR AED ,MPR Others ,MPR USC in raw data
-- - NOTE      : Final Table : app_tds_feature_IndexDate_PDC_AED_USC
--				 
--				 
-- - Execution : 
-- - Date      : 20-JAN-2018		
-- - Change History:	Change Date				Change Description
-- -					5-Feb-2018				Defect ANALYTICS-1441  resolved the defect for aed generation.
-- -					6-Feb-2018				Defect ANALYTICS-1416  added valid_index column in processed table.
--						7-Sep-2018				Apply filter of Days_Supply >0 [ANALYTICS-2208]

	-- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX]') IS NOT NULL
DROP TABLE  [$(TargetSchema)].[app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX]

select Row_Number()over(Partition by A.Patient_id,A.valid_index,D.AED order by A.Patient_id,A.valid_index,D.AED,B.Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date)) as Num,A.valid_index,AED_Generation,A.Patient_id,D.AED AS GENERIC_NAME,Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date) AS EndDate,Service_date as New_Service_Date ,DATEADD(DD,Days_Supply-1,B.Service_Date) AS New_EndDate
into [$(TargetSchema)].[app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
					ON A.patient_id=B.patient_id 
					AND B.service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
					LEFT JOIN  [$(SrcSchema)].[src_ref_product](NOLOCK) C
					ON B.NDC = C.NDC
					LEFT JOIN [$(TargetSchema)].[std_ref_AEDName] D
					ON C.Generic_name = D.Generic_Name
					LEFT JOIN [$(TargetSchema)].[std_ref_AED_Abbreviation] E
					ON D.AED = E.AED
					WHERE Days_Supply > 0 AND D.GENERIC_NAME IS NOT NULL 



Declare @max_AED_PDC_365d INT
Select @max_AED_PDC_365d=max(num) from [$(TargetSchema)].[app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX]

Set Nocount on
Declare @num_AED_PDC_365d bigint 
Set @num_AED_PDC_365d=1

While @num_AED_PDC_365d<=@max_AED_PDC_365d
Begin

       Select A.Patient_id,A.valid_Index,A.Generic_name,Service_Date_B,EndDate_B,Num_B,Datediff(dd,B.Service_Date_B,A.New_EndDate)+1 as day_diff into #tmp  
       from [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX A
       cross Apply (Select B.New_Service_Date as Service_Date_B,B.New_EndDate as EndDate_B,B.Num as Num_B From [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX B where A.Num=B.Num-1 and A.Patient_id=B.Patient_id and A.Valid_index=B.Valid_index AND  A.Generic_name=B.Generic_name and A.Num=@num_AED_PDC_365d)B
       where Datediff(dd,B.Service_Date_B,A.New_EndDate)+1>0
	 

       Update [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX
       Set [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX.New_Service_Date=dateadd(dd,day_diff,B.Service_Date_B),
              [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX.New_EndDate=dateadd(dd,day_diff,B.EndDate_B)
       From #tmp B
       Where [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX.Num=B.Num_B and [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX.Patient_id=B.Patient_id and [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX.Valid_Index=B.Valid_Index and [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX.Generic_name=B.Generic_name

       Drop Table #tmp
       Set @num_AED_PDC_365d=@num_AED_PDC_365d+1

End

Print 'Populate #PSPA_Regiments_Treatment_RAW_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_Regiments_Treatment_RAW_365d') IS NOT NULL
DROP TABLE #PSPA_Regiments_Treatment_RAW_365d

SELECT  --ROW_NUMBER() over(partition by C.Patient_id,Start_Point order by  C.Patient_id,Start_Point,aed) as ID,
		C.Patient_id,C.Valid_Index,
		Start_Point AS Start_Point_Original,
		CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point)  ELSE Start_Point END AS Start_Point,
		End_Point AS End_Point_Original,
		CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END   AS End_Point,
		Strt_End_Combination,
		D.AED,  Abbreviation,
		--DATEDIFF(dd,CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point) ELSE Start_Point END ,CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END ) +1 
		NULL AS Regiment_Days
		INTO #PSPA_Regiments_Treatment_RAW_365d
FROM (
				SELECT		Patient_id,
							Valid_index,
							Event_date AS Start_Point, 
							LEAD(Event_Date) OVER (PARTITION BY patient_id,Valid_index ORDER BY Event_Date ) AS End_Point,
							eventstatus+'-'+LEAD(eventstatus) OVER (PARTITION BY patient_id,Valid_index ORDER BY Event_Date ) AS Strt_end_Combination
				FROM		(

							SELECT ROW_NUMBER() OVER (PARTITION BY patient_id,Valid_Index ORDER BY Event_Date ) rownum,RANK() OVER (PARTITION BY patient_id,Valid_index,Event_Date ORDER BY eventstatus ) rankk,*
							FROM (
									SELECT patient_id,Valid_index,New_Service_date AS Event_Date , 'Start' AS eventstatus  FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX 
									UNION 
									SELECT patient_id ,Valid_index,New_enddate as enddate, 'End'  AS eventstatus FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX 
								 )A 
							)B WHERE Rankk =1
				) C
LEFT JOIN  (SELECT patient_id,Valid_Index,New_Service_date as maxdate, New_enddate AS enddate,GENERIC_NAME AS AED FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX(NOLOCK)) D
ON C.patient_id = D.patient_id
AND C.Valid_Index=D.Valid_Index
AND C.Start_Point >= D.Maxdate AND C.End_Point <= D.Enddate
LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation Abb
ON D.AED = Abb.AED
WHERE D.AED IS NOT NULL


UPDATE #PSPA_Regiments_Treatment_RAW_365d
SET #PSPA_Regiments_Treatment_RAW_365d.Regiment_Days = DATEDIFF(dd,start_point, End_point) +1 

DELETE FROM #PSPA_Regiments_Treatment_RAW_365d  WHERE Regiment_days <=0

IF OBJECT_ID('tempdb..#Regiments_Stable_365d') IS NOT NULL
DROP TABLE #Regiments_Stable_365d

SELECT ROW_NUMBER() OVER(PARTITION BY Patient_id,Valid_index,Start_Point ORDER BY  Patient_id,Valid_Index,Start_Point,abbreviation) AS ID,*
INTO #Regiments_Stable_365d
FROM #PSPA_Regiments_Treatment_RAW_365d 

Print 'Populate #Regimen_days_AED_PDC_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#Regimen_days_AED_PDC_365d') IS NOT NULL
DROP TABLE #Regimen_days_AED_PDC_365d

SELECT DISTINCT Patient_id,Valid_Index,start_point,End_Point,AED,DATEDIFF(dd,start_Point,End_Point)+1 AS AED_Days  
				into #Regimen_days_AED_PDC_365d  
				FROM (
				SELECT Patient_id,Valid_Index,start_Point,End_Point, ISNULL([1],'') +ISNULL('__'+[2],'')+ISNULL('__'+[3],'')+ISNULL('__'+[4],'')+ISNULL('__'+[5],'')+ISNULL('__'+[6],'')+ISNULL('__'+[7],'')+ISNULL('__'+[8],'')+ISNULL('__'+[9],'')+ISNULL('__'+[10],'')+ISNULL('__'+[11],'')+ISNULL('__'+[12],'')+ISNULL('__'+[13],'')+ISNULL('__'+[14],'')+ISNULL('__'+[15],'')+ISNULL('__'+[16],'')+ISNULL('__'+[17],'')+ISNULL('__'+[18],'')+ISNULL('__'+[19],'')+ISNULL('__'+[20],'') AS AED
	
					FROM
					(SELECT ID,Patient_id,Valid_Index,start_Point,End_Point,B.Abbreviation AS AED1
					FROM #Regiments_Stable_365d A LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation B
					ON	 A.aed=B.aed 
					) AS SourceTable
					PIVOT
					(
					max(AED1)
					FOR ID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
					) AS PivotTable
				) A
					WHERE AED <> ''

Print 'Populate #AED_Start_End_Dates_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#AED_Start_End_Dates_365d') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_365d

SELECT A.patient_id,A.Valid_Index,MIN(B.start_point) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.start_point) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_365d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT  JOIN #Regimen_days_AED_PDC_365d B
ON A.patient_id=B.Patient_id
AND A.Valid_INdex = B.Valid_Index
AND B.start_point BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index


IF OBJECT_ID('tempdb..#PDC_365d_All') IS NOT NULL
DROP TABLE #PDC_365d_All

select A.patient_id,A.Valid_Index--,start_point,end_point,AED_DAYS,CASE WHEN END_POINT >= Valid_Index THEN DATEDIFF(DD,start_point,Valid_Index) ELSE AED_DAYS END
--,SUM(CASE WHEN END_POINT >= Valid_Index THEN DATEDIFF(DD,start_point,Valid_Index) ELSE AED_DAYS END),AVG(Denometer)
,CAST(ISNULL(SUM(CASE WHEN END_POINT >= A.Valid_Index THEN DATEDIFF(DD,start_point,A.Valid_Index) ELSE AED_DAYS END)*1.0 /AVG(Denometer) , 0.0000) AS DECIMAL(6,4)) AS PDC_All_AED_Last_365d
INTO #PDC_365d_All
from #AED_Start_End_Dates_365d A
left join #Regimen_days_AED_PDC_365d B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index
AND B.start_point BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index


------------------------------------------------PDC AED LEVEL 365 Days-----------------------------------------------------

Print 'Populate #AED_Start_End_Dates_365d_AED'
Print Getdate()

IF OBJECT_ID('tempdb..#AED_Start_End_Dates_365d_AED') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_365d_AED

SELECT A.patient_id,A.Valid_Index,B.generic_name AS AED,C.Abbreviation,MIN(B.New_Service_date) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.New_Service_date) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_365d_AED
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT JOIN [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX B
ON A.patient_id=B.Patient_id
AND A.Valid_Index=B.Valid_Index
AND B.New_Service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation C
ON B.generic_name=C.AED
GROUP BY A.patient_id,A.Valid_Index,B.generic_name,C.Abbreviation

Print 'Populate #PDC_365d_AED'
Print Getdate()

IF OBJECT_ID('tempdb..#PDC_365d_AED') IS NOT NULL
DROP TABLE #PDC_365d_AED

SELECT Patient_id,Valid_Index,
 CAST(ISNULL(BRV,0.0000) AS DECIMAL(6,4)) AS PDC_BRV_Last_365d
,CAST(ISNULL(CBZ,0.0000) AS DECIMAL(6,4)) AS PDC_CBZ_Last_365d
,CAST(ISNULL(CLB,0.0000) AS DECIMAL(6,4)) AS PDC_CLB_Last_365d
,CAST(ISNULL(DVP,0.0000) AS DECIMAL(6,4)) AS PDC_DVP_Last_365d
,CAST(ISNULL(ESL,0.0000) AS DECIMAL(6,4)) AS PDC_ESL_Last_365d
,CAST(ISNULL(ESM,0.0000) AS DECIMAL(6,4)) AS PDC_ESM_Last_365d
,CAST(ISNULL(ETN,0.0000) AS DECIMAL(6,4)) AS PDC_ETN_Last_365d
,CAST(ISNULL(EZG,0.0000) AS DECIMAL(6,4)) AS PDC_EZG_Last_365d
,CAST(ISNULL(FBM,0.0000) AS DECIMAL(6,4)) AS PDC_FBM_Last_365d
,CAST(ISNULL(LAC,0.0000) AS DECIMAL(6,4)) AS PDC_LAC_Last_365d
,CAST(ISNULL(LTG,0.0000) AS DECIMAL(6,4)) AS PDC_LTG_Last_365d
,CAST(ISNULL(LEV,0.0000) AS DECIMAL(6,4)) AS PDC_LEV_Last_365d
,CAST(ISNULL(OXG,0.0000) AS DECIMAL(6,4)) AS PDC_OXG_Last_365d
,CAST(ISNULL(PER,0.0000) AS DECIMAL(6,4)) AS PDC_PER_Last_365d
,CAST(ISNULL(PBT,0.0000) AS DECIMAL(6,4)) AS PDC_PBT_Last_365d
,CAST(ISNULL(PTN,0.0000) AS DECIMAL(6,4)) AS PDC_PTN_Last_365d
,CAST(ISNULL(PGB,0.0000) AS DECIMAL(6,4)) AS PDC_PGB_Last_365d
,CAST(ISNULL(PRM,0.0000) AS DECIMAL(6,4)) AS PDC_PRM_Last_365d
,CAST(ISNULL(RTG,0.0000) AS DECIMAL(6,4)) AS PDC_RTG_Last_365d
,CAST(ISNULL(RFM,0.0000) AS DECIMAL(6,4)) AS PDC_RFM_Last_365d
,CAST(ISNULL(TPM,0.0000) AS DECIMAL(6,4)) AS PDC_TPM_Last_365d
,CAST(ISNULL(VPA,0.0000) AS DECIMAL(6,4)) AS PDC_VPA_Last_365d
,CAST(ISNULL(VGB,0.0000) AS DECIMAL(6,4)) AS PDC_VGB_Last_365d
,CAST(ISNULL(ZNS,0.0000) AS DECIMAL(6,4)) AS PDC_ZNS_Last_365d
INTO #PDC_365d_AED
FROM 
(SELECT A.patient_id,A.Valid_Index,Abbreviation, PDC AS COUNTT
FROM 
		(
		
				SELECT A.patient_id ,A.Valid_Index,A.AED,A.Abbreviation
						--,SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<NEW_EndDate THEN DATEDIFF(DD,New_Enddate,Next_Maxdate)-1 ELSE 0 END) AS Drug_Days  /* Just For Display */
						--,AVG(Denometer) AS Denometer   /* Just for Display */
						,ISNULL(CAST((SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<=NEW_EndDate THEN DATEDIFF(DD,New_Enddate,Next_Maxdate)-1 ELSE 0 END))*1.0/AVG(Denometer) AS DECIMAL(6,4)),0.0000) AS PDC
				FROM 
				(
					SELECT A.patient_id,A.Valid_Index,B.New_Service_date,B.New_Enddate as New_Enddatee,B.generic_name as AED,C.Abbreviation
							,CASE WHEN A.Valid_Index >=New_Enddate THEN New_Enddate  ELSE  DATEADD(DD,-1,A.Valid_Index) END AS New_EndDate 
							,DATEDIFF(DD,New_Service_date,CASE WHEN A.Valid_Index >New_Enddate THEN New_Enddate  ELSE  DATEADD(DD,-1,A.Valid_Index) END)+1 New_AED_Days
							,LEAD(New_Service_date,1) OVER (PARTITION BY A.Patient_id,A.Valid_Index,B.generic_name ORDER BY New_Service_date) Next_Maxdate
					FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT  JOIN [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX B
					ON A.patient_id=B.Patient_id
					AND A.Valid_Index=B.Valid_Index
					AND B.New_Service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
					LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation C
					ON B.generic_name=C.AED
					
				)A
				left join #AED_Start_End_Dates_365d_AED B
				ON A.patient_id= B.patient_id
				AND A.Valid_Index = B.Valid_Index
				AND A.AED=B.AED
				GROUP BY A.patient_id ,A.Valid_Index,A.AED,A.Abbreviation


		)  A 

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable


------------------------------------------------PDC ALL AED 180 Days-----------------------------------------------
Print 'Populate app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX') IS NOT NULL
DROP TABLE  [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX

select Row_Number()over(Partition by A.Patient_id,A.Valid_Index,D.AED order by A.Patient_id,A.Valid_Index,D.AED,B.Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date)) as Num,A.Valid_Index,A.Patient_id,D.AED AS GENERIC_NAME,AED_Generation,Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date) AS EndDate,Service_date as New_Service_Date ,DATEADD(DD,Days_Supply-1,B.Service_Date) AS New_EndDate
into [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
					ON A.patient_id=B.patient_id 
					AND B.service_date BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
					LEFT JOIN  [$(SrcSchema)].[src_ref_product](NOLOCK) C
					ON B.NDC = C.NDC
					LEFT JOIN [$(TargetSchema)].[std_ref_AEDName] D
					ON C.Generic_name = D.Generic_Name
					LEFT JOIN [$(TargetSchema)].[std_ref_AED_Abbreviation] E
					ON D.AED = E.AED
					WHERE Days_Supply > 0 AND D.GENERIC_NAME IS NOT NULL


Declare @max_AED_PDC_180d INT
Select @max_AED_PDC_180d=max(num) from [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX

Set Nocount on
Declare @num_AED_PDC_180d bigint 
Set @num_AED_PDC_180d=1

While @num_AED_PDC_180d<=@max_AED_PDC_180d
Begin

       Select A.Patient_id,A.Valid_Index,A.Generic_name,Service_Date_B,EndDate_B,Num_B,Datediff(dd,B.Service_Date_B,A.New_EndDate)+1 as day_diff 
	   into #tmp1  
       from [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX A
       cross Apply (Select B.New_Service_Date as Service_Date_B,B.New_EndDate as EndDate_B,B.Num as Num_B 
	   From [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX B 
	   where A.Num=B.Num-1 and A.Patient_id=B.Patient_id and A.Valid_index=B.Valid_index AND A.Generic_name=B.Generic_name and A.Num=@num_AED_PDC_180d)B
       where Datediff(dd,B.Service_Date_B,A.New_EndDate)+1>0

       Update [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX
       Set [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX.New_Service_Date=dateadd(dd,day_diff,B.Service_Date_B),
              [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX.New_EndDate=dateadd(dd,day_diff,B.EndDate_B)
       From #tmp1 B
       Where [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX.Num=B.Num_B 
	   and [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX.Patient_id=B.Patient_id 
	   and  [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX.Valid_Index=B.Valid_Index 
	   and  [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX.Generic_name=B.Generic_name

       Drop Table #tmp1
       Set @num_AED_PDC_180d=@num_AED_PDC_180d+1

End


Print 'Populate #PSPA_Regiments_Treatment_RAW_180d'
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_Regiments_Treatment_RAW_180d') IS NOT NULL
DROP TABLE #PSPA_Regiments_Treatment_RAW_180d

SELECT  --ROW_NUMBER() over(partition by C.Patient_id,Start_Point order by  C.Patient_id,Start_Point,aed) as ID,
		C.Patient_id,C.Valid_Index,
		Start_Point AS Start_Point_Original,
		CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point)  ELSE Start_Point END AS Start_Point,
		End_Point AS End_Point_Original,
		CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END   AS End_Point,
		Strt_End_Combination,
		D.AED,  Abbreviation,
		--DATEDIFF(dd,CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point) ELSE Start_Point END ,CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END ) +1 
		NULL AS Regiment_Days
		INTO #PSPA_Regiments_Treatment_RAW_180d
FROM (
				SELECT		Patient_id,
							Valid_Index,
							Event_date AS Start_Point, 
							LEAD(Event_Date) OVER (PARTITION BY patient_id,Valid_Index ORDER BY Event_Date ) AS End_Point,
							eventstatus+'-'+LEAD(eventstatus) OVER (PARTITION BY patient_id,Valid_Index ORDER BY Event_Date ) AS Strt_end_Combination
				FROM		(

							SELECT ROW_NUMBER() OVER (PARTITION BY patient_id,Valid_Index ORDER BY Event_Date ) rownum,RANK() OVER (PARTITION BY patient_id,Valid_Index,Event_Date ORDER BY eventstatus ) rankk,*
							FROM (
									SELECT patient_id,Valid_Index,New_Service_date AS Event_Date , 'Start' AS eventstatus  FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX 
									UNION 
									SELECT patient_id ,Valid_Index,New_enddate as enddate, 'End'  AS eventstatus FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX 
								 )A 
							)B WHERE Rankk =1
				) C
LEFT JOIN  (SELECT patient_id,Valid_Index,New_Service_date as maxdate, New_enddate AS enddate,GENERIC_NAME AS AED FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX(NOLOCK)) D
ON C.patient_id = D.patient_id
AND C.Valid_Index =D.Valid_Index
AND C.Start_Point >= D.Maxdate AND C.End_Point <= D.Enddate
LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation Abb
ON D.AED = Abb.AED
WHERE D.AED IS NOT NULL


UPDATE #PSPA_Regiments_Treatment_RAW_180d
SET #PSPA_Regiments_Treatment_RAW_180d.Regiment_Days = DATEDIFF(dd,start_point, End_point) +1 

DELETE FROM #PSPA_Regiments_Treatment_RAW_180d  WHERE Regiment_days <=0


IF OBJECT_ID('tempdb..#Regiments_Stable_180d') IS NOT NULL
DROP TABLE #Regiments_Stable_180d

SELECT ROW_NUMBER() OVER(PARTITION BY Patient_id,Valid_Index,Start_Point ORDER BY  Patient_id,Valid_Index,Start_Point,abbreviation) AS ID,*
INTO #Regiments_Stable_180d 
FROM #PSPA_Regiments_Treatment_RAW_180d

Print 'Populate #Regimen_days_AED_PDC_180d'
Print Getdate()

IF OBJECT_ID('tempdb..#Regimen_days_AED_PDC_180d') IS NOT NULL		
DROP TABLE #Regimen_days_AED_PDC_180d

SELECT DISTINCT Patient_id,Valid_Index,start_point,End_Point,AED,DATEDIFF(dd,start_Point,End_Point)+1 AS AED_Days  into #Regimen_days_AED_PDC_180d  FROM (
				SELECT Patient_id,Valid_Index,start_Point,End_Point, ISNULL([1],'') +ISNULL('__'+[2],'')+ISNULL('__'+[3],'')+ISNULL('__'+[4],'')+ISNULL('__'+[5],'')+ISNULL('__'+[6],'')+ISNULL('__'+[7],'')+ISNULL('__'+[8],'')+ISNULL('__'+[9],'')+ISNULL('__'+[10],'')+ISNULL('__'+[11],'')+ISNULL('__'+[12],'')+ISNULL('__'+[13],'')+ISNULL('__'+[14],'')+ISNULL('__'+[15],'')+ISNULL('__'+[16],'')+ISNULL('__'+[17],'')+ISNULL('__'+[18],'')+ISNULL('__'+[19],'')+ISNULL('__'+[20],'') AS AED
	
					FROM
					(SELECT ID,Patient_id,Valid_Index,start_Point,End_Point,B.Abbreviation AS AED1
					FROM #Regiments_Stable_180d A LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation B
					ON	 A.aed=B.aed 
					) AS SourceTable
					PIVOT
					(
					max(AED1)
					FOR ID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
					) AS PivotTable
				) A
					WHERE AED <> ''

Print 'Populate #AED_Start_End_Dates_180d'
Print Getdate()

IF OBJECT_ID('tempdb..#AED_Start_End_Dates_180d') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_180d

SELECT A.patient_id,A.Valid_Index,MIN(B.start_point) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.start_point) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_180d
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT  JOIN #Regimen_days_AED_PDC_180d B
ON A.patient_id=B.Patient_id
AND A.Valid_Index=B.Valid_Index
AND B.start_point BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index

Print 'Populate #PDC_180d_All'
Print Getdate()

IF OBJECT_ID('tempdb..#PDC_180d_All') IS NOT NULL
DROP TABLE #PDC_180d_All

select A.patient_id,A.Valid_Index
--,SUM(CASE WHEN END_POINT >= Valid_Index THEN DATEDIFF(DD,start_point,Valid_Index) ELSE AED_DAYS END),AVG(Denometer)
,CAST(ISNULL(SUM(CASE WHEN END_POINT >= A.Valid_Index THEN DATEDIFF(DD,start_point,A.Valid_Index) ELSE AED_DAYS END)*1.0 /AVG(Denometer),0.0000) AS DECIMAL(6,4)) AS PDC_All_AED_Last_180d
INTO #PDC_180d_All
from #AED_Start_End_Dates_180d A
left join #Regimen_days_AED_PDC_180d B
ON A.patient_id=B.patient_id
AND A.Valid_INdex = B.Valid_Index
AND B.start_point BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
--where A.patient_id =49374793
GROUP BY A.patient_id,A.Valid_Index


------------------------------------------------PDC AED LEVEL 180 Days-----------------------------------------------------

--0:22 min
Print 'Populate #AED_Start_End_Dates_180d_AED'
Print Getdate()

IF OBJECT_ID('tempdb..#AED_Start_End_Dates_180d_AED') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_180d_AED

SELECT A.patient_id,A.Valid_Index,B.generic_name AS AED,C.Abbreviation,MIN(B.New_Service_date) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.New_Service_date) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_180d_AED
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT JOIN [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX B
ON A.patient_id=B.Patient_id
AND A.Valid_Index = B.Valid_Index
AND B.New_Service_date BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation C
ON B.generic_name=C.AED
GROUP BY A.patient_id,A.Valid_Index,B.generic_name,C.Abbreviation

Print 'Populate #PDC_180d_AED'
Print Getdate()

IF OBJECT_ID('tempdb..#PDC_180d_AED') IS NOT NULL
DROP TABLE #PDC_180d_AED

SELECT Patient_id,Valid_Index,
 CAST(ISNULL(BRV,0.0000) AS DECIMAL(6,4)) AS PDC_BRV_Last_180d
,CAST(ISNULL(CBZ,0.0000) AS DECIMAL(6,4)) AS PDC_CBZ_Last_180d
,CAST(ISNULL(CLB,0.0000) AS DECIMAL(6,4)) AS PDC_CLB_Last_180d
,CAST(ISNULL(DVP,0.0000) AS DECIMAL(6,4)) AS PDC_DVP_Last_180d
,CAST(ISNULL(ESL,0.0000) AS DECIMAL(6,4)) AS PDC_ESL_Last_180d
,CAST(ISNULL(ESM,0.0000) AS DECIMAL(6,4)) AS PDC_ESM_Last_180d
,CAST(ISNULL(ETN,0.0000) AS DECIMAL(6,4)) AS PDC_ETN_Last_180d
,CAST(ISNULL(EZG,0.0000) AS DECIMAL(6,4)) AS PDC_EZG_Last_180d
,CAST(ISNULL(FBM,0.0000) AS DECIMAL(6,4)) AS PDC_FBM_Last_180d
,CAST(ISNULL(LAC,0.0000) AS DECIMAL(6,4)) AS PDC_LAC_Last_180d
,CAST(ISNULL(LTG,0.0000) AS DECIMAL(6,4)) AS PDC_LTG_Last_180d
,CAST(ISNULL(LEV,0.0000) AS DECIMAL(6,4)) AS PDC_LEV_Last_180d
,CAST(ISNULL(OXG,0.0000) AS DECIMAL(6,4)) AS PDC_OXG_Last_180d
,CAST(ISNULL(PER,0.0000) AS DECIMAL(6,4)) AS PDC_PER_Last_180d
,CAST(ISNULL(PBT,0.0000) AS DECIMAL(6,4)) AS PDC_PBT_Last_180d
,CAST(ISNULL(PTN,0.0000) AS DECIMAL(6,4)) AS PDC_PTN_Last_180d
,CAST(ISNULL(PGB,0.0000) AS DECIMAL(6,4)) AS PDC_PGB_Last_180d
,CAST(ISNULL(PRM,0.0000) AS DECIMAL(6,4)) AS PDC_PRM_Last_180d
,CAST(ISNULL(RTG,0.0000) AS DECIMAL(6,4)) AS PDC_RTG_Last_180d
,CAST(ISNULL(RFM,0.0000) AS DECIMAL(6,4)) AS PDC_RFM_Last_180d
,CAST(ISNULL(TPM,0.0000) AS DECIMAL(6,4)) AS PDC_TPM_Last_180d
,CAST(ISNULL(VPA,0.0000) AS DECIMAL(6,4)) AS PDC_VPA_Last_180d
,CAST(ISNULL(VGB,0.0000) AS DECIMAL(6,4)) AS PDC_VGB_Last_180d
,CAST(ISNULL(ZNS,0.0000) AS DECIMAL(6,4)) AS PDC_ZNS_Last_180d
INTO #PDC_180d_AED
FROM 
(SELECT A.patient_id,A.Valid_Index,Abbreviation, PDC AS COUNTT
FROM 
		(
		
				SELECT A.patient_id ,A.Valid_Index,A.AED,A.Abbreviation
						--,SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<NEW_EndDate THEN DATEDIFF(DD,New_Enddate,Next_Maxdate)-1 ELSE 0 END) AS Drug_Days  /* Just For Display */
						--,AVG(Denometer) AS Denometer   /* Just for Display */
						,ISNULL(CAST((SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<=NEW_EndDate THEN DATEDIFF(DD,New_Enddate,Next_Maxdate)-1 ELSE 0 END))*1.0/AVG(Denometer) AS DECIMAL(6,4)),0.0000) AS PDC
				FROM 
				(
					SELECT A.patient_id,A.Valid_Index,B.New_Service_date,B.New_Enddate as New_Enddatee,B.generic_name as AED,C.Abbreviation
							,CASE WHEN A.Valid_Index >=New_Enddate THEN New_Enddate  ELSE  DATEADD(DD,-1,A.Valid_Index) END AS New_EndDate 
							,DATEDIFF(DD,New_Service_date,CASE WHEN A.Valid_Index >New_Enddate THEN New_Enddate  ELSE  DATEADD(DD,-1,A.Valid_Index) END)+1 New_AED_Days
							,LEAD(New_Service_date,1) OVER (PARTITION BY A.Patient_id,A.Valid_Index,B.generic_name ORDER BY New_Service_date) Next_Maxdate
					FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT  JOIN [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX B
					ON A.patient_id=B.Patient_id
					AND A.Valid_Index =B.Valid_Index
					AND B.New_Service_date BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
					LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation C
					ON B.generic_name=C.AED
				)A
				left join #AED_Start_End_Dates_180d_AED B
				ON A.patient_id= B.patient_id
				AND A.Valid_Index = B.Valid_Index
				AND A.AED=B.AED
				GROUP BY A.patient_id ,A.Valid_Index,A.AED,A.Abbreviation


		)  A 

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR Abbreviation IN ([BRV],[CBZ],[CLB],[DVP],[ESL],[ESM],[ETN],[EZG],[FBM],[LAC],[LTG],[LEV],
[OXG],[PER],[PBT],[PTN],[PGB],[PRM],[RTG],[RFM],[TPM],[VPA],[VGB],[ZNS])) AS PivotTable



--*****************************************************************USC PDC **************************************************************************************************


--------------------------------------Create Processed data for USC (remove the overlap)------------------------------------------------------------

Print 'Populate app_tds_int_USC_List_Feature_365d_ADD_INDEX'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX') IS NOT NULL 
Drop table [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX

select Row_Number()over(Partition by A.Patient_id,A.Valid_Index,USC_Name order by A.Patient_id,A.Valid_Index,USC_Name,B.Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date)) as Num,A.Valid_Index,A.Patient_id,USC_Name,Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date) AS EndDate,Service_date as New_Service_Date ,DATEADD(DD,Days_Supply-1,B.Service_Date) AS New_EndDate
into [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT JOIN  [$(SrcSchema)].src_rx_claims (NOLOCK)B
					ON A.patient_id=B.patient_id 
					AND B.service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
					LEFT JOIN  [$(SrcSchema)].src_ref_product(NOLOCK) C
					ON B.NDC = C.NDC
					WHERE Days_Supply > 0 AND USC_NAME in (
'ACETAMINOPHEN'
,'ADENOSINE RECEPTOR ANTAGONISTS, ALONE/COMB'
,'ALPHA BLOCKERS, ALONE, COMBINATION'
,'ALPHA-BETA BLOCKER'
,'ALZHEIMER-TYPE DEMENTIA'
,'AMINOPENICILLINS'
,'ANALEPTICS'
,'ANESTH LOCAL & TOPICAL, OTHER'
,'ANTACIDS, PLAIN'
,'ANTIANXIETY, OTHER'
,'ANTI-ARRHYTHMIA AGENT'
,'ANTIARTH, COX-2 INHIBITORS'
,'ANTIARTH, GOUT SPECIFIC'
,'ANTIARTH, PLAIN'
,'ANTIDOPA PHENOTHIAZINE'
,'ANTIHYPERLIPIDEMIC AGENT, OTHER'
,'ANTIMALARIALS'
,'ANTI-MANIA'
,'ANTI-MIGRAINE, COMBINATIONS'
,'ANTINAUSEANT 5HT3 ANTAGONISTS'
,'ANTINEOPLASTIC FOLIC ACID ANALOGS'
,'ANTI-OBESITY, SYSTEMIC'
,'ANTI-PARKINSON, OTHER'
,'ANTI-ULCERANTS, OTHER'
,'BARB LONG-ACTING'
,'BENZODIAZEPINES'
,'B-LACTAM, INCREASED ACTIVITY'
,'CALCIUM BLOCKERS'
,'CALCIUM SUPPLEMENTS'
,'CARDIAC AGENTS, OTHER'
,'CENTRALLY ACTING AGENT, ALONE, COMB'
,'CEPHALOSPORINS & RELATED'
,'CODEINE & COMB, NON-INJECTABLE'
,'CYCLOOXYGENASE INHIBITORS, ALONE/COMB'
,'DERMATOLOGICAL PREP, OTHER'
,'DIABETIC ACCESSORIES'
,'DPP-4 INHIB, ALONE'
,'ERYTHROMYCIN'
,'EXPECTORANTS ALONE'
,'EXTENDED SPECTRUM MACROLIDE'
,'FIBRIC ACID DERIVATIVES'
,'FLUORIDES'
,'FRACTIONATED HEPARINS'
,'GABA ANALOGS'
,'GLITAZONES, ALONE'
,'H2 ANTAGONISTS'
,'HEMATINICS, OTHER'
,'HEMORRHOIDAL PREP W/CORTICOSTEROIDS'
,'HERPES ANTIVIRALS'
,'HMG-COA REDUCTASE INHIBITORS'
,'HYPEROSMOLAR LAXATIVE'
,'IMIDAZOLES'
,'IRRITANT-STIMULANT LAXATIVES'
,'L-DOPA'
,'LEUKOTRIENE AGENTS'
,'MACROLIDES & RELATED, OTHER'
,'MELATONIN AGONISTS'
,'MIOTICS & GLAUCOMA, OTHER'
,'MISCELLANEOUS, OTHER'
,'NATURAL PENICILLINS'
,'NEWER GENERATION ANTIDEPRESSANT'
,'NITRITES & NITRATES'
,'NON-BARB, OTHER'
,'NON-DRUG PRODUCTS'
,'NON-NARC COUGH ALONE'
,'PHENOTHIAZINE DERIVATIVES'
,'POLYENES'
,'POSITIVE INOTROPIC AGENT'
,'PRE-XRAY EVACUANTS'
,'PROPOXYPHENE'
,'PROTON PUMP INHIBITORS'
,'QUINOLONES, SYSTEMIC'
,'ACE INHIBITORS W/DIURETICS'
,'ACNE W/ ANTNFCT/ANTISEPTIC'
,'ANALOGS OF HUMAN INSULIN COMBINATION'
,'ANTICHOLINERGIC, BRONCHIAL'
,'ANTIDIARRHEALS W/ANTI-INFECTIVE'
,'ANTI-FUNGAL COMBINATION'
,'ANTIHISTAMINE'
,'ANTI-INFECTIVES, NON-SYSTEMIC, TOPICAL'
,'ANTINEOPLASTIC ESTROGENS'
,'ANTIPSYCHOTIC COMBINATION'
,'ANTISEPTIC, MOUTH/THROAT'
,'ASCORBIC ACID (VIT. C)'
,'BETA AGONISTS, AEROSOL'
,'BIGUANIDES, ALONE'
,'BILE ACID SEQUESTRANTS'
,'BISPHOSPHONATES'
,'CHOLESTEROL ABSORPTION INHIBITORS'
,'CONDOMS'
,'DIAGNOSTIC AIDS BLOOD GLUCOSE TEST'
,'DIURETICS, COMBINATIONS'
,'EMOLLIENT LAXATIVES'
,'FERROUS, IRON ALONE'
,'GASTROINTESTINAL ANTI-INFLAMMATORY'
,'HORMONES, ANDROGENS, ORAL' 
,'HORMONES, ESTROGENS, ORAL'
,'HORMONES, CORT COMB, DERM'
,'HUMAN INSULINS COMBINATION'
,'INFLUENZA'
,'MORPHINE/OPIUM, INJECTABLE'
,'MULTIVIT GENERAL'
,'MUS RELX, NON-SURG, W/ANALGESIC'
,'OPHTH ANTI-ALLERGY'
,'OTIC ANALGESICS'
,'POTASSIUM SUPPL, CHLORIDE'
,'SNRI'
,'STEROID, INHALED BRONCHIAL'
,'SULFONAMIDE & TRIMETHOPRIM COMBINATIONS'
,'SYNTH NON-NARC, INJECTABLE'
,'THYROID HORMONE, NATURAL'
,'UT ANTI-INFECTIVE/ANALGESIC'
,'SALICYLATES AND RELATED'
,'SCABICIDES/PEDICULOCIDES'
,'SEIZURE DISORDERS'
,'SEROTONIN 5HT-1 RECEPTOR AGONISTS'
,'SEXUAL FUNCTION DISORDER'
,'SMOKING DETERRENTS'
,'SPECIFIC ANTAGONISTS'
,'TETRACYCLINES & CONGENERS'
,'TRIAZOLES'
,'TRICHOMONACIDES/ANTIBACTERIALS'
,'TRICYCLICS & TETRACYCLICS'
,'UT ANTISPASMODICS'
,'UT BENIGN PROSTATE') 

--3:52:41 Hr
Declare @max_USC_365d INT
Select @max_USC_365d=max(num) from [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX

Set Nocount on
Declare @num_USC_PDC_365d bigint 
Set @num_USC_PDC_365d=1

While @num_USC_PDC_365d<=@max_USC_365d
Begin

       Select A.Patient_id,A.Valid_Index,A.USC_Name,Service_Date_B,EndDate_B,Num_B,Datediff(dd,B.Service_Date_B,A.New_EndDate)+1 as day_diff 
	   into #tmp2
       from [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX A
       cross Apply (Select B.New_Service_Date as Service_Date_B,B.New_EndDate as EndDate_B,B.Num as Num_B From [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX B where A.Num=B.Num-1 and A.Patient_id=B.Patient_id and A.Valid_Index=B.Valid_Index and A.USC_Name=B.USC_Name and A.Num=@num_USC_PDC_365d)B
       where Datediff(dd,B.Service_Date_B,A.New_EndDate)+1>0

       Update [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX
       Set [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX.New_Service_Date=dateadd(dd,day_diff,B.Service_Date_B),
              [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX.New_EndDate=dateadd(dd,day_diff,B.EndDate_B)
       From #tmp2 B
       Where [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX.Num=B.Num_B and [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX.Patient_id=B.Patient_id and [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX.Valid_Index=B.Valid_Index and [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX.USC_Name=B.USC_Name

       Drop Table #tmp2
       Set @num_USC_PDC_365d=@num_USC_PDC_365d+1

End

-----------------------------------------------------------------PDC USC 365 days-----------------------------------------------------------------------------

--1:34 Min
Print 'Populate #AED_Start_End_Dates_365d_USC '
Print Getdate()

IF OBJECT_ID('tempdb..#AED_Start_End_Dates_365d_USC') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_365d_USC

SELECT A.patient_id,A.Valid_Index,B.USC_NAME,MIN(B.New_Service_Date) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.New_Service_Date) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_365d_USC
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT JOIN [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX B
ON  A.patient_id=B.patient_id
AND A.Valid_Index =B.Valid_Index
AND B.New_Service_Date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index,B.USC_NAME


Print 'Populate #PDC_365d_USC '
Print Getdate()

IF OBJECT_ID('tempdb..#PDC_365d_USC') IS NOT NULL
DROP TABLE #PDC_365d_USC

SELECT Patient_id,Valid_Index
,ISNULL([ACETAMINOPHEN]									, 0.0000) AS USC_Rx_PDC_ACETAMINOPHEN_Last_365d
,ISNULL([ADENOSINE RECEPTOR ANTAGONISTS, ALONE/COMB]	, 0.0000) AS USC_Rx_PDC_ADENOSINE_RECEPTOR_ANTAGONISTS_ALONE_COMB_Last_365d
,ISNULL([ALPHA BLOCKERS, ALONE, COMBINATION]			, 0.0000) AS USC_Rx_PDC_ALPHA_BLOCKERS_ALONE_COMBINATION_Last_365d
,ISNULL([ALPHA-BETA BLOCKER]							, 0.0000) AS USC_Rx_PDC_ALPHA_BETA_BLOCKER_Last_365d
,ISNULL([ALZHEIMER-TYPE DEMENTIA]						, 0.0000) AS USC_Rx_PDC_ALZHEIMER_TYPE_DEMENTIA_Last_365d
,ISNULL([AMINOPENICILLINS]								, 0.0000) AS USC_Rx_PDC_AMINOPENICILLINS_Last_365d
,ISNULL([ANALEPTICS]									, 0.0000) AS USC_Rx_PDC_ANALEPTICS_Last_365d
,ISNULL([ANESTH LOCAL & TOPICAL, OTHER]					, 0.0000) AS USC_Rx_PDC_ANESTH_LOCAL_TOPICAL_OTHER_Last_365d
,ISNULL([ANTACIDS, PLAIN]								, 0.0000) AS USC_Rx_PDC_ANTACIDS_PLAIN_Last_365d
,ISNULL([ANTIANXIETY, OTHER]							, 0.0000) AS USC_Rx_PDC_ANTIANXIETY_OTHER_Last_365d
,ISNULL([ANTI-ARRHYTHMIA AGENT]							, 0.0000) AS USC_Rx_PDC_ANTI_ARRHYTHMIA_AGENT_Last_365d
,ISNULL([ANTIARTH, COX-2 INHIBITORS]					, 0.0000) AS USC_Rx_PDC_ANTIARTH_COX_2_INHIBITORS_Last_365d
,ISNULL([ANTIARTH, GOUT SPECIFIC]						, 0.0000) AS USC_Rx_PDC_ANTIARTH_GOUT_SPECIFIC_Last_365d
,ISNULL([ANTIARTH, PLAIN]								, 0.0000) AS USC_Rx_PDC_ANTIARTH_PLAIN_Last_365d
,ISNULL([ANTIDOPA PHENOTHIAZINE]						, 0.0000) AS USC_Rx_PDC_ANTIDOPA_PHENOTHIAZINE_Last_365d
,ISNULL([ANTIHYPERLIPIDEMIC AGENT, OTHER]				, 0.0000) AS USC_Rx_PDC_ANTIHYPERLIPIDEMIC_AGENT_OTHER_Last_365d
,ISNULL([ANTIMALARIALS]									, 0.0000) AS USC_Rx_PDC_ANTIMALARIALS_Last_365d
,ISNULL([ANTI-MANIA]									, 0.0000) AS USC_Rx_PDC_ANTI_MANIA_Last_365d
,ISNULL([ANTI-MIGRAINE, COMBINATIONS]					, 0.0000) AS USC_Rx_PDC_ANTI_MIGRAINE_COMBINATIONS_Last_365d
,ISNULL([ANTINAUSEANT 5HT3 ANTAGONISTS]					, 0.0000) AS USC_Rx_PDC_ANTINAUSEANT_5HT3_ANTAGONISTS_Last_365d
,ISNULL([ANTINEOPLASTIC FOLIC ACID ANALOGS]				, 0.0000) AS USC_Rx_PDC_ANTINEOPLASTIC_FOLIC_ACID_ANALOGS_Last_365d
,ISNULL([ANTI-OBESITY, SYSTEMIC]						, 0.0000) AS USC_Rx_PDC_ANTI_OBESITY_SYSTEMIC_Last_365d
,ISNULL([ANTI-PARKINSON, OTHER]							, 0.0000) AS USC_Rx_PDC_ANTI_PARKINSON_OTHER_Last_365d
,ISNULL([ANTI-ULCERANTS, OTHER]							, 0.0000) AS USC_Rx_PDC_ANTI_ULCERANTS_OTHER_Last_365d
,ISNULL([BARB LONG-ACTING]								, 0.0000) AS USC_Rx_PDC_BARB_LONG_ACTING_Last_365d
,ISNULL([BENZODIAZEPINES]								, 0.0000) AS USC_Rx_PDC_BENZODIAZEPINES_Last_365d
,ISNULL([B-LACTAM, INCREASED ACTIVITY]					, 0.0000) AS USC_Rx_PDC_B_LACTAM_INCREASED_ACTIVITY_Last_365d
,ISNULL([CALCIUM BLOCKERS]								, 0.0000) AS USC_Rx_PDC_CALCIUM_BLOCKERS_Last_365d
,ISNULL([CALCIUM SUPPLEMENTS]							, 0.0000) AS USC_Rx_PDC_CALCIUM_SUPPLEMENTS_Last_365d
,ISNULL([CARDIAC AGENTS, OTHER]							, 0.0000) AS USC_Rx_PDC_CARDIAC_AGENTS_OTHER_Last_365d
,ISNULL([CENTRALLY ACTING AGENT, ALONE, COMB]			, 0.0000) AS USC_Rx_PDC_CENTRALLY_ACTING_AGENT_ALONE_COMB_Last_365d
,ISNULL([CEPHALOSPORINS & RELATED]						, 0.0000) AS USC_Rx_PDC_CEPHALOSPORINS_RELATED_Last_365d
,ISNULL([CODEINE & COMB, NON-INJECTABLE]				, 0.0000) AS USC_Rx_PDC_CODEINE_COMB_NON_INJECTABLE_Last_365d
,ISNULL([CYCLOOXYGENASE INHIBITORS, ALONE/COMB]			, 0.0000) AS USC_Rx_PDC_CYCLOOXYGENASE_INHIBITORS_ALONE_COMB_Last_365d
,ISNULL([DERMATOLOGICAL PREP, OTHER]					, 0.0000) AS USC_Rx_PDC_DERMATOLOGICAL_PREP_OTHER_Last_365d
,ISNULL([DIABETIC ACCESSORIES]							, 0.0000) AS USC_Rx_PDC_DIABETIC_ACCESSORIES_Last_365d
,ISNULL([DPP-4 INHIB, ALONE]							, 0.0000) AS USC_Rx_PDC_DPP_4_INHIB_ALONE_Last_365d
,ISNULL([ERYTHROMYCIN]									, 0.0000) AS USC_Rx_PDC_ERYTHROMYCIN_Last_365d
,ISNULL([EXPECTORANTS ALONE]							, 0.0000) AS USC_Rx_PDC_EXPECTORANTS_ALONE_Last_365d
,ISNULL([EXTENDED SPECTRUM MACROLIDE]					, 0.0000) AS USC_Rx_PDC_EXTENDED_SPECTRUM_MACROLIDE_Last_365d
,ISNULL([FIBRIC ACID DERIVATIVES]						, 0.0000) AS USC_Rx_PDC_FIBRIC_ACID_DERIVATIVES_Last_365d
,ISNULL([FLUORIDES]										, 0.0000) AS USC_Rx_PDC_FLUORIDES_Last_365d
,ISNULL([FRACTIONATED HEPARINS]							, 0.0000) AS USC_Rx_PDC_FRACTIONATED_HEPARINS_Last_365d
,ISNULL([GABA ANALOGS]									, 0.0000) AS USC_Rx_PDC_GABA_ANALOGS_Last_365d
,ISNULL([GLITAZONES, ALONE]								, 0.0000) AS USC_Rx_PDC_GLITAZONES_ALONE_Last_365d
,ISNULL([H2 ANTAGONISTS]								, 0.0000) AS USC_Rx_PDC_H2_ANTAGONISTS_Last_365d
,ISNULL([HEMATINICS, OTHER]								, 0.0000) AS USC_Rx_PDC_HEMATINICS_OTHER_Last_365d
,ISNULL([HEMORRHOIDAL PREP W/CORTICOSTEROIDS]			, 0.0000) AS USC_Rx_PDC_HEMORRHOIDAL_PREP_W_CORTICOSTEROIDS_Last_365d
,ISNULL([HERPES ANTIVIRALS]								, 0.0000) AS USC_Rx_PDC_HERPES_ANTIVIRALS_Last_365d
,ISNULL([HMG-COA REDUCTASE INHIBITORS]					, 0.0000) AS USC_Rx_PDC_HMG_COA_REDUCTASE_INHIBITORS_Last_365d
,ISNULL([HYPEROSMOLAR LAXATIVE]							, 0.0000) AS USC_Rx_PDC_HYPEROSMOLAR_LAXATIVE_Last_365d
,ISNULL([IMIDAZOLES]									, 0.0000) AS USC_Rx_PDC_IMIDAZOLES_Last_365d
,ISNULL([IRRITANT-STIMULANT LAXATIVES]					, 0.0000) AS USC_Rx_PDC_IRRITANT_STIMULANT_LAXATIVES_Last_365d
,ISNULL([L-DOPA]										, 0.0000) AS USC_Rx_PDC_L_DOPA_Last_365d
,ISNULL([LEUKOTRIENE AGENTS]							, 0.0000) AS USC_Rx_PDC_LEUKOTRIENE_AGENTS_Last_365d
,ISNULL([MACROLIDES & RELATED, OTHER]					, 0.0000) AS USC_Rx_PDC_MACROLIDES_RELATED_OTHER_Last_365d
,ISNULL([MELATONIN AGONISTS]							, 0.0000) AS USC_Rx_PDC_MELATONIN_AGONISTS_Last_365d
,ISNULL([MIOTICS & GLAUCOMA, OTHER]						, 0.0000) AS USC_Rx_PDC_MIOTICS_GLAUCOMA_OTHER_Last_365d
,ISNULL([MISCELLANEOUS, OTHER]							, 0.0000) AS USC_Rx_PDC_MISCELLANEOUS_OTHER_Last_365d
,ISNULL([NATURAL PENICILLINS]							, 0.0000) AS USC_Rx_PDC_NATURAL_PENICILLINS_Last_365d
,ISNULL([NEWER GENERATION ANTIDEPRESSANT]				, 0.0000) AS USC_Rx_PDC_NEWER_GENERATION_ANTIDEPRESSANT_Last_365d
,ISNULL([NITRITES & NITRATES]							, 0.0000) AS USC_Rx_PDC_NITRITES_NITRATES_Last_365d
,ISNULL([NON-BARB, OTHER]								, 0.0000) AS USC_Rx_PDC_NON_BARB_OTHER_Last_365d
,ISNULL([NON-DRUG PRODUCTS]								, 0.0000) AS USC_Rx_PDC_NON_DRUG_PRODUCTS_Last_365d
,ISNULL([NON-NARC COUGH ALONE]							, 0.0000) AS USC_Rx_PDC_NON_NARC_COUGH_ALONE_Last_365d
,ISNULL([PHENOTHIAZINE DERIVATIVES]						, 0.0000) AS USC_Rx_PDC_PHENOTHIAZINE_DERIVATIVES_Last_365d
,ISNULL([POLYENES]										, 0.0000) AS USC_Rx_PDC_POLYENES_Last_365d
,ISNULL([POSITIVE INOTROPIC AGENT]						, 0.0000) AS USC_Rx_PDC_POSITIVE_INOTROPIC_AGENT_Last_365d
,ISNULL([PRE-XRAY EVACUANTS]							, 0.0000) AS USC_Rx_PDC_PRE_XRAY_EVACUANTS_Last_365d
,ISNULL([PROPOXYPHENE]									, 0.0000) AS USC_Rx_PDC_PROPOXYPHENE_Last_365d
,ISNULL([PROTON PUMP INHIBITORS]						, 0.0000) AS USC_Rx_PDC_PROTON_PUMP_INHIBITORS_Last_365d
,ISNULL([QUINOLONES, SYSTEMIC]							, 0.0000) AS USC_Rx_PDC_QUINOLONES_SYSTEMIC_Last_365d
,ISNULL([ACE INHIBITORS W/DIURETICS]					, 0.0000) AS USC_Rx_PDC_ACE_INHIBITORS_W_DIURETICS_Last_365d
,ISNULL([ACNE W/ ANTNFCT/ANTISEPTIC]					, 0.0000) AS USC_Rx_PDC_ACNE_W__ANTNFCT_ANTISEPTIC_Last_365d
,ISNULL([ANALOGS OF HUMAN INSULIN COMBINATION]			, 0.0000) AS USC_Rx_PDC_ANALOGS_OF_HUMAN_INSULIN_COMBINATION_Last_365d
,ISNULL([ANTICHOLINERGIC, BRONCHIAL]					, 0.0000) AS USC_Rx_PDC_ANTICHOLINERGIC_BRONCHIAL_Last_365d
,ISNULL([ANTIDIARRHEALS W/ANTI-INFECTIVE]				, 0.0000) AS USC_Rx_PDC_ANTIDIARRHEALS_W_ANTI_INFECTIVE_Last_365d
,ISNULL([ANTI-FUNGAL COMBINATION]						, 0.0000) AS USC_Rx_PDC_ANTI_FUNGAL_COMBINATION_Last_365d
,ISNULL([ANTIHISTAMINE]									, 0.0000) AS USC_Rx_PDC_ANTIHISTAMINE_Last_365d
,ISNULL([ANTI-INFECTIVES, NON-SYSTEMIC, TOPICAL]		, 0.0000) AS USC_Rx_PDC_ANTI_INFECTIVES_NON_SYSTEMIC_TOPICAL_Last_365d
,ISNULL([ANTINEOPLASTIC ESTROGENS]						, 0.0000) AS USC_Rx_PDC_ANTINEOPLASTIC_ESTROGENS_Last_365d
,ISNULL([ANTIPSYCHOTIC COMBINATION]						, 0.0000) AS USC_Rx_PDC_ANTIPSYCHOTIC_COMBINATION_Last_365d
,ISNULL([ANTISEPTIC, MOUTH/THROAT]						, 0.0000) AS USC_Rx_PDC_ANTISEPTIC_MOUTH_THROAT_Last_365d
,ISNULL([ASCORBIC ACID (VIT. C)]						, 0.0000) AS USC_Rx_PDC_ASCORBIC_ACID_VIT_C_Last_365d
,ISNULL([BETA AGONISTS, AEROSOL]						, 0.0000) AS USC_Rx_PDC_BETA_AGONISTS_AEROSOL_Last_365d
,ISNULL([BIGUANIDES, ALONE]								, 0.0000) AS USC_Rx_PDC_BIGUANIDES_ALONE_Last_365d
,ISNULL([BILE ACID SEQUESTRANTS]						, 0.0000) AS USC_Rx_PDC_BILE_ACID_SEQUESTRANTS_Last_365d
,ISNULL([BISPHOSPHONATES]								, 0.0000) AS USC_Rx_PDC_BISPHOSPHONATES_Last_365d
,ISNULL([CHOLESTEROL ABSORPTION INHIBITORS]				, 0.0000) AS USC_Rx_PDC_CHOLESTEROL_ABSORPTION_INHIBITORS_Last_365d
,ISNULL([CONDOMS]										, 0.0000) AS USC_Rx_PDC_CONDOMS_Last_365d
,ISNULL([DIAGNOSTIC AIDS BLOOD GLUCOSE TEST]			, 0.0000) AS USC_Rx_PDC_DIAGNOSTIC_AIDS_BLOOD_GLUCOSE_TEST_Last_365d
,ISNULL([DIURETICS, COMBINATIONS]						, 0.0000) AS USC_Rx_PDC_DIURETICS_COMBINATIONS_Last_365d
,ISNULL([EMOLLIENT LAXATIVES]							, 0.0000) AS USC_Rx_PDC_EMOLLIENT_LAXATIVES_Last_365d
,ISNULL([FERROUS, IRON ALONE]							, 0.0000) AS USC_Rx_PDC_FERROUS_IRON_ALONE_Last_365d
,ISNULL([GASTROINTESTINAL ANTI-INFLAMMATORY]			, 0.0000) AS USC_Rx_PDC_GASTROINTESTINAL_ANTI_INFLAMMATORY_Last_365d
,ISNULL([HORMONES, ANDROGENS, ORAL] 					, 0.0000) AS USC_Rx_PDC_HORMONES_ANDROGENS_ORAL_Last_365d
,ISNULL([HORMONES, ESTROGENS, ORAL]						, 0.0000) AS USC_Rx_PDC_HORMONES_ESTROGENS_ORAL_Last_365d
,ISNULL([HORMONES, CORT COMB, DERM]						, 0.0000) AS USC_Rx_PDC_HORMONES_CORT_COMB_DERM_Last_365d
,ISNULL([HUMAN INSULINS COMBINATION]					, 0.0000) AS USC_Rx_PDC_HUMAN_INSULINS_COMBINATION_Last_365d
,ISNULL([INFLUENZA]										, 0.0000) AS USC_Rx_PDC_INFLUENZA_Last_365d
,ISNULL([MORPHINE/OPIUM, INJECTABLE]					, 0.0000) AS USC_Rx_PDC_MORPHINE_OPIUM_INJECTABLE_Last_365d
,ISNULL([MULTIVIT GENERAL]								, 0.0000) AS USC_Rx_PDC_MULTIVIT_GENERAL_Last_365d
,ISNULL([MUS RELX, NON-SURG, W/ANALGESIC]				, 0.0000) AS USC_Rx_PDC_MUS_RELX_NON_SURG_W_ANALGESIC_Last_365d
,ISNULL([OPHTH ANTI-ALLERGY]							, 0.0000) AS USC_Rx_PDC_OPHTH_ANTI_ALLERGY_Last_365d
,ISNULL([OTIC ANALGESICS]								, 0.0000) AS USC_Rx_PDC_OTIC_ANALGESICS_Last_365d
,ISNULL([POTASSIUM SUPPL, CHLORIDE]						, 0.0000) AS USC_Rx_PDC_POTASSIUM_SUPPL_CHLORIDE_Last_365d
,ISNULL([SNRI]											, 0.0000) AS USC_Rx_PDC_SNRI_Last_365d
,ISNULL([STEROID, INHALED BRONCHIAL]					, 0.0000) AS USC_Rx_PDC_STEROID_INHALED_BRONCHIAL_Last_365d
,ISNULL([SULFONAMIDE & TRIMETHOPRIM COMBINATIONS]		, 0.0000) AS USC_Rx_PDC_SULFONAMIDE_TRIMETHOPRIM_COMBINATIONS_Last_365d
,ISNULL([SYNTH NON-NARC, INJECTABLE]					, 0.0000) AS USC_Rx_PDC_SYNTH_NON_NARC_INJECTABLE_Last_365d
,ISNULL([THYROID HORMONE, NATURAL]						, 0.0000) AS USC_Rx_PDC_THYROID_HORMONE_NATURAL_Last_365d
,ISNULL([UT ANTI-INFECTIVE/ANALGESIC]					, 0.0000) AS USC_Rx_PDC_UT_ANTI_INFECTIVE_ANALGESIC_Last_365d
,ISNULL([SALICYLATES AND RELATED]						, 0.0000) AS USC_Rx_PDC_SALICYLATES_AND_RELATED_Last_365d
,ISNULL([SCABICIDES/PEDICULOCIDES]						, 0.0000) AS USC_Rx_PDC_SCABICIDES_PEDICULOCIDES_Last_365d
,ISNULL([SEIZURE DISORDERS]								, 0.0000) AS USC_Rx_PDC_SEIZURE_DISORDERS_Last_365d
,ISNULL([SEROTONIN 5HT-1 RECEPTOR AGONISTS]				, 0.0000) AS USC_Rx_PDC_SEROTONIN_5HT_1_RECEPTOR_AGONISTS_Last_365d
,ISNULL([SEXUAL FUNCTION DISORDER]						, 0.0000) AS USC_Rx_PDC_SEXUAL_FUNCTION_DISORDER_Last_365d
,ISNULL([SMOKING DETERRENTS]							, 0.0000) AS USC_Rx_PDC_SMOKING_DETERRENTS_Last_365d
,ISNULL([SPECIFIC ANTAGONISTS]							, 0.0000) AS USC_Rx_PDC_SPECIFIC_ANTAGONISTS_Last_365d
,ISNULL([TETRACYCLINES & CONGENERS]						, 0.0000) AS USC_Rx_PDC_TETRACYCLINES_CONGENERS_Last_365d
,ISNULL([TRIAZOLES]										, 0.0000) AS USC_Rx_PDC_TRIAZOLES_Last_365d
,ISNULL([TRICHOMONACIDES/ANTIBACTERIALS]				, 0.0000) AS USC_Rx_PDC_TRICHOMONACIDES_ANTIBACTERIALS_Last_365d
,ISNULL([TRICYCLICS & TETRACYCLICS]						, 0.0000) AS USC_Rx_PDC_TRICYCLICS_TETRACYCLICS_Last_365d
,ISNULL([UT ANTISPASMODICS]								, 0.0000) AS USC_Rx_PDC_UT_ANTISPASMODICS_Last_365d

INTO #PDC_365d_USC
FROM 
(
SELECT A.patient_id,A.Valid_Index,USC_NAME, PDC AS COUNTT
FROM 
		(
		
				SELECT A.patient_id ,A.Valid_Index,A.USC_NAME
						--,SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<NEW_EndDate THEN DATEDIFF(DD,New_Enddate,Next_Maxdate)-1 ELSE 0 END) AS Drug_Days  /* Just For Display */
						--,AVG(Denometer) AS Denometer   /* Just for Display */
						,ISNULL(CAST((SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<=NEW_EndDate1 THEN DATEDIFF(DD,New_Enddate1,Next_Maxdate)-1 ELSE 0 END))*1.0/AVG(Denometer) AS DECIMAL(6,4)),0.0000) AS PDC
				FROM 
				(
					SELECT A.patient_id,A.Valid_Index,B.New_Service_Date,B.New_EndDate,B.USC_NAME
							,CASE WHEN A.Valid_Index >=New_EndDate THEN New_EndDate  ELSE  DATEADD(DD,-1,A.Valid_Index) END AS New_EndDate1
							,DATEDIFF(DD,New_Service_Date,CASE WHEN A.Valid_Index >New_EndDate THEN New_EndDate  ELSE  DATEADD(DD,-1,A.Valid_Index) END)+1 New_AED_Days
							,LEAD(New_Service_Date,1) OVER (PARTITION BY A.Patient_id,A.Valid_Index,B.USC_NAME ORDER BY New_Service_Date) Next_Maxdate
					FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT JOIN [$(TargetSchema)].app_tds_int_USC_List_Feature_365d_ADD_INDEX B
					ON  A.patient_id=B.patient_id
					AND A.Valid_Index=B.Valid_Index
					AND B.New_Service_Date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
				)A
				left join #AED_Start_End_Dates_365d_USC B
				ON A.patient_id= B.patient_id
				AND A.Valid_Index = B.Valid_Index
				AND A.USC_NAME=B.USC_NAME
				GROUP BY A.patient_id ,A.Valid_Index,A.USC_NAME

				
		)  A 

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR USC_NAME IN (
 [ACETAMINOPHEN]
,[ADENOSINE RECEPTOR ANTAGONISTS, ALONE/COMB]
,[ALPHA BLOCKERS, ALONE, COMBINATION]
,[ALPHA-BETA BLOCKER]
,[ALZHEIMER-TYPE DEMENTIA]
,[AMINOPENICILLINS]
,[ANALEPTICS]
,[ANESTH LOCAL & TOPICAL, OTHER]
,[ANTACIDS, PLAIN]
,[ANTIANXIETY, OTHER]
,[ANTI-ARRHYTHMIA AGENT]
,[ANTIARTH, COX-2 INHIBITORS]
,[ANTIARTH, GOUT SPECIFIC]
,[ANTIARTH, PLAIN]
,[ANTIDOPA PHENOTHIAZINE]
,[ANTIHYPERLIPIDEMIC AGENT, OTHER]
,[ANTIMALARIALS]
,[ANTI-MANIA]
,[ANTI-MIGRAINE, COMBINATIONS]
,[ANTINAUSEANT 5HT3 ANTAGONISTS]
,[ANTINEOPLASTIC FOLIC ACID ANALOGS]
,[ANTI-OBESITY, SYSTEMIC]
,[ANTI-PARKINSON, OTHER]
,[ANTI-ULCERANTS, OTHER]
,[BARB LONG-ACTING]
,[BENZODIAZEPINES]
,[B-LACTAM, INCREASED ACTIVITY]
,[CALCIUM BLOCKERS]
,[CALCIUM SUPPLEMENTS]
,[CARDIAC AGENTS, OTHER]
,[CENTRALLY ACTING AGENT, ALONE, COMB]
,[CEPHALOSPORINS & RELATED]
,[CODEINE & COMB, NON-INJECTABLE]
,[CYCLOOXYGENASE INHIBITORS, ALONE/COMB]
,[DERMATOLOGICAL PREP, OTHER]
,[DIABETIC ACCESSORIES]
,[DPP-4 INHIB, ALONE]
,[ERYTHROMYCIN]
,[EXPECTORANTS ALONE]
,[EXTENDED SPECTRUM MACROLIDE]
,[FIBRIC ACID DERIVATIVES]
,[FLUORIDES]
,[FRACTIONATED HEPARINS]
,[GABA ANALOGS]
,[GLITAZONES, ALONE]
,[H2 ANTAGONISTS]
,[HEMATINICS, OTHER]
,[HEMORRHOIDAL PREP W/CORTICOSTEROIDS]
,[HERPES ANTIVIRALS]
,[HMG-COA REDUCTASE INHIBITORS]
,[HYPEROSMOLAR LAXATIVE]
,[IMIDAZOLES]
,[IRRITANT-STIMULANT LAXATIVES]
,[L-DOPA]
,[LEUKOTRIENE AGENTS]
,[MACROLIDES & RELATED, OTHER]
,[MELATONIN AGONISTS]
,[MIOTICS & GLAUCOMA, OTHER]
,[MISCELLANEOUS, OTHER]
,[NATURAL PENICILLINS]
,[NEWER GENERATION ANTIDEPRESSANT]
,[NITRITES & NITRATES]
,[NON-BARB, OTHER]
,[NON-DRUG PRODUCTS]
,[NON-NARC COUGH ALONE]
,[PHENOTHIAZINE DERIVATIVES]
,[POLYENES]
,[POSITIVE INOTROPIC AGENT]
,[PRE-XRAY EVACUANTS]
,[PROPOXYPHENE]
,[PROTON PUMP INHIBITORS]
,[QUINOLONES, SYSTEMIC]
,[ACE INHIBITORS W/DIURETICS]
,[ACNE W/ ANTNFCT/ANTISEPTIC]
,[ANALOGS OF HUMAN INSULIN COMBINATION]
,[ANTICHOLINERGIC, BRONCHIAL]
,[ANTIDIARRHEALS W/ANTI-INFECTIVE]
,[ANTI-FUNGAL COMBINATION]
,[ANTIHISTAMINE]
,[ANTI-INFECTIVES, NON-SYSTEMIC, TOPICAL]
,[ANTINEOPLASTIC ESTROGENS]
,[ANTIPSYCHOTIC COMBINATION]
,[ANTISEPTIC, MOUTH/THROAT]
,[ASCORBIC ACID (VIT. C)]
,[BETA AGONISTS, AEROSOL]
,[BIGUANIDES, ALONE]
,[BILE ACID SEQUESTRANTS]
,[BISPHOSPHONATES]
,[CHOLESTEROL ABSORPTION INHIBITORS]
,[CONDOMS]
,[DIAGNOSTIC AIDS BLOOD GLUCOSE TEST]
,[DIURETICS, COMBINATIONS]
,[EMOLLIENT LAXATIVES]
,[FERROUS, IRON ALONE]
,[GASTROINTESTINAL ANTI-INFLAMMATORY]
,[HORMONES, ANDROGENS, ORAL] 
,[HORMONES, ESTROGENS, ORAL]
,[HORMONES, CORT COMB, DERM]
,[HUMAN INSULINS COMBINATION]
,[INFLUENZA]
,[MORPHINE/OPIUM, INJECTABLE]
,[MULTIVIT GENERAL]
,[MUS RELX, NON-SURG, W/ANALGESIC]
,[OPHTH ANTI-ALLERGY]
,[OTIC ANALGESICS]
,[POTASSIUM SUPPL, CHLORIDE]
,[SNRI]
,[STEROID, INHALED BRONCHIAL]
,[SULFONAMIDE & TRIMETHOPRIM COMBINATIONS]
,[SYNTH NON-NARC, INJECTABLE]
,[THYROID HORMONE, NATURAL]
,[UT ANTI-INFECTIVE/ANALGESIC]
,[SALICYLATES AND RELATED]
,[SCABICIDES/PEDICULOCIDES]
,[SEIZURE DISORDERS]
,[SEROTONIN 5HT-1 RECEPTOR AGONISTS]
,[SEXUAL FUNCTION DISORDER]
,[SMOKING DETERRENTS]
,[SPECIFIC ANTAGONISTS]
,[TETRACYCLINES & CONGENERS]
,[TRIAZOLES]
,[TRICHOMONACIDES/ANTIBACTERIALS]
,[TRICYCLICS & TETRACYCLICS]
,[UT ANTISPASMODICS]

)) AS PivotTable





--------------------------------------Create Processed data for USC (remove the overlap) 180 Day------------------------------------------------------------

--0:43 Min
Print 'Populate app_tds_int_USC_List_Feature_180d_ADD_INDEX '
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX') IS NOT NULL 
Drop table [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX

select Row_Number()over(Partition by A.Patient_id,A.Valid_Index,USC_Name order by A.Patient_id,A.Valid_Index,USC_Name,B.Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date)) as Num,A.Valid_Index,A.Patient_id,USC_Name,Service_date,DATEADD(DD,Days_Supply-1,B.Service_Date) AS EndDate,Service_date as New_Service_Date ,DATEADD(DD,Days_Supply-1,B.Service_Date) AS New_EndDate
into [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
					ON A.patient_id=B.patient_id 
					AND B.service_date BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
					LEFT JOIN  [$(SrcSchema)].[src_ref_product](NOLOCK) C
					ON B.NDC = C.NDC
					WHERE Days_Supply > 0 AND USC_NAME in (
'ACETAMINOPHEN'
,'ADENOSINE RECEPTOR ANTAGONISTS, ALONE/COMB'
,'ALPHA BLOCKERS, ALONE, COMBINATION'
,'ALPHA-BETA BLOCKER'
,'ALZHEIMER-TYPE DEMENTIA'
,'AMINOPENICILLINS'
,'ANALEPTICS'
,'ANESTH LOCAL & TOPICAL, OTHER'
,'ANTACIDS, PLAIN'
,'ANTIANXIETY, OTHER'
,'ANTI-ARRHYTHMIA AGENT'
,'ANTIARTH, COX-2 INHIBITORS'
,'ANTIARTH, GOUT SPECIFIC'
,'ANTIARTH, PLAIN'
,'ANTIDOPA PHENOTHIAZINE'
,'ANTIHYPERLIPIDEMIC AGENT, OTHER'
,'ANTIMALARIALS'
,'ANTI-MANIA'
,'ANTI-MIGRAINE, COMBINATIONS'
,'ANTINAUSEANT 5HT3 ANTAGONISTS'
,'ANTINEOPLASTIC FOLIC ACID ANALOGS'
,'ANTI-OBESITY, SYSTEMIC'
,'ANTI-PARKINSON, OTHER'
,'ANTI-ULCERANTS, OTHER'
,'BARB LONG-ACTING'
,'BENZODIAZEPINES'
,'B-LACTAM, INCREASED ACTIVITY'
,'CALCIUM BLOCKERS'
,'CALCIUM SUPPLEMENTS'
,'CARDIAC AGENTS, OTHER'
,'CENTRALLY ACTING AGENT, ALONE, COMB'
,'CEPHALOSPORINS & RELATED'
,'CODEINE & COMB, NON-INJECTABLE'
,'CYCLOOXYGENASE INHIBITORS, ALONE/COMB'
,'DERMATOLOGICAL PREP, OTHER'
,'DIABETIC ACCESSORIES'
,'DPP-4 INHIB, ALONE'
,'ERYTHROMYCIN'
,'EXPECTORANTS ALONE'
,'EXTENDED SPECTRUM MACROLIDE'
,'FIBRIC ACID DERIVATIVES'
,'FLUORIDES'
,'FRACTIONATED HEPARINS'
,'GABA ANALOGS'
,'GLITAZONES, ALONE'
,'H2 ANTAGONISTS'
,'HEMATINICS, OTHER'
,'HEMORRHOIDAL PREP W/CORTICOSTEROIDS'
,'HERPES ANTIVIRALS'
,'HMG-COA REDUCTASE INHIBITORS'
,'HYPEROSMOLAR LAXATIVE'
,'IMIDAZOLES'
,'IRRITANT-STIMULANT LAXATIVES'
,'L-DOPA'
,'LEUKOTRIENE AGENTS'
,'MACROLIDES & RELATED, OTHER'
,'MELATONIN AGONISTS'
,'MIOTICS & GLAUCOMA, OTHER'
,'MISCELLANEOUS, OTHER'
,'NATURAL PENICILLINS'
,'NEWER GENERATION ANTIDEPRESSANT'
,'NITRITES & NITRATES'
,'NON-BARB, OTHER'
,'NON-DRUG PRODUCTS'
,'NON-NARC COUGH ALONE'
,'PHENOTHIAZINE DERIVATIVES'
,'POLYENES'
,'POSITIVE INOTROPIC AGENT'
,'PRE-XRAY EVACUANTS'
,'PROPOXYPHENE'
,'PROTON PUMP INHIBITORS'
,'QUINOLONES, SYSTEMIC'
,'ACE INHIBITORS W/DIURETICS'
,'ACNE W/ ANTNFCT/ANTISEPTIC'
,'ANALOGS OF HUMAN INSULIN COMBINATION'
,'ANTICHOLINERGIC, BRONCHIAL'
,'ANTIDIARRHEALS W/ANTI-INFECTIVE'
,'ANTI-FUNGAL COMBINATION'
,'ANTIHISTAMINE'
,'ANTI-INFECTIVES, NON-SYSTEMIC, TOPICAL'
,'ANTINEOPLASTIC ESTROGENS'
,'ANTIPSYCHOTIC COMBINATION'
,'ANTISEPTIC, MOUTH/THROAT'
,'ASCORBIC ACID (VIT. C)'
,'BETA AGONISTS, AEROSOL'
,'BIGUANIDES, ALONE'
,'BILE ACID SEQUESTRANTS'
,'BISPHOSPHONATES'
,'CHOLESTEROL ABSORPTION INHIBITORS'
,'CONDOMS'
,'DIAGNOSTIC AIDS BLOOD GLUCOSE TEST'
,'DIURETICS, COMBINATIONS'
,'EMOLLIENT LAXATIVES'
,'FERROUS, IRON ALONE'
,'GASTROINTESTINAL ANTI-INFLAMMATORY'
,'HORMONES, ANDROGENS, ORAL' 
,'HORMONES, ESTROGENS, ORAL'
,'HORMONES, CORT COMB, DERM'
,'HUMAN INSULINS COMBINATION'
,'INFLUENZA'
,'MORPHINE/OPIUM, INJECTABLE'
,'MULTIVIT GENERAL'
,'MUS RELX, NON-SURG, W/ANALGESIC'
,'OPHTH ANTI-ALLERGY'
,'OTIC ANALGESICS'
,'POTASSIUM SUPPL, CHLORIDE'
,'SNRI'
,'STEROID, INHALED BRONCHIAL'
,'SULFONAMIDE & TRIMETHOPRIM COMBINATIONS'
,'SYNTH NON-NARC, INJECTABLE'
,'THYROID HORMONE, NATURAL'
,'UT ANTI-INFECTIVE/ANALGESIC'
,'SALICYLATES AND RELATED'
,'SCABICIDES/PEDICULOCIDES'
,'SEIZURE DISORDERS'
,'SEROTONIN 5HT-1 RECEPTOR AGONISTS'
,'SEXUAL FUNCTION DISORDER'
,'SMOKING DETERRENTS'
,'SPECIFIC ANTAGONISTS'
,'TETRACYCLINES & CONGENERS'
,'TRIAZOLES'
,'TRICHOMONACIDES/ANTIBACTERIALS'
,'TRICYCLICS & TETRACYCLICS'
,'UT ANTISPASMODICS'
,'UT BENIGN PROSTATE') 

--1:52:14 Hr
Declare @max_USC_180d INT
Select @max_USC_180d=max(num) from [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX

Set Nocount on
Declare @num_USC_PDC_180d bigint 
Set @num_USC_PDC_180d=1

While @num_USC_PDC_180d<=@max_USC_180d
Begin

       Select A.Patient_id,A.Valid_Index,A.USC_Name,Service_Date_B,EndDate_B,Num_B,Datediff(dd,B.Service_Date_B,A.New_EndDate)+1 as day_diff into #tmp3 
       from [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX A
       cross Apply (Select B.New_Service_Date as Service_Date_B,B.New_EndDate as EndDate_B,B.Num as Num_B From [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX B where A.Num=B.Num-1 and A.Patient_id=B.Patient_id  and A.Valid_Index=B.Valid_Index and A.USC_Name=B.USC_Name and A.Num=@num_USC_PDC_180d)B
       where Datediff(dd,B.Service_Date_B,A.New_EndDate)+1>0

       Update [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX
       Set [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX.New_Service_Date=dateadd(dd,day_diff,B.Service_Date_B),
              [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX.New_EndDate=dateadd(dd,day_diff,B.EndDate_B)
       From #tmp3 B
       Where [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX.Num=B.Num_B and [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX.Patient_id=B.Patient_id and [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX.Valid_Index=B.Valid_Index and [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX.USC_Name=B.USC_Name

       Drop Table #tmp3
       Set @num_USC_PDC_180d=@num_USC_PDC_180d+1

End


--------------------------------------------------------------------------- PDC USC for 180 Days-------------------------------------------------------------------------------


--1:19 Min
Print 'Populate #AED_Start_End_Dates_180d_USC '
Print Getdate()

IF OBJECT_ID('tempdb..#AED_Start_End_Dates_180d_USC') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_180d_USC

SELECT A.patient_id,A.Valid_Index,B.USC_NAME,MIN(B.New_Service_Date) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.New_Service_Date) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_180d_USC
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT JOIN [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX B
ON  A.patient_id=B.patient_id
AND A.Valid_Index = B.Valid_Index
AND B.New_Service_Date BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index,B.USC_NAME


Print 'Populate #PDC_180d_USC '
Print Getdate()

IF OBJECT_ID('tempdb..#PDC_180d_USC') IS NOT NULL
DROP TABLE #PDC_180d_USC

SELECT Patient_id,Valid_Index
,ISNULL([ACETAMINOPHEN]									, 0.0000) AS USC_Rx_PDC_ACETAMINOPHEN_Last_180d
,ISNULL([ADENOSINE RECEPTOR ANTAGONISTS, ALONE/COMB]	, 0.0000) AS USC_Rx_PDC_ADENOSINE_RECEPTOR_ANTAGONISTS_ALONE_COMB_Last_180d
,ISNULL([ALPHA BLOCKERS, ALONE, COMBINATION]			, 0.0000) AS USC_Rx_PDC_ALPHA_BLOCKERS_ALONE_COMBINATION_Last_180d
,ISNULL([ALPHA-BETA BLOCKER]							, 0.0000) AS USC_Rx_PDC_ALPHA_BETA_BLOCKER_Last_180d
,ISNULL([ALZHEIMER-TYPE DEMENTIA]						, 0.0000) AS USC_Rx_PDC_ALZHEIMER_TYPE_DEMENTIA_Last_180d
,ISNULL([AMINOPENICILLINS]								, 0.0000) AS USC_Rx_PDC_AMINOPENICILLINS_Last_180d
,ISNULL([ANALEPTICS]									, 0.0000) AS USC_Rx_PDC_ANALEPTICS_Last_180d
,ISNULL([ANESTH LOCAL & TOPICAL, OTHER]					, 0.0000) AS USC_Rx_PDC_ANESTH_LOCAL_TOPICAL_OTHER_Last_180d
,ISNULL([ANTACIDS, PLAIN]								, 0.0000) AS USC_Rx_PDC_ANTACIDS_PLAIN_Last_180d
,ISNULL([ANTIANXIETY, OTHER]							, 0.0000) AS USC_Rx_PDC_ANTIANXIETY_OTHER_Last_180d
,ISNULL([ANTI-ARRHYTHMIA AGENT]							, 0.0000) AS USC_Rx_PDC_ANTI_ARRHYTHMIA_AGENT_Last_180d
,ISNULL([ANTIARTH, COX-2 INHIBITORS]					, 0.0000) AS USC_Rx_PDC_ANTIARTH_COX_2_INHIBITORS_Last_180d
,ISNULL([ANTIARTH, GOUT SPECIFIC]						, 0.0000) AS USC_Rx_PDC_ANTIARTH_GOUT_SPECIFIC_Last_180d
,ISNULL([ANTIARTH, PLAIN]								, 0.0000) AS USC_Rx_PDC_ANTIARTH_PLAIN_Last_180d
,ISNULL([ANTIDOPA PHENOTHIAZINE]						, 0.0000) AS USC_Rx_PDC_ANTIDOPA_PHENOTHIAZINE_Last_180d
,ISNULL([ANTIHYPERLIPIDEMIC AGENT, OTHER]				, 0.0000) AS USC_Rx_PDC_ANTIHYPERLIPIDEMIC_AGENT_OTHER_Last_180d
,ISNULL([ANTIMALARIALS]									, 0.0000) AS USC_Rx_PDC_ANTIMALARIALS_Last_180d
,ISNULL([ANTI-MANIA]									, 0.0000) AS USC_Rx_PDC_ANTI_MANIA_Last_180d
,ISNULL([ANTI-MIGRAINE, COMBINATIONS]					, 0.0000) AS USC_Rx_PDC_ANTI_MIGRAINE_COMBINATIONS_Last_180d
,ISNULL([ANTINAUSEANT 5HT3 ANTAGONISTS]					, 0.0000) AS USC_Rx_PDC_ANTINAUSEANT_5HT3_ANTAGONISTS_Last_180d
,ISNULL([ANTINEOPLASTIC FOLIC ACID ANALOGS]				, 0.0000) AS USC_Rx_PDC_ANTINEOPLASTIC_FOLIC_ACID_ANALOGS_Last_180d
,ISNULL([ANTI-OBESITY, SYSTEMIC]						, 0.0000) AS USC_Rx_PDC_ANTI_OBESITY_SYSTEMIC_Last_180d
,ISNULL([ANTI-PARKINSON, OTHER]							, 0.0000) AS USC_Rx_PDC_ANTI_PARKINSON_OTHER_Last_180d
,ISNULL([ANTI-ULCERANTS, OTHER]							, 0.0000) AS USC_Rx_PDC_ANTI_ULCERANTS_OTHER_Last_180d
,ISNULL([BARB LONG-ACTING]								, 0.0000) AS USC_Rx_PDC_BARB_LONG_ACTING_Last_180d
,ISNULL([BENZODIAZEPINES]								, 0.0000) AS USC_Rx_PDC_BENZODIAZEPINES_Last_180d
,ISNULL([B-LACTAM, INCREASED ACTIVITY]					, 0.0000) AS USC_Rx_PDC_B_LACTAM_INCREASED_ACTIVITY_Last_180d
,ISNULL([CALCIUM BLOCKERS]								, 0.0000) AS USC_Rx_PDC_CALCIUM_BLOCKERS_Last_180d
,ISNULL([CALCIUM SUPPLEMENTS]							, 0.0000) AS USC_Rx_PDC_CALCIUM_SUPPLEMENTS_Last_180d
,ISNULL([CARDIAC AGENTS, OTHER]							, 0.0000) AS USC_Rx_PDC_CARDIAC_AGENTS_OTHER_Last_180d
,ISNULL([CENTRALLY ACTING AGENT, ALONE, COMB]			, 0.0000) AS USC_Rx_PDC_CENTRALLY_ACTING_AGENT_ALONE_COMB_Last_180d
,ISNULL([CEPHALOSPORINS & RELATED]						, 0.0000) AS USC_Rx_PDC_CEPHALOSPORINS_RELATED_Last_180d
,ISNULL([CODEINE & COMB, NON-INJECTABLE]				, 0.0000) AS USC_Rx_PDC_CODEINE_COMB_NON_INJECTABLE_Last_180d
,ISNULL([CYCLOOXYGENASE INHIBITORS, ALONE/COMB]			, 0.0000) AS USC_Rx_PDC_CYCLOOXYGENASE_INHIBITORS_ALONE_COMB_Last_180d
,ISNULL([DERMATOLOGICAL PREP, OTHER]					, 0.0000) AS USC_Rx_PDC_DERMATOLOGICAL_PREP_OTHER_Last_180d
,ISNULL([DIABETIC ACCESSORIES]							, 0.0000) AS USC_Rx_PDC_DIABETIC_ACCESSORIES_Last_180d
,ISNULL([DPP-4 INHIB, ALONE]							, 0.0000) AS USC_Rx_PDC_DPP_4_INHIB_ALONE_Last_180d
,ISNULL([ERYTHROMYCIN]									, 0.0000) AS USC_Rx_PDC_ERYTHROMYCIN_Last_180d
,ISNULL([EXPECTORANTS ALONE]							, 0.0000) AS USC_Rx_PDC_EXPECTORANTS_ALONE_Last_180d
,ISNULL([EXTENDED SPECTRUM MACROLIDE]					, 0.0000) AS USC_Rx_PDC_EXTENDED_SPECTRUM_MACROLIDE_Last_180d
,ISNULL([FIBRIC ACID DERIVATIVES]						, 0.0000) AS USC_Rx_PDC_FIBRIC_ACID_DERIVATIVES_Last_180d
,ISNULL([FLUORIDES]										, 0.0000) AS USC_Rx_PDC_FLUORIDES_Last_180d
,ISNULL([FRACTIONATED HEPARINS]							, 0.0000) AS USC_Rx_PDC_FRACTIONATED_HEPARINS_Last_180d
,ISNULL([GABA ANALOGS]									, 0.0000) AS USC_Rx_PDC_GABA_ANALOGS_Last_180d
,ISNULL([GLITAZONES, ALONE]								, 0.0000) AS USC_Rx_PDC_GLITAZONES_ALONE_Last_180d
,ISNULL([H2 ANTAGONISTS]								, 0.0000) AS USC_Rx_PDC_H2_ANTAGONISTS_Last_180d
,ISNULL([HEMATINICS, OTHER]								, 0.0000) AS USC_Rx_PDC_HEMATINICS_OTHER_Last_180d
,ISNULL([HEMORRHOIDAL PREP W/CORTICOSTEROIDS]			, 0.0000) AS USC_Rx_PDC_HEMORRHOIDAL_PREP_W_CORTICOSTEROIDS_Last_180d
,ISNULL([HERPES ANTIVIRALS]								, 0.0000) AS USC_Rx_PDC_HERPES_ANTIVIRALS_Last_180d
,ISNULL([HMG-COA REDUCTASE INHIBITORS]					, 0.0000) AS USC_Rx_PDC_HMG_COA_REDUCTASE_INHIBITORS_Last_180d
,ISNULL([HYPEROSMOLAR LAXATIVE]							, 0.0000) AS USC_Rx_PDC_HYPEROSMOLAR_LAXATIVE_Last_180d
,ISNULL([IMIDAZOLES]									, 0.0000) AS USC_Rx_PDC_IMIDAZOLES_Last_180d
,ISNULL([IRRITANT-STIMULANT LAXATIVES]					, 0.0000) AS USC_Rx_PDC_IRRITANT_STIMULANT_LAXATIVES_Last_180d
,ISNULL([L-DOPA]										, 0.0000) AS USC_Rx_PDC_L_DOPA_Last_180d
,ISNULL([LEUKOTRIENE AGENTS]							, 0.0000) AS USC_Rx_PDC_LEUKOTRIENE_AGENTS_Last_180d
,ISNULL([MACROLIDES & RELATED, OTHER]					, 0.0000) AS USC_Rx_PDC_MACROLIDES_RELATED_OTHER_Last_180d
,ISNULL([MELATONIN AGONISTS]							, 0.0000) AS USC_Rx_PDC_MELATONIN_AGONISTS_Last_180d
,ISNULL([MIOTICS & GLAUCOMA, OTHER]						, 0.0000) AS USC_Rx_PDC_MIOTICS_GLAUCOMA_OTHER_Last_180d
,ISNULL([MISCELLANEOUS, OTHER]							, 0.0000) AS USC_Rx_PDC_MISCELLANEOUS_OTHER_Last_180d
,ISNULL([NATURAL PENICILLINS]							, 0.0000) AS USC_Rx_PDC_NATURAL_PENICILLINS_Last_180d
,ISNULL([NEWER GENERATION ANTIDEPRESSANT]				, 0.0000) AS USC_Rx_PDC_NEWER_GENERATION_ANTIDEPRESSANT_Last_180d
,ISNULL([NITRITES & NITRATES]							, 0.0000) AS USC_Rx_PDC_NITRITES_NITRATES_Last_180d
,ISNULL([NON-BARB, OTHER]								, 0.0000) AS USC_Rx_PDC_NON_BARB_OTHER_Last_180d
,ISNULL([NON-DRUG PRODUCTS]								, 0.0000) AS USC_Rx_PDC_NON_DRUG_PRODUCTS_Last_180d
,ISNULL([NON-NARC COUGH ALONE]							, 0.0000) AS USC_Rx_PDC_NON_NARC_COUGH_ALONE_Last_180d
,ISNULL([PHENOTHIAZINE DERIVATIVES]						, 0.0000) AS USC_Rx_PDC_PHENOTHIAZINE_DERIVATIVES_Last_180d
,ISNULL([POLYENES]										, 0.0000) AS USC_Rx_PDC_POLYENES_Last_180d
,ISNULL([POSITIVE INOTROPIC AGENT]						, 0.0000) AS USC_Rx_PDC_POSITIVE_INOTROPIC_AGENT_Last_180d
,ISNULL([PRE-XRAY EVACUANTS]							, 0.0000) AS USC_Rx_PDC_PRE_XRAY_EVACUANTS_Last_180d
,ISNULL([PROPOXYPHENE]									, 0.0000) AS USC_Rx_PDC_PROPOXYPHENE_Last_180d
,ISNULL([PROTON PUMP INHIBITORS]						, 0.0000) AS USC_Rx_PDC_PROTON_PUMP_INHIBITORS_Last_180d
,ISNULL([QUINOLONES, SYSTEMIC]							, 0.0000) AS USC_Rx_PDC_QUINOLONES_SYSTEMIC_Last_180d
,ISNULL([ACE INHIBITORS W/DIURETICS]					, 0.0000) AS USC_Rx_PDC_ACE_INHIBITORS_W_DIURETICS_Last_180d
,ISNULL([ACNE W/ ANTNFCT/ANTISEPTIC]					, 0.0000) AS USC_Rx_PDC_ACNE_W__ANTNFCT_ANTISEPTIC_Last_180d
,ISNULL([ANALOGS OF HUMAN INSULIN COMBINATION]			, 0.0000) AS USC_Rx_PDC_ANALOGS_OF_HUMAN_INSULIN_COMBINATION_Last_180d
,ISNULL([ANTICHOLINERGIC, BRONCHIAL]					, 0.0000) AS USC_Rx_PDC_ANTICHOLINERGIC_BRONCHIAL_Last_180d
,ISNULL([ANTIDIARRHEALS W/ANTI-INFECTIVE]				, 0.0000) AS USC_Rx_PDC_ANTIDIARRHEALS_W_ANTI_INFECTIVE_Last_180d
,ISNULL([ANTI-FUNGAL COMBINATION]						, 0.0000) AS USC_Rx_PDC_ANTI_FUNGAL_COMBINATION_Last_180d
,ISNULL([ANTIHISTAMINE]									, 0.0000) AS USC_Rx_PDC_ANTIHISTAMINE_Last_180d
,ISNULL([ANTI-INFECTIVES, NON-SYSTEMIC, TOPICAL]		, 0.0000) AS USC_Rx_PDC_ANTI_INFECTIVES_NON_SYSTEMIC_TOPICAL_Last_180d
,ISNULL([ANTINEOPLASTIC ESTROGENS]						, 0.0000) AS USC_Rx_PDC_ANTINEOPLASTIC_ESTROGENS_Last_180d
,ISNULL([ANTIPSYCHOTIC COMBINATION]						, 0.0000) AS USC_Rx_PDC_ANTIPSYCHOTIC_COMBINATION_Last_180d
,ISNULL([ANTISEPTIC, MOUTH/THROAT]						, 0.0000) AS USC_Rx_PDC_ANTISEPTIC_MOUTH_THROAT_Last_180d
,ISNULL([ASCORBIC ACID (VIT. C)]						, 0.0000) AS USC_Rx_PDC_ASCORBIC_ACID_VIT_C_Last_180d
,ISNULL([BETA AGONISTS, AEROSOL]						, 0.0000) AS USC_Rx_PDC_BETA_AGONISTS_AEROSOL_Last_180d
,ISNULL([BIGUANIDES, ALONE]								, 0.0000) AS USC_Rx_PDC_BIGUANIDES_ALONE_Last_180d
,ISNULL([BILE ACID SEQUESTRANTS]						, 0.0000) AS USC_Rx_PDC_BILE_ACID_SEQUESTRANTS_Last_180d
,ISNULL([BISPHOSPHONATES]								, 0.0000) AS USC_Rx_PDC_BISPHOSPHONATES_Last_180d
,ISNULL([CHOLESTEROL ABSORPTION INHIBITORS]				, 0.0000) AS USC_Rx_PDC_CHOLESTEROL_ABSORPTION_INHIBITORS_Last_180d
,ISNULL([CONDOMS]										, 0.0000) AS USC_Rx_PDC_CONDOMS_Last_180d
,ISNULL([DIAGNOSTIC AIDS BLOOD GLUCOSE TEST]			, 0.0000) AS USC_Rx_PDC_DIAGNOSTIC_AIDS_BLOOD_GLUCOSE_TEST_Last_180d
,ISNULL([DIURETICS, COMBINATIONS]						, 0.0000) AS USC_Rx_PDC_DIURETICS_COMBINATIONS_Last_180d
,ISNULL([EMOLLIENT LAXATIVES]							, 0.0000) AS USC_Rx_PDC_EMOLLIENT_LAXATIVES_Last_180d
,ISNULL([FERROUS, IRON ALONE]							, 0.0000) AS USC_Rx_PDC_FERROUS_IRON_ALONE_Last_180d
,ISNULL([GASTROINTESTINAL ANTI-INFLAMMATORY]			, 0.0000) AS USC_Rx_PDC_GASTROINTESTINAL_ANTI_INFLAMMATORY_Last_180d
,ISNULL([HORMONES, ANDROGENS, ORAL] 					, 0.0000) AS USC_Rx_PDC_HORMONES_ANDROGENS_ORAL_Last_180d
,ISNULL([HORMONES, ESTROGENS, ORAL]						, 0.0000) AS USC_Rx_PDC_HORMONES_ESTROGENS_ORAL_Last_180d
,ISNULL([HORMONES, CORT COMB, DERM]						, 0.0000) AS USC_Rx_PDC_HORMONES_CORT_COMB_DERM_Last_180d
,ISNULL([HUMAN INSULINS COMBINATION]					, 0.0000) AS USC_Rx_PDC_HUMAN_INSULINS_COMBINATION_Last_180d
,ISNULL([INFLUENZA]										, 0.0000) AS USC_Rx_PDC_INFLUENZA_Last_180d
,ISNULL([MORPHINE/OPIUM, INJECTABLE]					, 0.0000) AS USC_Rx_PDC_MORPHINE_OPIUM_INJECTABLE_Last_180d
,ISNULL([MULTIVIT GENERAL]								, 0.0000) AS USC_Rx_PDC_MULTIVIT_GENERAL_Last_180d
,ISNULL([MUS RELX, NON-SURG, W/ANALGESIC]				, 0.0000) AS USC_Rx_PDC_MUS_RELX_NON_SURG_W_ANALGESIC_Last_180d
,ISNULL([OPHTH ANTI-ALLERGY]							, 0.0000) AS USC_Rx_PDC_OPHTH_ANTI_ALLERGY_Last_180d
,ISNULL([OTIC ANALGESICS]								, 0.0000) AS USC_Rx_PDC_OTIC_ANALGESICS_Last_180d
,ISNULL([POTASSIUM SUPPL, CHLORIDE]						, 0.0000) AS USC_Rx_PDC_POTASSIUM_SUPPL_CHLORIDE_Last_180d
,ISNULL([SNRI]											, 0.0000) AS USC_Rx_PDC_SNRI_Last_180d
,ISNULL([STEROID, INHALED BRONCHIAL]					, 0.0000) AS USC_Rx_PDC_STEROID_INHALED_BRONCHIAL_Last_180d
,ISNULL([SULFONAMIDE & TRIMETHOPRIM COMBINATIONS]		, 0.0000) AS USC_Rx_PDC_SULFONAMIDE_TRIMETHOPRIM_COMBINATIONS_Last_180d
,ISNULL([SYNTH NON-NARC, INJECTABLE]					, 0.0000) AS USC_Rx_PDC_SYNTH_NON_NARC_INJECTABLE_Last_180d
,ISNULL([THYROID HORMONE, NATURAL]						, 0.0000) AS USC_Rx_PDC_THYROID_HORMONE_NATURAL_Last_180d
,ISNULL([UT ANTI-INFECTIVE/ANALGESIC]					, 0.0000) AS USC_Rx_PDC_UT_ANTI_INFECTIVE_ANALGESIC_Last_180d
,ISNULL([SALICYLATES AND RELATED]						, 0.0000) AS USC_Rx_PDC_SALICYLATES_AND_RELATED_Last_180d
,ISNULL([SCABICIDES/PEDICULOCIDES]						, 0.0000) AS USC_Rx_PDC_SCABICIDES_PEDICULOCIDES_Last_180d
,ISNULL([SEIZURE DISORDERS]								, 0.0000) AS USC_Rx_PDC_SEIZURE_DISORDERS_Last_180d
,ISNULL([SEROTONIN 5HT-1 RECEPTOR AGONISTS]				, 0.0000) AS USC_Rx_PDC_SEROTONIN_5HT_1_RECEPTOR_AGONISTS_Last_180d
,ISNULL([SEXUAL FUNCTION DISORDER]						, 0.0000) AS USC_Rx_PDC_SEXUAL_FUNCTION_DISORDER_Last_180d
,ISNULL([SMOKING DETERRENTS]							, 0.0000) AS USC_Rx_PDC_SMOKING_DETERRENTS_Last_180d
,ISNULL([SPECIFIC ANTAGONISTS]							, 0.0000) AS USC_Rx_PDC_SPECIFIC_ANTAGONISTS_Last_180d
,ISNULL([TETRACYCLINES & CONGENERS]						, 0.0000) AS USC_Rx_PDC_TETRACYCLINES_CONGENERS_Last_180d
,ISNULL([TRIAZOLES]										, 0.0000) AS USC_Rx_PDC_TRIAZOLES_Last_180d
,ISNULL([TRICHOMONACIDES/ANTIBACTERIALS]				, 0.0000) AS USC_Rx_PDC_TRICHOMONACIDES_ANTIBACTERIALS_Last_180d
,ISNULL([TRICYCLICS & TETRACYCLICS]						, 0.0000) AS USC_Rx_PDC_TRICYCLICS_TETRACYCLICS_Last_180d
,ISNULL([UT ANTISPASMODICS]								, 0.0000) AS USC_Rx_PDC_UT_ANTISPASMODICS_Last_180d



INTO #PDC_180d_USC
FROM 
(
SELECT A.patient_id,A.Valid_Index,USC_NAME, PDC AS COUNTT
FROM 
		(
		
				SELECT A.patient_id ,A.Valid_Index,A.USC_NAME
						--,SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<NEW_EndDate THEN DATEDIFF(DD,New_Enddate,Next_Maxdate)-1 ELSE 0 END) AS Drug_Days  /* Just For Display */
						--,AVG(Denometer) AS Denometer   /* Just for Display */
						,ISNULL(CAST((SUM(NEW_AED_Days + CASE WHEN  NEXT_Maxdate<=NEW_EndDate1 THEN DATEDIFF(DD,New_Enddate1,Next_Maxdate)-1 ELSE 0 END))*1.0/AVG(Denometer) AS DECIMAL(6,4)),0.0000) AS PDC
				FROM 
				(
					SELECT A.patient_id,A.Valid_Index,B.New_Service_Date,B.New_EndDate,B.USC_NAME
							,CASE WHEN A.Valid_Index >=New_EndDate THEN New_EndDate  ELSE  DATEADD(DD,-1,A.Valid_Index) END AS New_EndDate1
							,DATEDIFF(DD,New_Service_Date,CASE WHEN A.Valid_Index >New_EndDate THEN New_EndDate  ELSE  DATEADD(DD,-1,A.Valid_Index) END)+1 New_AED_Days
							,LEAD(New_Service_Date,1) OVER (PARTITION BY A.Patient_id,A.Valid_Index,B.USC_NAME ORDER BY New_Service_Date) Next_Maxdate
					FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
					LEFT JOIN [$(TargetSchema)].app_tds_int_USC_List_Feature_180d_ADD_INDEX B
					ON  A.patient_id=B.patient_id
					AND A.Valid_Index = B.Valid_Index
					AND B.New_Service_Date BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
				)A
				left join #AED_Start_End_Dates_180d_USC B
				ON A.patient_id= B.patient_id
				AND A.Valid_Index = B.Valid_Index
				AND A.USC_NAME=B.USC_NAME
				GROUP BY A.patient_id ,A.Valid_Index,A.USC_NAME

				
		)  A 

 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR USC_NAME IN (
 [ACETAMINOPHEN]
,[ADENOSINE RECEPTOR ANTAGONISTS, ALONE/COMB]
,[ALPHA BLOCKERS, ALONE, COMBINATION]
,[ALPHA-BETA BLOCKER]
,[ALZHEIMER-TYPE DEMENTIA]
,[AMINOPENICILLINS]
,[ANALEPTICS]
,[ANESTH LOCAL & TOPICAL, OTHER]
,[ANTACIDS, PLAIN]
,[ANTIANXIETY, OTHER]
,[ANTI-ARRHYTHMIA AGENT]
,[ANTIARTH, COX-2 INHIBITORS]
,[ANTIARTH, GOUT SPECIFIC]
,[ANTIARTH, PLAIN]
,[ANTIDOPA PHENOTHIAZINE]
,[ANTIHYPERLIPIDEMIC AGENT, OTHER]
,[ANTIMALARIALS]
,[ANTI-MANIA]
,[ANTI-MIGRAINE, COMBINATIONS]
,[ANTINAUSEANT 5HT3 ANTAGONISTS]
,[ANTINEOPLASTIC FOLIC ACID ANALOGS]
,[ANTI-OBESITY, SYSTEMIC]
,[ANTI-PARKINSON, OTHER]
,[ANTI-ULCERANTS, OTHER]
,[BARB LONG-ACTING]
,[BENZODIAZEPINES]
,[B-LACTAM, INCREASED ACTIVITY]
,[CALCIUM BLOCKERS]
,[CALCIUM SUPPLEMENTS]
,[CARDIAC AGENTS, OTHER]
,[CENTRALLY ACTING AGENT, ALONE, COMB]
,[CEPHALOSPORINS & RELATED]
,[CODEINE & COMB, NON-INJECTABLE]
,[CYCLOOXYGENASE INHIBITORS, ALONE/COMB]
,[DERMATOLOGICAL PREP, OTHER]
,[DIABETIC ACCESSORIES]
,[DPP-4 INHIB, ALONE]
,[ERYTHROMYCIN]
,[EXPECTORANTS ALONE]
,[EXTENDED SPECTRUM MACROLIDE]
,[FIBRIC ACID DERIVATIVES]
,[FLUORIDES]
,[FRACTIONATED HEPARINS]
,[GABA ANALOGS]
,[GLITAZONES, ALONE]
,[H2 ANTAGONISTS]
,[HEMATINICS, OTHER]
,[HEMORRHOIDAL PREP W/CORTICOSTEROIDS]
,[HERPES ANTIVIRALS]
,[HMG-COA REDUCTASE INHIBITORS]
,[HYPEROSMOLAR LAXATIVE]
,[IMIDAZOLES]
,[IRRITANT-STIMULANT LAXATIVES]
,[L-DOPA]
,[LEUKOTRIENE AGENTS]
,[MACROLIDES & RELATED, OTHER]
,[MELATONIN AGONISTS]
,[MIOTICS & GLAUCOMA, OTHER]
,[MISCELLANEOUS, OTHER]
,[NATURAL PENICILLINS]
,[NEWER GENERATION ANTIDEPRESSANT]
,[NITRITES & NITRATES]
,[NON-BARB, OTHER]
,[NON-DRUG PRODUCTS]
,[NON-NARC COUGH ALONE]
,[PHENOTHIAZINE DERIVATIVES]
,[POLYENES]
,[POSITIVE INOTROPIC AGENT]
,[PRE-XRAY EVACUANTS]
,[PROPOXYPHENE]
,[PROTON PUMP INHIBITORS]
,[QUINOLONES, SYSTEMIC]
,[ACE INHIBITORS W/DIURETICS]
,[ACNE W/ ANTNFCT/ANTISEPTIC]
,[ANALOGS OF HUMAN INSULIN COMBINATION]
,[ANTICHOLINERGIC, BRONCHIAL]
,[ANTIDIARRHEALS W/ANTI-INFECTIVE]
,[ANTI-FUNGAL COMBINATION]
,[ANTIHISTAMINE]
,[ANTI-INFECTIVES, NON-SYSTEMIC, TOPICAL]
,[ANTINEOPLASTIC ESTROGENS]
,[ANTIPSYCHOTIC COMBINATION]
,[ANTISEPTIC, MOUTH/THROAT]
,[ASCORBIC ACID (VIT. C)]
,[BETA AGONISTS, AEROSOL]
,[BIGUANIDES, ALONE]
,[BILE ACID SEQUESTRANTS]
,[BISPHOSPHONATES]
,[CHOLESTEROL ABSORPTION INHIBITORS]
,[CONDOMS]
,[DIAGNOSTIC AIDS BLOOD GLUCOSE TEST]
,[DIURETICS, COMBINATIONS]
,[EMOLLIENT LAXATIVES]
,[FERROUS, IRON ALONE]
,[GASTROINTESTINAL ANTI-INFLAMMATORY]
,[HORMONES, ANDROGENS, ORAL] 
,[HORMONES, ESTROGENS, ORAL]
,[HORMONES, CORT COMB, DERM]
,[HUMAN INSULINS COMBINATION]
,[INFLUENZA]
,[MORPHINE/OPIUM, INJECTABLE]
,[MULTIVIT GENERAL]
,[MUS RELX, NON-SURG, W/ANALGESIC]
,[OPHTH ANTI-ALLERGY]
,[OTIC ANALGESICS]
,[POTASSIUM SUPPL, CHLORIDE]
,[SNRI]
,[STEROID, INHALED BRONCHIAL]
,[SULFONAMIDE & TRIMETHOPRIM COMBINATIONS]
,[SYNTH NON-NARC, INJECTABLE]
,[THYROID HORMONE, NATURAL]
,[UT ANTI-INFECTIVE/ANALGESIC]
,[SALICYLATES AND RELATED]
,[SCABICIDES/PEDICULOCIDES]
,[SEIZURE DISORDERS]
,[SEROTONIN 5HT-1 RECEPTOR AGONISTS]
,[SEXUAL FUNCTION DISORDER]
,[SMOKING DETERRENTS]
,[SPECIFIC ANTAGONISTS]
,[TETRACYCLINES & CONGENERS]
,[TRIAZOLES]
,[TRICHOMONACIDES/ANTIBACTERIALS]
,[TRICYCLICS & TETRACYCLICS]
,[UT ANTISPASMODICS]

)) AS PivotTable

--***************************************************************PDC for All generation drug***********************************************************************************************


-----------------------------------------------------------------PDC_Generation 365 days----------------------------------------------------
Print 'Populate #PSPA_Regiments_Treatment_RAW_Generation_365d '
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_Regiments_Treatment_RAW_Generation_365d') IS NOT NULL
DROP TABLE #PSPA_Regiments_Treatment_RAW_Generation_365d

SELECT  --ROW_NUMBER() over(partition by C.Patient_id,Start_Point order by  C.Patient_id,Start_Point,aed) as ID,
		C.Patient_id,C.Valid_Index,D.AED_Generation,
		Start_Point AS Start_Point_Original,
		CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point)  ELSE Start_Point END AS Start_Point,
		End_Point AS End_Point_Original,
		CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END   AS End_Point,
		Strt_End_Combination,
		D.AED,  Abbreviation,
		--DATEDIFF(dd,CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point) ELSE Start_Point END ,CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END ) +1 
		NULL AS Regiment_Days
		INTO #PSPA_Regiments_Treatment_RAW_Generation_365d
FROM (
				SELECT		Patient_id,
							Valid_index,
							AED_Generation,
							Event_date AS Start_Point, 
							LEAD(Event_Date) OVER (PARTITION BY patient_id,Valid_index,AED_Generation ORDER BY Event_Date ) AS End_Point,
							eventstatus+'-'+LEAD(eventstatus) OVER (PARTITION BY patient_id,Valid_index,AED_Generation ORDER BY Event_Date ) AS Strt_end_Combination
				FROM		(

							SELECT ROW_NUMBER() OVER (PARTITION BY patient_id,Valid_Index,AED_Generation ORDER BY Event_Date ) rownum,RANK() OVER (PARTITION BY patient_id,Valid_index,AED_Generation,Event_Date ORDER BY eventstatus ) rankk,*
							FROM (
									SELECT patient_id,Valid_index,AED_Generation,New_Service_date AS Event_Date , 'Start' AS eventstatus  FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX
									UNION 
									SELECT patient_id ,Valid_index,AED_Generation,New_enddate as enddate, 'End'  AS eventstatus FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX 
									
								 )A 
							)B WHERE Rankk =1
				) C
LEFT JOIN  (SELECT patient_id,Valid_Index,New_Service_date as maxdate, New_enddate AS enddate,GENERIC_NAME AS AED ,AED_Generation FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_365d_ADD_INDEX(NOLOCK)) D
ON C.patient_id = D.patient_id
AND C.Valid_Index=D.Valid_Index
AND C.AED_Generation =D.AED_Generation
AND C.Start_Point >= D.Maxdate AND C.End_Point <= D.Enddate
LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation Abb
ON D.AED = Abb.AED
WHERE D.AED IS NOT NULL


UPDATE #PSPA_Regiments_Treatment_RAW_Generation_365d
SET #PSPA_Regiments_Treatment_RAW_Generation_365d.Regiment_Days = DATEDIFF(dd,start_point, End_point) +1 

DELETE FROM #PSPA_Regiments_Treatment_RAW_Generation_365d  WHERE Regiment_days <=0

IF OBJECT_ID('tempdb..#Regiments_Stable_365d_Generation') IS NOT NULL
DROP TABLE #Regiments_Stable_365d_Generation

SELECT ROW_NUMBER() OVER(PARTITION BY Patient_id,Valid_index,AED_Generation,Start_Point ORDER BY  Patient_id,Valid_Index,AED_Generation,Start_Point,abbreviation) AS ID,*
INTO #Regiments_Stable_365d_Generation
FROM #PSPA_Regiments_Treatment_RAW_Generation_365d

--1:13 Min
Print 'Populate #Regimen_days_AED_PDC_365d_Generation '
Print Getdate()

IF OBJECT_ID('tempdb..#Regimen_days_AED_PDC_365d_Generation') IS NOT NULL
DROP TABLE #Regimen_days_AED_PDC_365d_Generation

SELECT DISTINCT Patient_id,Valid_Index,AED_Generation,start_point,End_Point,AED,DATEDIFF(dd,start_Point,End_Point)+1 AS AED_Days  
				into #Regimen_days_AED_PDC_365d_Generation  
				FROM (
				SELECT Patient_id,Valid_Index,AED_Generation,start_Point,End_Point, ISNULL([1],'') +ISNULL('__'+[2],'')+ISNULL('__'+[3],'')+ISNULL('__'+[4],'')+ISNULL('__'+[5],'')+ISNULL('__'+[6],'')+ISNULL('__'+[7],'')+ISNULL('__'+[8],'')+ISNULL('__'+[9],'')+ISNULL('__'+[10],'')+ISNULL('__'+[11],'')+ISNULL('__'+[12],'')+ISNULL('__'+[13],'')+ISNULL('__'+[14],'')+ISNULL('__'+[15],'')+ISNULL('__'+[16],'')+ISNULL('__'+[17],'')+ISNULL('__'+[18],'')+ISNULL('__'+[19],'')+ISNULL('__'+[20],'') AS AED
	
					FROM
					(SELECT ID,Patient_id,Valid_Index,A.AED_Generation,start_Point,End_Point,B.Abbreviation AS AED1
					FROM #Regiments_Stable_365d_Generation A LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation B
					ON	 A.aed=B.aed 
					) AS SourceTable
					PIVOT
					(
					max(AED1)
					FOR ID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
					) AS PivotTable
				) A
					WHERE AED <> ''

Print 'Populate #AED_Start_End_Dates_365d_Generation '
Print Getdate()

IF OBJECT_ID('tempdb..#AED_Start_End_Dates_365d_Generation') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_365d_Generation

SELECT A.patient_id,A.Valid_Index,AED_Generation,MIN(B.start_point) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.start_point) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_365d_Generation
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT  JOIN #Regimen_days_AED_PDC_365d_Generation B
ON A.patient_id=B.Patient_id
AND A.Valid_INdex = B.Valid_Index
AND B.start_point BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index,AED_Generation

Print 'Populate #PDC_365d_All_Generation '
Print Getdate()

IF OBJECT_ID('tempdb..#PDC_365d_All_Generation') IS NOT NULL
DROP TABLE #PDC_365d_All_Generation

select A.patient_id,A.Valid_Index,A.AED_Generation--,start_point,end_point,AED_DAYS,CASE WHEN END_POINT >= Valid_Index THEN DATEDIFF(DD,start_point,Valid_Index) ELSE AED_DAYS END
--,SUM(CASE WHEN END_POINT >= Valid_Index THEN DATEDIFF(DD,start_point,Valid_Index) ELSE AED_DAYS END),AVG(Denometer)
,CAST(ISNULL(SUM(CASE WHEN END_POINT >= A.Valid_Index THEN DATEDIFF(DD,start_point,A.Valid_Index) ELSE AED_DAYS END)*1.0 /AVG(Denometer) , 0.0000) AS DECIMAL(6,4)) AS PDC
INTO #PDC_365d_All_Generation
from #AED_Start_End_Dates_365d_Generation A
left join #Regimen_days_AED_PDC_365d_Generation B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index
and a.aed_generation =B.aed_generation
AND B.start_point BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index,A.AED_Generation


IF OBJECT_ID('tempdb..#PDC_365d_Generation') IS NOT NULL
DROP TABLE #PDC_365d_Generation

SELECT Patient_id,Valid_Index,
ISNULL([First] ,0.0000) AS PDC_first_gen_Last_365d
,ISNULL([Second],0.0000) AS PDC_Second_gen_Last_365d
,ISNULL([Third] ,0.0000) AS PDC_Third_gen_Last_365d
INTO #PDC_365d_Generation
FROM 
(SELECT patient_id,Valid_Index,AED_Generation, PDC AS COUNTT
FROM #PDC_365d_All_Generation 
 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR AED_Generation IN ([First],[Second],[Third])) AS PivotTable

-------------------------------------------------------------PDC Genreation 180 days--------------------------------------------------------------------------
Print 'Populate #PSPA_Regiments_Treatment_RAW_Generation_180d '
Print Getdate()

IF OBJECT_ID('tempdb..#PSPA_Regiments_Treatment_RAW_Generation_180d') IS NOT NULL
DROP TABLE #PSPA_Regiments_Treatment_RAW_Generation_180d

SELECT  --ROW_NUMBER() over(partition by C.Patient_id,Start_Point order by  C.Patient_id,Start_Point,aed) as ID,
		C.Patient_id,C.Valid_Index,D.AED_Generation,
		Start_Point AS Start_Point_Original,
		CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point)  ELSE Start_Point END AS Start_Point,
		End_Point AS End_Point_Original,
		CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END   AS End_Point,
		Strt_End_Combination,
		D.AED,  Abbreviation,
		--DATEDIFF(dd,CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point) ELSE Start_Point END ,CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END ) +1 
		NULL AS Regiment_Days
		INTO #PSPA_Regiments_Treatment_RAW_Generation_180d
FROM (
				SELECT		Patient_id,
							Valid_index,
							AED_Generation,
							Event_date AS Start_Point, 
							LEAD(Event_Date) OVER (PARTITION BY patient_id,Valid_index,AED_Generation ORDER BY Event_Date ) AS End_Point,
							eventstatus+'-'+LEAD(eventstatus) OVER (PARTITION BY patient_id,Valid_index,AED_Generation ORDER BY Event_Date ) AS Strt_end_Combination
				FROM		(

							SELECT ROW_NUMBER() OVER (PARTITION BY patient_id,Valid_Index,AED_Generation ORDER BY Event_Date ) rownum,RANK() OVER (PARTITION BY patient_id,Valid_index,AED_Generation,Event_Date ORDER BY eventstatus ) rankk,*
							FROM (
									SELECT patient_id,Valid_index,AED_Generation,New_Service_date AS Event_Date , 'Start' AS eventstatus  FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX
									UNION 
									SELECT patient_id ,Valid_index,AED_Generation,New_enddate as enddate, 'End'  AS eventstatus FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX 
									
								 )A 
							)B WHERE Rankk =1
				) C
LEFT JOIN  (SELECT patient_id,Valid_Index,New_Service_date as maxdate, New_enddate AS enddate,GENERIC_NAME AS AED ,AED_Generation FROM [$(TargetSchema)].app_tds_int_AED_List_Feature_PDC_180d_ADD_INDEX(NOLOCK)) D
ON C.patient_id = D.patient_id
AND C.Valid_Index=D.Valid_Index
AND C.AED_Generation =D.AED_Generation
AND C.Start_Point >= D.Maxdate AND C.End_Point <= D.Enddate
LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation Abb
ON D.AED = Abb.AED
WHERE D.AED IS NOT NULL


UPDATE #PSPA_Regiments_Treatment_RAW_Generation_180d
SET #PSPA_Regiments_Treatment_RAW_Generation_180d.Regiment_Days = DATEDIFF(dd,start_point, End_point) +1 

DELETE FROM #PSPA_Regiments_Treatment_RAW_Generation_180d  WHERE Regiment_days <=0

IF OBJECT_ID('tempdb..#Regiments_Stable_180d_Generation') IS NOT NULL
DROP TABLE #Regiments_Stable_180d_Generation

SELECT ROW_NUMBER() OVER(PARTITION BY Patient_id,Valid_index,AED_Generation,Start_Point ORDER BY  Patient_id,Valid_Index,AED_Generation,Start_Point,abbreviation) AS ID,*
INTO #Regiments_Stable_180d_Generation
FROM #PSPA_Regiments_Treatment_RAW_Generation_180d

--1:13 Min
Print 'Populate #Regimen_days_AED_PDC_180d_Generation '
Print Getdate()

IF OBJECT_ID('tempdb..#Regimen_days_AED_PDC_180d_Generation') IS NOT NULL
DROP TABLE #Regimen_days_AED_PDC_180d_Generation

SELECT DISTINCT Patient_id,Valid_Index,AED_Generation,start_point,End_Point,AED,DATEDIFF(dd,start_Point,End_Point)+1 AS AED_Days  
				into #Regimen_days_AED_PDC_180d_Generation  
				FROM (
				SELECT Patient_id,Valid_Index,AED_Generation,start_Point,End_Point, ISNULL([1],'') +ISNULL('__'+[2],'')+ISNULL('__'+[3],'')+ISNULL('__'+[4],'')+ISNULL('__'+[5],'')+ISNULL('__'+[6],'')+ISNULL('__'+[7],'')+ISNULL('__'+[8],'')+ISNULL('__'+[9],'')+ISNULL('__'+[10],'')+ISNULL('__'+[11],'')+ISNULL('__'+[12],'')+ISNULL('__'+[13],'')+ISNULL('__'+[14],'')+ISNULL('__'+[15],'')+ISNULL('__'+[16],'')+ISNULL('__'+[17],'')+ISNULL('__'+[18],'')+ISNULL('__'+[19],'')+ISNULL('__'+[20],'') AS AED
	
					FROM
					(SELECT ID,Patient_id,Valid_Index,A.AED_Generation,start_Point,End_Point,B.Abbreviation AS AED1
					FROM #Regiments_Stable_180d_Generation A LEFT JOIN [$(TargetSchema)].std_ref_AED_Abbreviation B
					ON	 A.aed=B.aed 
					) AS SourceTable
					PIVOT
					(
					max(AED1)
					FOR ID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
					) AS PivotTable
				) A
					WHERE AED <> ''


IF OBJECT_ID('tempdb..#AED_Start_End_Dates_180d_Generation') IS NOT NULL
DROP TABLE #AED_Start_End_Dates_180d_Generation

SELECT A.patient_id,A.Valid_Index,AED_Generation,MIN(B.start_point) AS First_Aed_Date
			--,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END )AS Last_Aed_date
			--,DATEDIFF(DD,MIN(B.maxdate) ,MAX(CASE WHEN Valid_Index >=Enddate THEN Enddate  ELSE  DATEADD(DD,-1,Valid_Index) END ))+1AS Denometer  /* Used when Last fill date is used for denomenator calculation*/
			,DATEDIFF(DD,MIN(B.start_point) ,DATEADD(DD,-1,A.Valid_Index))+1 AS Denometer    /* Use when considering the Index date as last date for denometer default*/
			INTO #AED_Start_End_Dates_180d_Generation
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT  JOIN #Regimen_days_AED_PDC_180d_Generation B
ON A.patient_id=B.Patient_id
AND A.Valid_INdex = B.Valid_Index
AND B.start_point BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index,AED_Generation

Print 'Populate #PDC_180d_All_Generation '
Print Getdate()

IF OBJECT_ID('tempdb..#PDC_180d_All_Generation') IS NOT NULL
DROP TABLE #PDC_180d_All_Generation

select A.patient_id,A.Valid_Index,A.AED_Generation--,start_point,end_point,AED_DAYS,CASE WHEN END_POINT >= Valid_Index THEN DATEDIFF(DD,start_point,Valid_Index) ELSE AED_DAYS END
--,SUM(CASE WHEN END_POINT >= Valid_Index THEN DATEDIFF(DD,start_point,Valid_Index) ELSE AED_DAYS END),AVG(Denometer)
,CAST(ISNULL(SUM(CASE WHEN END_POINT >= A.Valid_Index THEN DATEDIFF(DD,start_point,A.Valid_Index) ELSE AED_DAYS END)*1.0 /AVG(Denometer) , 0.0000) AS DECIMAL(6,4)) AS PDC
INTO #PDC_180d_All_Generation
from #AED_Start_End_Dates_180d_Generation A
left join #Regimen_days_AED_PDC_180d_Generation B
ON A.patient_id=B.patient_id
AND A.Valid_Index=B.Valid_Index
and a.aed_generation =B.aed_generation
AND B.start_point BETWEEN DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
GROUP BY A.patient_id,A.Valid_Index,A.AED_Generation


IF OBJECT_ID('tempdb..#PDC_180d_Generation') IS NOT NULL
DROP TABLE #PDC_180d_Generation

SELECT Patient_id,Valid_Index,
ISNULL([First] ,0.0000) AS PDC_first_gen_Last_180d
,ISNULL([Second],0.0000) AS PDC_Second_gen_Last_180d
,ISNULL([Third] ,0.0000) AS PDC_Third_gen_Last_180d
INTO #PDC_180d_Generation
FROM 
(SELECT patient_id,Valid_Index,AED_Generation, PDC AS COUNTT
FROM #PDC_180d_All_Generation 
 ) AS SourceTable 
PIVOT
(
SUM(COUNTT)
FOR AED_Generation IN ([First],[Second],[Third])) AS PivotTable


--*********************************************************************************Final table*******************************************************************************************
--1:19 Min
Print 'Populate app_tds_feature_IndexDate_PDC_AED_USC '
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].app_tds_feature_IndexDate_PDC_AED_USC') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_tds_feature_IndexDate_PDC_AED_USC

SELECT A.patient_id,A.Valid_Index
,PDC_All_AED_Last_365d
,PDC_All_AED_Last_180d
,PDC_BRV_Last_365d,PDC_CBZ_Last_365d,PDC_CLB_Last_365d,PDC_DVP_Last_365d,PDC_ESL_Last_365d,PDC_ESM_Last_365d,PDC_ETN_Last_365d,PDC_EZG_Last_365d,PDC_FBM_Last_365d,PDC_LAC_Last_365d,PDC_LTG_Last_365d,PDC_LEV_Last_365d
,PDC_OXG_Last_365d,PDC_PER_Last_365d,PDC_PBT_Last_365d,PDC_PTN_Last_365d,PDC_PGB_Last_365d,PDC_PRM_Last_365d,PDC_RTG_Last_365d,PDC_RFM_Last_365d,PDC_TPM_Last_365d,PDC_VPA_Last_365d,PDC_VGB_Last_365d,PDC_ZNS_Last_365d
,PDC_BRV_Last_180d,PDC_CBZ_Last_180d,PDC_CLB_Last_180d,PDC_DVP_Last_180d,PDC_ESL_Last_180d,PDC_ESM_Last_180d,PDC_ETN_Last_180d,PDC_EZG_Last_180d,PDC_FBM_Last_180d,PDC_LAC_Last_180d,PDC_LTG_Last_180d,PDC_LEV_Last_180d
,PDC_OXG_Last_180d,PDC_PER_Last_180d,PDC_PBT_Last_180d,PDC_PTN_Last_180d,PDC_PGB_Last_180d,PDC_PRM_Last_180d,PDC_RTG_Last_180d,PDC_RFM_Last_180d,PDC_TPM_Last_180d,PDC_VPA_Last_180d,PDC_VGB_Last_180d,PDC_ZNS_Last_180d

,USC_Rx_PDC_ACETAMINOPHEN_Last_365d ,USC_Rx_PDC_ADENOSINE_RECEPTOR_ANTAGONISTS_ALONE_COMB_Last_365d,USC_Rx_PDC_ALPHA_BLOCKERS_ALONE_COMBINATION_Last_365d,USC_Rx_PDC_ALPHA_BETA_BLOCKER_Last_365d,USC_Rx_PDC_ALZHEIMER_TYPE_DEMENTIA_Last_365d,USC_Rx_PDC_AMINOPENICILLINS_Last_365d,USC_Rx_PDC_ANALEPTICS_Last_365d,USC_Rx_PDC_ANESTH_LOCAL_TOPICAL_OTHER_Last_365d,USC_Rx_PDC_ANTACIDS_PLAIN_Last_365d,USC_Rx_PDC_ANTIANXIETY_OTHER_Last_365d,USC_Rx_PDC_ANTI_ARRHYTHMIA_AGENT_Last_365d,USC_Rx_PDC_ANTIARTH_COX_2_INHIBITORS_Last_365d,USC_Rx_PDC_ANTIARTH_GOUT_SPECIFIC_Last_365d,USC_Rx_PDC_ANTIARTH_PLAIN_Last_365d,USC_Rx_PDC_ANTIDOPA_PHENOTHIAZINE_Last_365d,USC_Rx_PDC_ANTIHYPERLIPIDEMIC_AGENT_OTHER_Last_365d
,USC_Rx_PDC_ANTIMALARIALS_Last_365d,USC_Rx_PDC_ANTI_MANIA_Last_365d,USC_Rx_PDC_ANTI_MIGRAINE_COMBINATIONS_Last_365d,USC_Rx_PDC_ANTINAUSEANT_5HT3_ANTAGONISTS_Last_365d,USC_Rx_PDC_ANTINEOPLASTIC_FOLIC_ACID_ANALOGS_Last_365d,USC_Rx_PDC_ANTI_OBESITY_SYSTEMIC_Last_365d,USC_Rx_PDC_ANTI_PARKINSON_OTHER_Last_365d,USC_Rx_PDC_ANTI_ULCERANTS_OTHER_Last_365d,USC_Rx_PDC_BARB_LONG_ACTING_Last_365d,USC_Rx_PDC_BENZODIAZEPINES_Last_365d,USC_Rx_PDC_B_LACTAM_INCREASED_ACTIVITY_Last_365d,USC_Rx_PDC_CALCIUM_BLOCKERS_Last_365d,USC_Rx_PDC_CALCIUM_SUPPLEMENTS_Last_365d,USC_Rx_PDC_CARDIAC_AGENTS_OTHER_Last_365d,USC_Rx_PDC_CENTRALLY_ACTING_AGENT_ALONE_COMB_Last_365d,USC_Rx_PDC_CEPHALOSPORINS_RELATED_Last_365d,USC_Rx_PDC_CODEINE_COMB_NON_INJECTABLE_Last_365d
,USC_Rx_PDC_CYCLOOXYGENASE_INHIBITORS_ALONE_COMB_Last_365d,USC_Rx_PDC_DERMATOLOGICAL_PREP_OTHER_Last_365d,USC_Rx_PDC_DIABETIC_ACCESSORIES_Last_365d,USC_Rx_PDC_DPP_4_INHIB_ALONE_Last_365d,USC_Rx_PDC_ERYTHROMYCIN_Last_365d,USC_Rx_PDC_EXPECTORANTS_ALONE_Last_365d,USC_Rx_PDC_EXTENDED_SPECTRUM_MACROLIDE_Last_365d,USC_Rx_PDC_FIBRIC_ACID_DERIVATIVES_Last_365d,USC_Rx_PDC_FLUORIDES_Last_365d,USC_Rx_PDC_FRACTIONATED_HEPARINS_Last_365d,USC_Rx_PDC_GABA_ANALOGS_Last_365d,USC_Rx_PDC_GLITAZONES_ALONE_Last_365d,USC_Rx_PDC_H2_ANTAGONISTS_Last_365d,USC_Rx_PDC_HEMATINICS_OTHER_Last_365d,USC_Rx_PDC_HEMORRHOIDAL_PREP_W_CORTICOSTEROIDS_Last_365d,USC_Rx_PDC_HERPES_ANTIVIRALS_Last_365d,USC_Rx_PDC_HMG_COA_REDUCTASE_INHIBITORS_Last_365d,USC_Rx_PDC_HYPEROSMOLAR_LAXATIVE_Last_365d
,USC_Rx_PDC_IMIDAZOLES_Last_365d,USC_Rx_PDC_IRRITANT_STIMULANT_LAXATIVES_Last_365d,USC_Rx_PDC_L_DOPA_Last_365d,USC_Rx_PDC_LEUKOTRIENE_AGENTS_Last_365d,USC_Rx_PDC_MACROLIDES_RELATED_OTHER_Last_365d,USC_Rx_PDC_MELATONIN_AGONISTS_Last_365d,USC_Rx_PDC_MIOTICS_GLAUCOMA_OTHER_Last_365d,USC_Rx_PDC_MISCELLANEOUS_OTHER_Last_365d,USC_Rx_PDC_NATURAL_PENICILLINS_Last_365d,USC_Rx_PDC_NEWER_GENERATION_ANTIDEPRESSANT_Last_365d,USC_Rx_PDC_NITRITES_NITRATES_Last_365d,USC_Rx_PDC_NON_BARB_OTHER_Last_365d,USC_Rx_PDC_NON_DRUG_PRODUCTS_Last_365d,USC_Rx_PDC_NON_NARC_COUGH_ALONE_Last_365d,USC_Rx_PDC_PHENOTHIAZINE_DERIVATIVES_Last_365d,USC_Rx_PDC_POLYENES_Last_365d,USC_Rx_PDC_POSITIVE_INOTROPIC_AGENT_Last_365d,USC_Rx_PDC_PRE_XRAY_EVACUANTS_Last_365d,USC_Rx_PDC_PROPOXYPHENE_Last_365d
,USC_Rx_PDC_PROTON_PUMP_INHIBITORS_Last_365d,USC_Rx_PDC_QUINOLONES_SYSTEMIC_Last_365d,USC_Rx_PDC_ACE_INHIBITORS_W_DIURETICS_Last_365d,USC_Rx_PDC_ACNE_W__ANTNFCT_ANTISEPTIC_Last_365d,USC_Rx_PDC_ANALOGS_OF_HUMAN_INSULIN_COMBINATION_Last_365d,USC_Rx_PDC_ANTICHOLINERGIC_BRONCHIAL_Last_365d,USC_Rx_PDC_ANTIDIARRHEALS_W_ANTI_INFECTIVE_Last_365d,USC_Rx_PDC_ANTI_FUNGAL_COMBINATION_Last_365d,USC_Rx_PDC_ANTIHISTAMINE_Last_365d,USC_Rx_PDC_ANTI_INFECTIVES_NON_SYSTEMIC_TOPICAL_Last_365d,USC_Rx_PDC_ANTINEOPLASTIC_ESTROGENS_Last_365d,USC_Rx_PDC_ANTIPSYCHOTIC_COMBINATION_Last_365d,USC_Rx_PDC_ANTISEPTIC_MOUTH_THROAT_Last_365d,USC_Rx_PDC_ASCORBIC_ACID_VIT_C_Last_365d,USC_Rx_PDC_BETA_AGONISTS_AEROSOL_Last_365d,USC_Rx_PDC_BIGUANIDES_ALONE_Last_365d,USC_Rx_PDC_BILE_ACID_SEQUESTRANTS_Last_365d
,USC_Rx_PDC_BISPHOSPHONATES_Last_365d,USC_Rx_PDC_CHOLESTEROL_ABSORPTION_INHIBITORS_Last_365d,USC_Rx_PDC_CONDOMS_Last_365d,USC_Rx_PDC_DIAGNOSTIC_AIDS_BLOOD_GLUCOSE_TEST_Last_365d,USC_Rx_PDC_DIURETICS_COMBINATIONS_Last_365d,USC_Rx_PDC_EMOLLIENT_LAXATIVES_Last_365d,USC_Rx_PDC_FERROUS_IRON_ALONE_Last_365d,USC_Rx_PDC_GASTROINTESTINAL_ANTI_INFLAMMATORY_Last_365d,USC_Rx_PDC_HORMONES_ANDROGENS_ORAL_Last_365d,USC_Rx_PDC_HORMONES_ESTROGENS_ORAL_Last_365d,USC_Rx_PDC_HORMONES_CORT_COMB_DERM_Last_365d,USC_Rx_PDC_HUMAN_INSULINS_COMBINATION_Last_365d,USC_Rx_PDC_INFLUENZA_Last_365d,USC_Rx_PDC_MORPHINE_OPIUM_INJECTABLE_Last_365d,USC_Rx_PDC_MULTIVIT_GENERAL_Last_365d,USC_Rx_PDC_MUS_RELX_NON_SURG_W_ANALGESIC_Last_365d,USC_Rx_PDC_OPHTH_ANTI_ALLERGY_Last_365d,USC_Rx_PDC_OTIC_ANALGESICS_Last_365d
,USC_Rx_PDC_POTASSIUM_SUPPL_CHLORIDE_Last_365d,USC_Rx_PDC_SNRI_Last_365d,USC_Rx_PDC_STEROID_INHALED_BRONCHIAL_Last_365d,USC_Rx_PDC_SULFONAMIDE_TRIMETHOPRIM_COMBINATIONS_Last_365d,USC_Rx_PDC_SYNTH_NON_NARC_INJECTABLE_Last_365d,USC_Rx_PDC_THYROID_HORMONE_NATURAL_Last_365d,USC_Rx_PDC_UT_ANTI_INFECTIVE_ANALGESIC_Last_365d,USC_Rx_PDC_SALICYLATES_AND_RELATED_Last_365d,USC_Rx_PDC_SCABICIDES_PEDICULOCIDES_Last_365d,USC_Rx_PDC_SEIZURE_DISORDERS_Last_365d,USC_Rx_PDC_SEROTONIN_5HT_1_RECEPTOR_AGONISTS_Last_365d,USC_Rx_PDC_SEXUAL_FUNCTION_DISORDER_Last_365d,USC_Rx_PDC_SMOKING_DETERRENTS_Last_365d,USC_Rx_PDC_SPECIFIC_ANTAGONISTS_Last_365d,USC_Rx_PDC_TETRACYCLINES_CONGENERS_Last_365d,USC_Rx_PDC_TRIAZOLES_Last_365d,USC_Rx_PDC_TRICHOMONACIDES_ANTIBACTERIALS_Last_365d,USC_Rx_PDC_TRICYCLICS_TETRACYCLICS_Last_365d,USC_Rx_PDC_UT_ANTISPASMODICS_Last_365d

,USC_Rx_PDC_ACETAMINOPHEN_Last_180d ,USC_Rx_PDC_ADENOSINE_RECEPTOR_ANTAGONISTS_ALONE_COMB_Last_180d,USC_Rx_PDC_ALPHA_BLOCKERS_ALONE_COMBINATION_Last_180d,USC_Rx_PDC_ALPHA_BETA_BLOCKER_Last_180d,USC_Rx_PDC_ALZHEIMER_TYPE_DEMENTIA_Last_180d,USC_Rx_PDC_AMINOPENICILLINS_Last_180d,USC_Rx_PDC_ANALEPTICS_Last_180d,USC_Rx_PDC_ANESTH_LOCAL_TOPICAL_OTHER_Last_180d,USC_Rx_PDC_ANTACIDS_PLAIN_Last_180d,USC_Rx_PDC_ANTIANXIETY_OTHER_Last_180d,USC_Rx_PDC_ANTI_ARRHYTHMIA_AGENT_Last_180d,USC_Rx_PDC_ANTIARTH_COX_2_INHIBITORS_Last_180d,USC_Rx_PDC_ANTIARTH_GOUT_SPECIFIC_Last_180d,USC_Rx_PDC_ANTIARTH_PLAIN_Last_180d,USC_Rx_PDC_ANTIDOPA_PHENOTHIAZINE_Last_180d,USC_Rx_PDC_ANTIHYPERLIPIDEMIC_AGENT_OTHER_Last_180d
,USC_Rx_PDC_ANTIMALARIALS_Last_180d,USC_Rx_PDC_ANTI_MANIA_Last_180d,USC_Rx_PDC_ANTI_MIGRAINE_COMBINATIONS_Last_180d,USC_Rx_PDC_ANTINAUSEANT_5HT3_ANTAGONISTS_Last_180d,USC_Rx_PDC_ANTINEOPLASTIC_FOLIC_ACID_ANALOGS_Last_180d,USC_Rx_PDC_ANTI_OBESITY_SYSTEMIC_Last_180d,USC_Rx_PDC_ANTI_PARKINSON_OTHER_Last_180d,USC_Rx_PDC_ANTI_ULCERANTS_OTHER_Last_180d,USC_Rx_PDC_BARB_LONG_ACTING_Last_180d,USC_Rx_PDC_BENZODIAZEPINES_Last_180d,USC_Rx_PDC_B_LACTAM_INCREASED_ACTIVITY_Last_180d,USC_Rx_PDC_CALCIUM_BLOCKERS_Last_180d,USC_Rx_PDC_CALCIUM_SUPPLEMENTS_Last_180d,USC_Rx_PDC_CARDIAC_AGENTS_OTHER_Last_180d,USC_Rx_PDC_CENTRALLY_ACTING_AGENT_ALONE_COMB_Last_180d,USC_Rx_PDC_CEPHALOSPORINS_RELATED_Last_180d,USC_Rx_PDC_CODEINE_COMB_NON_INJECTABLE_Last_180d
,USC_Rx_PDC_CYCLOOXYGENASE_INHIBITORS_ALONE_COMB_Last_180d,USC_Rx_PDC_DERMATOLOGICAL_PREP_OTHER_Last_180d,USC_Rx_PDC_DIABETIC_ACCESSORIES_Last_180d,USC_Rx_PDC_DPP_4_INHIB_ALONE_Last_180d,USC_Rx_PDC_ERYTHROMYCIN_Last_180d,USC_Rx_PDC_EXPECTORANTS_ALONE_Last_180d,USC_Rx_PDC_EXTENDED_SPECTRUM_MACROLIDE_Last_180d,USC_Rx_PDC_FIBRIC_ACID_DERIVATIVES_Last_180d,USC_Rx_PDC_FLUORIDES_Last_180d,USC_Rx_PDC_FRACTIONATED_HEPARINS_Last_180d,USC_Rx_PDC_GABA_ANALOGS_Last_180d,USC_Rx_PDC_GLITAZONES_ALONE_Last_180d,USC_Rx_PDC_H2_ANTAGONISTS_Last_180d,USC_Rx_PDC_HEMATINICS_OTHER_Last_180d,USC_Rx_PDC_HEMORRHOIDAL_PREP_W_CORTICOSTEROIDS_Last_180d,USC_Rx_PDC_HERPES_ANTIVIRALS_Last_180d,USC_Rx_PDC_HMG_COA_REDUCTASE_INHIBITORS_Last_180d,USC_Rx_PDC_HYPEROSMOLAR_LAXATIVE_Last_180d
,USC_Rx_PDC_IMIDAZOLES_Last_180d,USC_Rx_PDC_IRRITANT_STIMULANT_LAXATIVES_Last_180d,USC_Rx_PDC_L_DOPA_Last_180d,USC_Rx_PDC_LEUKOTRIENE_AGENTS_Last_180d,USC_Rx_PDC_MACROLIDES_RELATED_OTHER_Last_180d,USC_Rx_PDC_MELATONIN_AGONISTS_Last_180d,USC_Rx_PDC_MIOTICS_GLAUCOMA_OTHER_Last_180d,USC_Rx_PDC_MISCELLANEOUS_OTHER_Last_180d,USC_Rx_PDC_NATURAL_PENICILLINS_Last_180d,USC_Rx_PDC_NEWER_GENERATION_ANTIDEPRESSANT_Last_180d,USC_Rx_PDC_NITRITES_NITRATES_Last_180d,USC_Rx_PDC_NON_BARB_OTHER_Last_180d,USC_Rx_PDC_NON_DRUG_PRODUCTS_Last_180d,USC_Rx_PDC_NON_NARC_COUGH_ALONE_Last_180d,USC_Rx_PDC_PHENOTHIAZINE_DERIVATIVES_Last_180d,USC_Rx_PDC_POLYENES_Last_180d,USC_Rx_PDC_POSITIVE_INOTROPIC_AGENT_Last_180d,USC_Rx_PDC_PRE_XRAY_EVACUANTS_Last_180d,USC_Rx_PDC_PROPOXYPHENE_Last_180d
,USC_Rx_PDC_PROTON_PUMP_INHIBITORS_Last_180d,USC_Rx_PDC_QUINOLONES_SYSTEMIC_Last_180d,USC_Rx_PDC_ACE_INHIBITORS_W_DIURETICS_Last_180d,USC_Rx_PDC_ACNE_W__ANTNFCT_ANTISEPTIC_Last_180d,USC_Rx_PDC_ANALOGS_OF_HUMAN_INSULIN_COMBINATION_Last_180d,USC_Rx_PDC_ANTICHOLINERGIC_BRONCHIAL_Last_180d,USC_Rx_PDC_ANTIDIARRHEALS_W_ANTI_INFECTIVE_Last_180d,USC_Rx_PDC_ANTI_FUNGAL_COMBINATION_Last_180d,USC_Rx_PDC_ANTIHISTAMINE_Last_180d,USC_Rx_PDC_ANTI_INFECTIVES_NON_SYSTEMIC_TOPICAL_Last_180d,USC_Rx_PDC_ANTINEOPLASTIC_ESTROGENS_Last_180d,USC_Rx_PDC_ANTIPSYCHOTIC_COMBINATION_Last_180d,USC_Rx_PDC_ANTISEPTIC_MOUTH_THROAT_Last_180d,USC_Rx_PDC_ASCORBIC_ACID_VIT_C_Last_180d,USC_Rx_PDC_BETA_AGONISTS_AEROSOL_Last_180d,USC_Rx_PDC_BIGUANIDES_ALONE_Last_180d,USC_Rx_PDC_BILE_ACID_SEQUESTRANTS_Last_180d
,USC_Rx_PDC_BISPHOSPHONATES_Last_180d,USC_Rx_PDC_CHOLESTEROL_ABSORPTION_INHIBITORS_Last_180d,USC_Rx_PDC_CONDOMS_Last_180d,USC_Rx_PDC_DIAGNOSTIC_AIDS_BLOOD_GLUCOSE_TEST_Last_180d,USC_Rx_PDC_DIURETICS_COMBINATIONS_Last_180d,USC_Rx_PDC_EMOLLIENT_LAXATIVES_Last_180d,USC_Rx_PDC_FERROUS_IRON_ALONE_Last_180d,USC_Rx_PDC_GASTROINTESTINAL_ANTI_INFLAMMATORY_Last_180d,USC_Rx_PDC_HORMONES_ANDROGENS_ORAL_Last_180d,USC_Rx_PDC_HORMONES_ESTROGENS_ORAL_Last_180d,USC_Rx_PDC_HORMONES_CORT_COMB_DERM_Last_180d,USC_Rx_PDC_HUMAN_INSULINS_COMBINATION_Last_180d,USC_Rx_PDC_INFLUENZA_Last_180d,USC_Rx_PDC_MORPHINE_OPIUM_INJECTABLE_Last_180d,USC_Rx_PDC_MULTIVIT_GENERAL_Last_180d,USC_Rx_PDC_MUS_RELX_NON_SURG_W_ANALGESIC_Last_180d,USC_Rx_PDC_OPHTH_ANTI_ALLERGY_Last_180d,USC_Rx_PDC_OTIC_ANALGESICS_Last_180d
,USC_Rx_PDC_POTASSIUM_SUPPL_CHLORIDE_Last_180d,USC_Rx_PDC_SNRI_Last_180d,USC_Rx_PDC_STEROID_INHALED_BRONCHIAL_Last_180d,USC_Rx_PDC_SULFONAMIDE_TRIMETHOPRIM_COMBINATIONS_Last_180d,USC_Rx_PDC_SYNTH_NON_NARC_INJECTABLE_Last_180d,USC_Rx_PDC_THYROID_HORMONE_NATURAL_Last_180d,USC_Rx_PDC_UT_ANTI_INFECTIVE_ANALGESIC_Last_180d,USC_Rx_PDC_SALICYLATES_AND_RELATED_Last_180d,USC_Rx_PDC_SCABICIDES_PEDICULOCIDES_Last_180d,USC_Rx_PDC_SEIZURE_DISORDERS_Last_180d,USC_Rx_PDC_SEROTONIN_5HT_1_RECEPTOR_AGONISTS_Last_180d,USC_Rx_PDC_SEXUAL_FUNCTION_DISORDER_Last_180d,USC_Rx_PDC_SMOKING_DETERRENTS_Last_180d,USC_Rx_PDC_SPECIFIC_ANTAGONISTS_Last_180d,USC_Rx_PDC_TETRACYCLINES_CONGENERS_Last_180d,USC_Rx_PDC_TRIAZOLES_Last_180d,USC_Rx_PDC_TRICHOMONACIDES_ANTIBACTERIALS_Last_180d,USC_Rx_PDC_TRICYCLICS_TETRACYCLICS_Last_180d,USC_Rx_PDC_UT_ANTISPASMODICS_Last_180d

,PDC_first_gen_Last_365d,PDC_Second_gen_Last_365d,PDC_Third_gen_Last_365d 
,PDC_first_gen_Last_180d,PDC_Second_gen_Last_180d,PDC_Third_gen_Last_180d

INTO [$(TargetSchema)].[app_tds_feature_IndexDate_PDC_AED_USC]
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT JOIN #PDC_365d_All B
ON A.Patient_id =B.Patient_id
AND A.Valid_Index = B.Valid_Index
LEFT JOIN #PDC_180d_All C
ON A.Patient_id =C.Patient_id
AND A.Valid_Index = C.Valid_Index
LEFT JOIN #PDC_365d_AED D
ON A.Patient_id =D.Patient_id
AND A.Valid_Index = D.Valid_Index
LEFT JOIN #PDC_180d_AED E
ON A.Patient_id =E.Patient_id
AND A.Valid_Index = E.Valid_Index
LEFT JOIN #PDC_365d_USC F
ON A.Patient_id =F.Patient_id
AND A.Valid_Index = F.Valid_Index
LEFT JOIN #PDC_180d_USC G
ON A.Patient_id =G.Patient_id
AND A.Valid_Index = G.Valid_Index
LEFT JOIN #PDC_365d_Generation H
ON A.Patient_id =H.Patient_id
AND A.Valid_Index = H.Valid_Index
LEFT JOIN #PDC_180d_Generation I
ON A.Patient_id =I.Patient_id
AND A.Valid_Index = I.Valid_Index





---********************************************************* PDC AT USC LEVEL********************************************************************

/*

SELECT * INTO Dbo.PDC_365d_All FROM #PDC_365d_All
SELECT * INTO Dbo.PDC_180d_All FROM #PDC_180d_All
SELECT * INTO Dbo.PDC_365d_AED FROM #PDC_365d_AED
SELECT * INTO Dbo.PDC_180d_AED FROM #PDC_180d_AED
SELECT * INTO Dbo.PDC_365d_USC FROM #PDC_365d_USC
SELECT * INTO Dbo.PDC_180d_USC FROM #PDC_180d_USC
SELECT * INTO Dbo.PDC_365d_Generation FROM #PDC_365d_Generation
SELECT * INTO Dbo.PDC_180d_Generation FROM #PDC_180d_Generation


select * into dbo.Tmp_check from [$(TargetSchema)].std_ref_AED_Abbreviation 

create statistics IS_patient_id  ON [app_tds_int_BaselineModel_Input_IndexDate](patient_id)
create statistics IS_Valid_index ON [app_tds_int_BaselineModel_Input_IndexDate](Valid_Index)

create statistics IS_patient_id  ON [PDC_365d_All](patient_id)
create statistics IS_Valid_index ON [PDC_365d_All](Valid_Index)

create statistics IS_patient_id  ON [PDC_180d_All](patient_id)
create statistics IS_Valid_index ON [PDC_180d_All](Valid_Index)

create statistics IS_patient_id  ON [PDC_365d_AED](patient_id)
create statistics IS_Valid_index ON [PDC_365d_AED](Valid_Index)

create statistics IS_patient_id  ON [PDC_180d_AED](patient_id)
create statistics IS_Valid_index ON [PDC_180d_AED](Valid_Index)

create statistics IS_patient_id  ON [PDC_365d_USC](patient_id)
create statistics IS_Valid_index ON [PDC_365d_USC](Valid_Index)

create statistics IS_patient_id  ON [PDC_180d_USC](patient_id)
create statistics IS_Valid_index ON [PDC_180d_USC](Valid_Index)

create statistics IS_patient_id  ON [PDC_365d_Generation](patient_id)
create statistics IS_Valid_index ON [PDC_365d_Generation](Valid_Index)

create statistics IS_patient_id  ON [PDC_180d_Generation](patient_id)
create statistics IS_Valid_index ON [PDC_180d_Generation](Valid_Index)



SELECT A.patient_id,A.Valid_Index
,PDC_All_AED_Last_365d
,PDC_All_AED_Last_180d
,PDC_BRV_Last_365d,PDC_CBZ_Last_365d,PDC_CLB_Last_365d,PDC_DVP_Last_365d,PDC_ESL_Last_365d,PDC_ESM_Last_365d,PDC_ETN_Last_365d,PDC_EZG_Last_365d,PDC_FBM_Last_365d,PDC_LAC_Last_365d,PDC_LTG_Last_365d,PDC_LEV_Last_365d
,PDC_OXG_Last_365d,PDC_PER_Last_365d,PDC_PBT_Last_365d,PDC_PTN_Last_365d,PDC_PGB_Last_365d,PDC_PRM_Last_365d,PDC_RTG_Last_365d,PDC_RFM_Last_365d,PDC_TPM_Last_365d,PDC_VPA_Last_365d,PDC_VGB_Last_365d,PDC_ZNS_Last_365d
,PDC_BRV_Last_180d,PDC_CBZ_Last_180d,PDC_CLB_Last_180d,PDC_DVP_Last_180d,PDC_ESL_Last_180d,PDC_ESM_Last_180d,PDC_ETN_Last_180d,PDC_EZG_Last_180d,PDC_FBM_Last_180d,PDC_LAC_Last_180d,PDC_LTG_Last_180d,PDC_LEV_Last_180d
,PDC_OXG_Last_180d,PDC_PER_Last_180d,PDC_PBT_Last_180d,PDC_PTN_Last_180d,PDC_PGB_Last_180d,PDC_PRM_Last_180d,PDC_RTG_Last_180d,PDC_RFM_Last_180d,PDC_TPM_Last_180d,PDC_VPA_Last_180d,PDC_VGB_Last_180d,PDC_ZNS_Last_180d

,USC_Rx_PDC_ACETAMINOPHEN_Last_365d ,USC_Rx_PDC_ADENOSINE_RECEPTOR_ANTAGONISTS_ALONE_COMB_Last_365d,USC_Rx_PDC_ALPHA_BLOCKERS_ALONE_COMBINATION_Last_365d,USC_Rx_PDC_ALPHA_BETA_BLOCKER_Last_365d,USC_Rx_PDC_ALZHEIMER_TYPE_DEMENTIA_Last_365d,USC_Rx_PDC_AMINOPENICILLINS_Last_365d,USC_Rx_PDC_ANALEPTICS_Last_365d,USC_Rx_PDC_ANESTH_LOCAL_TOPICAL_OTHER_Last_365d,USC_Rx_PDC_ANTACIDS_PLAIN_Last_365d,USC_Rx_PDC_ANTIANXIETY_OTHER_Last_365d,USC_Rx_PDC_ANTI_ARRHYTHMIA_AGENT_Last_365d,USC_Rx_PDC_ANTIARTH_COX_2_INHIBITORS_Last_365d,USC_Rx_PDC_ANTIARTH_GOUT_SPECIFIC_Last_365d,USC_Rx_PDC_ANTIARTH_PLAIN_Last_365d,USC_Rx_PDC_ANTIDOPA_PHENOTHIAZINE_Last_365d,USC_Rx_PDC_ANTIHYPERLIPIDEMIC_AGENT_OTHER_Last_365d
,USC_Rx_PDC_ANTIMALARIALS_Last_365d,USC_Rx_PDC_ANTI_MANIA_Last_365d,USC_Rx_PDC_ANTI_MIGRAINE_COMBINATIONS_Last_365d,USC_Rx_PDC_ANTINAUSEANT_5HT3_ANTAGONISTS_Last_365d,USC_Rx_PDC_ANTINEOPLASTIC_FOLIC_ACID_ANALOGS_Last_365d,USC_Rx_PDC_ANTI_OBESITY_SYSTEMIC_Last_365d,USC_Rx_PDC_ANTI_PARKINSON_OTHER_Last_365d,USC_Rx_PDC_ANTI_ULCERANTS_OTHER_Last_365d,USC_Rx_PDC_BARB_LONG_ACTING_Last_365d,USC_Rx_PDC_BENZODIAZEPINES_Last_365d,USC_Rx_PDC_B_LACTAM_INCREASED_ACTIVITY_Last_365d,USC_Rx_PDC_CALCIUM_BLOCKERS_Last_365d,USC_Rx_PDC_CALCIUM_SUPPLEMENTS_Last_365d,USC_Rx_PDC_CARDIAC_AGENTS_OTHER_Last_365d,USC_Rx_PDC_CENTRALLY_ACTING_AGENT_ALONE_COMB_Last_365d,USC_Rx_PDC_CEPHALOSPORINS_RELATED_Last_365d,USC_Rx_PDC_CODEINE_COMB_NON_INJECTABLE_Last_365d
,USC_Rx_PDC_CYCLOOXYGENASE_INHIBITORS_ALONE_COMB_Last_365d,USC_Rx_PDC_DERMATOLOGICAL_PREP_OTHER_Last_365d,USC_Rx_PDC_DIABETIC_ACCESSORIES_Last_365d,USC_Rx_PDC_DPP_4_INHIB_ALONE_Last_365d,USC_Rx_PDC_ERYTHROMYCIN_Last_365d,USC_Rx_PDC_EXPECTORANTS_ALONE_Last_365d,USC_Rx_PDC_EXTENDED_SPECTRUM_MACROLIDE_Last_365d,USC_Rx_PDC_FIBRIC_ACID_DERIVATIVES_Last_365d,USC_Rx_PDC_FLUORIDES_Last_365d,USC_Rx_PDC_FRACTIONATED_HEPARINS_Last_365d,USC_Rx_PDC_GABA_ANALOGS_Last_365d,USC_Rx_PDC_GLITAZONES_ALONE_Last_365d,USC_Rx_PDC_H2_ANTAGONISTS_Last_365d,USC_Rx_PDC_HEMATINICS_OTHER_Last_365d,USC_Rx_PDC_HEMORRHOIDAL_PREP_W_CORTICOSTEROIDS_Last_365d,USC_Rx_PDC_HERPES_ANTIVIRALS_Last_365d,USC_Rx_PDC_HMG_COA_REDUCTASE_INHIBITORS_Last_365d,USC_Rx_PDC_HYPEROSMOLAR_LAXATIVE_Last_365d
,USC_Rx_PDC_IMIDAZOLES_Last_365d,USC_Rx_PDC_IRRITANT_STIMULANT_LAXATIVES_Last_365d,USC_Rx_PDC_L_DOPA_Last_365d,USC_Rx_PDC_LEUKOTRIENE_AGENTS_Last_365d,USC_Rx_PDC_MACROLIDES_RELATED_OTHER_Last_365d,USC_Rx_PDC_MELATONIN_AGONISTS_Last_365d,USC_Rx_PDC_MIOTICS_GLAUCOMA_OTHER_Last_365d,USC_Rx_PDC_MISCELLANEOUS_OTHER_Last_365d,USC_Rx_PDC_NATURAL_PENICILLINS_Last_365d,USC_Rx_PDC_NEWER_GENERATION_ANTIDEPRESSANT_Last_365d,USC_Rx_PDC_NITRITES_NITRATES_Last_365d,USC_Rx_PDC_NON_BARB_OTHER_Last_365d,USC_Rx_PDC_NON_DRUG_PRODUCTS_Last_365d,USC_Rx_PDC_NON_NARC_COUGH_ALONE_Last_365d,USC_Rx_PDC_PHENOTHIAZINE_DERIVATIVES_Last_365d,USC_Rx_PDC_POLYENES_Last_365d,USC_Rx_PDC_POSITIVE_INOTROPIC_AGENT_Last_365d,USC_Rx_PDC_PRE_XRAY_EVACUANTS_Last_365d,USC_Rx_PDC_PROPOXYPHENE_Last_365d
,USC_Rx_PDC_PROTON_PUMP_INHIBITORS_Last_365d,USC_Rx_PDC_QUINOLONES_SYSTEMIC_Last_365d,USC_Rx_PDC_ACE_INHIBITORS_W_DIURETICS_Last_365d,USC_Rx_PDC_ACNE_W__ANTNFCT_ANTISEPTIC_Last_365d,USC_Rx_PDC_ANALOGS_OF_HUMAN_INSULIN_COMBINATION_Last_365d,USC_Rx_PDC_ANTICHOLINERGIC_BRONCHIAL_Last_365d,USC_Rx_PDC_ANTIDIARRHEALS_W_ANTI_INFECTIVE_Last_365d,USC_Rx_PDC_ANTI_FUNGAL_COMBINATION_Last_365d,USC_Rx_PDC_ANTIHISTAMINE_Last_365d,USC_Rx_PDC_ANTI_INFECTIVES_NON_SYSTEMIC_TOPICAL_Last_365d,USC_Rx_PDC_ANTINEOPLASTIC_ESTROGENS_Last_365d,USC_Rx_PDC_ANTIPSYCHOTIC_COMBINATION_Last_365d,USC_Rx_PDC_ANTISEPTIC_MOUTH_THROAT_Last_365d,USC_Rx_PDC_ASCORBIC_ACID_VIT_C_Last_365d,USC_Rx_PDC_BETA_AGONISTS_AEROSOL_Last_365d,USC_Rx_PDC_BIGUANIDES_ALONE_Last_365d,USC_Rx_PDC_BILE_ACID_SEQUESTRANTS_Last_365d
,USC_Rx_PDC_BISPHOSPHONATES_Last_365d,USC_Rx_PDC_CHOLESTEROL_ABSORPTION_INHIBITORS_Last_365d,USC_Rx_PDC_CONDOMS_Last_365d,USC_Rx_PDC_DIAGNOSTIC_AIDS_BLOOD_GLUCOSE_TEST_Last_365d,USC_Rx_PDC_DIURETICS_COMBINATIONS_Last_365d,USC_Rx_PDC_EMOLLIENT_LAXATIVES_Last_365d,USC_Rx_PDC_FERROUS_IRON_ALONE_Last_365d,USC_Rx_PDC_GASTROINTESTINAL_ANTI_INFLAMMATORY_Last_365d,USC_Rx_PDC_HORMONES_ANDROGENS_ORAL_Last_365d,USC_Rx_PDC_HORMONES_ESTROGENS_ORAL_Last_365d,USC_Rx_PDC_HORMONES_CORT_COMB_DERM_Last_365d,USC_Rx_PDC_HUMAN_INSULINS_COMBINATION_Last_365d,USC_Rx_PDC_INFLUENZA_Last_365d,USC_Rx_PDC_MORPHINE_OPIUM_INJECTABLE_Last_365d,USC_Rx_PDC_MULTIVIT_GENERAL_Last_365d,USC_Rx_PDC_MUS_RELX_NON_SURG_W_ANALGESIC_Last_365d,USC_Rx_PDC_OPHTH_ANTI_ALLERGY_Last_365d,USC_Rx_PDC_OTIC_ANALGESICS_Last_365d
,USC_Rx_PDC_POTASSIUM_SUPPL_CHLORIDE_Last_365d,USC_Rx_PDC_SNRI_Last_365d,USC_Rx_PDC_STEROID_INHALED_BRONCHIAL_Last_365d,USC_Rx_PDC_SULFONAMIDE_TRIMETHOPRIM_COMBINATIONS_Last_365d,USC_Rx_PDC_SYNTH_NON_NARC_INJECTABLE_Last_365d,USC_Rx_PDC_THYROID_HORMONE_NATURAL_Last_365d,USC_Rx_PDC_UT_ANTI_INFECTIVE_ANALGESIC_Last_365d,USC_Rx_PDC_SALICYLATES_AND_RELATED_Last_365d,USC_Rx_PDC_SCABICIDES_PEDICULOCIDES_Last_365d,USC_Rx_PDC_SEIZURE_DISORDERS_Last_365d,USC_Rx_PDC_SEROTONIN_5HT_1_RECEPTOR_AGONISTS_Last_365d,USC_Rx_PDC_SEXUAL_FUNCTION_DISORDER_Last_365d,USC_Rx_PDC_SMOKING_DETERRENTS_Last_365d,USC_Rx_PDC_SPECIFIC_ANTAGONISTS_Last_365d,USC_Rx_PDC_TETRACYCLINES_CONGENERS_Last_365d,USC_Rx_PDC_TRIAZOLES_Last_365d,USC_Rx_PDC_TRICHOMONACIDES_ANTIBACTERIALS_Last_365d,USC_Rx_PDC_TRICYCLICS_TETRACYCLICS_Last_365d,USC_Rx_PDC_UT_ANTISPASMODICS_Last_365d

,USC_Rx_PDC_ACETAMINOPHEN_Last_180d ,USC_Rx_PDC_ADENOSINE_RECEPTOR_ANTAGONISTS_ALONE_COMB_Last_180d,USC_Rx_PDC_ALPHA_BLOCKERS_ALONE_COMBINATION_Last_180d,USC_Rx_PDC_ALPHA_BETA_BLOCKER_Last_180d,USC_Rx_PDC_ALZHEIMER_TYPE_DEMENTIA_Last_180d,USC_Rx_PDC_AMINOPENICILLINS_Last_180d,USC_Rx_PDC_ANALEPTICS_Last_180d,USC_Rx_PDC_ANESTH_LOCAL_TOPICAL_OTHER_Last_180d,USC_Rx_PDC_ANTACIDS_PLAIN_Last_180d,USC_Rx_PDC_ANTIANXIETY_OTHER_Last_180d,USC_Rx_PDC_ANTI_ARRHYTHMIA_AGENT_Last_180d,USC_Rx_PDC_ANTIARTH_COX_2_INHIBITORS_Last_180d,USC_Rx_PDC_ANTIARTH_GOUT_SPECIFIC_Last_180d,USC_Rx_PDC_ANTIARTH_PLAIN_Last_180d,USC_Rx_PDC_ANTIDOPA_PHENOTHIAZINE_Last_180d,USC_Rx_PDC_ANTIHYPERLIPIDEMIC_AGENT_OTHER_Last_180d
,USC_Rx_PDC_ANTIMALARIALS_Last_180d,USC_Rx_PDC_ANTI_MANIA_Last_180d,USC_Rx_PDC_ANTI_MIGRAINE_COMBINATIONS_Last_180d,USC_Rx_PDC_ANTINAUSEANT_5HT3_ANTAGONISTS_Last_180d,USC_Rx_PDC_ANTINEOPLASTIC_FOLIC_ACID_ANALOGS_Last_180d,USC_Rx_PDC_ANTI_OBESITY_SYSTEMIC_Last_180d,USC_Rx_PDC_ANTI_PARKINSON_OTHER_Last_180d,USC_Rx_PDC_ANTI_ULCERANTS_OTHER_Last_180d,USC_Rx_PDC_BARB_LONG_ACTING_Last_180d,USC_Rx_PDC_BENZODIAZEPINES_Last_180d,USC_Rx_PDC_B_LACTAM_INCREASED_ACTIVITY_Last_180d,USC_Rx_PDC_CALCIUM_BLOCKERS_Last_180d,USC_Rx_PDC_CALCIUM_SUPPLEMENTS_Last_180d,USC_Rx_PDC_CARDIAC_AGENTS_OTHER_Last_180d,USC_Rx_PDC_CENTRALLY_ACTING_AGENT_ALONE_COMB_Last_180d,USC_Rx_PDC_CEPHALOSPORINS_RELATED_Last_180d,USC_Rx_PDC_CODEINE_COMB_NON_INJECTABLE_Last_180d
,USC_Rx_PDC_CYCLOOXYGENASE_INHIBITORS_ALONE_COMB_Last_180d,USC_Rx_PDC_DERMATOLOGICAL_PREP_OTHER_Last_180d,USC_Rx_PDC_DIABETIC_ACCESSORIES_Last_180d,USC_Rx_PDC_DPP_4_INHIB_ALONE_Last_180d,USC_Rx_PDC_ERYTHROMYCIN_Last_180d,USC_Rx_PDC_EXPECTORANTS_ALONE_Last_180d,USC_Rx_PDC_EXTENDED_SPECTRUM_MACROLIDE_Last_180d,USC_Rx_PDC_FIBRIC_ACID_DERIVATIVES_Last_180d,USC_Rx_PDC_FLUORIDES_Last_180d,USC_Rx_PDC_FRACTIONATED_HEPARINS_Last_180d,USC_Rx_PDC_GABA_ANALOGS_Last_180d,USC_Rx_PDC_GLITAZONES_ALONE_Last_180d,USC_Rx_PDC_H2_ANTAGONISTS_Last_180d,USC_Rx_PDC_HEMATINICS_OTHER_Last_180d,USC_Rx_PDC_HEMORRHOIDAL_PREP_W_CORTICOSTEROIDS_Last_180d,USC_Rx_PDC_HERPES_ANTIVIRALS_Last_180d,USC_Rx_PDC_HMG_COA_REDUCTASE_INHIBITORS_Last_180d,USC_Rx_PDC_HYPEROSMOLAR_LAXATIVE_Last_180d
,USC_Rx_PDC_IMIDAZOLES_Last_180d,USC_Rx_PDC_IRRITANT_STIMULANT_LAXATIVES_Last_180d,USC_Rx_PDC_L_DOPA_Last_180d,USC_Rx_PDC_LEUKOTRIENE_AGENTS_Last_180d,USC_Rx_PDC_MACROLIDES_RELATED_OTHER_Last_180d,USC_Rx_PDC_MELATONIN_AGONISTS_Last_180d,USC_Rx_PDC_MIOTICS_GLAUCOMA_OTHER_Last_180d,USC_Rx_PDC_MISCELLANEOUS_OTHER_Last_180d,USC_Rx_PDC_NATURAL_PENICILLINS_Last_180d,USC_Rx_PDC_NEWER_GENERATION_ANTIDEPRESSANT_Last_180d,USC_Rx_PDC_NITRITES_NITRATES_Last_180d,USC_Rx_PDC_NON_BARB_OTHER_Last_180d,USC_Rx_PDC_NON_DRUG_PRODUCTS_Last_180d,USC_Rx_PDC_NON_NARC_COUGH_ALONE_Last_180d,USC_Rx_PDC_PHENOTHIAZINE_DERIVATIVES_Last_180d,USC_Rx_PDC_POLYENES_Last_180d,USC_Rx_PDC_POSITIVE_INOTROPIC_AGENT_Last_180d,USC_Rx_PDC_PRE_XRAY_EVACUANTS_Last_180d,USC_Rx_PDC_PROPOXYPHENE_Last_180d
,USC_Rx_PDC_PROTON_PUMP_INHIBITORS_Last_180d,USC_Rx_PDC_QUINOLONES_SYSTEMIC_Last_180d,USC_Rx_PDC_ACE_INHIBITORS_W_DIURETICS_Last_180d,USC_Rx_PDC_ACNE_W__ANTNFCT_ANTISEPTIC_Last_180d,USC_Rx_PDC_ANALOGS_OF_HUMAN_INSULIN_COMBINATION_Last_180d,USC_Rx_PDC_ANTICHOLINERGIC_BRONCHIAL_Last_180d,USC_Rx_PDC_ANTIDIARRHEALS_W_ANTI_INFECTIVE_Last_180d,USC_Rx_PDC_ANTI_FUNGAL_COMBINATION_Last_180d,USC_Rx_PDC_ANTIHISTAMINE_Last_180d,USC_Rx_PDC_ANTI_INFECTIVES_NON_SYSTEMIC_TOPICAL_Last_180d,USC_Rx_PDC_ANTINEOPLASTIC_ESTROGENS_Last_180d,USC_Rx_PDC_ANTIPSYCHOTIC_COMBINATION_Last_180d,USC_Rx_PDC_ANTISEPTIC_MOUTH_THROAT_Last_180d,USC_Rx_PDC_ASCORBIC_ACID_VIT_C_Last_180d,USC_Rx_PDC_BETA_AGONISTS_AEROSOL_Last_180d,USC_Rx_PDC_BIGUANIDES_ALONE_Last_180d,USC_Rx_PDC_BILE_ACID_SEQUESTRANTS_Last_180d
,USC_Rx_PDC_BISPHOSPHONATES_Last_180d,USC_Rx_PDC_CHOLESTEROL_ABSORPTION_INHIBITORS_Last_180d,USC_Rx_PDC_CONDOMS_Last_180d,USC_Rx_PDC_DIAGNOSTIC_AIDS_BLOOD_GLUCOSE_TEST_Last_180d,USC_Rx_PDC_DIURETICS_COMBINATIONS_Last_180d,USC_Rx_PDC_EMOLLIENT_LAXATIVES_Last_180d,USC_Rx_PDC_FERROUS_IRON_ALONE_Last_180d,USC_Rx_PDC_GASTROINTESTINAL_ANTI_INFLAMMATORY_Last_180d,USC_Rx_PDC_HORMONES_ANDROGENS_ORAL_Last_180d,USC_Rx_PDC_HORMONES_ESTROGENS_ORAL_Last_180d,USC_Rx_PDC_HORMONES_CORT_COMB_DERM_Last_180d,USC_Rx_PDC_HUMAN_INSULINS_COMBINATION_Last_180d,USC_Rx_PDC_INFLUENZA_Last_180d,USC_Rx_PDC_MORPHINE_OPIUM_INJECTABLE_Last_180d,USC_Rx_PDC_MULTIVIT_GENERAL_Last_180d,USC_Rx_PDC_MUS_RELX_NON_SURG_W_ANALGESIC_Last_180d,USC_Rx_PDC_OPHTH_ANTI_ALLERGY_Last_180d,USC_Rx_PDC_OTIC_ANALGESICS_Last_180d
,USC_Rx_PDC_POTASSIUM_SUPPL_CHLORIDE_Last_180d,USC_Rx_PDC_SNRI_Last_180d,USC_Rx_PDC_STEROID_INHALED_BRONCHIAL_Last_180d,USC_Rx_PDC_SULFONAMIDE_TRIMETHOPRIM_COMBINATIONS_Last_180d,USC_Rx_PDC_SYNTH_NON_NARC_INJECTABLE_Last_180d,USC_Rx_PDC_THYROID_HORMONE_NATURAL_Last_180d,USC_Rx_PDC_UT_ANTI_INFECTIVE_ANALGESIC_Last_180d,USC_Rx_PDC_SALICYLATES_AND_RELATED_Last_180d,USC_Rx_PDC_SCABICIDES_PEDICULOCIDES_Last_180d,USC_Rx_PDC_SEIZURE_DISORDERS_Last_180d,USC_Rx_PDC_SEROTONIN_5HT_1_RECEPTOR_AGONISTS_Last_180d,USC_Rx_PDC_SEXUAL_FUNCTION_DISORDER_Last_180d,USC_Rx_PDC_SMOKING_DETERRENTS_Last_180d,USC_Rx_PDC_SPECIFIC_ANTAGONISTS_Last_180d,USC_Rx_PDC_TETRACYCLINES_CONGENERS_Last_180d,USC_Rx_PDC_TRIAZOLES_Last_180d,USC_Rx_PDC_TRICHOMONACIDES_ANTIBACTERIALS_Last_180d,USC_Rx_PDC_TRICYCLICS_TETRACYCLICS_Last_180d,USC_Rx_PDC_UT_ANTISPASMODICS_Last_180d

,PDC_first_gen_Last_365d,PDC_Second_gen_Last_365d,PDC_Third_gen_Last_365d 
,PDC_first_gen_Last_180d,PDC_Second_gen_Last_180d,PDC_Third_gen_Last_180d

--INTO dbo.app_tds_IndexDate_PDC_AED_USC_Others_Details_Final
FROM [$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  A
LEFT JOIN PDC_365d_All B
ON A.Patient_id =B.Patient_id
AND A.Valid_Index = B.Valid_Index
LEFT JOIN PDC_180d_All C
ON A.Patient_id =C.Patient_id
AND A.Valid_Index = C.Valid_Index
LEFT JOIN PDC_365d_AED D
ON A.Patient_id =D.Patient_id
AND A.Valid_Index = D.Valid_Index
LEFT JOIN PDC_180d_AED E
ON A.Patient_id =E.Patient_id
AND A.Valid_Index = E.Valid_Index
LEFT JOIN PDC_365d_USC F
ON A.Patient_id =F.Patient_id
AND A.Valid_Index = F.Valid_Index
LEFT JOIN PDC_180d_USC G
ON A.Patient_id =G.Patient_id
AND A.Valid_Index = G.Valid_Index
LEFT JOIN PDC_365d_Generation H
ON A.Patient_id =H.Patient_id
AND A.Valid_Index = H.Valid_Index
LEFT JOIN PDC_180d_Generation I
ON A.Patient_id =I.Patient_id
AND A.Valid_Index = I.Valid_Index



select * from #Regiments_Stable_180d_Generation where patient_id = 83439249 order by 2,3,4,5
select datediff(dd,'2008-02-29','2008-08-06')+1 	
	select * from #Regimen_days_AED_PDC_180d_Generation  where patient_id = 83439249 order by 1,2,3,4
	
	*/

