-- ------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Scripts to run test scripts on the output of pipeline
-- - NOTE      : 	 
-- - Execution : 
-- - Date      : 27-Aug-2018		
-- ---------------------------------------------------------------------- 
Set Nocount on 
--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema


Print '####################################################################################################'
Print 'Negative Test Cases for Gap Elimination: No Records should be found ...'
-- Validating Overlap gap

Print ''
Print 'Negative Test Case for Overlapping Gaps: Here we look for patients where the prescriptions do not overlap, but yet are impacted by Overlapping Gap Logic'
Print ''
; With RCRD as
  (select row_number () over (partition by patient_id,aed order by patient_id,aed,startdate,enddate ) as NUM, 
  patient_id,aed,startdate,enddate from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
  ),
  FST AS
  (select  NUM, patient_id,aed,startdate,enddate from RCRD ),
  LST AS
  (select  NUM, patient_id,aed,startdate,enddate from RCRD )
  select FST.PATIENT_ID ,FST.AED,fst.enddate  ,LST.startdate,DATEDIFF(DAY,FST.enddate,LST.startdate) as date_diff from LST,FST 
  WHERE LST.PATIENT_ID=FST.PATIENT_ID AND LST.AED=FST.AED and fst.num=lst.num-1 and FST.enddate> LST.startdate

-- Validating Continuous gap
Print ''
Print 'Negative Test Case for Continuous Gaps: Here we look for patients where the prescriptions do not have gap, but yet are impacted by Continuous Gap Logic'
Print ''
; With RCRD as
  (select row_number () over (partition by patient_id,aed order by patient_id,aed,startdate,enddate ) as NUM, 
  patient_id,aed,startdate,enddate from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
  ),
  FST AS
  (select  NUM, patient_id,aed,startdate,enddate from RCRD ),
  LST AS
  (select  NUM, patient_id,aed,startdate,enddate from RCRD )
  select FST.PATIENT_ID ,FST.AED,fst.enddate  ,LST.startdate,DATEDIFF(DAY,FST.enddate,LST.startdate) as date_diff from LST,FST 
  WHERE LST.PATIENT_ID=FST.PATIENT_ID AND LST.AED=FST.AED and fst.num=lst.num-1 and FST.enddate= LST.startdate

-- Validating small Gap
Print ''
Print 'Negative Test Case for Small Gaps (Case 1): Here we check if there is no gap between prescriptions, but yet impacted by Small Gap Logic'
Print ''
-- Small Gap Case 1
; With RCRD as
(select row_number () over (partition by patient_id,aed order by patient_id,aed,startdate,enddate ) as NUM, 
patient_id,aed,startdate,enddate from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
),
FST AS
(select  NUM, patient_id,aed,startdate,enddate from RCRD ),
LST AS
(select  NUM, patient_id,aed,startdate,enddate from RCRD )
select FST.PATIENT_ID ,FST.AED,fst.enddate  ,LST.startdate,DATEDIFF(DAY,FST.enddate,LST.startdate) as date_diff from LST,FST 
WHERE LST.PATIENT_ID=FST.PATIENT_ID AND LST.AED=FST.AED and fst.num=lst.num-1 and datediff(day,FST.enddate, LST.startdate) -1 = 0
order by patient_id, aed, startdate

-- Small Gap Case 2
Print ''
Print 'Negative Test Case for Small Gaps (Case 2)'
Print ''
; With RCRD as
(select row_number () over (partition by patient_id,aed order by patient_id,aed,startdate,enddate ) as NUM, 
patient_id,aed,startdate,enddate from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
),
FST AS
(select  NUM, patient_id,aed,startdate,enddate from RCRD ),
LST AS
(select  NUM, patient_id,aed,startdate,enddate from RCRD )
select FST.PATIENT_ID ,FST.AED,fst.enddate  ,LST.startdate,DATEDIFF(DAY,FST.enddate,LST.startdate) as date_diff from LST,FST 
WHERE LST.PATIENT_ID=FST.PATIENT_ID AND LST.AED=FST.AED and fst.num=lst.num-1 and datediff(day,FST.enddate, LST.startdate) -1 = 1
order by patient_id, aed, startdate

-- Small Gap Case 3
Print ''
Print 'Negative Test Case for Small Gaps (Case 3)'
Print ''
; With RCRD as
(select row_number () over (partition by patient_id,aed order by patient_id,aed,startdate,enddate ) as NUM, 
patient_id,aed,startdate,enddate from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
),
FST AS
(select  NUM, patient_id,aed,startdate,enddate from RCRD ),
LST AS
(select  NUM, patient_id,aed,startdate,enddate from RCRD )
select FST.PATIENT_ID ,FST.AED,fst.enddate  ,LST.startdate,DATEDIFF(DAY,FST.enddate,LST.startdate) as date_diff from LST,FST 
WHERE LST.PATIENT_ID=FST.PATIENT_ID AND LST.AED=FST.AED and fst.num=lst.num-1 and datediff(day,FST.enddate, LST.startdate) -1 = 2
order by patient_id, aed, startdate

--Print 'No Records should be found for Gap Elimination'
Print '####################################################################################################'
-- Patients with Decimal Days Supply
Print''
Print 'Patients with Days Supply in Fraction'
Print ''
select count(patient_id) PatientsWithFractionDaysSupply
from [$(TargetSchema)].[app_int_GE_Base]
where days_supply like '%.%'

-- Continuous Prescription
Print 'Comparison of Patients before and after Gap Elimination Process'
Print ''
select 'Unique Number of Patient: ' + Convert(varchar(20),count(distinct patient_id)) + ' in AED_Analysis_Cohort_Base' 
from [$(TargetSchema)].[app_int_GE_Base]

Print''

select 'Unique Number of Patient: ' + Convert(varchar(20),count(distinct patient_id)) + ' in Continuous Prescription' 
from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
Print''
-- Missing AEDs

Print'Check for Missing Patient & AED .............Start '
select distinct patient_id,aed
from [$(TargetSchema)].[app_int_GE_Base]
except
select distinct patient_id,aed
from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
Print'Check for Missing Patient & AED ..............End '

/* TDS Cohort*/

Print''
Print'Patients Incorrectly Qualified for TDS Cohort...Start' 
Print''
select distinct patient_id,treatment_status  
from  [$(TargetSchema)].[app_tds_int_Output_Table_Filtered30Day]
where patient_id in 
(select patient_id  from [$(TargetSchema)].[app_int_GE_14_Days_Rule] group by patient_id having count (distinct maxdate)=1 ) 
and Treatment_status<>'New'
Print''
select distinct patient_id,treatment_status  
from  [$(TargetSchema)].[app_tds_cohort]
where patient_id in 
(select patient_id  from [$(TargetSchema)].[app_int_GE_14_Days_Rule] group by patient_id having count (distinct maxdate)=1 ) 
and Treatment_status='Add'
Print''
Print'Patient Incorrectly Qualified for TDS Cohort...End' 
Print''
