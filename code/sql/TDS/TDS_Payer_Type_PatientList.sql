/*
-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : PLATFORM-1946
-- - NOTE      :  
--				Logic:
--				1. Check for payer information from PX table at index date (consider Medicare , Medicaid and Private payers only)
--				2. Check for remaining patients from step 1 in PX table with +/-30 days window from index date. mark the payer nearest to index date.(consider Medicare , Medicaid and Private payers only)
--				3. Check for remaining patients from step 2 in RX table at index date. (consider Medicare , Medicaid and Private payers only)
--				4. Check for remaining patients from step 3 in RX table with +/- 30 days window from index date.(consider Medicare , Medicaid and Private payers only)
--				5. Check for remaining patients from step 4 in PX table at index (consider payer_type = Cash only)
--				6. Check remaining patients from step 5 in PX table with +/-30 days window from index date. mark the payer nearest to index date.(consider payer_type = Cash only)
--				7. Check for remaining patients from step 6 in RX table at index date.(consider payer_type = Cash only)
--				8. Check for remaining patients from step 7 in RX table with +/- 30 days window from index date.(consider payer_type = Cash only)
--				Consolidate all the patients mapped in different steps into single csv file
-- -			 
-- - Execution : Final table  name: [$(TargetSchema)].[TDS_Payer_Type_PatientList] 
-- - Date      : 06-OCT-2019
-- - Change History:	Change Date				Change Description
--						
-- -				
-- ---------------------------------------------------------------------- */

--:Setvar SrcSchema SHA2_Synoma_ID_SRC
--:setvar TargetSchema SHA2_Synoma_update_New_4_Nov

Set Nocount on

IF OBJECT_ID('tempdb..#Cohort_Temp') IS NOT NULL
	DROP TABLE #Cohort_Temp

SELECT	Patient_ID
		,[Valid_Index]
		,AED
INTO	[#Cohort_Temp]
FROM	[$(TargetSchema)].[app_tds_int_BaselineModel_Input_IndexDate]  WITH (NOLOCK)
WHERE	[Valid_Index] IS NOT NULL


IF OBJECT_ID('tempdb..#Cohort') IS NOT NULL
	DROP TABLE #Cohort

SELECT	A.[patient_id],A.[Valid_index],B.startdate AS [Original_Start],A.[aed] 
INTO	#Cohort
FROM	[#Cohort_Temp] A
JOIN	[$(TargetSchema)].[std_REF_AED_Abbreviation](NOLOCK) Abb
ON		A.aed	=	Abb.Abbreviation 
JOIN	[$(TargetSchema)].[app_int_GE_14_Days_Rule](NOLOCK) B
ON		A.patient_id	=	B.patient_id
AND		A.Valid_index BETWEEN B.Maxdate AND B.enddate
AND		Abb.AED = B.AED

--Patients payer infor at index date from PX table

IF OBJECT_ID('tempdb..#PX_TDS_IX') IS NOT NULL
DROP TABLE #PX_TDS_IX

SELECT *  
INTO #PX_TDS_IX
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Payer_type) AS ID ,* 
		FROM 
			(
				SELECT DISTINCT A.patient_id,A.[Valid_index],A.[Original_Start],A.[aed],PX.[Payer_type]
				FROM			#Cohort A
				INNER JOIN		[$(SrcSchema)].[src_dx_claims] PX
				ON				A.patient_id		=	PX.patient_id 
				AND				A.[Original_Start]	=	PX.service_date 
				WHERE			PX.Payer_type IN ('V','R','M','G')
			)A
	)B
WHERE B.ID = 1

--Patients payer info at +/- 30 window from PX table

IF OBJECT_ID('tempdb..#PX_TDS_Window') IS NOT NULL
DROP TABLE #PX_TDS_Window

SELECT *  
INTO #PX_TDS_Window
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Daysss,Payer_type) AS ID ,* 
		FROM 
			(
				SELECT	DISTINCT A.[patient_id],A.[Valid_index],A.[Original_Start],A.[aed],PX.[Payer_type] 
								,CASE WHEN	DATEDIFF(DD,A.[Original_Start],PX.[service_date]) < 0 THEN (DATEDIFF(DD,A.[Original_Start],PX.[service_date]))*(-1) 
									  ELSE	DATEDIFF(DD,A.[Original_Start],PX.[service_date]) 
								 END AS Daysss 
				FROM			#Cohort A
				INNER JOIN		[$(SrcSchema)].[src_dx_claims] PX 
				ON				A.patient_id	=	PX.patient_id 
				AND				PX.service_date BETWEEN DATEADD(DAY,-30,A.[Original_Start]) AND DATEADD(DAY,30,A.[Original_Start])
				LEFT JOIN		#PX_TDS_IX C
				ON				A.patient_id	=	C.patient_id
				AND				A.Valid_index	=	C.Valid_index
				WHERE			PX.Payer_type IN ('V','R','M','G')
				AND				C.patient_id IS NULL
			)A
	)B
WHERE B.ID = 1


IF OBJECT_ID('tempdb..#PX_patients') IS NOT NULL
DROP TABLE #PX_patients

SELECT Patient_id,Valid_index INTO #PX_patients FROM #PX_TDS_IX
UNION
SELECT Patient_id,Valid_index FROM #PX_TDS_Window



/* Get all the data for patients available in TDS Cohort from RX Table and where service date is same as index date*/

IF OBJECT_ID('tempdb..#RX_TDS_IX') IS NOT NULL
	DROP TABLE #RX_TDS_IX

SELECT *
INTO #RX_TDS_IX
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Payer_Type) AS ID,* 
	
		FROM 
			(
				SELECT DISTINCT A.patient_id,A.[Valid_index],A.[Original_Start],A.[aed],B.[Payer_type]
				FROM			[#Cohort] A
				JOIN			[$(SrcSchema)].[src_rx_claims] B WITH(NOLOCK)
				ON				A.[Patient_ID]		=	B.[Patient_ID]
				AND				A.[Original_Start]	=	B.[service_date]
				AND				B.Days_supply > 0
				LEFT JOIN		#PX_patients E
				ON				A.patient_id		=	E.patient_id
				AND				A.Valid_index		=	E.Valid_index
				WHERE			B.Payer_type IN ('V','R','M','G')
				AND				E.patient_id IS NULL
			)A
	)B
WHERE ID = 1



IF OBJECT_ID('tempdb..#RX_TDS_Window') IS NOT NULL
DROP TABLE #RX_TDS_Window

SELECT *  
INTO #RX_TDS_Window
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Daysss,Payer_type) AS ID ,* 
		FROM 
			(
				SELECT	DISTINCT A.[patient_id],A.[Valid_index],A.[Original_Start],A.[aed],B.[Payer_type] 
								,CASE	WHEN DATEDIFF(DD,A.[Original_Start],B.[service_date]) < 0 THEN (DATEDIFF(DD,A.[Original_Start],B.[service_date]))*(-1) 
										ELSE DATEDIFF(DD,A.[Original_Start],B.[service_date]) 
								END AS Daysss 
				FROM			#Cohort A
				INNER JOIN		[$(SrcSchema)].[src_rx_claims] B
				ON				A.patient_id	=	B.patient_id 
				AND				B.service_date BETWEEN DATEADD(DAY,-30,A.[Original_Start]) AND DATEADD(DAY,30,A.[Original_Start])
				AND				B.Days_supply > 0
				LEFT JOIN		#RX_TDS_IX E
				ON				A.patient_id	=	E.patient_id
				AND				A.Valid_index	=	E.Valid_index
				LEFT JOIN		#PX_patients F
				ON				A.patient_id	=	F.patient_id
				AND				A.Valid_index	=	F.Valid_index
				WHERE			B.Payer_type IN ('V','R','M','G')
				AND				E.patient_id IS NULL 
				AND				F.patient_id IS NULL
			)A
	)B
WHERE B.ID = 1



IF OBJECT_ID('tempdb..#Patient_Part1') IS NOT NULL
DROP TABLE #Patient_Part1

SELECT Patient_id,Valid_index,payer_type,'PX_TDS_IX' AS [Source] INTO #Patient_Part1 FROM #PX_TDS_IX
UNION
SELECT Patient_id,Valid_index,payer_type,'PX_TDS_Window'	FROM #PX_TDS_Window
UNION
SELECT Patient_id,Valid_index,payer_type,'RX_TDS_IX'		FROM #RX_TDS_IX
UNION 
SELECT Patient_id,Valid_index,payer_type,'RX_TDS_Window'	FROM #RX_TDS_Window


-------------*****************************************Handeling of Cash insurance type***************************************----------------------


IF OBJECT_ID('tempdb..#PX_TDS_IX_CASH') IS NOT NULL
DROP TABLE #PX_TDS_IX_CASH

SELECT *  
INTO #PX_TDS_IX_CASH
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Payer_type) AS ID ,* 
		FROM 
			(
				SELECT DISTINCT A.patient_id,A.[Valid_index],A.[Original_Start],A.[aed],PX.[Payer_type]
				FROM			#Cohort A
				INNER JOIN		[$(SrcSchema)].[src_dx_claims] PX
				ON				A.patient_id		=	PX.patient_id 
				AND				A.[Original_Start]	=	PX.service_date 
				LEFT JOIN		#Patient_Part1 B
				ON				A.patient_id		=	B.Patient_id
				AND				A.Valid_index		=	B.Valid_index
				WHERE			PX.Payer_type IN ('O') AND B.PATIENT_ID IS NULL
			)A
	)B
WHERE B.ID = 1



IF OBJECT_ID('tempdb..#PX_TDS_Window_CASH') IS NOT NULL
DROP TABLE #PX_TDS_Window_CASH

SELECT * 
INTO #PX_TDS_Window_CASH
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Daysss,Payer_type) AS ID ,* 
		FROM 
			(
				SELECT	DISTINCT A.[patient_id],A.[Valid_index],A.[Original_Start],A.[aed],PX.[Payer_type] 
								,CASE WHEN DATEDIFF(DD,A.[Original_Start],PX.[service_date]) < 0 THEN (DATEDIFF(DD,A.[Original_Start],PX.[service_date]))*(-1) 
									  ELSE  DATEDIFF(DD,A.[Original_Start],PX.[service_date]) 
								 END AS Daysss 
				FROM			#Cohort A
				INNER JOIN		[$(SrcSchema)].[src_dx_claims] PX 
				ON				A.patient_id	=	PX.patient_id 
				AND				PX.service_date BETWEEN DATEADD(DAY,-30,A.[Original_Start]) AND DATEADD(DAY,30,A.[Original_Start])
				LEFT JOIN		#PX_TDS_IX_CASH C
				ON				A.patient_id	=	C.patient_id
				AND				A.Valid_index	=	C.Valid_index
				LEFT JOIN		#Patient_Part1 D
				ON				A.patient_id	=	D.patient_id
				AND				A.Valid_index	=	D.Valid_index
				WHERE			PX.Payer_type IN ('O')
				AND				C.patient_id IS NULL AND D.patient_id IS NULL
			)A
	)B
WHERE B.ID = 1


IF OBJECT_ID('tempdb..#PX_patients_CASH') IS NOT NULL
DROP TABLE #PX_patients_CASH

SELECT Patient_id,Valid_index INTO #PX_patients_CASH FROM #PX_TDS_IX_CASH
UNION
SELECT Patient_id,Valid_index FROM #PX_TDS_Window_CASH
UNION 
SELECT Patient_id,Valid_index FROM  #Patient_Part1




/* Get all the data for patients available in TDS Cohort from RX Table and where service date is same as index date*/

IF OBJECT_ID('tempdb..#RX_TDS_IX_CASH') IS NOT NULL
	DROP TABLE #RX_TDS_IX_CASH

SELECT *
INTO #RX_TDS_IX_CASH
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Payer_Type) AS ID,* 
	
		FROM 
			(
				SELECT DISTINCT A.patient_id,A.[Valid_index],A.[Original_Start],A.[aed],B.[Payer_type]
				FROM			[#Cohort] A
				JOIN			[$(SrcSchema)].[src_rx_claims] B WITH(NOLOCK)
				ON				A.[Patient_ID]		=	B.[Patient_ID]
				AND				A.[Original_Start]	=	B.[service_date]
				AND				B.Days_supply > 0
				LEFT JOIN		#PX_patients_CASH E
				ON				A.patient_id		=	E.patient_id
				AND				A.Valid_index		=	E.Valid_index
				WHERE			B.Payer_type IN ('O')
				AND				E.patient_id IS NULL
			)A
	)B
WHERE ID = 1




IF OBJECT_ID('tempdb..#RX_TDS_Window_CASH') IS NOT NULL
DROP TABLE #RX_TDS_Window_CASH

SELECT *  
INTO #RX_TDS_Window_CASH
FROM 
	(
		SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id,Valid_index,Original_Start,Aed ORDER BY Daysss,Payer_type) AS ID ,* 
		FROM 
			(
				SELECT		DISTINCT A.[patient_id],A.[Valid_index],A.[Original_Start],A.[aed],B.[Payer_type] 
							,CASE	WHEN DATEDIFF(DD,A.[Original_Start],B.[service_date]) < 0 THEN (DATEDIFF(DD,A.[Original_Start],B.[service_date]))*(-1) 
									ELSE  DATEDIFF(DD,A.[Original_Start],B.[service_date]) 
							END AS Daysss 
				FROM		#Cohort A
				INNER JOIN	[$(SrcSchema)].[src_rx_claims] B
				ON			A.patient_id	=	B.patient_id 
				AND			B.service_date BETWEEN DATEADD(DAY,-30,A.[Original_Start]) AND DATEADD(DAY,30,A.[Original_Start])
				AND			B.Days_supply > 0
				LEFT JOIN	#RX_TDS_IX_CASH E
				ON			A.patient_id	=	E.patient_id
				AND			A.Valid_index	=	E.Valid_index
				LEFT JOIN	#PX_patients_CASH F
				ON			A.patient_id	=	F.patient_id
				AND			A.Valid_index	=	F.Valid_index
				WHERE		B.Payer_type IN ('O')
				AND			E.patient_id IS NULL 
				AND			F.patient_id IS NULL
			)A
	)B
WHERE B.ID = 1



IF OBJECT_ID('tempdb..#Patient_Part2') IS NOT NULL
DROP TABLE #Patient_Part2

SELECT Patient_id,Valid_index,payer_type,'PX_TDS_IX_CASH' AS [Source] INTO #Patient_Part2 FROM #PX_TDS_IX_CASH
UNION
SELECT Patient_id,Valid_index,payer_type,'PX_TDS_Window_CASH'	FROM #PX_TDS_Window_CASH
UNION
SELECT Patient_id,Valid_index,payer_type,'RX_TDS_IX_CASH'		FROM #RX_TDS_IX_CASH
UNION 
SELECT Patient_id,Valid_index,Payer_type,'RX_TDS_Window_CASH'	FROM #RX_TDS_Window_CASH



IF OBJECT_ID('tempdb..#Payer_type_Patient_list') IS NOT NULL
DROP TABLE #Payer_type_Patient_list

SELECT * INTO #Payer_type_Patient_list FROM #Patient_Part1
UNION
SELECT * FROM #Patient_Part2



IF OBJECT_ID('[$(TargetSchema)].[TDS_Payer_Type_PatientList]','U') IS NOT NULL
DROP TABLE [$(TargetSchema)].[TDS_Payer_Type_PatientList]

SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id ORDER BY Valid_index ) AS Index_Id , patient_id,Valid_Index,
CASE WHEN Payer_type IN ('R','G') THEN 'Medicare' WHEN Payer_Type = 'M' THEN 'Medicaid' WHEN Payer_Type = 'V' THEN 'Commercial' ELSE 'Cash' END AS Payer_Type
INTO [$(TargetSchema)].[TDS_Payer_Type_PatientList]
FROM #Payer_type_Patient_list 


SELECT  [Index_Id]
		,[patient_id]
		,[Valid_Index]
		,[Payer_Type]
FROM	[$(TargetSchema)].[TDS_Payer_Type_PatientList]