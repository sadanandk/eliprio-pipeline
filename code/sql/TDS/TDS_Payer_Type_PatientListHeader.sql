-- ---------------------------------------------------------------------
-- - CITIUS TECH
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - NOTE      : Generate header for Payer type csv
--				 
-- - Execution : 
-- - Date      : 07-Nov-2019		
-- -
	-- ---------------------------------------------------------------------- 
	
Set Nocount on

SELECT  [Index_Id]
		,[patient_id]
		,[Valid_Index]
		,[Payer_Type]
FROM	[$(TargetSchema)].[TDS_Payer_Type_PatientList]
WHERE 1<>1