-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Truncate table with sample data for populating production data
-- - NOTE      : 
--				 
-- - Execution : 
-- - Date      : 28-Sept-2018		
						
	-- ---------------------------------------------------------------------- 

-- update the schema name

truncate table [$(TargetSchema)].src_sample_dx_claims
truncate table [$(TargetSchema)].src_sample_hosp_encounter
truncate table [$(TargetSchema)].src_sample_hosp_procedure
truncate table [$(TargetSchema)].src_sample_ptnt_eligibility
truncate table [$(TargetSchema)].src_sample_ref_patient
truncate table [$(TargetSchema)].src_sample_ref_provider
truncate table [$(TargetSchema)].src_sample_rx_claims
truncate table [$(TargetSchema)].src_sample_pharmacy_stability