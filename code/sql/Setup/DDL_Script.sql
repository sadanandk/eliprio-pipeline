-- ------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Create structure for Source tables 
-- - NOTE      : 
-- -			 	 
--				 
-- - Execution : 
-- - Date      : 31-Aug-2018		
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ---------------------------------------------------------------------- 


-- Create schema if not exists
--:Setvar Schema Testpipeline_stg
/*Schemaname*/ -- update the schema name 

IF NOT EXISTS (select * from sys.schemas where name = '$(TargetSchema)')

BEGIN
EXEC sp_executesql N'CREATE SCHEMA $(TargetSchema)'
END



/* create tables if not exists (vendor supplied) */

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[AuditTable]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[AuditTable]
(
Step varchar(100) NULL,
StartTime datetime NULL,
EndTime  datetime NULL,
Flag char default 'N'
)


Insert [$(TargetSchema)].[AuditTable] (Step) values( 'Preprocess-Table structure')

END

Update [$(TargetSchema)].[AuditTable] Set StartTime=getdate(), Flag='P'


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_rx_claims]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[src_rx_claims]
(
	[SERVICE_DATE] [date] NULL,
	[AUTHORIZED_REFILLS] [float] NULL,
	[DAW] [varchar](2) NULL,
	[DAYS_SUPPLY] [float] NULL,
	[FILL_NBR] varchar(10) NULL, -- from float to varchar
	[NDC] [varchar](30) NULL, -- from 11 to 30 size
	[PATIENT_ID] [varchar](30) NULL,-- from 10 30 
	[SERVICE_CHARGE] float NULL,--varchar to float
	[PAYER_TYPE] [varchar](1) NULL,
	[PHARMACY_ID] [varchar](30) NULL,
	[PROVIDER_ID] [varchar](30) NULL, -- from 10 to 30
	[QUAN] float NULL -- numeric 18,2 to float 
)

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_rx_claims]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[sample_src_rx_claims]
(
	[SERVICE_DATE] [date] NULL,
	[AUTHORIZED_REFILLS] [float] NULL,
	[DAW] [varchar](2) NULL,
	[DAYS_SUPPLY] [float] NULL,
	[FILL_NBR] [float] NULL,
	[NDC] [varchar](11) NULL,
	[PATIENT_ID] [varchar](10) NULL,
	[SERVICE_CHARGE] [varchar](10) NULL,
	[PAYER_TYPE] [varchar](1) NULL,
	[PHARMACY_ID] [varchar](10) NULL,
	[PROVIDER_ID] [varchar](10) NULL,
	[QUAN] [numeric](18, 2) NULL
)
END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_hosp_encounter]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[src_hosp_encounter]
(
	[FROM_DT] [date] NULL,
	[TO_DT] [date] NULL,
	[ADMIT_DIAG] [varchar](10) NULL,
	[ADMIT_SRC] [varchar](10) NULL,
	[DIAG1] [varchar](10) NULL,
	[DIAG2] [varchar](10) NULL,
	[DIAG3] [varchar](10) NULL,
	[DIAG4] [varchar](10) NULL,
	[DIAG5] [varchar](10) NULL,
	[DIAG6] [varchar](10) NULL,
	[DIAG7] [varchar](10) NULL,
	[DIAG8] [varchar](10) NULL,
	[DISCHARGE_DISP] [varchar](130) NULL,
	[VISIT_ID] [varchar](25) NULL,
	[SERVICE_CHARGE] [float] NULL,
	[FACILITY_ID] [varchar](10) NULL,
	[PATIENT_ID] [varchar](10) NULL,
	[IP_OP_FLAG] [varchar](1) NULL,
	[PAYER_TYPE] [varchar](1) NULL,
	[DIAG_VERS_TYP_ID1] [float] NULL,
	[DIAG_VERS_TYP_ID2] [float] NULL,
	[DIAG_VERS_TYP_ID3] [float] NULL,
	[DIAG_VERS_TYP_ID4] [float] NULL,
	[DIAG_VERS_TYP_ID5] [float] NULL,
	[DIAG_VERS_TYP_ID6] [float] NULL,
	[DIAG_VERS_TYP_ID7] [float] NULL,
	[DIAG_VERS_TYP_ID8] [float] NULL,
	[ADMIT_DIAG_VERS_TYP_ID] [float] NULL
)

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_hosp_encounter]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[sample_src_hosp_encounter]
(
	[FROM_DT] [date] NULL,
	[TO_DT] [date] NULL,
	[ADMIT_DIAG] [varchar](10) NULL,
	[ADMIT_SRC] [varchar](10) NULL,
	[DIAG1] [varchar](10) NULL,
	[DIAG2] [varchar](10) NULL,
	[DIAG3] [varchar](10) NULL,
	[DIAG4] [varchar](10) NULL,
	[DIAG5] [varchar](10) NULL,
	[DIAG6] [varchar](10) NULL,
	[DIAG7] [varchar](10) NULL,
	[DIAG8] [varchar](10) NULL,
	[DISCHARGE_DISP] [varchar](130) NULL,
	[VISIT_ID] [varchar](25) NULL,
	[SERVICE_CHARGE] [float] NULL,
	[FACILITY_ID] [varchar](10) NULL,
	[PATIENT_ID] [varchar](10) NULL,
	[IP_OP_FLAG] [varchar](1) NULL,
	[PAYER_TYPE] [varchar](1) NULL,
	[DIAG_VERS_TYP_ID1] [float] NULL,
	[DIAG_VERS_TYP_ID2] [float] NULL,
	[DIAG_VERS_TYP_ID3] [float] NULL,
	[DIAG_VERS_TYP_ID4] [float] NULL,
	[DIAG_VERS_TYP_ID5] [float] NULL,
	[DIAG_VERS_TYP_ID6] [float] NULL,
	[DIAG_VERS_TYP_ID7] [float] NULL,
	[DIAG_VERS_TYP_ID8] [float] NULL,
	[ADMIT_DIAG_VERS_TYP_ID] [float] NULL
)

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_hosp_procedure]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(TargetSchema)].[src_hosp_procedure]
(
	[PROC_DATE] [date] NULL,
	[VISIT_ID] [varchar](10) NULL,
	[PROC_CD] [varchar](10) NULL,
	[PROC_VERS_TYP_ID] [float] NULL
)

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_hosp_procedure]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(TargetSchema)].[sample_src_hosp_procedure]
(
	[PROC_DATE] [date] NULL,
	[VISIT_ID] [varchar](10) NULL,
	[PROC_CD] [varchar](10) NULL,
	[PROC_VERS_TYP_ID] [float] NULL
)

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_dx_claims]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(TargetSchema)].[src_dx_claims]
(
	[SERVICE_DATE] [date] NULL,
	[DIAG1] [varchar](10) NULL,
	[DIAG2] [varchar](10) NULL,
	[DIAG3] [varchar](10) NULL,
	[DIAG4] [varchar](10) NULL,
	[DIAG5] [varchar](10) NULL,
	[DIAG6] [varchar](10) NULL,
	[DIAG7] [varchar](10) NULL,
	[DIAG8] [varchar](10) NULL,
	[PATIENT_ID] [varchar](30) NULL, -- 10 to 30 
	[PAYER_TYPE] [varchar](10) NULL,
	[PLACE_OF_SERVICE] [varchar](30) NULL,
	[PROC_CD] [varchar](10) NULL, -- 5 to 10 
	[PROVIDER_ID] [varchar](30) NULL, -- 10 to 30 
	[SERVICE_CHARGE] [float] NULL,
	[QUAN] [float] NULL,
	[VISIT_ID] [varchar](60) NULL,
	[DIAG_VERS_TYP_ID1] float NULL, -- int to float
	[DIAG_VERS_TYP_ID2] float NULL, -- int to float
	[DIAG_VERS_TYP_ID3] float NULL, -- int to float
	[DIAG_VERS_TYP_ID4] float NULL, -- int to float
	[DIAG_VERS_TYP_ID5] float NULL, -- int to float
	[DIAG_VERS_TYP_ID6] float NULL, -- int to float
	[DIAG_VERS_TYP_ID7] float NULL, -- int to float
	[DIAG_VERS_TYP_ID8] float NULL  -- int to float
)

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_dx_claims]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(TargetSchema)].[sample_src_dx_claims]
(
	[SERVICE_DATE] [date] NULL,
	[DIAG1] [varchar](10) NULL,
	[DIAG2] [varchar](10) NULL,
	[DIAG3] [varchar](10) NULL,
	[DIAG4] [varchar](10) NULL,
	[DIAG5] [varchar](10) NULL,
	[DIAG6] [varchar](10) NULL,
	[DIAG7] [varchar](10) NULL,
	[DIAG8] [varchar](10) NULL,
	[PATIENT_ID] [varchar](10) NULL,
	[PAYER_TYPE] [varchar](1) NULL,
	[PLACE_OF_SERVICE] [varchar](20) NULL,
	[PROC_CD] [varchar](5) NULL,
	[PROVIDER_ID] [varchar](10) NULL,
	[SERVICE_CHARGE] [float] NULL,
	[QUAN] [numeric](18,2) NULL,
	[VISIT_ID] [varchar](25) NULL,
	[DIAG_VERS_TYP_ID1] [float] NULL,
	[DIAG_VERS_TYP_ID2] [float] NULL,
	[DIAG_VERS_TYP_ID3] [float] NULL,
	[DIAG_VERS_TYP_ID4] [float] NULL,
	[DIAG_VERS_TYP_ID5] [float] NULL,
	[DIAG_VERS_TYP_ID6] [float] NULL,
	[DIAG_VERS_TYP_ID7] [float] NULL,
	[DIAG_VERS_TYP_ID8] [float] NULL
)
END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_ptnt_eligibility]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[src_ptnt_eligibility]
(
	[MONTH_ID] [float] NULL,
	[PATIENT_ID] [varchar](120) NULL,
	[RX_ACTIVITY] [varchar](1) NULL,
	[DX_ACTIVITY] [varchar](1) NULL,
	[HOSP_ACTIVITY] [varchar](1) NULL
)



END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ptnt_eligibility]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[sample_src_ptnt_eligibility]
(
	[MONTH_ID] [float] NULL,
	[PATIENT_ID] [varchar](120) NULL,
	[RX_ACTIVITY] [varchar](1) NULL,
	[DX_ACTIVITY] [varchar](1) NULL,
	[HOSP_ACTIVITY] [varchar](1) NULL
)

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_pharmacy_panel]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[src_pharmacy_panel]
(
	[PHARMACY_ID] [varchar](10) NULL,
	[MONTH_ID] [varchar](10) NULL,
	[STABLE_FLAG] [varchar](10) NULL
)

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_pharmacy_panel]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[sample_src_pharmacy_panel]
(	
	[PHARMACY_ID] [varchar](10) NULL,
	[MONTH_ID] [varchar](10) NULL,
	[STABLE_FLAG] [varchar](10) NULL
)

END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_REF_patient]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[src_REF_patient]
(
	[PATIENT_ID] [varchar](100) NULL,
	[YOB] [float] NULL,
	[SEX] [varchar](1) NULL,
	[GROUP_IND] [float] NULL,
	[ZIP3] [varchar](3) NULL,
	[REGION] [varchar](1) NULL
)


END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_REF_patient]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[sample_src_REF_patient]
(
	[PATIENT_ID] [varchar](100) NULL,
	[YOB] [float] NULL,
	[SEX] [varchar](1) NULL,
	[GROUP_IND] [float] NULL,
	[ZIP3] [varchar](3) NULL,
	[REGION] [varchar](1) NULL
)


END



IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_REF_Provider]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[src_REF_Provider]
(
	[ADDRESS_1] [varchar](150) NULL,
	[ADDRESS_2] [varchar](100) NULL,
	[CITY] [varchar](40) NULL,
	[DEA_NUMBER] [varchar](50) NULL,
	[FIRST_NAME] [varchar](40) NULL,
	[LAST_NAME] [varchar](40) NULL,
	[MIDDLE_NAME] [varchar](40) NULL,
	[NAME] [varchar](120) NULL,
	[NPI] [varchar](30) NULL,
	[PRI_SPECIALTY_CD] [varchar](10) NULL,
	[PRI_SPECIALTY_DESC] [varchar](100) NULL,
	[PROVIDER_CENSUS_REGION] [varchar](1) NULL,
	[PROVIDER_ID] [varchar](120) NULL,
	[PROVIDER_TYPE_ID] [varchar](10) NULL,
	[SEC_SPECIALTY_CD] [varchar](10) NULL,
	[SEC_SPECIALTY_DESC] [varchar](100) NULL,
	[STATE_CD] [varchar](3) NULL,
	[ZIPCODE] [varchar](5) NULL
)


END



IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_REF_Provider]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[sample_src_REF_Provider]
(
	[ADDRESS_1] [varchar](150) NULL,
	[ADDRESS_2] [varchar](100) NULL,
	[CITY] [varchar](40) NULL,
	[DEA_NUMBER] [varchar](10) NULL,
	[FIRST_NAME] [varchar](40) NULL,
	[LAST_NAME] [varchar](40) NULL,
	[MIDDLE_NAME] [varchar](40) NULL,
	[NAME] [varchar](120) NULL,
	[NPI] [varchar](10) NULL,
	[PRI_SPECIALTY_CD] [varchar](10) NULL,
	[PRI_SPECIALTY_DESC] [varchar](100) NULL,
	[PROVIDER_CENSUS_REGION] [varchar](1) NULL,
	[PROVIDER_ID] [varchar](120) NULL,
	[PROVIDER_TYPE_ID] [varchar](10) NULL,
	[SEC_SPECIALTY_CD] [varchar](10) NULL,
	[SEC_SPECIALTY_DESC] [varchar](100) NULL,
	[STATE_CD] [varchar](3) NULL,
	[ZIPCODE] [varchar](5) NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[src_REF_product]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[src_REF_product]
(
	[AHFS_CLASS] [varchar](40) NULL,
	[AHFS_DESC] [varchar](100) NULL,
	[BRAND_GENERIC_FLAG] [varchar](10) NULL,
	[BRAND_NAME] [varchar](80) NULL,
	[FORM_CD] [varchar](4) NULL,
	[FORM_DESC] [varchar](100) NULL,
	[GENERIC_NAME] [varchar](100) NULL,
	[GPI14] [varchar](30) NULL,
	[MFG_NAME] [varchar](50) NULL,
	[NDC] [varchar](15) NULL,
	[PACKSIZE] [float] NULL,
	[PACKSIZE_UOM] [varchar](2) NULL,
	[ROUTE_DESC] [varchar](30) NULL,
	[RX_OTC_CD] [varchar](1) NULL,
	[STRENGTH] [varchar](80) NULL,
	[USC_CD] [varchar](10) NULL,
	[USC_NAME] [varchar](100) NULL
)


END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_REF_product]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[sample_src_REF_product]
(
	[AHFS_CLASS] [varchar](40) NULL,
	[AHFS_DESC] [varchar](100) NULL,
	[BRAND_GENERIC_FLAG] [varchar](10) NULL,
	[BRAND_NAME] [varchar](80) NULL,
	[FORM_CD] [varchar](4) NULL,
	[FORM_DESC] [varchar](100) NULL,
	[GENERIC_NAME] [varchar](100) NULL,
	[GPI14] [varchar](30) NULL,
	[MFG_NAME] [varchar](50) NULL,
	[NDC] [varchar](15) NULL,
	[PACKSIZE] [float] NULL,
	[PACKSIZE_UOM] [varchar](2) NULL,
	[ROUTE_DESC] [varchar](30) NULL,
	[RX_OTC_CD] [varchar](1) NULL,
	[STRENGTH] [varchar](80) NULL,
	[USC_CD] [varchar](10) NULL,
	[USC_NAME] [varchar](100) NULL
)


END


/* create tables if not exists (custom created) */


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_icd9_icd10_cmgem]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_icd9_icd10_cmgem]
(
	[icd9cm] [varchar](50) NULL,
	[icd10cm] [varchar](50) NULL,
	[flags] [varchar](50) NULL,
	[approximate] [varchar](50) NULL,
	[no_map] [varchar](50) NULL,
	[combination] [varchar](50) NULL,
	[scenario] [varchar](50) NULL,
	[choice_list] [varchar](50) NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_MedicationCodesUSP]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_MedicationCodesUSP]
(
	[USP_Category] [varchar](5000) NULL,
	[USP Class] [varchar](5000) NULL,
	[Generic_Name] [varchar](5000) NULL
)

END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_AEDName]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_AEDName]
(
	[generic_name] [varchar](500) NULL,
	[aed] [varchar](500) NULL,
	[EP0102_AedName] [varchar](50) NULL,
	[Abbreviation] [varchar](3) NULL,
	[MoA_abbr] [varchar](10) NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_CPT_Code_All]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_CPT_Code_All]
(
	[CPT_CODE] [varchar](50) NULL,
	[CCS_CATEGORY] [varchar](50) NULL,
	[Code_Type] [varchar](50) NULL,
	[CCS_DESC] [varchar](1000) NULL,
	[CCS_DESC_NEW] [varchar](1000) NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_AED_Abbreviation]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_AED_Abbreviation]
(
	[AED] [varchar](100) NULL,
	[Abbreviation] [varchar](3) NULL,
	[AED_Generation] [varchar](10) NULL,
	[Primary_MOA] [varchar](20) NULL,
	[Abbreviation_MoA] [varchar](10) NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_CCI]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_CCI]
(
	[Source] [varchar](2) NOT NULL,
	[CCI_Category] [varchar](50) NULL,
	[Type of Code] [varchar](50) NULL,
	[Codes] [varchar](50) NULL,
	[Weightage_CCI] [int] NOT NULL,
	[Weightage_ECI] [int] NULL
)



END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_ICD_Code_All]') AND type in (N'U'))

BEGIN

CREATE TABLE [$(TargetSchema)].[std_REF_ICD_Code_All]
(
	[TYPE] [nvarchar](255) NULL,
	[ICD_CODE] [nvarchar](255) NULL,
	[CCS CATEGORY] [nvarchar](255) NULL,
	[CCS CATEGORY DESCRIPTION] [nvarchar](255) NULL
)


END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]
(
	[ICD codes] [varchar](50) NULL,
	[type] [varchar](50) NULL,
	[ICD9 to ICD 10 code(if service date<1st Oct 2015)] [varchar](50) NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_USPS_zip3zip1codes]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_USPS_zip3zip1codes]
(
	[Zip1_Code] [varchar](50) NULL,
	[Zip3_code] [varchar](50) NULL,
	[State] [varchar](50) NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_time_dimension]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_REF_time_dimension]
(
	[Sr_No_Q] int NULL,
	[Sr_No_month] int NULL,
	[Year] int NULL,
	[Month] int NULL,
	[Quarter] [varchar](50) NULL,
	[YYYYMM] int NULL
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_Rpt_TDS_Cohort_characterization_Train_Test_SHA]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_Rpt_TDS_Cohort_characterization_Train_Test_SHA]
(
	[ID] [Decimal] (5,1) NULL,
	[Variable] [Varchar] (300) NULL,
	[Label] [Varchar] (300) NULL,
	[Train] [Varchar] (300) NULL,
	[Test] [Varchar] (300) NULL,
	[SHA] [Varchar] (300) NULL
	--[Test] [Varchar] (300) NULL,
)


END


IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_SHA_patient_list]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[Std_Rpt_TDS_SHA_patient_list]
(
	[Patient_id] [Varchar] (100) NULL,
	[Valid_index] [Date] NULL,
	[Index_id] INT NULL
)


END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_Train_patient_list]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[Std_Rpt_TDS_Train_patient_list]
(
	[Patient_id] [Varchar] (100) NULL,
	[Valid_index] [Date] NULL,
	[Index_id] INT NULL
)
END 

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_Test_patient_list]') AND type in (N'U'))


BEGIN
CREATE TABLE [$(TargetSchema)].[Std_Rpt_TDS_Test_patient_list]
(
	[Patient_id] [Varchar] (100) NULL,
	[Valid_index] [Date] NULL,
	[Index_id] INT NULL
)


END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[PHPH_SynomaIDs]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(SrcSchema)].[PHPH_SynomaIDs]
(
	[SHS_PATIENT_ID] VARCHAR(50) NULL
   ,[Pat_ID] VARCHAR(100) NULL
   ,[Patient_ID] VARCHAR(20) NULL
)


END

IF  NOT EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_Rpt_TDS_patient_timeline_statistic]') AND type in (N'U'))

BEGIN
CREATE TABLE [$(TargetSchema)].[std_Rpt_TDS_patient_timeline_statistic]
(
	[ID] Decimal(5,1) NULL
   ,[Variable] VARCHAR(50) NULL
   ,[Train data] VARCHAR(100) NULL
   ,[Test data] VARCHAR(100) NULL
   ,[SHA data] VARCHAR(100) NULL
)


END




Update [$(TargetSchema)].[AuditTable] Set EndTime=getdate(), Flag='Y'
