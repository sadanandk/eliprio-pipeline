-- ------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Pre Processing For inserting sample data into base tables 
-- - NOTE      : 
-- -			 	 
--				 
-- - Execution : 
-- - Date      : 23-Aug-2018		
-- - Change History:     Change Date     Change Description
-- - 			   :    03-Dec-2018       Added Begin and End clause for each IF statements and commented out the insertion for ptnt_eligibility & time dimension
-- -					03-Jun-2019		  Defect ELCDS-1582 : converted std_REF_MedicationCodesUSP.tsv from CSV to TSV
 
 					
-- ---------------------------------------------------------------------- 

/* create tables if not exists (vendor supplied) */
--:SetVar SampleDataPath SampleDataRun\CSV_for_Tables\
--:Setvar Schema TestPipeline_stg

Print 'Populate sample_src_rx_claims'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_rx_claims]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_rx_claims]
	BULK INSERT [$(TargetSchema)].[sample_src_rx_claims]
	FROM '$(SampleDataPath)src_sample\src_sample_rx_claims.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_hosp_encounter'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_hosp_encounter]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE [$(TargetSchema)].[sample_src_hosp_encounter]
	BULK INSERT [$(TargetSchema)].[sample_src_hosp_encounter]
	FROM '$(SampleDataPath)src_sample\src_sample_hosp_encounter.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_hosp_procedure'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_hosp_procedure]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE [$(TargetSchema)].[sample_src_hosp_procedure]
	BULK INSERT [$(TargetSchema)].[sample_src_hosp_procedure]
	FROM '$(SampleDataPath)src_sample\src_sample_hosp_procedure.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_dx_claims'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_dx_claims]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_dx_claims]
	BULK INSERT [$(TargetSchema)].[sample_src_dx_claims]
	FROM '$(SampleDataPath)src_sample\src_sample_dx_claims.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_ptnt_eligibility'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ptnt_eligibility]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ptnt_eligibility]
	BULK INSERT [$(TargetSchema)].[sample_src_ptnt_eligibility]
	FROM '$(SampleDataPath)src_sample\src_sample_ptnt_eligibility.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);
END

Print 'Populate sample_src_pharmacy_panel'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_pharmacy_panel]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_pharmacy_panel]
	BULK INSERT [$(TargetSchema)].[sample_src_pharmacy_panel]
	FROM '$(SampleDataPath)src_sample\src_sample_pharmacy_panel.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);
END

Print 'Populate sample_src_ref_patient'
Print Getdate()


IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ref_patient]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ref_patient]
	BULK INSERT  [$(TargetSchema)].[sample_src_ref_patient]
	FROM '$(SampleDataPath)src_sample\src_sample_ref_patient.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);  
END

Print 'Populate sample_src_ref_Provider'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ref_Provider]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ref_Provider]
	BULK INSERT [$(TargetSchema)].[sample_src_ref_Provider]
	FROM '$(SampleDataPath)src_sample\src_sample_ref_Provider.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_ref_product'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ref_product]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ref_product]
	BULK INSERT [$(TargetSchema)].[sample_src_ref_product]
	FROM '$(SampleDataPath)src_sample\src_sample_ref_product.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

--/* Custom Ref Tables */

Print 'Populate std_REF_icd9_icd10_cmgem'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_icd9_icd10_cmgem]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_icd9_icd10_cmgem]
	BULK INSERT [$(TargetSchema)].[std_REF_icd9_icd10_cmgem]
	FROM '$(SampleDataPath)std_ref\std_REF_icd9_icd10_cmgem.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_MedicationCodesUSP'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_MedicationCodesUSP]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_MedicationCodesUSP]
	BULK INSERT [$(TargetSchema)].[std_REF_MedicationCodesUSP]
	FROM '$(SampleDataPath)std_ref\std_REF_MedicationCodesUSP.tsv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator ='\t'
	); 
END

Print 'Populate std_REF_AEDName'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_AEDName]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_AEDName]
	BULK INSERT [$(TargetSchema)].[std_REF_AEDName]
	FROM '$(SampleDataPath)std_ref\std_REF_AEDName.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_CPT_Code_All'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_CPT_Code_All]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_CPT_Code_All]
	BULK INSERT [$(TargetSchema)].[std_REF_CPT_Code_All]
	FROM '$(SampleDataPath)std_ref\std_REF_CPT_Code_All.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_AED_Abbreviation'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_AED_Abbreviation]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_AED_Abbreviation]
	BULK INSERT [$(TargetSchema)].[std_REF_AED_Abbreviation]
	FROM '$(SampleDataPath)std_ref\std_REF_AED_Abbreviation.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_CCI'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_CCI]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_CCI]
	BULK INSERT [$(TargetSchema)].[std_REF_CCI]
	FROM '$(SampleDataPath)std_ref\std_REF_CCI.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_ICD_Code_All'
Print Getdate()


IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_ICD_Code_All]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_ICD_Code_All]
	BULK INSERT [$(TargetSchema)].[std_REF_ICD_Code_All]
	FROM '$(SampleDataPath)std_ref\std_REF_ICD_Code_All.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_ICD9_To_ICD10_Conversion'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]
	BULK INSERT [$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]
	FROM '$(SampleDataPath)std_ref\std_REF_ICD9_To_ICD10_Conversion.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_USPS_zip3zip1codes'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_USPS_zip3zip1codes]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_USPS_zip3zip1codes]
	BULK INSERT [$(TargetSchema)].[std_REF_USPS_zip3zip1codes]
	FROM '$(SampleDataPath)std_ref\std_REF_USPS_zip3zip1codes.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_time_dimension'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_time_dimension]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].std_REF_time_dimension

	BULK INSERT [$(TargetSchema)].std_REF_time_dimension
	FROM '$(SampleDataPath)std_ref\std_REF_time_dimension.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 

/*DRE Score File Start*/

--Score File
IF  OBJECT_ID(N'[$(TargetSchema)].[d5_dre_sha_score_1_0yr_Test]') is Null
Begin
CREATE TABLE [$(TargetSchema)].d5_dre_sha_score_1_0yr_Test
(
	[patient_id] [varchar](50) NULL,
	[Index_Date] [varchar](50) NULL,
	[Age] [varchar](50) NULL,
	[Gender] [varchar](50) NULL,
	[CCI_Score] [varchar](50) NULL,
	[Outcome_variable] [varchar](50) NULL,
	[Model Label] [varchar](50) NULL,
	[Class probabilities] [varchar](50) NULL,
	[DRERiskStratification] [varchar](50) NULL
)
End


IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[d5_dre_sha_score_1_0yr_Test]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].d5_dre_sha_score_1_0yr_Test
	BULK INSERT [$(TargetSchema)].d5_dre_sha_score_1_0yr_Test
	FROM '$(SampleDataPath)src_sample\d5_dre_sha_score_1_0yr_Test.csv'
	WITH 
	(
	FirstRow=2,
	rowterminator = '\n',
	fieldterminator =','
	); 
END -- 94298

IF  OBJECT_ID(N'[$(TargetSchema)].[d5_dre_sha_score_1_0yr_Train]') is Null
Begin
CREATE TABLE [$(TargetSchema)].d5_dre_sha_score_1_0yr_Train
(
	[patient_id] [varchar](50) NULL,
	[Index_Date] [varchar](50) NULL,
	[Age] [varchar](50) NULL,
	[Gender] [varchar](50) NULL,
	[CCI_Score] [varchar](50) NULL,
	[Outcome_variable] [varchar](50) NULL,
	[Model Label] [varchar](50) NULL,
	[Class probabilities] [varchar](50) NULL,
	[DRERiskStratification] [varchar](50) NULL
)
End

--patient_id,Index date,Age,Gender,CCI Score,Outcome_variable,Model Label,Class probabilities,DRERiskStratification

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[d5_dre_sha_score_1_0yr_Train]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].d5_dre_sha_score_1_0yr_Train
	BULK INSERT [$(TargetSchema)].d5_dre_sha_score_1_0yr_Train
	FROM '$(SampleDataPath)src_sample\d5_dre_sha_score_1_0yr_Train.csv'
	WITH 
	(
	FirstRow=2,
	rowterminator = '\n',
	fieldterminator =','
	); 
END -- 377,227

IF  OBJECT_ID(N'[$(TargetSchema)].[d6_5_d5_dre_sha_score_2_5yr_Test]') is Null
Begin
CREATE TABLE [$(TargetSchema)].d6_5_d5_dre_sha_score_2_5yr_Test
(
	[patient_id] [varchar](50) NULL,
	[Index_Date] [varchar](50) NULL,
	[Age] [varchar](50) NULL,
	[Gender] [varchar](50) NULL,
	[CCI_Score] [varchar](50) NULL,
	[Outcome_variable] [varchar](50) NULL,
	[Model Label] [varchar](50) NULL,
	[Class probabilities] [varchar](50) NULL,
	[DRERiskStratification] [varchar](50) NULL
)
End

GO
--patient_id,Index date,Age,Gender,CCI Score,Outcome_variable,Model Label,Class probabilities,DRERiskStratification

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[d6_5_d5_dre_sha_score_2_5yr_Test]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].d6_5_d5_dre_sha_score_2_5yr_Test
	BULK INSERT [$(TargetSchema)].d6_5_d5_dre_sha_score_2_5yr_Test
	FROM '$(SampleDataPath)src_sample\d6_5_d5_dre_sha_score_2_5yr_Test.csv'
	WITH 
	(
	FirstRow=2,
	rowterminator = '\n',
	fieldterminator =','
	); 
END -- 70594

IF  OBJECT_ID(N'[$(TargetSchema)].[d6_5_d5_dre_sha_score_2_5yr_Train]') is Null
Begin
CREATE TABLE [$(TargetSchema)].d6_5_d5_dre_sha_score_2_5yr_Train
(
	[patient_id] [varchar](50) NULL,
	[Index_Date] [varchar](50) NULL,
	[Age] [varchar](50) NULL,
	[Gender] [varchar](50) NULL,
	[CCI_Score] [varchar](50) NULL,
	[Outcome_variable] [varchar](50) NULL,
	[Model Label] [varchar](50) NULL,
	[Class probabilities] [varchar](50) NULL,
	[DRERiskStratification] [varchar](50) NULL
)
End

GO
--patient_id,Index date,Age,Gender,CCI Score,Outcome_variable,Model Label,Class probabilities,DRERiskStratification

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[d6_5_d5_dre_sha_score_2_5yr_Train]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].d6_5_d5_dre_sha_score_2_5yr_Train
	BULK INSERT [$(TargetSchema)].d6_5_d5_dre_sha_score_2_5yr_Train
	FROM '$(SampleDataPath)src_sample\d6_5_d5_dre_sha_score_2_5yr_Train.csv'
	WITH 
	(
	FirstRow=2,
	rowterminator = '\n',
	fieldterminator =','
	); 
END -- 282801

--KPI

IF  OBJECT_ID(N'[$(TargetSchema)].[DRE_KPI_Outcome]') is Null
Begin
CREATE TABLE [$(TargetSchema)].DRE_KPI_Outcome
(
	[Metric] [varchar](200) NULL,
	[DRE Training Data High+Medium / Low (n=282,801)] [varchar](200) NULL,
	[DRE SHA Cohort High+Medium / Low (n=353,395*) (*Train+Test)] [varchar](200) NULL
)
End


IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[DRE_KPI_Outcome]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].DRE_KPI_Outcome
	BULK INSERT [$(TargetSchema)].DRE_KPI_Outcome
	FROM '$(SampleDataPath)src_sample\KPI.csv'
	WITH 
	(
	FirstRow=2,
	rowterminator = '\n',
	fieldterminator ='|'
	); 
END -- 

--Select * from dbo.DRE_KPI_Outcome
update [$(TargetSchema)].DRE_KPI_Outcome 
set [DRE Training Data High+Medium / Low (n=282,801)]=replace([DRE Training Data High+Medium / Low (n=282,801)],'�',' '),
	[DRE SHA Cohort High+Medium / Low (n=353,395*) (*Train+Test)]=replace([DRE SHA Cohort High+Medium / Low (n=353,395*) (*Train+Test)],'�',' ')

/*DRE Score File End*/

/*TDS Score File End*/
Print 'Populate TDS_Cohort_characterization_SHA_Train'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_Rpt_TDS_Cohort_characterization_Train_Test_SHA]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].std_Rpt_TDS_Cohort_characterization_Train_Test_SHA

	BULK INSERT [$(TargetSchema)].std_Rpt_TDS_Cohort_characterization_Train_Test_SHA
	FROM '$(SampleDataPath)std_ref\std_Rpt_TDS_Cohort_characterization_Train_Test_SHA.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 

Print 'Populate Std_Rpt_TDS_SHA_patient_list'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_SHA_patient_list]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[Std_Rpt_TDS_SHA_patient_list]

	BULK INSERT [$(TargetSchema)].Std_Rpt_TDS_SHA_patient_list
	FROM '$(SampleDataPath)std_ref\Std_Rpt_TDS_SHA_patient_list.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 

Print 'Populate Std_Rpt_TDS_Train_patient_list'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_Train_patient_list]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[Std_Rpt_TDS_Train_patient_list]

	BULK INSERT [$(TargetSchema)].Std_Rpt_TDS_Train_patient_list
	FROM '$(SampleDataPath)std_ref\Std_Rpt_TDS_Train_patient_list.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 


Print 'Populate Std_Rpt_TDS_Test_patient_list'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[Std_Rpt_TDS_Test_patient_list]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[Std_Rpt_TDS_Test_patient_list]

	BULK INSERT [$(TargetSchema)].Std_Rpt_TDS_Test_patient_list
	FROM '$(SampleDataPath)std_ref\Std_Rpt_TDS_Test_patient_list.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 

Print 'Populate PHPH_SynomaIDs'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(SrcSchema)].[PHPH_SynomaIDs]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(SrcSchema)].[PHPH_SynomaIDs]

	BULK INSERT [$(SrcSchema)].[PHPH_SynomaIDs]
	FROM '$(SampleDataPath)std_ref\std_REF_PHPHSynoma_IDs.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 

Print 'Populate PHPH_SynomaIDs'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_Rpt_TDS_patient_timeline_statistic]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_Rpt_TDS_patient_timeline_statistic]

	BULK INSERT [$(TargetSchema)].[std_Rpt_TDS_patient_timeline_statistic]
	FROM '$(SampleDataPath)std_ref\std_Rpt_TDS_patient_timeline_statistic.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 
/*TDS Score File End*/

DECLARE @Env varchar(50)
SET @Env ='$(Environment)'


IF @Env='UCB' 
BEGIN

	IF  NOT EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_ref_Economic_EndPoints]') AND type in (N'U'))

		BEGIN
			CREATE TABLE [$(TargetSchema)].[std_ref_Economic_EndPoints]
			(
			  [Type] varchar(100) NULL
			, [Government Non_DRE] int NULL
			, [Government DRE] int NULL
			, [Private Non_DRE] int NULL
			, [Private DRE] int NULL
			, [Government Indeterminate] int NULL
			, [Private Indeterminate] int NULL
			)

			TRUNCATE TABLE [$(TargetSchema)].[std_ref_Economic_EndPoints]

			BULK INSERT [$(TargetSchema)].[std_ref_Economic_EndPoints]
			FROM '$(SampleDataPath)std_ref\std_HEOR_High_Low_EndPoints.csv'
			WITH 
			(
			FirstRow = 2,
			rowterminator = '\n',
			fieldterminator =','
			); 

		END
	Print 'Populate std_ref_Economic_EndPoints'
	Print Getdate()
END