

Select * into Truven_Pipeline_Execution.AuditTable From IMS_Pipeline_Execution.AuditTable where 1<>1


Create Schema Truven_Pipeline_Execution_Src



Create View Truven_Pipeline_Execution_Src.src_dx_claims
As Select * from Truven_To_IMS.dx_claims

Create View Truven_Pipeline_Execution_Src.src_hosp_encounter
As Select * from Truven_To_IMS.Hosp_Encounter

Create View Truven_Pipeline_Execution_Src.src_hosp_procedure
As Select * from Truven_To_IMS.hosp_procedure

Select * into Truven_Pipeline_Execution_Src.src_pharmacy_panel_tab From IMS.epilepsy_pharmacy_panel A where 1<>1

Create View Truven_Pipeline_Execution_Src.src_pharmacy_panel
As Select * from Truven_Pipeline_Execution_Src.src_pharmacy_panel_tab

Select * into Truven_Pipeline_Execution_Src.src_ptnt_eligibility From [Truven_To_IMS].[Ptnt_Eligibility where 1<>1


Alter View Truven_Pipeline_Execution_Src.src_REF_patient
As Select Patient_ID,YOB,case	when SEX=1 then 'M' when SEX=2 then 'F' Else 'U' end as SEX ,Group_IND,ZIP3,Region 
from [Truven_To_IMS].[Ref_patient] A--9,810,193


Create View Truven_Pipeline_Execution_Src.src_REF_product
As Select * from [Truven_To_IMS].REF_product

Alter View Truven_Pipeline_Execution_Src.src_REF_Provider
As Select * from [Truven_To_IMS].REF_Provider


Create View Truven_Pipeline_Execution_Src.src_rx_claims
As Select * from [Truven_To_IMS].rx_claims 


------------------------------------------------------------------------------------------------------------------------------
--Create Schema Truven_Pipeline_Execution

Select * into Truven_Pipeline_Execution.std_REF_AED_Abbreviation from SHA_Pipeline_All.std_REF_AED_Abbreviation--24

Select * into Truven_Pipeline_Execution.std_REF_AEDName from SHA_Pipeline_All.std_REF_AEDName --44

Select * into Truven_Pipeline_Execution.std_REF_CCI from SHA_Pipeline_All.std_REF_CCI--11474

Select * into Truven_Pipeline_Execution.std_REF_CPT_Code_All From SHA_Pipeline_All.std_REF_CPT_Code_All--133960

Select * into Truven_Pipeline_Execution.std_REF_ICD_Code_All From SHA_Pipeline_All.std_REF_ICD_Code_All--87480

Select * into Truven_Pipeline_Execution.std_REF_icd9_icd10_cmgem from SHA_Pipeline_All.std_REF_icd9_icd10_cmgem--23912

Select * into Truven_Pipeline_Execution.std_REF_ICD9_To_ICD10_Conversion from SHA_Pipeline_All.std_REF_ICD9_To_ICD10_Conversion--39

Select * into Truven_Pipeline_Execution.std_REF_MedicationCodesUSP from SHA_Pipeline_All.std_REF_MedicationCodesUSP--1137

Select * into Truven_Pipeline_Execution.std_REF_time_dimension from SHA_Pipeline_All.std_REF_time_dimension--120

Select * into Truven_Pipeline_Execution.std_REF_USPS_zip3zip1codes from SHA_Pipeline_All.std_REF_USPS_zip3zip1codes--1000

------------------------------------------------------------



