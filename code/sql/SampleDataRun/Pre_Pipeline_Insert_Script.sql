-- ------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Pre Processing For inserting sample data into base tables 
-- - NOTE      : 
-- -			 	 
--				 
-- - Execution : 
-- - Date      : 23-Aug-2018		
-- - Change History: 
-- - Change Date		: 3-Dec-2018
-- - Change Description	: Added Begin and End clause for each IF statements and commented out the insertion for ptnt_eligibility & time dimension 
 					
-- ---------------------------------------------------------------------- 
	
/* create tables if not exists (vendor supplied) */
--:SetVar SampleDataPath F:\SnehaM\Pipeline\TDS_Pipeline\scripts\SampleDataRun\CSV_for_Tables\
--:Setvar Schema TestPipeline_stg

Print 'Populate sample_src_rx_claims'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_rx_claims]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_rx_claims]
	BULK INSERT [$(TargetSchema)].[sample_src_rx_claims]
	FROM '$(SampleDataPath)src_sample\src_sample_rx_claims.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_hosp_encounter'
Print Getdate()


IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_hosp_encounter]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE [$(TargetSchema)].[sample_src_hosp_encounter]
	BULK INSERT [$(TargetSchema)].[sample_src_hosp_encounter]
	FROM '$(SampleDataPath)src_sample\src_sample_hosp_encounter.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_hosp_procedure'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_hosp_procedure]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE [$(TargetSchema)].[sample_src_hosp_procedure]
	BULK INSERT [$(TargetSchema)].[sample_src_hosp_procedure]
	FROM '$(SampleDataPath)src_sample\src_sample_hosp_procedure.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_dx_claims'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_dx_claims]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_dx_claims]
	BULK INSERT [$(TargetSchema)].[sample_src_dx_claims]
	FROM '$(SampleDataPath)src_sample\src_sample_dx_claims.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_ptnt_eligibility'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ptnt_eligibility]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ptnt_eligibility]
	BULK INSERT [$(TargetSchema)].[sample_src_ptnt_eligibility]
	FROM '$(SampleDataPath)src_sample\src_sample_ptnt_eligibility.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);
END

Print 'Populate sample_src_pharmacy_panel'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_pharmacy_panel]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_pharmacy_panel]
	BULK INSERT [$(TargetSchema)].[sample_src_pharmacy_panel]
	FROM '$(SampleDataPath)src_sample\src_sample_pharmacy_panel.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);
END

Print 'Populate sample_src_ref_patient'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ref_patient]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ref_patient]
	BULK INSERT  [$(TargetSchema)].[sample_src_ref_patient]
	FROM '$(SampleDataPath)src_sample\src_sample_ref_patient.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);  
END

Print 'Populate sample_src_ref_Provider'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ref_Provider]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ref_Provider]
	BULK INSERT [$(TargetSchema)].[sample_src_ref_Provider]
	FROM '$(SampleDataPath)src_sample\src_sample_ref_Provider.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate sample_src_ref_product'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[sample_src_ref_product]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[sample_src_ref_product]
	BULK INSERT [$(TargetSchema)].[sample_src_ref_product]
	FROM '$(SampleDataPath)src_sample\src_sample_ref_product.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

--/* Custom Ref Tables */

Print 'Populate std_REF_icd9_icd10_cmgem'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_icd9_icd10_cmgem]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_icd9_icd10_cmgem]
	BULK INSERT [$(TargetSchema)].[std_REF_icd9_icd10_cmgem]
	FROM '$(SampleDataPath)std_ref\std_REF_icd9_icd10_cmgem.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_MedicationCodesUSP'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_MedicationCodesUSP]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_MedicationCodesUSP]
	BULK INSERT [$(TargetSchema)].[std_REF_MedicationCodesUSP]
	FROM '$(SampleDataPath)std_ref\std_REF_MedicationCodesUSP.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_AEDName'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_AEDName]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_AEDName]
	BULK INSERT [$(TargetSchema)].[std_REF_AEDName]
	FROM '$(SampleDataPath)std_ref\std_REF_AEDName.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_CPT_Code_All'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_CPT_Code_All]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_CPT_Code_All]
	BULK INSERT [$(TargetSchema)].[std_REF_CPT_Code_All]
	FROM '$(SampleDataPath)std_ref\std_REF_CPT_Code_All.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_AED_Abbreviation'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_AED_Abbreviation]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_AED_Abbreviation]
	BULK INSERT [$(TargetSchema)].[std_REF_AED_Abbreviation]
	FROM '$(SampleDataPath)std_ref\std_REF_AED_Abbreviation.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_CCI'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_CCI]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_CCI]
	BULK INSERT [$(TargetSchema)].[std_REF_CCI]
	FROM '$(SampleDataPath)std_ref\std_REF_CCI.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_ICD_Code_All'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_ICD_Code_All]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_ICD_Code_All]
	BULK INSERT [$(TargetSchema)].[std_REF_ICD_Code_All]
	FROM '$(SampleDataPath)std_ref\std_REF_ICD_Code_All.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_ICD9_To_ICD10_Conversion'
Print Getdate()


IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]
	BULK INSERT [$(TargetSchema)].[std_REF_ICD9_To_ICD10_Conversion]
	FROM '$(SampleDataPath)std_ref\std_REF_ICD9_To_ICD10_Conversion.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_USPS_zip3zip1codes'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_USPS_zip3zip1codes]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].[std_REF_USPS_zip3zip1codes]
	BULK INSERT [$(TargetSchema)].[std_REF_USPS_zip3zip1codes]
	FROM '$(SampleDataPath)std_ref\std_REF_USPS_zip3zip1codes.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	); 
END

Print 'Populate std_REF_time_dimension'
Print Getdate()

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[$(TargetSchema)].[std_REF_time_dimension]') AND type in (N'U'))
BEGIN
	TRUNCATE TABLE  [$(TargetSchema)].std_REF_time_dimension

	BULK INSERT [$(TargetSchema)].std_REF_time_dimension
	FROM '$(SampleDataPath)std_ref\std_REF_time_dimension.csv'
	WITH 
	(
	rowterminator = '\n',
	fieldterminator =','
	);

END 