-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : 14 Day table for IMS

-- - NOTE      :
-- - Execution : 
-- - Date      : 13-Jun-2018                             
-- - Change History:        
-- - Change Date                                                      
-- - Change Description                                            
-- ----------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate [app_int_AED_Analysis_Continuous_Prescription]'
Print getdate()


IF Object_Id('[$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription]


Select *,datediff(dd,startdate,enddate)+1 AED_Days 
into [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription]
from [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
where datediff(dd,startdate,enddate)+1 > 29


Create statistics stats_AED_Analysis_Continuous_Prescription_patient_id on [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] (patient_id)
Create statistics stats_AED_Analysis_Continuous_Prescription_startdate  on [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] (startdate)
Create statistics stats_AED_Analysis_Continuous_Prescription_enddate	on [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] (enddate)
Create statistics stats_AED_Analysis_Continuous_Prescription_aed		on [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] (aed)


Print 'Populate [app_int_AED_Analysis_Continuous_Prescription_Step1]'
Print getdate()

IF Object_Id('[$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription_Step1]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription_Step1]

Select *,row_number() over(partition by patient_id order by patient_id,startdate) Num
into [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription_Step1]
from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription]

IF OBJECT_ID('tempdb..#tmp1', 'U') IS NOT NULL
DROP TABLE #tmp1

Select A.*,B.*
into #tmp1
from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription_Step1] A
cross Apply (Select Datediff(dd,B.StartDate,A.startdate) diff ,Cast (Null as date) maxdate, B.startdate as prevdate
			from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription_Step1] B
			where A.patient_id=B.Patient_id 
			and (A.num=B.num+1 or (A.num=B.Num and A.num=1))
			)b 

IF OBJECT_ID('tempdb..#tmp', 'U') IS NOT NULL
DROP TABLE #tmp

Select * into #tmp from #tmp1 
where patient_id in (Select distinct patient_id from #tmp1 where diff<=14 and num<>1 )

update #tmp set prevdate=null where num=1

IF OBJECT_ID('tempdb..#tmptestrange', 'U') IS NOT NULL
DROP TABLE #tmptestrange

Select Case when diff <=14 then 1 else 0 end groups,* 
into #tmptestrange
from #tmp 

Print 'Populate #finaltbl'
Print getdate()

IF OBJECT_ID('tempdb..#finaltbl', 'U') IS NOT NULL
DROP TABLE #finaltbl

Select A.patient_id,A.startdate,A.enddate,A.aed,a.aed_days,A.Num,A.diff,a.prevdate,sum(B.groups) loops
into #finaltbl
from #tmptestrange A , #tmptestrange B
where  a.patient_id=b.patient_id and b.num<=a.num 
and b.num>=isnull((select max(num) from #tmptestrange C where a.patient_id=c.patient_id and  C.num<=A.num and c.groups=0),0)
group by A.patient_id,A.startdate,A.enddate,A.aed,a.aed_days,A.Num,A.diff,a.prevdate

Print 'Populate #maxdate'
Print getdate()

IF OBJECT_ID('tempdb..#maxdate', 'U') IS NOT NULL
DROP TABLE #maxdate

Select a.*,Case when caldate1 is null then a.startdate else caldate1 end as caldate1  
into #maxdate
from #finaltbl A
outer apply (Select case when datediff(dd,prevdate,startdate)<=14 then startdate else prevdate end  caldate1
			from #finaltbl B
			where A.num=B.num-1 and a.patient_id=B.patient_id)B

Print 'Populate #mincases'
Print getdate()

IF OBJECT_ID('tempdb..#mincases', 'U') IS NOT NULL
DROP TABLE #mincases

Select A.* 
into #mincases 
from #maxdate A
inner join (Select b.patient_id, min(num)-1 as num
from #maxdate b where b.loops=0   group by patient_id )B 
on A.patient_id=B.patient_id
and a.num=b.num

Print 'Populate #maxcases'
Print getdate()

IF OBJECT_ID('tempdb..#maxcases', 'U') IS NOT NULL
DROP TABLE #maxcases

Select A.*,A.num-loops uptomax 
into #maxcases 
from #maxdate A
inner join (Select patient_id, max(num) as num
from #maxdate  where loops<>0 group by patient_id )B 
on A.patient_id=B.patient_id
and a.num=b.num

Print 'Populate #intermediate'
Print getdate()


IF OBJECT_ID('tempdb..#intermediate', 'U') IS NOT NULL
DROP TABLE #intermediate

Select * 
into #intermediate 
from
(
Select * from #maxdate A
where Exists (Select 1 from #mincases b where a.num>b.num and a.patient_id=b.patient_id)
intersect
Select * from #maxdate A
where Exists (Select 1 from #maxcases b where not (a.num between uptomax and b.num) and a.patient_id=b.patient_id)
)A where loops<>0

Print 'Populate #final'
Print getdate()

IF OBJECT_ID('tempdb..#final', 'U') IS NOT NULL
DROP TABLE #final

Select * 
into #final 
From (
Select * from #maxdate A 
cross apply (Select caldate1 as maxdate from #mincases B where A.num<=b.num and a.patient_id=b.patient_id)B

union
Select * from #maxdate A 
cross apply (Select caldate1 as maxdate from #maxcases B where A.num between b.uptomax and b.num and a.patient_id=b.patient_id)B

union
Select A.patient_id,A.startdate,A.enddate,A.aed,a.aed_days,A.Num,A.diff,a.prevdate,a.loops,a.caldate1,max(maxdate) as maxdate 
from #maxdate A 
cross apply (Select caldate1 as maxdate from #intermediate B where 
(A.num between b.num-b.loops and b.num ) and a.patient_id=b.patient_id )B
group by A.patient_id,A.startdate,A.enddate,A.aed,a.aed_days,A.Num,A.diff,a.prevdate,a.loops,a.caldate1
)A

IF Object_Id('[$(TargetSchema)].[Test_Full]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[Test_Full]

Select * into [$(TargetSchema)].Test_Full from #maxdate 

IF Object_Id('[$(TargetSchema)].[TestSample]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[TestSample]

Select * into [$(TargetSchema)].TestSample from #final

Alter table [$(TargetSchema)].Test_Full Add maxdate date
Update [$(TargetSchema)].Test_Full
Set [$(TargetSchema)].Test_Full.maxdate=B.maxdate
From [$(TargetSchema)].TestSample B
where [$(TargetSchema)].Test_Full.Patient_id=B.Patient_id
and [$(TargetSchema)].Test_full.num=B.num

update [$(TargetSchema)].Test_Full set maxdate=startdate where maxdate is null

Print 'Populate app_int_GE_14_Days_Rule'
Print getdate()

IF Object_Id('[$(TargetSchema)].[app_int_GE_14_Days_Rule]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_int_GE_14_Days_Rule]

Select patient_id,startdate,enddate,aed,aed_days,Num,diff,prevdate,loops,caldate1,maxdate 
into [$(TargetSchema)].[app_int_GE_14_Days_Rule]
From [$(TargetSchema)].Test_Full
union all
Select patient_id,startdate,enddate,aed,aed_days,Num,diff,prevdate,Null loops,Null caldate1,startdate maxdate
From #tmp1 A
where not exists(Select 1 From [$(TargetSchema)].Test_Full B where A.patient_id=B.Patient_id)

Delete From [$(TargetSchema)].[app_int_GE_14_Days_Rule] where datediff(dd,maxdate,enddate)<=0


drop table [$(TargetSchema)].Test_Full
drop table [$(TargetSchema)].TestSample
drop table [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription_Step1]