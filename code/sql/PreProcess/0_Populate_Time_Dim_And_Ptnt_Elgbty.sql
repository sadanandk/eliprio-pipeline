-- ---------------------------------------------------------------------
-- - AUTHOR    : Vikas S
-- - USED BY   : 
-- - PURPOSE   : Refresh/Populate Time_Dimension & Patient Eligibility
-- - NOTE      :
-- - Execution : 
-- - Date      : 11-Dec-2018                             
-- - Change History		:   Added Hospital Encounter table while populating Time Dimension and Patient Eligibility     
-- - Change Date        :	10th Jan 2019
-- - Change Description :   ELREFPRED-804
-- ----------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate Time Dimension'
Print Getdate()

Select * into #tmp
From 
(
Select distinct Year(service_date)Year,datepart(mm,service_date) month,'Q'+convert(varchar,datepart(qq,service_date)) quarter,convert(varchar(6),Service_date,112) YYYYMM
From [$(SrcSchema)].Src_RX_Claims where days_supply>0
union
Select distinct Year(service_date)Year,datepart(mm,service_date) month,'Q'+convert(varchar,datepart(qq,service_date)) quarter,convert(varchar(6),Service_date,112) YYYYMM
From [$(SrcSchema)].Src_DX_Claims
union
Select distinct Year(FROM_DT)Year,datepart(mm,FROM_DT) month,'Q'+convert(varchar,datepart(qq,FROM_DT)) quarter,convert(varchar(6),FROM_DT,112) YYYYMM
From [$(SrcSchema)].SRC_HOSP_ENCOUNTER
)A

Truncate Table [$(TargetSchema)].std_REF_time_dimension 

Insert into [$(TargetSchema)].std_REF_time_dimension
Select dense_rank() over(order by Year,quarter),Row_number() over(order by Year,month),* 
From #tmp 


------------------------------------------------------------------------------------
Print 'Populate Patient Eligibility'
Print Getdate()

Select distinct convert(varchar(6),Service_date,112)Month_ID,Patient_ID,'Y' as RX 
into #tmpRX
from [$(SrcSchema)].Src_RX_Claims 
where Days_Supply >0

Select distinct convert(varchar(6),Service_date,112)Month_ID,Patient_ID, 'Y' as DX 
into #tmpDX
from [$(SrcSchema)].Src_DX_Claims 

Select distinct convert(varchar(6),FROM_DT,112)Month_ID,Patient_ID, 'Y' as HX 
into #tmpHX
from [$(SrcSchema)].SRC_HOSP_ENCOUNTER -- 

Truncate table [$(SrcSchema)].src_ptnt_eligibility

Insert into [$(SrcSchema)].src_ptnt_eligibility (Month_ID,Patient_ID)
Select distinct Month_ID,Patient_ID From #tmpRX
union 
Select distinct Month_ID,Patient_ID From #tmpDX 
union 
Select distinct Month_ID,Patient_ID From #tmpHX 

Update [$(SrcSchema)].src_ptnt_eligibility Set [RX_ACTIVITY]='N',[DX_ACTIVITY]='N',[HOSP_ACTIVITY]='N'

Update [$(SrcSchema)].src_ptnt_eligibility
Set [$(SrcSchema)].src_ptnt_eligibility.[RX_ACTIVITY]='Y'
From  #tmpRX B
Where [$(SrcSchema)].src_ptnt_eligibility.Month_ID=B.Month_ID
And [$(SrcSchema)].src_ptnt_eligibility.Patient_ID=B.Patient_ID -- 


Update [$(SrcSchema)].src_ptnt_eligibility
Set [$(SrcSchema)].src_ptnt_eligibility.[DX_ACTIVITY]='Y'
From  #tmpDX B
Where [$(SrcSchema)].src_ptnt_eligibility.Month_ID=B.Month_ID
And [$(SrcSchema)].src_ptnt_eligibility.Patient_ID=B.Patient_ID --

Update [$(SrcSchema)].src_ptnt_eligibility
Set [$(SrcSchema)].src_ptnt_eligibility.[HOSP_ACTIVITY]='Y'
From  #tmpHX B
Where [$(SrcSchema)].src_ptnt_eligibility.Month_ID=B.Month_ID
And [$(SrcSchema)].src_ptnt_eligibility.Patient_ID=B.Patient_ID --