-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Stored procedure to fill gap days in the base table for Continuous Presctiption       
-- - NOTE      :
-- - Execution : 
-- - Date      : 31-May-2018                             
-- - Change History:        
-- - Change Date                                                      
-- - Change Description                                            
-- ----------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Generate Temp table to hold the gap days'
Print getdate()

select * into #cte_gap from 
( 
select ROW_NUMBER() OVER(partition by patient_id,aed ORDER BY Patient_id,aed,service_date,days_supply) AS Num,
patient_id,service_date,drug_end_date,aed,Days_Supply
from [$(TargetSchema)].[app_int_GE_Base]
) A

select * into #GapDays from 
(
Select distinct A.Num, A.patient_id,A.service_date,A.drug_end_date,A.aed,A.Days_Supply
,A.service_date as next_service_date,B.drug_end_date as gap_drug_end_date, DateDiff(DAY,A.drug_end_date,B.service_date)-1   Gap_Days
from #CTE_gap A 
inner join #CTE_gap B on A.Num=B.Num-1  and A.patient_id = B.patient_id and A.aed = B.aed

union

Select distinct A.Num, A.patient_id,A.service_date,A.drug_end_date,A.aed,A.Days_Supply
,A.service_date as next_service_date,A.drug_end_date as gap_drug_end_date, NULL
from #CTE_gap A 
inner join (select max(num) Num,patient_id,aed from #CTE_gap group by patient_id,aed) B on A.Num=B.Num  and A.patient_id = B.patient_id and A.aed = B.aed
)A
--order by A.patient_id,A.aed,A.service_date

Print 'Update Gap Days in app_int_GE_Base '
Print getdate()

update [$(TargetSchema)].[app_int_GE_Base]
set Gap_days =  G.Gap_Days
from #GapDays G
where	  [$(TargetSchema)].[app_int_GE_Base].patient_id = G.patient_id
	  and [$(TargetSchema)].[app_int_GE_Base].service_date = G.service_date
	  and [$(TargetSchema)].[app_int_GE_Base].aed = G.aed
	  and [$(TargetSchema)].[app_int_GE_Base].aed is not null
	  and [$(TargetSchema)].[app_int_GE_Base].days_supply = G.days_supply


