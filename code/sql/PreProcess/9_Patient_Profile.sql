-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Patient Profile Table

-- - NOTE      :
-- - Execution : 
-- - Date      : 13-Jun-2018                             
-- - Change History:        
-- - Change		Date					Modify by			  Change Description   
-- -			8-Jan-2019              Suresh G              ELCDS-790 : Iter4 : Remove columns AgeAbove16,Q_Rx_Elg ,Genpop  and move it to cohort table.
-- -			11-Jan-2019				Suresh G			  ELCDS-954 : Iter4 : Added Lastencounter column 
-- -			15-Mar-2019				Suresh G			  ELCDS-1173 : Replaces code from R569 -> R56* ,Added Index in PatientProfile table, modify logic for EXP_ESLICARBAZEPINE ACETATE
-- ----------------------------------------------------------------------

--:setvar Schema Patient_Target
--:Setvar SrcSchema Patient

Print 'Populate app_int_Patient_Profile'
Print getdate()

IF Object_Id('[$(TargetSchema)].[app_int_Patient_Profile]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_int_Patient_Profile]


select  distinct
 patient.patient_id
,patient.YOB
,patient.Sex as Gender
into [$(TargetSchema)].[app_int_Patient_Profile]
from [$(SrcSchema)].[Src_REF_patient] patient

-- First AED and First AED Date
alter table [$(TargetSchema)].[app_int_Patient_Profile]
add First_AED varchar(100),First_AED_Date date

Print 'Populate #FirstAEDDate'
Print getdate()

IF OBJECT_ID('tempdb..#FirstAEDDate', 'U') IS NOT NULL
DROP TABLE #FirstAEDDate

select patient.patient_id,min(convert(date,rx.service_date)) First_AED_Date
into #FirstAEDDate
from [$(SrcSchema)].[Src_REF_patient] patient with (nolock) 
inner join [$(SrcSchema)].[Src_rx_claims] rx with (nolock)  on patient.patient_id = rx.patient_id
inner join [$(SrcSchema)].[Src_ref_product] product with (nolock)  on rx.ndc = product.ndc
inner join [$(TargetSchema)].[std_ref_AEDName] aed on product.generic_name = aed.generic_name
where aed.aed is not null and days_supply>0
group by patient.patient_id

create statistics FirstAEDDate on #FirstAEDDate (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set First_AED_Date = FAD.First_AED_Date
from #FirstAEDDate FAD
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = FAD.patient_id


Print 'Populate #First_AED_Regimen'
Print getdate()

IF OBJECT_ID('tempdb..#First_AED_Regimen') IS NOT NULL
DROP TABLE #First_AED_Regimen

SELECT ROW_NUMBER() OVER (PARTITION BY Patient_id Order by Abbreviation ) AS id,patient_id,First_AED_Date,Abbreviation 
INTO #First_AED_Regimen
FROM (
SELECT DISTINCT patient.patient_id, R.First_AED_Date, aed.Abbreviation
FROM [$(SrcSchema)].[src_ref_patient] patient WITH (NOLOCK) 
INNER JOIN [$(SrcSchema)].[src_rx_claims] rx WITH (NOLOCK)  ON patient.patient_id = rx.patient_id
INNER JOIN [$(SrcSchema)].[src_ref_product] product WITH (NOLOCK)  ON rx.ndc = product.ndc
INNER JOIN [$(TargetSchema)].[std_ref_AEDName] aed ON product.generic_name = aed.generic_name
INNER JOIN #FirstAEDDate R ON R.patient_id = patient.patient_id and R.First_AED_Date = CONVERT(DATE,rx.service_date)
where days_supply>0
)A

Print 'Populate #FirstAED'
Print getdate()


IF OBJECT_ID('tempdb..#FirstAED') IS NOT NULL
DROP TABLE #FirstAED

SELECT DISTINCT Patient_id,First_AED_Date,AED INTO  #FirstAED FROM (
				SELECT Patient_id,First_AED_Date, ISNULL([1],'') +ISNULL('__'+[2],'')+ISNULL('__'+[3],'')+ISNULL('__'+[4],'')+ISNULL('__'+[5],'')+ISNULL('__'+[6],'')+ISNULL('__'+[7],'')+ISNULL('__'+[8],'')+ISNULL('__'+[9],'')+ISNULL('__'+[10],'')+ISNULL('__'+[11],'')+ISNULL('__'+[12],'')+ISNULL('__'+[13],'')+ISNULL('__'+[14],'')+ISNULL('__'+[15],'')+ISNULL('__'+[16],'')+ISNULL('__'+[17],'')+ISNULL('__'+[18],'')+ISNULL('__'+[19],'')+ISNULL('__'+[20],'') AS AED
	
					FROM
					(SELECT ID ,Patient_id,First_AED_Date,Abbreviation AS AED1 FROM #First_AED_Regimen  
					) AS SourceTable
					PIVOT
					(
					max(AED1)
					FOR ID IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],[11],[12],[13],[14],[15],[16],[17],[18],[19],[20])
					) AS PivotTable
				) A
					WHERE AED <> ''

create statistics FirstAED
on #FirstAED (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set First_AED = FAD.aed
from #FirstAED FAD
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = FAD.patient_id

-- RX date updates 9 min
alter table [$(TargetSchema)].[app_int_Patient_Profile]
add First_RX_Encounter date,Last_RX_Encounter date

Print 'Populate #Rx'
Print getdate()

IF OBJECT_ID('tempdb..#rx', 'U') IS NOT NULL
DROP TABLE #rx

select rx.patient_id,min(convert(date,rx.service_date)) First_RX_Encounter,
max(convert(date,rx.service_date)) Last_RX_Encounter
into #RX
from [$(SrcSchema)].[Src_rx_claims] rx 
where days_supply>0
group by rx.patient_id

create statistics RX
on #RX (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set First_RX_Encounter = rx.First_RX_Encounter
   ,Last_RX_Encounter =  rx.Last_RX_Encounter
from #RX rx 
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = rx.patient_id

-- DX date updates 11 min
alter table [$(TargetSchema)].[app_int_Patient_Profile]
add  First_DX_Encounter date,Last_DX_Encounter date


Print 'Populate #dx_en'
Print getdate()

IF OBJECT_ID('tempdb..#dx_en', 'U') IS NOT NULL
DROP TABLE #dx_en

select dx.patient_id,min(convert(date,dx.service_date)) First_DX_Encounter,
max(convert(date,dx.service_date)) Last_DX_Encounter
into #DX_en
from [$(SrcSchema)].[src_dx_claims] dx 
group by dx.patient_id

create statistics DX
on #DX_en (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set First_DX_Encounter = dx.First_DX_Encounter
   ,Last_DX_Encounter =  dx.Last_DX_Encounter
from #DX_en dx 
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = dx.patient_id

-- Hosptial Encounter date updates 3 min
alter table [$(TargetSchema)].[app_int_Patient_Profile]
add  First_Hosp_Encounter date,Last_Hosp_Encounter date

Print 'Populate #hosp'
Print getdate()

IF OBJECT_ID('tempdb..#hosp', 'U') IS NOT NULL
DROP TABLE #hosp

select hosp.patient_id,min(convert(date,hosp.FROM_DT)) First_Hosp_Encounter,
max(convert(date,hosp.FROM_DT)) Last_Hosp_Encounter
into #hosp
from [$(SrcSchema)].[src_hosp_encounter] hosp
group by hosp.patient_id

create statistics Hosp
on #hosp (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set First_Hosp_Encounter = hosp.First_Hosp_Encounter
   ,Last_Hosp_Encounter =  hosp.Last_Hosp_Encounter
from #hosp hosp
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = hosp.patient_id

-- IS_345.* update 3 min
alter table [$(TargetSchema)].[app_int_Patient_Profile]
add  [IS_345.x] date,[First_780.3x] date,[Second_780.3x] date,[Last_780.3x] date--,NewlyDiagnosed int

Print 'Populate #Dx'
Print getdate()

IF OBJECT_ID('tempdb..#Dx', 'U') IS NOT NULL
DROP TABLE #Dx

select  patient_id,min(convert(date,dx.service_date)) date
into #DX
from [$(SrcSchema)].[src_dx_claims] dx
where
replace(dx.DIAG1,'.','') like '345%' or replace(dx.DIAG2,'.','') like '345%' or replace(dx.DIAG3,'.','') like '345%' or replace(dx.DIAG4,'.','') like '345%' or
replace(dx.DIAG5,'.','') like '345%' or replace(dx.DIAG6,'.','') like '345%' or replace(dx.DIAG7,'.','') like '345%' or replace(dx.DIAG8,'.','') like '345%' or
replace(dx.DIAG1,'.','') like 'G40%' or replace(dx.DIAG2,'.','') like 'G40%' or replace(dx.DIAG3,'.','') like 'G40%' or replace(dx.DIAG4,'.','') like 'G40%' or
replace(dx.DIAG5,'.','') like 'G40%' or replace(dx.DIAG6,'.','') like 'G40%' or replace(dx.DIAG7,'.','') like 'G40%' or replace(dx.DIAG8,'.','') like 'G40%'  
group by patient_id

create statistics DXPAtient
on #DX (patient_Id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set [IS_345.x] = dx.date
from #dx dx
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = dx.patient_id

-- First_7803* Second_7803* 

Print 'Populate #con'
Print getdate()

IF OBJECT_ID('tempdb..#con', 'U') IS NOT NULL
DROP TABLE #con

						 
SELECT DISTINCT patient_id,service_date
INTO #con
FROM (
		SELECT DISTINCT  patient_id,convert(date,service_date) service_date,dx_codes
	
		FROM
			(
				SELECT [PATIENT_ID], SERVICE_DATE ,[DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8]
    			FROM   [$(SrcSchema)].[src_dx_claims] dx with(nolock)
				where (  replace(dx.DIAG1,'.','') like '7803%' or replace(dx.DIAG2,'.','') like '7803%' or replace(dx.DIAG3,'.','') like '7803%' or replace(dx.DIAG4,'.','') like '7803%' or
						replace(dx.DIAG5,'.','') like '7803%' or replace(dx.DIAG6,'.','') like '7803%' or replace(dx.DIAG7,'.','') like '7803%' or replace(dx.DIAG8,'.','') like '7803%' or
						replace(dx.DIAG1,'.','') like 'R56%'  or replace(dx.DIAG2,'.','') like 'R56%'  or replace(dx.DIAG3,'.','') like 'R56%'	or replace(dx.DIAG4,'.','') like 'R56%' or
						replace(dx.DIAG5,'.','') like 'R56%'  or replace(dx.DIAG6,'.','') like 'R56%'  or replace(dx.DIAG7,'.','') like 'R56%'	or replace(dx.DIAG8,'.','') like 'R56%')
						
			)strw
			UNPIVOT
			(
				Dx_Codes FOR Codes_Column IN ([DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8])
			) AS A
		WHERE replace(Dx_Codes,'.','') LIKE '7803%' OR replace(Dx_Codes,'.','') LIKE 'R56%'
	)A
WHERE replace(Dx_Codes,'.','') NOT IN ('78031','78032') AND   replace(Dx_Codes,'.','') NOT LIKE 'R560%'			


IF OBJECT_ID('tempdb..#DX1', 'U') IS NOT NULL
DROP TABLE #DX1

select   patient_id,service_date,
Row_number() over(partition by patient_id order by patient_id,service_date)Num
into #DX1
from #con

create statistics DXPAtient
on #DX1 (patient_Id)


update [$(TargetSchema)].[app_int_Patient_Profile]
set [First_780.3x] = dx.service_date
from #dx1 dx
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = dx.patient_id
and dx.Num=1

update [$(TargetSchema)].[app_int_Patient_Profile]
set [Second_780.3x] = dx.service_date
from #dx1 dx
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = dx.patient_id
and dx.Num=2

Print 'Populate #Max'
Print getdate()

IF OBJECT_ID('tempdb..#Max', 'U') IS NOT NULL
DROP TABLE #Max

select distinct patient_id,Max(Num)Num
into #Max
from #DX1
group by patient_id

IF OBJECT_ID('tempdb..#Last', 'U') IS NOT NULL
DROP TABLE #Last

select D.patient_id,D.service_date,M.Num
into #Last
from #Max M
left join #DX1 D on M.patient_id=D.patient_id and M.Num=D.Num

update [$(TargetSchema)].[app_int_Patient_Profile]
set [Last_780.3x] = dx.service_date
from #Last dx
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = dx.patient_id

-- Is_780.3x

Print 'Alter [app_int_Patient_Profile]'
Print getdate()

alter table [$(TargetSchema)].[app_int_Patient_Profile]
add [Is_780.3x] date

IF OBJECT_ID('tempdb..#Temp', 'U') IS NOT NULL
DROP TABLE #Temp

select distinct patient_id, [Second_780.3x]
into #Temp
from [$(TargetSchema)].[app_int_Patient_Profile]
where [Second_780.3x] is not null and  ([First_780.3x] <> [Second_780.3x] or [First_780.3x] <> [Last_780.3x])

update [$(TargetSchema)].[app_int_Patient_Profile]
set [Is_780.3x] = T.[Second_780.3x]
from #temp T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id =T.patient_id

-- Newly Diagnosed update 1 min -- (not as per PSPA)

/*drop table #Temp

select  patient_id,First_DX_Encounter,[IS_345.x],[Is_780.3x],NewlyDiagnosed 
,FirstEncounter = case when First_Encounter<=First_Hosp_encounter  then First_Encounter
			 when First_Encounter>=First_Hosp_encounter  then First_Hosp_encounter
			 when First_Encounter is null then First_Hosp_encounter
			 when First_Hosp_encounter is null then First_Encounter end
,FirstDXEncounter = case when [IS_345.x]<=[Is_780.3x]  then [IS_345.x]
			 when [IS_345.x]>=[Is_780.3x]  then [Is_780.3x]
			 when [Is_780.3x] is null then [IS_345.x]
			 when [IS_345.x] is null then [IS_780.3x]
		end
into #temp
from [$(TargetSchema)].[app_int_Patient_Profile]
where [IS_345.x] is not null or [Second_780.3x] is not null

select  patient_id--,datediff(DAY,First_DX_Encounter,MinEP),MinEP,First_DX_Encounter
into #temp2
from #temp
where datediff(DAY,FirstEncounter,FirstDXEncounter)>=730

update [$(TargetSchema)].[app_int_Patient_Profile]
set NewlyDiagnosed = 1
from #Temp2 T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id = T.patient_id
*/

-- AED Date Update 8 min
Print 'alter [app_int_Patient_Profile]'
Print getdate()

alter table [$(TargetSchema)].[app_int_Patient_Profile]
add  Exp_FELBAMATE date,Exp_TOPIRAMATE date,Exp_PRIMIDONE date,Exp_LACOSAMIDE date,Exp_PREGABALIN date,Exp_EZOGABINE date,
Exp_VALPROATESODIUM date,Exp_CLOBAZAM date,Exp_LEVETIRACETAM date,Exp_LAMOTRIGINE date,Exp_ETHOTOIN date,Exp_PHENOBARBITAL date,
Exp_PHENYTOIN date, Exp_ZONISAMIDE date,Exp_ETHOSUXIMIDE date,Exp_RUFINAMIDE date,Exp_CARBAMAZEPINE date,Exp_VIGABATRIN date,
Exp_OXCARBAZEPINE date, Exp_BRIVARACETAM date, Exp_PERAMPANEL date, Exp_ESLICARBAZEPINE_ACETATE date, Exp_RETIGABINE date
,PharmacyStability int

IF OBJECT_ID('tempdb..#temp_aed', 'U') IS NOT NULL
DROP TABLE #temp_aed

select rx.patient_id,min(convert(date,service_date)) service_date,aed.aed
into #temp_aed
from [$(SrcSchema)].[Src_rx_claims] rx
inner join [$(SrcSchema)].[Src_ref_product] product on rx.ndc = product.ndc
inner join [$(TargetSchema)].[std_ref_AEDName] aed on product.generic_name = aed.generic_name
where rx.days_supply>0
group by rx.patient_id, aed.aed

create statistics Temp
on #temp_aed (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_FELBAMATE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'FELBAMATE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_TOPIRAMATE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'TOPIRAMATE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_PRIMIDONE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'PRIMIDONE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_LACOSAMIDE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'LACOSAMIDE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_PREGABALIN = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'PREGABALIN'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_EZOGABINE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'EZOGABINE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_VALPROATESODIUM = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'VALPROATE SODIUM'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_CLOBAZAM = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'CLOBAZAM'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_LEVETIRACETAM = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'LEVETIRACETAM'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_LAMOTRIGINE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'LAMOTRIGINE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_ETHOTOIN = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'ETHOTOIN'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_PHENOBARBITAL = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'PHENOBARBITAL'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_PHENYTOIN = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'PHENYTOIN'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_ZONISAMIDE= T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'ZONISAMIDE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_ETHOSUXIMIDE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'ETHOSUXIMIDE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_RUFINAMIDE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'RUFINAMIDE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_CARBAMAZEPINE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'CARBAMAZEPINE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_VIGABATRIN = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'VIGABATRIN'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_OXCARBAZEPINE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'OXCARBAZEPINE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_BRIVARACETAM = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'BRIVARACETAM'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_PERAMPANEL = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'PERAMPANEL'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_ESLICARBAZEPINE_ACETATE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'ESLICARBAZEPINE ACETATE'

update [$(TargetSchema)].[app_int_Patient_Profile]
set  Exp_RETIGABINE = T.service_date
from #temp_aed T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id
and T.aed = 'RETIGABINE'


-- Pharmacy Stability
-- Patients for a given time period the mentioned pharmacy was accessed

Print 'Populate [pharmacy1]'
Print getdate()

IF Object_Id('[$(TargetSchema)].[pharmacy1]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[pharmacy1]

select  patient_id,pharmacy_id
,concat(datepart(Year,min(convert(date,service_date))),SUBSTRING(CONVERT(nvarchar(6),min(convert(date,service_date)), 112),5,2)) SMonth_Id
,concat(datepart(Year,max(convert(date,service_date))),SUBSTRING(CONVERT(nvarchar(6),max(convert(date,service_date)), 112),5,2)) EMonth_Id
into [$(TargetSchema)].pharmacy1
from [$(SrcSchema)].[Src_rx_claims] rx
where days_Supply>0
group by patient_id,pharmacy_id

create statistics patientpharma
on [$(TargetSchema)].pharmacy1 (patient_id,Smonth_id,EMonth_id)

-- Compute total months in time period

-- Process patients in batches

IF OBJECT_ID('tempdb..#DistinctRXpatients', 'U') IS NOT NULL
DROP TABLE #DistinctRXpatients

select distinct patient_id into #DistinctRXpatients from [$(SrcSchema)].[Src_rx_claims]

IF Object_Id('[$(TargetSchema)].[PatientProcessStatus]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[PatientProcessStatus]

Select ntile(4000) over(order by PATIENT_ID) as Batch, PATIENT_ID,'N' as isProcess
into [$(TargetSchema)].PatientProcessStatus
From #DistinctRXpatients 

IF Object_Id('[$(TargetSchema)].[pharmacy]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[pharmacy]

create table [$(TargetSchema)].pharmacy
(
patient_id varchar(100)
, pharmacy_id nvarchar(100)
, month_id int
, stable_flag int
)

declare @MinP int = (select min(batch) from [$(TargetSchema)].PatientProcessStatus where ISProcess = 'N')
declare @MaxP int = (select max(batch) from [$(TargetSchema)].PatientProcessStatus where ISProcess = 'N')

IF Exists(Select top 1 'X' From [$(TargetSchema)].pharmacy1)
Begin
		while (@MinP<=@MaxP)
		begin
		insert into  [$(TargetSchema)].pharmacy

		-- Pharmacy Stability for the given time period of the patient

		select distinct T.patient_id,T.pharmacy_id,PP.month_id,PP.stable_flag
		from [$(TargetSchema)].pharmacy1 T
		inner join [$(SrcSchema)].[src_pharmacy_panel] PP with (nolock) 
		on (PP.Month_id >= T.SMonth_Id and  PP.Month_id<=T.EMonth_id) and T.pharmacy_id = PP.pharmacy_id
		and exists (select 1 from [$(TargetSchema)].PatientProcessStatus P where T.patient_id = P.patient_id and P.batch = @MinP)

		update [$(TargetSchema)].PatientProcessStatus 
		set IsProcess = 'Y'
		where Batch = @MinP

		set @MinP = @MinP+1

		end
End

create statistics patientpharma
on [$(TargetSchema)].pharmacy (patient_id)

-- For whole time period

-- Calculating total Months for Patients
Print 'Populate [PatientTimePeriod]'
Print getdate()

IF Object_Id('[$(TargetSchema)].[PatientTimePeriod]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[PatientTimePeriod]

select  patient_id,(datediff(Month,min(convert(date,service_Date)),max(convert(date,service_Date)))+1) TotalMonths
into [$(TargetSchema)].[PatientTimePeriod]
from [$(SrcSchema)].[Src_rx_claims]
where days_supply>0
group by patient_id

-- Computing Pharmacy Stability for Entire Patient Duration
Print 'Populate [pharmacy4]'
Print getdate()

IF Object_Id('[$(TargetSchema)].[pharmacy4]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[pharmacy4]

select  P2.patient_id,P1.pharmacy_id , sum(case when stable_flag =1 then 1 else 0 end)*100.0/TotalMonths Stability
into [$(TargetSchema)].pharmacy4
from [$(TargetSchema)].pharmacy P2 with (nolock)
inner join [$(TargetSchema)].pharmacy1 P1 with (nolock) on P1.patient_id = P2.patient_id and P1.pharmacy_id = P2.pharmacy_id
inner join [$(TargetSchema)].PatientTimePeriod PTP on PTP.patient_id = P2.patient_id
group by P2.patient_id,P1.pharmacy_id,TotalMonths
having (sum(case when stable_flag =1 then 1 else 0 end)*100.0/(TotalMonths) )>=80  -- patients with atleast one pharmacy with >=80% stability for the entire patient rx data

-- Set the new values
update [$(TargetSchema)].[app_int_Patient_Profile]
set PharmacyStability=1
from [$(TargetSchema)].pharmacy4 P
where [$(TargetSchema)].app_int_Patient_Profile.patient_id = P.patient_id

/*
-- Age Above 16

IF OBJECT_ID('tempdb..#Age', 'U') IS NOT NULL
DROP TABLE #Age

select distinct patient_id,age = case when (2005-YOB)>16 then 2005-YOB end
into #Age
from [$(TargetSchema)].[app_int_Patient_Profile] with (nolock)

create statistics Age
on #Age (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set [AgeAbove16] = A.Age
from #Age A
where  A.patient_id = [$(TargetSchema)].[app_int_Patient_Profile].Patient_id
*/

-- IS Epilepsy
alter table [$(TargetSchema)].[app_int_Patient_Profile]
add [IsEpilepsy] int

update [$(TargetSchema)].[app_int_Patient_Profile]
set IsEpilepsy = 1
where [IS_345.x] is not null or [IS_780.3x] is not null


-- continuous eligibility

alter table [$(TargetSchema)].[app_int_Patient_Profile]
add [ContinuousEligibility] int

Print 'Populate [eligibility1]'
Print getdate()

IF Object_Id('[$(TargetSchema)].[eligibility1]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[eligibility1]

select  patient_id,RX_Activity,DX_Activity,Hosp_Activity 
,left (Month_Id,4) [Year] 
into [$(TargetSchema)].eligibility1 
from [$(SrcSchema)].[src_ptnt_eligibility]



create statistics eligibilitypatient
on [$(TargetSchema)].eligibility1 (patient_id)


IF Object_Id('[$(TargetSchema)].[eligibility2]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[eligibility2]


select patient_id, [Year] 
, RX_Eligibility = case when (sum(case when RX_Activity ='Y' then 1 else 0 end) * 100 /12 )>=80 then 1 else 0 end
, DX_Eligibility = case when (sum(case when DX_Activity ='Y' then 1 else 0 end) * 100 /12 )>=80 then 1 else 0 end
, Hosp_Eligibility = case when (sum(case when Hosp_Activity ='Y' then 1 else 0 end) * 100 /12)>=80 then 1 else 0 end
, Row_number () over(partition by patient_id order by patient_id,[Year]) Num 
into [$(TargetSchema)].eligibility2
from [$(TargetSchema)].eligibility1 
group by patient_id,[Year]


create statistics eligibilitypatient
on [$(TargetSchema)].eligibility2 (patient_id,[Year])

IF Object_Id('[$(TargetSchema)].[eligibility3]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[eligibility3]


select distinct A.patient_id
into [$(TargetSchema)].eligibility3
from [$(TargetSchema)].eligibility2 A 
inner join [$(TargetSchema)].eligibility2 B on A.Num=B.Num-1 and A.patient_id = B.patient_id and A.[Year]=B.[Year]-1 
where (A.RX_Eligibility = 1 and B.RX_Eligibility =1 ) or 
(A.DX_Eligibility = 1 and B.DX_Eligibility =1 ) or 
(A.Hosp_Eligibility = 1 and B.Hosp_Eligibility =1 )


create statistics eligibilitypatient
on [$(TargetSchema)].eligibility3 (patient_id)

update [$(TargetSchema)].[app_int_Patient_Profile]
set [ContinuousEligibility] = 1
from [$(TargetSchema)].eligibility3 T
where T.patient_id = [$(TargetSchema)].[app_int_Patient_Profile].patient_id



alter table [$(TargetSchema)].[app_int_Patient_Profile]
add  First_Encounter date , Last_Encounter date

--First_Encounter
Print 'Populate #temp_en'
Print getdate()

IF OBJECT_ID('tempdb..#temp_en', 'U') IS NOT NULL
DROP TABLE #temp_en

select patient_id, 
First_Encounter = case 
						when First_RX_Encounter<=First_DX_Encounter then First_RX_Encounter 
						when First_RX_Encounter>=First_DX_Encounter then First_DX_Encounter 
						when First_RX_Encounter is null then First_DX_Encounter
						when First_DX_Encounter is null then First_RX_Encounter
				  end 
,Last_Encounter = case 
						when Last_RX_Encounter >= Last_DX_Encounter then Last_RX_Encounter 
						when Last_RX_Encounter <= Last_DX_Encounter then Last_DX_Encounter 
						when Last_RX_Encounter is null then Last_DX_Encounter
						when Last_DX_Encounter is null then Last_RX_Encounter
				  end 
into #temp_en
from [$(TargetSchema)].[app_int_Patient_Profile]


update [$(TargetSchema)].[app_int_Patient_Profile]
set [$(TargetSchema)].[app_int_Patient_Profile].First_Encounter = T.First_Encounter
,[$(TargetSchema)].[app_int_Patient_Profile].Last_Encounter = T.Last_Encounter
from #Temp_en T
where [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id


/*
-- Quarterly Rx Eligibility
Print 'Populate Quarterly Rx Eligibility'
Print getdate()

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
DROP TABLE #Temp1

SELECT *,SUBSTRING(CAST(MONTH_ID AS VARCHAR),1,4) AS YEAR_, SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) AS MONTH_ ,
                     CASE WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('01','02','03') THEN 'Q1'
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('04','05','06') THEN 'Q2'
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('07','08','09') THEN 'Q3'
                           ELSE 'Q4' END AS QUARTER_INFO
INTO #Temp1
FROM [$(TargetSchema)].[src_Ptnt_Eligibility]



--##Checking whether every quarter of the patient has atleast 1 Rx activity or not
IF OBJECT_ID('tempdb..#temp2') IS NOT NULL
DROP TABLE #temp2

SELECT PATIENT_ID,YEAR_,QUARTER_INFO, 
CASE WHEN SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS QUARTER_RX_ELIG  
INTO #temp2
FROM #temp1 A
GROUP BY PATIENT_ID,YEAR_,QUARTER_INFO


--##Marking all those patients as 1 for whom every quarter has atleast 1 Rx_Activity 
IF OBJECT_ID('tempdb..#temp3') IS NOT NULL
DROP TABLE #temp3
SELECT PATIENT_ID,CASE WHEN COUNT_BIG(1) = SUM(QUARTER_RX_ELIG) THEN 1 ELSE 0 END AS FIFTH_CRITERIA_SATISFIED 
INTO #temp3 
FROM #Temp2
GROUP BY PATIENT_ID


Print 'Update app_int_Patient_Profile'
Print getdate()

UPDATE [$(TargetSchema)].[app_int_Patient_Profile]
SET [$(TargetSchema)].[app_int_Patient_Profile].Q_Rx_Elg = T.FIFTH_CRITERIA_SATISFIED
FROM #temp3 T
WHERE  [$(TargetSchema)].[app_int_Patient_Profile].patient_id=T.patient_id


----General Population

--UPDATE [$(TargetSchema)].[app_int_Patient_Profile]
--SET GenPop = 1
--WHERE IsEpilepsy = 1 
--AND ContinuousEligibility =1
--AND AgeAbove16 >16
--AND Q_RX_Elg = 1

*/

CREATE INDEX IX_PatPrf_PatID ON [$(TargetSchema)].[app_int_Patient_Profile](patient_id)


IF OBJECT_ID('[$(TargetSchema)].eligibility1') IS NOT NULL 
DROP TABLE [$(TargetSchema)].eligibility1

IF OBJECT_ID('[$(TargetSchema)].eligibility2') IS NOT NULL 
DROP TABLE [$(TargetSchema)].eligibility2

IF OBJECT_ID('[$(TargetSchema)].eligibility3') IS NOT NULL 
DROP TABLE [$(TargetSchema)].eligibility3

IF OBJECT_ID('[$(TargetSchema)].pharmacy') IS NOT NULL 
DROP TABLE [$(TargetSchema)].pharmacy

IF OBJECT_ID('[$(TargetSchema)].pharmacy1') IS NOT NULL 
DROP TABLE [$(TargetSchema)].pharmacy1

IF OBJECT_ID('[$(TargetSchema)].pharmacy4') IS NOT NULL 
DROP TABLE [$(TargetSchema)].pharmacy4

IF OBJECT_ID('[$(TargetSchema)].PatientProcessStatus') IS NOT NULL 
DROP TABLE [$(TargetSchema)].PatientProcessStatus

IF OBJECT_ID('[$(TargetSchema)].[PatientTimePeriod]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[PatientTimePeriod]

