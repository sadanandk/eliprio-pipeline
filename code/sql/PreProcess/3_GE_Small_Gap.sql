-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : To fill Small Gaps       
-- - NOTE      :
-- - Execution : 
-- - Date      : 31-May-2018                             
-- - Change History:        
-- - Change Date                                                      
-- - Change Description                                            
-- ----------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Generate Temp table to hold the small gap'
Print getdate()

select * into #cte_sml from  
( 
select ROW_NUMBER() OVER(partition by patient_id ORDER BY Patient_id,aed,service_date,days_supply) AS Num,
patient_id,service_date,drug_end_date,aed,Days_Supply,Gap_DAys
from [$(TargetSchema)].[app_int_GE_Base]
) A 


select distinct * into #SmallGap from
(
Select A.patient_id,A.service_date,A.drug_end_date,A.aed,A.Days_Supply,
A.service_date as next_service_date,B.service_date as gap_drug_end_date
from #CTE_sml A 
inner join #CTE_sml B on A.Num=B.Num-1  and A.patient_id = B.patient_id and A.aed = B.aed
where (datediff(DAY,A.drug_end_date,B.service_date)-1 <=( A.Days_Supply *2)) and A.Gap_days>=0

union

select  A.patient_id,A.service_date,A.drug_end_date,A.aed,A.Days_Supply,
A.service_date as next_service_date,A.drug_end_date as gap_drug_end_date
from #CTE_sml A
inner join (select max(num) Num,patient_id,aed from #CTE_sml group by patient_id,aed) B on A.Num=B.Num and A.patient_id = B.patient_id and A.aed=B.aed
)A

/************************* Update dataset **********************************************/

Print 'Update small gaps in app_int_GE_Base'
Print getdate()

update [$(TargetSchema)].[app_int_GE_Base]
set gap_start_date = T.next_service_date,
	gap_end_date =  T.gap_drug_end_date 
from #SmallGap T 
where     [$(TargetSchema)].[app_int_GE_Base].patient_id = T.patient_id
	  and [$(TargetSchema)].[app_int_GE_Base].service_date = T.service_date
	  and [$(TargetSchema)].[app_int_GE_Base].aed = T.aed
	  and [$(TargetSchema)].[app_int_GE_Base].days_supply = T.days_supply
