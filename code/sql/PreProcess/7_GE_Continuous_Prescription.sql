-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Stored procedure to create Continuous Presctiption       
-- - NOTE      :
-- - Execution : 
-- - Date      :  31-May-2018                           
-- - Change History:        
-- - Change Date                                                      
-- - Change Description                                            
-- ----------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate [app_int_GE_Continuous_Prescription]'
Print getdate()

IF Object_Id('[$(TargetSchema)].[app_int_GE_Continuous_Prescription]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_int_GE_Continuous_Prescription]

select distinct patient_id, mindate startdate, maxdate enddate, aed 
into [$(TargetSchema)].[app_int_GE_Continuous_Prescription]
 from  
(select patient_id, service_date, gap_start_date, gap_end_date, drug_end_date, aed
-- row_number() OVER(PARTITION BY patient_id, aed,relevantSdT, relevantEdT order by  patient_id,aed, service_date,days_supply ) AS grp
,max(drug_end_date) OVER(PARTITION BY patient_id,aed, relevantEdT ) AS maxdate
,min(service_date) OVER(PARTITION BY patient_id,aed, relevantEdT ) AS mindate from
(
select  patient_id, service_date,case when gap_start_date is null then service_date else gap_start_date end as gap_start_date 
,case when  gap_end_date is null then drug_end_date else gap_end_date end as gap_end_date, drug_end_date, aed,relevantSdT,relevantEdT,days_supply
FROM
(
select patient_id, service_date, gap_start_date, gap_end_date, drug_end_date, aed,relevantSdT,days_supply
,Min(relevantSdT) OVER(PARTITION BY patient_id,aed order by  patient_id,aed,num desc ROWS UNBOUNDED PRECEDING) AS relevantEdT
from 
(
select patient_id, service_date, gap_start_date, gap_end_date,drug_end_date, aed
, relevantSdT=case when gap_end_date is null then service_date end, days_supply
,row_number() OVER(PARTITION BY patient_id, aed order by  patient_id,aed, service_date,days_supply ) as num

FROM [$(TargetSchema)].app_int_GE_Base AS AAN 

)d  --order by service_date
)a --order by service_date
where relevantEdT is not null

union
select  patient_id, service_date, gap_start_date, gap_end_date,drug_end_date,aed,relevantSdT,relevantEdT,days_supply from
(
select patient_id, service_date, gap_start_date, gap_end_date,drug_end_date,aed,relevantSdT, max(drug_end_date) OVER(PARTITION BY patient_id,aed order by  patient_id,aed) as relevantEdT,days_supply
from
(
select  patient_id, service_date,case when gap_start_date is null then service_date else gap_start_date end as gap_start_date 
,case when  gap_end_date is null then drug_end_date else gap_end_date end as gap_end_date, drug_end_date, aed,relevantSdT,relevantEdT,days_supply
FROM
(
select patient_id, service_date, gap_start_date, gap_end_date, drug_end_date, aed,relevantSdT, days_supply
,Min(relevantSdT) OVER(PARTITION BY patient_id,aed order by  patient_id,aed,num desc ROWS UNBOUNDED PRECEDING) AS relevantEdT
from 
(
select patient_id, service_date, gap_start_date, gap_end_date,drug_end_date, aed
, relevantSdT=case when gap_end_date is null then service_date end,days_supply
,row_number() OVER(PARTITION BY patient_id, aed order by  patient_id,aed, service_date,days_supply ) as num
FROM [$(TargetSchema)].app_int_GE_Base AS AAN 
)a 
)b
)c
where relevantEdT is null
)d --order by service_date
)e --order by service_date
)f
