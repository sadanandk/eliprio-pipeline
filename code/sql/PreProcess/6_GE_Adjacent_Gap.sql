-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Stored procedure to fill adjacent gaps in the base table for Continuous Presctiption       
-- - NOTE      :
-- - Execution : 
-- - Date      : 31-May-2018                            
-- - Change History:        
-- - Change Date                                                      
-- - Change Description                                            
-- ----------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Generate Temp table to hold the adjacent gap'
Print getdate()

select * into #cte_adj from 
( 
select ROW_NUMBER() OVER(partition by patient_id ORDER BY Patient_id,aed,service_date,days_supply) AS Num,
patient_id,service_date,drug_end_date,aed,Days_Supply,gap_days
from [$(TargetSchema)].[app_int_GE_Base]
) A

select distinct  * into #Adjacent from
(
Select  A.patient_id,A.service_date,A.drug_end_date,A.aed,A.Days_Supply
,A.service_date as next_service_date,B.drug_end_date as gap_drug_end_date
from #cte_adj A 
inner join #cte_adj B on A.Num=B.Num-1  and A.patient_id = B.patient_id and A.aed = B.aed
where datediff(DAY,A.drug_end_date, B.drug_end_date)<=90  and (B.gap_days>0 and A.gap_days>0)

union

select  A.patient_id,A.service_date,A.drug_end_date,A.aed,A.Days_Supply,
A.service_date as next_service_date,A.drug_end_date as gap_drug_end_date
from #cte_adj A
inner join (select max(num) Num,patient_id,aed from #cte_adj group by patient_id,aed) B on A.Num=B.Num and A.patient_id = B.patient_id and A.aed=B.aed
)A


/************************* Update dataset **********************************************/

Print 'Update adjacent gaps in app_int_GE_Base'
Print getdate()

update [$(TargetSchema)].[app_int_GE_Base]
set gap_start_date = [$(TargetSchema)].[app_int_GE_Base].service_date,
	gap_end_date =  T.gap_drug_end_date 
from #Adjacent T 
where	  [$(TargetSchema)].[app_int_GE_Base].patient_id = T.patient_id
	  and [$(TargetSchema)].[app_int_GE_Base].service_date = T.service_date
	  and [$(TargetSchema)].[app_int_GE_Base].aed = T.aed
	  and [$(TargetSchema)].[app_int_GE_Base].days_supply = T.days_supply
