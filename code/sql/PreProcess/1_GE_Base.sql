-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Stored procedure to create the base table for Continuous Presctiption       
-- - NOTE      :
-- - Execution : 
-- - Date      : 31-May-2018                             
-- - Change History:        
-- - Change Date                                                      
-- - Change Description                                        
-- ----------------------------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Generate Temp table to hold RX Data'
Print getdate()

Select distinct   claims.patient_id,convert(date,claims.service_date) service_date,convert(INT, CEILING(claims.Days_Supply))days_supply,aedref.aed,
dateadd(DAY,-1,convert(date,dateadd(DAY,convert(INT, CEILING(claims.Days_Supply)),claims.service_date))) as drug_end_date 
into #Temp
from [$(SrcSchema)].[src_rx_claims] claims 
left join  [$(SrcSchema)].[src_ref_product] prod  on claims.ndc = prod.ndc
left join [$(TargetSchema)].[std_ref_AEDName] aedref on prod.generic_name = aedref.generic_name
where aedref.aed is not null and claims.days_supply >0

-- drop table #temp1
select *,ROW_NUMBER() OVER(partition by patient_id ORDER BY Patient_id,aed,service_date,days_supply) AS Num
into #temp1
from #temp

-- drop table #recordstoexclude
Select  A.*
into #recordstoexclude
from #temp1 A 
inner join #temp1 B on A.Num<B.Num and A.patient_id = B.patient_id and A.aed = B.aed
where B.service_date = A.service_date and A.drug_end_date <= B.drug_end_date

union

Select  B.*
from #temp1 A 
inner join #temp1 B on A.Num<B.Num and A.patient_id = B.patient_id and A.aed = B.aed
where B.service_date >= A.service_date and B.drug_end_date <= A.drug_end_date

Print 'Populate app_int_GE_Base'
Print getdate()


IF Object_Id('[$(TargetSchema)].[app_int_GE_Base]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_int_GE_Base]

select patient_id,service_date,days_supply,aed, drug_end_date 
into  [$(TargetSchema)].[app_int_GE_Base]
from #temp
except
select patient_id,service_date,days_supply,aed, drug_end_date 
from #recordstoexclude

alter table [$(TargetSchema)].[app_int_GE_Base]
add  gap_start_date date, gap_end_date date,Gap_days int

create statistics base_patient_id on	[$(TargetSchema)].[app_int_GE_Base](patient_id)
create statistics base_service_date on  [$(TargetSchema)].[app_int_GE_Base](service_date)
create statistics base_aed on			[$(TargetSchema)].[app_int_GE_Base](aed)




