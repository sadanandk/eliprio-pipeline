-- ---------------------------------------------------------------------
-- - CITIUS TECH
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create GT feature table for  USP classes for DRE patients   
-- - NOTE      : 
--				 
-- - Execution : 
-- - Date      : 21-May-2018		
	-- ---------------------------------------------------------------------- 

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate #USP_GN_Mapping_Distinct'
Print Getdate()

IF OBJECT_ID('tempdb..#USP_GN_Mapping_Distinct') IS NOT NULL
DROP TABLE #USP_GN_Mapping_Distinct

SELECT  DISTINCT A.patient_id,A.valid_index,B.service_date,B.NDC,C.generic_name,D.[USP Class]  
INTO #USP_GN_Mapping_Distinct
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
LEFT JOIN [$(SrcSchema)].[src_rx_claims] B 	 WITH(NOLOCK)
ON A.patient_id=B.patient_id
AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
and B.days_supply>0
LEFT JOIN  [$(SrcSchema)].[src_Ref_Product] C
ON B.NDC =C.NDC
LEFT JOIN [$(TargetSchema)].[std_ref_MedicationCodesUSP] D
ON C.[Generic_Name] = D.[Generic_Name]


Print 'Populate #Main_USP_Class'
Print Getdate()

IF OBJECT_ID('tempdb..#Main_USP_Class') IS NOT NULL
DROP TABLE #Main_USP_Class

SELECT A.patient_id,A.valid_index
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  IS NULL THEN 1 ELSE  0 END) AS 'USP_'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = '1st Generation/Typical' THEN 1 ELSE  0 END) AS 'USP_1st Generation/Typical'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = '2nd Generation/Atypical' THEN 1 ELSE  0 END) AS 'USP_2nd Generation/Atypical'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Alcohol Deterrents/Anti-craving' THEN 1 ELSE  0 END) AS 'USP_Alcohol Deterrents/Anti-craving'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Alkylating Agents' THEN 1 ELSE  0 END) AS 'USP_Alkylating Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Alpha-adrenergic Agonists' THEN 1 ELSE  0 END) AS 'USP_Alpha-adrenergic Agonists'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Alpha-adrenergic Blocking Agents' THEN 1 ELSE  0 END) AS 'USP_Alpha-adrenergic Blocking Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Aminoglycosides' THEN 1 ELSE  0 END) AS 'USP_Aminoglycosides'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Aminosalicylates' THEN 1 ELSE  0 END) AS 'USP_Aminosalicylates'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anabolic Steroids' THEN 1 ELSE  0 END) AS 'USP_Anabolic Steroids'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Androgens' THEN 1 ELSE  0 END) AS 'USP_Androgens'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Angioedema (HAE) Agents' THEN 1 ELSE  0 END) AS 'USP_Angioedema (HAE) Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Angiotensin II Receptor Antagonists' THEN 1 ELSE  0 END) AS 'USP_Angiotensin II Receptor Antagonists'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Angiotensin-converting Enzyme (ACE) Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Angiotensin-converting Enzyme (ACE) Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anthelmintics' THEN 1 ELSE  0 END) AS 'USP_Anthelmintics'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiandrogens' THEN 1 ELSE  0 END) AS 'USP_Antiandrogens'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiangiogenic Agents' THEN 1 ELSE  0 END) AS 'USP_Antiangiogenic Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiarrhythmics' THEN 1 ELSE  0 END) AS 'USP_Antiarrhythmics'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antibacterials Other' THEN 1 ELSE  0 END) AS 'USP_Antibacterials Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anticholinergics' THEN 1 ELSE  0 END) AS 'USP_Anticholinergics'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anticoagulants' THEN 1 ELSE  0 END) AS 'USP_Anticoagulants'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anticonvulsants Other' THEN 1 ELSE  0 END) AS 'USP_Anticonvulsants Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-cytomegalovirus (CMV) Agents' THEN 1 ELSE  0 END) AS 'USP_Anti-cytomegalovirus (CMV) Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antidepressants Other' THEN 1 ELSE  0 END) AS 'USP_Antidepressants Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antidiabetic Agents' THEN 1 ELSE  0 END) AS 'USP_Antidiabetic Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiemetics Other' THEN 1 ELSE  0 END) AS 'USP_Antiemetics Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiestrogens/Modifiers' THEN 1 ELSE  0 END) AS 'USP_Antiestrogens/Modifiers'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-hepatitis B (HBV) Agents' THEN 1 ELSE  0 END) AS 'USP_Anti-hepatitis B (HBV) Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-hepatitis C (HCV) Agents' THEN 1 ELSE  0 END) AS 'USP_Anti-hepatitis C (HCV) Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiherpetic Agents' THEN 1 ELSE  0 END) AS 'USP_Antiherpetic Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antihistamines' THEN 1 ELSE  0 END) AS 'USP_Antihistamines'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-HIV Agents Integrase Inhibitors (INSTI)' THEN 1 ELSE  0 END) AS 'USP_Anti-HIV Agents Integrase Inhibitors (INSTI)'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-HIV Agents Non-nucleoside Reverse Transcriptase Inhibitors (NNRTI)' THEN 1 ELSE  0 END) AS 'USP_Anti-HIV Agents Non-nucleoside Reverse Transcriptase Inhibitors (NNRTI)'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-HIV Agents Nucleoside and Nucleotide Reverse Transcriptase Inhibitors (NRTI)' THEN 1 ELSE  0 END) AS 'USP_Anti-HIV Agents Nucleoside and Nucleotide Reverse Transcriptase Inhibitors (NRTI)'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-HIV Agents Other' THEN 1 ELSE  0 END) AS 'USP_Anti-HIV Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-HIV Agents Protease Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Anti-HIV Agents Protease Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-inflammatories Inhaled Corticosteroids' THEN 1 ELSE  0 END) AS 'USP_Anti-inflammatories Inhaled Corticosteroids'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anti-influenza Agents' THEN 1 ELSE  0 END) AS 'USP_Anti-influenza Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antileukotrienes' THEN 1 ELSE  0 END) AS 'USP_Antileukotrienes'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antimetabolites' THEN 1 ELSE  0 END) AS 'USP_Antimetabolites'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antimycobacterials Other' THEN 1 ELSE  0 END) AS 'USP_Antimycobacterials Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antineoplastics Other' THEN 1 ELSE  0 END) AS 'USP_Antineoplastics Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiparkinson Agents Other' THEN 1 ELSE  0 END) AS 'USP_Antiparkinson Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antiprotozoals' THEN 1 ELSE  0 END) AS 'USP_Antiprotozoals'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antispasmodics Gastrointestinal' THEN 1 ELSE  0 END) AS 'USP_Antispasmodics Gastrointestinal'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antispasmodics Urinary' THEN 1 ELSE  0 END) AS 'USP_Antispasmodics Urinary'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antithyroid Agents' THEN 1 ELSE  0 END) AS 'USP_Antithyroid Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Antituberculars' THEN 1 ELSE  0 END) AS 'USP_Antituberculars'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Anxiolytics Other' THEN 1 ELSE  0 END) AS 'USP_Anxiolytics Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Aromatase Inhibitors 3rd Generation' THEN 1 ELSE  0 END) AS 'USP_Aromatase Inhibitors 3rd Generation'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Attention Deficit Hyperactivity Disorder Agents  Non-amphetamines' THEN 1 ELSE  0 END) AS 'USP_Attention Deficit Hyperactivity Disorder Agents  Non-amphetamines'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Attention Deficit Hyperactivity Disorder Agents Amphetamines' THEN 1 ELSE  0 END) AS 'USP_Attention Deficit Hyperactivity Disorder Agents Amphetamines'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Benign Prostatic Hypertrophy Agents' THEN 1 ELSE  0 END) AS 'USP_Benign Prostatic Hypertrophy Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Benzodiazepines' THEN 1 ELSE  0 END) AS 'USP_Benzodiazepines'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Beta-adrenergic Blocking Agents' THEN 1 ELSE  0 END) AS 'USP_Beta-adrenergic Blocking Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Beta-lactam Cephalosporins' THEN 1 ELSE  0 END) AS 'USP_Beta-lactam Cephalosporins'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Beta-lactam Other' THEN 1 ELSE  0 END) AS 'USP_Beta-lactam Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Beta-lactam Penicillins' THEN 1 ELSE  0 END) AS 'USP_Beta-lactam Penicillins'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Bipolar Agents Other' THEN 1 ELSE  0 END) AS 'USP_Bipolar Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Blood Formation Modifiers' THEN 1 ELSE  0 END) AS 'USP_Blood Formation Modifiers'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Bronchodilators Anticholinergic' THEN 1 ELSE  0 END) AS 'USP_Bronchodilators Anticholinergic'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Bronchodilators Phosphodiesterase Inhibitors (Xanthines) Phosphodiesterase Inhibitors Airways Disease' THEN 1 ELSE  0 END) AS 'USP_Bronchodilators Phosphodiesterase Inhibitors (Xanthines) Phosphodiesterase Inhibitors Airways Disease'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Bronchodilators Sympathomimetic' THEN 1 ELSE  0 END) AS 'USP_Bronchodilators Sympathomimetic'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Calcium Channel Blocking Agents' THEN 1 ELSE  0 END) AS 'USP_Calcium Channel Blocking Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Calcium Channel Modifying Agents' THEN 1 ELSE  0 END) AS 'USP_Calcium Channel Modifying Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Cardiovascular Agents Other' THEN 1 ELSE  0 END) AS 'USP_Cardiovascular Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Central Nervous System Other' THEN 1 ELSE  0 END) AS 'USP_Central Nervous System Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Cholinesterase Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Cholinesterase Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Cystic Fibrosis Agents' THEN 1 ELSE  0 END) AS 'USP_Cystic Fibrosis Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Diuretics Carbonic Anhydrase Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Diuretics Carbonic Anhydrase Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Diuretics Loop' THEN 1 ELSE  0 END) AS 'USP_Diuretics Loop'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Diuretics Potassium-sparing' THEN 1 ELSE  0 END) AS 'USP_Diuretics Potassium-sparing'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Diuretics Thiazide' THEN 1 ELSE  0 END) AS 'USP_Diuretics Thiazide'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Dopamine Agonists' THEN 1 ELSE  0 END) AS 'USP_Dopamine Agonists'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Dopamine Precursors/ L-Amino Acid Decarboxylase Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Dopamine Precursors/ L-Amino Acid Decarboxylase Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Dyslipidemics Fibric Acid Derivatives' THEN 1 ELSE  0 END) AS 'USP_Dyslipidemics Fibric Acid Derivatives'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Dyslipidemics HMG CoA Reductase Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Dyslipidemics HMG CoA Reductase Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Dyslipidemics Other' THEN 1 ELSE  0 END) AS 'USP_Dyslipidemics Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Electrolyte/Mineral Modifiers' THEN 1 ELSE  0 END) AS 'USP_Electrolyte/Mineral Modifiers'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Electrolyte/Mineral Replacement' THEN 1 ELSE  0 END) AS 'USP_Electrolyte/Mineral Replacement'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Emetogenic Therapy Adjuncts' THEN 1 ELSE  0 END) AS 'USP_Emetogenic Therapy Adjuncts'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Enzyme Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Enzyme Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Ergot Alkaloids' THEN 1 ELSE  0 END) AS 'USP_Ergot Alkaloids'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Estrogens' THEN 1 ELSE  0 END) AS 'USP_Estrogens'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Fibromyalgia Agents' THEN 1 ELSE  0 END) AS 'USP_Fibromyalgia Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'GABA Receptor Modulators' THEN 1 ELSE  0 END) AS 'USP_GABA Receptor Modulators'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Gamma-aminobutyric Acid (GABA) Augmenting Agents' THEN 1 ELSE  0 END) AS 'USP_Gamma-aminobutyric Acid (GABA) Augmenting Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Gastrointestinal Agents Other' THEN 1 ELSE  0 END) AS 'USP_Gastrointestinal Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Genitourinary Agents Other' THEN 1 ELSE  0 END) AS 'USP_Genitourinary Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Glucocorticoids' THEN 1 ELSE  0 END) AS 'USP_Glucocorticoids'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Glucocorticoids/ Mineralocorticoids' THEN 1 ELSE  0 END) AS 'USP_Glucocorticoids/ Mineralocorticoids'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Glutamate Reducing Agents' THEN 1 ELSE  0 END) AS 'USP_Glutamate Reducing Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Glycemic Agents' THEN 1 ELSE  0 END) AS 'USP_Glycemic Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Histamine2 (H2) Receptor Antagonists' THEN 1 ELSE  0 END) AS 'USP_Histamine2 (H2) Receptor Antagonists'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Immune Suppressants' THEN 1 ELSE  0 END) AS 'USP_Immune Suppressants'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Immunomodulators' THEN 1 ELSE  0 END) AS 'USP_Immunomodulators'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Irritable Bowel Syndrome Agents' THEN 1 ELSE  0 END) AS 'USP_Irritable Bowel Syndrome Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Laxatives' THEN 1 ELSE  0 END) AS 'USP_Laxatives'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Local Anesthetics' THEN 1 ELSE  0 END) AS 'USP_Local Anesthetics'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Macrolides' THEN 1 ELSE  0 END) AS 'USP_Macrolides'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Mast Cell Stabilizers' THEN 1 ELSE  0 END) AS 'USP_Mast Cell Stabilizers'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Molecular Target Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Molecular Target Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Monoamine Oxidase B (MAO-B) Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Monoamine Oxidase B (MAO-B) Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Monoamine Oxidase Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Monoamine Oxidase Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Monoclonal Antibodies' THEN 1 ELSE  0 END) AS 'USP_Monoclonal Antibodies'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Mood Stabilizers' THEN 1 ELSE  0 END) AS 'USP_Mood Stabilizers'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Multiple Sclerosis Agents' THEN 1 ELSE  0 END) AS 'USP_Multiple Sclerosis Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'N-methyl-D-aspartate (NMDA) Receptor Antagonist' THEN 1 ELSE  0 END) AS 'USP_N-methyl-D-aspartate (NMDA) Receptor Antagonist'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Nonsteroidal Anti-inflammatory Drugs' THEN 1 ELSE  0 END) AS 'USP_Nonsteroidal Anti-inflammatory Drugs'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Ophthalmic  Prostaglandin and Prostamide Analogs' THEN 1 ELSE  0 END) AS 'USP_Ophthalmic  Prostaglandin and Prostamide Analogs'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Ophthalmic Agents Other' THEN 1 ELSE  0 END) AS 'USP_Ophthalmic Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Ophthalmic Anti-allergy Agents' THEN 1 ELSE  0 END) AS 'USP_Ophthalmic Anti-allergy Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Ophthalmic Antiglaucoma Agents' THEN 1 ELSE  0 END) AS 'USP_Ophthalmic Antiglaucoma Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Ophthalmic Anti-inflammatories' THEN 1 ELSE  0 END) AS 'USP_Ophthalmic Anti-inflammatories'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Opioid Analgesics Long-acting' THEN 1 ELSE  0 END) AS 'USP_Opioid Analgesics Long-acting'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Opioid Analgesics Short-acting' THEN 1 ELSE  0 END) AS 'USP_Opioid Analgesics Short-acting'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Opioid Antagonists-Opioid Reversal Agents' THEN 1 ELSE  0 END) AS 'USP_Opioid Antagonists-Opioid Reversal Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Opioid Dependence Treatments' THEN 1 ELSE  0 END) AS 'USP_Opioid Dependence Treatments'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Parasympathomimetics' THEN 1 ELSE  0 END) AS 'USP_Parasympathomimetics'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Pediculicides/Scabicides' THEN 1 ELSE  0 END) AS 'USP_Pediculicides/Scabicides'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Phosphate Binders' THEN 1 ELSE  0 END) AS 'USP_Phosphate Binders'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Platelet Modifying Agents' THEN 1 ELSE  0 END) AS 'USP_Platelet Modifying Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Progestins' THEN 1 ELSE  0 END) AS 'USP_Progestins'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Prophylactic' THEN 1 ELSE  0 END) AS 'USP_Prophylactic'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Protectants' THEN 1 ELSE  0 END) AS 'USP_Protectants'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Proton Pump Inhibitors' THEN 1 ELSE  0 END) AS 'USP_Proton Pump Inhibitors'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Pulmonary Antihypertensives' THEN 1 ELSE  0 END) AS 'USP_Pulmonary Antihypertensives'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Quinolones' THEN 1 ELSE  0 END) AS 'USP_Quinolones'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Respiratory Tract Agents Other' THEN 1 ELSE  0 END) AS 'USP_Respiratory Tract Agents Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Retinoids' THEN 1 ELSE  0 END) AS 'USP_Retinoids'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Selective Estrogen Receptor Modifying Agents' THEN 1 ELSE  0 END) AS 'USP_Selective Estrogen Receptor Modifying Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Serotonin (5-HT) 1b/1d Receptor Agonists' THEN 1 ELSE  0 END) AS 'USP_Serotonin (5-HT) 1b/1d Receptor Agonists'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Serotonin/Norepinephrine Reuptake Inhibitors  SSRIs/SNRIs (Selective Serotonin Reuptake Inhibitors/ Serotonin and Norepinephrine Reuptake Inhibitors)' THEN 1 ELSE  0 END) AS 'USP_Serotonin/Norepinephrine Reuptake Inhibitors  SSRIs/SNRIs'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Sleep Disorders Other' THEN 1 ELSE  0 END) AS 'USP_Sleep Disorders Other'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Smoking Cessation Agents' THEN 1 ELSE  0 END) AS 'USP_Smoking Cessation Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Sodium Channel Agents' THEN 1 ELSE  0 END) AS 'USP_Sodium Channel Agents'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'SSRIs/SNRIs (Selective Serotonin Reuptake Inhibitors/ Serotonin and Norepinephrine Reuptake Inhibitors)' THEN 1 ELSE  0 END) AS 'USP_SSRIs/SNRIs (Selective Serotonin Reuptake Inhibitors/ Serotonin and Norepinephrine Reuptake Inhibitors)'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Sulfonamides' THEN 1 ELSE  0 END) AS 'USP_Sulfonamides'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Tetracyclines' THEN 1 ELSE  0 END) AS 'USP_Tetracyclines'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Treatment-Resistant' THEN 1 ELSE  0 END) AS 'USP_Treatment-Resistant'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Tricyclics' THEN 1 ELSE  0 END) AS 'USP_Tricyclics'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Vasodilators Direct-acting Arterial' THEN 1 ELSE  0 END) AS 'USP_Vasodilators Direct-acting Arterial'
,SUM(CASE WHEN REPLACE(B.[USP Class] ,',','')  = 'Vasodilators Direct-acting Arterial/Venous' THEN 1 ELSE  0 END) AS 'USP_Vasodilators Direct-acting Arterial/Venous'
INTO #Main_USP_Class
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
LEFT JOIN #USP_GN_Mapping_Distinct B
ON A.patient_id =B.patient_id
AND A.Valid_index=B.Valid_index
GROUP BY A.patient_id,A.valid_index


IF OBJECT_ID('tempdb..#USP_GN_Mapping_Distinct_30days') IS NOT NULL
DROP TABLE #USP_GN_Mapping_Distinct_30days

SELECT  DISTINCT A.patient_id,A.valid_index,B.service_date,B.NDC,C.generic_name,D.[USP Class]  
INTO #USP_GN_Mapping_Distinct_30days
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
LEFT JOIN [$(SrcSchema)].[src_rx_claims] B 	 WITH(NOLOCK)
ON A.patient_id=B.patient_id
AND B.Service_Date BETWEEN  DATEADD(DD,-30,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
and B.days_supply>0
LEFT JOIN  [$(SrcSchema)].[src_Ref_Product] C
ON B.NDC =C.NDC
LEFT JOIN [$(TargetSchema)].[std_ref_MedicationCodesUSP] D
ON C.[Generic_Name] = D.[Generic_Name]


IF OBJECT_ID('tempdb..#Cotreatments_last30Days') IS NOT NULL
DROP TABLE #Cotreatments_last30Days

SELECT A.patient_id,A.valid_index 
,CASE WHEN SUM(CASE WHEN REPLACE(B.[USP Class] ,',','') like '%ANTIBACTERIALS%' THEN 1 ELSE  0 END) >=1 THEN 1 ELSE 0 END AS [COTREAT30_ANTIBIOTICS]
,CASE WHEN SUM(CASE WHEN REPLACE(B.[USP Class] ,',','') like '%ANTICOAGULANTS%' THEN 1 ELSE  0 END) >=1 THEN 1 ELSE 0 END AS [COTREAT30_ANTICOAGULANTS]
,CASE WHEN SUM(CASE WHEN REPLACE(B.[USP Class] ,',','') like '%CORTICOSTEROIDS%' THEN 1 ELSE  0 END)>=1 THEN 1 ELSE 0 END AS [COTREAT30_CORTICOSTEROIDS]
,CASE WHEN SUM(CASE WHEN REPLACE(B.[USP Class] ,',','') like '%BARBITURATES%' OR REPLACE(B.[USP Class] ,',','') like '%BENZODIAZEPINES%' OR REPLACE(B.[USP Class] ,',','') like '%ANTIPSYCHOTICS%' OR REPLACE(B.[USP Class] ,',','') like '%ANTIDEPRESSANTS%'  OR REPLACE(B.[USP Class] ,',','') like '%TRICYCLICS%' THEN 1 ELSE  0 END) >= 1 THEN 1 ELSE 0 END AS [COTREAT30_PHARMACA]
INTO #Cotreatments_last30Days
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
LEFT JOIN #USP_GN_Mapping_Distinct_30days B
ON A.patient_id =B.patient_id
AND A.Valid_index=B.Valid_index
GROUP BY A.patient_id,A.valid_index

Print 'Populate app_dre_features_USP_Class'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_USP_Class]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_USP_Class]

SELECT A.patient_id,A.Valid_Index
	  ,[USP_]
      ,[USP_1st Generation/Typical]
      ,[USP_2nd Generation/Atypical]
      ,[USP_Alcohol Deterrents/Anti-craving]
      ,[USP_Alkylating Agents]
      ,[USP_Alpha-adrenergic Agonists]
      ,[USP_Alpha-adrenergic Blocking Agents]
      ,[USP_Aminoglycosides]
      ,[USP_Aminosalicylates]
      ,[USP_Anabolic Steroids]
      ,[USP_Androgens]
      ,[USP_Angioedema (HAE) Agents]
      ,[USP_Angiotensin II Receptor Antagonists]
      ,[USP_Angiotensin-converting Enzyme (ACE) Inhibitors]
      ,[USP_Anthelmintics]
      ,[USP_Antiandrogens]
      ,[USP_Antiangiogenic Agents]
      ,[USP_Antiarrhythmics]
      ,[USP_Antibacterials Other]
      ,[USP_Anticholinergics]
      ,[USP_Anticoagulants]
      ,[USP_Anticonvulsants Other]
      ,[USP_Anti-cytomegalovirus (CMV) Agents]
      ,[USP_Antidepressants Other]
      ,[USP_Antidiabetic Agents]
      ,[USP_Antiemetics Other]
      ,[USP_Antiestrogens/Modifiers]
      ,[USP_Anti-hepatitis B (HBV) Agents]
      ,[USP_Anti-hepatitis C (HCV) Agents]
      ,[USP_Antiherpetic Agents]
      ,[USP_Antihistamines]
      ,[USP_Anti-HIV Agents Integrase Inhibitors (INSTI)]
      ,[USP_Anti-HIV Agents Non-nucleoside Reverse Transcriptase Inhibitors (NNRTI)]
      ,[USP_Anti-HIV Agents Nucleoside and Nucleotide Reverse Transcriptase Inhibitors (NRTI)]
      ,[USP_Anti-HIV Agents Other]
      ,[USP_Anti-HIV Agents Protease Inhibitors]
      ,[USP_Anti-inflammatories Inhaled Corticosteroids]
      ,[USP_Anti-influenza Agents]
      ,[USP_Antileukotrienes]
      ,[USP_Antimetabolites]
      ,[USP_Antimycobacterials Other]
      ,[USP_Antineoplastics Other]
      ,[USP_Antiparkinson Agents Other]
      ,[USP_Antiprotozoals]
      ,[USP_Antispasmodics Gastrointestinal]
      ,[USP_Antispasmodics Urinary]
      ,[USP_Antithyroid Agents]
      ,[USP_Antituberculars]
      ,[USP_Anxiolytics Other]
      ,[USP_Aromatase Inhibitors 3rd Generation]
      ,[USP_Attention Deficit Hyperactivity Disorder Agents  Non-amphetamines]
      ,[USP_Attention Deficit Hyperactivity Disorder Agents Amphetamines]
      ,[USP_Benign Prostatic Hypertrophy Agents]
      ,[USP_Benzodiazepines]
      ,[USP_Beta-adrenergic Blocking Agents]
      ,[USP_Beta-lactam Cephalosporins]
      ,[USP_Beta-lactam Other]
      ,[USP_Beta-lactam Penicillins]
      ,[USP_Bipolar Agents Other]
      ,[USP_Blood Formation Modifiers]
      ,[USP_Bronchodilators Anticholinergic]
      ,[USP_Bronchodilators Phosphodiesterase Inhibitors (Xanthines) Phosphodiesterase Inhibitors Airways Disease]
      ,[USP_Bronchodilators Sympathomimetic]
      ,[USP_Calcium Channel Blocking Agents]
      ,[USP_Calcium Channel Modifying Agents]
      ,[USP_Cardiovascular Agents Other]
      ,[USP_Central Nervous System Other]
      ,[USP_Cholinesterase Inhibitors]
      ,[USP_Cystic Fibrosis Agents]
      ,[USP_Diuretics Carbonic Anhydrase Inhibitors]
      ,[USP_Diuretics Loop]
      ,[USP_Diuretics Potassium-sparing]
      ,[USP_Diuretics Thiazide]
      ,[USP_Dopamine Agonists]
      ,[USP_Dopamine Precursors/ L-Amino Acid Decarboxylase Inhibitors]
      ,[USP_Dyslipidemics Fibric Acid Derivatives]
      ,[USP_Dyslipidemics HMG CoA Reductase Inhibitors]
      ,[USP_Dyslipidemics Other]
      ,[USP_Electrolyte/Mineral Modifiers]
      ,[USP_Electrolyte/Mineral Replacement]
      ,[USP_Emetogenic Therapy Adjuncts]
      ,[USP_Enzyme Inhibitors]
      ,[USP_Ergot Alkaloids]
      ,[USP_Estrogens]
      ,[USP_Fibromyalgia Agents]
      ,[USP_GABA Receptor Modulators]
      ,[USP_Gamma-aminobutyric Acid (GABA) Augmenting Agents]
      ,[USP_Gastrointestinal Agents Other]
      ,[USP_Genitourinary Agents Other]
      ,[USP_Glucocorticoids]
      ,[USP_Glucocorticoids/ Mineralocorticoids]
      ,[USP_Glutamate Reducing Agents]
      ,[USP_Glycemic Agents]
      ,[USP_Histamine2 (H2) Receptor Antagonists]
      ,[USP_Immune Suppressants]
      ,[USP_Immunomodulators]
      ,[USP_Irritable Bowel Syndrome Agents]
      ,[USP_Laxatives]
      ,[USP_Local Anesthetics]
      ,[USP_Macrolides]
      ,[USP_Mast Cell Stabilizers]
      ,[USP_Molecular Target Inhibitors]
      ,[USP_Monoamine Oxidase B (MAO-B) Inhibitors]
      ,[USP_Monoamine Oxidase Inhibitors]
      ,[USP_Monoclonal Antibodies]
      ,[USP_Mood Stabilizers]
      ,[USP_Multiple Sclerosis Agents]
      ,[USP_N-methyl-D-aspartate (NMDA) Receptor Antagonist]
      ,[USP_Nonsteroidal Anti-inflammatory Drugs]
      ,[USP_Ophthalmic  Prostaglandin and Prostamide Analogs]
      ,[USP_Ophthalmic Agents Other]
      ,[USP_Ophthalmic Anti-allergy Agents]
      ,[USP_Ophthalmic Antiglaucoma Agents]
      ,[USP_Ophthalmic Anti-inflammatories]
      ,[USP_Opioid Analgesics Long-acting]
      ,[USP_Opioid Analgesics Short-acting]
      ,[USP_Opioid Antagonists-Opioid Reversal Agents]
      ,[USP_Opioid Dependence Treatments]
      ,[USP_Parasympathomimetics]
      ,[USP_Pediculicides/Scabicides]
      ,[USP_Phosphate Binders]
      ,[USP_Platelet Modifying Agents]
      ,[USP_Progestins]
      ,[USP_Prophylactic]
      ,[USP_Protectants]
      ,[USP_Proton Pump Inhibitors]
      ,[USP_Pulmonary Antihypertensives]
      ,[USP_Quinolones]
      ,[USP_Respiratory Tract Agents Other]
      ,[USP_Retinoids]
      ,[USP_Selective Estrogen Receptor Modifying Agents]
      ,[USP_Serotonin (5-HT) 1b/1d Receptor Agonists]
      ,[USP_Serotonin/Norepinephrine Reuptake Inhibitors  SSRIs/SNRIs]
      ,[USP_Sleep Disorders Other]
      ,[USP_Smoking Cessation Agents]
      ,[USP_Sodium Channel Agents]
      ,[USP_SSRIs/SNRIs (Selective Serotonin Reuptake Inhibitors/ Serotonin and Norepinephrine Reuptake Inhibitors)]
      ,[USP_Sulfonamides]
      ,[USP_Tetracyclines]
      ,[USP_Treatment-Resistant]
      ,[USP_Tricyclics]
      ,[USP_Vasodilators Direct-acting Arterial]
      ,[USP_Vasodilators Direct-acting Arterial/Venous]
	  ,[COTREAT30_ANTIBIOTICS]
	  ,[COTREAT30_ANTICOAGULANTS]
	  ,[COTREAT30_CORTICOSTEROIDS]
	  ,[COTREAT30_PHARMACA]
INTO [$(TargetSchema)].[app_dre_features_USP_Class]
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A
LEFT JOIN #Main_USP_Class(nolock) B
ON A.patient_id   = B.patient_id
AND A.Valid_Index = B.Valid_Index
LEFT JOIN #Cotreatments_last30Days(nolock) C
ON A.patient_id   = C.patient_id
AND A.Valid_Index = C.Valid_Index
