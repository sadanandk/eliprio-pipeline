-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create GT feature table for  CCI and ECI  for DRE patients   
-- - NOTE      : 
--				 
-- - Execution : 
-- - Date      : 21-May-2018		
	-- ---------------------------------------------------------------------- 
--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate app_dre_features_CCI_ECI'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_CCI_ECI]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_CCI_ECI]


SELECT  patient_id,Valid_Index
,CONGESTHRTFAIL_1			AS CHARLESON_CONGESTHRTFAIL
,CEREBVASDISEASE_1			AS CHARLESON_CEREBVASDISEASE
,CHRNPULMDISEASE_1			AS CHARLESON_CHRNPULMDISEASE
,DEMENTIA_1					AS CHARLESON_DEMENTIA
,MILDLIVDISEASE_1			AS CHARLESON_MILDLIVDISEASE
,MYOCARDINFARCT_1			AS CHARLESON_MYOCARDINFARCT
,PEPULCERDISEASE_1			AS CHARLESON_PEPULCERDISEASE
,PERIPHVASDISEASE_1			AS CHARLESON_PERIPHVASDISEASE
,RHEUMDISEASE_1				AS CHARLESON_RHEUMDISEASE
,ANYTUMOR_2					AS CHARLESON_ANYTUMOR
,DIAB_2						AS CHARLESON_DIAB
,HEMIPLEGIA_2				AS CHARLESON_HEMIPLEGIA
,LEUKAMIA_2					AS CHARLESON_LEUKAMIA
,LYMPHOMA_2					AS CHARLESON_LYMPHOMA
,RENALDISEASE_2				AS CHARLESON_RENALDISEASE
,METSOLIDTUMOR_6			AS CHARLESON_METSOLIDTUMOR
,MODLIVDISEASE_6			AS CHARLESON_MODLIVDISEASE
,E_CARDIACARRHYTHMIAS_1		AS EPCOMOR_CARDIACARRHYTHMIAS
,E_HYPERTENSION_1			AS EPCOMOR_HYPERTENSION
,E_MYOCARDINFARCT_1			AS EPCOMOR_MYOCARDINFARCT
,E_SOLIDTUMOR_2				AS EPCOMOR_SOLIDTUMOR
,E_ASPPNEU_2				AS EPCOMOR_ASPPNEU
,E_ANOXICBRAININJURY_3		AS EPCOMOR_ANOXICBRAININJURY
,E_BRAINTUMOR_3				AS EPCOMOR_BRAINTUMOR
,(CONGESTHRTFAIL_1 + CEREBVASDISEASE_1 + CHRNPULMDISEASE_1 + DEMENTIA_1 +  MILDLIVDISEASE_1 +  MYOCARDINFARCT_1 +  PEPULCERDISEASE_1 +  PERIPHVASDISEASE_1 +  RHEUMDISEASE_1 )*1 +
 (ANYTUMOR_2 +  DIAB_2 +  HEMIPLEGIA_2 +  LEUKAMIA_2 +  LYMPHOMA_2 +  RENALDISEASE_2)*2 +
 (METSOLIDTUMOR_6 +  MODLIVDISEASE_6)*6 AS CCISCORE
,(E_MYOCARDINFARCT_1 + E_HYPERTENSION_1 + E_CARDIACARRHYTHMIAS_1)*1 +  ( CONGESTHRTFAIL_1 + PERIPHVASDISEASE_1 + RENALDISEASE_2 + E_SOLIDTUMOR_2 + HEMIPLEGIA_2 + E_ASPPNEU_2 + DEMENTIA_1  )*2 + 
 (E_BRAINTUMOR_3 + E_ANOXICBRAININJURY_3 + MODLIVDISEASE_6 )*3 + (METSOLIDTUMOR_6)*6 AS EPSCORE
 INTO [$(TargetSchema)].[app_dre_features_CCI_ECI]
FROM 
(
	SELECT A.patient_id,A.Valid_index
	,CASE WHEN CONGESTHRTFAIL_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS CONGESTHRTFAIL_1
	,CASE WHEN CEREBVASDISEASE_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS CEREBVASDISEASE_1
	,CASE WHEN CHRNPULMDISEASE_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS CHRNPULMDISEASE_1
	,CASE WHEN DEMENTIA_1.patient_id				IS NOT NULL THEN 1 ELSE 0 END AS DEMENTIA_1
	,CASE WHEN MILDLIVDISEASE_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS MILDLIVDISEASE_1
	,CASE WHEN MYOCARDINFARCT_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS MYOCARDINFARCT_1
	,CASE WHEN PEPULCERDISEASE_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS PEPULCERDISEASE_1
	,CASE WHEN PERIPHVASDISEASE_1.patient_id		IS NOT NULL THEN 1 ELSE 0 END AS PERIPHVASDISEASE_1
	,CASE WHEN RHEUMDISEASE_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS RHEUMDISEASE_1
	,CASE WHEN ANYTUMOR_2.patient_id				IS NOT NULL THEN 1 ELSE 0 END AS ANYTUMOR_2
	,CASE WHEN DIAB_2.patient_id					IS NOT NULL THEN 1 ELSE 0 END AS DIAB_2
	,CASE WHEN HEMIPLEGIA_2.patient_id				IS NOT NULL THEN 1 ELSE 0 END AS HEMIPLEGIA_2
	,CASE WHEN LEUKAMIA_2.patient_id				IS NOT NULL THEN 1 ELSE 0 END AS LEUKAMIA_2
	,CASE WHEN LYMPHOMA_2.patient_id				IS NOT NULL THEN 1 ELSE 0 END AS LYMPHOMA_2
	,CASE WHEN RENALDISEASE_2.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS RENALDISEASE_2
	,CASE WHEN METSOLIDTUMOR_6.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS METSOLIDTUMOR_6
	,CASE WHEN MODLIVDISEASE_6.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS MODLIVDISEASE_6
	,CASE WHEN E_CARDIACARRHYTHMIAS_1.patient_id	IS NOT NULL THEN 1 ELSE 0 END AS E_CARDIACARRHYTHMIAS_1
	,CASE WHEN E_HYPERTENSION_1.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS E_HYPERTENSION_1
	,CASE WHEN E_MYOCARDINFARCT_1.patient_id		IS NOT NULL THEN 1 ELSE 0 END AS E_MYOCARDINFARCT_1
	,CASE WHEN E_SOLIDTUMOR_2.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS E_SOLIDTUMOR_2
	,CASE WHEN E_ASPPNEU_2.patient_id				IS NOT NULL THEN 1 ELSE 0 END AS E_ASPPNEU_2
	,CASE WHEN E_ANOXICBRAININJURY_3.patient_id		IS NOT NULL THEN 1 ELSE 0 END AS E_ANOXICBRAININJURY_3
	,CASE WHEN E_BRAINTUMOR_3.patient_id			IS NOT NULL THEN 1 ELSE 0 END AS E_BRAINTUMOR_3
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (nolock) A


	/*  code.startsWith("398") || code.startsWith("402") || code.startsWith("428")) */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '398%' or  [icd9cm] like '402%' or  [icd9cm] like '428%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '398%' or  [icd9cm] like '402%' or  [icd9cm] like '428%' 
									)
				) CONGESTHRTFAIL_1
	ON A.Patient_id = CONGESTHRTFAIL_1.patient_id


	/* 430* or 431* or 432* or 433* or 435* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '430%' or  [icd9cm] like '431%' or  [icd9cm] like '432%' OR  [icd9cm] like '433%'  OR  [icd9cm] like '435%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '430%' or  [icd9cm] like '431%' or  [icd9cm] like '432%' OR  [icd9cm] like '433%'  OR  [icd9cm] like '435%' 
									)
				) CEREBVASDISEASE_1
	ON A.Patient_id = CEREBVASDISEASE_1.patient_id

	/* 491* to 493* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='491' AND  [icd9cm] <='49392'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='491' AND  [icd9cm] <='49392'
									)
				) CHRNPULMDISEASE_1
	ON A.Patient_id = CHRNPULMDISEASE_1.patient_id

	/* 290* or 291* or 294* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '290%' or  [icd9cm] like '291%' or  [icd9cm] like '294%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '290%' or  [icd9cm] like '291%' or  [icd9cm] like '294%' 
									)
				) DEMENTIA_1
	ON A.Patient_id = DEMENTIA_1.patient_id

	/* 571* or 573* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '571%' or  [icd9cm] like '573%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '571%' or  [icd9cm] like '573%' 
									)
				) MILDLIVDISEASE_1
	ON A.Patient_id = MILDLIVDISEASE_1.patient_id


	/* 410* or 411* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '410%' or  [icd9cm] like '411%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '410%' or  [icd9cm] like '411%' 
									)
				) MYOCARDINFARCT_1
	ON A.Patient_id = MYOCARDINFARCT_1.patient_id


	/* 531* to 534*  */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='531' AND  [icd9cm] <='53491'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='531' AND  [icd9cm] <='53491'
									)
				) PEPULCERDISEASE_1
	ON A.Patient_id = PEPULCERDISEASE_1.patient_id


	/* 440* to 447*  */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='440' AND  [icd9cm] <='4479'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='440' AND  [icd9cm] <='4479'
									)
				) PERIPHVASDISEASE_1
	ON A.Patient_id = PERIPHVASDISEASE_1.patient_id


	/* 710* or 714* or 725* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '710%' or  [icd9cm] like '714%' or  [icd9cm] like '725%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '710%' or  [icd9cm] like '714%' or  [icd9cm] like '725%' 
									)
				) RHEUMDISEASE_1
	ON A.Patient_id = RHEUMDISEASE_1.patient_id

	/* 140* to 195* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='140' AND  [icd9cm] <='1958'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='140' AND  [icd9cm] <='1958'
									)
				) ANYTUMOR_2
	ON A.Patient_id = ANYTUMOR_2.patient_id

	/* 250* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '250%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '250%' 
									)
				) DIAB_2
	ON A.Patient_id = DIAB_2.patient_id

	/* 342* or 434* or 436* or 437* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '342%' or  [icd9cm] like '434%' or  [icd9cm] like '436%' or  [icd9cm] like '437%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '342%' or  [icd9cm] like '434%' or  [icd9cm] like '436%' or  [icd9cm] like '437%' 
									)
				) HEMIPLEGIA_2
	ON A.Patient_id = HEMIPLEGIA_2.patient_id


	/* 204* to 207* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='204' AND  [icd9cm] <='20782'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='204' AND  [icd9cm] <='20782'
									)
				) LEUKAMIA_2
	ON A.Patient_id = LEUKAMIA_2.patient_id

	/* 200* to 203* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='200' AND  [icd9cm] <='20382'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='200' AND  [icd9cm] <='20382'
									)
				) LYMPHOMA_2
	ON A.Patient_id = LYMPHOMA_2.patient_id

	/* 403* or 404* or (580* to 586*) */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '403%' or  [icd9cm] like '404%' or ( [icd9cm]  >= '580' AND  [icd9cm] <= '586' )
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '403%' or  [icd9cm] like '404%' or ( [icd9cm]  >= '580' AND  [icd9cm] <= '586' )
									)
				) RENALDISEASE_2
	ON A.Patient_id = RENALDISEASE_2.patient_id


	/* 196* to 199* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='196' AND  [icd9cm] <='1992'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='196' AND  [icd9cm] <='1992'
									)
				) METSOLIDTUMOR_6
	ON A.Patient_id = METSOLIDTUMOR_6.patient_id


	/* 070* or 570* or 572* */

	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '070%' or  [icd9cm] like '570%' or  [icd9cm] like '572%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '070%' or  [icd9cm] like '570%' or  [icd9cm] like '572%' 
									)
				) MODLIVDISEASE_6
	ON A.Patient_id = MODLIVDISEASE_6.patient_id


	/* 42610* or 42611* or 42613* or 42731* or 42760* or 4279* or 7850* or V45* or V533 or (4262* to 4264*) or (42650* to 42653*) or (4266* to 4267*) or (42680* TO 42689*) or (4270* TO 4272*) */

	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '42610%' or  [icd9cm] like '42611%' or  [icd9cm] like '42613%' or [icd9cm] like '42731%' or  [icd9cm] like '42760%' or  [icd9cm] like '4279%' or [icd9cm] like '7850%' or  [icd9cm] like 'V45%' or  [icd9cm] like 'V533%' 
																		or ( [icd9cm]  >= '4262' AND  [icd9cm] <= '4264') or  ( [icd9cm]  >= '42650' AND  [icd9cm] <= '42653') or  ( [icd9cm]  >= '4266' AND  [icd9cm] <= '4267')   or  ( [icd9cm]  >= '42680' AND  [icd9cm] <= '42689')  or  ( [icd9cm]  >= '4270' AND  [icd9cm] <= '4272')
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '42610%' or  [icd9cm] like '42611%' or  [icd9cm] like '42613%' or [icd9cm] like '42731%' or  [icd9cm] like '42760%' or  [icd9cm] like '4279%' or [icd9cm] like '7850%' or  [icd9cm] like 'V45%' or  [icd9cm] like 'V533%' 
																		or ( [icd9cm]  >= '4262' AND  [icd9cm] <= '4264') or  ( [icd9cm]  >= '42650' AND  [icd9cm] <= '42653') or  ( [icd9cm]  >= '4266' AND  [icd9cm] <= '4267')   or  ( [icd9cm]  >= '42680' AND  [icd9cm] <= '42689')  or  ( [icd9cm]  >= '4270' AND  [icd9cm] <= '4272')
									)
				) E_CARDIACARRHYTHMIAS_1
	ON A.Patient_id = E_CARDIACARRHYTHMIAS_1.patient_id

	/* 401* to 405*  */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='401' AND  [icd9cm] <='40599'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='401' AND  [icd9cm] <='40599'
									)
				) E_HYPERTENSION_1
	ON A.Patient_id = E_HYPERTENSION_1.patient_id

	/* 410* or 411* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '410%' or  [icd9cm] like '411%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '410%' or  [icd9cm] like '411%' 
									)
				) E_MYOCARDINFARCT_1
	ON A.Patient_id = E_MYOCARDINFARCT_1.patient_id


	/* 196* to 199* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='196' AND  [icd9cm] <='1992'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] >='196' AND  [icd9cm] <='1992'
									)
				) E_SOLIDTUMOR_2
	ON A.Patient_id = E_SOLIDTUMOR_2.patient_id


	/* code.startsWith("99732") */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '99732%' 
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '99732%' 
									)
				) E_ASPPNEU_2
	ON A.Patient_id = E_ASPPNEU_2.patient_id


	/* 4379* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '4379%'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '4379%'
									)
				) E_ANOXICBRAININJURY_3
	ON A.Patient_id = E_ANOXICBRAININJURY_3.patient_id

	/* 191* or 225* */
	LEFT JOIN (	SELECT	Distinct A.patient_id  
				FROM	[$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A(nolock) 
				JOIN	[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B(nolock) 
				ON		A.patient_id=B.patient_id
				AND		B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)   
				WHERE REPLACE(DX_Codes,'.','')  IN (
									SELECT DISTINCT [icd9cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '191%' OR [icd9cm] like '225%'
									UNION
									SELECT DISTINCT [icd10cm]
									FROM [$(TargetSchema)].[std_REF_icd9_icd10_cmgem] WHERE [icd9cm] like '191%' OR [icd9cm] like '225%'
									)
				) E_BRAINTUMOR_3
	ON A.Patient_id = E_BRAINTUMOR_3.patient_id



)Main



