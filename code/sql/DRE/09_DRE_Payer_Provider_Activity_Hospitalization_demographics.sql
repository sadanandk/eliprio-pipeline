-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create GT feature table for  Payer,Physician,rx ,dx,demography and Hospitalization fro DRE patients  
-- - NOTE      :  
--				 
-- - Execution : 
-- - Date      : 16-May-2018		
					
	-- ---------------------------------------------------------------------- 

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema



----******************************************Payer Features***********************************************----
Print 'Populate #Payer_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#Payer_365d') IS NOT NULL
DROP TABLE #Payer_365d

	SELECT A.patient_id,A.Valid_Index,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'C' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYER365_C,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'M' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYER365_M,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'O' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYER365_O,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'R' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYER365_R
	INTO #Payer_365d
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
	LEFT JOIN [$(SrcSchema)].[src_rx_claims] B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	and B.days_supply>0
	GROUP BY A.patient_id,A.Valid_Index

Print 'Populate #PayerFirstAED_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#PayerFirstAED_365d') IS NOT NULL
DROP TABLE #PayerFirstAED_365d

	SELECT A.patient_id,A.Valid_Index,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'C' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYERFIRSTAED_C,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'M' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYERFIRSTAED_M,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'O' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYERFIRSTAED_O,
	   CASE WHEN SUM(CASE WHEN B.Payer_Type = 'R' THEN 1 ELSE 0 END) >=1 THEN 1 ELSE 0 END AS PAYERFIRSTAED_R
	INTO #PayerFirstAED_365d
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
	LEFT JOIN [$(TargetSchema)].[app_dre_Cohort] A1
	ON A.patient_id=A1.Patient_id
	AND A.Valid_index =A1.Valid_index
	LEFT JOIN [$(SrcSchema)].[src_rx_claims] B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.Service_Date = A.Valid_Index 
	and B.days_supply>0
	LEFT JOIN [$(SrcSchema)].[src_ref_product] C
	ON B.NDC=C.NDC 
	LEFT JOIN [$(TargetSchema)].[std_ref_AEDName] D
	ON C.generic_name =D.generic_name
	WHERE  A1.AED =D.AED
	GROUP BY A.patient_id,A.Valid_Index


----******************************************Physician Features***********************************************----
Print 'Populate #PhyFirstAED_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#PhyFirstAED_365d') IS NOT NULL
DROP TABLE #PhyFirstAED_365d

	SELECT DISTINCT a.patient_id ,A.Valid_index,
	CASE WHEN SUM(CASE WHEN PRI_SPECIALTY_DESC LIKE '%EMERGENCY%' THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END AS PHYSPECEMER
	,CASE WHEN SUM(CASE WHEN PRI_SPECIALTY_DESC LIKE '%FAMILY PRACTICE%' OR PRI_SPECIALTY_DESC LIKE '%PSYCHIATRY%' OR PRI_SPECIALTY_DESC LIKE '%EMERGENCY MEDICINE%' OR PRI_SPECIALTY_DESC LIKE '%GENERAL PRACTICE%' THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END AS PHYSPECGENERAL
	,CASE WHEN SUM(CASE WHEN PRI_SPECIALTY_DESC LIKE '%NEURO%' THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END AS PHYSPECNEURO
	,CASE WHEN SUM(CASE WHEN PRI_SPECIALTY_DESC LIKE '%RHEUMATOLOGY%' OR PRI_SPECIALTY_DESC LIKE '%ORTHOPEDIC SURGERY%' OR PRI_SPECIALTY_DESC LIKE '%PAIN MEDICINE%' OR PRI_SPECIALTY_DESC LIKE '%PODIATRY%' THEN 1 ELSE 0 END) >= 1 THEN 1 ELSE 0 END AS PHYSPECPAIN
	INTO  #PhyFirstAED_365d
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
	LEFT JOIN [$(TargetSchema)].[app_DRE_Cohort] A1
	ON A.patient_id=A1.Patient_id
	AND A.Valid_index =A1.Valid_index
	LEFT JOIN [$(SrcSchema)].[src_rx_Claims] B
	ON A.Patient_id=B.patient_id
	and B.days_supply>0
	AND A.Valid_Index = B.Service_date
	LEFT JOIN [$(SrcSchema)].[src_ref_Provider]  C
	ON B.Provider_id = C.Provider_id
	LEFT JOIN [$(SrcSchema)].[src_ref_product] D
	ON B.NDC=D.NDC 
	LEFT JOIN [$(TargetSchema)].[std_ref_AEDName] E
	ON D.generic_name =E.generic_name
	WHERE  A1.AED =E.AED
	GROUP BY A.patient_id,A.Valid_Index

----******************************************Rx/DX/HX activity Features***********************************************----

Print 'Populate #rx_activity_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#rx_activity_365d') IS NOT NULL
DROP TABLE #rx_activity_365d

	SELECT Distinct A.patient_id,A.Valid_Index,COUNT(DISTINCT CASE WHEN CONCAT(DATEPART(YYYY,C.service_date),DATEPART(MM,C.service_date)) = '' THEN NULL ELSE CONCAT(DATEPART(YYYY,C.service_date),DATEPART(MM,C.service_date)) END)*1.0/12 AS  RXACTIVITY365
	INTO #rx_activity_365d
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
	LEFT JOIN [$(SrcSchema)].[src_rx_claims] C 	 WITH(NOLOCK) 
	ON A.patient_id=C.patient_id
	AND C.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	and C.days_supply>0
	GROUP BY A.patient_id,A.Valid_Index


Print 'Populate #dx_dctivity_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#dx_dctivity_365d') IS NOT NULL
DROP TABLE #dx_dctivity_365d

	SELECT Distinct A.patient_id,A.Valid_Index,COUNT(DISTINCT CASE WHEN CONCAT(DATEPART(YYYY,C.service_date),DATEPART(MM,C.service_date)) = '' THEN NULL ELSE CONCAT(DATEPART(YYYY,C.service_date),DATEPART(MM,C.service_date)) END)*1.0/12 AS DXACTIVITY365
	INTO #dx_dctivity_365d
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
	LEFT JOIN [$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] C 	 WITH(NOLOCK) 
	ON A.patient_id=C.patient_id
	AND C.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	GROUP BY A.patient_id,A.Valid_Index


Print 'Populate #HOSPACTIVITY365'
Print Getdate()

IF OBJECT_ID('tempdb..#HOSPACTIVITY365') IS NOT NULL
DROP TABLE #HOSPACTIVITY365

	SELECT Distinct A.patient_id,A.Valid_Index,COUNT(DISTINCT CASE WHEN CONCAT(DATEPART(YYYY, C.[FROM_DT]),DATEPART(MM, C.[FROM_DT])) = '' THEN NULL ELSE CONCAT(DATEPART(YYYY, C.[FROM_DT]),DATEPART(MM, C.[FROM_DT])) END)*1.0/12 AS  HOSPACTIVITY365
	INTO #HOSPACTIVITY365
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
	LEFT JOIN [$(SrcSchema)].[src_hosp_encounter] C 	 WITH(NOLOCK) 
	ON A.patient_id=C.patient_id
	AND C.[FROM_DT] BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	GROUP BY A.patient_id,A.Valid_Index


----******************************************Hosp encounter Features***********************************************----

Print 'Populate #HASHOSPENC365'
Print Getdate()

IF OBJECT_ID('tempdb..#HASHOSPENC365') IS NOT NULL
DROP TABLE #HASHOSPENC365

	SELECT Distinct A.patient_id,A.Valid_Index,CASE WHEN COUNT(DISTINCT [FROM_DT])>=1 THEN 1 ELSE 0 END AS  HASHOSPENC365
	INTO #HASHOSPENC365
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
	LEFT JOIN [$(SrcSchema)].[src_hosp_encounter] C 	 WITH(NOLOCK) 
	ON A.patient_id=C.patient_id
	AND C.[FROM_DT] BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	GROUP BY A.patient_id,A.Valid_Index


Print 'Populate #HASHOSPENC30'
Print Getdate()

IF OBJECT_ID('tempdb..#HASHOSPENC30') IS NOT NULL
DROP TABLE #HASHOSPENC30

	SELECT Distinct A.patient_id,A.Valid_Index,CASE WHEN COUNT(DISTINCT [FROM_DT])>=1 THEN 1 ELSE 0 END AS  HASHOSPENC30
	INTO #HASHOSPENC30
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
	LEFT JOIN [$(SrcSchema)].[src_hosp_encounter] C 	 WITH(NOLOCK) 
	ON A.patient_id=C.patient_id
	AND C.[FROM_DT] BETWEEN  DATEADD(DD,-30,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	GROUP BY A.patient_id,A.Valid_Index


----******************************************Procedure encounter Features***********************************************----

Print 'Populate #epilepsy_hosp_encounter_subset'
Print Getdate()

IF OBJECT_ID('tempdb..#epilepsy_hosp_encounter_subset') IS NOT NULL
DROP TABLE #epilepsy_hosp_encounter_subset

SELECT B.* 
INTO #epilepsy_hosp_encounter_subset
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
JOIN [$(SrcSchema)].[src_hosp_encounter] B 	 WITH(NOLOCK) 
ON A.patient_id=B.patient_id

IF OBJECT_ID('tempdb..#epilepsy_hosp_procedure_Subset') IS NOT NULL
DROP TABLE #epilepsy_hosp_procedure_Subset

SELECT * 
INTO  #epilepsy_hosp_procedure_Subset 
FROM [$(SrcSchema)].[src_hosp_procedure] 
WHERE visit_id IN (SELECT DISTINCT visit_id FROM #epilepsy_hosp_encounter_subset)

Print 'Populate #HASPROC30'
Print Getdate()
	
IF OBJECT_ID('tempdb..#HASPROC30') IS NOT NULL
DROP TABLE #HASPROC30
	
	SELECT A.patient_id,A.Valid_Index,CASE WHEN HASPROC30 >=1 THEN 1 ELSE 0 END AS HASPROC30
	INTO #HASPROC30
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A
	LEFT JOIN (
				SELECT  A.patient_id,A.Valid_Index,CASE WHEN COUNT(DISTINCT [PROC_DATE])>=1 THEN 1 ELSE 0 END AS  HASPROC30
				FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
				LEFT JOIN #epilepsy_hosp_encounter_subset C 	 WITH(NOLOCK) 
				ON A.patient_id=C.patient_id
				LEFT JOIN #epilepsy_hosp_procedure_Subset D
				ON C.Visit_id=D.Visit_id
				WHERE D.[PROC_DATE] BETWEEN  DATEADD(DD,-30,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
				GROUP BY A.patient_id,A.Valid_Index
			 ) B
	ON A.patient_id =B.patient_id
	AND A.Valid_Index =B.Valid_Index

Print 'Populate #HASPROC365'
Print Getdate()
	
IF OBJECT_ID('tempdb..#HASPROC365') IS NOT NULL
DROP TABLE #HASPROC365
	
	SELECT A.patient_id,A.Valid_Index,CASE WHEN HASPROC365 >=1 THEN 1 ELSE 0 END AS HASPROC365
	INTO #HASPROC365
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A
	LEFT JOIN (
				SELECT  A.patient_id,A.Valid_Index,CASE WHEN COUNT(DISTINCT [PROC_DATE])>=1 THEN 1 ELSE 0 END AS  HASPROC365
				FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
				LEFT JOIN #epilepsy_hosp_encounter_subset C 	 WITH(NOLOCK) 
				ON A.patient_id=C.patient_id
				LEFT JOIN #epilepsy_hosp_procedure_Subset D
				ON C.Visit_id=D.Visit_id
				WHERE D.[PROC_DATE] BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
				GROUP BY A.patient_id,A.Valid_Index
			 ) B
	ON A.patient_id =B.patient_id
	AND A.Valid_Index =B.Valid_Index


----******************************************Demographics Features***********************************************----
Print 'Populate #Demographics'
Print Getdate()

IF OBJECT_ID('tempdb..#Demographics') IS NOT NULL
DROP TABLE #Demographics

SELECT A.patient_id,Valid_Index,Gender, CAST(SUBSTRING(CAST(A.Valid_Index AS VARCHAR),1,4)  AS INT )- YOB Age 
		,Zip.[Zip1_code] AS zip1_
		,CASE  WHEN Zip.[Zip1_code] = 0 THEN 1 ELSE 0   END AS zip1_0 
		,CASE  WHEN Zip.[Zip1_code] = 1 THEN 1 ELSE 0 	END AS zip1_1 
		,CASE  WHEN Zip.[Zip1_code]	= 2 THEN 1 ELSE 0 	END AS zip1_2 
		,CASE  WHEN Zip.[Zip1_code]	= 3 THEN 1 ELSE 0 	END AS zip1_3 
		,CASE  WHEN Zip.[Zip1_code]	= 4 THEN 1 ELSE 0 	END AS zip1_4 
		,CASE  WHEN Zip.[Zip1_code]	= 5 THEN 1 ELSE 0 	END AS zip1_5 
		,CASE  WHEN Zip.[Zip1_code]	= 6 THEN 1 ELSE 0 	END AS zip1_6 
		,CASE  WHEN Zip.[Zip1_code]	= 7 THEN 1 ELSE 0 	END AS zip1_7 
		,CASE  WHEN Zip.[Zip1_code]	= 8 THEN 1 ELSE 0 	END AS zip1_8 
		,CASE  WHEN Zip.[Zip1_code]	= 9 THEN 1 ELSE 0 	END AS zip1_9 
INTO #Demographics
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A
LEFT JOIN [$(SrcSchema)].[src_ref_patient](nolock)  PP
ON  A.Patient_id =PP.patient_id
LEFT JOIN [$(TargetSchema)].[std_ref_USPS_zip3zip1codes](nolock) Zip
ON PP.Zip3 =Zip.Zip3_Code

----******************************************Combine all Features***********************************************----

Print 'Populate app_dre_features_Payer_Provider_Activity_Hospitalization_Demographics'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_Payer_Provider_Activity_Hospitalization_Demographics]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_Payer_Provider_Activity_Hospitalization_Demographics]

	SELECT A.patient_id,A.Valid_Index ,
	B.PAYER365_C,B.PAYER365_M,B.PAYER365_O,B.PAYER365_R,
	C.PAYERFIRSTAED_C,C.PAYERFIRSTAED_M,C.PAYERFIRSTAED_O, C.PAYERFIRSTAED_R,
	D.PHYSPECEMER,D.PHYSPECGENERAL,D.PHYSPECNEURO ,D.PHYSPECPAIN,
	E.RXACTIVITY365,F.DXACTIVITY365,G.HOSPACTIVITY365,
	H.HASHOSPENC365,I.HASHOSPENC30,
	J.HASPROC30,K.HASPROC365,
	L.Gender,L.Age,L.zip1_,L.zip1_0 ,L.zip1_1 ,L.zip1_2 ,L.zip1_3 ,L.zip1_4 ,L.zip1_5 ,L.zip1_6 ,L.zip1_7 ,L.zip1_8 ,L.zip1_9
	INTO [$(TargetSchema)].[app_dre_features_Payer_Provider_Activity_Hospitalization_Demographics]
	from [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A
	left join #Payer_365d(nolock)		B
	ON A.patient_id   =  B.patient_id
	AND A.Valid_Index =  B.Valid_Index
	left join #PayerFirstAED_365d(nolock) C
	ON A.patient_id   =  C.patient_id
	AND A.Valid_Index =  C.Valid_Index
	left join #PhyFirstAED_365d(nolock) D
	ON A.patient_id   =  D.patient_id
	AND A.Valid_Index =  D.Valid_Index
	left join #rx_activity_365d(nolock) E
	ON A.patient_id   =  E.patient_id
	AND A.Valid_Index =  E.Valid_Index
	left join #dx_dctivity_365d(nolock) F
	ON A.patient_id   =  F.patient_id
	AND A.Valid_Index =  F.Valid_Index
	left join #HOSPACTIVITY365(nolock)	G
	ON A.patient_id   =  G.patient_id
	AND A.Valid_Index =  G.Valid_Index
	left join #HASHOSPENC365(nolock)	H
	ON A.patient_id   =  H.patient_id
	AND A.Valid_Index =  H.Valid_Index
	left join #HASHOSPENC30(nolock)		I
	ON A.patient_id   =  I.patient_id
	AND A.Valid_Index =  I.Valid_Index
	left join #HASPROC30(nolock)		J
	ON A.patient_id   =  J.patient_id
	AND A.Valid_Index =  J.Valid_Index
	left join #HASPROC365(nolock)		K
	ON A.patient_id   =  K.patient_id
	AND A.Valid_Index =  K.Valid_Index
	left join #Demographics(nolock)		L
	ON A.patient_id   =  L.patient_id
	AND A.Valid_Index =  L.Valid_Index