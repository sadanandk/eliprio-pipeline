---- ------------------------------------------------------------------
-- - AUTHOR    : Priti P
-- - USED BY   : 
-- - PURPOSE   : Drop all tables related to DRE features
-- - NOTE      : 
-- -			 	 
--				 
-- - Execution : 
-- - Date      : 22-05-2019	
-- - Change History:	
-- - Change Date				
-- - Change Description
 					
-- ---------------------------------------------------------------------- 

IF OBJECT_ID('[$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts

IF OBJECT_ID('[$(TargetSchema)].app_dre_int_Cohort_Monotherapy') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy

IF OBJECT_ID('[$(TargetSchema)].app_dre_Cohort') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_dre_Cohort

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_Demographic]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_Demographic]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_DX_Claims]') IS Not NULL 
Drop Table [$(TargetSchema)].[app_dre_int_DX_Claims]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_DX_Claims_Pivot]') IS NOT NULL 
Drop Table [$(TargetSchema)].[app_dre_int_DX_Claims_Pivot]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_DX_180D_Within_IndexDate]') IS NOT NULL 
DROP TABLE[$(TargetSchema)].[app_dre_features_DX_180D_Within_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_DX_180D_After_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_DX_180D_After_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_Proc_Claims]') IS NOT NULL 
Drop Table [$(TargetSchema)].[app_dre_int_Proc_Claims]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_Proc_Claims_Pivot]') IS NOT NULL 
Drop Table [$(TargetSchema)].[app_dre_int_Proc_Claims_Pivot]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_Proc_180D_Within_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_Proc_180D_Within_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_Proc_180D_After_IndexDate]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_Proc_180D_After_IndexDate]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot]') IS Not NULL 
Drop Table [$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_CCI_ECI]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_CCI_ECI]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_Payer_Treatment_Provider]') IS NOT NULL 
    DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_Payer_Treatment_Provider]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_USC_Others_Epi_Code]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_USC_Others_Epi_Code]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_MPR_USC]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_MPR_USC]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_USC_List_Feature_365d_ADD_INDEX]') IS NOT NULL 
Drop table [$(TargetSchema)].[app_dre_int_USC_List_Feature_365d_ADD_INDEX]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_USC_List_Feature_180d_ADD_INDEX]') IS NOT NULL 
Drop table [$(TargetSchema)].[app_dre_int_USC_List_Feature_180d_ADD_INDEX]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_Features_IndexDate_PDC_USC]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_Features_IndexDate_PDC_USC]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_DX]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_DX]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_Features_IndexDate_Proc]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_Features_IndexDate_Proc]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_Payer_Provider_Activity_Hospitalization_Demographics]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_Payer_Provider_Activity_Hospitalization_Demographics]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_Comorbidity]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_Comorbidity]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_USP_Class]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_USP_Class]

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_CCI_ECI]') IS NOT NULL
DROP TABLE [$(TargetSchema)].[app_dre_features_CCI_ECI]