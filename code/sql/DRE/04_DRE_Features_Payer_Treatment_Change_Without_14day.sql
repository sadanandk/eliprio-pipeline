-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera. 
-- - USED BY   : 
-- - PURPOSE   : Create the table to stored the DRE features related to Payer , Treatment_change and Provider Speciality 
--				 
--				 
-- - Execution : 
-- - Date      : 23-Apr-2018		
		
	-- ---------------------------------------------------------------------- 
--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate #Payer_365d'
Print Getdate()

IF OBJECT_ID('tempdb..#Payer_365d') IS NOT NULL
DROP TABLE #Payer_365d

	SELECT A.patient_id,A.Valid_Index,
	   SUM(CASE WHEN B.Payer_Type = 'C' THEN 1 ELSE 0 END) AS Payer_Third_Party_Claim_365d_C,
	   SUM(CASE WHEN B.Payer_Type = 'M' THEN 1 ELSE 0 END) AS Payer_Medicaid_Claim_365d_M,
	   SUM(CASE WHEN B.Payer_Type = 'O' THEN 1 ELSE 0 END) AS Payer_Cash_Claim_365d_O,
	   SUM(CASE WHEN B.Payer_Type = 'R' THEN 1 ELSE 0 END) AS Payer_Medicare_Claim_365d_R
	INTO #Payer_365d
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
	LEFT JOIN [$(SrcSchema)].[src_rx_claims] B 	 WITH(NOLOCK)
	ON A.patient_id=B.patient_id
	AND B.Service_Date BETWEEN  DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND B.Days_Supply >0
	GROUP BY A.patient_id,A.Valid_Index

	Print 'Populate #Payer_180d'
	Print Getdate()


	IF OBJECT_ID('tempdb..#Payer_180d') IS NOT NULL
    DROP TABLE #Payer_180d

	SELECT A.patient_id,A.Valid_Index,
	   SUM(CASE WHEN C.Payer_Type = 'C' THEN 1 ELSE 0 END) AS Payer_Third_Party_Claim_180d_C,
	   SUM(CASE WHEN C.Payer_Type = 'M' THEN 1 ELSE 0 END) AS Payer_Medicaid_Claim_180d_M,
	   SUM(CASE WHEN C.Payer_Type = 'O' THEN 1 ELSE 0 END) AS Payer_Cash_Claim_180d_O,
	   SUM(CASE WHEN C.Payer_Type = 'R' THEN 1 ELSE 0 END) AS Payer_Medicare_Claim_180d_R
	   INTO #Payer_180d
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] A 	 WITH(NOLOCK)
	LEFT JOIN [$(SrcSchema)].[src_rx_claims] C 	 WITH(NOLOCK)
	ON A.patient_id=C.patient_id
	AND C.Service_Date BETWEEN  DATEADD(DD,-180,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
	AND C.Days_Supply >0 
	GROUP BY A.patient_id,A.Valid_Index


 
	---------------------------------
	Print 'Populate #Provider_Speciality'
	Print Getdate()
		
	IF OBJECT_ID('tempdb..#Provider_Speciality') IS NOT NULL
    DROP TABLE #Provider_Speciality
	SELECT patient_id,Valid_Index
		   ,specBf_Cnum_Percode_Ep0_365d_CL
		   ,specBf_Cnum_Percode_Ep0_365d_UPS
		   ,specBf_Cnum_Percode_Ep0_365d_C
		   ,specBf_Cnum_Percode_Ep0_365d_PT
		   ,specBf_Cnum_Percode_Ep0_365d_282N00000X
		   ,specBf_Cnum_Percode_Ep0_365d_291U00000X
		   ,specBf_Cnum_Percode_Ep0_365d_332B00000X
		   ,specBf_Cnum_Percode_Ep0_365d_3336C0003X
		   ,specBf_Cnum_Percode_Ep0_365d_AI
		   ,specBf_Cnum_Percode_Ep0_365d_AN
		   ,specBf_Cnum_Percode_Ep0_365d_APM
		   ,specBf_Cnum_Percode_Ep0_365d_CCM
		   ,specBf_Cnum_Percode_Ep0_365d_CD
		   ,specBf_Cnum_Percode_Ep0_365d_CHN
		   ,specBf_Cnum_Percode_Ep0_365d_CHP
		   ,specBf_Cnum_Percode_Ep0_365d_CN
		   ,specBf_Cnum_Percode_Ep0_365d_D
		   ,specBf_Cnum_Percode_Ep0_365d_DEN
		   ,specBf_Cnum_Percode_Ep0_365d_DR
		   ,specBf_Cnum_Percode_Ep0_365d_EM
		   ,specBf_Cnum_Percode_Ep0_365d_END
		   ,specBf_Cnum_Percode_Ep0_365d_FM
		   ,specBf_Cnum_Percode_Ep0_365d_FP
		   ,specBf_Cnum_Percode_Ep0_365d_GE
		   ,specBf_Cnum_Percode_Ep0_365d_GP
		   ,specBf_Cnum_Percode_Ep0_365d_GS
		   ,specBf_Cnum_Percode_Ep0_365d_GYN
		   ,specBf_Cnum_Percode_Ep0_365d_HEM
		   ,specBf_Cnum_Percode_Ep0_365d_HO
		   ,specBf_Cnum_Percode_Ep0_365d_HS
		   ,specBf_Cnum_Percode_Ep0_365d_IC
		   ,specBf_Cnum_Percode_Ep0_365d_ICE
		   ,specBf_Cnum_Percode_Ep0_365d_ID
		   ,specBf_Cnum_Percode_Ep0_365d_IM
		   ,specBf_Cnum_Percode_Ep0_365d_IMG
		   ,specBf_Cnum_Percode_Ep0_365d_MPD
		   ,specBf_Cnum_Percode_Ep0_365d_N
		   ,specBf_Cnum_Percode_Ep0_365d_NEP
		   ,specBf_Cnum_Percode_Ep0_365d_NM
		   ,specBf_Cnum_Percode_Ep0_365d_NS
		   ,specBf_Cnum_Percode_Ep0_365d_OBG
		   ,specBf_Cnum_Percode_Ep0_365d_ON
		   ,specBf_Cnum_Percode_Ep0_365d_OPH
		   ,specBf_Cnum_Percode_Ep0_365d_OPT
		   ,specBf_Cnum_Percode_Ep0_365d_ORS
		   ,specBf_Cnum_Percode_Ep0_365d_OS
		   ,specBf_Cnum_Percode_Ep0_365d_OTO
		   ,specBf_Cnum_Percode_Ep0_365d_P
		   ,specBf_Cnum_Percode_Ep0_365d_PCC
		   ,specBf_Cnum_Percode_Ep0_365d_PD
		   ,specBf_Cnum_Percode_Ep0_365d_PM
		   ,specBf_Cnum_Percode_Ep0_365d_PME
		   ,specBf_Cnum_Percode_Ep0_365d_POD
		   ,specBf_Cnum_Percode_Ep0_365d_PS
		   ,specBf_Cnum_Percode_Ep0_365d_PTH
		   ,specBf_Cnum_Percode_Ep0_365d_PUD
		   ,specBf_Cnum_Percode_Ep0_365d_R
		   ,specBf_Cnum_Percode_Ep0_365d_RHU
		   ,specBf_Cnum_Percode_Ep0_365d_RNR
		   ,specBf_Cnum_Percode_Ep0_365d_RO
		   ,specBf_Cnum_Percode_Ep0_365d_TS
		   ,specBf_Cnum_Percode_Ep0_365d_U
		   ,specBf_Cnum_Percode_Ep0_365d_UNK
		   ,specBf_Cnum_Percode_Ep0_365d_US
		   ,specBf_Cnum_Percode_Ep0_365d_VIR
		   ,specBf_Cnum_Percode_Ep0_365d_VS
		   ,specBf_Cnum_Percode_Ep0_365d_XXX
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_CL			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_CL
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_UPS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_UPS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_C			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_C
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PT			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PT
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_282N00000X	>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_282N00000X
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_291U00000X	>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_291U00000X
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_332B00000X	>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_332B00000X
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_3336C0003X	>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_3336C0003X
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_AI			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_AI
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_AN			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_AN
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_APM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_APM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_CCM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_CCM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_CD			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_CD
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_CHN			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_CHN
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_CHP			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_CHP
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_CN			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_CN
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_D			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_D
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_DEN			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_DEN
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_DR			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_DR
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_EM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_EM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_END			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_END
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_FM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_FM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_FP			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_FP
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_GE			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_GE
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_GP			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_GP
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_GS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_GS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_GYN			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_GYN
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_HEM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_HEM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_HO			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_HO
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_HS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_HS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_IC			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_IC
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_ICE			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_ICE
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_ID			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_ID
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_IM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_IM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_IMG			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_IMG
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_MPD			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_MPD
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_N			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_N
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_NEP			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_NEP
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_NM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_NM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_NS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_NS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_OBG			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_OBG
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_ON			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_ON
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_OPH			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_OPH
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_OPT			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_OPT
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_ORS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_ORS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_OS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_OS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_OTO			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_OTO
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_P			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_P
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PCC			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PCC
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PD			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PD
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PM			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PM
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PME			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PME
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_POD			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_POD
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PTH			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PTH
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_PUD			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_PUD
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_R			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_R
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_RHU			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_RHU
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_RNR			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_RNR
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_RO			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_RO
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_TS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_TS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_U			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_U
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_UNK			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_UNK
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_US			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_US
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_VIR			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_VIR
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_VS			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_VS
		   ,CAST(CASE WHEN specBf_Cnum_Percode_Ep0_365d_XXX			>=1 THEN 1 ELSE 0 END AS BIT) AS specBf_Cany_Percode_Ep0_365d_XXX
INTO #Provider_Speciality
FROM 
(
SELECT 
       A.[patient_id]
	  ,A.Valid_Index
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CLINICAL LABORATORY'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_CL
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='UNKNOWN PHYSICIAN SPECIALTY'					THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_UPS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CHIROPRACTOR'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_C
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PHYSICAL THERAPIST'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PT
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_CD]   ='282N00000X'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_282N00000X
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_CD]   ='291U00000X'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_291U00000X
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_CD]   ='332B00000X'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_332B00000X
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_CD]   ='3336C0003X'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_3336C0003X
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='ALLERGY & IMMUNOLOGY'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_AI
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='ANESTHESIOLOGY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_AN
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PAIN MEDICINE (ANESTHESIOLOGY)'				THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_APM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CRITICAL CARE MEDICINE (INTERNAL MEDICINE)'	THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_CCM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CARDIOVASCULAR DISEASE'						THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_CD
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CHILD NEUROLOGY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_CHN
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CHILD AND ADOLESCENT PSYCHIATRY'				THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_CHP
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CLINICAL NEUROPHYSIOLOGY'						THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_CN
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='DERMATOLOGY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_D
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='DENTISTRY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_DEN
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='DIAGNOSTIC RADIOLOGY'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_DR
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='EMERGENCY MEDICINE'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_EM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='ENDOCRINOLOGY, DIABETES & METABOLISM'			THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_END
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='FAMILY MEDICINE'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_FM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='FAMILY PRACTICE'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_FP
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='GASTROENTEROLOGY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_GE
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='GENERAL PRACTICE'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_GP
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='GENERAL SURGERY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_GS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='GYNECOLOGY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_GYN
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='HEMATOLOGY (INTERNAL MEDICINE)'				THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_HEM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='HEMATOLOGY/ONCOLOGY'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_HO
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='HAND SURGERY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_HS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='INTERVENTIONAL CARDIOLOGY'					THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_IC
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='CLINICAL CARDIAC ELECTROPHYSIOLOGY'			THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_ICE
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='INFECTIOUS DISEASES'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_ID
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='INTERNAL MEDICINE'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_IM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='GERIATRIC MEDICINE (INTERNAL MEDICINE)'		THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_IMG
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='INTERNAL MEDICINE/PEDIATRICS'					THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_MPD
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='NEUROLOGY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_N
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='NEPHROLOGY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_NEP
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='NUCLEAR MEDICINE'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_NM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='NEUROLOGICAL SURGERY'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_NS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='OBSTETRICS & GYNECOLOGY'						THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_OBG
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='MEDICAL ONCOLOGY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_ON
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='OPHTHALMOLOGY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_OPH
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='OPTOMETRY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_OPT
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='ORTHOPEDIC SURGERY'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_ORS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='OTHER SPECIALTY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_OS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='OTOLARYNGOLOGY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_OTO
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PSYCHIATRY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_P
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PULMONARY CRITICAL CARE MEDICINE'				THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PCC
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PEDIATRICS'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PD
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PHYSICAL MEDICINE & REHABILITATION'			THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PM
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PAIN MANAGEMENT'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PME
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PODIATRY'										THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_POD
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PLASTIC SURGERY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='ANATOMIC/CLINICAL PATHOLOGY'					THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PTH
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='PULMONARY DISEASE'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_PUD
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='RADIOLOGY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_R
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='RHEUMATOLOGY'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_RHU
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='NEURORADIOLOGY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_RNR
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='RADIATION ONCOLOGY'							THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_RO
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='THORACIC SURGERY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_TS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='UROLOGY'										THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_U
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='UNKNOWN'										THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_UNK
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='UNSPECIFIED'									THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_US
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='VASCULAR AND INTERVENTIONAL RADIOLOGY'		THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_VIR
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='VASCULAR SURGERY'								THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_VS
      ,SUM(CASE WHEN C.[PRI_SPECIALTY_DESC] ='NULL FIELD CORRECTED BY MEDIC'				THEN 1 ELSE 0 END) AS specBf_Cnum_Percode_Ep0_365d_XXX
FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic](nolock) A
LEFT JOIN  [$(SrcSchema)].[src_rx_claims] (NOLOCK)B
ON A.patient_id=B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index) 
LEFT JOIN  [$(SrcSchema)].[src_ref_Provider](NOLOCK) C
ON B.Provider_id = C.Provider_id
AND B.Days_Supply > 0
GROUP BY  A.[patient_id],A.Valid_Index
  ) A

------------------------------------------------------
	Print 'Populate app_dre_features_IndexDate_Payer_Treatment_Provider'
	Print Getdate()

  	IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_Payer_Treatment_Provider]') IS NOT NULL 
    DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_Payer_Treatment_Provider]

    SELECT A.patient_id,A.Valid_Index
	,Payer_Third_Party_Claim_365d_C
	,Payer_Medicaid_Claim_365d_M
	,Payer_Cash_Claim_365d_O
	,Payer_Medicare_Claim_365d_R
	,Payer_Third_Party_Claim_180d_C
	,Payer_Medicaid_Claim_180d_M
	,Payer_Cash_Claim_180d_O
	,Payer_Medicare_Claim_180d_R
	,specBf_Cany_Percode_Ep0_365d_CL
	,specBf_Cany_Percode_Ep0_365d_UPS
	,specBf_Cany_Percode_Ep0_365d_C
	,specBf_Cany_Percode_Ep0_365d_PT
	,specBf_Cany_Percode_Ep0_365d_282N00000X
	,specBf_Cany_Percode_Ep0_365d_291U00000X
	,specBf_Cany_Percode_Ep0_365d_332B00000X
	,specBf_Cany_Percode_Ep0_365d_3336C0003X
	,specBf_Cany_Percode_Ep0_365d_AI
	,specBf_Cany_Percode_Ep0_365d_AN
	,specBf_Cany_Percode_Ep0_365d_APM
	,specBf_Cany_Percode_Ep0_365d_CCM
	,specBf_Cany_Percode_Ep0_365d_CD
	,specBf_Cany_Percode_Ep0_365d_CHN
	,specBf_Cany_Percode_Ep0_365d_CHP
	,specBf_Cany_Percode_Ep0_365d_CN
	,specBf_Cany_Percode_Ep0_365d_D
	,specBf_Cany_Percode_Ep0_365d_DEN
	,specBf_Cany_Percode_Ep0_365d_DR
	,specBf_Cany_Percode_Ep0_365d_EM
	,specBf_Cany_Percode_Ep0_365d_END
	,specBf_Cany_Percode_Ep0_365d_FM
	,specBf_Cany_Percode_Ep0_365d_FP
	,specBf_Cany_Percode_Ep0_365d_GE
	,specBf_Cany_Percode_Ep0_365d_GP
	,specBf_Cany_Percode_Ep0_365d_GS
	,specBf_Cany_Percode_Ep0_365d_GYN
	,specBf_Cany_Percode_Ep0_365d_HEM
	,specBf_Cany_Percode_Ep0_365d_HO
	,specBf_Cany_Percode_Ep0_365d_HS
	,specBf_Cany_Percode_Ep0_365d_IC
	,specBf_Cany_Percode_Ep0_365d_ICE
	,specBf_Cany_Percode_Ep0_365d_ID
	,specBf_Cany_Percode_Ep0_365d_IM
	,specBf_Cany_Percode_Ep0_365d_IMG
	,specBf_Cany_Percode_Ep0_365d_MPD
	,specBf_Cany_Percode_Ep0_365d_N
	,specBf_Cany_Percode_Ep0_365d_NEP
	,specBf_Cany_Percode_Ep0_365d_NM
	,specBf_Cany_Percode_Ep0_365d_NS
	,specBf_Cany_Percode_Ep0_365d_OBG
	,specBf_Cany_Percode_Ep0_365d_ON
	,specBf_Cany_Percode_Ep0_365d_OPH
	,specBf_Cany_Percode_Ep0_365d_OPT
	,specBf_Cany_Percode_Ep0_365d_ORS
	,specBf_Cany_Percode_Ep0_365d_OS
	,specBf_Cany_Percode_Ep0_365d_OTO
	,specBf_Cany_Percode_Ep0_365d_P
	,specBf_Cany_Percode_Ep0_365d_PCC
	,specBf_Cany_Percode_Ep0_365d_PD
	,specBf_Cany_Percode_Ep0_365d_PM
	,specBf_Cany_Percode_Ep0_365d_PME
	,specBf_Cany_Percode_Ep0_365d_POD
	,specBf_Cany_Percode_Ep0_365d_PS
	,specBf_Cany_Percode_Ep0_365d_PTH
	,specBf_Cany_Percode_Ep0_365d_PUD
	,specBf_Cany_Percode_Ep0_365d_R
	,specBf_Cany_Percode_Ep0_365d_RHU
	,specBf_Cany_Percode_Ep0_365d_RNR
	,specBf_Cany_Percode_Ep0_365d_RO
	,specBf_Cany_Percode_Ep0_365d_TS
	,specBf_Cany_Percode_Ep0_365d_U
	,specBf_Cany_Percode_Ep0_365d_UNK
	,specBf_Cany_Percode_Ep0_365d_US
	,specBf_Cany_Percode_Ep0_365d_VIR
	,specBf_Cany_Percode_Ep0_365d_VS
	,specBf_Cany_Percode_Ep0_365d_XXX
	,specBf_Cnum_Percode_Ep0_365d_CL
	,specBf_Cnum_Percode_Ep0_365d_UPS
	,specBf_Cnum_Percode_Ep0_365d_C
	,specBf_Cnum_Percode_Ep0_365d_PT
	,specBf_Cnum_Percode_Ep0_365d_282N00000X
	,specBf_Cnum_Percode_Ep0_365d_291U00000X
	,specBf_Cnum_Percode_Ep0_365d_332B00000X
	,specBf_Cnum_Percode_Ep0_365d_3336C0003X
	,specBf_Cnum_Percode_Ep0_365d_AI
	,specBf_Cnum_Percode_Ep0_365d_AN
	,specBf_Cnum_Percode_Ep0_365d_APM
	,specBf_Cnum_Percode_Ep0_365d_CCM
	,specBf_Cnum_Percode_Ep0_365d_CD
	,specBf_Cnum_Percode_Ep0_365d_CHN
	,specBf_Cnum_Percode_Ep0_365d_CHP
	,specBf_Cnum_Percode_Ep0_365d_CN
	,specBf_Cnum_Percode_Ep0_365d_D
	,specBf_Cnum_Percode_Ep0_365d_DEN
	,specBf_Cnum_Percode_Ep0_365d_DR
	,specBf_Cnum_Percode_Ep0_365d_EM
	,specBf_Cnum_Percode_Ep0_365d_END
	,specBf_Cnum_Percode_Ep0_365d_FM
	,specBf_Cnum_Percode_Ep0_365d_FP
	,specBf_Cnum_Percode_Ep0_365d_GE
	,specBf_Cnum_Percode_Ep0_365d_GP
	,specBf_Cnum_Percode_Ep0_365d_GS
	,specBf_Cnum_Percode_Ep0_365d_GYN
	,specBf_Cnum_Percode_Ep0_365d_HEM
	,specBf_Cnum_Percode_Ep0_365d_HO
	,specBf_Cnum_Percode_Ep0_365d_HS
	,specBf_Cnum_Percode_Ep0_365d_IC
	,specBf_Cnum_Percode_Ep0_365d_ICE
	,specBf_Cnum_Percode_Ep0_365d_ID
	,specBf_Cnum_Percode_Ep0_365d_IM
	,specBf_Cnum_Percode_Ep0_365d_IMG
	,specBf_Cnum_Percode_Ep0_365d_MPD
	,specBf_Cnum_Percode_Ep0_365d_N
	,specBf_Cnum_Percode_Ep0_365d_NEP
	,specBf_Cnum_Percode_Ep0_365d_NM
	,specBf_Cnum_Percode_Ep0_365d_NS
	,specBf_Cnum_Percode_Ep0_365d_OBG
	,specBf_Cnum_Percode_Ep0_365d_ON
	,specBf_Cnum_Percode_Ep0_365d_OPH
	,specBf_Cnum_Percode_Ep0_365d_OPT
	,specBf_Cnum_Percode_Ep0_365d_ORS
	,specBf_Cnum_Percode_Ep0_365d_OS
	,specBf_Cnum_Percode_Ep0_365d_OTO
	,specBf_Cnum_Percode_Ep0_365d_P
	,specBf_Cnum_Percode_Ep0_365d_PCC
	,specBf_Cnum_Percode_Ep0_365d_PD
	,specBf_Cnum_Percode_Ep0_365d_PM
	,specBf_Cnum_Percode_Ep0_365d_PME
	,specBf_Cnum_Percode_Ep0_365d_POD
	,specBf_Cnum_Percode_Ep0_365d_PS
	,specBf_Cnum_Percode_Ep0_365d_PTH
	,specBf_Cnum_Percode_Ep0_365d_PUD
	,specBf_Cnum_Percode_Ep0_365d_R
	,specBf_Cnum_Percode_Ep0_365d_RHU
	,specBf_Cnum_Percode_Ep0_365d_RNR
	,specBf_Cnum_Percode_Ep0_365d_RO
	,specBf_Cnum_Percode_Ep0_365d_TS
	,specBf_Cnum_Percode_Ep0_365d_U
	,specBf_Cnum_Percode_Ep0_365d_UNK
	,specBf_Cnum_Percode_Ep0_365d_US
	,specBf_Cnum_Percode_Ep0_365d_VIR
	,specBf_Cnum_Percode_Ep0_365d_VS
	,specBf_Cnum_Percode_Ep0_365d_XXX

	INTO [$(TargetSchema)].[app_dre_features_IndexDate_Payer_Treatment_Provider]
	FROM #Payer_365d A
	JOIN #Payer_180d B
	ON		A.patient_id		=	B.patient_id
	AND		A.Valid_Index	=	B.Valid_Index 
	JOIN #Provider_Speciality E
	ON		A.patient_id		=	E.patient_id
	AND		A.Valid_Index	=	E.Valid_Index 


---------------------------------------
