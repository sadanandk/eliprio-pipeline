 
-- ---------------------------------------------------------------------
-- - AUTHOR			: Suresh Gajera
-- - USED BY		: 
-- - 
-- - PURPOSE		: Cohort creation for DRE Model do not use 14 days validate rule table for cohort creation
-- - NOTE			:  remove molecule with aed_days <=30 and then creat DRE cohort.
-- - Execution		: 
-- - Date			: 18-Apr-2018			
-- - Tables Required: [$(TargetSchema)].[PatientProfile], [$(TargetSchema)].[epi_dx_claims],[$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] ,
-- - OutPut Table	: [$(TargetSchema)].app_dre_int_Cohort_Monotherapy
-- - Change History:	Change Date				Change Description
-- -					20-Apr-2018				Change the code from 780.3* to 78039 and R56.* to R569
-- -					20-Apr-2018				Updated column Outcome_Variable and Patient_Exclusion_Flag based on new logic of Distinct AED failure count.
-- -					24-Apr-2018				Rename the column putcome_Variable to Minimum_3Failure_Flag
-- -					27-Apr-2018				ELREFPRED-224 - New defination of refractory and non refractory patient classification,Considering only patients has 1 yr pf history.
-- -					07-May-2018				Added Outcome_Variable column
-- -					04-Jun-2018				Change the cohort index logic with first 60days aed as index.ELREFPRED-341
-- -					13-Jun-2018				Implemented Q_Rx_Elg as per the new logic: ELREFPRED-373
-- -					27-Jun-2018				Iteration 4 changes consider records till last Q Rx eligibility =1
-- -					28-Jun-2018				Resolve issue for ELREFPRED-473 (Q_rx_elg) 
-- -					31-Jul-2018				Added column DS_0_LessThan_0 to identify patients which has days supply <=0
-- -					04-Oct-2018				Updated logic for column [Second_780.39]
-- -					06-Oct-2018				Iteration 5 : (ELREFPRED-704) Implementation of days_supply >= 0 for First_Rx_Date and Last_Rx_Date column & adding lookforward period of one year as inclussion criteria 
-- -					04-Oct-2018				Added inclusion criteria Lookforward_Present = 1 [ELREFPRED-717]
-- -					26-Feb-2019				Update Lookforward_Present = 1 on max(Rx,Dx,proc date) and Lookback_Present = 1 on min(Rx,Dx,proc date)
---						15-Mar-2019				ICD code updated from 7803% to 78039
-- -                    05-Apr-2019				ELCDS-1401 : Iteration4.2 : Filtered out the patients having overlapping diagnosis codes.
-- -			        03-May-2019				ELREFPRED-1227: Remove 780.31, 780.32 and R56.0* Code
-- -					11-June-2019			PLATFORM-1542: Updated logic to identify Polytherapy record on index date
-- ---------------------------------------------------------------------- 

--:setvar Schema SHA_Pipeline_All
--:Setvar SrcSchema SHA_Pipeline_All

Print 'Ignore patients with overlapping diagnosis codes'
Print Getdate()

IF OBJECT_ID('tempdb..#IgnorePatientsWithOverlap') IS NOT NULL
	DROP TABLE #IgnorePatientsWithOverlap

SELECT  DISTINCT Patient_id , DIAG1 INTO #IgnorePatientsWithOverlap
FROM [$(SrcSchema)].[src_dx_claims](NOLOCK) 
WHERE REPLACE(DIAG1,'.','') IN ('E8581','E8582','E8589') 


Print 'Populate #PatientProfile'
Print Getdate()

IF OBJECT_ID('tempdb..#PatientProfile') IS NOT NULL
DROP TABLE #PatientProfile

create table #PatientProfile
(patient_id varchar(20),
[YOB] INT,
[IS_345.x] DATE,
[First_780.39] DATE , 
[Second_780.39] DATE ,
[Last_780.39] DATE,
[Is_780.39] DATE,
[IsEpilepsy] INT,
[Q_Rx_Elg] INT
)


INSERT INTO #PatientProfile (patient_id,YOB,[IS_345.x],[First_780.39],[Second_780.39],[Last_780.39],[Is_780.39],[IsEpilepsy] )
SELECT DISTINCT patient_id,YOB,[IS_345.x]  as [IS_345.x],[First_780.3x] as [First_780.39],[Second_780.3x] as [Second_780.39]
,[Last_780.3x] as [Last_780.39],[Is_780.3x] as [Is_780.39],[IsEpilepsy]
FROM  [$(TargetSchema)].[app_int_Patient_Profile](NOLOCK) 
WHERE Patient_id IN (
SELECT DISTINCT Patient_id FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription](NOLOCK) 
WHERE DATEDIFF(DD,startdate,enddate)+1 > 30 AND PATIENT_ID NOT IN (SELECT DISTINCT patient_id FROM #IgnorePatientsWithOverlap )
)  /*take only required patients with aed_days >30 and filtered out patients tha has overlapping IDC dagnosis codes*/
AND [IsEpilepsy]=1

/*
Print 'Populate #DX'
Print Getdate()

IF OBJECT_ID('tempdb..#DX') IS NOT NULL
DROP TABLE #DX
SELECT  patient_id,MIN(CONVERT(DATE,dx.service_date)) DATE
INTO #DX
FROM [$(SrcSchema)].[src_dx_claims] dx

WHERE 
replace(dx.DIAG1,'.','') like '345%' or replace(dx.DIAG2,'.','') like '345%' or replace(dx.DIAG3,'.','') like '345%' or replace(dx.DIAG4,'.','') like '345%' or
replace(dx.DIAG5,'.','') like '345%' or replace(dx.DIAG6,'.','') like '345%' or replace(dx.DIAG7,'.','') like '345%' or replace(dx.DIAG8,'.','') like '345%' or
replace(dx.DIAG1,'.','') like 'G40%' or replace(dx.DIAG2,'.','') like 'G40%' or replace(dx.DIAG3,'.','') like 'G40%' or replace(dx.DIAG4,'.','') like 'G40%' or
replace(dx.DIAG5,'.','') like 'G40%' or replace(dx.DIAG6,'.','') like 'G40%' or replace(dx.DIAG7,'.','') like 'G40%' or replace(dx.DIAG8,'.','') like 'G40%'  
GROUP BY patient_id

CREATE STATISTICS DXPAtient
ON #DX (patient_Id)

UPDATE #PatientProfile
SET [IS_345.x] = dx.date
FROM #dx dx
WHERE #PatientProfile.patient_id = dx.patient_id


-- First_78039 Second_78039
Print 'Populate #ConIntermediate'
Print getdate()

IF OBJECT_ID('tempdb..#ConIntermediate', 'U') IS NOT NULL
DROP TABLE #ConIntermediate

select distinct patient_id,dx.service_date, dx.DIAG1, dx.DIAG2, dx.DIAG3, dx.DIAG4, dx.DIAG5, dx.DIAG6, dx.DIAG7, dx.DIAG8
into #ConIntermediate
from  [$(SrcSchema)].[src_dx_claims]  dx with (nolock)
where  
replace(dx.DIAG1,'.','') like '7803%' or replace(dx.DIAG2,'.','') like '7803%' or replace(dx.DIAG3,'.','') like '7803%' or replace(dx.DIAG4,'.','') like '7803%' or
replace(dx.DIAG5,'.','') like '7803%' or replace(dx.DIAG6,'.','') like '7803%' or replace(dx.DIAG7,'.','') like '7803%' or replace(dx.DIAG8,'.','') like '7803%' or
replace(dx.DIAG1,'.','') like 'R56%'  or replace(dx.DIAG2,'.','') like 'R56%'  or replace(dx.DIAG3,'.','') like 'R56%'	or replace(dx.DIAG4,'.','') like 'R56%' or
replace(dx.DIAG5,'.','') like 'R56%'  or replace(dx.DIAG6,'.','') like 'R56%'  or replace(dx.DIAG7,'.','') like 'R56%'	or replace(dx.DIAG8,'.','') like 'R56%'
*/
--Remove 780.31, 780.32 and R56.0* Code and calculate First_78039 Second_78039*/
/*
Print 'Populate #DX1'
Print Getdate()

IF OBJECT_ID('tempdb..#DX1') IS NOT NULL
DROP TABLE #DX1
SELECT patient_id,service_date,
ROW_NUMBER() OVER(PARTITION BY patient_id ORDER BY patient_id,service_date)Num
INTO #DX1
FROM (
		SELECT DISTINCT  patient_id,CONVERT(DATE,dx.service_date) service_date
		FROM #ConIntermediate  dx WITH (NOLOCK)
		
		WHERE  
		replace(dx.DIAG1,'.','') NOT like '78031' or replace(dx.DIAG2,'.','') NOT like '78031' or replace(dx.DIAG3,'.','') NOT like '78031' or replace(dx.DIAG4,'.','') NOT like '78031' or
		replace(dx.DIAG5,'.','') NOT like '78031' or replace(dx.DIAG6,'.','') NOT like '78031' or replace(dx.DIAG7,'.','') NOT like '78031' or replace(dx.DIAG8,'.','') NOT like '78031' or
		replace(dx.DIAG1,'.','') NOT like '78032' or replace(dx.DIAG2,'.','') NOT like '78032' or replace(dx.DIAG3,'.','') NOT like '78032' or replace(dx.DIAG4,'.','') NOT like '78032' or
		replace(dx.DIAG5,'.','') NOT like '78032' or replace(dx.DIAG6,'.','') NOT like '78032' or replace(dx.DIAG7,'.','') NOT like '78032' or replace(dx.DIAG8,'.','') NOT like '78032' or
		replace(dx.DIAG1,'.','') NOT like 'R56.0%'  or replace(dx.DIAG2,'.','') NOT like 'R56.0%'  or replace(dx.DIAG3,'.','') NOT like 'R56.0%'	or replace(dx.DIAG4,'.','') NOT like 'R56.0%' or
		replace(dx.DIAG5,'.','') NOT like 'R56.0%'  or replace(dx.DIAG6,'.','') NOT like 'R56.0%'  or replace(dx.DIAG7,'.','') NOT like 'R56.0%'	or replace(dx.DIAG8,'.','') NOT like 'R56.0%'	

	 ) A


CREATE STATISTICS DXPAtient
ON #DX1 (patient_Id)

UPDATE #PatientProfile
SET [First_780.39] = dx.service_date
FROM #dx1 dx
WHERE #PatientProfile.patient_id = dx.patient_id
AND dx.Num=1

UPDATE #PatientProfile
SET [Second_780.39] = dx.service_date
FROM #dx1 dx
WHERE #PatientProfile.patient_id = dx.patient_id
AND dx.Num=2


-- Last_780.3*
IF OBJECT_ID('tempdb..#MAx') IS NOT NULL
DROP TABLE #MAx 

SELECT DISTINCT patient_id,MAX(Num)Num
INTO #MAx
FROM #DX1
GROUP BY patient_id


IF OBJECT_ID('tempdb..#Last') IS NOT NULL
DROP TABLE #Last

SELECT D.patient_id,D.service_date,M.Num
INTO #Last
FROM #Max M
LEFT JOIN #DX1 D ON M.patient_id=D.patient_id AND M.Num=D.Num

UPDATE #PatientProfile
SET [Last_780.39] = dx.service_date
FROM #Last dx
WHERE #PatientProfile.patient_id = dx.patient_id

--select * from #PatientProfile where [Last_780.3x] is not null

IF OBJECT_ID('tempdb..#Temp') IS NOT NULL
DROP TABLE #Temp

SELECT DISTINCT patient_id, [Second_780.39]
INTO #Temp
FROM #PatientProfile
WHERE [Second_780.39] IS NOT NULL --AND  ([First_780.39] <> [Second_780.39] or [First_780.39] <> [Last_780.39])

UPDATE #PatientProfile
SET [Is_780.39] = T.[Second_780.39]
FROM #temp T
WHERE #PatientProfile.patient_id =T.patient_id


UPDATE #PatientProfile
SET IsEpilepsy = 1
WHERE [IS_345.x] IS NOT NULL OR [IS_780.39] IS NOT NULL

*/
/********************************************************************************************************/

--------------------Finding patints first aed with duration greated than 60 days--------------------------

Print 'Finding patints first aed with duration greated than 60 days'
Print Getdate()

IF OBJECT_ID('tempdb..#First_AED') IS NOT NULL
DROP TABLE #First_AED

SELECT [patient_id]
       ,MIN([startdate]) AS First_AED 
INTO #First_AED
FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] WHERE DATEDIFF(DD,startdate,enddate)+1 >= 60
GROUP BY [patient_id]


-------finding age of patients at  first aed with 60days duration -------------------------
IF OBJECT_ID('tempdb..#FirstAED_Age') IS NOT NULL
DROP TABLE #FirstAED_Age

SELECT A.patient_id,A.First_AED,B.YOB,CAST(SUBSTRING(CAST(First_AED AS VARCHAR),1,4)  AS INT )- YOB AS  Age 
INTO #FirstAED_Age
FROM #First_AED A
JOIN #PatientProfile B
ON A.Patient_id=B.patient_id


----------list of patients starting as monotheraoy and with first aed having aed days greater than 60 days----------------------------------
Print 'Populate #MonoPatient'
Print Getdate()

IF OBJECT_ID('tempdb..#MonoPatient') IS NOT NULL
DROP TABLE #MonoPatient

SELECT A.patient_id,A.startdate
INTO #MonoPatient 
FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription]  A
JOIN  #FirstAED_Age B
ON A.Patient_id = B.patient_id
AND A.startdate = B.First_AED
WHERE DATEDIFF(DD,startdate,enddate)+1 > 30   /*To check if patients is on monotherapy or not even with less then 60 days supply but greater than 30 days of supply*/
GROUP BY  A.patient_id,A.startdate
HAVING COUNT(1)=1

-------------------------------Populate table for Pipelin Matrix--------------------------------------

Print 'Populate #AED_Failure_Flag'
Print Getdate()


IF OBJECT_ID('tempdb..#AED_Failure_Flag') IS NOT NULL
DROP TABLE #AED_Failure_Flag

select A.patient_id,DATEDIFF(DD,A.startdate,A.enddate)+1 as AED_Days , CASE WHEN DATEDIFF(DD,A.startdate,A.enddate)+1  <365 THEN 1 ELSE 0 END AS AED_Failure_Flag
INTO #AED_Failure_Flag
from [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] A
JOIN #MonoPatient B
ON A.patient_Id=B.patient_id
AND A.startdate = B.startdate
WHERE DATEDIFF(DD,A.startdate,A.enddate)+1 >= 60

IF OBJECT_ID('tempdb..#Distinct_No_Of_AED') IS NOT NULL
DROP TABLE #Distinct_No_Of_AED

SELECT DISTINCT A.Patient_id,COUNT(DISTINCT AED)  AS Distinct_No_Of_AED
INTO #Distinct_No_Of_AED
FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription]  A
JOIN #MonoPatient B
ON A.patient_id =B.patient_id 
AND A.startdate >= B.startdate 
WHERE DATEDIFF(DD,A.startdate,A.enddate)+1 > 30  
GROUP BY A.Patient_id

Print 'Populate #AtleastOneAed'
Print Getdate()


IF OBJECT_ID('tempdb..#AtleastOneAed') IS NOT NULL
DROP TABLE #AtleastOneAed

SELECT [patient_id]
       ,COUNT(DISTINCT AED) AS AtleastOneAed 
INTO #AtleastOneAed
FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] 
GROUP BY [patient_id]

Print 'Get minimum Dx, Rx and Proc code'
Print Getdate()


IF OBJECT_ID('tempdb..#MinRxDxProcServiceDate') IS NOT NULL
DROP TABLE #MinRxDxProcServiceDate

Select Patient_ID,Min(First_RX_Encounter) AS [FirstRxDxServiceDate], Max(Last_RX_Encounter) AS [LastRxDxServiceDate]
INTO #MinRxDxProcServiceDate
From [$(TargetSchema)].app_int_patient_Profile WITH(NOLOCK)
GROUP BY Patient_ID
UNION all
Select Patient_ID,Min(First_DX_Encounter) AS [FirstRxDxServiceDate], Max(Last_DX_Encounter) AS [LastRxDxServiceDate]
From [$(TargetSchema)].app_int_patient_Profile WITH(NOLOCK)
GROUP BY Patient_ID

IF OBJECT_ID('tempdb..#RxDxProcServiceDate') IS NOT NULL
DROP TABLE #RxDxProcServiceDate

SELECT Patient_ID,Min(FirstRxDxServiceDate) as FirstRxDxServiceDate, MAX([LastRxDxServiceDate]) AS [LastRxDxServiceDate]
INTO #RxDxProcServiceDate
FROM #MinRxDxProcServiceDate
GROUP BY Patient_ID

Print 'Populate app_dre_int_Pipeline_Metrix_Patient_Counts'
Print Getdate()


IF OBJECT_ID('[$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts

Select 
A.*,CASE WHEN B.AtleastOneAed IS NOT NULL THEN 1 ELSE 0 END AS AtleastOneAed ,C.Age,CASE WHEN D.patient_id IS NOT NULL THEN 1 ELSE 0 END AS Start_Mono,D.startdate as Valid_Index,Distinct_No_Of_AED,AED_Failure_Flag,
CASE WHEN Distinct_No_Of_AED < 4 THEN  CASE WHEN Distinct_No_Of_AED = 1 THEN CASE WHEN AED_Failure_Flag  = 0 THEN  0 ELSE 1 END ELSE 1 END ELSE 0 END AS Patient_Exclusion_Flag ,
Gender,First_RX_Encounter AS First_rx_date,Last_RX_Encounter AS Last_rx_date,First_DX_Encounter AS First_dx_date,Last_DX_Encounter AS Last_dx_date,RD.[FirstRxDxServiceDate] AS [FirstRxDxServiceDate],RD.[LastRxDxServiceDate] AS [LastRxDxServiceDate],CASE WHEN DATEDIFF(dd,RD.[FirstRxDxServiceDate],D.startdate)+1  >  365 THEN 1  ELSE 0 END AS Lookback_Present  ,CASE WHEN DATEDIFF(dd,D.startdate,RD.[LastRxDxServiceDate])+1  >  365 THEN 1  ELSE 0 END AS Lookforward_Present
INTO [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
FROM #PatientProfile A
LEFT JOIN #AtleastOneAed B
ON A.patient_id=B.patient_id
LEFT JOIN  #FirstAED_Age C
ON A.patient_id=C.patient_id
LEFT JOIN #MonoPatient D
ON A.Patient_id =D.Patient_id
LEFT JOIN #Distinct_No_Of_AED F
ON A.patient_id = F.Patient_id
LEFT JOIN #AED_Failure_Flag G
ON A.patient_id = G.patient_id
LEFT JOIN #RxDxProcServiceDate RD
ON A.patient_id = RD.patient_id
LEFT JOIN [$(TargetSchema)].app_int_patient_Profile H
ON A.patient_id =H.Patient_id




--Constraint-V : Quarterly Rx Eligibility
--A Quarterly Rx Eligibility indicates that in a patient history, each quarter has RX_ACTIVITY flag set to 'Y'  at least once.

--##Identifying quarters for each record of the patient 

IF OBJECT_ID('tempdb..#Temp1') IS NOT NULL
DROP TABLE #Temp1

SELECT *,SUBSTRING(CAST(MONTH_ID AS VARCHAR),1,4) AS YEAR_, SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) AS MONTH_ ,
                     CASE WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('01','02','03') THEN 'Q1'--,
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('04','05','06') THEN 'Q2'--,
                           WHEN SUBSTRING(CAST(MONTH_ID AS VARCHAR),5,2) IN ('07','08','09') THEN 'Q3'--,
                           ELSE 'Q4' END AS QUARTER_INFO
INTO #Temp1
FROM [$(SrcSchema)].[src_ptnt_eligibility] 


IF OBJECT_ID('tempdb..#IndexQuarterNo') IS NOT NULL
DROP TABLE #IndexQuarterNo

select DISTINCT A.PATIENT_ID,CAST([Sr_No_Q] AS INT) AS [Sr_No_Q]
INTO #IndexQuarterNo
from #temp1 A
JOIN [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts B
ON A.patient_id = B.patient_id
AND A.YEAR_+A.Month_=REPLACE(SUBSTRING(CAST(B.Valid_index AS Varchar),1,7),'-','')
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]


IF OBJECT_ID('tempdb..#preIndex') IS NOT NULL
DROP TABLE #preIndex

select A.PATIENT_ID,YEAR_,QUARTER_INFO,CAST(C.[Sr_No_Q] AS INT) AS [Sr_No_Q] ,CASE WHEN SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS QUARTER_RX_ELIG
INTO #preIndex
from #temp1 A
JOIN  #IndexQuarterNo B
ON A.patient_id =B.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
where C.Sr_No_Q BETWEEN B.Sr_No_Q-4 AND B.Sr_No_Q
GROUP BY A.PATIENT_ID,YEAR_,QUARTER_INFO,C.[Sr_No_Q]



IF OBJECT_ID('tempdb..#postIndex') IS NOT NULL
DROP TABLE #postIndex

select A.PATIENT_ID,YEAR_,QUARTER_INFO,CAST(C.[Sr_No_Q] AS INT) AS [Sr_No_Q] ,CASE WHEN SUM(CASE WHEN RX_ACTIVITY = 'Y' THEN 1 ELSE 0 END) > 0 THEN 1 ELSE 0 END AS QUARTER_RX_ELIG
INTO #postIndex
from #temp1 A
JOIN  #IndexQuarterNo B
ON A.patient_id =B.patient_id
LEFT JOIN [$(TargetSchema)].[std_REF_time_dimension] C
ON A.YEAR_ = C.[Year] AND A.QUARTER_INFO =C.[Quarter]
where C.Sr_No_Q >= B.Sr_No_Q
GROUP BY A.PATIENT_ID,YEAR_,QUARTER_INFO,C.[Sr_No_Q]

----------------------------------------------------------------------

/* Iteration 4 changes consider records till last Rx eligibility =1 */

Print 'Populate #MaxElg'
Print Getdate()

IF OBJECT_ID('tempdb..#MaxElg') IS NOT NULL
DROP TABLE #MaxElg

SELECT Patient_id,MAX(Sr_No_Q) Sr_No_Q 
INTO #MaxElg
FROM #postIndex  
WHERE QUARTER_RX_ELIG =1
GROUP BY patient_id

IF OBJECT_ID('tempdb..#postIndexLimited') IS NOT NULL
DROP TABLE #postIndexLimited

SELECT A.*  
INTO #postIndexLimited
FROM #postIndex A
JOIN #MaxElg B
ON A.Patient_id =B.Patient_id
AND A.Sr_No_Q <= B.Sr_No_Q

Print 'Populate #Elg_4_5_Q'
Print Getdate()

IF OBJECT_ID('tempdb..#Elg_4_5_Q') IS NOT NULL
DROP TABLE #Elg_4_5_Q

select patient_id ,MAX(Sr_No_Q) Maxx ,MIN(Sr_No_Q) Minn,CASE WHEN MAX(Sr_No_Q)-3 <0 THEN 0 ELSE MAX(Sr_No_Q)-3  END as Minus4 
,CASE WHEN MAX(Sr_No_Q)-4 <0 THEN 0 ELSE MAX(Sr_No_Q)-4 END  Minus5 
,CASE WHEN MIN(Sr_No_Q) <= CASE WHEN MAX(Sr_No_Q)-3 <0 THEN 0 ELSE MAX(Sr_No_Q)-3  END THEN  1 ELSE 0 END AS Elg_4 
,CASE WHEN MIN(Sr_No_Q) <= CASE WHEN MAX(Sr_No_Q)-4 <0 THEN 0 ELSE MAX(Sr_No_Q)-4  END THEN  1 ELSE 0 END AS Elg_5
,COUNT(*) cuntt , SUM(QUARTER_RX_ELIG) AS Summ
INTO #Elg_4_5_Q
from #preIndex 
GROUP BY  patient_id


--------------------------------------------------------------------------------------------------------

--CASE 2 : Patient with 5 quarter eligibility  without ignoring missing values

Print 'Populate #Patient_5Q_Final_List'
Print Getdate()

IF OBJECT_ID('tempdb..#Patient_5Q_Final_List') IS NOT NULL
DROP TABLE #Patient_5Q_Final_List

select A.patient_id
INTO #Patient_5Q_Final_List
from #preIndex A
JOIN #Elg_4_5_Q B
ON A.patient_id =B.patient_id
AND A.Sr_No_Q BETWEEN  B.Minus5  and B.Maxx
WHERE Elg_5 =1
GROUP BY A.patient_id
having SUM(QUARTER_RX_ELIG) = Count(QUARTER_RX_ELIG) 
	AND Count(QUARTER_RX_ELIG) = 5



------------------------------------------------------------------------------------------

Print 'Populate #Elg_After_Q'
Print Getdate()

IF OBJECT_ID('tempdb..#Elg_After_Q') IS NOT NULL
DROP TABLE #Elg_After_Q

SELECT patient_id ,MAX(Sr_No_Q) Maxx,MIN(Sr_No_Q) Minn, (MAX(Sr_No_Q)- MIN(Sr_No_Q))+1 DiffFull,COUNT(*) Cntfull ,SUM(QUARTER_RX_ELIG) as summ
INTO #Elg_After_Q
FROM #postIndexLimited 
GROUP BY patient_id 



--------------------------------------------------------------------------------------------------
--CASE 5 : Patient with all quarters after index date and without ignoring missing Values

Print 'Populate #AfterIndex'
Print Getdate()

IF OBJECT_ID('tempdb..#AfterIndex') IS NOT NULL
DROP TABLE #AfterIndex

SELECT DISTINCT patient_id
INTO #AfterIndex
FROM #Elg_After_Q
WHERE DiffFull = cntfull AND  cntfull = summ


--------------------------------------------------------------------------------------------------
--CASE WHEN PriorIndex has 5  quarters with NO missing values and all quarters after Index  With NO Missing values --70885

IF OBJECT_ID('tempdb..#temp3') IS NOT NULL
DROP TABLE #temp3

SELECT DISTINCT patient_id INTO #temp3 FROM #Patient_5Q_Final_List
INTERSECT
SELECT DISTINCT patient_id FROM #AfterIndex
 
-----------------------------------------Update Q_Rx_Elg column-------------------------------------------

Update [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
set Q_Rx_Elg = 0

Update [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
set [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.Q_Rx_Elg = 1
FROM #temp3 T
WHERE  [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.patient_id=T.patient_id

---------------------------------------------------------------------------------------------------------

/*

IF OBJECT_ID('tempdb..#Regiments_Treatment') IS NOT NULL
DROP TABLE #Regiments_Treatment

SELECT  --ROW_NUMBER() over(partition by C.Patient_id,Start_Point order by  C.Patient_id,Start_Point,aed) as ID,
		C.Patient_id,
		Start_Point AS Start_Point_Original,
		CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point)  ELSE Start_Point END AS Start_Point,
		End_Point AS End_Point_Original,
		CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END   AS End_Point,
		Strt_End_Combination,
		D.AED,  Abbreviation,
		--DATEDIFF(dd,CASE WHEN Strt_End_Combination LIKE 'End%'  THEN DATEADD(DD,1,Start_Point) ELSE Start_Point END ,CASE WHEN Strt_End_Combination LIKE  '%Start' THEN DATEADD(DD,-1,End_Point) ELSE End_Point END ) +1 
		NULL AS Regiment_Days
		INTO #Regiments_Treatment
FROM (
				SELECT		Patient_id,
							Event_date AS Start_Point, 
							LEAD(Event_Date) OVER (PARTITION BY patient_id ORDER BY Event_Date ) AS End_Point,
							eventstatus+'-'+LEAD(eventstatus) OVER (PARTITION BY patient_id ORDER BY Event_Date ) AS Strt_end_Combination
				FROM		(

							SELECT ROW_NUMBER() OVER (PARTITION BY patient_id ORDER BY Event_Date ) rownum,RANK() OVER (PARTITION BY patient_id,Event_Date ORDER BY eventstatus ) rankk,*
							FROM (
									SELECT patient_id,startdate AS Event_Date , 'Start' AS eventstatus  FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] WHERE DATEDIFF(DD,startdate,enddate)+1 > 30
									UNION 
									SELECT patient_id ,enddate, 'End'  AS eventstatus FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] WHERE DATEDIFF(DD,startdate,enddate)+1 > 30
								 )A 
							)B WHERE Rankk =1
				) C
LEFT JOIN  (SELECT patient_id,startdate,enddate,AED FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription](NOLOCK) WHERE DATEDIFF(DD,startdate,enddate)+1 > 30 ) D
ON C.patient_id = D.patient_id
AND C.Start_Point >= D.startdate AND C.End_Point <= D.Enddate
LEFT JOIN [$(TargetSchema)].[AED_Abbreviation] Abb
ON D.AED = Abb.AED
WHERE D.AED IS NOT NULL



UPDATE #Regiments_Treatment
SET #Regiments_Treatment.Regiment_Days = DATEDIFF(dd,start_point, End_point) +1 

DELETE FROM #Regiments_Treatment  WHERE Regiment_days <=0


IF OBJECT_ID('tempdb..#monotherapy_Patient') IS NOT NULL
DROP TABLE #monotherapy_Patient

SELECT DISTINCT A.patient_id 
INTO #monotherapy_Patient
FROM #Regiments_Treatment A
JOIN #MonoPatient B
ON  A.Patient_id =B.Patient_id
AND  A.Start_Point_Original = B.startdate
AND Regiment_Days >=60
*/

------------------------------Create and populate table with relevent patients for cohoet analysis---------------------------------
Print 'Populate app_dre_int_Cohort_Monotherapy'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].app_dre_int_Cohort_Monotherapy') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy

SELECT patient_id,startdate AS maxdate,enddate,aed,DATEDIFF(dd,startdate,enddate)+1 AS aed_days 
INTO [$(TargetSchema)].app_dre_int_Cohort_Monotherapy
FROM [$(TargetSchema)].[app_int_AED_Analysis_Continuous_Prescription] A
WHERE patient_id IN (select distinct patient_id from [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	AND AGE >= 18 AND Gender IN ('M','F') ) AND DATEDIFF(DD,startdate,enddate)+1 > 30 



ALTER TABLE  [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
ADD --Zip3 INT ,Zip1 INT,[State] Varchar(10),
AED_ROWNO INT , AED_Failure_Flag INT,Valid_Index DATE ,Unique_AED_Count INT ,First_Rx_date DATE , Last_Rx_Date DATE, First_dx_date DATE,Last_dx_Date DATE, [FirstRxDxServiceDate] DATE,[LastRxDxServiceDate] DATE,Patient_Exclusion_Flag INT
,Lookback_Present INT ,Lookforward_Present INT,AED_Success_Flag INT,Outcome_variable VARCHAR(10),Q_Rx_Elg INT,[DS_0_LessThan_0] BIT



--IF OBJECT_ID('tempdb..#ZipInfo') IS NOT NULL
--DROP TABLE #ZipInfo

--SELECT A.patient_id,A.maxdate,PP.ZIP3 AS Zip3_Code,Zip.[Zip1_code],Zip.[State] INTO #ZipInfo
--FROM app_dre_int_Cohort_Monotherapy A
--LEFT JOIN [$(TargetSchema)].[epilepsy_ref_patient](nolock)  PP
--ON  A.Patient_id =PP.patient_id
--LEFT JOIN [$(TargetSchema)].[USPS_zip3zip1codes](nolock) Zip
--ON PP.Zip3 =Zip.Zip3_Code



--UPDATE app_dre_int_Cohort_Monotherapy
--SET  app_dre_int_Cohort_Monotherapy.Zip3	 = #ZipInfo.Zip3_Code
--	,app_dre_int_Cohort_Monotherapy.Zip1	 = #ZipInfo.Zip1_code 
--	,app_dre_int_Cohort_Monotherapy.State = #ZipInfo.[State]
--FROM #ZipInfo 
--WHERE app_dre_int_Cohort_Monotherapy.patient_id = #ZipInfo.patient_id
--AND app_dre_int_Cohort_Monotherapy.maxdate = #ZipInfo.maxdate


------------Assigning RowNumber to each records of a patients---------------------
Print 'Populate #temp4'
Print Getdate()

IF OBJECT_ID('tempdb..#temp4') IS NOT NULL
DROP TABLE #temp4

SELECT ROW_NUMBER() OVER(PARTITION BY patient_id ORDER BY maxdate,enddate,aed) AS AED_ROWNO ,patient_id,maxdate,enddate,aed
INTO #temp4 FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 

UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET AED_ROWNO = #temp4.AED_ROWNO
FROM #temp4
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].patient_id =#temp4.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].maxdate=#temp4.maxdate
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].enddate=#temp4.enddate
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].aed=#temp4.aed

--Populate Index date


UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Valid_Index = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.Valid_Index
FROM [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Patient_id = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].maxdate = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.Valid_Index 


--Populate first_rx_date and last_rx_date


UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].First_Rx_date = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.First_Rx_date
,[$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Last_Rx_Date = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.Last_Rx_Date
FROM  [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].patient_id = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Valid_Index IS NOT NULL 

--Populate first_dx_date and last_dx_date


UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].First_dx_date = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.First_dx_date
,[$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Last_dx_Date = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.Last_dx_Date
FROM  [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].patient_id = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Valid_Index IS NOT NULL 

--Populate [FirstRxDxServiceDate] and [LastRxDxServiceDate]


UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].FirstRxDxServiceDate = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.FirstRxDxServiceDate
,[$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].LastRxDxServiceDate = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.LastRxDxServiceDate
FROM  [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].patient_id = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Valid_Index IS NOT NULL 

--Populate Lookback and lookforward period
UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 
SET Lookback_Present = CASE WHEN DATEDIFF(dd,[FirstRxDxServiceDate],Valid_Index)+1  > 365 THEN 1  ELSE 0 END
WHERE Valid_Index IS NOT NULL 

UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 
SET Lookforward_Present = CASE WHEN DATEDIFF(dd,Valid_Index,[LastRxDxServiceDate])+1  >  365 THEN 1  ELSE 0 END
WHERE Valid_Index IS NOT NULL 




UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 
SET AED_Failure_Flag = CASE WHEN aed_days <365 THEN 1 ELSE 0 END,
AED_Success_Flag = CASE WHEN aed_days <365 THEN 0 ELSE 1 END



--Populate Outcome Variable 
/* --Previous logic

IF OBJECT_ID('tempdb..#DRE_Classification') IS NOT NULL
DROP TABLE #DRE_Classification

SELECT Patient_id , SUM(Distinct_No_Of_Failure)  AS No_Of_Failure
INTO #DRE_Classification
FROM (
		SELECT DISTINCT Patient_id,AED,SUM(DISTINCT AED_Failure_Flag)  AS Distinct_No_Of_Failure
		FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 
		GROUP BY Patient_id,AED
	 )A
GROUP BY patient_id 


UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 
SET Minimum_3Failure_Flag = CASE WHEN #DRE_Classification.No_Of_Failure >= 3 THEN 1 ELSE 0 END
FROM #DRE_Classification 
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].patient_id =#DRE_Classification.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Valid_index IS NOT NULL
*/


UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] 
SET Unique_AED_Count = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.Distinct_No_Of_AED 
FROM [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts 
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].patient_id =[$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].Valid_Index IS NOT NULL



--Populate Patient_Exclusion_flag 
/*-- previous logic

IF OBJECT_ID('tempdb..#Patient_To_Be_Exclude') IS NOT NULL
DROP TABLE #Patient_To_Be_Exclude

SELECT patient_id,SUM(Distinct_cntt) AS cntt
INTO #Patient_To_Be_Exclude 
FROM (
		SELECT patient_id,AED,SUM(DISTINCT AED_Failure_Flag) AS Distinct_cntt 
		FROM app_dre_int_Cohort_Monotherapy 
		WHERE  patient_id in (SELECT DISTINCT patient_id FROM app_dre_int_Cohort_Monotherapy WHERE  Minimum_4Unique_AED_flag =0)
		GROUP BY  patient_id,AED
	)A
GROUP BY patient_id 
HAVING  SUM(Distinct_cntt) >=1 

UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET	Patient_Exclusion_Flag = 0
WHERE AED_ROWNO = 1

UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET	Patient_Exclusion_Flag = 1
FROM #Patient_To_Be_Exclude
WHERE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].patient_id =#Patient_To_Be_Exclude.patient_id
AND [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy].AED_ROWNO = 1
*/

--Patients having 2 or 3 distinct AEDs in look forward period are excluded. if distinct aed =1 then check for aed_days ... aed_days <365 then exclude else include as non refractory
UPDATE [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
SET	Patient_Exclusion_Flag = 0
WHERE Valid_Index IS NOT NULL

UPDATE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy
SET  Patient_Exclusion_Flag = CASE WHEN Unique_AED_Count = 1 THEN CASE WHEN AED_Failure_Flag  = 0 THEN  0 ELSE 1 END ELSE 1 END
WHERE  patient_id in (SELECT DISTINCT patient_id FROM [$(TargetSchema)].app_dre_int_Cohort_Monotherapy WHERE  Unique_AED_Count < 4) 
AND Valid_Index IS NOT NULL



UPDATE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy
SET Outcome_variable = CASE WHEN Unique_AED_Count >= 4 THEN 'DRE' ELSE 'Non_DRE' END 
WHERE Valid_Index IS NOT NULL AND Patient_Exclusion_Flag= 0 AND Lookback_Present= 1 AND Lookforward_Present = 1

UPDATE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy
SET [$(TargetSchema)].app_dre_int_Cohort_Monotherapy.Q_Rx_Elg = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.Q_Rx_Elg
FROM [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts
WHERE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy.patient_id = [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts.patient_id
AND [$(TargetSchema)].app_dre_int_Cohort_Monotherapy.Valid_Index IS NOT NULL



------Populate Column [DS_0_LessThan_0]  to identify the patients with dase supply <=0

Print 'Populate #PatientNegative'
Print Getdate()


IF OBJECT_ID('tempdb..#PatientNegative') IS NOT NULL
DROP TABLE #PatientNegative

SELECT DISTINCT A.patient_id  INTO  #PatientNegative
FROM  [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy](NOLOCK) A
LEFT JOIN [$(SrcSchema)].[src_rx_claims]   B
ON A.patient_id=B.patient_id 
AND B.service_date BETWEEN DATEADD(DD,-365,A.Valid_Index) AND DATEADD(DD,-1,A.Valid_Index)
WHERE DAYS_Supply <=0 


Print 'Populate #DS_0_LessThan_0'
Print Getdate()



IF OBJECT_ID('tempdb..#DS_0_LessThan_0') IS NOT NULL
DROP TABLE #DS_0_LessThan_0

select distinct A.patient_id,CASE WHEN B.patient_Id IS NOT NULL THEN 1 ELSE 0 END AS [DS_0_LessThan_0] 
INTO #DS_0_LessThan_0 
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy](NOLOCK) A
LEFT JOIN  #PatientNegative B
ON A.Patient_id=B.patient_Id

UPDATE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy
SET [$(TargetSchema)].app_dre_int_Cohort_Monotherapy.[DS_0_LessThan_0] = #DS_0_LessThan_0.[DS_0_LessThan_0]
FROM #DS_0_LessThan_0 
WHERE [$(TargetSchema)].app_dre_int_Cohort_Monotherapy.patient_id = #DS_0_LessThan_0.patient_id
AND [$(TargetSchema)].app_dre_int_Cohort_Monotherapy.Valid_Index IS NOT NULL

/* First monotherapy only as index (from Index Date within at least 60 days no other aed should be there) */
/* Patients who are taking other aed within 60 days of index date*/
IF OBJECT_ID('tempdb..#PolyWithin60DaysOfIndexDate') IS NOT NULL
DROP TABLE #PolyWithin60DaysOfIndexDate

SELECT DISTINCT a.Patient_ID
INTO #PolyWithin60DaysOfIndexDate
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] a WITH(NOLOCK)
INNER JOIN 
( SELECT * FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy] WITH(NOLOCK)
WHERE Valid_Index is not null
) b
ON a.Patient_ID = b.Patient_ID
WHERE --a.patient_id = '00000000000171254050' and
 b.valid_index <> a.maxdate
AND
 ((b.valid_index  between a.maxdate and a.enddate )
 or (dateadd(day, 59, b.valid_index) between a.maxdate and a.enddate) or (b.valid_index< a.maxdate and dateadd(day, 59, b.valid_index) > a.enddate))

 
 /* Delete rows for those patients who are not monotherapy within 60 days of index date */
 
DELETE 
FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy]
WHERE Patient_id IN 
(SELECT DISTINCT Patient_ID from #PolyWithin60DaysOfIndexDate)
------------------------------To get the counts for stats ----------------------------------------
/*
select COUNT(1) from app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 --2138651
select COUNT(1) from app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1  --2138651
select COUNT(1) from app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	--1915117
select COUNT(1) from app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	AND Gender IN ('M','F') AND Patient_Exclusion_Flag = 0 --624529
select COUNT(1) from app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	AND Gender IN ('M','F') AND Patient_Exclusion_Flag = 0 AND AGE >= 16 --517060
select COUNT(1) from app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	AND Gender IN ('M','F') AND Patient_Exclusion_Flag = 0 AND AGE >= 16 AND Lookback_Present = 1 --193495
select COUNT(1) from app_dre_int_Pipeline_Metrix_Patient_Counts WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	AND Gender IN ('M','F') AND Patient_Exclusion_Flag = 0 AND AGE >= 16 AND Lookback_Present = 1 AND Q_Rx_Elg = 1 --57824

select outcome_variable , COUNT(distinct patient_id) as Patient_count from [$(TargetSchema)].app_dre_int_Cohort_Monotherapy 	
where Valid_Index IS NOT NULL  And Patient_Exclusion_Flag=0 AND	Lookback_Present=1 
group by Outcome_Variable	
	
select outcome_variable , COUNT(distinct patient_id) as Patient_count from [$(TargetSchema)].app_dre_int_Cohort_Monotherapy 	
where Valid_Index IS NOT NULL  And Patient_Exclusion_Flag=0 AND	Lookback_Present=1 and patient_id in (select Distinct Patient_id from app_dre_int_Pipeline_Metrix_Patient_Counts where Q_Rx_Elg =1)
group by Outcome_Variable	
*/
