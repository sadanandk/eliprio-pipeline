-- ---------------------------------------------------------------------
-- - AUTHOR			: Suresh Gajera 
-- - USED BY		: 
-- - 
-- - PURPOSE		: create final DRE  cohort after applying exclusion criteria.
-- - NOTE			:  ELREFPRED-210
-- - Execution		: 
-- - Date			: 24-Apr-2018			

-- - Change History:	Change Date				Change Description
-- -					26-Apr-2018					Create the CSV file
-- -					27-Apr-2018					Updatd as per the new logic(ELREFPRED-224) of refractory and non refractory patient classification.
-- -					09-May-2018					removed the aptients not having Gender information.
-- -					31-Jul-2018					Added column DS_0_LessThan_0 to identify patients which has days supply <=0
-- -					04-Oct-2018					Added inclusion criteria Lookforward_Present = 1 [ELREFPRED-717]
-- -					22-May-2019                 Removed patient_exclusion criteria to include indeterminate patient in the cohort
-- ---------------------------------------------------------------------- 

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate app_dre_Cohort'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].app_dre_Cohort') IS NOT NULL
DROP TABLE [$(TargetSchema)].app_dre_Cohort


SELECT A.[patient_id]
      ,[maxdate]
      ,[enddate]
      ,[aed]
      ,[aed_days]
      ,A.[Valid_Index]
      ,[Unique_AED_Count]
      ,A.[First_Rx_date]
      ,A.[Last_Rx_Date]
      ,A.[Lookback_Present]
      ,A.[Lookforward_Present]
      ,[AED_Success_Flag]
	  --,CASE WHEN Valid_Index IS NOT NULL THEN CASE WHEN Unique_AED_Count >=4 THEN 'DRE' ELSE 'Non DRE' END ELSE NULL END AS [Target]
	  ,Outcome_variable,B.Q_Rx_Elg
	  ,[DS_0_LessThan_0]
INTO [$(TargetSchema)].app_dre_Cohort
 FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy](nolock) A 
 LEFT JOIN [$(TargetSchema)].app_dre_int_Pipeline_Metrix_Patient_Counts  B
 ON A.patient_id =B.patient_id
 AND A.Valid_index =B.Valid_Index
WHERE A.patient_id IN ( 
                     SELECT DISTINCT patient_id FROM [$(TargetSchema)].[app_dre_int_Cohort_Monotherapy](nolock) A 
					 WHERE Lookback_Present = 1  AND [Lookforward_Present] =1
					)

DELETE FROM [$(TargetSchema)].[app_dre_Cohort]
WHERE patient_id in ( 
SELECT DISTINCT A.patient_id FROM [$(TargetSchema)].[app_dre_Cohort] A
JOIN [$(TargetSchema)].app_int_Patient_Profile B
ON A.patient_id =B.patient_id
WHERE B.gender IS NULL OR B.gender = 'U' )





