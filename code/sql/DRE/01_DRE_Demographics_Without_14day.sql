-- ---------------------------------------------------------------------
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create the feature related to demographic for DRE model   
-- ----------------------------------------------------------------------  
 
--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate #DRE_IndxDate_Demographic_Details'
Print Getdate()

IF OBJECT_ID('tempdb..#DRE_IndxDate_Demographic_Details') IS NOT NULL 
DROP TABLE #DRE_IndxDate_Demographic_Details

select A.Patient_id,Valid_Index ,Sex Gender , CAST(SUBSTRING(CAST(A.Valid_Index AS VARCHAR),1,4)  AS INT )- YOB Age 
INTO #DRE_IndxDate_Demographic_Details
FROM [$(TargetSchema)].[app_DRE_int_Cohort_Monotherapy](nolock) A
JOIN [$(SrcSchema)].[src_REF_patient]  B
ON A.Patient_id= B.Patient_id
WHERE Valid_Index IS NOT NULL AND Patient_Exclusion_Flag = 0  AND lookback_Present =1 AND Lookforward_Present = 1


Print 'Populate app_dre_features_IndexDate_Demographic'
Print Getdate()

IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_Demographic]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_Demographic]

SELECT A.patient_id,Valid_Index,Gender,Age
		,Age + (Age*Age) AS AgePlusAge2 
		,CAST(CASE WHEN Gender = 'F' AND Age BETWEEN 18 AND 45 THEN 1 ELSE 0 END AS BIT) AS isInChildbearingAge
		,PP.ZIP3 AS Index_Zip3
		,Zip.[Zip1_code] AS Index_Zip1
		,Zip.[State] AS Index_State 
INTO    [$(TargetSchema)].[app_dre_features_IndexDate_Demographic]
FROM #DRE_IndxDate_Demographic_Details A
LEFT JOIN [$(SrcSchema)].[src_REF_patient](nolock)  PP
ON  A.Patient_id =PP.patient_id
LEFT JOIN [$(TargetSchema)].[std_ref_USPS_zip3zip1codes](nolock) Zip
ON PP.Zip3 =Zip.Zip3_Code

