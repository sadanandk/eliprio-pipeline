﻿-- ---------------------------------------------------------------------
-- - AUTHOR    : Sneha M
-- - USED BY   : 
-- - PURPOSE   : Create scropts for DRE patients statistics 
-- - NOTE      :   Part 1: Scripts for Q_Rx_Elg =1 
-- - Execution : 
-- - Date      : 14-Sept-2018
-- - Change History:	Change Date				Change Description
-- -					
-- ---------------------------------------------------------------------- 

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema


SET Nocount ON

--******************************************* START: Confluence Page Graphs with Q_Rx_Elg**************************************************--

SELECT COUNT(DISTINCT patient_id) [Patients in Raw Dataset:] from [$(SrcSchema)].[src_ref_patient]
PRINT ''

PRINT 'Filtering Criterias for DRE Cohort:'
PRINT ''

select '1. Patient had more than or equal to 1 epilepsy diagnosis claim with ICD9 code 345.* or ICD10 code G40.*, or 2 claims for convulsions, including ICD9 code 780.39 or ICD10 code R56.9' Criteria,
COUNT(Distinct Patient_id) [#PatientCount]
from [$(TargetSchema)].app_DRE_int_Pipeline_Metrix_Patient_Counts 
WHERE [IsEpilepsy] =1
union
select '2. The first AED was prescribed as monotherapy with >=60 days supply',
COUNT(Distinct Patient_id)  [#PatientCount]
from [$(TargetSchema)].app_DRE_int_Pipeline_Metrix_Patient_Counts 
WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	
union
select '3. Age>=16',COUNT(Distinct Patient_id) 
from [$(TargetSchema)].app_DRE_int_Pipeline_Metrix_Patient_Counts 
WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	
AND Gender IN ('M','F') AND Patient_Exclusion_Flag = 0 AND AGE >= 16 
union
select '4. Patients with at least 1 year (365 days) of history prior to index date',
COUNT(Distinct Patient_id) 
from [$(TargetSchema)].app_DRE_int_Pipeline_Metrix_Patient_Counts  
WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	
AND Gender IN ('M','F') AND Patient_Exclusion_Flag = 0 AND AGE >= 16 AND Lookback_Present = 1 
union
select '5. Data Completeness :patients with active quarterly pharmacy claims every year',
COUNT(Distinct Patient_id) 
from [$(TargetSchema)].app_DRE_int_Pipeline_Metrix_Patient_Counts
WHERE [IsEpilepsy] =1 AND AtleastOneAed  =1 AND Start_Mono =1	
AND Gender IN ('M','F') AND Patient_Exclusion_Flag = 0 AND AGE >= 16 
AND Lookback_Present = 1 AND Q_Rx_Elg = 1 
PRINT ''

PRINT ''
PRINT 'DRE Cohort Statistics - Summary:'
PRINT ''

BEGIN
DECLARE @Total_Patient INT
SELECT @Total_Patient = COUNT(DISTINCT patient_id)  FROM [$(TargetSchema)].app_DRE_Cohort WHERE Q_Rx_Elg = 1

SELECT @Total_Patient AS Total_Patient_DRE_Cohort
PRINT ''
PRINT ''

SELECT Outcome_variable,COUNT(DISTINCT patient_id) Patient_Count , (COUNT(DISTINCT patient_id))*100.0 /@Total_Patient   AS [%TotalCohortPatients]
FROM [$(TargetSchema)].app_DRE_Cohort WHERE Q_Rx_Elg = 1 
GROUP BY Outcome_variable
END


IF OBJECT_ID('tempdb..#distribution_QRX') IS NOT NULL 
DROP TABLE #distribution_QRX

SELECT A.patient_id,age,gender,CASE WHEN B.Unique_AED_Count >=4 THEN 'Refractory' ELSE 'Non-Refractory' END as outcome
INTO #distribution_QRX
FROM [$(TargetSchema)].app_DRE_Features_IndexDate_Demographic A
JOIN [$(TargetSchema)].app_DRE_int_COHORT_Monotherapy  B
ON A.patient_id =B.patient_id
AND A.valid_index = B.Valid_index
WHERE Q_Rx_Elg = 1



PRINT ''
Print 'Cohort Gender Distribution:'
PRINT ''

SELECT Gender= case when gender='M' then 'Male' 
					when gender='F' then 'Female' 
					else Gender
				end
, count(distinct PAtient_id) [#PatientCount]
, (COUNT(DISTINCT patient_id))*100.0 /@Total_Patient   AS [%TotalCohortPatients]
from #distribution_QRX
group by gender

PRINT ''

PRINT ''
Print 'DRE Cohort Gender Distribution:'
PRINT ''


PRINT ''

PRINT 'Cohort Age Distribution:'
PRINT ''

IF OBJECT_ID('tempdb..#pop') IS NOT NULL 
DROP TABLE #pop

SELECT Population = CASE WHEN age between 0 AND 64 THEN 'Young (0-64)'
						 WHEN age >=65 THEN 'Senior(65 &Above)'
					END
, patient_id
into #pop
from #distribution_QRX

select [Population], count(distinct patient_id) [#PatientCount],(COUNT(DISTINCT patient_id))*100.0 /@Total_Patient   AS [%TotalCohortPatients]
from #pop
group by [Population]