--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

If (Schema_ID('$(TargetSchema)')) is null 
Begin
	RaisError('Schema is not generated,Please create the schema',16,1)
End
Else
	Begin
		If (Object_id('$(TargetSchema).AuditTable')) is null
			Begin
				RaisError('No Tables are present, Please check the path',16,1)
			End
		Else 
			Begin
				If (Select Flag from [$(TargetSchema)].AuditTable where step='Preprocess-Table structure') <> 'Y' 
					RaisError('Table structure not created, perform preprocessing step',16,1)
				Else
					PRINT 'Table creation validation completed successfully'
			End

	End
