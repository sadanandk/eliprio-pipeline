-- ------------------------------------------------------------------ 
-- - AUTHOR    : Suresh Gajera.
-- - USED BY   : 
-- - PURPOSE   : Create the table to stored the features related to index. i.e. CCI (Charlson co-morbidity Index) and ECI (Epilepsy co-morbidity Index)
-- - Execution : 
-- - Date      : 23-Apr-2018		
	-- ---------------------------------------------------------------------- 


----------------------------------------------------START: Create the pivot table DRE_DX_8_to_1_Claims_Pivot if not exist ----------------------------------------------------

--:setvar Schema TestPipeline
--:Setvar SrcSchema Src_Schema

Print 'Populate app_dre_int_DX_8_to_1_Claims_Pivot'
Print Getdate()

--Create the pivot table which will stores the value of 8 DX columns into one column
IF OBJECT_ID('[$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot]') IS Not NULL 
Drop Table [$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot]

SELECT DISTINCT  patient_id,service_date ,Codes_Column,B.[CCS CATEGORY DESCRIPTION],Type,Dx_Codes   into [$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot]
FROM
	(
		SELECT [PATIENT_ID], SERVICE_DATE ,[DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8]
    	FROM [$(TargetSchema)].[app_dre_int_DX_Claims] A with(nolock)
		--where exists (select 1 from dbo.decimal_ICD_rown B with(nolock) where A.rownum=b.rownum )--969

	)strw
	UNPIVOT
	(
	    Dx_Codes FOR Codes_Column IN ([DIAG1]	  ,[DIAG2]      ,[DIAG3]      ,[DIAG4]      ,[DIAG5]      ,[DIAG6]      ,[DIAG7]      ,[DIAG8])
	) AS A
LEFT JOIN [$(TargetSchema)].[std_ref_ICD_Code_ALL]  B   --Lookyp table which has CCS Columns names that need to be matched
ON A.Dx_Codes = REPLACE(B.icd_code,'.','')


-----------------------------------------------------END: Create the pivot table DRE_DX_8_to_1_Claims_Pivot if not exist ----------------------------------------------------

----------------------------------------------------START: Create the CCI table  ----------------------------------------------------
Print 'Populate app_dre_features_IndexDate_CCI_ECI'
Print Getdate()


IF OBJECT_ID('[$(TargetSchema)].[app_dre_features_IndexDate_CCI_ECI]') IS NOT NULL 
DROP TABLE [$(TargetSchema)].[app_dre_features_IndexDate_CCI_ECI]


SELECT patient_id,Valid_Index,Age
,CAST([Cerebrovascular_Disease]					AS BIT ) AS [Cerebrovascular_Disease]
,CAST([Chronic_Pulmonary_Disease]				AS BIT ) AS [Chronic_Pulmonary_Disease]
,CAST([Connective_Tissue_Disease]				AS BIT ) AS [Connective_Tissue_Disease]
,CAST([Diabetes_Without_Organ_Damage]			AS BIT ) AS [Diabetes_Without_Organ_Damage]
,CAST([Mild_Liver_Disease]						AS BIT ) AS [Mild_Liver_Disease]
,CAST([Myocardial_Infarction]					AS BIT ) AS [Myocardial_Infarction]
,CAST([Peptic_Ulcer_Disease]					AS BIT ) AS [Peptic_Ulcer_Disease]
,CAST([Congestive_Heart_Failure]				AS BIT ) AS [Congestive_Heart_Failure]
,CAST([Dementia]								AS BIT ) AS [Dementia]
,CAST([Peripheral_Vascular_Disease]				AS BIT ) AS [Peripheral_Vascular_Disease]
,CAST([Diabetes_With_Organ_Damage]				AS BIT ) AS [Diabetes_With_Organ_Damage]
,CAST([Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]	AS BIT ) AS [Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]
,CAST([Hemiplegia_or_Paraplegia]				AS BIT ) AS [Hemiplegia_or_Paraplegia]
,CAST([Mod_Severe_Renal_Disease]				AS BIT ) AS [Mod_Severe_Renal_Disease]
,CAST([Mod_Severe_liver_Disease]				AS BIT ) AS [Mod_Severe_liver_Disease]
,CAST([AIDS_HIV]								AS BIT ) AS [AIDS_HIV]
,CAST([Metastatic_Solid_Tumor]					AS BIT ) AS [Metastatic_Solid_Tumor]
,CAST([Cardiac_Arrhythmias]						AS BIT ) AS [Cardiac_Arrhythmias]
,CAST([Hypertension]							AS BIT ) AS [Hypertension]
,CAST([Pulmonary_Circulation_Disorders]			AS BIT ) AS [Pulmonary_Circulation_Disorders]
,CAST([Aspiration_Pneumonia]					AS BIT ) AS [Aspiration_Pneumonia]
,CAST([Solid_Tumor_Without_Metastases]			AS BIT ) AS [Solid_Tumor_Without_Metastases]
,CAST([Anoxic_Brain_Injury]						AS BIT ) AS [Anoxic_Brain_Injury]
,CAST([Brain_Tumor]								AS BIT ) AS [Brain_Tumor]
,CAST([Depression]								AS BIT ) AS [Depression]
,CAST([Psychoses]								AS BIT ) AS [Psychoses]

,([Peptic_Ulcer_Disease]+
[Myocardial_Infarction]+
[Mild_Liver_Disease]+
[Peripheral_Vascular_Disease]+
[Connective_Tissue_Disease]+
[Chronic_Pulmonary_Disease]+
[Congestive_Heart_Failure]+
[Cerebrovascular_Disease]+
[Diabetes_Without_Organ_Damage]+
[Dementia]
)*1 
+
(
[Hemiplegia_or_Paraplegia]+
[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin]+
[Diabetes_With_Organ_Damage]+
[Mod_Severe_Renal_Disease]
)*2
+
[Mod_Severe_liver_Disease]*3
+
(
[Metastatic_Solid_Tumor]+
[AIDS_HIV]
)*6
+
CASE   WHEN Age BETWEEN 50 AND 59 THEN 1
       WHEN Age BETWEEN 60 AND 69 THEN 2
       WHEN Age BETWEEN 70 AND 79 THEN 3
       WHEN Age BETWEEN 80 AND 89 THEN 4
       WHEN Age BETWEEN 90 AND 99 THEN 5
       WHEN Age BETWEEN 100 AND 109 THEN 6
       WHEN Age >= 110 THEN 7
       ELSE 0 
END	   AS CCI_Score

,
(
[Hypertension] +
[Cardiac_Arrhythmias]+
[Pulmonary_Circulation_Disorders] 
)*1
+
(
[Aspiration_Pneumonia]+
[Hemiplegia_or_Paraplegia]+
CASE WHEN [Solid_Tumor_Without_Metastases] = 1 AND  [Metastatic_Solid_Tumor] =1 THEN 0 ELSE [Solid_Tumor_Without_Metastases] END+
[Peripheral_Vascular_Disease]+
[Mod_Severe_Renal_Disease]+
[Congestive_Heart_Failure]+
[Dementia]
)*2
+
(
[Mod_Severe_liver_Disease]+
[Anoxic_Brain_Injury]+
CASE WHEN [Brain_Tumor] = 1 AND  [Metastatic_Solid_Tumor] =1 THEN 0 ELSE [Brain_Tumor] END
)*3 
+
(
[Metastatic_Solid_Tumor]
)*6 AS ECI_Score
INTO [$(TargetSchema)].[app_dre_features_IndexDate_CCI_ECI]
FROM
(
	SELECT distinct A.patient_id,A.Valid_Index,A.Age,CCI_Category, 1 AS COUNTT
	FROM [$(TargetSchema)].[app_dre_features_IndexDate_Demographic] (NOLOCK) A
	LEFT JOIN (
				SELECT		patient_id,service_date,cci_category,Codes FROM [$(TargetSchema)].[app_dre_int_DX_8_to_1_Claims_Pivot] B (NOLOCK)
				INNER JOIN	[$(TargetSchema)].[std_ref_CCI]  C (NOLOCK)
				ON			B.Dx_Codes = REPLACE(C.codes,'.','')
			  ) B
	ON A.Patient_id = B.patient_id
	AND B.Service_date BETWEEN  DATEADD(DD,-365,A.Valid_Index)  AND   DATEADD(DD,-1,A.Valid_Index)

) AS SourceTable
PIVOT
(
COUNT(COUNTT)
FOR [CCI_Category] IN (
						[Myocardial_Infarction],[Hemiplegia_or_Paraplegia],[Mod_Severe_liver_Disease],[Cerebrovascular_Disease],[Anoxic_Brain_Injury],[Metastatic_Solid_Tumor],[Cardiac_Arrhythmias],[Peripheral_Vascular_Disease],[Mod_Severe_Renal_Disease],[Congestive_Heart_Failure]
						,[Chronic_Pulmonary_Disease],[AIDS_HIV],[Dementia],[Diabetes_With_Organ_Damage],[Pulmonary_Circulation_Disorders],[Hypertension],[Aspiration_Pneumonia],[Brain_Tumor],[Solid_Tumor_Without_Metastases],[Peptic_Ulcer_Disease],[Connective_Tissue_Disease],
						[Maligncy_lym_leuk_ex_mlgnt_neoplsm_skin],[Diabetes_Without_Organ_Damage],[Mild_Liver_Disease],[Depression],[Psychoses]
						)                                                                                             
) AS PivotTable


----------------------------------------------------END: Create the CCI table [PSPA_IndxDate_CCI_ECI_Details] ----------------------------------------------------

