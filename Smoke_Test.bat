@ECHO OFF

IF "%PASSWORD%"=="" GOTO MissingPasswd

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat


:DeleteLogFiles
REM call %BatchDir%\common\deletefiles.bat %_Log%

setlocal enabledelayedexpansion
REM Call SmokeTest batch file 
echo %_Date% %time% Smoke Test Results 
echo %_Date% %time% Smoke Test Results >Batchlog.txt

:SmokeTest
echo %_Date% %time%  Validation for counts after pipeline executed started
for /F "tokens=*" %%A in ('type "%BatchDir%\%SmokeTestList%"') do (
echo %_Date% %time% %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %INPUT%\SmokeTestRun\%%A.sql -v TargetSchema=%TargetSchema% -o %OUTPUT%\%%A.txt -I -b
IF !ERRORLEVEL! NEQ 0 (ECHO %_Date% %time% ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file %OUTPUT%\%%A.txt && GOTO Error ) 
ECHO %_Date% %time% %%A completed
)

Goto End


REM If any error is encountered 
:ERROR
rem copy BatchLog.txt %OUTPUT%\BatchLog.txt
echo %_Date% %time%  Pipeline: You encountered Error! Check log file for further detail.
GOTO Exit

REM On successful execution
:End
IF %ERRORLEVEL%==0 ( echo %_Date% %time% SUCCESSFULLY executed Pipeline )
Rem copy BatchLog.txt %OUTPUT%\BatchLog.txt>>Batchlog.txt

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto Exit

:Exit
cmd /k