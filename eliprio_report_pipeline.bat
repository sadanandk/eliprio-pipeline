@ECHO OFF
SETLOCAL enableDelayedExpansion

IF "%PASSWORD%"=="" GOTO MissingPasswd

REM Read config files for required values  
IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

if not exist "input" mkdir "input"
if not exist "output" mkdir "output"
if not exist "log" mkdir "log"

REM set enviornment variables
set PATH=%PATH%;C:\Program Files\7-Zip\;
set PATH=%PATH%;C:\Program Files\R\R-3.5.0\bin;
SET rootDir=%~dp0


REM set outDataPath to network path if DB resides on other machine
if "%nwrootPath%" NEQ "" (
SET outDataPath=%nwrootPath%\output
echo setting up n/w path to %nwrootPath% >Batchlog.txt
) ELSE (
SET outDataPath=%~dp0\output
echo setting up outDataPath to %outDataPath% >Batchlog.txt
)

REM SET outDataPath=%~dp0\output

echo %_Date% %time% START Reporting execution
echo.

:DeleteLogFiles
call %BatchDir%\common\deletefiles.bat %_Log%

REM delete old output files
REM call %BatchDir%\common\deletefiles.bat %outputDir%


echo %_Date% %time% Started Reporting pipeline >Batchlog.txt

copy Config.txt %OUTPUT%\Config.txt >>Batchlog.txt

:Reporting
Rem IF "%RunDRECohort%"=="N" ( GOTO TDS_Reporting )
IF "%RunDRECohort%"=="Y" ( GOTO DRE_Reporting ) ELSE ( GOTO TDS_Reporting )

:DRE_Reporting
REM Call Reporting batch file 
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for DRE
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for DRE >>Batchlog.txt
IF not exist %BatchDir%\Reporting\DRE_Reporting.bat ( echo %_Date% %time% Error Message: DRE_Reporting.bat file does not exist! >>Batchlog.txt && GOTO Error )
Call %BatchDir%\reporting\DRE_Reporting.bat %~dp0 %_DT% %outDataPath% >>BatchLog.txt
REM IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Reporting.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Reporting.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: Some of the reports has not been generated for DRE. Please check the logs) 
echo.

:TDS_Reporting
IF "%RunTDSCohort%"=="N" ( GOTO END )
REM Call Reporting batch file 
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for TDS
echo %_Date% %time% Generate Clinical Stats and Descriptive Reports for TDS >>Batchlog.txt
IF not exist %BatchDir%\reporting\TDS_Reporting.bat ( echo %_Date% %time% Error Message: TDS_Reporting.bat file does not exist! >>Batchlog.txt && GOTO Error )
Call %BatchDir%\reporting\TDS_Reporting.bat %~dp0 %_DT% >>BatchLog.txt
REM IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS_Reporting.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_Reporting.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: Some of the reports has not been generated for TDS. Please check the logs)  
echo.

Goto End

REM On Error
:ERROR
copy BatchLog.txt %OUTPUT%\BatchLog.txt
echo %_Date% %time%  Pipeline: You encountered Error! Check log file for further detail.
SET executionName=%executionName%Error_report_
GOTO ARCHIVE

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto Exit

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed Pipeline
copy BatchLog.txt %OUTPUT%\BatchLog.txt>>Batchlog.txt
IF "%executionName%"=="" ( SET executionName=%TargetSchema%_report_ )
GOTO ARCHIVE

:ARCHIVE
echo.
echo creating archive file >>BatchLog.txt
call %BatchDir%\archive.bat %_DT% %executionName% %OUTPUT% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 (
ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in archive.bat 
) ELSE (
ECHO %_Date% %time% Created archive file with name %executionName%%_DT%.zip 
)

:Exit
cmd /k