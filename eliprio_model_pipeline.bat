@ECHO OFF
SETLOCAL EnableDelayedExpansion
REM Read config files for required values  
IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

if not exist "input" mkdir "input"
if not exist "output" mkdir "output"

REM set enviornment variables
set PATH=%PATH%;C:\Program Files\7-Zip\;
set PATH=%PATH%;C:\Program Files\R\R-3.5.1\bin;
SET rootDir=%~dp0

echo %_Date% %time% START Model scoring execution
echo.

:DeleteLogFiles
call %BatchDir%\common\deletefiles.bat %_Log%

REM delete old output files
call %BatchDir%\common\deletefiles.bat %outputDir%

copy Config.txt %_Log%\Config.txt >Batchlog.txt

REM set outDataPath to network path if DB resides on other machine
if "%nwrootPath%" NEQ "" (
SET outDataPath=%nwrootPath%\output
echo setting up n/w path to %nwrootPath% >Batchlog.txt
) ELSE (
SET outDataPath=%~dp0\output
echo setting up outDataPath to %outDataPath% >Batchlog.txt
)

:ENV_VALIDATION
ECHO.
ECHO %_Date% %time% Model environment setup validation is in progress...
CALL %BatchDir%\validate\model_env_validate.bat>>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Environment validation error, please check BatchLog for more details && GOTO Error) ELSE ( ECHO %_Date% %time% Environment setup found OK)
echo.

REM Copy Score file of Train and Test 
echo f | xcopy /y code\sql\Setup\CSV_for_Tables\src_sample\d5_dre_sha_score_1_0yr_Test.csv OUTPUT\d5_dre_sha_score_1_0yr_Test.csv >>Batchlog.txt
echo f | xcopy /y code\sql\Setup\CSV_for_Tables\src_sample\d5_dre_sha_score_1_0yr_Train.csv OUTPUT\d5_dre_sha_score_1_0yr_Train.csv >>Batchlog.txt
echo f | xcopy /y code\sql\Setup\CSV_for_Tables\src_sample\d6_5_d5_dre_sha_score_2_5yr_Test.csv OUTPUT\d6_5_d5_dre_sha_score_2_5yr_Test.csv >>Batchlog.txt
echo f | xcopy /y code\sql\Setup\CSV_for_Tables\src_sample\d6_5_d5_dre_sha_score_2_5yr_Train.csv OUTPUT\d6_5_d5_dre_sha_score_2_5yr_Train.csv >>Batchlog.txt
echo f | xcopy /y code\sql\Setup\CSV_for_Tables\src_sample\KPI.csv OUTPUT\KPI.csv >>Batchlog.txt

IF "%RunDRECohort%"=="Y" (
GOTO ModelScoringPipeline_DRE 
) ELSE (
IF "%RunTDSCohort%"=="Y" (GOTO TDS_Feature_Encode) ELSE (GOTO END)
)

:ModelScoringPipeline_DRE
REM Call Model Scoring Pipeline
SET featureFilePre=DRE_feature
SET Threshold=%DRE_Threshold%
echo %_Date% %time% Generate Scored file using DRE Model 
echo %_Date% %time%  Prepare Scored file using DRE Model >>BatchLog.txt
IF not exist %ModelDir%\ModelScoringPipeline.bat ( echo %_Date% %time% Error Message: ModelScoringPipeline.bat file does not exist! >>BatchLog.txt && GOTO Error )
IF not exist %ModelDir%\libs\ ( echo %_Date% %time% Error Message: libs folder does not exist! >>BatchLog.txt && GOTO Error )
IF not exist %Report%\DRE_feature.csv ( echo %_Date% %time% WARN Message: feature file for DRE does not exist! >>BatchLog.txt && GOTO Error)
Call %ModelDir%\ModelScoringPipeline.bat %DRE% %DREmodel% %Report% %DREModel% %_DT% %outputDir% %_Log% %featureFilePre% %Threshold%>>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model: ERRORLEVEL %ERRORLEVEL% in DRE ModelScoringPipeline.bat) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_ModelScoringPipeline.bat )
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)

:GENERATE_DRE_1Yr_SUBPOP_REPORT
echo %_Date% %time% DRE 1 Year Subpop Report >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_Subpop_1yr.bat ( echo %_Date% %time% Error Message: DRE_Subpop_1yr.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_Subpop_1yr.bat %DRECCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Subpop_1yr.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Subpop_1yr.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: 2.5 Evaluation Period Report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt


REM START
REM echo.
REM echo %_Date% %time% Generate DRE 2.5 Evaluation Period Report
REM IF "%RunDRECohort%"=="Y" (GOTO GENERATE_DRE_2_5_REPORT) ELSE (GOTO END)

:GENERATE_DRE_2_5_REPORT

REM echo %_Date% %time% Generate KPI and Summary reports 
echo %_Date% %time% DRE:: Generate KPI report file
IF not exist %outputDir%\%Score_rpt%%DREmodel%_score.csv ( echo %_Date% %time% WARN Message: score file for DRE does not exist! >>log\model_reports.txt && GOTO Error )
Call %BatchDir%\modelreports\RModelKPI_1Yr.bat %rootDir% %outputDir% %DREmodel% %DRE% %_DT% %delimiter% >>log\model_reports.txt  2>&1
REM Call %BatchDir%\modelreports\RModelKPI_1Yr.bat %fileName% %modelType% %outputFile1% %delimiter% %metadataFile% %risk_out% %prevalence%
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model:: ERRORLEVEL %ERRORLEVEL% in DRE RModelKPI_1Yr.bat && GOTO ERROR )
IF %ERRORLEVEL% EQU 0 ( ECHO %_Date% %time% SUCCESSFULLY executed DRE RModelKPI_1Yr.bat, check output directory )

echo %_Date% %time% DRE 2.5 Evaluation Period Report >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_2_5yr_evl_period.bat ( echo %_Date% %time% Error Message: DRE_2_5yr_evl_period.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_2_5yr_evl_period.bat %DRECCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_2_5yr_evl_period.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_2_5yr_evl_period.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: 2.5 Evaluation Period Report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt

:GENERATE_DRE_2_5Yr_SUBPOP_REPORT
echo %_Date% %time% DRE 2_5 Year Subpop Report >>%Output%\DREReporting.txt
IF not exist %BatchDir%\reporting\DRE_Subpop_2_5yr.bat ( echo %_Date% %time% Error Message: DRE_Subpop_2_5yr.bat file does not exist! >>%Output%\DREReporting.txt && GOTO Error )

Call %BatchDir%\reporting\DRE_Subpop_2_5yr.bat %DRECCList% %SERVER% %DB% %LOGIN% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPath% %OUTPUT% %Report% %_DT% %outputDir% >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE_Subpop_2_5yr.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE_Subpop_2_5yr.bat ) >>%Output%\DREReporting.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% WARN:: 2.5 Evaluation Period Report has not been generated for DRE. Please check the logs in DREReporting.txt)
echo. >>%Output%\DREReporting.txt

IF "%RunTDSCohort%"=="Y" (GOTO TDS_Feature_Encode) ELSE (GOTO model_reporting)
:TDS_Feature_Encode
REM call to encode  input feature file
echo.
echo %_Date% %time% Encode feature file for TDS
IF not exist %Report%\TDS_feature.csv ( echo %_Date% %time% WARN Message: feature file for TDS does not exist! >>BatchLog.txt && GOTO Error )
Call %BatchDir%\modelreports\R_Encoding.bat %rootDir% %outputDir% %TDSmodel% %TDS% %_DT% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model: ERRORLEVEL %ERRORLEVEL% in TDS R_Encoding.bat) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS R_Encoding.bat )
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)
echo. >>BatchLog.txt
echo.

:ModelScoringPipeline_TDS
echo.
REM Call Model Scoring Pipeline
SET featureFilePre=TDS_feature_en
echo %_Date% %time% Generate Scored file using TDS Model 
echo %_Date% %time%  Prepare Scored file using TDS Model >>BatchLog.txt
IF not exist %ModelDir%\ModelScoringPipeline.bat ( echo %_Date% %time% Error Message: ModelScoringPipeline.bat file does not exist! >>BatchLog.txt && GOTO Error )
IF not exist %ModelDir%\libs\ ( echo %_Date% %time% Error Message: libs folder does not exist! >>BatchLog.txt && GOTO Error )
IF not exist %Report%\TDS_feature_en.csv ( echo %_Date% %time% WARN Message: feature file for TDS does not exist! >>BatchLog.txt && GOTO Error)
Call %ModelDir%\ModelScoringPipeline.bat %TDS% %TDSmodel% %Report% %TDSModel% %_DT% %outputDir% %_Log% %featureFilePre% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model: ERRORLEVEL %ERRORLEVEL% in TDS ModelScoringPipeline.bat) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS_ModelScoringPipeline.bat )
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)


:model_reporting
echo.
echo %_Date% %time% Generate KPI and Summary reports 
call %BatchDir%\modelreports\ModelReporting.bat >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Model: ERRORLEVEL %ERRORLEVEL% in ModelReporting.bat)
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)

GOTO END

REM On Error
:ERROR
copy BatchLog.txt %_Log%\BatchLog.txt
echo %_Date% %time%  Pipeline: You encountered Error! Check log file for further detail.
SET executionName=%executionName%Error_model_
GOTO ARCHIVE

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed Pipeline
copy BatchLog.txt %_Log%\BatchLog.txt>>BatchLog.txt
IF "%executionName%"=="" ( SET executionName=%TargetSchema%_model_)
GOTO ARCHIVE

:ARCHIVE
echo.
echo creating archive file >>BatchLog.txt
call %BatchDir%\archive.bat %_DT% %executionName% %_Log% >>BatchLog.txt
IF %ERRORLEVEL% NEQ 0 (
ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in archive.bat 
) ELSE (
ECHO %_Date% %time% Created archive file with name %executionName%%_DT%.zip 
)

:Exit
SET executionName=
cmd /k