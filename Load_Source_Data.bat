@ECHO OFF

IF "%PASSWORD%"=="" GOTO MissingPasswd

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

if "%nwrootPath%" NEQ "" (
SET SampleDataPath=%nwrootPath%\code\sql\Load_Source_Data\CSV\
echo setting up n/w path to %nwrootPath% >Batchlog.txt
) ELSE (
SET SampleDataPath=%~dp0\code\sql\Load_Source_Data\CSV\
echo setting up SampleDataPath to %SampleDataPath% >Batchlog.txt
)

setlocal enabledelayedexpansion

:Setup
echo %_Date% %time%  Loading started .......
for /F "tokens=*" %%A in ('type "%BatchDir%\Load_Source_Data.txt"') do (  
echo %_Date% !time! %%A is in progress... 
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %SQLDir%\Load_Source_Data\%%A.sql -v SrcSchema=%SrcSchema% -o log\%%A.txt -I -e -b
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file log\%%A.txt && GOTO Error ) 
ECHO %_Date% !time! %%A completed
echo.
)
GOTO End

:Error

echo You encountered Error! Please check the log file for further details... 

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto End

:End
cmd /k