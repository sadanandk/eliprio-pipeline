@ECHO OFF
REM Read config files for required values  
SETLocal

IF "%PASSWORD%"=="" GOTO MissingPasswd

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

if not exist "input" mkdir "input"
if not exist "output" mkdir "output"

REM set enviornment variables
set PATH=%PATH%;C:\Program Files\7-Zip\;
set PATH=%PATH%;C:\Program Files\R\R-3.5.0\bin;

echo %_Date% %time% Start Process Execution >Batchlog.txt | type Batchlog.txt

SET executionName=%1
if "%executionName%" NEQ "" (SET executionName=%executionName%_)

REM set sampleDataPath to network path if DB resides on other machine
if "%nwrootPath%" NEQ "" (
SET SampleDataPath=%nwrootPath%\code\sql\SampleDataRun\CSV_for_Tables\
echo setting up n/w path to %nwrootPath% >>Batchlog.txt
) ELSE (
SET SampleDataPath=%~dp0\code\sql\SampleDataRun\CSV_for_Tables\
echo setting up SampleDataPath to %SampleDataPath% >>Batchlog.txt
)

SET Src_Schema=%SrcSchema%
REM Create folder with date & timestamp under log folder & check if the folder already exists
IF not exist %_Log% ( mkdir %_Log% )

REM Delete any log files created in the previous execution 
:DeleteLogFiles
call %BatchDir%\common\deletefiles.bat %_Log%

REM deleteing old input files from the input folder
call %BatchDir%\common\deletefiles.bat %Report%

REM delete old output files
call %BatchDir%\common\deletefiles.bat %outputDir%

copy Config.txt %OUTPUT%\Config.txt >>Batchlog.txt

:ProcessSelection
IF "%PreProcess%"=="Y" ( GOTO PrePRocess ) ELSE ( IF "%RunDRECohort%"=="Y" ( GOTO ProcessDREData ) ELSE ( GOTO ENDDREProcess ) )

:PreProcess
echo.
REM Call Pre-processing batch file 
echo %_Date% %time% Pre-processing  data 
echo %_Date% %time% Pre-processing  data  >>Batchlog.txt

IF not exist %BatchDir%\PreProcess.bat ( echo %_Date% %time% Error Message: PreProcess.bat file does not exist! >>Batchlog.txt && GOTO Error )

CALL %BatchDir%\PreProcess.bat %PreProcessList% %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPATH% %SampleDataPath% %OUTPUT% %Report% %sampledatarun% %Src_Schema% >>Batchlog.txt

IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in PreProcess.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed PreProcess.bat && ECHO.) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO ERROR)
echo.
IF "%RunDRECohort%"=="Y" ( GOTO ProcessDREData ) ELSE ( GOTO ENDDREProcess )

:ProcessDREData
REM Call DRE Processing batch file 
echo %_Date% %time% Process data for DRE
echo %_Date% %time%  Process data for DRE >>Batchlog.txt
IF not exist %BatchDir%\DRE.bat ( echo %_Date% %time% Error Message: DRE.bat file does not exist! >>Batchlog.txt && GOTO Error )

CALL %BatchDir%\DRE.bat %DRElist% %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPATH% %SampleDataPath% %OUTPUT% %Report% %sampledatarun% %utilityDir% %Src_Schema% >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in DRE.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed DRE.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO Error ) 

:ENDDREProcess
IF "%RunTDSCohort%"=="Y" ( GOTO ProcessTDSData ) ELSE ( GOTO END )

:ProcessTDSData
REM Call TDS Processing batch file 
echo.
echo %_Date% %time% Process data for TDS
echo %_Date% %time%  Process data for TDS >>Batchlog.txt
IF not exist %BatchDir%\TDS.bat ( echo %_Date% %time% Error Message: TDS.bat file does not exist! >>Batchlog.txt && GOTO Error )

Call %BatchDir%\TDS.bat %TDSList% %SERVER% %DB% %Login% %PASSWORD% %TargetSchema% %SQLCMD% %ScriptPATH% %SampleDataPath% %OUTPUT% %Report% %sampledatarun% %utilityDir% %Src_Schema% >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in TDS.bat ) ELSE ( ECHO %_Date% %time% SUCCESSFULLY executed TDS.bat ) >>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( GOTO Error ) 
echo.

Goto End

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto Exit

REM If any error is encountered 
:ERROR
copy BatchLog.txt %OUTPUT%\BatchLog.txt
echo %_Date% %time%  Pipeline: You encountered Error! Check log file for further detail.
SET executionName=%executionName%Error_feature_
GOTO ARCHIVE

REM On successful execution
:End
echo %_Date% %time% SUCCESSFULLY executed Pipeline
copy BatchLog.txt %OUTPUT%\BatchLog.txt>>Batchlog.txt
IF "%executionName%"=="" ( SET executionName=%TargetSchema%_feature_)
GOTO ARCHIVE

:ARCHIVE
echo.
echo creating archive file >>BatchLog.txt
call %BatchDir%\archive.bat %_DT% %executionName% %OUTPUT% >>BatchLog.txt 2>&1
IF %ERRORLEVEL% NEQ 0 (
ECHO %_Date% %time% Pipeline: ERRORLEVEL %ERRORLEVEL% in archive.bat 
) ELSE (
ECHO %_Date% %time% Created archive file with name %executionName%%_DT%.zip 
)

:Exit
SET executionName=
cmd /k

