1) Eliprio_deployment_process.pdf: This document explains overall deployment process
2) Pipline_enviornment_setup.pdf: This document guides the overall environment setup
3) Batch_file_for_endtoend_pipeline.pdf: It contains the documentation of batch file which are used in end to end pipeline execution.
4) Cohort_and_feature_generation.pdf: This has the list of table name which are used in cohort and features generations.
5) Eliprio_app_std_ref_tables_datadictionary.xlsx: Data dictionary for standard reference table.
6) Eliprio_data_dictionary.docx: Data dictinary for source data set.
7) Security_documents.pdf: This document contains security information of environment.
8) SQLpreprocessing.pdf: It contains information of tables which are generated or used in pre processing.

