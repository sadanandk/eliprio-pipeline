@ECHO OFF
setlocal enabledelayedexpansion

IF "%PASSWORD%"=="" GOTO MissingPasswd

IF "%SERVER%"=="" (
call code\batch\common\loadconfig.bat
)
REM load variables
call code\batch\common\constants.bat

if not exist "log" mkdir "log"
if not exist "input" mkdir "input"
if not exist "output" mkdir "output"

REM set sampleDataPath to network path if DB resides on other machine
if "%nwrootPath%" NEQ "" (
SET SampleDataPath=%nwrootPath%\code\sql\Setup\CSV_for_Tables\
echo setting up n/w path to %nwrootPath% >Batchlog.txt
) ELSE (
SET SampleDataPath=%~dp0\code\sql\Setup\CSV_for_Tables\
echo setting up SampleDataPath to %SampleDataPath% >Batchlog.txt
)

set PATH=%PATH%;C:\Program Files\7-Zip\;
set PATH=%PATH%;C:\Program Files\R\R-3.5.1\bin;

:DeleteLogFiles
call %BatchDir%\common\deletefiles.bat %_Log%

SET executionName=Setup
echo SampleDataPath.. %SampleDataPath% >>Batchlog.txt

copy Config.txt %OUTPUT%\Config.txt >>Batchlog.txt

:ENV_VALIDATION
ECHO.
ECHO %_Date% %time% Environment setup validation is in progress...
CALL %BatchDir%\validate\feature_env_validate.bat>>Batchlog.txt
IF %ERRORLEVEL% NEQ 0 ( ECHO %_Date% %time% Environment validation error, please check BatchLog for more details && GOTO Error) ELSE ( ECHO %_Date% %time% Environment setup found OK)
echo.

:Setup
echo %_Date% !time! DB Setup started .......
echo.
echo %_Date% !time! DB Setup started ....... >>Batchlog.txt
for /F "tokens=*" %%A in ('type "%BatchDir%\Initialize_Setup.txt"') do (  

if %IsDW% EQU Y ( IF %%A NEQ %BulkInsertScript% ( 
echo %_Date% !time! %%A DW scripts excution is in progress...
echo %_Date% !time! %%A DW scripts excution is in progress...>>Batchlog.txt
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %SQLDir%\Setup\%%A.sql -v TargetSchema=%TargetSchema% -o log\%%A.txt -I -e -b >>Batchlog.txt 2>&1
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.bat >>Batchlog.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A >>Batchlog.txt )
IF !ERRORLEVEL! NEQ 0 ( echo %_Date% !time! Please check the log file log\%%A.txt >>Batchlog.txt && GOTO Error ) 
echo %_Date% !time! DW scripts execution completed
)
) ELSE (
echo %_Date% !time! %%A is in progress... 
echo %_Date% !time! %%A is in progress... >>Batchlog.txt
%SQLCMD% -S %SERVER% -d %DB% -U %LOGIN% -P %PASSWORD% -i %SQLDir%\Setup\%%A.sql -v TargetSchema=%TargetSchema% -o log\%%A.txt -I -e -b >>Batchlog.txt 2>&1
IF !ERRORLEVEL! NEQ 0 ( ECHO %_Date% !time! ERRORLEVEL !ERRORLEVEL! in %%A.bat >>Batchlog.txt ) ELSE ( ECHO %_Date% !time! SUCCESSFULLY executed %%A >>Batchlog.txt )
IF !ERRORLEVEL! NEQ 0 ( echo Please check the log file log\%%A.txt >>Batchlog.txt && GOTO Error ) 
echo %_Date% !time! %%A execution is completed 
)
echo.
)

GOTO end

:MissingPasswd
echo Environment variable for database password not set
echo Exiting...
Goto end

:Error
echo You encountered Error! Please check the BatchLog file for further details... 
setlocal disabledelayedexpansion

:end
setlocal disabledelayedexpansion
cmd /k